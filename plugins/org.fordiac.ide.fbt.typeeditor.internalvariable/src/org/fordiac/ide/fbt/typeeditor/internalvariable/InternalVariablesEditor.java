/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.internalvariable;

import java.text.MessageFormat;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.EditorPart;
import org.fordiac.ide.fbt.typeeditor.commands.ChangeArraySizeCommand;
import org.fordiac.ide.fbt.typeeditor.commands.ChangeInitialValueCommand;
import org.fordiac.ide.fbt.typeeditor.commands.ChangeNameCommand;
import org.fordiac.ide.fbt.typeeditor.commands.ChangeTypeCommand;
import org.fordiac.ide.fbt.typeeditor.editors.IFBTEditorPart;
import org.fordiac.ide.fbt.typeeditor.internalvariable.commands.CreateInternalVariableCommand;
import org.fordiac.ide.fbt.typeeditor.internalvariable.commands.DeleteInternalVariableCommand;
import org.fordiac.ide.fbt.typemanagement.FBTypeEditorInput;
import org.fordiac.ide.model.data.DataType;
import org.fordiac.ide.model.libraryElement.BasicFBType;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.libraryElement.provider.InternalVarsItemProvider;
import org.fordiac.ide.typelibrary.DataTypeLibrary;
import org.fordiac.ide.util.IdentifierVerifyListener;
import org.fordiac.ide.util.commands.ChangeCommentCommand;
import org.fordiac.ide.util.imageprovider.ImageProvider;

/**
 * The Class InternalVariablesEditor.
 */
public class InternalVariablesEditor extends EditorPart implements
		IFBTEditorPart {

	class InternalVarViewerComparator extends ViewerComparator {

		private int columnIndex;
		private boolean dirDescending;

		public InternalVarViewerComparator() {
			columnIndex = -1;
			dirDescending = false;
		}

		public int getDirection() {
			return (dirDescending) ? SWT.DOWN : SWT.UP;
		}

		public void setColumn(int column) {
			if (column == columnIndex) {
				dirDescending = !dirDescending;
			} else {
				columnIndex = column;
				dirDescending = false;
			}
		}

		@Override
		public int compare(org.eclipse.jface.viewers.Viewer viewer, Object e1,
				Object e2) {

			VarDeclaration var1 = (VarDeclaration)e1;
			VarDeclaration var2 = (VarDeclaration)e2;
			
			int rc = 0;
			switch (columnIndex) {
			case 0:
				// sort according to internal variable name
				rc = var1.getName().compareTo(var2.getName());
				break;
			case 1:
				// sort according to internal variable type
				rc = var1.getTypeName().compareTo(var2.getTypeName());
				break;
			default:
				rc = 0;
			}

			// If descending order, flip the direction
			if (dirDescending) {
				rc = -rc;
			}
			return rc;
		}

	}

	/** The type. */
	private BasicFBType type;

	/** The name text. */
	private Text nameText;

	/** The comment text. */
	private Text commentText;

	/** The initial value text. */
	private Text initialValueText;

	/** The array size text. */
	private Text arraySizeText;

	/** The selected variable. */
	private VarDeclaration selectedVariable;

	/** The type combo. */
	private Combo typeCombo;

	/** The internal var viewer. */
	private TableViewer internalVarViewer;

	private InternalVarViewerComparator viewerComparator;

	/** if true listeners in the bottom tabs will not be executed */
	private boolean blockListeners = false;

	
	/** The adapter. */
	private final EContentAdapter adapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {
			super.notifyChanged(notification);
			
			if(!blockListeners) {
				//final Object feature = notification.getFeature();
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						blockListeners = true;
						
						if((null != internalVarViewer) && (!internalVarViewer.getControl().isDisposed())){
							internalVarViewer.refresh();
							updateSelectedVariable(selectedVariable);
						}
						blockListeners = false;
					}
				});
			}
		}
	};

	/**
	 * Instantiates a new internal variables editor.
	 */
	public InternalVariablesEditor() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor
	 * )
	 */
	@Override
	public void doSave(final IProgressMonitor monitor) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.EditorPart#doSaveAs()
	 */
	@Override
	public void doSaveAs() {

	}
	
	@Override
	public void dispose() {
		if (type != null && type.eAdapters().contains(adapter)) {
			type.eAdapters().remove(adapter);
		}
		
		super.dispose();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.EditorPart#init(org.eclipse.ui.IEditorSite,
	 * org.eclipse.ui.IEditorInput)
	 */
	@Override
	public void init(final IEditorSite site, final IEditorInput input)
			throws PartInitException {
		setInput(input);
		if (input instanceof FBTypeEditorInput) {
			FBTypeEditorInput untypedInput = (FBTypeEditorInput) input;
			if (untypedInput.getContent() instanceof BasicFBType) {
				type = (BasicFBType) untypedInput.getContent();
				type.eAdapters().add(adapter);
			}
		}
		setSite(site);
		setPartName("Internal Variables");
		setTitleImage(ImageProvider.getImage("variables.gif"));

	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.EditorPart#isSaveAsAllowed()
	 */
	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	public void createPartControl(final Composite parent) {
		SashForm form = new SashForm(parent, SWT.HORIZONTAL);
		form.setLayout(new FillLayout());
		Composite variablesContainer = new Composite(form, SWT.BORDER);
		variablesContainer.setLayout(new GridLayout());

		Composite buttonContainer = new Composite(variablesContainer, SWT.NONE);
		buttonContainer.setLayout(new FillLayout());

		Button addButton = new Button(buttonContainer, SWT.NONE);
		addButton.setImage(ImageProvider.getImage("add_obj.gif"));
		addButton.setText("Add  ");
		addButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				
				CreateInternalVariableCommand cmd = new CreateInternalVariableCommand(type);
				getCommandStack().execute(cmd);
			}

			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
			}

		});

		Button deleteButton = new Button(buttonContainer, SWT.NONE);
		deleteButton.setImage(ImageProvider.getImage("delete_obj.gif"));

		deleteButton.setText("Delete");
		deleteButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				if (selectedVariable != null) {
					MessageBox deleteBox = new MessageBox(PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow().getShell(), SWT.YES | SWT.NO
							| SWT.ICON_QUESTION);
					deleteBox.setText("Delete Internal Variable");
					deleteBox.setMessage(MessageFormat.format("Delete \"{0}\" ?",
							new Object[] { selectedVariable.getName() }));
					int ret = deleteBox.open();
					if (ret == SWT.YES) {
						getCommandStack().execute(new DeleteInternalVariableCommand(type, selectedVariable));
						
					}
				} 
			}

			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
			}

		});

		setupInternalVariablesViewer(variablesContainer);

		Composite editingArea = new Composite(form, SWT.BORDER);
		editingArea.setLayout(new GridLayout(2, false));
		Label nameLabel = new Label(editingArea, SWT.NONE);
		nameLabel.setText("Name: ");

		nameText = new Text(editingArea, SWT.SINGLE | SWT.BORDER);
		nameText.addVerifyListener(new IdentifierVerifyListener());
		nameText.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				// update variable name
				if (selectedVariable != null) {
					if(!blockListeners){
						blockListeners = true;
						getCommandStack().execute(new ChangeNameCommand(selectedVariable, nameText.getText()));
						internalVarViewer.refresh();
						blockListeners = false;
					}
				}
			}
		});
		GridData textGridData = new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_BEGINNING);
		nameText.setLayoutData(textGridData);
		Label commentLabel = new Label(editingArea, SWT.NONE);
		commentLabel.setText("Comment: ");

		commentText = new Text(editingArea, SWT.SINGLE | SWT.BORDER);
		commentText.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				// update variable comment
				if (selectedVariable != null) {
					if (!blockListeners) {
						blockListeners = true;
						getCommandStack().execute(new ChangeCommentCommand(selectedVariable, commentText.getText()));
						internalVarViewer.refresh();
						blockListeners = false;
					}
				}
			}
		});
		GridData commentTextGridData = new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_BEGINNING);
		commentText.setLayoutData(commentTextGridData);

		Label typeLabel = new Label(editingArea, SWT.NONE);
		typeLabel.setText("Type: ");
		typeCombo = new Combo(editingArea, SWT.SINGLE | SWT.READ_ONLY | SWT.BORDER);
		GridData languageComboGridData = new GridData(
				GridData.VERTICAL_ALIGN_BEGINNING);
		typeCombo.setLayoutData(languageComboGridData);
		
		for (DataType dataType : DataTypeLibrary.getInstance().getDataTypesSorted()) {
			typeCombo.add(dataType.getName());
		}
		
		
		typeCombo.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				if (selectedVariable != null) {
					if(!blockListeners){
						blockListeners = true;
						getCommandStack().execute(new ChangeTypeCommand(selectedVariable, DataTypeLibrary.getInstance()
								.getType(typeCombo.getText())));
						internalVarViewer.refresh();
						blockListeners = false;
					}
				}
			}

			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
			}

		});

		Label arraySizeValueLabel = new Label(editingArea, SWT.NONE);
		arraySizeValueLabel.setText("ArraySize: ");

		arraySizeText = new Text(editingArea, SWT.SINGLE | SWT.BORDER);
		arraySizeText.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				if (selectedVariable != null) {
					if(!blockListeners){
						blockListeners = true;
						getCommandStack().execute(new ChangeArraySizeCommand(selectedVariable, arraySizeText.getText()));
						blockListeners = false;
					}					
				}
			}
		});
		GridData arraySizeTextGridData = new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_BEGINNING);
		arraySizeText.setLayoutData(arraySizeTextGridData);

		Label initialValueLabel = new Label(editingArea, SWT.NONE);
		initialValueLabel.setText("Initial Value: ");

		initialValueText = new Text(editingArea, SWT.SINGLE | SWT.BORDER);
		initialValueText.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				if (selectedVariable != null) {
					if(!blockListeners){
						blockListeners = true;
						getCommandStack().execute(new ChangeInitialValueCommand(selectedVariable, initialValueText.getText()));
						blockListeners = false;
					}
				}

			}
		});
		GridData initialValueTextGridData = new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_BEGINNING);
		initialValueText.setLayoutData(initialValueTextGridData);

		form.setWeights(new int[] { 25, 75 });

	}

	private void setupInternalVariablesViewer(Composite variablesContainer) {
		GridData internalVarViewerGridData = new GridData();
		internalVarViewerGridData.horizontalAlignment = GridData.FILL;
		internalVarViewerGridData.verticalAlignment = GridData.FILL;
		internalVarViewerGridData.grabExcessHorizontalSpace = true;
		internalVarViewerGridData.grabExcessVerticalSpace = true;

		internalVarViewer = new TableViewer(variablesContainer, SWT.SINGLE
				| SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION);

		viewerComparator = new InternalVarViewerComparator();
		internalVarViewer.setComparator(viewerComparator);

		TableColumn column = new TableColumn(internalVarViewer.getTable(),
				SWT.LEFT);
		column.setText("Variable"); //$NON-NLS-1$
		column.setWidth(80);
		column.addSelectionListener(getColumnSelectionAdapter(column, 0));

		column = new TableColumn(internalVarViewer.getTable(), SWT.LEFT);
		column.setText("Datatype"); //$NON-NLS-1$
		column.setWidth(80);
		column.addSelectionListener(getColumnSelectionAdapter(column, 1));

		column = new TableColumn(internalVarViewer.getTable(), SWT.LEFT);
		column.setText("Comment"); //$NON-NLS-1$
		column.setWidth(150);
		//don't set a selection listener here as we don't want to sort to the comment

		internalVarViewer.getTable().setLayoutData(internalVarViewerGridData);
		internalVarViewer.getTable().setHeaderVisible(true);

		internalVarViewer.setContentProvider(new InternalVarsContentProvider(
				type));

		internalVarViewer.setLabelProvider(new InternalVarsLabelProvider());

		internalVarViewer.setInput(new Object());
		internalVarViewer
				.addSelectionChangedListener(new ISelectionChangedListener() {
					public void selectionChanged(
							final SelectionChangedEvent event) {
						if (event.getSelection() instanceof StructuredSelection) {
							StructuredSelection selection = (StructuredSelection) event
									.getSelection();
							blockListeners = true;
							updateSelectedVariable((VarDeclaration) selection
									.getFirstElement());
							blockListeners = false;
						}
					}
				});
	}

	private SelectionAdapter getColumnSelectionAdapter(final TableColumn column,
			final int index) {
		SelectionAdapter selectionAdapter = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				viewerComparator.setColumn(index);
				int dir = viewerComparator.getDirection();
				internalVarViewer.getTable().setSortDirection(dir);
				internalVarViewer.getTable().setSortColumn(column);
				internalVarViewer.refresh();
			}
		};
		return selectionAdapter;
	}

	/**
	 * Update selected variable.
	 * 
	 * @param variable
	 *          the variable
	 */
	protected void updateSelectedVariable(final VarDeclaration variable) {
		
		if (variable != null) {
			selectedVariable = variable;
			nameText.setText(variable.getName() != null ? variable.getName() : "");
			commentText.setText(variable.getComment() != null ? variable.getComment()
					: "");
			initialValueText
					.setText(variable.getVarInitialization() != null
							&& variable.getVarInitialization().getInitialValue() != null ? variable
							.getVarInitialization().getInitialValue()
							: "");
			if (variable.isArray()) {
				arraySizeText.setText(Integer.toString(variable.getArraySize()));
			} else {
				arraySizeText.setText("");
			}

			for (int i = 0; i < typeCombo.getItems().length; i++) {
				String item = typeCombo.getItems()[i];
				if (item.equals(variable.getType().getName())) {
					typeCombo.select(i);
					break;
				}
			}
			nameText.setEnabled(true);
			commentText.setEnabled(true);
			initialValueText.setEnabled(true);
			arraySizeText.setEnabled(true);
			typeCombo.setEnabled(true);
			
		} else {
			selectedVariable = null;
			nameText.setText("");			
			commentText.setText("");
			initialValueText.setText("");
			arraySizeText.setText("");
			typeCombo.deselectAll();
			
			nameText.setEnabled(false);
			commentText.setEnabled(false);
			initialValueText.setEnabled(false);
			arraySizeText.setEnabled(false);
			typeCombo.setEnabled(false);
		}			
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.
	 * IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	@Override
	public void selectionChanged(final IWorkbenchPart part,
			final ISelection selection) {
	}

	@Override
	public boolean outlineSelectionChanged(Object selectedElement) {
		boolean bRetVal = false;
		blockListeners = true;
		if(selectedElement instanceof VarDeclaration){
			VarDeclaration var = (VarDeclaration) selectedElement;
			if(var.eContainer() instanceof BasicFBType){
				updateSelectedVariable((VarDeclaration) selectedElement);
				bRetVal = true;
			}
		}
		if(selectedElement instanceof InternalVarsItemProvider){
			updateSelectedVariable(null);
			bRetVal = true;
		}
		blockListeners = false;
		return bRetVal;
	}
	
	private org.eclipse.gef.commands.CommandStack commandStack;
	
	private org.eclipse.gef.commands.CommandStack getCommandStack() {
		return commandStack;
	}

	@Override
	public void setCommonCommandStack(
			org.eclipse.gef.commands.CommandStack commandStack) {
		this.commandStack = commandStack;
	}

	@Override
	public boolean isDirty() {
		return getCommandStack().isDirty();
	}

}
