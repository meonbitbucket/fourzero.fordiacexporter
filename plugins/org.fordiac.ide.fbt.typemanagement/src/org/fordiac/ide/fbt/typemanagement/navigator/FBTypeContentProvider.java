/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typemanagement.navigator;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.fordiac.ide.fbt.typemanagement.util.FBTypeUtils;
import org.fordiac.ide.model.Palette.Palette;
import org.fordiac.ide.model.Palette.PaletteEntry;
import org.fordiac.ide.model.libraryElement.AdapterType;
import org.fordiac.ide.typelibrary.TypeLibrary;

public class FBTypeContentProvider extends AdapterFactoryContentProvider  {

	public FBTypeContentProvider() {
		super(FBTypeComposedAdapterFactory.getAdapterFactory());
	}

	@Override
	public void dispose() {
		super.dispose();
		//TODO add resource monitoring
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
	}

	@Override
	public Object[] getElements(Object inputElement) {
		return getChildren(inputElement);
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof IFile)
		{
			IFile element = (IFile)parentElement;
			Palette palette = FBTypeUtils.getPalletteForFBTypeFile(element);
			if(palette != null){
				PaletteEntry entry = TypeLibrary.getPaletteEntry(palette, element);
				if(null != entry){
					parentElement = entry.getType();
					if(parentElement instanceof AdapterType){
						parentElement = ((AdapterType)parentElement).getAdapterFBType();
					}
				}
			}
		}
		return super.getChildren(parentElement);
	}

	@Override
	public Object getParent(Object element) {
		if (element instanceof IFile){
			return ((IResource)element).getParent();
		}
		return super.getParent(element);
	}

	@Override
	public boolean hasChildren(Object element) {
		if (element instanceof IFile){
			return true;
		}
		return super.hasChildren(element);
	}

}
