package org.fordiac.ide.fbt.typemanagement.navigator;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.PatternFilter;
import org.eclipse.ui.navigator.CommonNavigator;
import org.eclipse.ui.navigator.CommonViewer;
import org.eclipse.ui.views.properties.IPropertySheetPage;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertySheetPageContributor;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.fordiac.ide.model.libraryElement.provider.TransientBasicFBTypeListItemProvider;
import org.fordiac.ide.model.libraryElement.provider.TransientInterfaceListItemProvider;
import org.fordiac.ide.model.libraryElement.provider.TransientLibraryElementItemProvider;

public class TypeNavigator extends CommonNavigator implements ITabbedPropertySheetPageContributor {

	
	
	public void createPartControl(Composite aParent) {
		Composite container = new Composite(aParent, SWT.NONE);
		
		GridLayout layout = new GridLayout(1, false);
		layout.marginLeft = 0;
		layout.marginRight = 0;
		layout.marginBottom = 0;
		layout.marginTop = 0;
		container.setLayout(layout);
		
		
		final Text text = new Text(container, SWT.SEARCH | SWT.ICON_CANCEL);
		
		text.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		text.addSelectionListener(new SelectionAdapter() {
			public void widgetDefaultSelected(SelectionEvent e) {
				if (e.detail == SWT.CANCEL) {
					setSearchFilter("");
				} else {
					setSearchFilter(text.getText());
				}
			}
		});
		text.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				setSearchFilter(text.getText());
			}
		});
		
		super.createPartControl(container);
		
		GridData fillBoth = new GridData();
		fillBoth.horizontalAlignment = GridData.FILL;
		fillBoth.grabExcessHorizontalSpace = true;
		fillBoth.verticalAlignment = GridData.FILL;
		fillBoth.grabExcessVerticalSpace = true;
		getCommonViewer().getControl().setLayoutData(fillBoth);
		
	}
	
	PatternFilter patternFilter = null;
	
	void setSearchFilter(String filterString){
		CommonViewer cv = getCommonViewer();
		if (patternFilter == null)	{
			patternFilter = new PatternFilter(){
				protected boolean isParentMatch(Viewer viewer, Object element){
					//prevent children filtering if is a file, avoids type loading on filtering
					if((element instanceof IFolder) ||
							(element instanceof IProject)){
						return super.isParentMatch(viewer, element);
					}
					
					return false;
				}
				
				protected boolean isLeafMatch(Viewer viewer, Object element){
					if((element instanceof EObject) ||
					   (element instanceof TransientInterfaceListItemProvider) ||
					   (element instanceof TransientBasicFBTypeListItemProvider) ||
					   (element instanceof TransientLibraryElementItemProvider)){
						//do not filter on type content
						return true;
					}
					//TODO add also matching for other elements e.g., description ev. in is parent Match einbauen
					return super.isLeafMatch(viewer, element);
				}
			};
			patternFilter.setIncludeLeadingWildcard(true);
			patternFilter.setPattern(filterString);
			cv.addFilter(patternFilter);
		} else {
			patternFilter.setPattern(filterString);
			cv.refresh(false);
		}
	}

	@Override
	public String getContributorId() {
		return "property.contributor.fb";
	}

	@SuppressWarnings("rawtypes")
	public Object getAdapter(Class required) {
		if(required == IPropertySheetPage.class){
			return new TabbedPropertySheetPage(this);
		}
		return super.getAdapter(required);
	}
}
