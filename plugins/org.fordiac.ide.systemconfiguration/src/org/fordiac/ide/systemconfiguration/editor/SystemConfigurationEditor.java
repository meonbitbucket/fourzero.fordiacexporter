/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.editor;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.dnd.TemplateTransferDropTargetListener;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.gef.ui.actions.AlignmentAction;
import org.eclipse.gef.ui.palette.FlyoutPaletteComposite.FlyoutPreferences;
import org.eclipse.gef.ui.parts.ScrollingGraphicalViewer;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.util.TransferDropTargetListener;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.gmfextensions.AnimatedZoomScalableFreeformRootEditPart;
import org.fordiac.ide.gef.DiagramEditorWithFlyoutPalette;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.SystemConfiguration;
import org.fordiac.ide.model.ui.UISystemConfiguration;
import org.fordiac.ide.systemconfiguration.editparts.SystemConfEditPartFactory;
import org.fordiac.systemmanagement.ISystemEditor;
import org.fordiac.systemmanagement.SystemManager;

/**
 * The main editor for editing system configurations.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class SystemConfigurationEditor extends DiagramEditorWithFlyoutPalette implements ISystemEditor{

	/** The sys conf. */
	SystemConfiguration sysConf;

	/**
	 * Instantiates a new system configuration editor.
	 */
	public SystemConfigurationEditor() {
		// setEditDomain(new DefaultEditDomain(this));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.DiagramEditor#getEditPartFactory()
	 */
	@Override
	protected EditPartFactory getEditPartFactory() {
		return new SystemConfEditPartFactory(getZoomManger());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.DiagramEditor#getContextMenuProvider(org.eclipse.gef
	 * .ui.parts.ScrollingGraphicalViewer,
	 * org.fordiac.gmfextensions.AnimatedZoomScalableFreeformRootEditPart)
	 */
	@Override
	protected ContextMenuProvider getContextMenuProvider(
			final ScrollingGraphicalViewer viewer,
			final AnimatedZoomScalableFreeformRootEditPart root) {
		return new SystemConfigurationContextMenueProvider(viewer, root.getZoomManager(),
				getActionRegistry());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.DiagramEditor#createTransferDropTargetListener()
	 */
	@Override
	protected TransferDropTargetListener createTransferDropTargetListener() {
		return new TemplateTransferDropTargetListener(getViewer());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.DiagramEditor#setModel(org.eclipse.ui.IEditorInput)
	 */
	@Override
	protected void setModel(final IEditorInput input) {
		if (input instanceof org.fordiac.ide.util.PersistableUntypedEditorInput) {
			org.fordiac.ide.util.PersistableUntypedEditorInput untypedInput = (org.fordiac.ide.util.PersistableUntypedEditorInput) input;
			Object content = untypedInput.getContent();
			if (content instanceof SystemConfiguration) {
				sysConf = (SystemConfiguration) content;
				setDiagramModel((UISystemConfiguration) sysConf
						.getSystemConfigurationNetwork().eContainer());

				if (input.getName() != null) {
					setPartName(input.getName());
				}
			}
			super.setModel(untypedInput);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.DiagramEditor#getSystem()
	 */
	@Override
	public AutomationSystem getSystem() {
		return (AutomationSystem) sysConf.eContainer();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.DiagramEditor#getFileName()
	 */
	@Override
	public String getFileName() {
		return "SysConf.xml"; //$NON-NLS-1$
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.fordiac.ide.gef.DiagramEditor#doSave(org.eclipse.core.runtime.
	 * IProgressMonitor)
	 */
	@Override
	public void doSave(final IProgressMonitor monitor) {
		// TODO __gebenh error handling if save fails!
		SystemManager.getInstance().saveSystem(getSystem(), true);

		getCommandStack().markSaveLocation();
		firePropertyChange(IEditorPart.PROP_DIRTY);
	}

	@Override
	protected FlyoutPreferences getPalettePreferences() {
		return SystemConfPaletteFactory.createPalettePreferences();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.ui.parts.GraphicalEditorWithFlyoutPalette#getPaletteRoot()
	 */
	@Override
	protected PaletteRoot getPaletteRoot() {

		if (getDiagramModel() != null && getSystem() != null) {
			return SystemConfPaletteFactory.createPalette(getSystem());
		}
		return new PaletteRoot();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void createActions() {
		ActionRegistry registry = getActionRegistry();
		
		IAction action = new AlignmentAction((IWorkbenchPart) this,
				PositionConstants.LEFT);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());

		action = new AlignmentAction((IWorkbenchPart) this,
				PositionConstants.RIGHT);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());

		action = new AlignmentAction((IWorkbenchPart) this,
				PositionConstants.TOP);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());

		action = new AlignmentAction((IWorkbenchPart) this,
				PositionConstants.BOTTOM);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());

		action = new AlignmentAction((IWorkbenchPart) this,
				PositionConstants.CENTER);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());

		action = new AlignmentAction((IWorkbenchPart) this,
				PositionConstants.MIDDLE);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());
		
		super.createActions();
	}

}
