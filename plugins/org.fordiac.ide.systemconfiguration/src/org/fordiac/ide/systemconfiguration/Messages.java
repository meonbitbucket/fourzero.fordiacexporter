/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration;

import org.eclipse.osgi.util.NLS;

/**
 * The Class Messages.
 */
public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.fordiac.ide.systemconfiguration.messages"; //$NON-NLS-1$
	
	/** The Device create command_ labe l_ create device. */
	public static String DeviceCreateCommand_LABEL_CreateDevice;
	
	/** The Device edit part_ ico n_ close. */
	public static String DeviceEditPart_ICON_Close;
	
	/** The Device edit part_ ico n_ open. */
	public static String DeviceEditPart_ICON_Open;
	
	/** The Device edit part_ labe l_ not defined. */
	public static String DeviceEditPart_LABEL_NotDefined;
	
	/** The Resource container view edit part_ ico n_ close. */
	public static String ResourceContainerViewEditPart_ICON_Close;
	
	/** The Resource container view edit part_ ico n_ open. */
	public static String ResourceContainerViewEditPart_ICON_Open;
	
	/** The Resource edit part_ ico n_ locked resource_ firmware resource. */
	public static String ResourceEditPart_ICON_LockedResource_FirmwareResource;
	
	/** The System conf palette factory_ ico n_ device. */
	public static String SystemConfPaletteFactory_ICON_Device;
	
	/** The System conf palette factory_ ico n_ resource. */
	public static String SystemConfPaletteFactory_ICON_Resource;
	
	/** The System conf palette factory_ ico n_ segment. */
	public static String SystemConfPaletteFactory_ICON_Segment;
	
	/** The System conf palette factory_ ico n_ tools. */
	public static String SystemConfPaletteFactory_ICON_Tools;
	
	/** The System conf palette factory_ labe l_ tools. */
	public static String SystemConfPaletteFactory_LABEL_Tools;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
		// empty private constructor
	}
}
