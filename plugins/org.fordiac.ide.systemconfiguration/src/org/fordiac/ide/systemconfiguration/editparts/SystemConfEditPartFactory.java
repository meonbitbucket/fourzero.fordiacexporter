/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.editparts;

import java.text.MessageFormat;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.gef.ui.parts.GraphicalEditor;
import org.fordiac.ide.application.Messages;
import org.fordiac.ide.gef.IConnectionEditPartCreator;
import org.fordiac.ide.gef.editparts.ValueEditPart;
import org.fordiac.ide.model.libraryElement.Value;
import org.fordiac.ide.model.ui.DeviceView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.LinkView;
import org.fordiac.ide.model.ui.ResourceContainerView;
import org.fordiac.ide.model.ui.ResourceView;
import org.fordiac.ide.model.ui.SegmentView;
import org.fordiac.ide.model.ui.UISystemConfiguration;
import org.fordiac.systemmanagement.Activator;

/**
 * A factory for creating new EditParts.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class SystemConfEditPartFactory implements EditPartFactory {

	/**Editparts that have gradient fill require a zoom manger to correctly scale the pattern when zoomed*/
	protected ZoomManager zoomManager;
	private GraphicalEditor editor; 
	
	
	public SystemConfEditPartFactory(ZoomManager zoomManager) {
		super();
		this.zoomManager = zoomManager;
	}
	
	public GraphicalEditor getEditor() {
		return editor;
	}

	public void setEditor(GraphicalEditor editor) {
		this.editor = editor;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.EditPartFactory#createEditPart(org.eclipse.gef.EditPart,
	 * java.lang.Object)
	 */
	public EditPart createEditPart(final EditPart context,
			final Object modelElement) {
		// get EditPart for model element
		EditPart part = null;
		try {
			part = getPartForElement(context, modelElement);
			// store model element in EditPart
			part.setModel(modelElement);
		} catch (RuntimeException e) {
			Activator.getDefault().logError(e.getMessage(), e);
		}
		return part;
	}

	/**
	 * Maps an object to an EditPart.
	 * 
	 * @param context
	 *          the context
	 * @param modelElement
	 *          the model element
	 * 
	 * @return the part for element
	 * 
	 * @throws RuntimeException
	 *           if no match was found (programming error)
	 */
	protected EditPart getPartForElement(final EditPart context,
			final Object modelElement) {
		if (modelElement instanceof UISystemConfiguration) {
			return new SystemNetworkEditPart();
		}
		if (modelElement instanceof LinkView) {
			return new LinkEditPart();
		}
		if (modelElement instanceof DeviceView) {
			return new DeviceEditPart(zoomManager);
		}
		if (modelElement instanceof InterfaceElementView) {
			return new DeviceInterfaceEditPart();
		}
		if (modelElement instanceof Value) {
			ValueEditPart part = new ValueEditPart();
			part.setContext(context);
			return part;
		}
		if (modelElement instanceof SegmentView) {
			return new SegmentEditPart(zoomManager);
		}
		if (modelElement instanceof ResourceContainerView) {
			return new ResourceContainerViewEditPart();
		}
		if (modelElement instanceof ResourceView) {
			return new ResourceEditPart();
		}
		if (modelElement instanceof IConnectionEditPartCreator) {
			return ((IConnectionEditPartCreator) modelElement).createEditPart(Integer.toString(editor.hashCode()));
		}
		throw new RuntimeException(MessageFormat.format(
				Messages.ElementEditPartFactory_ERROR_CantCreatePartForModelElement,
				new Object[] { ((modelElement != null) ? modelElement.getClass()
						.getName() : "null") })); //$NON-NLS-1$

	}

}
