/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.commands;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gef.commands.Command;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.model.Palette.DeviceTypePaletteEntry;
import org.fordiac.ide.model.Palette.ResourceTypeEntry;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.libraryElement.Value;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.ui.Color;
import org.fordiac.ide.model.ui.DeviceView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.ResourceContainerView;
import org.fordiac.ide.model.ui.ResourceView;
import org.fordiac.ide.model.ui.UISystemConfiguration;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.ide.preferences.PreferenceConstants;
import org.fordiac.ide.systemconfiguration.Messages;
import org.fordiac.ide.util.Activator;
import org.fordiac.ide.util.ColorHelper;
import org.fordiac.ide.util.Utils;

/**
 * The Class DeviceCreateCommand.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class DeviceCreateCommand extends Command {

	/** The Constant CREATE_DEVICE_LABEL. */
	private static final String CREATE_DEVICE_LABEL = Messages.DeviceCreateCommand_LABEL_CreateDevice;

	/** The type. */
	protected final DeviceTypePaletteEntry entry;

	/** The parent. */
	protected final UISystemConfiguration parent;

	/** The bounds. */
	private final Rectangle bounds;

	/** The device. */
	protected Device device;

	/**
	 * Gets the device.
	 * 
	 * @return the device
	 */
	public Device getDevice() {
		return device;
	}

	/**
	 * Gets the device view.
	 * 
	 * @return the device view
	 */
	public DeviceView getDeviceView() {
		return deviceView;
	}

	/** The device view. */
	protected DeviceView deviceView;

	/** The editor. */
	private IEditorPart editor;

	private AutomationSystem system;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return editor.equals(Utils.getCurrentActiveEditor());

	}

	/**
	 * Instantiates a new device create command.
	 * 
	 * @param type
	 *            the type
	 * @param parent
	 *            the parent
	 * @param bounds
	 *            the bounds
	 */
	public DeviceCreateCommand(final DeviceTypePaletteEntry entry,
			final UISystemConfiguration parent, final Rectangle bounds) {
		this.entry = entry;
		this.parent = parent;
		this.bounds = bounds;
		setLabel(CREATE_DEVICE_LABEL);
	}

	/**
	 * Instantiates a new device create command.
	 * 
	 * @param type
	 *            the type
	 * @param parent
	 *            the parent
	 * @param bounds
	 *            the bounds
	 * @param system
	 *            the system
	 */
	public DeviceCreateCommand(final DeviceTypePaletteEntry entry,
			final UISystemConfiguration parent, final Rectangle bounds,
			AutomationSystem system) {
		this.entry = entry;
		this.parent = parent;
		this.bounds = bounds;
		this.system = system;
		setLabel(CREATE_DEVICE_LABEL);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		return entry != null && bounds != null && (parent != null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		editor = Utils.getCurrentActiveEditor();
		setLabel(getLabel()
				+ "(" + (editor != null ? editor.getTitle() : "") + ")"); //$NON-NLS-1$ //$NON-NLS-2$
		if (parent != null) {

			createDevice();
			device.setPaletteEntry(entry);
			device.setTypePath(entry.getProjectRelativeTypePath());

			device.getVarDeclarations()
					.addAll(EcoreUtil.copyAll(entry.getDeviceType()
							.getVarDeclaration()));

			device.setName(entry.getDeviceType().getName());
			device.setIdentifier(true);
			String profile = Activator
					.getDefault()
					.getPreferenceStore()
					.getString(PreferenceConstants.P_DEFAULT_COMPLIANCE_PROFILE);
			device.setProfile(profile);

			parent.getSystemConfigNetwork().getDevices().add(device);
			createValues();
			createDeviceView(device);

			createResourceViews();

			parent.getChildren().add(deviceView);
		}
	}

	protected void createDevice() {
		device = LibraryElementFactory.eINSTANCE.createDevice();
	}

	/**
	 * Creates the resource views.
	 */
	private void createResourceViews() {
		for (Iterator<Resource> iterator = entry.getDeviceType().getResource()
				.iterator(); iterator.hasNext();) {

			Resource res = iterator.next();
			ResourceCreateCommand cmd = null;
			if (res.getPaletteEntry() != null) {

				if (system != null) {
					cmd = new ResourceCreateCommand(
							(ResourceTypeEntry) res.getPaletteEntry(),
							deviceView.getResourceContainerView(), system);
				} else {
					cmd = new ResourceCreateCommand(
							(ResourceTypeEntry) res.getPaletteEntry(),
							deviceView.getResourceContainerView(), -1);

				}
				cmd.execute();

				Resource copy = cmd.getResource();
				copy.setDeviceTypeResource(true);
				copy.setName(res.getName());

				ResourceView resView = cmd.getResView();
				resView.setDeviceTypeResource(true);
			} else {
				org.fordiac.ide.systemconfiguration.Activator.getDefault().logInfo(
						"Referenced Resource Type: "
								+ (res.getName() != null ? res.getName() : "N/A") + (res.getType() != null ? " (" + res.getType() + ") " : "(N/A)")
								+ " not found. Please check whether your palette contains that type and add it manually to your device!");
			}
		}
	}

	/**
	 * Creates the values.
	 */
	protected void createValues() {
		ArrayList<IInterfaceElement> iInterfaceElements = new ArrayList<IInterfaceElement>();

		// iInterfaceElements.addAll(fB.getInterface().getEventInputs());

		iInterfaceElements.addAll(device.getVarDeclarations());

		for (Iterator<IInterfaceElement> iterator = iInterfaceElements
				.iterator(); iterator.hasNext();) {
			IInterfaceElement element = iterator.next();
			VarDeclaration varDecl = (VarDeclaration) element;
			Value value = LibraryElementFactory.eINSTANCE.createValue();
			element.setValue(value);
			if (varDecl.getVarInitialization() != null
					&& varDecl.getVarInitialization().getInitialValue() != null) {
				String initialValue = varDecl.getVarInitialization()
						.getInitialValue();
				value.setValue(initialValue);
			}

		}
	}

	/**
	 * Creates the device view.
	 * 
	 * @param device
	 *            the device
	 */
	private void createDeviceView(final Device device) {
		deviceView = UiFactory.eINSTANCE.createDeviceView();

		org.fordiac.ide.model.ui.Position pos = UiFactory.eINSTANCE
				.createPosition();
		pos.setX(bounds.x);
		pos.setY(bounds.y);
		deviceView.setPosition(pos);
		deviceView.setDeviceElement(device);

		Color color = UiFactory.eINSTANCE.createColor();
		RGB randomColor = ColorHelper.createRandomColor();
		color.setBlue(randomColor.blue);
		color.setRed(randomColor.red);
		color.setGreen(randomColor.green);
		deviceView.setBackgroundColor(color);

		ResourceContainerView resourceContainerView = UiFactory.eINSTANCE
				.createResourceContainerView();
		resourceContainerView.setShowContent(true);
		deviceView.setResourceContainerView(resourceContainerView);

		for (Iterator<VarDeclaration> iterator = device.getVarDeclarations()
				.iterator(); iterator.hasNext();) {
			VarDeclaration v = iterator.next();
			InterfaceElementView view = UiFactory.eINSTANCE
					.createInterfaceElementView();
			view.setIInterfaceElement(v);
			deviceView.getInterfaceElements().add(view);
		}

	}

	/**
	 * Redo.
	 * 
	 * @see DeviceCreateCommand#execute()
	 */
	@Override
	public void redo() {
		if (parent != null) {
			parent.getSystemConfigNetwork().getDevices().add(device);
			parent.getChildren().add(deviceView);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		if (parent != null) {
			parent.getSystemConfigNetwork().getDevices().remove(device);
			parent.getChildren().remove(deviceView);
		}
	}
}