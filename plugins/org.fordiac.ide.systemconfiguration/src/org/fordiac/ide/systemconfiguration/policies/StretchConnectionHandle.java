/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.policies;

import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.PlatformUI;
import org.fordiac.ide.gef.editparts.IConnectionHandleHelper;
import org.fordiac.ide.util.imageprovider.ImageProvider;

/**
 * The Class StretchConnectionHandle.
 */
public class StretchConnectionHandle extends
		org.fordiac.gmfextensions.ConnectionHandle {

	/**
	 * Instantiates a new stretch connection handle.
	 * 
	 * @param ownerEditPart the owner edit part
	 * @param relationshipDirection the relationship direction
	 * @param tooltip the tooltip
	 */
	public StretchConnectionHandle(final GraphicalEditPart ownerEditPart,
			final HandleDirection relationshipDirection, final String tooltip) {
		super(ownerEditPart, relationshipDirection, tooltip);
	}

	@Override
	protected Image getImage(final int side) {
		Image image = ImageProvider.getImage("segmentLocator.gif");
		if(null == scaled){
			if (getOwner() instanceof IConnectionHandleHelper) {
				scaled = new Image(PlatformUI.getWorkbench().getDisplay(), image
						.getImageData().scaledTo(
								((IConnectionHandleHelper) getOwner())
										.getConnectionHandleWidth(), 5));
			} else {
				scaled = new Image(PlatformUI.getWorkbench().getDisplay(), image
						.getImageData().scaledTo(getOwnerFigure().getSize().width,
								5));
			}
		}
		return scaled;
	}
	
	static Image scaled = null;
}
