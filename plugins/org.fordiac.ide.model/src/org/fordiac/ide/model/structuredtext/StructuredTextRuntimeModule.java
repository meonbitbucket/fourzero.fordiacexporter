/*
 * generated by Xtext
 */
package org.fordiac.ide.model.structuredtext;

/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
public class StructuredTextRuntimeModule extends org.fordiac.ide.model.structuredtext.AbstractStructuredTextRuntimeModule {

}
