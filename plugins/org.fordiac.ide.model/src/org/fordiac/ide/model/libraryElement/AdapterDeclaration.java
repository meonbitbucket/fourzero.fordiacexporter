/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement;

import org.fordiac.ide.model.ui.FBView;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Adapter Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.AdapterDeclaration#getAdapterBlock <em>Adapter Block</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getAdapterDeclaration()
 * @model
 * @generated
 */
public interface AdapterDeclaration extends VarDeclaration {

	/**
	 * Returns the value of the '<em><b>Adapter Block</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Adapter Block</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Adapter Block</em>' reference.
	 * @see #setAdapterBlock(FBView)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getAdapterDeclaration_AdapterBlock()
	 * @model transient="true"
	 * @generated
	 */
	FBView getAdapterBlock();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.AdapterDeclaration#getAdapterBlock <em>Adapter Block</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Adapter Block</em>' reference.
	 * @see #getAdapterBlock()
	 * @generated
	 */
	void setAdapterBlock(FBView value);

} // AdapterDeclaration
