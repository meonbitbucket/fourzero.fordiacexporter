/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Output Primitive</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.OutputPrimitive#getEvent <em>Event</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.OutputPrimitive#getParameters <em>Parameters</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.OutputPrimitive#getInterface <em>Interface</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.OutputPrimitive#getTestResult <em>Test Result</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getOutputPrimitive()
 * @model
 * @generated
 */
public interface OutputPrimitive extends EObject {
	/**
	 * Returns the value of the '<em><b>Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event</em>' attribute.
	 * @see #setEvent(String)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getOutputPrimitive_Event()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='Event'"
	 * @generated
	 */
	String getEvent();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.OutputPrimitive#getEvent <em>Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event</em>' attribute.
	 * @see #getEvent()
	 * @generated
	 */
	void setEvent(String value);

	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameters</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' attribute.
	 * @see #setParameters(String)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getOutputPrimitive_Parameters()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='Parameters'"
	 * @generated
	 */
	String getParameters();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.OutputPrimitive#getParameters <em>Parameters</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameters</em>' attribute.
	 * @see #getParameters()
	 * @generated
	 */
	void setParameters(String value);

	/**
	 * Returns the value of the '<em><b>Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interface</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interface</em>' reference.
	 * @see #setInterface(ServiceInterface)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getOutputPrimitive_Interface()
	 * @model required="true"
	 * @generated
	 */
	ServiceInterface getInterface();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.OutputPrimitive#getInterface <em>Interface</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interface</em>' reference.
	 * @see #getInterface()
	 * @generated
	 */
	void setInterface(ServiceInterface value);

	/**
	 * Returns the value of the '<em><b>Test Result</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test Result</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test Result</em>' attribute.
	 * @see #isSetTestResult()
	 * @see #unsetTestResult()
	 * @see #setTestResult(int)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getOutputPrimitive_TestResult()
	 * @model default="0" unique="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Int" volatile="true"
	 * @generated
	 */
	int getTestResult();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.OutputPrimitive#getTestResult <em>Test Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Test Result</em>' attribute.
	 * @see #isSetTestResult()
	 * @see #unsetTestResult()
	 * @see #getTestResult()
	 * @generated
	 */
	void setTestResult(int value);

	/**
	 * Unsets the value of the '{@link org.fordiac.ide.model.libraryElement.OutputPrimitive#getTestResult <em>Test Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTestResult()
	 * @see #getTestResult()
	 * @see #setTestResult(int)
	 * @generated
	 */
	void unsetTestResult();

	/**
	 * Returns whether the value of the '{@link org.fordiac.ide.model.libraryElement.OutputPrimitive#getTestResult <em>Test Result</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Test Result</em>' attribute is set.
	 * @see #unsetTestResult()
	 * @see #getTestResult()
	 * @see #setTestResult(int)
	 * @generated
	 */
	boolean isSetTestResult();

	public static final int NOT_TESTED = 0;
	public static final int TEST_OK = 1;
	public static final int TEST_FAIL = -1;
	
} // OutputPrimitive
