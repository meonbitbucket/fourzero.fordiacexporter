/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EContentAdapter;

import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.ui.FBView;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Adapter Declaration</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.AdapterDeclarationImpl#getAdapterBlock <em>Adapter Block</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AdapterDeclarationImpl extends VarDeclarationImpl implements
		AdapterDeclaration {
	/**
	 * The cached value of the '{@link #getAdapterBlock() <em>Adapter Block</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getAdapterBlock()
	 * @generated
	 * @ordered
	 */
	protected FBView adapterBlock;

	private boolean blockNotifiers = false;
	private EContentAdapter adapter = new EContentAdapter() {
		public void notifyChanged(Notification notification) {
			Object feature = notification.getFeature();
			if (!blockNotifiers) {
				blockNotifiers = true;
				if (LibraryElementPackage.eINSTANCE
						.getINamedElement_Name().equals(feature)) {
					setName(adapterBlock.getFb().getName());
				}
				blockNotifiers = false;
			}
			super.notifyChanged(notification);
		}
	};
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected AdapterDeclarationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LibraryElementPackage.Literals.ADAPTER_DECLARATION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public FBView getAdapterBlock() {
		if (adapterBlock != null && adapterBlock.eIsProxy()) {
			InternalEObject oldAdapterBlock = (InternalEObject)adapterBlock;
			adapterBlock = (FBView)eResolveProxy(oldAdapterBlock);
			if (adapterBlock != oldAdapterBlock) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, LibraryElementPackage.ADAPTER_DECLARATION__ADAPTER_BLOCK, oldAdapterBlock, adapterBlock));
			}
		}
		return adapterBlock;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public FBView basicGetAdapterBlock() {
		return adapterBlock;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	public void setAdapterBlock(FBView newAdapterBlock) {
		FBView oldAdapterBlock = adapterBlock;
		// remove adapter from oldAdapterBlock;
		if (oldAdapterBlock != null && oldAdapterBlock.getFb() != null) {
			oldAdapterBlock.getFb().eAdapters().remove(adapter);
		}
		adapterBlock = newAdapterBlock;
		// add adapter to new adapterblock (if not null)
		if (adapterBlock != null && adapterBlock.getFb() != null) {
			adapterBlock.getFb().eAdapters().add(adapter);
		}

		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					LibraryElementPackage.ADAPTER_DECLARATION__ADAPTER_BLOCK,
					oldAdapterBlock, adapterBlock));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LibraryElementPackage.ADAPTER_DECLARATION__ADAPTER_BLOCK:
				if (resolve) return getAdapterBlock();
				return basicGetAdapterBlock();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LibraryElementPackage.ADAPTER_DECLARATION__ADAPTER_BLOCK:
				setAdapterBlock((FBView)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LibraryElementPackage.ADAPTER_DECLARATION__ADAPTER_BLOCK:
				setAdapterBlock((FBView)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LibraryElementPackage.ADAPTER_DECLARATION__ADAPTER_BLOCK:
				return adapterBlock != null;
		}
		return super.eIsSet(featureID);
	}

	@Override
	public void setName(String newName) {
		super.setName(newName);
		if (adapterBlock != null) {
			if (!blockNotifiers) {
				blockNotifiers = true;
				adapterBlock.getFb().setName(newName);
				blockNotifiers = false;
			}
		}
	}

} // AdapterDeclarationImpl
