/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.libraryElement.OutputPrimitive;
import org.fordiac.ide.model.libraryElement.ServiceInterface;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Output Primitive</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.OutputPrimitiveImpl#getEvent <em>Event</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.OutputPrimitiveImpl#getParameters <em>Parameters</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.OutputPrimitiveImpl#getInterface <em>Interface</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.OutputPrimitiveImpl#getTestResult <em>Test Result</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class OutputPrimitiveImpl extends EObjectImpl implements OutputPrimitive {
	/**
	 * The default value of the '{@link #getEvent() <em>Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvent()
	 * @generated
	 * @ordered
	 */
	protected static final String EVENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEvent() <em>Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvent()
	 * @generated
	 * @ordered
	 */
	protected String event = EVENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getParameters() <em>Parameters</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameters()
	 * @generated
	 * @ordered
	 */
	protected static final String PARAMETERS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getParameters() <em>Parameters</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameters()
	 * @generated
	 * @ordered
	 */
	protected String parameters = PARAMETERS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInterface() <em>Interface</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterface()
	 * @generated
	 * @ordered
	 */
	protected ServiceInterface interface_;

	/**
	 * The default value of the '{@link #getTestResult() <em>Test Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestResult()
	 * @generated
	 * @ordered
	 */
	protected static final int TEST_RESULT_EDEFAULT = 0;

	public static final int NOT_TESTED = 0;
	public static final int TEST_OK = 1;
	public static final int TEST_FAIL = -1;
	

	private int testResult = TEST_RESULT_EDEFAULT;
	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OutputPrimitiveImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LibraryElementPackage.Literals.OUTPUT_PRIMITIVE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEvent() {
		return event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEvent(String newEvent) {
		String oldEvent = event;
		event = newEvent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.OUTPUT_PRIMITIVE__EVENT, oldEvent, event));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getParameters() {
		return parameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameters(String newParameters) {
		String oldParameters = parameters;
		parameters = newParameters;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.OUTPUT_PRIMITIVE__PARAMETERS, oldParameters, parameters));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceInterface getInterface() {
		if (interface_ != null && interface_.eIsProxy()) {
			InternalEObject oldInterface = (InternalEObject)interface_;
			interface_ = (ServiceInterface)eResolveProxy(oldInterface);
			if (interface_ != oldInterface) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, LibraryElementPackage.OUTPUT_PRIMITIVE__INTERFACE, oldInterface, interface_));
			}
		}
		return interface_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceInterface basicGetInterface() {
		return interface_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterface(ServiceInterface newInterface) {
		ServiceInterface oldInterface = interface_;
		interface_ = newInterface;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.OUTPUT_PRIMITIVE__INTERFACE, oldInterface, interface_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getTestResult() {
		return testResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setTestResult(int newTestResult) {
		int oldTestResult = testResult;
		testResult=newTestResult;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.OUTPUT_PRIMITIVE__TEST_RESULT, oldTestResult, testResult));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void unsetTestResult() {
		int oldTestResult = testResult;
		testResult=TEST_RESULT_EDEFAULT;
		if (eNotificationRequired())
		  eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.OUTPUT_PRIMITIVE__TEST_RESULT, oldTestResult, testResult));

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public boolean isSetTestResult() {
		return (testResult != TEST_RESULT_EDEFAULT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LibraryElementPackage.OUTPUT_PRIMITIVE__EVENT:
				return getEvent();
			case LibraryElementPackage.OUTPUT_PRIMITIVE__PARAMETERS:
				return getParameters();
			case LibraryElementPackage.OUTPUT_PRIMITIVE__INTERFACE:
				if (resolve) return getInterface();
				return basicGetInterface();
			case LibraryElementPackage.OUTPUT_PRIMITIVE__TEST_RESULT:
				return getTestResult();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LibraryElementPackage.OUTPUT_PRIMITIVE__EVENT:
				setEvent((String)newValue);
				return;
			case LibraryElementPackage.OUTPUT_PRIMITIVE__PARAMETERS:
				setParameters((String)newValue);
				return;
			case LibraryElementPackage.OUTPUT_PRIMITIVE__INTERFACE:
				setInterface((ServiceInterface)newValue);
				return;
			case LibraryElementPackage.OUTPUT_PRIMITIVE__TEST_RESULT:
				setTestResult((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LibraryElementPackage.OUTPUT_PRIMITIVE__EVENT:
				setEvent(EVENT_EDEFAULT);
				return;
			case LibraryElementPackage.OUTPUT_PRIMITIVE__PARAMETERS:
				setParameters(PARAMETERS_EDEFAULT);
				return;
			case LibraryElementPackage.OUTPUT_PRIMITIVE__INTERFACE:
				setInterface((ServiceInterface)null);
				return;
			case LibraryElementPackage.OUTPUT_PRIMITIVE__TEST_RESULT:
				unsetTestResult();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LibraryElementPackage.OUTPUT_PRIMITIVE__EVENT:
				return EVENT_EDEFAULT == null ? event != null : !EVENT_EDEFAULT.equals(event);
			case LibraryElementPackage.OUTPUT_PRIMITIVE__PARAMETERS:
				return PARAMETERS_EDEFAULT == null ? parameters != null : !PARAMETERS_EDEFAULT.equals(parameters);
			case LibraryElementPackage.OUTPUT_PRIMITIVE__INTERFACE:
				return interface_ != null;
			case LibraryElementPackage.OUTPUT_PRIMITIVE__TEST_RESULT:
				return isSetTestResult();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (event: ");
		result.append(event);
		result.append(", parameters: ");
		result.append(parameters);
		result.append(')');
		return result.toString();
	}

} //OutputPrimitiveImpl
