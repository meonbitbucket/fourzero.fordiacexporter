/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.data;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.data.ValueType#getInitialization <em>Initialization</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.data.DataPackage#getValueType()
 * @model
 * @generated
 */
public interface ValueType extends DataType {
	/**
	 * Returns the value of the '<em><b>Initialization</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initialization</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initialization</em>' containment reference.
	 * @see #setInitialization(VarInitialization)
	 * @see org.fordiac.ide.model.data.DataPackage#getValueType_Initialization()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	VarInitialization getInitialization();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.data.ValueType#getInitialization <em>Initialization</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initialization</em>' containment reference.
	 * @see #getInitialization()
	 * @generated
	 */
	void setInitialization(VarInitialization value);

} // ValueType
