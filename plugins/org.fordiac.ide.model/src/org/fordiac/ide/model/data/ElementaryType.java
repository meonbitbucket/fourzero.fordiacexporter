/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.data;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Elementary Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.fordiac.ide.model.data.DataPackage#getElementaryType()
 * @model
 * @generated
 */
public interface ElementaryType extends ValueType {
	
	public final static String[] TYPE_NAMES = { "BOOL", "SINT", "INT", "DINT",
		"LINT", "USINT", "UINT", "UDINT", "ULINT", "REAL", "LREAL", "TIME",
		"DATE", "TIME_OF_DAY", "DATE_AND_TIME", "STRING", "BYTE", "WORD",
		"DWORD", "LWORD", "WSTRING", "ANY", "ANY_NUM", "ANY_REAL",
		"ANY_INT", "ANY_BIT", "ANY_DATE", "ANY_STRING" };
	
} // ElementaryType
