/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.data;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ASN1 Tag</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.data.ASN1Tag#getClass_ <em>Class</em>}</li>
 *   <li>{@link org.fordiac.ide.model.data.ASN1Tag#getNumber <em>Number</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.data.DataPackage#getASN1Tag()
 * @model
 * @generated
 */
public interface ASN1Tag extends EObject {
	/**
	 * Returns the value of the '<em><b>Class</b></em>' attribute.
	 * The default value is <code>"CONTEXT"</code>.
	 * The literals are from the enumeration {@link org.fordiac.ide.model.data.ASN1Class}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class</em>' attribute.
	 * @see org.fordiac.ide.model.data.ASN1Class
	 * @see #isSetClass()
	 * @see #unsetClass()
	 * @see #setClass(ASN1Class)
	 * @see org.fordiac.ide.model.data.DataPackage#getASN1Tag_Class()
	 * @model default="CONTEXT" unsettable="true"
	 *        extendedMetaData="kind='attribute' name='Class'"
	 * @generated
	 */
	ASN1Class getClass_();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.data.ASN1Tag#getClass_ <em>Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class</em>' attribute.
	 * @see org.fordiac.ide.model.data.ASN1Class
	 * @see #isSetClass()
	 * @see #unsetClass()
	 * @see #getClass_()
	 * @generated
	 */
	void setClass(ASN1Class value);

	/**
	 * Unsets the value of the '{@link org.fordiac.ide.model.data.ASN1Tag#getClass_ <em>Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetClass()
	 * @see #getClass_()
	 * @see #setClass(ASN1Class)
	 * @generated
	 */
	void unsetClass();

	/**
	 * Returns whether the value of the '{@link org.fordiac.ide.model.data.ASN1Tag#getClass_ <em>Class</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Class</em>' attribute is set.
	 * @see #unsetClass()
	 * @see #getClass_()
	 * @see #setClass(ASN1Class)
	 * @generated
	 */
	boolean isSetClass();

	/**
	 * Returns the value of the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number</em>' attribute.
	 * @see #setNumber(String)
	 * @see org.fordiac.ide.model.data.DataPackage#getASN1Tag_Number()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='Number'"
	 * @generated
	 */
	String getNumber();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.data.ASN1Tag#getNumber <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number</em>' attribute.
	 * @see #getNumber()
	 * @generated
	 */
	void setNumber(String value);

} // ASN1Tag
