/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.data;

import org.fordiac.ide.model.libraryElement.CompilerInfo;
import org.fordiac.ide.model.libraryElement.LibraryElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.data.DataType#getCompilerInfo <em>Compiler Info</em>}</li>
 *   <li>{@link org.fordiac.ide.model.data.DataType#getASN1Tag <em>ASN1 Tag</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.data.DataPackage#getDataType()
 * @model abstract="true"
 * @generated
 */
public interface DataType extends LibraryElement {
	/**
	 * Returns the value of the '<em><b>Compiler Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Compiler Info</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Compiler Info</em>' containment reference.
	 * @see #setCompilerInfo(CompilerInfo)
	 * @see org.fordiac.ide.model.data.DataPackage#getDataType_CompilerInfo()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='CompilerInfo' namespace='http://www.fordiac.org/61499/lib'"
	 * @generated
	 */
	CompilerInfo getCompilerInfo();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.data.DataType#getCompilerInfo <em>Compiler Info</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Compiler Info</em>' containment reference.
	 * @see #getCompilerInfo()
	 * @generated
	 */
	void setCompilerInfo(CompilerInfo value);

	/**
	 * Returns the value of the '<em><b>ASN1 Tag</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ASN1 Tag</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ASN1 Tag</em>' containment reference.
	 * @see #setASN1Tag(ASN1Tag)
	 * @see org.fordiac.ide.model.data.DataPackage#getDataType_ASN1Tag()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ASN1Tag' namespace='##targetNamespace'"
	 * @generated
	 */
	ASN1Tag getASN1Tag();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.data.DataType#getASN1Tag <em>ASN1 Tag</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ASN1 Tag</em>' containment reference.
	 * @see #getASN1Tag()
	 * @generated
	 */
	void setASN1Tag(ASN1Tag value);

} // DataType
