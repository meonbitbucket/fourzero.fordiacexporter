/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;

import org.eclipse.emf.ecore.EObject;
import org.fordiac.ide.model.data.DataType;
import org.fordiac.ide.model.libraryElement.Algorithm;
import org.fordiac.ide.model.libraryElement.Annotation;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.BasicFBType;
import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.ECC;
import org.fordiac.ide.model.libraryElement.ECState;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.libraryElement.ResourceFBNetwork;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.typelibrary.DataTypeLibrary;

/**
 * @author gebenh
 * 
 */
public class NameRepository {

	private final Hashtable<AutomationSystem, Hashtable<String, String>> fbUniqueSystemInstanceNames = new Hashtable<AutomationSystem, Hashtable<String, String>>();

	private final Hashtable<Device, HashSet<String>> devices = new Hashtable<Device, HashSet<String>>();

	private final HashSet<String> deviceNames = new HashSet<String>();
	private static NameRepository instance;

	private NameRepository() {
		// empty private constructor
	}

	public static NameRepository getInstance() {
		if (instance == null) {
			instance = new NameRepository();
		}
		return instance;
	}
	
	
	public static boolean isValidIdentifier(String identifierName){
		return identifierName.matches("[a-zA-Z_]{1}[\\w]*");
	}
	
	public static void checkNameIdentifier(INamedElement element){
		element.getAnnotations().clear();
		if(!isValidIdentifier(element.getName())){
			Annotation ano = element.createAnnotation("Name: " + element.getName() + " is not a valid identifier!");
			ano.setServity(2); // 2 means error!
		}
	}

	public void removeInstanceName(FB fb) {
		if (fb != null) {
			EObject object = fb.eContainer();
			if (object instanceof FBNetwork) {
				Application app = ((FBNetwork) object).getApplication();
				if (app != null
						&& app.eContainer() instanceof AutomationSystem) {
					Hashtable<String, String> uniqueNames = fbUniqueSystemInstanceNames
							.get(app.eContainer());
					uniqueNames.remove(fb.getName());
				}
			}
		}
	}

	public String removeSystemUniqueInstanceName(
			AutomationSystem automationSystem, final String name) {
		if (automationSystem != null) {
			Hashtable<String, String> uniqueNames = fbUniqueSystemInstanceNames
					.get(automationSystem);
			if (uniqueNames == null) {
				uniqueNames = new Hashtable<String, String>();
				fbUniqueSystemInstanceNames.put(automationSystem, uniqueNames);
			}
			if (name != null) {
				return uniqueNames.remove(name);
			}
		}
		return null;
	}

	public void clearUniqueFBInstanceNames(AutomationSystem system) {
		if (system != null) {
			Hashtable<String, String> uniqueNames = fbUniqueSystemInstanceNames
					.get(system);
			if (uniqueNames != null) {
				uniqueNames.clear();
			}
		}

	}
	
	
	public boolean addSystemUniqueFBInstanceName(FB fb) {
		if (fb != null) {
			EObject object = fb.eContainer();
			if (object instanceof FBNetwork) {
				Application app = ((FBNetwork) object).getApplication();
				if (app != null
						&& app.eContainer() instanceof AutomationSystem) {
					return addSystemUniqueFBInstanceName((AutomationSystem)app.eContainer(), fb.getName(), fb.getId());
				}
			}
		}
		return false;
	}

	public boolean addSystemUniqueFBInstanceName(
			final AutomationSystem automationSystem, final String name,
			final String id) {
		if (automationSystem != null) {
			Hashtable<String, String> uniqueNames = fbUniqueSystemInstanceNames
					.get(automationSystem);
			if (uniqueNames == null) {
				uniqueNames = new Hashtable<String, String>();
				fbUniqueSystemInstanceNames.put(automationSystem, uniqueNames);
			}
			if (uniqueNames.containsKey(name)) {
				return uniqueNames.get(name).equals(id);
			} else {
				uniqueNames.put(name, id);
				return true;
			}
		}
		return false;
	}

	public boolean isSystemUniqueFBInstanceName(AutomationSystem system,
			String name, String id) {

		Hashtable<String, String> uniqueNames = fbUniqueSystemInstanceNames
				.get(system);
		if (uniqueNames.containsKey(name)) {
			if (uniqueNames.get(name).equals(id)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Checks whether the name is unique
	 * 
	 * @param fb
	 * @return
	 */
	public boolean isSystemUniqueFBInstanceName(FB fb) {
		if (fb != null) {
			EObject object = fb.eContainer();
			if (!(object instanceof FBNetwork) && object instanceof SubAppNetwork) {
				//get the Parent FBNetwork
				FBNetwork parentNet = getRootFBNetwork(((SubAppNetwork) fb.eContainer()).getParentSubApp());
				object = parentNet;
			}
			if (object instanceof FBNetwork) {
				Application app = ((FBNetwork) object).getApplication();
				AutomationSystem sys = null;
				if (app != null && app.eContainer() instanceof AutomationSystem) {
					sys = (AutomationSystem)app.eContainer();
				}
				else if(object instanceof ResourceFBNetwork){
					sys = ((Resource)((ResourceFBNetwork)object).eContainer()).getAutomationSystem();
				}
				
				if(null != sys){
					Hashtable<String, String> uniqueNames = fbUniqueSystemInstanceNames
							.get(sys);
					if (uniqueNames.containsKey(fb.getName())) {
						if (uniqueNames.get(fb.getName()).equals(fb.getId())) {
							return true;
						}
					} else {
						uniqueNames.put(fb.getName(), fb.getId());
						return true;
					}
				} else if (fb.getParentCompositeFBType() != null) {// fb inside
																	// a
																	// composite,
					Hashtable<String, String> uniqueNames = new Hashtable<String, String>();
					for (FB fbx : fb.getParentCompositeFBType().getFBNetwork()
							.getFBs()) {
						uniqueNames.put(fbx.getName(), fbx.getId());
					}
					String id = fb.getId();
					if (uniqueNames.containsKey(fb.getName())) {
						if (uniqueNames.get(fb.getName()).equals(id)) {
							return true;
						} else {
							return false;
						}
					} else {
						return true;
					}
				}
			}
		}
		return false;
	}

	public FBNetwork getRootFBNetwork(EObject childElement){
		
		FBNetwork rootNet = null;
		EObject currentParent;
		
		if(childElement != null && childElement instanceof SubApp){
			currentParent = ((SubApp) childElement).eContainer();
			rootNet = getRootFBNetwork(currentParent);
		}
		else if(childElement != null && childElement instanceof FBNetwork){
			rootNet = (FBNetwork)childElement;
		}
		else if(childElement != null && childElement instanceof SubAppNetwork){
			currentParent = ((SubAppNetwork) childElement).getParentSubApp();
			currentParent = ((SubApp) currentParent).eContainer();
			rootNet = getRootFBNetwork(currentParent);
		}
		
		return rootNet;
	}
	
	public String getNetworkUniqueSubApplicationName(final SubAppNetwork theParentSubnetwork, final String name){
		if (theParentSubnetwork != null) {
			HashSet<String> uniqueNames = new HashSet<String>();
	
			for (FB fb : theParentSubnetwork.getFBs()) {
				uniqueNames.add(fb.getName());
			}
			
			for(SubApp currentSubApp : theParentSubnetwork.getSubApps()){
				uniqueNames.add(currentSubApp.getName());
			}
	
			return getUniqueName(uniqueNames, name);
		}
		return name;
	}
	
	
	/** Generating a unique name for a name proposal which is definitely not in the list of given existing names
	 * 
	 * If the proposed name is already found in the list an '_' and a consecutive number is appended to the proposed name. 
	 * The number incremented until a unique name is found.  
	 * 
	 * @param existingNameList the list of names already existing in the context 
	 * @param nameProposal a proposal for a name as starting point
	 * @return a unique name
	 */
	private String getUniqueName(HashSet<String> existingNameList, String nameProposal){
		String temp = nameProposal;
		
		int i = 1;
		while (existingNameList.contains(temp)) {
			temp = nameProposal + "_" + i;
			i++;
		}
		return temp;
	}
	
	/**
	 * Returns a unique Instance name
	 * 
	 * @param automationSystem
	 * @param name
	 * @param id
	 * @return
	 */
	public String getSystemUniqueFBInstanceName(
			final AutomationSystem automationSystem, final String name,
			final String id) {
		if (automationSystem != null) {
			Hashtable<String, String> uniqueNames = fbUniqueSystemInstanceNames
					.get(automationSystem);
			if (uniqueNames == null) {
				uniqueNames = new Hashtable<String, String>();
				fbUniqueSystemInstanceNames.put(automationSystem, uniqueNames);
			}
			String temp = name;
			int i = 0;
			if (uniqueNames.containsKey(name)
					&& uniqueNames.get(name).equals(id)) {
				return name;
			}
			while (uniqueNames.containsKey(temp)
					&& !uniqueNames.get(temp).equals(id)) {
				temp = name + "_" + i;
				i++;
			}
			uniqueNames.put(temp, id);
			return temp;
		}
		return name;
	}

	/**
	 * Returns a unique Instance name for a composite FB
	 * 
	 * @param compositeParent
	 * @param newFB
	 * @return
	 */
	public String getCompositeUniqueFBInstanceName(
			CompositeFBType compositeParent, String nameProposal) {
		if (compositeParent != null) {
			HashSet<String> uniqueNames = new HashSet<String>();

			for (FB fb : compositeParent.getFBNetwork().getFBs()) {
				uniqueNames.add(fb.getName());
			}

			return getUniqueName(uniqueNames, nameProposal);
		}
		return nameProposal;
	}

	public boolean removeResourceInstanceName(final Device device,
			final String name) {
		if (device != null && name != null) {
			HashSet<String> names = devices.get(device);
			if (names != null) {
				return names.remove(name);
			}
		}
		return false;
	}

	public String getUniqueResourceInstanceName(final Device device,
			final String name) {
		if (device != null) {
			HashSet<String> resourceNames = devices.get(device);
			if (resourceNames == null) {
				resourceNames = new HashSet<String>();
				devices.put(device, resourceNames);
			}
			String uniqueName = getUniqueName(resourceNames, name);
			resourceNames.add(uniqueName);
			return uniqueName;
		}
		return name;
	}

	public boolean removeDeviceInstanceName(final String name) {
		return deviceNames.remove(name);
	}

	public String getUniqueDeviceInstanceName(final String name) {
		String temp = getUniqueName(deviceNames, name);
		deviceNames.add(temp);
		return temp;
	}

	public String getUniqueECCStateName(ECState state, ECC ecc, String name) {
		String temp = name;
		int i = 0;
		if (!isUniqueStateName(state, ecc, temp)) {
			while (!isUniqueStateName(state, ecc, temp)) {
				temp = name;
				temp += "_";
				temp += i;
				i++;
			}
		}
		return temp;
	}

	private boolean isUniqueStateName(ECState ecState, ECC ecc, String name) {
		for (ECState state : ecc.getECState()) {
			if (state.getName().equalsIgnoreCase(name)) {
				if (!ecState.equals(state)) {
					return false;
				}
			}
		}
		return true;
	}

	public static String getUniqueInterfaceElementName(IInterfaceElement iElement, FBType fbType, String name) {
		ArrayList<INamedElement> elements = new ArrayList<INamedElement>();
		elements.addAll(fbType.getInterfaceList().getEventInputs());
		elements.addAll(fbType.getInterfaceList().getEventOutputs());
		elements.addAll(fbType.getInterfaceList().getInputVars());
		elements.addAll(fbType.getInterfaceList().getOutputVars());
		elements.addAll(fbType.getInterfaceList().getPlugs());
		elements.addAll(fbType.getInterfaceList().getSockets());
		if(fbType instanceof BasicFBType){
			elements.addAll(((BasicFBType)fbType).getInternalVars());
		}
		String retVal = checkReservedKeyWords(name);
		int i = 1;
		while (!isUnique(iElement, retVal, elements))  {
			retVal = name + i;
			i++;
		}
		return retVal;
	}

	public static String getUniqueAlgorithmName(Algorithm algorithm, BasicFBType fbType, String name){
		ArrayList<INamedElement> elements = new ArrayList<INamedElement>();
		elements.addAll(fbType.getAlgorithm());
		
		String retVal = name;
		int i = 1;
		while (!isUnique(algorithm, retVal, elements))  {
			retVal = name + i;
			i++;
		}
		return retVal;
	}

	private static boolean isUnique(INamedElement iElement, String name, ArrayList<INamedElement> elements) {
		for (INamedElement element : elements) {
			 if (!element.equals(iElement) && element.getName()
					.toUpperCase().equals(name.toUpperCase())) {
				 return false;
			 }
		}
		return true;
	}
	
	private static String checkReservedKeyWords(String name) {
		//Currently we check only for data type names
		for (DataType dataType : DataTypeLibrary.getInstance().getDataTypesSorted()) {
			if(dataType.getName().equals(name)){
				return name + "1";
			}
		}
		//TODO maybe it makes sense to check also for other reserved key words
		return name;
	}
}
