/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.typeexport.tests;

import junit.framework.TestCase;

import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.typeexport.FbtExporter;
import org.fordiac.ide.typeimport.FBTImporter;
import org.junit.Test;
import org.w3c.dom.Document;

public class TestFBTExporter extends TestCase {


	
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}
	@Test
	public void testGetDocumentBasicTypeContainsInterface() {
		FBType temp = LibraryElementFactory.eINSTANCE.createBasicFBType();
		createInterfaceList(temp);
		
		Document dom = new FbtExporter().getDocument(temp);
		org.w3c.dom.NodeList nodeList = dom.getElementsByTagName(FBTImporter.INTERFACE_LIST_ELEMENT);
		assertEquals("Must have exactly one InterfaceList!", 1, nodeList.getLength());
		org.w3c.dom.NodeList eventList = dom.getElementsByTagName(FBTImporter.EVENT_ELEMENT);
		assertEquals("Must have 2 Events", 2, eventList.getLength());
	}
	
	@Test
	public void testGetDocumentServiceInterfaceTypeContainsInterface() {
		FBType temp = LibraryElementFactory.eINSTANCE.createServiceInterfaceFBType();
		createInterfaceList(temp);

		Document dom = new FbtExporter().getDocument(temp);
		org.w3c.dom.NodeList nodeList = dom.getElementsByTagName(FBTImporter.INTERFACE_LIST_ELEMENT);
		assertEquals("Must have exactly one InterfaceList!", 1, nodeList.getLength());
		org.w3c.dom.NodeList eventList = dom.getElementsByTagName(FBTImporter.EVENT_ELEMENT);
		assertEquals("Must have 2 Events", 2, eventList.getLength());
	}
	@Test
	public void testGetDocumentCompositeTypeContainsInterface() {
		CompositeFBType temp = createCompositeFBType();
		
		Document dom = new FbtExporter().getDocument(temp);
		org.w3c.dom.NodeList nodeList = dom.getElementsByTagName(FBTImporter.INTERFACE_LIST_ELEMENT);
		assertEquals("Must have exactly one InterfaceList!", 1, nodeList.getLength());
		org.w3c.dom.NodeList eventList = dom.getElementsByTagName(FBTImporter.EVENT_ELEMENT);
		assertEquals("Must have 2 Events", 2, eventList.getLength());
	}
	
	private CompositeFBType createCompositeFBType() {
		CompositeFBType temp = LibraryElementFactory.eINSTANCE.createCompositeFBType();
		createInterfaceList(temp);
		FBNetwork network = LibraryElementFactory.eINSTANCE.createFBNetwork();
		temp.setFBNetwork(network);
		return temp;
	}

	private void createInterfaceList(FBType temp) {
		InterfaceList interfaceList = LibraryElementFactory.eINSTANCE.createInterfaceList();
		Event inputEvent = LibraryElementFactory.eINSTANCE.createEvent();
		inputEvent.setName("INE1");
		inputEvent.setIdentifier(true);
		interfaceList.getEventInputs().add(inputEvent);
		Event outputEvent = LibraryElementFactory.eINSTANCE.createEvent();
		outputEvent.setName("OUTE1");
		outputEvent.setIdentifier(false);
		interfaceList.getEventOutputs().add(outputEvent);
		temp.setInterfaceList(interfaceList);
	}

}