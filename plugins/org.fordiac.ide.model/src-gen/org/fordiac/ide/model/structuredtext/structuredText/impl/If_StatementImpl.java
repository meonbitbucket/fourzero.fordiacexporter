/**
 */
package org.fordiac.ide.model.structuredtext.structuredText.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.fordiac.ide.model.structuredtext.structuredText.ElseIf_Statement;
import org.fordiac.ide.model.structuredtext.structuredText.If_Statement;
import org.fordiac.ide.model.structuredtext.structuredText.StatementList;
import org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>If Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.impl.If_StatementImpl#getStatments <em>Statments</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.impl.If_StatementImpl#getElseif <em>Elseif</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.impl.If_StatementImpl#getElseStatements <em>Else Statements</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class If_StatementImpl extends Selection_StatementImpl implements If_Statement
{
  /**
   * The cached value of the '{@link #getStatments() <em>Statments</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStatments()
   * @generated
   * @ordered
   */
  protected StatementList statments;

  /**
   * The cached value of the '{@link #getElseif() <em>Elseif</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getElseif()
   * @generated
   * @ordered
   */
  protected EList<ElseIf_Statement> elseif;

  /**
   * The cached value of the '{@link #getElseStatements() <em>Else Statements</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getElseStatements()
   * @generated
   * @ordered
   */
  protected StatementList elseStatements;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected If_StatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return StructuredTextPackage.Literals.IF_STATEMENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StatementList getStatments()
  {
    return statments;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetStatments(StatementList newStatments, NotificationChain msgs)
  {
    StatementList oldStatments = statments;
    statments = newStatments;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StructuredTextPackage.IF_STATEMENT__STATMENTS, oldStatments, newStatments);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setStatments(StatementList newStatments)
  {
    if (newStatments != statments)
    {
      NotificationChain msgs = null;
      if (statments != null)
        msgs = ((InternalEObject)statments).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StructuredTextPackage.IF_STATEMENT__STATMENTS, null, msgs);
      if (newStatments != null)
        msgs = ((InternalEObject)newStatments).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StructuredTextPackage.IF_STATEMENT__STATMENTS, null, msgs);
      msgs = basicSetStatments(newStatments, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, StructuredTextPackage.IF_STATEMENT__STATMENTS, newStatments, newStatments));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ElseIf_Statement> getElseif()
  {
    if (elseif == null)
    {
      elseif = new EObjectContainmentEList<ElseIf_Statement>(ElseIf_Statement.class, this, StructuredTextPackage.IF_STATEMENT__ELSEIF);
    }
    return elseif;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StatementList getElseStatements()
  {
    return elseStatements;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetElseStatements(StatementList newElseStatements, NotificationChain msgs)
  {
    StatementList oldElseStatements = elseStatements;
    elseStatements = newElseStatements;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StructuredTextPackage.IF_STATEMENT__ELSE_STATEMENTS, oldElseStatements, newElseStatements);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setElseStatements(StatementList newElseStatements)
  {
    if (newElseStatements != elseStatements)
    {
      NotificationChain msgs = null;
      if (elseStatements != null)
        msgs = ((InternalEObject)elseStatements).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StructuredTextPackage.IF_STATEMENT__ELSE_STATEMENTS, null, msgs);
      if (newElseStatements != null)
        msgs = ((InternalEObject)newElseStatements).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StructuredTextPackage.IF_STATEMENT__ELSE_STATEMENTS, null, msgs);
      msgs = basicSetElseStatements(newElseStatements, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, StructuredTextPackage.IF_STATEMENT__ELSE_STATEMENTS, newElseStatements, newElseStatements));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case StructuredTextPackage.IF_STATEMENT__STATMENTS:
        return basicSetStatments(null, msgs);
      case StructuredTextPackage.IF_STATEMENT__ELSEIF:
        return ((InternalEList<?>)getElseif()).basicRemove(otherEnd, msgs);
      case StructuredTextPackage.IF_STATEMENT__ELSE_STATEMENTS:
        return basicSetElseStatements(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case StructuredTextPackage.IF_STATEMENT__STATMENTS:
        return getStatments();
      case StructuredTextPackage.IF_STATEMENT__ELSEIF:
        return getElseif();
      case StructuredTextPackage.IF_STATEMENT__ELSE_STATEMENTS:
        return getElseStatements();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case StructuredTextPackage.IF_STATEMENT__STATMENTS:
        setStatments((StatementList)newValue);
        return;
      case StructuredTextPackage.IF_STATEMENT__ELSEIF:
        getElseif().clear();
        getElseif().addAll((Collection<? extends ElseIf_Statement>)newValue);
        return;
      case StructuredTextPackage.IF_STATEMENT__ELSE_STATEMENTS:
        setElseStatements((StatementList)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case StructuredTextPackage.IF_STATEMENT__STATMENTS:
        setStatments((StatementList)null);
        return;
      case StructuredTextPackage.IF_STATEMENT__ELSEIF:
        getElseif().clear();
        return;
      case StructuredTextPackage.IF_STATEMENT__ELSE_STATEMENTS:
        setElseStatements((StatementList)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case StructuredTextPackage.IF_STATEMENT__STATMENTS:
        return statments != null;
      case StructuredTextPackage.IF_STATEMENT__ELSEIF:
        return elseif != null && !elseif.isEmpty();
      case StructuredTextPackage.IF_STATEMENT__ELSE_STATEMENTS:
        return elseStatements != null;
    }
    return super.eIsSet(featureID);
  }

} //If_StatementImpl
