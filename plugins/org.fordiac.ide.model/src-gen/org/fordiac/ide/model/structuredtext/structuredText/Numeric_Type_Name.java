/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Numeric Type Name</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getNumeric_Type_Name()
 * @model
 * @generated
 */
public interface Numeric_Type_Name extends Elementary_Type_Name
{
} // Numeric_Type_Name
