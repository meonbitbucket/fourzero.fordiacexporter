/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Adapter Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.Adapter_Variable#getAdapter <em>Adapter</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getAdapter_Variable()
 * @model
 * @generated
 */
public interface Adapter_Variable extends Variable
{
  /**
   * Returns the value of the '<em><b>Adapter</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Adapter</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Adapter</em>' reference.
   * @see #setAdapter(AdapterDeclaration)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getAdapter_Variable_Adapter()
   * @model
   * @generated
   */
  AdapterDeclaration getAdapter();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.Adapter_Variable#getAdapter <em>Adapter</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Adapter</em>' reference.
   * @see #getAdapter()
   * @generated
   */
  void setAdapter(AdapterDeclaration value);

} // Adapter_Variable
