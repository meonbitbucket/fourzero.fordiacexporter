package org.fordiac.ide.model.structuredtext.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.fordiac.ide.model.structuredtext.services.StructuredTextGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalStructuredTextParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_BINARY_INTEGER_VALUE", "RULE_OCTAL_INTEGER_VALUE", "RULE_HEX_INTEGER_VALUE", "RULE_REALVALUE", "RULE_S_BYTE_CHAR_STRING", "RULE_D_BYTE_CHAR_STRING", "RULE_TIME", "RULE_ML_COMMENT", "RULE_DIGIT", "RULE_HEX_DIGIT", "RULE_COMMON_CHAR_VALUE", "RULE_STRING", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'VAR_INPUT'", "'END_VAR'", "'VAR_OUTPUT'", "'VAR_INTERNAL'", "'PLUGS'", "'END_PLUGS'", "'SOCKETS'", "'END_SOCKETS'", "'VAR'", "':'", "';'", "'ADAPTER'", "'END_ADAPTER'", "'('", "','", "')'", "':='", "'ARRAY'", "'['", "']'", "'OF'", "'..'", "'STRING'", "'WSTRING'", "'.'", "'IF'", "'THEN'", "'ELSE'", "'END_IF'", "'ELSIF'", "'CASE'", "'END_CASE'", "'EXIT'", "'CONTINUE'", "'RETURN'", "'FOR'", "'TO'", "'BY'", "'DO'", "'END_FOR'", "'REPEAT'", "'UNTIL'", "'END_REPEAT'", "'WHILE'", "'END_WHILE'", "'OR'", "'XOR'", "'&'", "'AND'", "'='", "'<>'", "'<'", "'<='", "'>'", "'>='", "'+'", "'-'", "'*'", "'/'", "'MOD'", "'**'", "'NOT'", "'=>'", "'UINT#'", "'USINT#'", "'UDINT#'", "'ULINT#'", "'BYTE#'", "'WORD#'", "'DWORD#'", "'LWORD#'", "'INT#'", "'SINT#'", "'DINT#'", "'LINT#'", "'REAL#'", "'LREAL#'", "'E'", "'e'", "'BOOL#'", "'TRUE'", "'true'", "'FALSE'", "'false'", "'STRING#'", "'WSTRING#'", "'TIME#'", "'T#'", "'TIME_OF_DAY#'", "'TOD#'", "'DATE#'", "'D#'", "'DATE_AND_TIME#'", "'DT#'", "'DINT'", "'INT'", "'SINT'", "'LINT'", "'UNIT'", "'USINT'", "'UDINT'", "'ULINT'", "'REAL'", "'LREAL'", "'BYTE'", "'WORD'", "'DWORD'", "'BOOL'", "'LWORD'", "'TIME'", "'LTIME'", "'DATE'", "'TIME_OF_DAY'", "'DATE_AND_TIME'"
    };
    public static final int RULE_ID=4;
    public static final int T__29=29;
    public static final int T__28=28;
    public static final int T__27=27;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=20;
    public static final int T__21=21;
    public static final int RULE_HEX_INTEGER_VALUE=8;
    public static final int RULE_REALVALUE=9;
    public static final int EOF=-1;
    public static final int T__93=93;
    public static final int T__94=94;
    public static final int T__91=91;
    public static final int T__92=92;
    public static final int T__90=90;
    public static final int T__99=99;
    public static final int T__98=98;
    public static final int T__97=97;
    public static final int T__96=96;
    public static final int T__95=95;
    public static final int T__80=80;
    public static final int T__81=81;
    public static final int T__82=82;
    public static final int T__83=83;
    public static final int RULE_BINARY_INTEGER_VALUE=6;
    public static final int RULE_HEX_DIGIT=15;
    public static final int T__85=85;
    public static final int T__84=84;
    public static final int T__87=87;
    public static final int T__86=86;
    public static final int T__89=89;
    public static final int T__88=88;
    public static final int RULE_ML_COMMENT=13;
    public static final int T__126=126;
    public static final int T__125=125;
    public static final int T__128=128;
    public static final int RULE_STRING=17;
    public static final int T__127=127;
    public static final int T__71=71;
    public static final int T__129=129;
    public static final int T__72=72;
    public static final int T__70=70;
    public static final int T__76=76;
    public static final int RULE_DIGIT=14;
    public static final int T__75=75;
    public static final int T__130=130;
    public static final int T__74=74;
    public static final int T__131=131;
    public static final int T__73=73;
    public static final int T__132=132;
    public static final int T__133=133;
    public static final int T__79=79;
    public static final int T__134=134;
    public static final int T__78=78;
    public static final int T__77=77;
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int T__66=66;
    public static final int T__67=67;
    public static final int T__64=64;
    public static final int T__65=65;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__118=118;
    public static final int T__119=119;
    public static final int T__116=116;
    public static final int T__117=117;
    public static final int T__114=114;
    public static final int T__115=115;
    public static final int T__124=124;
    public static final int T__123=123;
    public static final int RULE_OCTAL_INTEGER_VALUE=7;
    public static final int T__122=122;
    public static final int T__121=121;
    public static final int T__120=120;
    public static final int T__61=61;
    public static final int RULE_S_BYTE_CHAR_STRING=10;
    public static final int T__60=60;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__107=107;
    public static final int T__108=108;
    public static final int T__109=109;
    public static final int T__59=59;
    public static final int T__103=103;
    public static final int T__104=104;
    public static final int T__105=105;
    public static final int T__106=106;
    public static final int RULE_TIME=12;
    public static final int T__111=111;
    public static final int RULE_COMMON_CHAR_VALUE=16;
    public static final int T__110=110;
    public static final int RULE_INT=5;
    public static final int T__113=113;
    public static final int T__112=112;
    public static final int T__50=50;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__102=102;
    public static final int T__101=101;
    public static final int T__100=100;
    public static final int RULE_SL_COMMENT=18;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int RULE_D_BYTE_CHAR_STRING=11;
    public static final int RULE_WS=19;

    // delegates
    // delegators


        public InternalStructuredTextParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalStructuredTextParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalStructuredTextParser.tokenNames; }
    public String getGrammarFileName() { return "../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g"; }



     	private StructuredTextGrammarAccess grammarAccess;
     	
        public InternalStructuredTextParser(TokenStream input, StructuredTextGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "structuredTextAlgorithm";	
       	}
       	
       	@Override
       	protected StructuredTextGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRulestructuredTextAlgorithm"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:67:1: entryRulestructuredTextAlgorithm returns [EObject current=null] : iv_rulestructuredTextAlgorithm= rulestructuredTextAlgorithm EOF ;
    public final EObject entryRulestructuredTextAlgorithm() throws RecognitionException {
        EObject current = null;

        EObject iv_rulestructuredTextAlgorithm = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:68:2: (iv_rulestructuredTextAlgorithm= rulestructuredTextAlgorithm EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:69:2: iv_rulestructuredTextAlgorithm= rulestructuredTextAlgorithm EOF
            {
             newCompositeNode(grammarAccess.getStructuredTextAlgorithmRule()); 
            pushFollow(FOLLOW_rulestructuredTextAlgorithm_in_entryRulestructuredTextAlgorithm75);
            iv_rulestructuredTextAlgorithm=rulestructuredTextAlgorithm();

            state._fsp--;

             current =iv_rulestructuredTextAlgorithm; 
            match(input,EOF,FOLLOW_EOF_in_entryRulestructuredTextAlgorithm85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulestructuredTextAlgorithm"


    // $ANTLR start "rulestructuredTextAlgorithm"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:76:1: rulestructuredTextAlgorithm returns [EObject current=null] : ( () ( (lv_adapters_1_0= ruleAdapterDefinition ) )* otherlv_2= 'VAR_INPUT' ( (lv_varDeclarations_3_0= ruleVarDeclaration ) )* otherlv_4= 'END_VAR' otherlv_5= 'VAR_OUTPUT' ( (lv_varDeclarations_6_0= ruleVarDeclaration ) )* otherlv_7= 'END_VAR' otherlv_8= 'VAR_INTERNAL' ( (lv_varDeclarations_9_0= ruleVarDeclaration ) )* otherlv_10= 'END_VAR' (otherlv_11= 'PLUGS' ( (lv_varDeclarations_12_0= ruleAdapterDeclaration ) )* otherlv_13= 'END_PLUGS' )? (otherlv_14= 'SOCKETS' ( (lv_varDeclarations_15_0= ruleAdapterDeclaration ) )* otherlv_16= 'END_SOCKETS' )? (otherlv_17= 'VAR' ( (lv_varDeclarations_18_0= ruleVarDeclaration ) )* otherlv_19= 'END_VAR' )? ( (lv_statements_20_0= ruleStatementList ) ) ) ;
    public final EObject rulestructuredTextAlgorithm() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        EObject lv_adapters_1_0 = null;

        EObject lv_varDeclarations_3_0 = null;

        EObject lv_varDeclarations_6_0 = null;

        EObject lv_varDeclarations_9_0 = null;

        EObject lv_varDeclarations_12_0 = null;

        EObject lv_varDeclarations_15_0 = null;

        EObject lv_varDeclarations_18_0 = null;

        EObject lv_statements_20_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:79:28: ( ( () ( (lv_adapters_1_0= ruleAdapterDefinition ) )* otherlv_2= 'VAR_INPUT' ( (lv_varDeclarations_3_0= ruleVarDeclaration ) )* otherlv_4= 'END_VAR' otherlv_5= 'VAR_OUTPUT' ( (lv_varDeclarations_6_0= ruleVarDeclaration ) )* otherlv_7= 'END_VAR' otherlv_8= 'VAR_INTERNAL' ( (lv_varDeclarations_9_0= ruleVarDeclaration ) )* otherlv_10= 'END_VAR' (otherlv_11= 'PLUGS' ( (lv_varDeclarations_12_0= ruleAdapterDeclaration ) )* otherlv_13= 'END_PLUGS' )? (otherlv_14= 'SOCKETS' ( (lv_varDeclarations_15_0= ruleAdapterDeclaration ) )* otherlv_16= 'END_SOCKETS' )? (otherlv_17= 'VAR' ( (lv_varDeclarations_18_0= ruleVarDeclaration ) )* otherlv_19= 'END_VAR' )? ( (lv_statements_20_0= ruleStatementList ) ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:80:1: ( () ( (lv_adapters_1_0= ruleAdapterDefinition ) )* otherlv_2= 'VAR_INPUT' ( (lv_varDeclarations_3_0= ruleVarDeclaration ) )* otherlv_4= 'END_VAR' otherlv_5= 'VAR_OUTPUT' ( (lv_varDeclarations_6_0= ruleVarDeclaration ) )* otherlv_7= 'END_VAR' otherlv_8= 'VAR_INTERNAL' ( (lv_varDeclarations_9_0= ruleVarDeclaration ) )* otherlv_10= 'END_VAR' (otherlv_11= 'PLUGS' ( (lv_varDeclarations_12_0= ruleAdapterDeclaration ) )* otherlv_13= 'END_PLUGS' )? (otherlv_14= 'SOCKETS' ( (lv_varDeclarations_15_0= ruleAdapterDeclaration ) )* otherlv_16= 'END_SOCKETS' )? (otherlv_17= 'VAR' ( (lv_varDeclarations_18_0= ruleVarDeclaration ) )* otherlv_19= 'END_VAR' )? ( (lv_statements_20_0= ruleStatementList ) ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:80:1: ( () ( (lv_adapters_1_0= ruleAdapterDefinition ) )* otherlv_2= 'VAR_INPUT' ( (lv_varDeclarations_3_0= ruleVarDeclaration ) )* otherlv_4= 'END_VAR' otherlv_5= 'VAR_OUTPUT' ( (lv_varDeclarations_6_0= ruleVarDeclaration ) )* otherlv_7= 'END_VAR' otherlv_8= 'VAR_INTERNAL' ( (lv_varDeclarations_9_0= ruleVarDeclaration ) )* otherlv_10= 'END_VAR' (otherlv_11= 'PLUGS' ( (lv_varDeclarations_12_0= ruleAdapterDeclaration ) )* otherlv_13= 'END_PLUGS' )? (otherlv_14= 'SOCKETS' ( (lv_varDeclarations_15_0= ruleAdapterDeclaration ) )* otherlv_16= 'END_SOCKETS' )? (otherlv_17= 'VAR' ( (lv_varDeclarations_18_0= ruleVarDeclaration ) )* otherlv_19= 'END_VAR' )? ( (lv_statements_20_0= ruleStatementList ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:80:2: () ( (lv_adapters_1_0= ruleAdapterDefinition ) )* otherlv_2= 'VAR_INPUT' ( (lv_varDeclarations_3_0= ruleVarDeclaration ) )* otherlv_4= 'END_VAR' otherlv_5= 'VAR_OUTPUT' ( (lv_varDeclarations_6_0= ruleVarDeclaration ) )* otherlv_7= 'END_VAR' otherlv_8= 'VAR_INTERNAL' ( (lv_varDeclarations_9_0= ruleVarDeclaration ) )* otherlv_10= 'END_VAR' (otherlv_11= 'PLUGS' ( (lv_varDeclarations_12_0= ruleAdapterDeclaration ) )* otherlv_13= 'END_PLUGS' )? (otherlv_14= 'SOCKETS' ( (lv_varDeclarations_15_0= ruleAdapterDeclaration ) )* otherlv_16= 'END_SOCKETS' )? (otherlv_17= 'VAR' ( (lv_varDeclarations_18_0= ruleVarDeclaration ) )* otherlv_19= 'END_VAR' )? ( (lv_statements_20_0= ruleStatementList ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:80:2: ()
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:81:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getStructuredTextAlgorithmAccess().getStructuredTextAlgorithmAction_0(),
                        current);
                

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:86:2: ( (lv_adapters_1_0= ruleAdapterDefinition ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==32) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:87:1: (lv_adapters_1_0= ruleAdapterDefinition )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:87:1: (lv_adapters_1_0= ruleAdapterDefinition )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:88:3: lv_adapters_1_0= ruleAdapterDefinition
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getStructuredTextAlgorithmAccess().getAdaptersAdapterDefinitionParserRuleCall_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleAdapterDefinition_in_rulestructuredTextAlgorithm140);
            	    lv_adapters_1_0=ruleAdapterDefinition();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getStructuredTextAlgorithmRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"adapters",
            	            		lv_adapters_1_0, 
            	            		"AdapterDefinition");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            otherlv_2=(Token)match(input,21,FOLLOW_21_in_rulestructuredTextAlgorithm153); 

                	newLeafNode(otherlv_2, grammarAccess.getStructuredTextAlgorithmAccess().getVAR_INPUTKeyword_2());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:108:1: ( (lv_varDeclarations_3_0= ruleVarDeclaration ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_ID) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:109:1: (lv_varDeclarations_3_0= ruleVarDeclaration )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:109:1: (lv_varDeclarations_3_0= ruleVarDeclaration )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:110:3: lv_varDeclarations_3_0= ruleVarDeclaration
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getStructuredTextAlgorithmAccess().getVarDeclarationsVarDeclarationParserRuleCall_3_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleVarDeclaration_in_rulestructuredTextAlgorithm174);
            	    lv_varDeclarations_3_0=ruleVarDeclaration();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getStructuredTextAlgorithmRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"varDeclarations",
            	            		lv_varDeclarations_3_0, 
            	            		"VarDeclaration");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            otherlv_4=(Token)match(input,22,FOLLOW_22_in_rulestructuredTextAlgorithm187); 

                	newLeafNode(otherlv_4, grammarAccess.getStructuredTextAlgorithmAccess().getEND_VARKeyword_4());
                
            otherlv_5=(Token)match(input,23,FOLLOW_23_in_rulestructuredTextAlgorithm199); 

                	newLeafNode(otherlv_5, grammarAccess.getStructuredTextAlgorithmAccess().getVAR_OUTPUTKeyword_5());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:134:1: ( (lv_varDeclarations_6_0= ruleVarDeclaration ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==RULE_ID) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:135:1: (lv_varDeclarations_6_0= ruleVarDeclaration )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:135:1: (lv_varDeclarations_6_0= ruleVarDeclaration )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:136:3: lv_varDeclarations_6_0= ruleVarDeclaration
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getStructuredTextAlgorithmAccess().getVarDeclarationsVarDeclarationParserRuleCall_6_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleVarDeclaration_in_rulestructuredTextAlgorithm220);
            	    lv_varDeclarations_6_0=ruleVarDeclaration();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getStructuredTextAlgorithmRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"varDeclarations",
            	            		lv_varDeclarations_6_0, 
            	            		"VarDeclaration");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            otherlv_7=(Token)match(input,22,FOLLOW_22_in_rulestructuredTextAlgorithm233); 

                	newLeafNode(otherlv_7, grammarAccess.getStructuredTextAlgorithmAccess().getEND_VARKeyword_7());
                
            otherlv_8=(Token)match(input,24,FOLLOW_24_in_rulestructuredTextAlgorithm245); 

                	newLeafNode(otherlv_8, grammarAccess.getStructuredTextAlgorithmAccess().getVAR_INTERNALKeyword_8());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:160:1: ( (lv_varDeclarations_9_0= ruleVarDeclaration ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==RULE_ID) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:161:1: (lv_varDeclarations_9_0= ruleVarDeclaration )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:161:1: (lv_varDeclarations_9_0= ruleVarDeclaration )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:162:3: lv_varDeclarations_9_0= ruleVarDeclaration
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getStructuredTextAlgorithmAccess().getVarDeclarationsVarDeclarationParserRuleCall_9_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleVarDeclaration_in_rulestructuredTextAlgorithm266);
            	    lv_varDeclarations_9_0=ruleVarDeclaration();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getStructuredTextAlgorithmRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"varDeclarations",
            	            		lv_varDeclarations_9_0, 
            	            		"VarDeclaration");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            otherlv_10=(Token)match(input,22,FOLLOW_22_in_rulestructuredTextAlgorithm279); 

                	newLeafNode(otherlv_10, grammarAccess.getStructuredTextAlgorithmAccess().getEND_VARKeyword_10());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:182:1: (otherlv_11= 'PLUGS' ( (lv_varDeclarations_12_0= ruleAdapterDeclaration ) )* otherlv_13= 'END_PLUGS' )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==25) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:182:3: otherlv_11= 'PLUGS' ( (lv_varDeclarations_12_0= ruleAdapterDeclaration ) )* otherlv_13= 'END_PLUGS'
                    {
                    otherlv_11=(Token)match(input,25,FOLLOW_25_in_rulestructuredTextAlgorithm292); 

                        	newLeafNode(otherlv_11, grammarAccess.getStructuredTextAlgorithmAccess().getPLUGSKeyword_11_0());
                        
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:186:1: ( (lv_varDeclarations_12_0= ruleAdapterDeclaration ) )*
                    loop5:
                    do {
                        int alt5=2;
                        int LA5_0 = input.LA(1);

                        if ( (LA5_0==RULE_ID) ) {
                            alt5=1;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:187:1: (lv_varDeclarations_12_0= ruleAdapterDeclaration )
                    	    {
                    	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:187:1: (lv_varDeclarations_12_0= ruleAdapterDeclaration )
                    	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:188:3: lv_varDeclarations_12_0= ruleAdapterDeclaration
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getStructuredTextAlgorithmAccess().getVarDeclarationsAdapterDeclarationParserRuleCall_11_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_ruleAdapterDeclaration_in_rulestructuredTextAlgorithm313);
                    	    lv_varDeclarations_12_0=ruleAdapterDeclaration();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getStructuredTextAlgorithmRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"varDeclarations",
                    	            		lv_varDeclarations_12_0, 
                    	            		"AdapterDeclaration");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop5;
                        }
                    } while (true);

                    otherlv_13=(Token)match(input,26,FOLLOW_26_in_rulestructuredTextAlgorithm326); 

                        	newLeafNode(otherlv_13, grammarAccess.getStructuredTextAlgorithmAccess().getEND_PLUGSKeyword_11_2());
                        

                    }
                    break;

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:208:3: (otherlv_14= 'SOCKETS' ( (lv_varDeclarations_15_0= ruleAdapterDeclaration ) )* otherlv_16= 'END_SOCKETS' )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==27) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:208:5: otherlv_14= 'SOCKETS' ( (lv_varDeclarations_15_0= ruleAdapterDeclaration ) )* otherlv_16= 'END_SOCKETS'
                    {
                    otherlv_14=(Token)match(input,27,FOLLOW_27_in_rulestructuredTextAlgorithm341); 

                        	newLeafNode(otherlv_14, grammarAccess.getStructuredTextAlgorithmAccess().getSOCKETSKeyword_12_0());
                        
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:212:1: ( (lv_varDeclarations_15_0= ruleAdapterDeclaration ) )*
                    loop7:
                    do {
                        int alt7=2;
                        int LA7_0 = input.LA(1);

                        if ( (LA7_0==RULE_ID) ) {
                            alt7=1;
                        }


                        switch (alt7) {
                    	case 1 :
                    	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:213:1: (lv_varDeclarations_15_0= ruleAdapterDeclaration )
                    	    {
                    	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:213:1: (lv_varDeclarations_15_0= ruleAdapterDeclaration )
                    	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:214:3: lv_varDeclarations_15_0= ruleAdapterDeclaration
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getStructuredTextAlgorithmAccess().getVarDeclarationsAdapterDeclarationParserRuleCall_12_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_ruleAdapterDeclaration_in_rulestructuredTextAlgorithm362);
                    	    lv_varDeclarations_15_0=ruleAdapterDeclaration();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getStructuredTextAlgorithmRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"varDeclarations",
                    	            		lv_varDeclarations_15_0, 
                    	            		"AdapterDeclaration");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop7;
                        }
                    } while (true);

                    otherlv_16=(Token)match(input,28,FOLLOW_28_in_rulestructuredTextAlgorithm375); 

                        	newLeafNode(otherlv_16, grammarAccess.getStructuredTextAlgorithmAccess().getEND_SOCKETSKeyword_12_2());
                        

                    }
                    break;

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:234:3: (otherlv_17= 'VAR' ( (lv_varDeclarations_18_0= ruleVarDeclaration ) )* otherlv_19= 'END_VAR' )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==29) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:234:5: otherlv_17= 'VAR' ( (lv_varDeclarations_18_0= ruleVarDeclaration ) )* otherlv_19= 'END_VAR'
                    {
                    otherlv_17=(Token)match(input,29,FOLLOW_29_in_rulestructuredTextAlgorithm390); 

                        	newLeafNode(otherlv_17, grammarAccess.getStructuredTextAlgorithmAccess().getVARKeyword_13_0());
                        
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:238:1: ( (lv_varDeclarations_18_0= ruleVarDeclaration ) )*
                    loop9:
                    do {
                        int alt9=2;
                        int LA9_0 = input.LA(1);

                        if ( (LA9_0==RULE_ID) ) {
                            alt9=1;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:239:1: (lv_varDeclarations_18_0= ruleVarDeclaration )
                    	    {
                    	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:239:1: (lv_varDeclarations_18_0= ruleVarDeclaration )
                    	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:240:3: lv_varDeclarations_18_0= ruleVarDeclaration
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getStructuredTextAlgorithmAccess().getVarDeclarationsVarDeclarationParserRuleCall_13_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_ruleVarDeclaration_in_rulestructuredTextAlgorithm411);
                    	    lv_varDeclarations_18_0=ruleVarDeclaration();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getStructuredTextAlgorithmRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"varDeclarations",
                    	            		lv_varDeclarations_18_0, 
                    	            		"VarDeclaration");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop9;
                        }
                    } while (true);

                    otherlv_19=(Token)match(input,22,FOLLOW_22_in_rulestructuredTextAlgorithm424); 

                        	newLeafNode(otherlv_19, grammarAccess.getStructuredTextAlgorithmAccess().getEND_VARKeyword_13_2());
                        

                    }
                    break;

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:260:3: ( (lv_statements_20_0= ruleStatementList ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:261:1: (lv_statements_20_0= ruleStatementList )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:261:1: (lv_statements_20_0= ruleStatementList )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:262:3: lv_statements_20_0= ruleStatementList
            {
             
            	        newCompositeNode(grammarAccess.getStructuredTextAlgorithmAccess().getStatementsStatementListParserRuleCall_14_0()); 
            	    
            pushFollow(FOLLOW_ruleStatementList_in_rulestructuredTextAlgorithm447);
            lv_statements_20_0=ruleStatementList();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getStructuredTextAlgorithmRule());
            	        }
                   		set(
                   			current, 
                   			"statements",
                    		lv_statements_20_0, 
                    		"StatementList");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulestructuredTextAlgorithm"


    // $ANTLR start "entryRuleVarDeclaration"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:286:1: entryRuleVarDeclaration returns [EObject current=null] : iv_ruleVarDeclaration= ruleVarDeclaration EOF ;
    public final EObject entryRuleVarDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVarDeclaration = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:287:2: (iv_ruleVarDeclaration= ruleVarDeclaration EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:288:2: iv_ruleVarDeclaration= ruleVarDeclaration EOF
            {
             newCompositeNode(grammarAccess.getVarDeclarationRule()); 
            pushFollow(FOLLOW_ruleVarDeclaration_in_entryRuleVarDeclaration483);
            iv_ruleVarDeclaration=ruleVarDeclaration();

            state._fsp--;

             current =iv_ruleVarDeclaration; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVarDeclaration493); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVarDeclaration"


    // $ANTLR start "ruleVarDeclaration"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:295:1: ruleVarDeclaration returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( ( (lv_specification_2_1= ruleSimple_Spec_Init | lv_specification_2_2= ruleArray_Spec_Init | lv_specification_2_3= ruleS_Byte_String_Spec | lv_specification_2_4= ruleD_Byte_String_Spec ) ) ) otherlv_3= ';' ) ;
    public final EObject ruleVarDeclaration() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_specification_2_1 = null;

        EObject lv_specification_2_2 = null;

        EObject lv_specification_2_3 = null;

        EObject lv_specification_2_4 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:298:28: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( ( (lv_specification_2_1= ruleSimple_Spec_Init | lv_specification_2_2= ruleArray_Spec_Init | lv_specification_2_3= ruleS_Byte_String_Spec | lv_specification_2_4= ruleD_Byte_String_Spec ) ) ) otherlv_3= ';' ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:299:1: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( ( (lv_specification_2_1= ruleSimple_Spec_Init | lv_specification_2_2= ruleArray_Spec_Init | lv_specification_2_3= ruleS_Byte_String_Spec | lv_specification_2_4= ruleD_Byte_String_Spec ) ) ) otherlv_3= ';' )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:299:1: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( ( (lv_specification_2_1= ruleSimple_Spec_Init | lv_specification_2_2= ruleArray_Spec_Init | lv_specification_2_3= ruleS_Byte_String_Spec | lv_specification_2_4= ruleD_Byte_String_Spec ) ) ) otherlv_3= ';' )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:299:2: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( ( (lv_specification_2_1= ruleSimple_Spec_Init | lv_specification_2_2= ruleArray_Spec_Init | lv_specification_2_3= ruleS_Byte_String_Spec | lv_specification_2_4= ruleD_Byte_String_Spec ) ) ) otherlv_3= ';'
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:299:2: ( (lv_name_0_0= RULE_ID ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:300:1: (lv_name_0_0= RULE_ID )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:300:1: (lv_name_0_0= RULE_ID )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:301:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleVarDeclaration535); 

            			newLeafNode(lv_name_0_0, grammarAccess.getVarDeclarationAccess().getNameIDTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getVarDeclarationRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"ID");
            	    

            }


            }

            otherlv_1=(Token)match(input,30,FOLLOW_30_in_ruleVarDeclaration552); 

                	newLeafNode(otherlv_1, grammarAccess.getVarDeclarationAccess().getColonKeyword_1());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:321:1: ( ( (lv_specification_2_1= ruleSimple_Spec_Init | lv_specification_2_2= ruleArray_Spec_Init | lv_specification_2_3= ruleS_Byte_String_Spec | lv_specification_2_4= ruleD_Byte_String_Spec ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:322:1: ( (lv_specification_2_1= ruleSimple_Spec_Init | lv_specification_2_2= ruleArray_Spec_Init | lv_specification_2_3= ruleS_Byte_String_Spec | lv_specification_2_4= ruleD_Byte_String_Spec ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:322:1: ( (lv_specification_2_1= ruleSimple_Spec_Init | lv_specification_2_2= ruleArray_Spec_Init | lv_specification_2_3= ruleS_Byte_String_Spec | lv_specification_2_4= ruleD_Byte_String_Spec ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:323:1: (lv_specification_2_1= ruleSimple_Spec_Init | lv_specification_2_2= ruleArray_Spec_Init | lv_specification_2_3= ruleS_Byte_String_Spec | lv_specification_2_4= ruleD_Byte_String_Spec )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:323:1: (lv_specification_2_1= ruleSimple_Spec_Init | lv_specification_2_2= ruleArray_Spec_Init | lv_specification_2_3= ruleS_Byte_String_Spec | lv_specification_2_4= ruleD_Byte_String_Spec )
            int alt11=4;
            switch ( input.LA(1) ) {
            case 115:
            case 116:
            case 117:
            case 118:
            case 119:
            case 120:
            case 121:
            case 122:
            case 123:
            case 124:
            case 125:
            case 126:
            case 127:
            case 128:
            case 129:
            case 130:
            case 131:
            case 132:
            case 133:
            case 134:
                {
                alt11=1;
                }
                break;
            case 38:
                {
                alt11=2;
                }
                break;
            case 43:
                {
                alt11=3;
                }
                break;
            case 44:
                {
                alt11=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }

            switch (alt11) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:324:3: lv_specification_2_1= ruleSimple_Spec_Init
                    {
                     
                    	        newCompositeNode(grammarAccess.getVarDeclarationAccess().getSpecificationSimple_Spec_InitParserRuleCall_2_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruleSimple_Spec_Init_in_ruleVarDeclaration575);
                    lv_specification_2_1=ruleSimple_Spec_Init();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getVarDeclarationRule());
                    	        }
                           		set(
                           			current, 
                           			"specification",
                            		lv_specification_2_1, 
                            		"Simple_Spec_Init");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:339:8: lv_specification_2_2= ruleArray_Spec_Init
                    {
                     
                    	        newCompositeNode(grammarAccess.getVarDeclarationAccess().getSpecificationArray_Spec_InitParserRuleCall_2_0_1()); 
                    	    
                    pushFollow(FOLLOW_ruleArray_Spec_Init_in_ruleVarDeclaration594);
                    lv_specification_2_2=ruleArray_Spec_Init();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getVarDeclarationRule());
                    	        }
                           		set(
                           			current, 
                           			"specification",
                            		lv_specification_2_2, 
                            		"Array_Spec_Init");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }
                    break;
                case 3 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:354:8: lv_specification_2_3= ruleS_Byte_String_Spec
                    {
                     
                    	        newCompositeNode(grammarAccess.getVarDeclarationAccess().getSpecificationS_Byte_String_SpecParserRuleCall_2_0_2()); 
                    	    
                    pushFollow(FOLLOW_ruleS_Byte_String_Spec_in_ruleVarDeclaration613);
                    lv_specification_2_3=ruleS_Byte_String_Spec();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getVarDeclarationRule());
                    	        }
                           		set(
                           			current, 
                           			"specification",
                            		lv_specification_2_3, 
                            		"S_Byte_String_Spec");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }
                    break;
                case 4 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:369:8: lv_specification_2_4= ruleD_Byte_String_Spec
                    {
                     
                    	        newCompositeNode(grammarAccess.getVarDeclarationAccess().getSpecificationD_Byte_String_SpecParserRuleCall_2_0_3()); 
                    	    
                    pushFollow(FOLLOW_ruleD_Byte_String_Spec_in_ruleVarDeclaration632);
                    lv_specification_2_4=ruleD_Byte_String_Spec();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getVarDeclarationRule());
                    	        }
                           		set(
                           			current, 
                           			"specification",
                            		lv_specification_2_4, 
                            		"D_Byte_String_Spec");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }
                    break;

            }


            }


            }

            otherlv_3=(Token)match(input,31,FOLLOW_31_in_ruleVarDeclaration647); 

                	newLeafNode(otherlv_3, grammarAccess.getVarDeclarationAccess().getSemicolonKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVarDeclaration"


    // $ANTLR start "entryRuleAdapterDefinition"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:399:1: entryRuleAdapterDefinition returns [EObject current=null] : iv_ruleAdapterDefinition= ruleAdapterDefinition EOF ;
    public final EObject entryRuleAdapterDefinition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAdapterDefinition = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:400:2: (iv_ruleAdapterDefinition= ruleAdapterDefinition EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:401:2: iv_ruleAdapterDefinition= ruleAdapterDefinition EOF
            {
             newCompositeNode(grammarAccess.getAdapterDefinitionRule()); 
            pushFollow(FOLLOW_ruleAdapterDefinition_in_entryRuleAdapterDefinition683);
            iv_ruleAdapterDefinition=ruleAdapterDefinition();

            state._fsp--;

             current =iv_ruleAdapterDefinition; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAdapterDefinition693); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAdapterDefinition"


    // $ANTLR start "ruleAdapterDefinition"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:408:1: ruleAdapterDefinition returns [EObject current=null] : (otherlv_0= 'ADAPTER' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'VAR_INPUT' ( (lv_varDeclarations_3_0= ruleVarDeclaration ) )* otherlv_4= 'END_VAR' )? (otherlv_5= 'VAR_OUTPUT' ( (lv_varDeclarations_6_0= ruleVarDeclaration ) )* otherlv_7= 'END_VAR' )? otherlv_8= 'END_ADAPTER' ) ;
    public final EObject ruleAdapterDefinition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        EObject lv_varDeclarations_3_0 = null;

        EObject lv_varDeclarations_6_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:411:28: ( (otherlv_0= 'ADAPTER' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'VAR_INPUT' ( (lv_varDeclarations_3_0= ruleVarDeclaration ) )* otherlv_4= 'END_VAR' )? (otherlv_5= 'VAR_OUTPUT' ( (lv_varDeclarations_6_0= ruleVarDeclaration ) )* otherlv_7= 'END_VAR' )? otherlv_8= 'END_ADAPTER' ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:412:1: (otherlv_0= 'ADAPTER' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'VAR_INPUT' ( (lv_varDeclarations_3_0= ruleVarDeclaration ) )* otherlv_4= 'END_VAR' )? (otherlv_5= 'VAR_OUTPUT' ( (lv_varDeclarations_6_0= ruleVarDeclaration ) )* otherlv_7= 'END_VAR' )? otherlv_8= 'END_ADAPTER' )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:412:1: (otherlv_0= 'ADAPTER' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'VAR_INPUT' ( (lv_varDeclarations_3_0= ruleVarDeclaration ) )* otherlv_4= 'END_VAR' )? (otherlv_5= 'VAR_OUTPUT' ( (lv_varDeclarations_6_0= ruleVarDeclaration ) )* otherlv_7= 'END_VAR' )? otherlv_8= 'END_ADAPTER' )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:412:3: otherlv_0= 'ADAPTER' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'VAR_INPUT' ( (lv_varDeclarations_3_0= ruleVarDeclaration ) )* otherlv_4= 'END_VAR' )? (otherlv_5= 'VAR_OUTPUT' ( (lv_varDeclarations_6_0= ruleVarDeclaration ) )* otherlv_7= 'END_VAR' )? otherlv_8= 'END_ADAPTER'
            {
            otherlv_0=(Token)match(input,32,FOLLOW_32_in_ruleAdapterDefinition730); 

                	newLeafNode(otherlv_0, grammarAccess.getAdapterDefinitionAccess().getADAPTERKeyword_0());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:416:1: ( (lv_name_1_0= RULE_ID ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:417:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:417:1: (lv_name_1_0= RULE_ID )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:418:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleAdapterDefinition747); 

            			newLeafNode(lv_name_1_0, grammarAccess.getAdapterDefinitionAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getAdapterDefinitionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:434:2: (otherlv_2= 'VAR_INPUT' ( (lv_varDeclarations_3_0= ruleVarDeclaration ) )* otherlv_4= 'END_VAR' )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==21) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:434:4: otherlv_2= 'VAR_INPUT' ( (lv_varDeclarations_3_0= ruleVarDeclaration ) )* otherlv_4= 'END_VAR'
                    {
                    otherlv_2=(Token)match(input,21,FOLLOW_21_in_ruleAdapterDefinition765); 

                        	newLeafNode(otherlv_2, grammarAccess.getAdapterDefinitionAccess().getVAR_INPUTKeyword_2_0());
                        
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:438:1: ( (lv_varDeclarations_3_0= ruleVarDeclaration ) )*
                    loop12:
                    do {
                        int alt12=2;
                        int LA12_0 = input.LA(1);

                        if ( (LA12_0==RULE_ID) ) {
                            alt12=1;
                        }


                        switch (alt12) {
                    	case 1 :
                    	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:439:1: (lv_varDeclarations_3_0= ruleVarDeclaration )
                    	    {
                    	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:439:1: (lv_varDeclarations_3_0= ruleVarDeclaration )
                    	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:440:3: lv_varDeclarations_3_0= ruleVarDeclaration
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getAdapterDefinitionAccess().getVarDeclarationsVarDeclarationParserRuleCall_2_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_ruleVarDeclaration_in_ruleAdapterDefinition786);
                    	    lv_varDeclarations_3_0=ruleVarDeclaration();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getAdapterDefinitionRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"varDeclarations",
                    	            		lv_varDeclarations_3_0, 
                    	            		"VarDeclaration");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop12;
                        }
                    } while (true);

                    otherlv_4=(Token)match(input,22,FOLLOW_22_in_ruleAdapterDefinition799); 

                        	newLeafNode(otherlv_4, grammarAccess.getAdapterDefinitionAccess().getEND_VARKeyword_2_2());
                        

                    }
                    break;

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:460:3: (otherlv_5= 'VAR_OUTPUT' ( (lv_varDeclarations_6_0= ruleVarDeclaration ) )* otherlv_7= 'END_VAR' )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==23) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:460:5: otherlv_5= 'VAR_OUTPUT' ( (lv_varDeclarations_6_0= ruleVarDeclaration ) )* otherlv_7= 'END_VAR'
                    {
                    otherlv_5=(Token)match(input,23,FOLLOW_23_in_ruleAdapterDefinition814); 

                        	newLeafNode(otherlv_5, grammarAccess.getAdapterDefinitionAccess().getVAR_OUTPUTKeyword_3_0());
                        
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:464:1: ( (lv_varDeclarations_6_0= ruleVarDeclaration ) )*
                    loop14:
                    do {
                        int alt14=2;
                        int LA14_0 = input.LA(1);

                        if ( (LA14_0==RULE_ID) ) {
                            alt14=1;
                        }


                        switch (alt14) {
                    	case 1 :
                    	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:465:1: (lv_varDeclarations_6_0= ruleVarDeclaration )
                    	    {
                    	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:465:1: (lv_varDeclarations_6_0= ruleVarDeclaration )
                    	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:466:3: lv_varDeclarations_6_0= ruleVarDeclaration
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getAdapterDefinitionAccess().getVarDeclarationsVarDeclarationParserRuleCall_3_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_ruleVarDeclaration_in_ruleAdapterDefinition835);
                    	    lv_varDeclarations_6_0=ruleVarDeclaration();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getAdapterDefinitionRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"varDeclarations",
                    	            		lv_varDeclarations_6_0, 
                    	            		"VarDeclaration");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop14;
                        }
                    } while (true);

                    otherlv_7=(Token)match(input,22,FOLLOW_22_in_ruleAdapterDefinition848); 

                        	newLeafNode(otherlv_7, grammarAccess.getAdapterDefinitionAccess().getEND_VARKeyword_3_2());
                        

                    }
                    break;

            }

            otherlv_8=(Token)match(input,33,FOLLOW_33_in_ruleAdapterDefinition862); 

                	newLeafNode(otherlv_8, grammarAccess.getAdapterDefinitionAccess().getEND_ADAPTERKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAdapterDefinition"


    // $ANTLR start "entryRuleAdapterDeclaration"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:498:1: entryRuleAdapterDeclaration returns [EObject current=null] : iv_ruleAdapterDeclaration= ruleAdapterDeclaration EOF ;
    public final EObject entryRuleAdapterDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAdapterDeclaration = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:499:2: (iv_ruleAdapterDeclaration= ruleAdapterDeclaration EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:500:2: iv_ruleAdapterDeclaration= ruleAdapterDeclaration EOF
            {
             newCompositeNode(grammarAccess.getAdapterDeclarationRule()); 
            pushFollow(FOLLOW_ruleAdapterDeclaration_in_entryRuleAdapterDeclaration898);
            iv_ruleAdapterDeclaration=ruleAdapterDeclaration();

            state._fsp--;

             current =iv_ruleAdapterDeclaration; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAdapterDeclaration908); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAdapterDeclaration"


    // $ANTLR start "ruleAdapterDeclaration"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:507:1: ruleAdapterDeclaration returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) (otherlv_3= '(' ( (lv_parameters_4_0= ruleParameter ) ) (otherlv_5= ',' ( (lv_parameters_6_0= ruleParameter ) ) )* otherlv_7= ')' )? otherlv_8= ';' ) ;
    public final EObject ruleAdapterDeclaration() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        EObject lv_parameters_4_0 = null;

        EObject lv_parameters_6_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:510:28: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) (otherlv_3= '(' ( (lv_parameters_4_0= ruleParameter ) ) (otherlv_5= ',' ( (lv_parameters_6_0= ruleParameter ) ) )* otherlv_7= ')' )? otherlv_8= ';' ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:511:1: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) (otherlv_3= '(' ( (lv_parameters_4_0= ruleParameter ) ) (otherlv_5= ',' ( (lv_parameters_6_0= ruleParameter ) ) )* otherlv_7= ')' )? otherlv_8= ';' )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:511:1: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) (otherlv_3= '(' ( (lv_parameters_4_0= ruleParameter ) ) (otherlv_5= ',' ( (lv_parameters_6_0= ruleParameter ) ) )* otherlv_7= ')' )? otherlv_8= ';' )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:511:2: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) (otherlv_3= '(' ( (lv_parameters_4_0= ruleParameter ) ) (otherlv_5= ',' ( (lv_parameters_6_0= ruleParameter ) ) )* otherlv_7= ')' )? otherlv_8= ';'
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:511:2: ( (lv_name_0_0= RULE_ID ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:512:1: (lv_name_0_0= RULE_ID )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:512:1: (lv_name_0_0= RULE_ID )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:513:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleAdapterDeclaration950); 

            			newLeafNode(lv_name_0_0, grammarAccess.getAdapterDeclarationAccess().getNameIDTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getAdapterDeclarationRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"ID");
            	    

            }


            }

            otherlv_1=(Token)match(input,30,FOLLOW_30_in_ruleAdapterDeclaration967); 

                	newLeafNode(otherlv_1, grammarAccess.getAdapterDeclarationAccess().getColonKeyword_1());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:533:1: ( (otherlv_2= RULE_ID ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:534:1: (otherlv_2= RULE_ID )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:534:1: (otherlv_2= RULE_ID )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:535:3: otherlv_2= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getAdapterDeclarationRule());
            	        }
                    
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleAdapterDeclaration987); 

            		newLeafNode(otherlv_2, grammarAccess.getAdapterDeclarationAccess().getTypeAdapterDefinitionCrossReference_2_0()); 
            	

            }


            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:546:2: (otherlv_3= '(' ( (lv_parameters_4_0= ruleParameter ) ) (otherlv_5= ',' ( (lv_parameters_6_0= ruleParameter ) ) )* otherlv_7= ')' )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==34) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:546:4: otherlv_3= '(' ( (lv_parameters_4_0= ruleParameter ) ) (otherlv_5= ',' ( (lv_parameters_6_0= ruleParameter ) ) )* otherlv_7= ')'
                    {
                    otherlv_3=(Token)match(input,34,FOLLOW_34_in_ruleAdapterDeclaration1000); 

                        	newLeafNode(otherlv_3, grammarAccess.getAdapterDeclarationAccess().getLeftParenthesisKeyword_3_0());
                        
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:550:1: ( (lv_parameters_4_0= ruleParameter ) )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:551:1: (lv_parameters_4_0= ruleParameter )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:551:1: (lv_parameters_4_0= ruleParameter )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:552:3: lv_parameters_4_0= ruleParameter
                    {
                     
                    	        newCompositeNode(grammarAccess.getAdapterDeclarationAccess().getParametersParameterParserRuleCall_3_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleParameter_in_ruleAdapterDeclaration1021);
                    lv_parameters_4_0=ruleParameter();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getAdapterDeclarationRule());
                    	        }
                           		add(
                           			current, 
                           			"parameters",
                            		lv_parameters_4_0, 
                            		"Parameter");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:568:2: (otherlv_5= ',' ( (lv_parameters_6_0= ruleParameter ) ) )*
                    loop16:
                    do {
                        int alt16=2;
                        int LA16_0 = input.LA(1);

                        if ( (LA16_0==35) ) {
                            alt16=1;
                        }


                        switch (alt16) {
                    	case 1 :
                    	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:568:4: otherlv_5= ',' ( (lv_parameters_6_0= ruleParameter ) )
                    	    {
                    	    otherlv_5=(Token)match(input,35,FOLLOW_35_in_ruleAdapterDeclaration1034); 

                    	        	newLeafNode(otherlv_5, grammarAccess.getAdapterDeclarationAccess().getCommaKeyword_3_2_0());
                    	        
                    	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:572:1: ( (lv_parameters_6_0= ruleParameter ) )
                    	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:573:1: (lv_parameters_6_0= ruleParameter )
                    	    {
                    	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:573:1: (lv_parameters_6_0= ruleParameter )
                    	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:574:3: lv_parameters_6_0= ruleParameter
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getAdapterDeclarationAccess().getParametersParameterParserRuleCall_3_2_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_ruleParameter_in_ruleAdapterDeclaration1055);
                    	    lv_parameters_6_0=ruleParameter();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getAdapterDeclarationRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"parameters",
                    	            		lv_parameters_6_0, 
                    	            		"Parameter");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop16;
                        }
                    } while (true);

                    otherlv_7=(Token)match(input,36,FOLLOW_36_in_ruleAdapterDeclaration1069); 

                        	newLeafNode(otherlv_7, grammarAccess.getAdapterDeclarationAccess().getRightParenthesisKeyword_3_3());
                        

                    }
                    break;

            }

            otherlv_8=(Token)match(input,31,FOLLOW_31_in_ruleAdapterDeclaration1083); 

                	newLeafNode(otherlv_8, grammarAccess.getAdapterDeclarationAccess().getSemicolonKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAdapterDeclaration"


    // $ANTLR start "entryRuleParameter"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:606:1: entryRuleParameter returns [EObject current=null] : iv_ruleParameter= ruleParameter EOF ;
    public final EObject entryRuleParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParameter = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:607:2: (iv_ruleParameter= ruleParameter EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:608:2: iv_ruleParameter= ruleParameter EOF
            {
             newCompositeNode(grammarAccess.getParameterRule()); 
            pushFollow(FOLLOW_ruleParameter_in_entryRuleParameter1119);
            iv_ruleParameter=ruleParameter();

            state._fsp--;

             current =iv_ruleParameter; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleParameter1129); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParameter"


    // $ANTLR start "ruleParameter"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:615:1: ruleParameter returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':=' ( ( (lv_value_2_1= ruleConstant | lv_value_2_2= ruleArray_Initialization ) ) ) otherlv_3= ';' ) ;
    public final EObject ruleParameter() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_value_2_1 = null;

        EObject lv_value_2_2 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:618:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':=' ( ( (lv_value_2_1= ruleConstant | lv_value_2_2= ruleArray_Initialization ) ) ) otherlv_3= ';' ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:619:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':=' ( ( (lv_value_2_1= ruleConstant | lv_value_2_2= ruleArray_Initialization ) ) ) otherlv_3= ';' )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:619:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':=' ( ( (lv_value_2_1= ruleConstant | lv_value_2_2= ruleArray_Initialization ) ) ) otherlv_3= ';' )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:619:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= ':=' ( ( (lv_value_2_1= ruleConstant | lv_value_2_2= ruleArray_Initialization ) ) ) otherlv_3= ';'
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:619:2: ( (otherlv_0= RULE_ID ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:620:1: (otherlv_0= RULE_ID )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:620:1: (otherlv_0= RULE_ID )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:621:3: otherlv_0= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getParameterRule());
            	        }
                    
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleParameter1174); 

            		newLeafNode(otherlv_0, grammarAccess.getParameterAccess().getParamVarDeclarationCrossReference_0_0()); 
            	

            }


            }

            otherlv_1=(Token)match(input,37,FOLLOW_37_in_ruleParameter1186); 

                	newLeafNode(otherlv_1, grammarAccess.getParameterAccess().getColonEqualsSignKeyword_1());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:636:1: ( ( (lv_value_2_1= ruleConstant | lv_value_2_2= ruleArray_Initialization ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:637:1: ( (lv_value_2_1= ruleConstant | lv_value_2_2= ruleArray_Initialization ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:637:1: ( (lv_value_2_1= ruleConstant | lv_value_2_2= ruleArray_Initialization ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:638:1: (lv_value_2_1= ruleConstant | lv_value_2_2= ruleArray_Initialization )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:638:1: (lv_value_2_1= ruleConstant | lv_value_2_2= ruleArray_Initialization )
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( ((LA18_0>=RULE_INT && LA18_0<=RULE_D_BYTE_CHAR_STRING)||(LA18_0>=76 && LA18_0<=77)||(LA18_0>=84 && LA18_0<=97)||(LA18_0>=100 && LA18_0<=114)) ) {
                alt18=1;
            }
            else if ( (LA18_0==39) ) {
                alt18=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }
            switch (alt18) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:639:3: lv_value_2_1= ruleConstant
                    {
                     
                    	        newCompositeNode(grammarAccess.getParameterAccess().getValueConstantParserRuleCall_2_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruleConstant_in_ruleParameter1209);
                    lv_value_2_1=ruleConstant();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getParameterRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_2_1, 
                            		"Constant");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:654:8: lv_value_2_2= ruleArray_Initialization
                    {
                     
                    	        newCompositeNode(grammarAccess.getParameterAccess().getValueArray_InitializationParserRuleCall_2_0_1()); 
                    	    
                    pushFollow(FOLLOW_ruleArray_Initialization_in_ruleParameter1228);
                    lv_value_2_2=ruleArray_Initialization();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getParameterRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_2_2, 
                            		"Array_Initialization");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }
                    break;

            }


            }


            }

            otherlv_3=(Token)match(input,31,FOLLOW_31_in_ruleParameter1243); 

                	newLeafNode(otherlv_3, grammarAccess.getParameterAccess().getSemicolonKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParameter"


    // $ANTLR start "entryRuleSimple_Spec_Init"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:684:1: entryRuleSimple_Spec_Init returns [EObject current=null] : iv_ruleSimple_Spec_Init= ruleSimple_Spec_Init EOF ;
    public final EObject entryRuleSimple_Spec_Init() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimple_Spec_Init = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:685:2: (iv_ruleSimple_Spec_Init= ruleSimple_Spec_Init EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:686:2: iv_ruleSimple_Spec_Init= ruleSimple_Spec_Init EOF
            {
             newCompositeNode(grammarAccess.getSimple_Spec_InitRule()); 
            pushFollow(FOLLOW_ruleSimple_Spec_Init_in_entryRuleSimple_Spec_Init1279);
            iv_ruleSimple_Spec_Init=ruleSimple_Spec_Init();

            state._fsp--;

             current =iv_ruleSimple_Spec_Init; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSimple_Spec_Init1289); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimple_Spec_Init"


    // $ANTLR start "ruleSimple_Spec_Init"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:693:1: ruleSimple_Spec_Init returns [EObject current=null] : ( ( (lv_type_0_0= ruleElementary_Type_Name ) ) (otherlv_1= ':=' ( (lv_constant_2_0= ruleConstant ) ) )? ) ;
    public final EObject ruleSimple_Spec_Init() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_type_0_0 = null;

        EObject lv_constant_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:696:28: ( ( ( (lv_type_0_0= ruleElementary_Type_Name ) ) (otherlv_1= ':=' ( (lv_constant_2_0= ruleConstant ) ) )? ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:697:1: ( ( (lv_type_0_0= ruleElementary_Type_Name ) ) (otherlv_1= ':=' ( (lv_constant_2_0= ruleConstant ) ) )? )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:697:1: ( ( (lv_type_0_0= ruleElementary_Type_Name ) ) (otherlv_1= ':=' ( (lv_constant_2_0= ruleConstant ) ) )? )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:697:2: ( (lv_type_0_0= ruleElementary_Type_Name ) ) (otherlv_1= ':=' ( (lv_constant_2_0= ruleConstant ) ) )?
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:697:2: ( (lv_type_0_0= ruleElementary_Type_Name ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:698:1: (lv_type_0_0= ruleElementary_Type_Name )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:698:1: (lv_type_0_0= ruleElementary_Type_Name )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:699:3: lv_type_0_0= ruleElementary_Type_Name
            {
             
            	        newCompositeNode(grammarAccess.getSimple_Spec_InitAccess().getTypeElementary_Type_NameParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleElementary_Type_Name_in_ruleSimple_Spec_Init1335);
            lv_type_0_0=ruleElementary_Type_Name();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSimple_Spec_InitRule());
            	        }
                   		set(
                   			current, 
                   			"type",
                    		lv_type_0_0, 
                    		"Elementary_Type_Name");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:715:2: (otherlv_1= ':=' ( (lv_constant_2_0= ruleConstant ) ) )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==37) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:715:4: otherlv_1= ':=' ( (lv_constant_2_0= ruleConstant ) )
                    {
                    otherlv_1=(Token)match(input,37,FOLLOW_37_in_ruleSimple_Spec_Init1348); 

                        	newLeafNode(otherlv_1, grammarAccess.getSimple_Spec_InitAccess().getColonEqualsSignKeyword_1_0());
                        
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:719:1: ( (lv_constant_2_0= ruleConstant ) )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:720:1: (lv_constant_2_0= ruleConstant )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:720:1: (lv_constant_2_0= ruleConstant )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:721:3: lv_constant_2_0= ruleConstant
                    {
                     
                    	        newCompositeNode(grammarAccess.getSimple_Spec_InitAccess().getConstantConstantParserRuleCall_1_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleConstant_in_ruleSimple_Spec_Init1369);
                    lv_constant_2_0=ruleConstant();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getSimple_Spec_InitRule());
                    	        }
                           		set(
                           			current, 
                           			"constant",
                            		lv_constant_2_0, 
                            		"Constant");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimple_Spec_Init"


    // $ANTLR start "entryRuleArray_Spec_Init"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:745:1: entryRuleArray_Spec_Init returns [EObject current=null] : iv_ruleArray_Spec_Init= ruleArray_Spec_Init EOF ;
    public final EObject entryRuleArray_Spec_Init() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleArray_Spec_Init = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:746:2: (iv_ruleArray_Spec_Init= ruleArray_Spec_Init EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:747:2: iv_ruleArray_Spec_Init= ruleArray_Spec_Init EOF
            {
             newCompositeNode(grammarAccess.getArray_Spec_InitRule()); 
            pushFollow(FOLLOW_ruleArray_Spec_Init_in_entryRuleArray_Spec_Init1407);
            iv_ruleArray_Spec_Init=ruleArray_Spec_Init();

            state._fsp--;

             current =iv_ruleArray_Spec_Init; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleArray_Spec_Init1417); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleArray_Spec_Init"


    // $ANTLR start "ruleArray_Spec_Init"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:754:1: ruleArray_Spec_Init returns [EObject current=null] : (this_Array_Specification_0= ruleArray_Specification (otherlv_1= ':=' ( (lv_initialization_2_0= ruleArray_Initialization ) ) )? ) ;
    public final EObject ruleArray_Spec_Init() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject this_Array_Specification_0 = null;

        EObject lv_initialization_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:757:28: ( (this_Array_Specification_0= ruleArray_Specification (otherlv_1= ':=' ( (lv_initialization_2_0= ruleArray_Initialization ) ) )? ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:758:1: (this_Array_Specification_0= ruleArray_Specification (otherlv_1= ':=' ( (lv_initialization_2_0= ruleArray_Initialization ) ) )? )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:758:1: (this_Array_Specification_0= ruleArray_Specification (otherlv_1= ':=' ( (lv_initialization_2_0= ruleArray_Initialization ) ) )? )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:759:5: this_Array_Specification_0= ruleArray_Specification (otherlv_1= ':=' ( (lv_initialization_2_0= ruleArray_Initialization ) ) )?
            {
             
                    newCompositeNode(grammarAccess.getArray_Spec_InitAccess().getArray_SpecificationParserRuleCall_0()); 
                
            pushFollow(FOLLOW_ruleArray_Specification_in_ruleArray_Spec_Init1464);
            this_Array_Specification_0=ruleArray_Specification();

            state._fsp--;

             
                    current = this_Array_Specification_0; 
                    afterParserOrEnumRuleCall();
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:767:1: (otherlv_1= ':=' ( (lv_initialization_2_0= ruleArray_Initialization ) ) )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==37) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:767:3: otherlv_1= ':=' ( (lv_initialization_2_0= ruleArray_Initialization ) )
                    {
                    otherlv_1=(Token)match(input,37,FOLLOW_37_in_ruleArray_Spec_Init1476); 

                        	newLeafNode(otherlv_1, grammarAccess.getArray_Spec_InitAccess().getColonEqualsSignKeyword_1_0());
                        
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:771:1: ( (lv_initialization_2_0= ruleArray_Initialization ) )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:772:1: (lv_initialization_2_0= ruleArray_Initialization )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:772:1: (lv_initialization_2_0= ruleArray_Initialization )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:773:3: lv_initialization_2_0= ruleArray_Initialization
                    {
                     
                    	        newCompositeNode(grammarAccess.getArray_Spec_InitAccess().getInitializationArray_InitializationParserRuleCall_1_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleArray_Initialization_in_ruleArray_Spec_Init1497);
                    lv_initialization_2_0=ruleArray_Initialization();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getArray_Spec_InitRule());
                    	        }
                           		set(
                           			current, 
                           			"initialization",
                            		lv_initialization_2_0, 
                            		"Array_Initialization");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArray_Spec_Init"


    // $ANTLR start "entryRuleArray_Specification"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:797:1: entryRuleArray_Specification returns [EObject current=null] : iv_ruleArray_Specification= ruleArray_Specification EOF ;
    public final EObject entryRuleArray_Specification() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleArray_Specification = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:798:2: (iv_ruleArray_Specification= ruleArray_Specification EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:799:2: iv_ruleArray_Specification= ruleArray_Specification EOF
            {
             newCompositeNode(grammarAccess.getArray_SpecificationRule()); 
            pushFollow(FOLLOW_ruleArray_Specification_in_entryRuleArray_Specification1535);
            iv_ruleArray_Specification=ruleArray_Specification();

            state._fsp--;

             current =iv_ruleArray_Specification; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleArray_Specification1545); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleArray_Specification"


    // $ANTLR start "ruleArray_Specification"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:806:1: ruleArray_Specification returns [EObject current=null] : (otherlv_0= 'ARRAY' otherlv_1= '[' ( (lv_subrange_2_0= ruleSubrange ) ) (otherlv_3= ',' ( (lv_subrange_4_0= ruleSubrange ) ) )* otherlv_5= ']' otherlv_6= 'OF' ( (lv_type_7_0= ruleNon_Generic_Type_Name ) ) ) ;
    public final EObject ruleArray_Specification() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        EObject lv_subrange_2_0 = null;

        EObject lv_subrange_4_0 = null;

        EObject lv_type_7_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:809:28: ( (otherlv_0= 'ARRAY' otherlv_1= '[' ( (lv_subrange_2_0= ruleSubrange ) ) (otherlv_3= ',' ( (lv_subrange_4_0= ruleSubrange ) ) )* otherlv_5= ']' otherlv_6= 'OF' ( (lv_type_7_0= ruleNon_Generic_Type_Name ) ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:810:1: (otherlv_0= 'ARRAY' otherlv_1= '[' ( (lv_subrange_2_0= ruleSubrange ) ) (otherlv_3= ',' ( (lv_subrange_4_0= ruleSubrange ) ) )* otherlv_5= ']' otherlv_6= 'OF' ( (lv_type_7_0= ruleNon_Generic_Type_Name ) ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:810:1: (otherlv_0= 'ARRAY' otherlv_1= '[' ( (lv_subrange_2_0= ruleSubrange ) ) (otherlv_3= ',' ( (lv_subrange_4_0= ruleSubrange ) ) )* otherlv_5= ']' otherlv_6= 'OF' ( (lv_type_7_0= ruleNon_Generic_Type_Name ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:810:3: otherlv_0= 'ARRAY' otherlv_1= '[' ( (lv_subrange_2_0= ruleSubrange ) ) (otherlv_3= ',' ( (lv_subrange_4_0= ruleSubrange ) ) )* otherlv_5= ']' otherlv_6= 'OF' ( (lv_type_7_0= ruleNon_Generic_Type_Name ) )
            {
            otherlv_0=(Token)match(input,38,FOLLOW_38_in_ruleArray_Specification1582); 

                	newLeafNode(otherlv_0, grammarAccess.getArray_SpecificationAccess().getARRAYKeyword_0());
                
            otherlv_1=(Token)match(input,39,FOLLOW_39_in_ruleArray_Specification1594); 

                	newLeafNode(otherlv_1, grammarAccess.getArray_SpecificationAccess().getLeftSquareBracketKeyword_1());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:818:1: ( (lv_subrange_2_0= ruleSubrange ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:819:1: (lv_subrange_2_0= ruleSubrange )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:819:1: (lv_subrange_2_0= ruleSubrange )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:820:3: lv_subrange_2_0= ruleSubrange
            {
             
            	        newCompositeNode(grammarAccess.getArray_SpecificationAccess().getSubrangeSubrangeParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleSubrange_in_ruleArray_Specification1615);
            lv_subrange_2_0=ruleSubrange();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getArray_SpecificationRule());
            	        }
                   		add(
                   			current, 
                   			"subrange",
                    		lv_subrange_2_0, 
                    		"Subrange");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:836:2: (otherlv_3= ',' ( (lv_subrange_4_0= ruleSubrange ) ) )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==35) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:836:4: otherlv_3= ',' ( (lv_subrange_4_0= ruleSubrange ) )
            	    {
            	    otherlv_3=(Token)match(input,35,FOLLOW_35_in_ruleArray_Specification1628); 

            	        	newLeafNode(otherlv_3, grammarAccess.getArray_SpecificationAccess().getCommaKeyword_3_0());
            	        
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:840:1: ( (lv_subrange_4_0= ruleSubrange ) )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:841:1: (lv_subrange_4_0= ruleSubrange )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:841:1: (lv_subrange_4_0= ruleSubrange )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:842:3: lv_subrange_4_0= ruleSubrange
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getArray_SpecificationAccess().getSubrangeSubrangeParserRuleCall_3_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleSubrange_in_ruleArray_Specification1649);
            	    lv_subrange_4_0=ruleSubrange();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getArray_SpecificationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"subrange",
            	            		lv_subrange_4_0, 
            	            		"Subrange");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);

            otherlv_5=(Token)match(input,40,FOLLOW_40_in_ruleArray_Specification1663); 

                	newLeafNode(otherlv_5, grammarAccess.getArray_SpecificationAccess().getRightSquareBracketKeyword_4());
                
            otherlv_6=(Token)match(input,41,FOLLOW_41_in_ruleArray_Specification1675); 

                	newLeafNode(otherlv_6, grammarAccess.getArray_SpecificationAccess().getOFKeyword_5());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:866:1: ( (lv_type_7_0= ruleNon_Generic_Type_Name ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:867:1: (lv_type_7_0= ruleNon_Generic_Type_Name )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:867:1: (lv_type_7_0= ruleNon_Generic_Type_Name )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:868:3: lv_type_7_0= ruleNon_Generic_Type_Name
            {
             
            	        newCompositeNode(grammarAccess.getArray_SpecificationAccess().getTypeNon_Generic_Type_NameParserRuleCall_6_0()); 
            	    
            pushFollow(FOLLOW_ruleNon_Generic_Type_Name_in_ruleArray_Specification1696);
            lv_type_7_0=ruleNon_Generic_Type_Name();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getArray_SpecificationRule());
            	        }
                   		set(
                   			current, 
                   			"type",
                    		lv_type_7_0, 
                    		"Non_Generic_Type_Name");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArray_Specification"


    // $ANTLR start "entryRuleSubrange"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:892:1: entryRuleSubrange returns [EObject current=null] : iv_ruleSubrange= ruleSubrange EOF ;
    public final EObject entryRuleSubrange() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSubrange = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:893:2: (iv_ruleSubrange= ruleSubrange EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:894:2: iv_ruleSubrange= ruleSubrange EOF
            {
             newCompositeNode(grammarAccess.getSubrangeRule()); 
            pushFollow(FOLLOW_ruleSubrange_in_entryRuleSubrange1732);
            iv_ruleSubrange=ruleSubrange();

            state._fsp--;

             current =iv_ruleSubrange; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSubrange1742); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSubrange"


    // $ANTLR start "ruleSubrange"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:901:1: ruleSubrange returns [EObject current=null] : ( ( (lv_min_0_0= ruleSignedInteger ) ) otherlv_1= '..' ( (lv_max_2_0= ruleSignedInteger ) ) ) ;
    public final EObject ruleSubrange() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_min_0_0 = null;

        AntlrDatatypeRuleToken lv_max_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:904:28: ( ( ( (lv_min_0_0= ruleSignedInteger ) ) otherlv_1= '..' ( (lv_max_2_0= ruleSignedInteger ) ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:905:1: ( ( (lv_min_0_0= ruleSignedInteger ) ) otherlv_1= '..' ( (lv_max_2_0= ruleSignedInteger ) ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:905:1: ( ( (lv_min_0_0= ruleSignedInteger ) ) otherlv_1= '..' ( (lv_max_2_0= ruleSignedInteger ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:905:2: ( (lv_min_0_0= ruleSignedInteger ) ) otherlv_1= '..' ( (lv_max_2_0= ruleSignedInteger ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:905:2: ( (lv_min_0_0= ruleSignedInteger ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:906:1: (lv_min_0_0= ruleSignedInteger )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:906:1: (lv_min_0_0= ruleSignedInteger )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:907:3: lv_min_0_0= ruleSignedInteger
            {
             
            	        newCompositeNode(grammarAccess.getSubrangeAccess().getMinSignedIntegerParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleSignedInteger_in_ruleSubrange1788);
            lv_min_0_0=ruleSignedInteger();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSubrangeRule());
            	        }
                   		set(
                   			current, 
                   			"min",
                    		lv_min_0_0, 
                    		"SignedInteger");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_1=(Token)match(input,42,FOLLOW_42_in_ruleSubrange1800); 

                	newLeafNode(otherlv_1, grammarAccess.getSubrangeAccess().getFullStopFullStopKeyword_1());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:927:1: ( (lv_max_2_0= ruleSignedInteger ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:928:1: (lv_max_2_0= ruleSignedInteger )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:928:1: (lv_max_2_0= ruleSignedInteger )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:929:3: lv_max_2_0= ruleSignedInteger
            {
             
            	        newCompositeNode(grammarAccess.getSubrangeAccess().getMaxSignedIntegerParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleSignedInteger_in_ruleSubrange1821);
            lv_max_2_0=ruleSignedInteger();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSubrangeRule());
            	        }
                   		set(
                   			current, 
                   			"max",
                    		lv_max_2_0, 
                    		"SignedInteger");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSubrange"


    // $ANTLR start "entryRuleArray_Initialization"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:953:1: entryRuleArray_Initialization returns [EObject current=null] : iv_ruleArray_Initialization= ruleArray_Initialization EOF ;
    public final EObject entryRuleArray_Initialization() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleArray_Initialization = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:954:2: (iv_ruleArray_Initialization= ruleArray_Initialization EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:955:2: iv_ruleArray_Initialization= ruleArray_Initialization EOF
            {
             newCompositeNode(grammarAccess.getArray_InitializationRule()); 
            pushFollow(FOLLOW_ruleArray_Initialization_in_entryRuleArray_Initialization1857);
            iv_ruleArray_Initialization=ruleArray_Initialization();

            state._fsp--;

             current =iv_ruleArray_Initialization; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleArray_Initialization1867); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleArray_Initialization"


    // $ANTLR start "ruleArray_Initialization"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:962:1: ruleArray_Initialization returns [EObject current=null] : (otherlv_0= '[' ( (lv_initElement_1_0= ruleArray_Initial_Elements ) ) (otherlv_2= ',' ( (lv_initElement_3_0= ruleArray_Initial_Elements ) ) )* otherlv_4= ']' ) ;
    public final EObject ruleArray_Initialization() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_initElement_1_0 = null;

        EObject lv_initElement_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:965:28: ( (otherlv_0= '[' ( (lv_initElement_1_0= ruleArray_Initial_Elements ) ) (otherlv_2= ',' ( (lv_initElement_3_0= ruleArray_Initial_Elements ) ) )* otherlv_4= ']' ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:966:1: (otherlv_0= '[' ( (lv_initElement_1_0= ruleArray_Initial_Elements ) ) (otherlv_2= ',' ( (lv_initElement_3_0= ruleArray_Initial_Elements ) ) )* otherlv_4= ']' )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:966:1: (otherlv_0= '[' ( (lv_initElement_1_0= ruleArray_Initial_Elements ) ) (otherlv_2= ',' ( (lv_initElement_3_0= ruleArray_Initial_Elements ) ) )* otherlv_4= ']' )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:966:3: otherlv_0= '[' ( (lv_initElement_1_0= ruleArray_Initial_Elements ) ) (otherlv_2= ',' ( (lv_initElement_3_0= ruleArray_Initial_Elements ) ) )* otherlv_4= ']'
            {
            otherlv_0=(Token)match(input,39,FOLLOW_39_in_ruleArray_Initialization1904); 

                	newLeafNode(otherlv_0, grammarAccess.getArray_InitializationAccess().getLeftSquareBracketKeyword_0());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:970:1: ( (lv_initElement_1_0= ruleArray_Initial_Elements ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:971:1: (lv_initElement_1_0= ruleArray_Initial_Elements )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:971:1: (lv_initElement_1_0= ruleArray_Initial_Elements )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:972:3: lv_initElement_1_0= ruleArray_Initial_Elements
            {
             
            	        newCompositeNode(grammarAccess.getArray_InitializationAccess().getInitElementArray_Initial_ElementsParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleArray_Initial_Elements_in_ruleArray_Initialization1925);
            lv_initElement_1_0=ruleArray_Initial_Elements();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getArray_InitializationRule());
            	        }
                   		add(
                   			current, 
                   			"initElement",
                    		lv_initElement_1_0, 
                    		"Array_Initial_Elements");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:988:2: (otherlv_2= ',' ( (lv_initElement_3_0= ruleArray_Initial_Elements ) ) )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==35) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:988:4: otherlv_2= ',' ( (lv_initElement_3_0= ruleArray_Initial_Elements ) )
            	    {
            	    otherlv_2=(Token)match(input,35,FOLLOW_35_in_ruleArray_Initialization1938); 

            	        	newLeafNode(otherlv_2, grammarAccess.getArray_InitializationAccess().getCommaKeyword_2_0());
            	        
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:992:1: ( (lv_initElement_3_0= ruleArray_Initial_Elements ) )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:993:1: (lv_initElement_3_0= ruleArray_Initial_Elements )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:993:1: (lv_initElement_3_0= ruleArray_Initial_Elements )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:994:3: lv_initElement_3_0= ruleArray_Initial_Elements
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getArray_InitializationAccess().getInitElementArray_Initial_ElementsParserRuleCall_2_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleArray_Initial_Elements_in_ruleArray_Initialization1959);
            	    lv_initElement_3_0=ruleArray_Initial_Elements();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getArray_InitializationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"initElement",
            	            		lv_initElement_3_0, 
            	            		"Array_Initial_Elements");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);

            otherlv_4=(Token)match(input,40,FOLLOW_40_in_ruleArray_Initialization1973); 

                	newLeafNode(otherlv_4, grammarAccess.getArray_InitializationAccess().getRightSquareBracketKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArray_Initialization"


    // $ANTLR start "entryRuleArray_Initial_Elements"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1022:1: entryRuleArray_Initial_Elements returns [EObject current=null] : iv_ruleArray_Initial_Elements= ruleArray_Initial_Elements EOF ;
    public final EObject entryRuleArray_Initial_Elements() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleArray_Initial_Elements = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1023:2: (iv_ruleArray_Initial_Elements= ruleArray_Initial_Elements EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1024:2: iv_ruleArray_Initial_Elements= ruleArray_Initial_Elements EOF
            {
             newCompositeNode(grammarAccess.getArray_Initial_ElementsRule()); 
            pushFollow(FOLLOW_ruleArray_Initial_Elements_in_entryRuleArray_Initial_Elements2009);
            iv_ruleArray_Initial_Elements=ruleArray_Initial_Elements();

            state._fsp--;

             current =iv_ruleArray_Initial_Elements; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleArray_Initial_Elements2019); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleArray_Initial_Elements"


    // $ANTLR start "ruleArray_Initial_Elements"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1031:1: ruleArray_Initial_Elements returns [EObject current=null] : (this_Constant_0= ruleConstant | ( ( (lv_num_1_0= RULE_INT ) ) otherlv_2= '(' ( (lv_arrayElement_3_0= ruleConstant ) )? otherlv_4= ')' ) ) ;
    public final EObject ruleArray_Initial_Elements() throws RecognitionException {
        EObject current = null;

        Token lv_num_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject this_Constant_0 = null;

        EObject lv_arrayElement_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1034:28: ( (this_Constant_0= ruleConstant | ( ( (lv_num_1_0= RULE_INT ) ) otherlv_2= '(' ( (lv_arrayElement_3_0= ruleConstant ) )? otherlv_4= ')' ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1035:1: (this_Constant_0= ruleConstant | ( ( (lv_num_1_0= RULE_INT ) ) otherlv_2= '(' ( (lv_arrayElement_3_0= ruleConstant ) )? otherlv_4= ')' ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1035:1: (this_Constant_0= ruleConstant | ( ( (lv_num_1_0= RULE_INT ) ) otherlv_2= '(' ( (lv_arrayElement_3_0= ruleConstant ) )? otherlv_4= ')' ) )
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( ((LA24_0>=RULE_BINARY_INTEGER_VALUE && LA24_0<=RULE_D_BYTE_CHAR_STRING)||(LA24_0>=76 && LA24_0<=77)||(LA24_0>=84 && LA24_0<=97)||(LA24_0>=100 && LA24_0<=114)) ) {
                alt24=1;
            }
            else if ( (LA24_0==RULE_INT) ) {
                int LA24_2 = input.LA(2);

                if ( (LA24_2==EOF||LA24_2==35||LA24_2==40) ) {
                    alt24=1;
                }
                else if ( (LA24_2==34) ) {
                    alt24=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 24, 2, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;
            }
            switch (alt24) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1036:5: this_Constant_0= ruleConstant
                    {
                     
                            newCompositeNode(grammarAccess.getArray_Initial_ElementsAccess().getConstantParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleConstant_in_ruleArray_Initial_Elements2066);
                    this_Constant_0=ruleConstant();

                    state._fsp--;

                     
                            current = this_Constant_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1045:6: ( ( (lv_num_1_0= RULE_INT ) ) otherlv_2= '(' ( (lv_arrayElement_3_0= ruleConstant ) )? otherlv_4= ')' )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1045:6: ( ( (lv_num_1_0= RULE_INT ) ) otherlv_2= '(' ( (lv_arrayElement_3_0= ruleConstant ) )? otherlv_4= ')' )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1045:7: ( (lv_num_1_0= RULE_INT ) ) otherlv_2= '(' ( (lv_arrayElement_3_0= ruleConstant ) )? otherlv_4= ')'
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1045:7: ( (lv_num_1_0= RULE_INT ) )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1046:1: (lv_num_1_0= RULE_INT )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1046:1: (lv_num_1_0= RULE_INT )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1047:3: lv_num_1_0= RULE_INT
                    {
                    lv_num_1_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleArray_Initial_Elements2089); 

                    			newLeafNode(lv_num_1_0, grammarAccess.getArray_Initial_ElementsAccess().getNumINTTerminalRuleCall_1_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getArray_Initial_ElementsRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"num",
                            		lv_num_1_0, 
                            		"INT");
                    	    

                    }


                    }

                    otherlv_2=(Token)match(input,34,FOLLOW_34_in_ruleArray_Initial_Elements2106); 

                        	newLeafNode(otherlv_2, grammarAccess.getArray_Initial_ElementsAccess().getLeftParenthesisKeyword_1_1());
                        
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1067:1: ( (lv_arrayElement_3_0= ruleConstant ) )?
                    int alt23=2;
                    int LA23_0 = input.LA(1);

                    if ( ((LA23_0>=RULE_INT && LA23_0<=RULE_D_BYTE_CHAR_STRING)||(LA23_0>=76 && LA23_0<=77)||(LA23_0>=84 && LA23_0<=97)||(LA23_0>=100 && LA23_0<=114)) ) {
                        alt23=1;
                    }
                    switch (alt23) {
                        case 1 :
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1068:1: (lv_arrayElement_3_0= ruleConstant )
                            {
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1068:1: (lv_arrayElement_3_0= ruleConstant )
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1069:3: lv_arrayElement_3_0= ruleConstant
                            {
                             
                            	        newCompositeNode(grammarAccess.getArray_Initial_ElementsAccess().getArrayElementConstantParserRuleCall_1_2_0()); 
                            	    
                            pushFollow(FOLLOW_ruleConstant_in_ruleArray_Initial_Elements2127);
                            lv_arrayElement_3_0=ruleConstant();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getArray_Initial_ElementsRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"arrayElement",
                                    		lv_arrayElement_3_0, 
                                    		"Constant");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }
                            break;

                    }

                    otherlv_4=(Token)match(input,36,FOLLOW_36_in_ruleArray_Initial_Elements2140); 

                        	newLeafNode(otherlv_4, grammarAccess.getArray_Initial_ElementsAccess().getRightParenthesisKeyword_1_3());
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArray_Initial_Elements"


    // $ANTLR start "entryRuleS_Byte_String_Spec"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1099:1: entryRuleS_Byte_String_Spec returns [EObject current=null] : iv_ruleS_Byte_String_Spec= ruleS_Byte_String_Spec EOF ;
    public final EObject entryRuleS_Byte_String_Spec() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleS_Byte_String_Spec = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1100:2: (iv_ruleS_Byte_String_Spec= ruleS_Byte_String_Spec EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1101:2: iv_ruleS_Byte_String_Spec= ruleS_Byte_String_Spec EOF
            {
             newCompositeNode(grammarAccess.getS_Byte_String_SpecRule()); 
            pushFollow(FOLLOW_ruleS_Byte_String_Spec_in_entryRuleS_Byte_String_Spec2179);
            iv_ruleS_Byte_String_Spec=ruleS_Byte_String_Spec();

            state._fsp--;

             current =iv_ruleS_Byte_String_Spec; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleS_Byte_String_Spec2189); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleS_Byte_String_Spec"


    // $ANTLR start "ruleS_Byte_String_Spec"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1108:1: ruleS_Byte_String_Spec returns [EObject current=null] : ( ( (lv_type_0_0= 'STRING' ) ) (otherlv_1= '[' ( (lv_size_2_0= RULE_INT ) ) otherlv_3= ']' )? (otherlv_4= ':=' ( (lv_string_5_0= ruleSingle_Byte_Character_String_Literal ) ) )? ) ;
    public final EObject ruleS_Byte_String_Spec() throws RecognitionException {
        EObject current = null;

        Token lv_type_0_0=null;
        Token otherlv_1=null;
        Token lv_size_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        EObject lv_string_5_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1111:28: ( ( ( (lv_type_0_0= 'STRING' ) ) (otherlv_1= '[' ( (lv_size_2_0= RULE_INT ) ) otherlv_3= ']' )? (otherlv_4= ':=' ( (lv_string_5_0= ruleSingle_Byte_Character_String_Literal ) ) )? ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1112:1: ( ( (lv_type_0_0= 'STRING' ) ) (otherlv_1= '[' ( (lv_size_2_0= RULE_INT ) ) otherlv_3= ']' )? (otherlv_4= ':=' ( (lv_string_5_0= ruleSingle_Byte_Character_String_Literal ) ) )? )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1112:1: ( ( (lv_type_0_0= 'STRING' ) ) (otherlv_1= '[' ( (lv_size_2_0= RULE_INT ) ) otherlv_3= ']' )? (otherlv_4= ':=' ( (lv_string_5_0= ruleSingle_Byte_Character_String_Literal ) ) )? )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1112:2: ( (lv_type_0_0= 'STRING' ) ) (otherlv_1= '[' ( (lv_size_2_0= RULE_INT ) ) otherlv_3= ']' )? (otherlv_4= ':=' ( (lv_string_5_0= ruleSingle_Byte_Character_String_Literal ) ) )?
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1112:2: ( (lv_type_0_0= 'STRING' ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1113:1: (lv_type_0_0= 'STRING' )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1113:1: (lv_type_0_0= 'STRING' )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1114:3: lv_type_0_0= 'STRING'
            {
            lv_type_0_0=(Token)match(input,43,FOLLOW_43_in_ruleS_Byte_String_Spec2232); 

                    newLeafNode(lv_type_0_0, grammarAccess.getS_Byte_String_SpecAccess().getTypeSTRINGKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getS_Byte_String_SpecRule());
            	        }
                   		setWithLastConsumed(current, "type", lv_type_0_0, "STRING");
            	    

            }


            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1127:2: (otherlv_1= '[' ( (lv_size_2_0= RULE_INT ) ) otherlv_3= ']' )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==39) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1127:4: otherlv_1= '[' ( (lv_size_2_0= RULE_INT ) ) otherlv_3= ']'
                    {
                    otherlv_1=(Token)match(input,39,FOLLOW_39_in_ruleS_Byte_String_Spec2258); 

                        	newLeafNode(otherlv_1, grammarAccess.getS_Byte_String_SpecAccess().getLeftSquareBracketKeyword_1_0());
                        
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1131:1: ( (lv_size_2_0= RULE_INT ) )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1132:1: (lv_size_2_0= RULE_INT )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1132:1: (lv_size_2_0= RULE_INT )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1133:3: lv_size_2_0= RULE_INT
                    {
                    lv_size_2_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleS_Byte_String_Spec2275); 

                    			newLeafNode(lv_size_2_0, grammarAccess.getS_Byte_String_SpecAccess().getSizeINTTerminalRuleCall_1_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getS_Byte_String_SpecRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"size",
                            		lv_size_2_0, 
                            		"INT");
                    	    

                    }


                    }

                    otherlv_3=(Token)match(input,40,FOLLOW_40_in_ruleS_Byte_String_Spec2292); 

                        	newLeafNode(otherlv_3, grammarAccess.getS_Byte_String_SpecAccess().getRightSquareBracketKeyword_1_2());
                        

                    }
                    break;

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1153:3: (otherlv_4= ':=' ( (lv_string_5_0= ruleSingle_Byte_Character_String_Literal ) ) )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==37) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1153:5: otherlv_4= ':=' ( (lv_string_5_0= ruleSingle_Byte_Character_String_Literal ) )
                    {
                    otherlv_4=(Token)match(input,37,FOLLOW_37_in_ruleS_Byte_String_Spec2307); 

                        	newLeafNode(otherlv_4, grammarAccess.getS_Byte_String_SpecAccess().getColonEqualsSignKeyword_2_0());
                        
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1157:1: ( (lv_string_5_0= ruleSingle_Byte_Character_String_Literal ) )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1158:1: (lv_string_5_0= ruleSingle_Byte_Character_String_Literal )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1158:1: (lv_string_5_0= ruleSingle_Byte_Character_String_Literal )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1159:3: lv_string_5_0= ruleSingle_Byte_Character_String_Literal
                    {
                     
                    	        newCompositeNode(grammarAccess.getS_Byte_String_SpecAccess().getStringSingle_Byte_Character_String_LiteralParserRuleCall_2_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleSingle_Byte_Character_String_Literal_in_ruleS_Byte_String_Spec2328);
                    lv_string_5_0=ruleSingle_Byte_Character_String_Literal();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getS_Byte_String_SpecRule());
                    	        }
                           		set(
                           			current, 
                           			"string",
                            		lv_string_5_0, 
                            		"Single_Byte_Character_String_Literal");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleS_Byte_String_Spec"


    // $ANTLR start "entryRuleD_Byte_String_Spec"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1183:1: entryRuleD_Byte_String_Spec returns [EObject current=null] : iv_ruleD_Byte_String_Spec= ruleD_Byte_String_Spec EOF ;
    public final EObject entryRuleD_Byte_String_Spec() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleD_Byte_String_Spec = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1184:2: (iv_ruleD_Byte_String_Spec= ruleD_Byte_String_Spec EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1185:2: iv_ruleD_Byte_String_Spec= ruleD_Byte_String_Spec EOF
            {
             newCompositeNode(grammarAccess.getD_Byte_String_SpecRule()); 
            pushFollow(FOLLOW_ruleD_Byte_String_Spec_in_entryRuleD_Byte_String_Spec2366);
            iv_ruleD_Byte_String_Spec=ruleD_Byte_String_Spec();

            state._fsp--;

             current =iv_ruleD_Byte_String_Spec; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleD_Byte_String_Spec2376); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleD_Byte_String_Spec"


    // $ANTLR start "ruleD_Byte_String_Spec"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1192:1: ruleD_Byte_String_Spec returns [EObject current=null] : ( ( (lv_type_0_0= 'WSTRING' ) ) (otherlv_1= '[' ( (lv_size_2_0= RULE_INT ) ) otherlv_3= ']' )? (otherlv_4= ':=' ( (lv_string_5_0= ruleDouble_Byte_Character_String_Literal ) ) )? ) ;
    public final EObject ruleD_Byte_String_Spec() throws RecognitionException {
        EObject current = null;

        Token lv_type_0_0=null;
        Token otherlv_1=null;
        Token lv_size_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        EObject lv_string_5_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1195:28: ( ( ( (lv_type_0_0= 'WSTRING' ) ) (otherlv_1= '[' ( (lv_size_2_0= RULE_INT ) ) otherlv_3= ']' )? (otherlv_4= ':=' ( (lv_string_5_0= ruleDouble_Byte_Character_String_Literal ) ) )? ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1196:1: ( ( (lv_type_0_0= 'WSTRING' ) ) (otherlv_1= '[' ( (lv_size_2_0= RULE_INT ) ) otherlv_3= ']' )? (otherlv_4= ':=' ( (lv_string_5_0= ruleDouble_Byte_Character_String_Literal ) ) )? )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1196:1: ( ( (lv_type_0_0= 'WSTRING' ) ) (otherlv_1= '[' ( (lv_size_2_0= RULE_INT ) ) otherlv_3= ']' )? (otherlv_4= ':=' ( (lv_string_5_0= ruleDouble_Byte_Character_String_Literal ) ) )? )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1196:2: ( (lv_type_0_0= 'WSTRING' ) ) (otherlv_1= '[' ( (lv_size_2_0= RULE_INT ) ) otherlv_3= ']' )? (otherlv_4= ':=' ( (lv_string_5_0= ruleDouble_Byte_Character_String_Literal ) ) )?
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1196:2: ( (lv_type_0_0= 'WSTRING' ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1197:1: (lv_type_0_0= 'WSTRING' )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1197:1: (lv_type_0_0= 'WSTRING' )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1198:3: lv_type_0_0= 'WSTRING'
            {
            lv_type_0_0=(Token)match(input,44,FOLLOW_44_in_ruleD_Byte_String_Spec2419); 

                    newLeafNode(lv_type_0_0, grammarAccess.getD_Byte_String_SpecAccess().getTypeWSTRINGKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getD_Byte_String_SpecRule());
            	        }
                   		setWithLastConsumed(current, "type", lv_type_0_0, "WSTRING");
            	    

            }


            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1211:2: (otherlv_1= '[' ( (lv_size_2_0= RULE_INT ) ) otherlv_3= ']' )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==39) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1211:4: otherlv_1= '[' ( (lv_size_2_0= RULE_INT ) ) otherlv_3= ']'
                    {
                    otherlv_1=(Token)match(input,39,FOLLOW_39_in_ruleD_Byte_String_Spec2445); 

                        	newLeafNode(otherlv_1, grammarAccess.getD_Byte_String_SpecAccess().getLeftSquareBracketKeyword_1_0());
                        
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1215:1: ( (lv_size_2_0= RULE_INT ) )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1216:1: (lv_size_2_0= RULE_INT )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1216:1: (lv_size_2_0= RULE_INT )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1217:3: lv_size_2_0= RULE_INT
                    {
                    lv_size_2_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleD_Byte_String_Spec2462); 

                    			newLeafNode(lv_size_2_0, grammarAccess.getD_Byte_String_SpecAccess().getSizeINTTerminalRuleCall_1_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getD_Byte_String_SpecRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"size",
                            		lv_size_2_0, 
                            		"INT");
                    	    

                    }


                    }

                    otherlv_3=(Token)match(input,40,FOLLOW_40_in_ruleD_Byte_String_Spec2479); 

                        	newLeafNode(otherlv_3, grammarAccess.getD_Byte_String_SpecAccess().getRightSquareBracketKeyword_1_2());
                        

                    }
                    break;

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1237:3: (otherlv_4= ':=' ( (lv_string_5_0= ruleDouble_Byte_Character_String_Literal ) ) )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==37) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1237:5: otherlv_4= ':=' ( (lv_string_5_0= ruleDouble_Byte_Character_String_Literal ) )
                    {
                    otherlv_4=(Token)match(input,37,FOLLOW_37_in_ruleD_Byte_String_Spec2494); 

                        	newLeafNode(otherlv_4, grammarAccess.getD_Byte_String_SpecAccess().getColonEqualsSignKeyword_2_0());
                        
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1241:1: ( (lv_string_5_0= ruleDouble_Byte_Character_String_Literal ) )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1242:1: (lv_string_5_0= ruleDouble_Byte_Character_String_Literal )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1242:1: (lv_string_5_0= ruleDouble_Byte_Character_String_Literal )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1243:3: lv_string_5_0= ruleDouble_Byte_Character_String_Literal
                    {
                     
                    	        newCompositeNode(grammarAccess.getD_Byte_String_SpecAccess().getStringDouble_Byte_Character_String_LiteralParserRuleCall_2_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleDouble_Byte_Character_String_Literal_in_ruleD_Byte_String_Spec2515);
                    lv_string_5_0=ruleDouble_Byte_Character_String_Literal();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getD_Byte_String_SpecRule());
                    	        }
                           		set(
                           			current, 
                           			"string",
                            		lv_string_5_0, 
                            		"Double_Byte_Character_String_Literal");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleD_Byte_String_Spec"


    // $ANTLR start "entryRuleStatementList"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1267:1: entryRuleStatementList returns [EObject current=null] : iv_ruleStatementList= ruleStatementList EOF ;
    public final EObject entryRuleStatementList() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStatementList = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1268:2: (iv_ruleStatementList= ruleStatementList EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1269:2: iv_ruleStatementList= ruleStatementList EOF
            {
             newCompositeNode(grammarAccess.getStatementListRule()); 
            pushFollow(FOLLOW_ruleStatementList_in_entryRuleStatementList2553);
            iv_ruleStatementList=ruleStatementList();

            state._fsp--;

             current =iv_ruleStatementList; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleStatementList2563); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStatementList"


    // $ANTLR start "ruleStatementList"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1276:1: ruleStatementList returns [EObject current=null] : ( () ( ( (lv_statements_1_0= ruleStatement ) )? otherlv_2= ';' )* ) ;
    public final EObject ruleStatementList() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject lv_statements_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1279:28: ( ( () ( ( (lv_statements_1_0= ruleStatement ) )? otherlv_2= ';' )* ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1280:1: ( () ( ( (lv_statements_1_0= ruleStatement ) )? otherlv_2= ';' )* )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1280:1: ( () ( ( (lv_statements_1_0= ruleStatement ) )? otherlv_2= ';' )* )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1280:2: () ( ( (lv_statements_1_0= ruleStatement ) )? otherlv_2= ';' )*
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1280:2: ()
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1281:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getStatementListAccess().getStatementListAction_0(),
                        current);
                

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1286:2: ( ( (lv_statements_1_0= ruleStatement ) )? otherlv_2= ';' )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==RULE_ID||LA30_0==31||LA30_0==46||LA30_0==51||(LA30_0>=53 && LA30_0<=56)||LA30_0==61||LA30_0==64) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1286:3: ( (lv_statements_1_0= ruleStatement ) )? otherlv_2= ';'
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1286:3: ( (lv_statements_1_0= ruleStatement ) )?
            	    int alt29=2;
            	    int LA29_0 = input.LA(1);

            	    if ( (LA29_0==RULE_ID||LA29_0==46||LA29_0==51||(LA29_0>=53 && LA29_0<=56)||LA29_0==61||LA29_0==64) ) {
            	        alt29=1;
            	    }
            	    switch (alt29) {
            	        case 1 :
            	            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1287:1: (lv_statements_1_0= ruleStatement )
            	            {
            	            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1287:1: (lv_statements_1_0= ruleStatement )
            	            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1288:3: lv_statements_1_0= ruleStatement
            	            {
            	             
            	            	        newCompositeNode(grammarAccess.getStatementListAccess().getStatementsStatementParserRuleCall_1_0_0()); 
            	            	    
            	            pushFollow(FOLLOW_ruleStatement_in_ruleStatementList2619);
            	            lv_statements_1_0=ruleStatement();

            	            state._fsp--;


            	            	        if (current==null) {
            	            	            current = createModelElementForParent(grammarAccess.getStatementListRule());
            	            	        }
            	                   		add(
            	                   			current, 
            	                   			"statements",
            	                    		lv_statements_1_0, 
            	                    		"Statement");
            	            	        afterParserOrEnumRuleCall();
            	            	    

            	            }


            	            }
            	            break;

            	    }

            	    otherlv_2=(Token)match(input,31,FOLLOW_31_in_ruleStatementList2632); 

            	        	newLeafNode(otherlv_2, grammarAccess.getStatementListAccess().getSemicolonKeyword_1_1());
            	        

            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStatementList"


    // $ANTLR start "entryRuleStatement"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1316:1: entryRuleStatement returns [EObject current=null] : iv_ruleStatement= ruleStatement EOF ;
    public final EObject entryRuleStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStatement = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1317:2: (iv_ruleStatement= ruleStatement EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1318:2: iv_ruleStatement= ruleStatement EOF
            {
             newCompositeNode(grammarAccess.getStatementRule()); 
            pushFollow(FOLLOW_ruleStatement_in_entryRuleStatement2670);
            iv_ruleStatement=ruleStatement();

            state._fsp--;

             current =iv_ruleStatement; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleStatement2680); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStatement"


    // $ANTLR start "ruleStatement"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1325:1: ruleStatement returns [EObject current=null] : (this_Assignment_Statement_0= ruleAssignment_Statement | this_Selection_Statement_1= ruleSelection_Statement | this_IterationControl_Stmt_2= ruleIterationControl_Stmt | this_For_Statement_3= ruleFor_Statement | this_Repeat_Statement_4= ruleRepeat_Statement | this_While_Statement_5= ruleWhile_Statement | this_Subprog_Ctrl_Stmt_6= ruleSubprog_Ctrl_Stmt ) ;
    public final EObject ruleStatement() throws RecognitionException {
        EObject current = null;

        EObject this_Assignment_Statement_0 = null;

        EObject this_Selection_Statement_1 = null;

        EObject this_IterationControl_Stmt_2 = null;

        EObject this_For_Statement_3 = null;

        EObject this_Repeat_Statement_4 = null;

        EObject this_While_Statement_5 = null;

        EObject this_Subprog_Ctrl_Stmt_6 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1328:28: ( (this_Assignment_Statement_0= ruleAssignment_Statement | this_Selection_Statement_1= ruleSelection_Statement | this_IterationControl_Stmt_2= ruleIterationControl_Stmt | this_For_Statement_3= ruleFor_Statement | this_Repeat_Statement_4= ruleRepeat_Statement | this_While_Statement_5= ruleWhile_Statement | this_Subprog_Ctrl_Stmt_6= ruleSubprog_Ctrl_Stmt ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1329:1: (this_Assignment_Statement_0= ruleAssignment_Statement | this_Selection_Statement_1= ruleSelection_Statement | this_IterationControl_Stmt_2= ruleIterationControl_Stmt | this_For_Statement_3= ruleFor_Statement | this_Repeat_Statement_4= ruleRepeat_Statement | this_While_Statement_5= ruleWhile_Statement | this_Subprog_Ctrl_Stmt_6= ruleSubprog_Ctrl_Stmt )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1329:1: (this_Assignment_Statement_0= ruleAssignment_Statement | this_Selection_Statement_1= ruleSelection_Statement | this_IterationControl_Stmt_2= ruleIterationControl_Stmt | this_For_Statement_3= ruleFor_Statement | this_Repeat_Statement_4= ruleRepeat_Statement | this_While_Statement_5= ruleWhile_Statement | this_Subprog_Ctrl_Stmt_6= ruleSubprog_Ctrl_Stmt )
            int alt31=7;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                int LA31_1 = input.LA(2);

                if ( (LA31_1==34) ) {
                    alt31=7;
                }
                else if ( (LA31_1==37||LA31_1==39||LA31_1==45) ) {
                    alt31=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 31, 1, input);

                    throw nvae;
                }
                }
                break;
            case 46:
            case 51:
                {
                alt31=2;
                }
                break;
            case 53:
            case 54:
            case 55:
                {
                alt31=3;
                }
                break;
            case 56:
                {
                alt31=4;
                }
                break;
            case 61:
                {
                alt31=5;
                }
                break;
            case 64:
                {
                alt31=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 31, 0, input);

                throw nvae;
            }

            switch (alt31) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1330:5: this_Assignment_Statement_0= ruleAssignment_Statement
                    {
                     
                            newCompositeNode(grammarAccess.getStatementAccess().getAssignment_StatementParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleAssignment_Statement_in_ruleStatement2727);
                    this_Assignment_Statement_0=ruleAssignment_Statement();

                    state._fsp--;

                     
                            current = this_Assignment_Statement_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1340:5: this_Selection_Statement_1= ruleSelection_Statement
                    {
                     
                            newCompositeNode(grammarAccess.getStatementAccess().getSelection_StatementParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleSelection_Statement_in_ruleStatement2754);
                    this_Selection_Statement_1=ruleSelection_Statement();

                    state._fsp--;

                     
                            current = this_Selection_Statement_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1350:5: this_IterationControl_Stmt_2= ruleIterationControl_Stmt
                    {
                     
                            newCompositeNode(grammarAccess.getStatementAccess().getIterationControl_StmtParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_ruleIterationControl_Stmt_in_ruleStatement2781);
                    this_IterationControl_Stmt_2=ruleIterationControl_Stmt();

                    state._fsp--;

                     
                            current = this_IterationControl_Stmt_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1360:5: this_For_Statement_3= ruleFor_Statement
                    {
                     
                            newCompositeNode(grammarAccess.getStatementAccess().getFor_StatementParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_ruleFor_Statement_in_ruleStatement2808);
                    this_For_Statement_3=ruleFor_Statement();

                    state._fsp--;

                     
                            current = this_For_Statement_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 5 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1370:5: this_Repeat_Statement_4= ruleRepeat_Statement
                    {
                     
                            newCompositeNode(grammarAccess.getStatementAccess().getRepeat_StatementParserRuleCall_4()); 
                        
                    pushFollow(FOLLOW_ruleRepeat_Statement_in_ruleStatement2835);
                    this_Repeat_Statement_4=ruleRepeat_Statement();

                    state._fsp--;

                     
                            current = this_Repeat_Statement_4; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 6 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1380:5: this_While_Statement_5= ruleWhile_Statement
                    {
                     
                            newCompositeNode(grammarAccess.getStatementAccess().getWhile_StatementParserRuleCall_5()); 
                        
                    pushFollow(FOLLOW_ruleWhile_Statement_in_ruleStatement2862);
                    this_While_Statement_5=ruleWhile_Statement();

                    state._fsp--;

                     
                            current = this_While_Statement_5; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 7 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1390:5: this_Subprog_Ctrl_Stmt_6= ruleSubprog_Ctrl_Stmt
                    {
                     
                            newCompositeNode(grammarAccess.getStatementAccess().getSubprog_Ctrl_StmtParserRuleCall_6()); 
                        
                    pushFollow(FOLLOW_ruleSubprog_Ctrl_Stmt_in_ruleStatement2889);
                    this_Subprog_Ctrl_Stmt_6=ruleSubprog_Ctrl_Stmt();

                    state._fsp--;

                     
                            current = this_Subprog_Ctrl_Stmt_6; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStatement"


    // $ANTLR start "entryRuleAssignment_Statement"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1406:1: entryRuleAssignment_Statement returns [EObject current=null] : iv_ruleAssignment_Statement= ruleAssignment_Statement EOF ;
    public final EObject entryRuleAssignment_Statement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssignment_Statement = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1407:2: (iv_ruleAssignment_Statement= ruleAssignment_Statement EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1408:2: iv_ruleAssignment_Statement= ruleAssignment_Statement EOF
            {
             newCompositeNode(grammarAccess.getAssignment_StatementRule()); 
            pushFollow(FOLLOW_ruleAssignment_Statement_in_entryRuleAssignment_Statement2924);
            iv_ruleAssignment_Statement=ruleAssignment_Statement();

            state._fsp--;

             current =iv_ruleAssignment_Statement; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAssignment_Statement2934); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssignment_Statement"


    // $ANTLR start "ruleAssignment_Statement"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1415:1: ruleAssignment_Statement returns [EObject current=null] : ( ( (lv_variable_0_0= ruleVariable ) ) otherlv_1= ':=' ( (lv_expression_2_0= ruleExpression ) ) ) ;
    public final EObject ruleAssignment_Statement() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_variable_0_0 = null;

        EObject lv_expression_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1418:28: ( ( ( (lv_variable_0_0= ruleVariable ) ) otherlv_1= ':=' ( (lv_expression_2_0= ruleExpression ) ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1419:1: ( ( (lv_variable_0_0= ruleVariable ) ) otherlv_1= ':=' ( (lv_expression_2_0= ruleExpression ) ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1419:1: ( ( (lv_variable_0_0= ruleVariable ) ) otherlv_1= ':=' ( (lv_expression_2_0= ruleExpression ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1419:2: ( (lv_variable_0_0= ruleVariable ) ) otherlv_1= ':=' ( (lv_expression_2_0= ruleExpression ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1419:2: ( (lv_variable_0_0= ruleVariable ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1420:1: (lv_variable_0_0= ruleVariable )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1420:1: (lv_variable_0_0= ruleVariable )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1421:3: lv_variable_0_0= ruleVariable
            {
             
            	        newCompositeNode(grammarAccess.getAssignment_StatementAccess().getVariableVariableParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleVariable_in_ruleAssignment_Statement2980);
            lv_variable_0_0=ruleVariable();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAssignment_StatementRule());
            	        }
                   		set(
                   			current, 
                   			"variable",
                    		lv_variable_0_0, 
                    		"Variable");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_1=(Token)match(input,37,FOLLOW_37_in_ruleAssignment_Statement2992); 

                	newLeafNode(otherlv_1, grammarAccess.getAssignment_StatementAccess().getColonEqualsSignKeyword_1());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1441:1: ( (lv_expression_2_0= ruleExpression ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1442:1: (lv_expression_2_0= ruleExpression )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1442:1: (lv_expression_2_0= ruleExpression )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1443:3: lv_expression_2_0= ruleExpression
            {
             
            	        newCompositeNode(grammarAccess.getAssignment_StatementAccess().getExpressionExpressionParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleExpression_in_ruleAssignment_Statement3013);
            lv_expression_2_0=ruleExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAssignment_StatementRule());
            	        }
                   		set(
                   			current, 
                   			"expression",
                    		lv_expression_2_0, 
                    		"Expression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssignment_Statement"


    // $ANTLR start "entryRuleVariable"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1467:1: entryRuleVariable returns [EObject current=null] : iv_ruleVariable= ruleVariable EOF ;
    public final EObject entryRuleVariable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariable = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1468:2: (iv_ruleVariable= ruleVariable EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1469:2: iv_ruleVariable= ruleVariable EOF
            {
             newCompositeNode(grammarAccess.getVariableRule()); 
            pushFollow(FOLLOW_ruleVariable_in_entryRuleVariable3049);
            iv_ruleVariable=ruleVariable();

            state._fsp--;

             current =iv_ruleVariable; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVariable3059); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1476:1: ruleVariable returns [EObject current=null] : (this_Var_Variable_0= ruleVar_Variable | this_Array_Variable_1= ruleArray_Variable | this_Adapter_Variable_2= ruleAdapter_Variable ) ;
    public final EObject ruleVariable() throws RecognitionException {
        EObject current = null;

        EObject this_Var_Variable_0 = null;

        EObject this_Array_Variable_1 = null;

        EObject this_Adapter_Variable_2 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1479:28: ( (this_Var_Variable_0= ruleVar_Variable | this_Array_Variable_1= ruleArray_Variable | this_Adapter_Variable_2= ruleAdapter_Variable ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1480:1: (this_Var_Variable_0= ruleVar_Variable | this_Array_Variable_1= ruleArray_Variable | this_Adapter_Variable_2= ruleAdapter_Variable )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1480:1: (this_Var_Variable_0= ruleVar_Variable | this_Array_Variable_1= ruleArray_Variable | this_Adapter_Variable_2= ruleAdapter_Variable )
            int alt32=3;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==RULE_ID) ) {
                switch ( input.LA(2) ) {
                case 45:
                    {
                    alt32=3;
                    }
                    break;
                case 39:
                    {
                    alt32=2;
                    }
                    break;
                case EOF:
                case 31:
                case 35:
                case 36:
                case 37:
                case 40:
                case 41:
                case 47:
                case 57:
                case 58:
                case 59:
                case 63:
                case 66:
                case 67:
                case 68:
                case 69:
                case 70:
                case 71:
                case 72:
                case 73:
                case 74:
                case 75:
                case 76:
                case 77:
                case 78:
                case 79:
                case 80:
                case 81:
                    {
                    alt32=1;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 32, 1, input);

                    throw nvae;
                }

            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 32, 0, input);

                throw nvae;
            }
            switch (alt32) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1481:5: this_Var_Variable_0= ruleVar_Variable
                    {
                     
                            newCompositeNode(grammarAccess.getVariableAccess().getVar_VariableParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleVar_Variable_in_ruleVariable3106);
                    this_Var_Variable_0=ruleVar_Variable();

                    state._fsp--;

                     
                            current = this_Var_Variable_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1491:5: this_Array_Variable_1= ruleArray_Variable
                    {
                     
                            newCompositeNode(grammarAccess.getVariableAccess().getArray_VariableParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleArray_Variable_in_ruleVariable3133);
                    this_Array_Variable_1=ruleArray_Variable();

                    state._fsp--;

                     
                            current = this_Array_Variable_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1501:5: this_Adapter_Variable_2= ruleAdapter_Variable
                    {
                     
                            newCompositeNode(grammarAccess.getVariableAccess().getAdapter_VariableParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_ruleAdapter_Variable_in_ruleVariable3160);
                    this_Adapter_Variable_2=ruleAdapter_Variable();

                    state._fsp--;

                     
                            current = this_Adapter_Variable_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRuleVar_Variable"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1517:1: entryRuleVar_Variable returns [EObject current=null] : iv_ruleVar_Variable= ruleVar_Variable EOF ;
    public final EObject entryRuleVar_Variable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVar_Variable = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1518:2: (iv_ruleVar_Variable= ruleVar_Variable EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1519:2: iv_ruleVar_Variable= ruleVar_Variable EOF
            {
             newCompositeNode(grammarAccess.getVar_VariableRule()); 
            pushFollow(FOLLOW_ruleVar_Variable_in_entryRuleVar_Variable3195);
            iv_ruleVar_Variable=ruleVar_Variable();

            state._fsp--;

             current =iv_ruleVar_Variable; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVar_Variable3205); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVar_Variable"


    // $ANTLR start "ruleVar_Variable"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1526:1: ruleVar_Variable returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleVar_Variable() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1529:28: ( ( (otherlv_0= RULE_ID ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1530:1: ( (otherlv_0= RULE_ID ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1530:1: ( (otherlv_0= RULE_ID ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1531:1: (otherlv_0= RULE_ID )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1531:1: (otherlv_0= RULE_ID )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1532:3: otherlv_0= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getVar_VariableRule());
            	        }
                    
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleVar_Variable3249); 

            		newLeafNode(otherlv_0, grammarAccess.getVar_VariableAccess().getVarVarDeclarationCrossReference_0()); 
            	

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVar_Variable"


    // $ANTLR start "entryRuleAdapter_Variable"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1551:1: entryRuleAdapter_Variable returns [EObject current=null] : iv_ruleAdapter_Variable= ruleAdapter_Variable EOF ;
    public final EObject entryRuleAdapter_Variable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAdapter_Variable = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1552:2: (iv_ruleAdapter_Variable= ruleAdapter_Variable EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1553:2: iv_ruleAdapter_Variable= ruleAdapter_Variable EOF
            {
             newCompositeNode(grammarAccess.getAdapter_VariableRule()); 
            pushFollow(FOLLOW_ruleAdapter_Variable_in_entryRuleAdapter_Variable3284);
            iv_ruleAdapter_Variable=ruleAdapter_Variable();

            state._fsp--;

             current =iv_ruleAdapter_Variable; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAdapter_Variable3294); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAdapter_Variable"


    // $ANTLR start "ruleAdapter_Variable"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1560:1: ruleAdapter_Variable returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( (otherlv_2= RULE_ID ) ) ) ;
    public final EObject ruleAdapter_Variable() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1563:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( (otherlv_2= RULE_ID ) ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1564:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( (otherlv_2= RULE_ID ) ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1564:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( (otherlv_2= RULE_ID ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1564:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( (otherlv_2= RULE_ID ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1564:2: ( (otherlv_0= RULE_ID ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1565:1: (otherlv_0= RULE_ID )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1565:1: (otherlv_0= RULE_ID )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1566:3: otherlv_0= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getAdapter_VariableRule());
            	        }
                    
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleAdapter_Variable3339); 

            		newLeafNode(otherlv_0, grammarAccess.getAdapter_VariableAccess().getAdapterAdapterDeclarationCrossReference_0_0()); 
            	

            }


            }

            otherlv_1=(Token)match(input,45,FOLLOW_45_in_ruleAdapter_Variable3351); 

                	newLeafNode(otherlv_1, grammarAccess.getAdapter_VariableAccess().getFullStopKeyword_1());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1581:1: ( (otherlv_2= RULE_ID ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1582:1: (otherlv_2= RULE_ID )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1582:1: (otherlv_2= RULE_ID )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1583:3: otherlv_2= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getAdapter_VariableRule());
            	        }
                    
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleAdapter_Variable3371); 

            		newLeafNode(otherlv_2, grammarAccess.getAdapter_VariableAccess().getVarVarDeclarationCrossReference_2_0()); 
            	

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAdapter_Variable"


    // $ANTLR start "entryRuleArray_Variable"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1602:1: entryRuleArray_Variable returns [EObject current=null] : iv_ruleArray_Variable= ruleArray_Variable EOF ;
    public final EObject entryRuleArray_Variable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleArray_Variable = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1603:2: (iv_ruleArray_Variable= ruleArray_Variable EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1604:2: iv_ruleArray_Variable= ruleArray_Variable EOF
            {
             newCompositeNode(grammarAccess.getArray_VariableRule()); 
            pushFollow(FOLLOW_ruleArray_Variable_in_entryRuleArray_Variable3407);
            iv_ruleArray_Variable=ruleArray_Variable();

            state._fsp--;

             current =iv_ruleArray_Variable; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleArray_Variable3417); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleArray_Variable"


    // $ANTLR start "ruleArray_Variable"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1611:1: ruleArray_Variable returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '[' ( (lv_num_2_0= ruleExpression ) ) otherlv_3= ']' ) ;
    public final EObject ruleArray_Variable() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_num_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1614:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '[' ( (lv_num_2_0= ruleExpression ) ) otherlv_3= ']' ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1615:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '[' ( (lv_num_2_0= ruleExpression ) ) otherlv_3= ']' )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1615:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '[' ( (lv_num_2_0= ruleExpression ) ) otherlv_3= ']' )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1615:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '[' ( (lv_num_2_0= ruleExpression ) ) otherlv_3= ']'
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1615:2: ( (otherlv_0= RULE_ID ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1616:1: (otherlv_0= RULE_ID )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1616:1: (otherlv_0= RULE_ID )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1617:3: otherlv_0= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getArray_VariableRule());
            	        }
                    
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleArray_Variable3462); 

            		newLeafNode(otherlv_0, grammarAccess.getArray_VariableAccess().getVarVarDeclarationCrossReference_0_0()); 
            	

            }


            }

            otherlv_1=(Token)match(input,39,FOLLOW_39_in_ruleArray_Variable3474); 

                	newLeafNode(otherlv_1, grammarAccess.getArray_VariableAccess().getLeftSquareBracketKeyword_1());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1632:1: ( (lv_num_2_0= ruleExpression ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1633:1: (lv_num_2_0= ruleExpression )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1633:1: (lv_num_2_0= ruleExpression )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1634:3: lv_num_2_0= ruleExpression
            {
             
            	        newCompositeNode(grammarAccess.getArray_VariableAccess().getNumExpressionParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleExpression_in_ruleArray_Variable3495);
            lv_num_2_0=ruleExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getArray_VariableRule());
            	        }
                   		set(
                   			current, 
                   			"num",
                    		lv_num_2_0, 
                    		"Expression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,40,FOLLOW_40_in_ruleArray_Variable3507); 

                	newLeafNode(otherlv_3, grammarAccess.getArray_VariableAccess().getRightSquareBracketKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArray_Variable"


    // $ANTLR start "entryRuleSelection_Statement"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1662:1: entryRuleSelection_Statement returns [EObject current=null] : iv_ruleSelection_Statement= ruleSelection_Statement EOF ;
    public final EObject entryRuleSelection_Statement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSelection_Statement = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1663:2: (iv_ruleSelection_Statement= ruleSelection_Statement EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1664:2: iv_ruleSelection_Statement= ruleSelection_Statement EOF
            {
             newCompositeNode(grammarAccess.getSelection_StatementRule()); 
            pushFollow(FOLLOW_ruleSelection_Statement_in_entryRuleSelection_Statement3543);
            iv_ruleSelection_Statement=ruleSelection_Statement();

            state._fsp--;

             current =iv_ruleSelection_Statement; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSelection_Statement3553); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSelection_Statement"


    // $ANTLR start "ruleSelection_Statement"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1671:1: ruleSelection_Statement returns [EObject current=null] : (this_If_Statement_0= ruleIf_Statement | this_Case_Statement_1= ruleCase_Statement ) ;
    public final EObject ruleSelection_Statement() throws RecognitionException {
        EObject current = null;

        EObject this_If_Statement_0 = null;

        EObject this_Case_Statement_1 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1674:28: ( (this_If_Statement_0= ruleIf_Statement | this_Case_Statement_1= ruleCase_Statement ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1675:1: (this_If_Statement_0= ruleIf_Statement | this_Case_Statement_1= ruleCase_Statement )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1675:1: (this_If_Statement_0= ruleIf_Statement | this_Case_Statement_1= ruleCase_Statement )
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==46) ) {
                alt33=1;
            }
            else if ( (LA33_0==51) ) {
                alt33=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 33, 0, input);

                throw nvae;
            }
            switch (alt33) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1676:5: this_If_Statement_0= ruleIf_Statement
                    {
                     
                            newCompositeNode(grammarAccess.getSelection_StatementAccess().getIf_StatementParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleIf_Statement_in_ruleSelection_Statement3600);
                    this_If_Statement_0=ruleIf_Statement();

                    state._fsp--;

                     
                            current = this_If_Statement_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1686:5: this_Case_Statement_1= ruleCase_Statement
                    {
                     
                            newCompositeNode(grammarAccess.getSelection_StatementAccess().getCase_StatementParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleCase_Statement_in_ruleSelection_Statement3627);
                    this_Case_Statement_1=ruleCase_Statement();

                    state._fsp--;

                     
                            current = this_Case_Statement_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSelection_Statement"


    // $ANTLR start "entryRuleIf_Statement"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1702:1: entryRuleIf_Statement returns [EObject current=null] : iv_ruleIf_Statement= ruleIf_Statement EOF ;
    public final EObject entryRuleIf_Statement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIf_Statement = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1703:2: (iv_ruleIf_Statement= ruleIf_Statement EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1704:2: iv_ruleIf_Statement= ruleIf_Statement EOF
            {
             newCompositeNode(grammarAccess.getIf_StatementRule()); 
            pushFollow(FOLLOW_ruleIf_Statement_in_entryRuleIf_Statement3662);
            iv_ruleIf_Statement=ruleIf_Statement();

            state._fsp--;

             current =iv_ruleIf_Statement; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIf_Statement3672); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIf_Statement"


    // $ANTLR start "ruleIf_Statement"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1711:1: ruleIf_Statement returns [EObject current=null] : (otherlv_0= 'IF' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= 'THEN' ( (lv_statments_3_0= ruleStatementList ) ) ( (lv_elseif_4_0= ruleElseIf_Statement ) )? (otherlv_5= 'ELSE' ( (lv_elseStatements_6_0= ruleStatementList ) ) )? otherlv_7= 'END_IF' ) ;
    public final EObject ruleIf_Statement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject lv_expression_1_0 = null;

        EObject lv_statments_3_0 = null;

        EObject lv_elseif_4_0 = null;

        EObject lv_elseStatements_6_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1714:28: ( (otherlv_0= 'IF' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= 'THEN' ( (lv_statments_3_0= ruleStatementList ) ) ( (lv_elseif_4_0= ruleElseIf_Statement ) )? (otherlv_5= 'ELSE' ( (lv_elseStatements_6_0= ruleStatementList ) ) )? otherlv_7= 'END_IF' ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1715:1: (otherlv_0= 'IF' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= 'THEN' ( (lv_statments_3_0= ruleStatementList ) ) ( (lv_elseif_4_0= ruleElseIf_Statement ) )? (otherlv_5= 'ELSE' ( (lv_elseStatements_6_0= ruleStatementList ) ) )? otherlv_7= 'END_IF' )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1715:1: (otherlv_0= 'IF' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= 'THEN' ( (lv_statments_3_0= ruleStatementList ) ) ( (lv_elseif_4_0= ruleElseIf_Statement ) )? (otherlv_5= 'ELSE' ( (lv_elseStatements_6_0= ruleStatementList ) ) )? otherlv_7= 'END_IF' )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1715:3: otherlv_0= 'IF' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= 'THEN' ( (lv_statments_3_0= ruleStatementList ) ) ( (lv_elseif_4_0= ruleElseIf_Statement ) )? (otherlv_5= 'ELSE' ( (lv_elseStatements_6_0= ruleStatementList ) ) )? otherlv_7= 'END_IF'
            {
            otherlv_0=(Token)match(input,46,FOLLOW_46_in_ruleIf_Statement3709); 

                	newLeafNode(otherlv_0, grammarAccess.getIf_StatementAccess().getIFKeyword_0());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1719:1: ( (lv_expression_1_0= ruleExpression ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1720:1: (lv_expression_1_0= ruleExpression )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1720:1: (lv_expression_1_0= ruleExpression )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1721:3: lv_expression_1_0= ruleExpression
            {
             
            	        newCompositeNode(grammarAccess.getIf_StatementAccess().getExpressionExpressionParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleExpression_in_ruleIf_Statement3730);
            lv_expression_1_0=ruleExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getIf_StatementRule());
            	        }
                   		set(
                   			current, 
                   			"expression",
                    		lv_expression_1_0, 
                    		"Expression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,47,FOLLOW_47_in_ruleIf_Statement3742); 

                	newLeafNode(otherlv_2, grammarAccess.getIf_StatementAccess().getTHENKeyword_2());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1741:1: ( (lv_statments_3_0= ruleStatementList ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1742:1: (lv_statments_3_0= ruleStatementList )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1742:1: (lv_statments_3_0= ruleStatementList )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1743:3: lv_statments_3_0= ruleStatementList
            {
             
            	        newCompositeNode(grammarAccess.getIf_StatementAccess().getStatmentsStatementListParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_ruleStatementList_in_ruleIf_Statement3763);
            lv_statments_3_0=ruleStatementList();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getIf_StatementRule());
            	        }
                   		set(
                   			current, 
                   			"statments",
                    		lv_statments_3_0, 
                    		"StatementList");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1759:2: ( (lv_elseif_4_0= ruleElseIf_Statement ) )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==50) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1760:1: (lv_elseif_4_0= ruleElseIf_Statement )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1760:1: (lv_elseif_4_0= ruleElseIf_Statement )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1761:3: lv_elseif_4_0= ruleElseIf_Statement
                    {
                     
                    	        newCompositeNode(grammarAccess.getIf_StatementAccess().getElseifElseIf_StatementParserRuleCall_4_0()); 
                    	    
                    pushFollow(FOLLOW_ruleElseIf_Statement_in_ruleIf_Statement3784);
                    lv_elseif_4_0=ruleElseIf_Statement();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getIf_StatementRule());
                    	        }
                           		add(
                           			current, 
                           			"elseif",
                            		lv_elseif_4_0, 
                            		"ElseIf_Statement");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1777:3: (otherlv_5= 'ELSE' ( (lv_elseStatements_6_0= ruleStatementList ) ) )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==48) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1777:5: otherlv_5= 'ELSE' ( (lv_elseStatements_6_0= ruleStatementList ) )
                    {
                    otherlv_5=(Token)match(input,48,FOLLOW_48_in_ruleIf_Statement3798); 

                        	newLeafNode(otherlv_5, grammarAccess.getIf_StatementAccess().getELSEKeyword_5_0());
                        
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1781:1: ( (lv_elseStatements_6_0= ruleStatementList ) )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1782:1: (lv_elseStatements_6_0= ruleStatementList )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1782:1: (lv_elseStatements_6_0= ruleStatementList )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1783:3: lv_elseStatements_6_0= ruleStatementList
                    {
                     
                    	        newCompositeNode(grammarAccess.getIf_StatementAccess().getElseStatementsStatementListParserRuleCall_5_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleStatementList_in_ruleIf_Statement3819);
                    lv_elseStatements_6_0=ruleStatementList();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getIf_StatementRule());
                    	        }
                           		set(
                           			current, 
                           			"elseStatements",
                            		lv_elseStatements_6_0, 
                            		"StatementList");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            otherlv_7=(Token)match(input,49,FOLLOW_49_in_ruleIf_Statement3833); 

                	newLeafNode(otherlv_7, grammarAccess.getIf_StatementAccess().getEND_IFKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIf_Statement"


    // $ANTLR start "entryRuleElseIf_Statement"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1811:1: entryRuleElseIf_Statement returns [EObject current=null] : iv_ruleElseIf_Statement= ruleElseIf_Statement EOF ;
    public final EObject entryRuleElseIf_Statement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleElseIf_Statement = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1812:2: (iv_ruleElseIf_Statement= ruleElseIf_Statement EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1813:2: iv_ruleElseIf_Statement= ruleElseIf_Statement EOF
            {
             newCompositeNode(grammarAccess.getElseIf_StatementRule()); 
            pushFollow(FOLLOW_ruleElseIf_Statement_in_entryRuleElseIf_Statement3869);
            iv_ruleElseIf_Statement=ruleElseIf_Statement();

            state._fsp--;

             current =iv_ruleElseIf_Statement; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleElseIf_Statement3879); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleElseIf_Statement"


    // $ANTLR start "ruleElseIf_Statement"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1820:1: ruleElseIf_Statement returns [EObject current=null] : (otherlv_0= 'ELSIF' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= 'THEN' ( (lv_statements_3_0= ruleStatementList ) ) ) ;
    public final EObject ruleElseIf_Statement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_expression_1_0 = null;

        EObject lv_statements_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1823:28: ( (otherlv_0= 'ELSIF' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= 'THEN' ( (lv_statements_3_0= ruleStatementList ) ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1824:1: (otherlv_0= 'ELSIF' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= 'THEN' ( (lv_statements_3_0= ruleStatementList ) ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1824:1: (otherlv_0= 'ELSIF' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= 'THEN' ( (lv_statements_3_0= ruleStatementList ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1824:3: otherlv_0= 'ELSIF' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= 'THEN' ( (lv_statements_3_0= ruleStatementList ) )
            {
            otherlv_0=(Token)match(input,50,FOLLOW_50_in_ruleElseIf_Statement3916); 

                	newLeafNode(otherlv_0, grammarAccess.getElseIf_StatementAccess().getELSIFKeyword_0());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1828:1: ( (lv_expression_1_0= ruleExpression ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1829:1: (lv_expression_1_0= ruleExpression )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1829:1: (lv_expression_1_0= ruleExpression )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1830:3: lv_expression_1_0= ruleExpression
            {
             
            	        newCompositeNode(grammarAccess.getElseIf_StatementAccess().getExpressionExpressionParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleExpression_in_ruleElseIf_Statement3937);
            lv_expression_1_0=ruleExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getElseIf_StatementRule());
            	        }
                   		set(
                   			current, 
                   			"expression",
                    		lv_expression_1_0, 
                    		"Expression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,47,FOLLOW_47_in_ruleElseIf_Statement3949); 

                	newLeafNode(otherlv_2, grammarAccess.getElseIf_StatementAccess().getTHENKeyword_2());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1850:1: ( (lv_statements_3_0= ruleStatementList ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1851:1: (lv_statements_3_0= ruleStatementList )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1851:1: (lv_statements_3_0= ruleStatementList )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1852:3: lv_statements_3_0= ruleStatementList
            {
             
            	        newCompositeNode(grammarAccess.getElseIf_StatementAccess().getStatementsStatementListParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_ruleStatementList_in_ruleElseIf_Statement3970);
            lv_statements_3_0=ruleStatementList();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getElseIf_StatementRule());
            	        }
                   		set(
                   			current, 
                   			"statements",
                    		lv_statements_3_0, 
                    		"StatementList");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleElseIf_Statement"


    // $ANTLR start "entryRuleCase_Statement"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1876:1: entryRuleCase_Statement returns [EObject current=null] : iv_ruleCase_Statement= ruleCase_Statement EOF ;
    public final EObject entryRuleCase_Statement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCase_Statement = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1877:2: (iv_ruleCase_Statement= ruleCase_Statement EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1878:2: iv_ruleCase_Statement= ruleCase_Statement EOF
            {
             newCompositeNode(grammarAccess.getCase_StatementRule()); 
            pushFollow(FOLLOW_ruleCase_Statement_in_entryRuleCase_Statement4006);
            iv_ruleCase_Statement=ruleCase_Statement();

            state._fsp--;

             current =iv_ruleCase_Statement; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCase_Statement4016); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCase_Statement"


    // $ANTLR start "ruleCase_Statement"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1885:1: ruleCase_Statement returns [EObject current=null] : (otherlv_0= 'CASE' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= 'OF' ( (lv_caseElements_3_0= ruleCase_Element ) )+ ( (lv_caseElse_4_0= ruleCase_Else ) )? otherlv_5= 'END_CASE' ) ;
    public final EObject ruleCase_Statement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        EObject lv_expression_1_0 = null;

        EObject lv_caseElements_3_0 = null;

        EObject lv_caseElse_4_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1888:28: ( (otherlv_0= 'CASE' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= 'OF' ( (lv_caseElements_3_0= ruleCase_Element ) )+ ( (lv_caseElse_4_0= ruleCase_Else ) )? otherlv_5= 'END_CASE' ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1889:1: (otherlv_0= 'CASE' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= 'OF' ( (lv_caseElements_3_0= ruleCase_Element ) )+ ( (lv_caseElse_4_0= ruleCase_Else ) )? otherlv_5= 'END_CASE' )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1889:1: (otherlv_0= 'CASE' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= 'OF' ( (lv_caseElements_3_0= ruleCase_Element ) )+ ( (lv_caseElse_4_0= ruleCase_Else ) )? otherlv_5= 'END_CASE' )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1889:3: otherlv_0= 'CASE' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= 'OF' ( (lv_caseElements_3_0= ruleCase_Element ) )+ ( (lv_caseElse_4_0= ruleCase_Else ) )? otherlv_5= 'END_CASE'
            {
            otherlv_0=(Token)match(input,51,FOLLOW_51_in_ruleCase_Statement4053); 

                	newLeafNode(otherlv_0, grammarAccess.getCase_StatementAccess().getCASEKeyword_0());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1893:1: ( (lv_expression_1_0= ruleExpression ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1894:1: (lv_expression_1_0= ruleExpression )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1894:1: (lv_expression_1_0= ruleExpression )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1895:3: lv_expression_1_0= ruleExpression
            {
             
            	        newCompositeNode(grammarAccess.getCase_StatementAccess().getExpressionExpressionParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleExpression_in_ruleCase_Statement4074);
            lv_expression_1_0=ruleExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getCase_StatementRule());
            	        }
                   		set(
                   			current, 
                   			"expression",
                    		lv_expression_1_0, 
                    		"Expression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,41,FOLLOW_41_in_ruleCase_Statement4086); 

                	newLeafNode(otherlv_2, grammarAccess.getCase_StatementAccess().getOFKeyword_2());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1915:1: ( (lv_caseElements_3_0= ruleCase_Element ) )+
            int cnt36=0;
            loop36:
            do {
                int alt36=2;
                int LA36_0 = input.LA(1);

                if ( (LA36_0==RULE_INT) ) {
                    alt36=1;
                }


                switch (alt36) {
            	case 1 :
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1916:1: (lv_caseElements_3_0= ruleCase_Element )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1916:1: (lv_caseElements_3_0= ruleCase_Element )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1917:3: lv_caseElements_3_0= ruleCase_Element
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getCase_StatementAccess().getCaseElementsCase_ElementParserRuleCall_3_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleCase_Element_in_ruleCase_Statement4107);
            	    lv_caseElements_3_0=ruleCase_Element();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getCase_StatementRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"caseElements",
            	            		lv_caseElements_3_0, 
            	            		"Case_Element");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt36 >= 1 ) break loop36;
                        EarlyExitException eee =
                            new EarlyExitException(36, input);
                        throw eee;
                }
                cnt36++;
            } while (true);

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1933:3: ( (lv_caseElse_4_0= ruleCase_Else ) )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==48) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1934:1: (lv_caseElse_4_0= ruleCase_Else )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1934:1: (lv_caseElse_4_0= ruleCase_Else )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1935:3: lv_caseElse_4_0= ruleCase_Else
                    {
                     
                    	        newCompositeNode(grammarAccess.getCase_StatementAccess().getCaseElseCase_ElseParserRuleCall_4_0()); 
                    	    
                    pushFollow(FOLLOW_ruleCase_Else_in_ruleCase_Statement4129);
                    lv_caseElse_4_0=ruleCase_Else();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCase_StatementRule());
                    	        }
                           		set(
                           			current, 
                           			"caseElse",
                            		lv_caseElse_4_0, 
                            		"Case_Else");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,52,FOLLOW_52_in_ruleCase_Statement4142); 

                	newLeafNode(otherlv_5, grammarAccess.getCase_StatementAccess().getEND_CASEKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCase_Statement"


    // $ANTLR start "entryRuleCase_Element"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1963:1: entryRuleCase_Element returns [EObject current=null] : iv_ruleCase_Element= ruleCase_Element EOF ;
    public final EObject entryRuleCase_Element() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCase_Element = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1964:2: (iv_ruleCase_Element= ruleCase_Element EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1965:2: iv_ruleCase_Element= ruleCase_Element EOF
            {
             newCompositeNode(grammarAccess.getCase_ElementRule()); 
            pushFollow(FOLLOW_ruleCase_Element_in_entryRuleCase_Element4178);
            iv_ruleCase_Element=ruleCase_Element();

            state._fsp--;

             current =iv_ruleCase_Element; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCase_Element4188); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCase_Element"


    // $ANTLR start "ruleCase_Element"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1972:1: ruleCase_Element returns [EObject current=null] : ( ( (lv_case_0_0= RULE_INT ) ) otherlv_1= ':' ( (lv_statements_2_0= ruleStatementList ) ) ) ;
    public final EObject ruleCase_Element() throws RecognitionException {
        EObject current = null;

        Token lv_case_0_0=null;
        Token otherlv_1=null;
        EObject lv_statements_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1975:28: ( ( ( (lv_case_0_0= RULE_INT ) ) otherlv_1= ':' ( (lv_statements_2_0= ruleStatementList ) ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1976:1: ( ( (lv_case_0_0= RULE_INT ) ) otherlv_1= ':' ( (lv_statements_2_0= ruleStatementList ) ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1976:1: ( ( (lv_case_0_0= RULE_INT ) ) otherlv_1= ':' ( (lv_statements_2_0= ruleStatementList ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1976:2: ( (lv_case_0_0= RULE_INT ) ) otherlv_1= ':' ( (lv_statements_2_0= ruleStatementList ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1976:2: ( (lv_case_0_0= RULE_INT ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1977:1: (lv_case_0_0= RULE_INT )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1977:1: (lv_case_0_0= RULE_INT )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1978:3: lv_case_0_0= RULE_INT
            {
            lv_case_0_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleCase_Element4230); 

            			newLeafNode(lv_case_0_0, grammarAccess.getCase_ElementAccess().getCaseINTTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getCase_ElementRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"case",
                    		lv_case_0_0, 
                    		"INT");
            	    

            }


            }

            otherlv_1=(Token)match(input,30,FOLLOW_30_in_ruleCase_Element4247); 

                	newLeafNode(otherlv_1, grammarAccess.getCase_ElementAccess().getColonKeyword_1());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1998:1: ( (lv_statements_2_0= ruleStatementList ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1999:1: (lv_statements_2_0= ruleStatementList )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:1999:1: (lv_statements_2_0= ruleStatementList )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2000:3: lv_statements_2_0= ruleStatementList
            {
             
            	        newCompositeNode(grammarAccess.getCase_ElementAccess().getStatementsStatementListParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleStatementList_in_ruleCase_Element4268);
            lv_statements_2_0=ruleStatementList();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getCase_ElementRule());
            	        }
                   		set(
                   			current, 
                   			"statements",
                    		lv_statements_2_0, 
                    		"StatementList");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCase_Element"


    // $ANTLR start "entryRuleCase_Else"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2024:1: entryRuleCase_Else returns [EObject current=null] : iv_ruleCase_Else= ruleCase_Else EOF ;
    public final EObject entryRuleCase_Else() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCase_Else = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2025:2: (iv_ruleCase_Else= ruleCase_Else EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2026:2: iv_ruleCase_Else= ruleCase_Else EOF
            {
             newCompositeNode(grammarAccess.getCase_ElseRule()); 
            pushFollow(FOLLOW_ruleCase_Else_in_entryRuleCase_Else4304);
            iv_ruleCase_Else=ruleCase_Else();

            state._fsp--;

             current =iv_ruleCase_Else; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCase_Else4314); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCase_Else"


    // $ANTLR start "ruleCase_Else"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2033:1: ruleCase_Else returns [EObject current=null] : ( () otherlv_1= 'ELSE' ( (lv_statements_2_0= ruleStatementList ) ) ) ;
    public final EObject ruleCase_Else() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_statements_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2036:28: ( ( () otherlv_1= 'ELSE' ( (lv_statements_2_0= ruleStatementList ) ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2037:1: ( () otherlv_1= 'ELSE' ( (lv_statements_2_0= ruleStatementList ) ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2037:1: ( () otherlv_1= 'ELSE' ( (lv_statements_2_0= ruleStatementList ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2037:2: () otherlv_1= 'ELSE' ( (lv_statements_2_0= ruleStatementList ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2037:2: ()
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2038:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getCase_ElseAccess().getCase_ElseAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,48,FOLLOW_48_in_ruleCase_Else4360); 

                	newLeafNode(otherlv_1, grammarAccess.getCase_ElseAccess().getELSEKeyword_1());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2047:1: ( (lv_statements_2_0= ruleStatementList ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2048:1: (lv_statements_2_0= ruleStatementList )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2048:1: (lv_statements_2_0= ruleStatementList )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2049:3: lv_statements_2_0= ruleStatementList
            {
             
            	        newCompositeNode(grammarAccess.getCase_ElseAccess().getStatementsStatementListParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleStatementList_in_ruleCase_Else4381);
            lv_statements_2_0=ruleStatementList();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getCase_ElseRule());
            	        }
                   		set(
                   			current, 
                   			"statements",
                    		lv_statements_2_0, 
                    		"StatementList");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCase_Else"


    // $ANTLR start "entryRuleIterationControl_Stmt"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2073:1: entryRuleIterationControl_Stmt returns [EObject current=null] : iv_ruleIterationControl_Stmt= ruleIterationControl_Stmt EOF ;
    public final EObject entryRuleIterationControl_Stmt() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIterationControl_Stmt = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2074:2: (iv_ruleIterationControl_Stmt= ruleIterationControl_Stmt EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2075:2: iv_ruleIterationControl_Stmt= ruleIterationControl_Stmt EOF
            {
             newCompositeNode(grammarAccess.getIterationControl_StmtRule()); 
            pushFollow(FOLLOW_ruleIterationControl_Stmt_in_entryRuleIterationControl_Stmt4417);
            iv_ruleIterationControl_Stmt=ruleIterationControl_Stmt();

            state._fsp--;

             current =iv_ruleIterationControl_Stmt; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIterationControl_Stmt4427); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIterationControl_Stmt"


    // $ANTLR start "ruleIterationControl_Stmt"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2082:1: ruleIterationControl_Stmt returns [EObject current=null] : ( ( (lv_statement_0_1= 'EXIT' | lv_statement_0_2= 'CONTINUE' | lv_statement_0_3= 'RETURN' ) ) ) ;
    public final EObject ruleIterationControl_Stmt() throws RecognitionException {
        EObject current = null;

        Token lv_statement_0_1=null;
        Token lv_statement_0_2=null;
        Token lv_statement_0_3=null;

         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2085:28: ( ( ( (lv_statement_0_1= 'EXIT' | lv_statement_0_2= 'CONTINUE' | lv_statement_0_3= 'RETURN' ) ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2086:1: ( ( (lv_statement_0_1= 'EXIT' | lv_statement_0_2= 'CONTINUE' | lv_statement_0_3= 'RETURN' ) ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2086:1: ( ( (lv_statement_0_1= 'EXIT' | lv_statement_0_2= 'CONTINUE' | lv_statement_0_3= 'RETURN' ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2087:1: ( (lv_statement_0_1= 'EXIT' | lv_statement_0_2= 'CONTINUE' | lv_statement_0_3= 'RETURN' ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2087:1: ( (lv_statement_0_1= 'EXIT' | lv_statement_0_2= 'CONTINUE' | lv_statement_0_3= 'RETURN' ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2088:1: (lv_statement_0_1= 'EXIT' | lv_statement_0_2= 'CONTINUE' | lv_statement_0_3= 'RETURN' )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2088:1: (lv_statement_0_1= 'EXIT' | lv_statement_0_2= 'CONTINUE' | lv_statement_0_3= 'RETURN' )
            int alt38=3;
            switch ( input.LA(1) ) {
            case 53:
                {
                alt38=1;
                }
                break;
            case 54:
                {
                alt38=2;
                }
                break;
            case 55:
                {
                alt38=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 38, 0, input);

                throw nvae;
            }

            switch (alt38) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2089:3: lv_statement_0_1= 'EXIT'
                    {
                    lv_statement_0_1=(Token)match(input,53,FOLLOW_53_in_ruleIterationControl_Stmt4471); 

                            newLeafNode(lv_statement_0_1, grammarAccess.getIterationControl_StmtAccess().getStatementEXITKeyword_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getIterationControl_StmtRule());
                    	        }
                           		setWithLastConsumed(current, "statement", lv_statement_0_1, null);
                    	    

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2101:8: lv_statement_0_2= 'CONTINUE'
                    {
                    lv_statement_0_2=(Token)match(input,54,FOLLOW_54_in_ruleIterationControl_Stmt4500); 

                            newLeafNode(lv_statement_0_2, grammarAccess.getIterationControl_StmtAccess().getStatementCONTINUEKeyword_0_1());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getIterationControl_StmtRule());
                    	        }
                           		setWithLastConsumed(current, "statement", lv_statement_0_2, null);
                    	    

                    }
                    break;
                case 3 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2113:8: lv_statement_0_3= 'RETURN'
                    {
                    lv_statement_0_3=(Token)match(input,55,FOLLOW_55_in_ruleIterationControl_Stmt4529); 

                            newLeafNode(lv_statement_0_3, grammarAccess.getIterationControl_StmtAccess().getStatementRETURNKeyword_0_2());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getIterationControl_StmtRule());
                    	        }
                           		setWithLastConsumed(current, "statement", lv_statement_0_3, null);
                    	    

                    }
                    break;

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIterationControl_Stmt"


    // $ANTLR start "entryRuleFor_Statement"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2136:1: entryRuleFor_Statement returns [EObject current=null] : iv_ruleFor_Statement= ruleFor_Statement EOF ;
    public final EObject entryRuleFor_Statement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFor_Statement = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2137:2: (iv_ruleFor_Statement= ruleFor_Statement EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2138:2: iv_ruleFor_Statement= ruleFor_Statement EOF
            {
             newCompositeNode(grammarAccess.getFor_StatementRule()); 
            pushFollow(FOLLOW_ruleFor_Statement_in_entryRuleFor_Statement4580);
            iv_ruleFor_Statement=ruleFor_Statement();

            state._fsp--;

             current =iv_ruleFor_Statement; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFor_Statement4590); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFor_Statement"


    // $ANTLR start "ruleFor_Statement"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2145:1: ruleFor_Statement returns [EObject current=null] : (otherlv_0= 'FOR' ( (otherlv_1= RULE_ID ) ) otherlv_2= ':=' ( (lv_expressionFrom_3_0= ruleExpression ) ) otherlv_4= 'TO' ( (lv_expressionTo_5_0= ruleExpression ) ) (otherlv_6= 'BY' ( (lv_expressionBy_7_0= ruleExpression ) ) )? otherlv_8= 'DO' ( (lv_statements_9_0= ruleStatementList ) ) otherlv_10= 'END_FOR' ) ;
    public final EObject ruleFor_Statement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        EObject lv_expressionFrom_3_0 = null;

        EObject lv_expressionTo_5_0 = null;

        EObject lv_expressionBy_7_0 = null;

        EObject lv_statements_9_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2148:28: ( (otherlv_0= 'FOR' ( (otherlv_1= RULE_ID ) ) otherlv_2= ':=' ( (lv_expressionFrom_3_0= ruleExpression ) ) otherlv_4= 'TO' ( (lv_expressionTo_5_0= ruleExpression ) ) (otherlv_6= 'BY' ( (lv_expressionBy_7_0= ruleExpression ) ) )? otherlv_8= 'DO' ( (lv_statements_9_0= ruleStatementList ) ) otherlv_10= 'END_FOR' ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2149:1: (otherlv_0= 'FOR' ( (otherlv_1= RULE_ID ) ) otherlv_2= ':=' ( (lv_expressionFrom_3_0= ruleExpression ) ) otherlv_4= 'TO' ( (lv_expressionTo_5_0= ruleExpression ) ) (otherlv_6= 'BY' ( (lv_expressionBy_7_0= ruleExpression ) ) )? otherlv_8= 'DO' ( (lv_statements_9_0= ruleStatementList ) ) otherlv_10= 'END_FOR' )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2149:1: (otherlv_0= 'FOR' ( (otherlv_1= RULE_ID ) ) otherlv_2= ':=' ( (lv_expressionFrom_3_0= ruleExpression ) ) otherlv_4= 'TO' ( (lv_expressionTo_5_0= ruleExpression ) ) (otherlv_6= 'BY' ( (lv_expressionBy_7_0= ruleExpression ) ) )? otherlv_8= 'DO' ( (lv_statements_9_0= ruleStatementList ) ) otherlv_10= 'END_FOR' )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2149:3: otherlv_0= 'FOR' ( (otherlv_1= RULE_ID ) ) otherlv_2= ':=' ( (lv_expressionFrom_3_0= ruleExpression ) ) otherlv_4= 'TO' ( (lv_expressionTo_5_0= ruleExpression ) ) (otherlv_6= 'BY' ( (lv_expressionBy_7_0= ruleExpression ) ) )? otherlv_8= 'DO' ( (lv_statements_9_0= ruleStatementList ) ) otherlv_10= 'END_FOR'
            {
            otherlv_0=(Token)match(input,56,FOLLOW_56_in_ruleFor_Statement4627); 

                	newLeafNode(otherlv_0, grammarAccess.getFor_StatementAccess().getFORKeyword_0());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2153:1: ( (otherlv_1= RULE_ID ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2154:1: (otherlv_1= RULE_ID )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2154:1: (otherlv_1= RULE_ID )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2155:3: otherlv_1= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getFor_StatementRule());
            	        }
                    
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleFor_Statement4647); 

            		newLeafNode(otherlv_1, grammarAccess.getFor_StatementAccess().getControlVariableVarDeclarationCrossReference_1_0()); 
            	

            }


            }

            otherlv_2=(Token)match(input,37,FOLLOW_37_in_ruleFor_Statement4659); 

                	newLeafNode(otherlv_2, grammarAccess.getFor_StatementAccess().getColonEqualsSignKeyword_2());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2170:1: ( (lv_expressionFrom_3_0= ruleExpression ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2171:1: (lv_expressionFrom_3_0= ruleExpression )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2171:1: (lv_expressionFrom_3_0= ruleExpression )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2172:3: lv_expressionFrom_3_0= ruleExpression
            {
             
            	        newCompositeNode(grammarAccess.getFor_StatementAccess().getExpressionFromExpressionParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_ruleExpression_in_ruleFor_Statement4680);
            lv_expressionFrom_3_0=ruleExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getFor_StatementRule());
            	        }
                   		set(
                   			current, 
                   			"expressionFrom",
                    		lv_expressionFrom_3_0, 
                    		"Expression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,57,FOLLOW_57_in_ruleFor_Statement4692); 

                	newLeafNode(otherlv_4, grammarAccess.getFor_StatementAccess().getTOKeyword_4());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2192:1: ( (lv_expressionTo_5_0= ruleExpression ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2193:1: (lv_expressionTo_5_0= ruleExpression )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2193:1: (lv_expressionTo_5_0= ruleExpression )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2194:3: lv_expressionTo_5_0= ruleExpression
            {
             
            	        newCompositeNode(grammarAccess.getFor_StatementAccess().getExpressionToExpressionParserRuleCall_5_0()); 
            	    
            pushFollow(FOLLOW_ruleExpression_in_ruleFor_Statement4713);
            lv_expressionTo_5_0=ruleExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getFor_StatementRule());
            	        }
                   		set(
                   			current, 
                   			"expressionTo",
                    		lv_expressionTo_5_0, 
                    		"Expression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2210:2: (otherlv_6= 'BY' ( (lv_expressionBy_7_0= ruleExpression ) ) )?
            int alt39=2;
            int LA39_0 = input.LA(1);

            if ( (LA39_0==58) ) {
                alt39=1;
            }
            switch (alt39) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2210:4: otherlv_6= 'BY' ( (lv_expressionBy_7_0= ruleExpression ) )
                    {
                    otherlv_6=(Token)match(input,58,FOLLOW_58_in_ruleFor_Statement4726); 

                        	newLeafNode(otherlv_6, grammarAccess.getFor_StatementAccess().getBYKeyword_6_0());
                        
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2214:1: ( (lv_expressionBy_7_0= ruleExpression ) )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2215:1: (lv_expressionBy_7_0= ruleExpression )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2215:1: (lv_expressionBy_7_0= ruleExpression )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2216:3: lv_expressionBy_7_0= ruleExpression
                    {
                     
                    	        newCompositeNode(grammarAccess.getFor_StatementAccess().getExpressionByExpressionParserRuleCall_6_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleExpression_in_ruleFor_Statement4747);
                    lv_expressionBy_7_0=ruleExpression();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getFor_StatementRule());
                    	        }
                           		set(
                           			current, 
                           			"expressionBy",
                            		lv_expressionBy_7_0, 
                            		"Expression");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            otherlv_8=(Token)match(input,59,FOLLOW_59_in_ruleFor_Statement4761); 

                	newLeafNode(otherlv_8, grammarAccess.getFor_StatementAccess().getDOKeyword_7());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2236:1: ( (lv_statements_9_0= ruleStatementList ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2237:1: (lv_statements_9_0= ruleStatementList )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2237:1: (lv_statements_9_0= ruleStatementList )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2238:3: lv_statements_9_0= ruleStatementList
            {
             
            	        newCompositeNode(grammarAccess.getFor_StatementAccess().getStatementsStatementListParserRuleCall_8_0()); 
            	    
            pushFollow(FOLLOW_ruleStatementList_in_ruleFor_Statement4782);
            lv_statements_9_0=ruleStatementList();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getFor_StatementRule());
            	        }
                   		set(
                   			current, 
                   			"statements",
                    		lv_statements_9_0, 
                    		"StatementList");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_10=(Token)match(input,60,FOLLOW_60_in_ruleFor_Statement4794); 

                	newLeafNode(otherlv_10, grammarAccess.getFor_StatementAccess().getEND_FORKeyword_9());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFor_Statement"


    // $ANTLR start "entryRuleRepeat_Statement"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2266:1: entryRuleRepeat_Statement returns [EObject current=null] : iv_ruleRepeat_Statement= ruleRepeat_Statement EOF ;
    public final EObject entryRuleRepeat_Statement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRepeat_Statement = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2267:2: (iv_ruleRepeat_Statement= ruleRepeat_Statement EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2268:2: iv_ruleRepeat_Statement= ruleRepeat_Statement EOF
            {
             newCompositeNode(grammarAccess.getRepeat_StatementRule()); 
            pushFollow(FOLLOW_ruleRepeat_Statement_in_entryRuleRepeat_Statement4830);
            iv_ruleRepeat_Statement=ruleRepeat_Statement();

            state._fsp--;

             current =iv_ruleRepeat_Statement; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRepeat_Statement4840); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRepeat_Statement"


    // $ANTLR start "ruleRepeat_Statement"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2275:1: ruleRepeat_Statement returns [EObject current=null] : (otherlv_0= 'REPEAT' ( (lv_statements_1_0= ruleStatementList ) ) otherlv_2= 'UNTIL' ( (lv_expression_3_0= ruleExpression ) ) otherlv_4= 'END_REPEAT' ) ;
    public final EObject ruleRepeat_Statement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_statements_1_0 = null;

        EObject lv_expression_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2278:28: ( (otherlv_0= 'REPEAT' ( (lv_statements_1_0= ruleStatementList ) ) otherlv_2= 'UNTIL' ( (lv_expression_3_0= ruleExpression ) ) otherlv_4= 'END_REPEAT' ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2279:1: (otherlv_0= 'REPEAT' ( (lv_statements_1_0= ruleStatementList ) ) otherlv_2= 'UNTIL' ( (lv_expression_3_0= ruleExpression ) ) otherlv_4= 'END_REPEAT' )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2279:1: (otherlv_0= 'REPEAT' ( (lv_statements_1_0= ruleStatementList ) ) otherlv_2= 'UNTIL' ( (lv_expression_3_0= ruleExpression ) ) otherlv_4= 'END_REPEAT' )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2279:3: otherlv_0= 'REPEAT' ( (lv_statements_1_0= ruleStatementList ) ) otherlv_2= 'UNTIL' ( (lv_expression_3_0= ruleExpression ) ) otherlv_4= 'END_REPEAT'
            {
            otherlv_0=(Token)match(input,61,FOLLOW_61_in_ruleRepeat_Statement4877); 

                	newLeafNode(otherlv_0, grammarAccess.getRepeat_StatementAccess().getREPEATKeyword_0());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2283:1: ( (lv_statements_1_0= ruleStatementList ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2284:1: (lv_statements_1_0= ruleStatementList )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2284:1: (lv_statements_1_0= ruleStatementList )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2285:3: lv_statements_1_0= ruleStatementList
            {
             
            	        newCompositeNode(grammarAccess.getRepeat_StatementAccess().getStatementsStatementListParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleStatementList_in_ruleRepeat_Statement4898);
            lv_statements_1_0=ruleStatementList();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getRepeat_StatementRule());
            	        }
                   		set(
                   			current, 
                   			"statements",
                    		lv_statements_1_0, 
                    		"StatementList");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,62,FOLLOW_62_in_ruleRepeat_Statement4910); 

                	newLeafNode(otherlv_2, grammarAccess.getRepeat_StatementAccess().getUNTILKeyword_2());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2305:1: ( (lv_expression_3_0= ruleExpression ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2306:1: (lv_expression_3_0= ruleExpression )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2306:1: (lv_expression_3_0= ruleExpression )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2307:3: lv_expression_3_0= ruleExpression
            {
             
            	        newCompositeNode(grammarAccess.getRepeat_StatementAccess().getExpressionExpressionParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_ruleExpression_in_ruleRepeat_Statement4931);
            lv_expression_3_0=ruleExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getRepeat_StatementRule());
            	        }
                   		set(
                   			current, 
                   			"expression",
                    		lv_expression_3_0, 
                    		"Expression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,63,FOLLOW_63_in_ruleRepeat_Statement4943); 

                	newLeafNode(otherlv_4, grammarAccess.getRepeat_StatementAccess().getEND_REPEATKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRepeat_Statement"


    // $ANTLR start "entryRuleWhile_Statement"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2335:1: entryRuleWhile_Statement returns [EObject current=null] : iv_ruleWhile_Statement= ruleWhile_Statement EOF ;
    public final EObject entryRuleWhile_Statement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWhile_Statement = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2336:2: (iv_ruleWhile_Statement= ruleWhile_Statement EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2337:2: iv_ruleWhile_Statement= ruleWhile_Statement EOF
            {
             newCompositeNode(grammarAccess.getWhile_StatementRule()); 
            pushFollow(FOLLOW_ruleWhile_Statement_in_entryRuleWhile_Statement4979);
            iv_ruleWhile_Statement=ruleWhile_Statement();

            state._fsp--;

             current =iv_ruleWhile_Statement; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleWhile_Statement4989); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWhile_Statement"


    // $ANTLR start "ruleWhile_Statement"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2344:1: ruleWhile_Statement returns [EObject current=null] : (otherlv_0= 'WHILE' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= 'DO' ( (lv_statements_3_0= ruleStatementList ) ) otherlv_4= 'END_WHILE' ) ;
    public final EObject ruleWhile_Statement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_expression_1_0 = null;

        EObject lv_statements_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2347:28: ( (otherlv_0= 'WHILE' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= 'DO' ( (lv_statements_3_0= ruleStatementList ) ) otherlv_4= 'END_WHILE' ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2348:1: (otherlv_0= 'WHILE' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= 'DO' ( (lv_statements_3_0= ruleStatementList ) ) otherlv_4= 'END_WHILE' )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2348:1: (otherlv_0= 'WHILE' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= 'DO' ( (lv_statements_3_0= ruleStatementList ) ) otherlv_4= 'END_WHILE' )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2348:3: otherlv_0= 'WHILE' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= 'DO' ( (lv_statements_3_0= ruleStatementList ) ) otherlv_4= 'END_WHILE'
            {
            otherlv_0=(Token)match(input,64,FOLLOW_64_in_ruleWhile_Statement5026); 

                	newLeafNode(otherlv_0, grammarAccess.getWhile_StatementAccess().getWHILEKeyword_0());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2352:1: ( (lv_expression_1_0= ruleExpression ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2353:1: (lv_expression_1_0= ruleExpression )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2353:1: (lv_expression_1_0= ruleExpression )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2354:3: lv_expression_1_0= ruleExpression
            {
             
            	        newCompositeNode(grammarAccess.getWhile_StatementAccess().getExpressionExpressionParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleExpression_in_ruleWhile_Statement5047);
            lv_expression_1_0=ruleExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getWhile_StatementRule());
            	        }
                   		set(
                   			current, 
                   			"expression",
                    		lv_expression_1_0, 
                    		"Expression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,59,FOLLOW_59_in_ruleWhile_Statement5059); 

                	newLeafNode(otherlv_2, grammarAccess.getWhile_StatementAccess().getDOKeyword_2());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2374:1: ( (lv_statements_3_0= ruleStatementList ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2375:1: (lv_statements_3_0= ruleStatementList )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2375:1: (lv_statements_3_0= ruleStatementList )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2376:3: lv_statements_3_0= ruleStatementList
            {
             
            	        newCompositeNode(grammarAccess.getWhile_StatementAccess().getStatementsStatementListParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_ruleStatementList_in_ruleWhile_Statement5080);
            lv_statements_3_0=ruleStatementList();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getWhile_StatementRule());
            	        }
                   		set(
                   			current, 
                   			"statements",
                    		lv_statements_3_0, 
                    		"StatementList");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,65,FOLLOW_65_in_ruleWhile_Statement5092); 

                	newLeafNode(otherlv_4, grammarAccess.getWhile_StatementAccess().getEND_WHILEKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWhile_Statement"


    // $ANTLR start "entryRuleSubprog_Ctrl_Stmt"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2404:1: entryRuleSubprog_Ctrl_Stmt returns [EObject current=null] : iv_ruleSubprog_Ctrl_Stmt= ruleSubprog_Ctrl_Stmt EOF ;
    public final EObject entryRuleSubprog_Ctrl_Stmt() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSubprog_Ctrl_Stmt = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2405:2: (iv_ruleSubprog_Ctrl_Stmt= ruleSubprog_Ctrl_Stmt EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2406:2: iv_ruleSubprog_Ctrl_Stmt= ruleSubprog_Ctrl_Stmt EOF
            {
             newCompositeNode(grammarAccess.getSubprog_Ctrl_StmtRule()); 
            pushFollow(FOLLOW_ruleSubprog_Ctrl_Stmt_in_entryRuleSubprog_Ctrl_Stmt5128);
            iv_ruleSubprog_Ctrl_Stmt=ruleSubprog_Ctrl_Stmt();

            state._fsp--;

             current =iv_ruleSubprog_Ctrl_Stmt; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSubprog_Ctrl_Stmt5138); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSubprog_Ctrl_Stmt"


    // $ANTLR start "ruleSubprog_Ctrl_Stmt"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2413:1: ruleSubprog_Ctrl_Stmt returns [EObject current=null] : ( (lv_call_0_0= ruleFunc_Call ) ) ;
    public final EObject ruleSubprog_Ctrl_Stmt() throws RecognitionException {
        EObject current = null;

        EObject lv_call_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2416:28: ( ( (lv_call_0_0= ruleFunc_Call ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2417:1: ( (lv_call_0_0= ruleFunc_Call ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2417:1: ( (lv_call_0_0= ruleFunc_Call ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2418:1: (lv_call_0_0= ruleFunc_Call )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2418:1: (lv_call_0_0= ruleFunc_Call )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2419:3: lv_call_0_0= ruleFunc_Call
            {
             
            	        newCompositeNode(grammarAccess.getSubprog_Ctrl_StmtAccess().getCallFunc_CallParserRuleCall_0()); 
            	    
            pushFollow(FOLLOW_ruleFunc_Call_in_ruleSubprog_Ctrl_Stmt5183);
            lv_call_0_0=ruleFunc_Call();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSubprog_Ctrl_StmtRule());
            	        }
                   		set(
                   			current, 
                   			"call",
                    		lv_call_0_0, 
                    		"Func_Call");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSubprog_Ctrl_Stmt"


    // $ANTLR start "entryRuleExpression"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2443:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2444:2: (iv_ruleExpression= ruleExpression EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2445:2: iv_ruleExpression= ruleExpression EOF
            {
             newCompositeNode(grammarAccess.getExpressionRule()); 
            pushFollow(FOLLOW_ruleExpression_in_entryRuleExpression5218);
            iv_ruleExpression=ruleExpression();

            state._fsp--;

             current =iv_ruleExpression; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleExpression5228); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2452:1: ruleExpression returns [EObject current=null] : (this_Xor_Expression_0= ruleXor_Expression ( () ( (lv_operator_2_0= 'OR' ) ) ( (lv_right_3_0= ruleXor_Expression ) ) )* ) ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_0=null;
        EObject this_Xor_Expression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2455:28: ( (this_Xor_Expression_0= ruleXor_Expression ( () ( (lv_operator_2_0= 'OR' ) ) ( (lv_right_3_0= ruleXor_Expression ) ) )* ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2456:1: (this_Xor_Expression_0= ruleXor_Expression ( () ( (lv_operator_2_0= 'OR' ) ) ( (lv_right_3_0= ruleXor_Expression ) ) )* )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2456:1: (this_Xor_Expression_0= ruleXor_Expression ( () ( (lv_operator_2_0= 'OR' ) ) ( (lv_right_3_0= ruleXor_Expression ) ) )* )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2457:5: this_Xor_Expression_0= ruleXor_Expression ( () ( (lv_operator_2_0= 'OR' ) ) ( (lv_right_3_0= ruleXor_Expression ) ) )*
            {
             
                    newCompositeNode(grammarAccess.getExpressionAccess().getXor_ExpressionParserRuleCall_0()); 
                
            pushFollow(FOLLOW_ruleXor_Expression_in_ruleExpression5275);
            this_Xor_Expression_0=ruleXor_Expression();

            state._fsp--;

             
                    current = this_Xor_Expression_0; 
                    afterParserOrEnumRuleCall();
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2465:1: ( () ( (lv_operator_2_0= 'OR' ) ) ( (lv_right_3_0= ruleXor_Expression ) ) )*
            loop40:
            do {
                int alt40=2;
                int LA40_0 = input.LA(1);

                if ( (LA40_0==66) ) {
                    alt40=1;
                }


                switch (alt40) {
            	case 1 :
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2465:2: () ( (lv_operator_2_0= 'OR' ) ) ( (lv_right_3_0= ruleXor_Expression ) )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2465:2: ()
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2466:5: 
            	    {

            	            current = forceCreateModelElementAndSet(
            	                grammarAccess.getExpressionAccess().getExpressionLeftAction_1_0(),
            	                current);
            	        

            	    }

            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2471:2: ( (lv_operator_2_0= 'OR' ) )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2472:1: (lv_operator_2_0= 'OR' )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2472:1: (lv_operator_2_0= 'OR' )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2473:3: lv_operator_2_0= 'OR'
            	    {
            	    lv_operator_2_0=(Token)match(input,66,FOLLOW_66_in_ruleExpression5302); 

            	            newLeafNode(lv_operator_2_0, grammarAccess.getExpressionAccess().getOperatorORKeyword_1_1_0());
            	        

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getExpressionRule());
            	    	        }
            	           		setWithLastConsumed(current, "operator", lv_operator_2_0, "OR");
            	    	    

            	    }


            	    }

            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2486:2: ( (lv_right_3_0= ruleXor_Expression ) )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2487:1: (lv_right_3_0= ruleXor_Expression )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2487:1: (lv_right_3_0= ruleXor_Expression )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2488:3: lv_right_3_0= ruleXor_Expression
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getExpressionAccess().getRightXor_ExpressionParserRuleCall_1_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleXor_Expression_in_ruleExpression5336);
            	    lv_right_3_0=ruleXor_Expression();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getExpressionRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"right",
            	            		lv_right_3_0, 
            	            		"Xor_Expression");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop40;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleXor_Expression"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2512:1: entryRuleXor_Expression returns [EObject current=null] : iv_ruleXor_Expression= ruleXor_Expression EOF ;
    public final EObject entryRuleXor_Expression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXor_Expression = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2513:2: (iv_ruleXor_Expression= ruleXor_Expression EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2514:2: iv_ruleXor_Expression= ruleXor_Expression EOF
            {
             newCompositeNode(grammarAccess.getXor_ExpressionRule()); 
            pushFollow(FOLLOW_ruleXor_Expression_in_entryRuleXor_Expression5374);
            iv_ruleXor_Expression=ruleXor_Expression();

            state._fsp--;

             current =iv_ruleXor_Expression; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleXor_Expression5384); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXor_Expression"


    // $ANTLR start "ruleXor_Expression"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2521:1: ruleXor_Expression returns [EObject current=null] : (this_And_Expression_0= ruleAnd_Expression ( () ( (lv_operator_2_0= 'XOR' ) ) ( (lv_right_3_0= ruleAnd_Expression ) ) )* ) ;
    public final EObject ruleXor_Expression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_0=null;
        EObject this_And_Expression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2524:28: ( (this_And_Expression_0= ruleAnd_Expression ( () ( (lv_operator_2_0= 'XOR' ) ) ( (lv_right_3_0= ruleAnd_Expression ) ) )* ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2525:1: (this_And_Expression_0= ruleAnd_Expression ( () ( (lv_operator_2_0= 'XOR' ) ) ( (lv_right_3_0= ruleAnd_Expression ) ) )* )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2525:1: (this_And_Expression_0= ruleAnd_Expression ( () ( (lv_operator_2_0= 'XOR' ) ) ( (lv_right_3_0= ruleAnd_Expression ) ) )* )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2526:5: this_And_Expression_0= ruleAnd_Expression ( () ( (lv_operator_2_0= 'XOR' ) ) ( (lv_right_3_0= ruleAnd_Expression ) ) )*
            {
             
                    newCompositeNode(grammarAccess.getXor_ExpressionAccess().getAnd_ExpressionParserRuleCall_0()); 
                
            pushFollow(FOLLOW_ruleAnd_Expression_in_ruleXor_Expression5431);
            this_And_Expression_0=ruleAnd_Expression();

            state._fsp--;

             
                    current = this_And_Expression_0; 
                    afterParserOrEnumRuleCall();
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2534:1: ( () ( (lv_operator_2_0= 'XOR' ) ) ( (lv_right_3_0= ruleAnd_Expression ) ) )*
            loop41:
            do {
                int alt41=2;
                int LA41_0 = input.LA(1);

                if ( (LA41_0==67) ) {
                    alt41=1;
                }


                switch (alt41) {
            	case 1 :
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2534:2: () ( (lv_operator_2_0= 'XOR' ) ) ( (lv_right_3_0= ruleAnd_Expression ) )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2534:2: ()
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2535:5: 
            	    {

            	            current = forceCreateModelElementAndSet(
            	                grammarAccess.getXor_ExpressionAccess().getXor_ExpressionLeftAction_1_0(),
            	                current);
            	        

            	    }

            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2540:2: ( (lv_operator_2_0= 'XOR' ) )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2541:1: (lv_operator_2_0= 'XOR' )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2541:1: (lv_operator_2_0= 'XOR' )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2542:3: lv_operator_2_0= 'XOR'
            	    {
            	    lv_operator_2_0=(Token)match(input,67,FOLLOW_67_in_ruleXor_Expression5458); 

            	            newLeafNode(lv_operator_2_0, grammarAccess.getXor_ExpressionAccess().getOperatorXORKeyword_1_1_0());
            	        

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getXor_ExpressionRule());
            	    	        }
            	           		setWithLastConsumed(current, "operator", lv_operator_2_0, "XOR");
            	    	    

            	    }


            	    }

            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2555:2: ( (lv_right_3_0= ruleAnd_Expression ) )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2556:1: (lv_right_3_0= ruleAnd_Expression )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2556:1: (lv_right_3_0= ruleAnd_Expression )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2557:3: lv_right_3_0= ruleAnd_Expression
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getXor_ExpressionAccess().getRightAnd_ExpressionParserRuleCall_1_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleAnd_Expression_in_ruleXor_Expression5492);
            	    lv_right_3_0=ruleAnd_Expression();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getXor_ExpressionRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"right",
            	            		lv_right_3_0, 
            	            		"And_Expression");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop41;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXor_Expression"


    // $ANTLR start "entryRuleAnd_Expression"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2581:1: entryRuleAnd_Expression returns [EObject current=null] : iv_ruleAnd_Expression= ruleAnd_Expression EOF ;
    public final EObject entryRuleAnd_Expression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnd_Expression = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2582:2: (iv_ruleAnd_Expression= ruleAnd_Expression EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2583:2: iv_ruleAnd_Expression= ruleAnd_Expression EOF
            {
             newCompositeNode(grammarAccess.getAnd_ExpressionRule()); 
            pushFollow(FOLLOW_ruleAnd_Expression_in_entryRuleAnd_Expression5530);
            iv_ruleAnd_Expression=ruleAnd_Expression();

            state._fsp--;

             current =iv_ruleAnd_Expression; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAnd_Expression5540); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnd_Expression"


    // $ANTLR start "ruleAnd_Expression"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2590:1: ruleAnd_Expression returns [EObject current=null] : (this_Comparison_0= ruleComparison ( () ( ( (lv_operator_2_1= '&' | lv_operator_2_2= 'AND' ) ) ) ( (lv_right_3_0= ruleComparison ) ) )* ) ;
    public final EObject ruleAnd_Expression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_1=null;
        Token lv_operator_2_2=null;
        EObject this_Comparison_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2593:28: ( (this_Comparison_0= ruleComparison ( () ( ( (lv_operator_2_1= '&' | lv_operator_2_2= 'AND' ) ) ) ( (lv_right_3_0= ruleComparison ) ) )* ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2594:1: (this_Comparison_0= ruleComparison ( () ( ( (lv_operator_2_1= '&' | lv_operator_2_2= 'AND' ) ) ) ( (lv_right_3_0= ruleComparison ) ) )* )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2594:1: (this_Comparison_0= ruleComparison ( () ( ( (lv_operator_2_1= '&' | lv_operator_2_2= 'AND' ) ) ) ( (lv_right_3_0= ruleComparison ) ) )* )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2595:5: this_Comparison_0= ruleComparison ( () ( ( (lv_operator_2_1= '&' | lv_operator_2_2= 'AND' ) ) ) ( (lv_right_3_0= ruleComparison ) ) )*
            {
             
                    newCompositeNode(grammarAccess.getAnd_ExpressionAccess().getComparisonParserRuleCall_0()); 
                
            pushFollow(FOLLOW_ruleComparison_in_ruleAnd_Expression5587);
            this_Comparison_0=ruleComparison();

            state._fsp--;

             
                    current = this_Comparison_0; 
                    afterParserOrEnumRuleCall();
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2603:1: ( () ( ( (lv_operator_2_1= '&' | lv_operator_2_2= 'AND' ) ) ) ( (lv_right_3_0= ruleComparison ) ) )*
            loop43:
            do {
                int alt43=2;
                int LA43_0 = input.LA(1);

                if ( ((LA43_0>=68 && LA43_0<=69)) ) {
                    alt43=1;
                }


                switch (alt43) {
            	case 1 :
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2603:2: () ( ( (lv_operator_2_1= '&' | lv_operator_2_2= 'AND' ) ) ) ( (lv_right_3_0= ruleComparison ) )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2603:2: ()
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2604:5: 
            	    {

            	            current = forceCreateModelElementAndSet(
            	                grammarAccess.getAnd_ExpressionAccess().getAnd_ExpressionLeftAction_1_0(),
            	                current);
            	        

            	    }

            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2609:2: ( ( (lv_operator_2_1= '&' | lv_operator_2_2= 'AND' ) ) )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2610:1: ( (lv_operator_2_1= '&' | lv_operator_2_2= 'AND' ) )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2610:1: ( (lv_operator_2_1= '&' | lv_operator_2_2= 'AND' ) )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2611:1: (lv_operator_2_1= '&' | lv_operator_2_2= 'AND' )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2611:1: (lv_operator_2_1= '&' | lv_operator_2_2= 'AND' )
            	    int alt42=2;
            	    int LA42_0 = input.LA(1);

            	    if ( (LA42_0==68) ) {
            	        alt42=1;
            	    }
            	    else if ( (LA42_0==69) ) {
            	        alt42=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 42, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt42) {
            	        case 1 :
            	            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2612:3: lv_operator_2_1= '&'
            	            {
            	            lv_operator_2_1=(Token)match(input,68,FOLLOW_68_in_ruleAnd_Expression5616); 

            	                    newLeafNode(lv_operator_2_1, grammarAccess.getAnd_ExpressionAccess().getOperatorAmpersandKeyword_1_1_0_0());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getAnd_ExpressionRule());
            	            	        }
            	                   		setWithLastConsumed(current, "operator", lv_operator_2_1, null);
            	            	    

            	            }
            	            break;
            	        case 2 :
            	            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2624:8: lv_operator_2_2= 'AND'
            	            {
            	            lv_operator_2_2=(Token)match(input,69,FOLLOW_69_in_ruleAnd_Expression5645); 

            	                    newLeafNode(lv_operator_2_2, grammarAccess.getAnd_ExpressionAccess().getOperatorANDKeyword_1_1_0_1());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getAnd_ExpressionRule());
            	            	        }
            	                   		setWithLastConsumed(current, "operator", lv_operator_2_2, null);
            	            	    

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2639:2: ( (lv_right_3_0= ruleComparison ) )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2640:1: (lv_right_3_0= ruleComparison )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2640:1: (lv_right_3_0= ruleComparison )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2641:3: lv_right_3_0= ruleComparison
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getAnd_ExpressionAccess().getRightComparisonParserRuleCall_1_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleComparison_in_ruleAnd_Expression5682);
            	    lv_right_3_0=ruleComparison();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getAnd_ExpressionRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"right",
            	            		lv_right_3_0, 
            	            		"Comparison");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop43;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnd_Expression"


    // $ANTLR start "entryRuleComparison"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2665:1: entryRuleComparison returns [EObject current=null] : iv_ruleComparison= ruleComparison EOF ;
    public final EObject entryRuleComparison() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComparison = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2666:2: (iv_ruleComparison= ruleComparison EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2667:2: iv_ruleComparison= ruleComparison EOF
            {
             newCompositeNode(grammarAccess.getComparisonRule()); 
            pushFollow(FOLLOW_ruleComparison_in_entryRuleComparison5720);
            iv_ruleComparison=ruleComparison();

            state._fsp--;

             current =iv_ruleComparison; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleComparison5730); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComparison"


    // $ANTLR start "ruleComparison"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2674:1: ruleComparison returns [EObject current=null] : (this_Equ_Expression_0= ruleEqu_Expression ( () ( ( (lv_operator_2_1= '=' | lv_operator_2_2= '<>' ) ) ) ( (lv_right_3_0= ruleEqu_Expression ) ) )* ) ;
    public final EObject ruleComparison() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_1=null;
        Token lv_operator_2_2=null;
        EObject this_Equ_Expression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2677:28: ( (this_Equ_Expression_0= ruleEqu_Expression ( () ( ( (lv_operator_2_1= '=' | lv_operator_2_2= '<>' ) ) ) ( (lv_right_3_0= ruleEqu_Expression ) ) )* ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2678:1: (this_Equ_Expression_0= ruleEqu_Expression ( () ( ( (lv_operator_2_1= '=' | lv_operator_2_2= '<>' ) ) ) ( (lv_right_3_0= ruleEqu_Expression ) ) )* )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2678:1: (this_Equ_Expression_0= ruleEqu_Expression ( () ( ( (lv_operator_2_1= '=' | lv_operator_2_2= '<>' ) ) ) ( (lv_right_3_0= ruleEqu_Expression ) ) )* )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2679:5: this_Equ_Expression_0= ruleEqu_Expression ( () ( ( (lv_operator_2_1= '=' | lv_operator_2_2= '<>' ) ) ) ( (lv_right_3_0= ruleEqu_Expression ) ) )*
            {
             
                    newCompositeNode(grammarAccess.getComparisonAccess().getEqu_ExpressionParserRuleCall_0()); 
                
            pushFollow(FOLLOW_ruleEqu_Expression_in_ruleComparison5777);
            this_Equ_Expression_0=ruleEqu_Expression();

            state._fsp--;

             
                    current = this_Equ_Expression_0; 
                    afterParserOrEnumRuleCall();
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2687:1: ( () ( ( (lv_operator_2_1= '=' | lv_operator_2_2= '<>' ) ) ) ( (lv_right_3_0= ruleEqu_Expression ) ) )*
            loop45:
            do {
                int alt45=2;
                int LA45_0 = input.LA(1);

                if ( ((LA45_0>=70 && LA45_0<=71)) ) {
                    alt45=1;
                }


                switch (alt45) {
            	case 1 :
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2687:2: () ( ( (lv_operator_2_1= '=' | lv_operator_2_2= '<>' ) ) ) ( (lv_right_3_0= ruleEqu_Expression ) )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2687:2: ()
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2688:5: 
            	    {

            	            current = forceCreateModelElementAndSet(
            	                grammarAccess.getComparisonAccess().getComparisonLeftAction_1_0(),
            	                current);
            	        

            	    }

            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2693:2: ( ( (lv_operator_2_1= '=' | lv_operator_2_2= '<>' ) ) )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2694:1: ( (lv_operator_2_1= '=' | lv_operator_2_2= '<>' ) )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2694:1: ( (lv_operator_2_1= '=' | lv_operator_2_2= '<>' ) )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2695:1: (lv_operator_2_1= '=' | lv_operator_2_2= '<>' )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2695:1: (lv_operator_2_1= '=' | lv_operator_2_2= '<>' )
            	    int alt44=2;
            	    int LA44_0 = input.LA(1);

            	    if ( (LA44_0==70) ) {
            	        alt44=1;
            	    }
            	    else if ( (LA44_0==71) ) {
            	        alt44=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 44, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt44) {
            	        case 1 :
            	            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2696:3: lv_operator_2_1= '='
            	            {
            	            lv_operator_2_1=(Token)match(input,70,FOLLOW_70_in_ruleComparison5806); 

            	                    newLeafNode(lv_operator_2_1, grammarAccess.getComparisonAccess().getOperatorEqualsSignKeyword_1_1_0_0());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getComparisonRule());
            	            	        }
            	                   		setWithLastConsumed(current, "operator", lv_operator_2_1, null);
            	            	    

            	            }
            	            break;
            	        case 2 :
            	            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2708:8: lv_operator_2_2= '<>'
            	            {
            	            lv_operator_2_2=(Token)match(input,71,FOLLOW_71_in_ruleComparison5835); 

            	                    newLeafNode(lv_operator_2_2, grammarAccess.getComparisonAccess().getOperatorLessThanSignGreaterThanSignKeyword_1_1_0_1());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getComparisonRule());
            	            	        }
            	                   		setWithLastConsumed(current, "operator", lv_operator_2_2, null);
            	            	    

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2723:2: ( (lv_right_3_0= ruleEqu_Expression ) )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2724:1: (lv_right_3_0= ruleEqu_Expression )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2724:1: (lv_right_3_0= ruleEqu_Expression )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2725:3: lv_right_3_0= ruleEqu_Expression
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getComparisonAccess().getRightEqu_ExpressionParserRuleCall_1_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleEqu_Expression_in_ruleComparison5872);
            	    lv_right_3_0=ruleEqu_Expression();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getComparisonRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"right",
            	            		lv_right_3_0, 
            	            		"Equ_Expression");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop45;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComparison"


    // $ANTLR start "entryRuleEqu_Expression"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2749:1: entryRuleEqu_Expression returns [EObject current=null] : iv_ruleEqu_Expression= ruleEqu_Expression EOF ;
    public final EObject entryRuleEqu_Expression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEqu_Expression = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2750:2: (iv_ruleEqu_Expression= ruleEqu_Expression EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2751:2: iv_ruleEqu_Expression= ruleEqu_Expression EOF
            {
             newCompositeNode(grammarAccess.getEqu_ExpressionRule()); 
            pushFollow(FOLLOW_ruleEqu_Expression_in_entryRuleEqu_Expression5910);
            iv_ruleEqu_Expression=ruleEqu_Expression();

            state._fsp--;

             current =iv_ruleEqu_Expression; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleEqu_Expression5920); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEqu_Expression"


    // $ANTLR start "ruleEqu_Expression"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2758:1: ruleEqu_Expression returns [EObject current=null] : (this_Add_Expression_0= ruleAdd_Expression ( () ( ( (lv_operator_2_1= '<' | lv_operator_2_2= '<=' | lv_operator_2_3= '>' | lv_operator_2_4= '>=' ) ) ) ( (lv_right_3_0= ruleAdd_Expression ) ) )* ) ;
    public final EObject ruleEqu_Expression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_1=null;
        Token lv_operator_2_2=null;
        Token lv_operator_2_3=null;
        Token lv_operator_2_4=null;
        EObject this_Add_Expression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2761:28: ( (this_Add_Expression_0= ruleAdd_Expression ( () ( ( (lv_operator_2_1= '<' | lv_operator_2_2= '<=' | lv_operator_2_3= '>' | lv_operator_2_4= '>=' ) ) ) ( (lv_right_3_0= ruleAdd_Expression ) ) )* ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2762:1: (this_Add_Expression_0= ruleAdd_Expression ( () ( ( (lv_operator_2_1= '<' | lv_operator_2_2= '<=' | lv_operator_2_3= '>' | lv_operator_2_4= '>=' ) ) ) ( (lv_right_3_0= ruleAdd_Expression ) ) )* )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2762:1: (this_Add_Expression_0= ruleAdd_Expression ( () ( ( (lv_operator_2_1= '<' | lv_operator_2_2= '<=' | lv_operator_2_3= '>' | lv_operator_2_4= '>=' ) ) ) ( (lv_right_3_0= ruleAdd_Expression ) ) )* )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2763:5: this_Add_Expression_0= ruleAdd_Expression ( () ( ( (lv_operator_2_1= '<' | lv_operator_2_2= '<=' | lv_operator_2_3= '>' | lv_operator_2_4= '>=' ) ) ) ( (lv_right_3_0= ruleAdd_Expression ) ) )*
            {
             
                    newCompositeNode(grammarAccess.getEqu_ExpressionAccess().getAdd_ExpressionParserRuleCall_0()); 
                
            pushFollow(FOLLOW_ruleAdd_Expression_in_ruleEqu_Expression5967);
            this_Add_Expression_0=ruleAdd_Expression();

            state._fsp--;

             
                    current = this_Add_Expression_0; 
                    afterParserOrEnumRuleCall();
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2771:1: ( () ( ( (lv_operator_2_1= '<' | lv_operator_2_2= '<=' | lv_operator_2_3= '>' | lv_operator_2_4= '>=' ) ) ) ( (lv_right_3_0= ruleAdd_Expression ) ) )*
            loop47:
            do {
                int alt47=2;
                int LA47_0 = input.LA(1);

                if ( ((LA47_0>=72 && LA47_0<=75)) ) {
                    alt47=1;
                }


                switch (alt47) {
            	case 1 :
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2771:2: () ( ( (lv_operator_2_1= '<' | lv_operator_2_2= '<=' | lv_operator_2_3= '>' | lv_operator_2_4= '>=' ) ) ) ( (lv_right_3_0= ruleAdd_Expression ) )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2771:2: ()
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2772:5: 
            	    {

            	            current = forceCreateModelElementAndSet(
            	                grammarAccess.getEqu_ExpressionAccess().getEqu_ExpressionLeftAction_1_0(),
            	                current);
            	        

            	    }

            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2777:2: ( ( (lv_operator_2_1= '<' | lv_operator_2_2= '<=' | lv_operator_2_3= '>' | lv_operator_2_4= '>=' ) ) )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2778:1: ( (lv_operator_2_1= '<' | lv_operator_2_2= '<=' | lv_operator_2_3= '>' | lv_operator_2_4= '>=' ) )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2778:1: ( (lv_operator_2_1= '<' | lv_operator_2_2= '<=' | lv_operator_2_3= '>' | lv_operator_2_4= '>=' ) )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2779:1: (lv_operator_2_1= '<' | lv_operator_2_2= '<=' | lv_operator_2_3= '>' | lv_operator_2_4= '>=' )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2779:1: (lv_operator_2_1= '<' | lv_operator_2_2= '<=' | lv_operator_2_3= '>' | lv_operator_2_4= '>=' )
            	    int alt46=4;
            	    switch ( input.LA(1) ) {
            	    case 72:
            	        {
            	        alt46=1;
            	        }
            	        break;
            	    case 73:
            	        {
            	        alt46=2;
            	        }
            	        break;
            	    case 74:
            	        {
            	        alt46=3;
            	        }
            	        break;
            	    case 75:
            	        {
            	        alt46=4;
            	        }
            	        break;
            	    default:
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 46, 0, input);

            	        throw nvae;
            	    }

            	    switch (alt46) {
            	        case 1 :
            	            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2780:3: lv_operator_2_1= '<'
            	            {
            	            lv_operator_2_1=(Token)match(input,72,FOLLOW_72_in_ruleEqu_Expression5996); 

            	                    newLeafNode(lv_operator_2_1, grammarAccess.getEqu_ExpressionAccess().getOperatorLessThanSignKeyword_1_1_0_0());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getEqu_ExpressionRule());
            	            	        }
            	                   		setWithLastConsumed(current, "operator", lv_operator_2_1, null);
            	            	    

            	            }
            	            break;
            	        case 2 :
            	            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2792:8: lv_operator_2_2= '<='
            	            {
            	            lv_operator_2_2=(Token)match(input,73,FOLLOW_73_in_ruleEqu_Expression6025); 

            	                    newLeafNode(lv_operator_2_2, grammarAccess.getEqu_ExpressionAccess().getOperatorLessThanSignEqualsSignKeyword_1_1_0_1());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getEqu_ExpressionRule());
            	            	        }
            	                   		setWithLastConsumed(current, "operator", lv_operator_2_2, null);
            	            	    

            	            }
            	            break;
            	        case 3 :
            	            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2804:8: lv_operator_2_3= '>'
            	            {
            	            lv_operator_2_3=(Token)match(input,74,FOLLOW_74_in_ruleEqu_Expression6054); 

            	                    newLeafNode(lv_operator_2_3, grammarAccess.getEqu_ExpressionAccess().getOperatorGreaterThanSignKeyword_1_1_0_2());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getEqu_ExpressionRule());
            	            	        }
            	                   		setWithLastConsumed(current, "operator", lv_operator_2_3, null);
            	            	    

            	            }
            	            break;
            	        case 4 :
            	            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2816:8: lv_operator_2_4= '>='
            	            {
            	            lv_operator_2_4=(Token)match(input,75,FOLLOW_75_in_ruleEqu_Expression6083); 

            	                    newLeafNode(lv_operator_2_4, grammarAccess.getEqu_ExpressionAccess().getOperatorGreaterThanSignEqualsSignKeyword_1_1_0_3());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getEqu_ExpressionRule());
            	            	        }
            	                   		setWithLastConsumed(current, "operator", lv_operator_2_4, null);
            	            	    

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2831:2: ( (lv_right_3_0= ruleAdd_Expression ) )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2832:1: (lv_right_3_0= ruleAdd_Expression )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2832:1: (lv_right_3_0= ruleAdd_Expression )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2833:3: lv_right_3_0= ruleAdd_Expression
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getEqu_ExpressionAccess().getRightAdd_ExpressionParserRuleCall_1_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleAdd_Expression_in_ruleEqu_Expression6120);
            	    lv_right_3_0=ruleAdd_Expression();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getEqu_ExpressionRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"right",
            	            		lv_right_3_0, 
            	            		"Add_Expression");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop47;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEqu_Expression"


    // $ANTLR start "entryRuleAdd_Expression"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2857:1: entryRuleAdd_Expression returns [EObject current=null] : iv_ruleAdd_Expression= ruleAdd_Expression EOF ;
    public final EObject entryRuleAdd_Expression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAdd_Expression = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2858:2: (iv_ruleAdd_Expression= ruleAdd_Expression EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2859:2: iv_ruleAdd_Expression= ruleAdd_Expression EOF
            {
             newCompositeNode(grammarAccess.getAdd_ExpressionRule()); 
            pushFollow(FOLLOW_ruleAdd_Expression_in_entryRuleAdd_Expression6158);
            iv_ruleAdd_Expression=ruleAdd_Expression();

            state._fsp--;

             current =iv_ruleAdd_Expression; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAdd_Expression6168); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAdd_Expression"


    // $ANTLR start "ruleAdd_Expression"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2866:1: ruleAdd_Expression returns [EObject current=null] : (this_Term_0= ruleTerm ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleTerm ) ) )* ) ;
    public final EObject ruleAdd_Expression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_1=null;
        Token lv_operator_2_2=null;
        EObject this_Term_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2869:28: ( (this_Term_0= ruleTerm ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleTerm ) ) )* ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2870:1: (this_Term_0= ruleTerm ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleTerm ) ) )* )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2870:1: (this_Term_0= ruleTerm ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleTerm ) ) )* )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2871:5: this_Term_0= ruleTerm ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleTerm ) ) )*
            {
             
                    newCompositeNode(grammarAccess.getAdd_ExpressionAccess().getTermParserRuleCall_0()); 
                
            pushFollow(FOLLOW_ruleTerm_in_ruleAdd_Expression6215);
            this_Term_0=ruleTerm();

            state._fsp--;

             
                    current = this_Term_0; 
                    afterParserOrEnumRuleCall();
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2879:1: ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleTerm ) ) )*
            loop49:
            do {
                int alt49=2;
                int LA49_0 = input.LA(1);

                if ( ((LA49_0>=76 && LA49_0<=77)) ) {
                    alt49=1;
                }


                switch (alt49) {
            	case 1 :
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2879:2: () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleTerm ) )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2879:2: ()
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2880:5: 
            	    {

            	            current = forceCreateModelElementAndSet(
            	                grammarAccess.getAdd_ExpressionAccess().getAdd_ExpressionLeftAction_1_0(),
            	                current);
            	        

            	    }

            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2885:2: ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2886:1: ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2886:1: ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2887:1: (lv_operator_2_1= '+' | lv_operator_2_2= '-' )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2887:1: (lv_operator_2_1= '+' | lv_operator_2_2= '-' )
            	    int alt48=2;
            	    int LA48_0 = input.LA(1);

            	    if ( (LA48_0==76) ) {
            	        alt48=1;
            	    }
            	    else if ( (LA48_0==77) ) {
            	        alt48=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 48, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt48) {
            	        case 1 :
            	            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2888:3: lv_operator_2_1= '+'
            	            {
            	            lv_operator_2_1=(Token)match(input,76,FOLLOW_76_in_ruleAdd_Expression6244); 

            	                    newLeafNode(lv_operator_2_1, grammarAccess.getAdd_ExpressionAccess().getOperatorPlusSignKeyword_1_1_0_0());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getAdd_ExpressionRule());
            	            	        }
            	                   		setWithLastConsumed(current, "operator", lv_operator_2_1, null);
            	            	    

            	            }
            	            break;
            	        case 2 :
            	            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2900:8: lv_operator_2_2= '-'
            	            {
            	            lv_operator_2_2=(Token)match(input,77,FOLLOW_77_in_ruleAdd_Expression6273); 

            	                    newLeafNode(lv_operator_2_2, grammarAccess.getAdd_ExpressionAccess().getOperatorHyphenMinusKeyword_1_1_0_1());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getAdd_ExpressionRule());
            	            	        }
            	                   		setWithLastConsumed(current, "operator", lv_operator_2_2, null);
            	            	    

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2915:2: ( (lv_right_3_0= ruleTerm ) )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2916:1: (lv_right_3_0= ruleTerm )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2916:1: (lv_right_3_0= ruleTerm )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2917:3: lv_right_3_0= ruleTerm
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getAdd_ExpressionAccess().getRightTermParserRuleCall_1_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleTerm_in_ruleAdd_Expression6310);
            	    lv_right_3_0=ruleTerm();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getAdd_ExpressionRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"right",
            	            		lv_right_3_0, 
            	            		"Term");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop49;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAdd_Expression"


    // $ANTLR start "entryRuleTerm"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2941:1: entryRuleTerm returns [EObject current=null] : iv_ruleTerm= ruleTerm EOF ;
    public final EObject entryRuleTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTerm = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2942:2: (iv_ruleTerm= ruleTerm EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2943:2: iv_ruleTerm= ruleTerm EOF
            {
             newCompositeNode(grammarAccess.getTermRule()); 
            pushFollow(FOLLOW_ruleTerm_in_entryRuleTerm6348);
            iv_ruleTerm=ruleTerm();

            state._fsp--;

             current =iv_ruleTerm; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTerm6358); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTerm"


    // $ANTLR start "ruleTerm"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2950:1: ruleTerm returns [EObject current=null] : (this_Power_Expression_0= rulePower_Expression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' | lv_operator_2_3= 'MOD' ) ) ) ( (lv_right_3_0= rulePower_Expression ) ) )* ) ;
    public final EObject ruleTerm() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_1=null;
        Token lv_operator_2_2=null;
        Token lv_operator_2_3=null;
        EObject this_Power_Expression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2953:28: ( (this_Power_Expression_0= rulePower_Expression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' | lv_operator_2_3= 'MOD' ) ) ) ( (lv_right_3_0= rulePower_Expression ) ) )* ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2954:1: (this_Power_Expression_0= rulePower_Expression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' | lv_operator_2_3= 'MOD' ) ) ) ( (lv_right_3_0= rulePower_Expression ) ) )* )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2954:1: (this_Power_Expression_0= rulePower_Expression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' | lv_operator_2_3= 'MOD' ) ) ) ( (lv_right_3_0= rulePower_Expression ) ) )* )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2955:5: this_Power_Expression_0= rulePower_Expression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' | lv_operator_2_3= 'MOD' ) ) ) ( (lv_right_3_0= rulePower_Expression ) ) )*
            {
             
                    newCompositeNode(grammarAccess.getTermAccess().getPower_ExpressionParserRuleCall_0()); 
                
            pushFollow(FOLLOW_rulePower_Expression_in_ruleTerm6405);
            this_Power_Expression_0=rulePower_Expression();

            state._fsp--;

             
                    current = this_Power_Expression_0; 
                    afterParserOrEnumRuleCall();
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2963:1: ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' | lv_operator_2_3= 'MOD' ) ) ) ( (lv_right_3_0= rulePower_Expression ) ) )*
            loop51:
            do {
                int alt51=2;
                int LA51_0 = input.LA(1);

                if ( ((LA51_0>=78 && LA51_0<=80)) ) {
                    alt51=1;
                }


                switch (alt51) {
            	case 1 :
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2963:2: () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' | lv_operator_2_3= 'MOD' ) ) ) ( (lv_right_3_0= rulePower_Expression ) )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2963:2: ()
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2964:5: 
            	    {

            	            current = forceCreateModelElementAndSet(
            	                grammarAccess.getTermAccess().getTermLeftAction_1_0(),
            	                current);
            	        

            	    }

            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2969:2: ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' | lv_operator_2_3= 'MOD' ) ) )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2970:1: ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' | lv_operator_2_3= 'MOD' ) )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2970:1: ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' | lv_operator_2_3= 'MOD' ) )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2971:1: (lv_operator_2_1= '*' | lv_operator_2_2= '/' | lv_operator_2_3= 'MOD' )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2971:1: (lv_operator_2_1= '*' | lv_operator_2_2= '/' | lv_operator_2_3= 'MOD' )
            	    int alt50=3;
            	    switch ( input.LA(1) ) {
            	    case 78:
            	        {
            	        alt50=1;
            	        }
            	        break;
            	    case 79:
            	        {
            	        alt50=2;
            	        }
            	        break;
            	    case 80:
            	        {
            	        alt50=3;
            	        }
            	        break;
            	    default:
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 50, 0, input);

            	        throw nvae;
            	    }

            	    switch (alt50) {
            	        case 1 :
            	            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2972:3: lv_operator_2_1= '*'
            	            {
            	            lv_operator_2_1=(Token)match(input,78,FOLLOW_78_in_ruleTerm6434); 

            	                    newLeafNode(lv_operator_2_1, grammarAccess.getTermAccess().getOperatorAsteriskKeyword_1_1_0_0());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getTermRule());
            	            	        }
            	                   		setWithLastConsumed(current, "operator", lv_operator_2_1, null);
            	            	    

            	            }
            	            break;
            	        case 2 :
            	            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2984:8: lv_operator_2_2= '/'
            	            {
            	            lv_operator_2_2=(Token)match(input,79,FOLLOW_79_in_ruleTerm6463); 

            	                    newLeafNode(lv_operator_2_2, grammarAccess.getTermAccess().getOperatorSolidusKeyword_1_1_0_1());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getTermRule());
            	            	        }
            	                   		setWithLastConsumed(current, "operator", lv_operator_2_2, null);
            	            	    

            	            }
            	            break;
            	        case 3 :
            	            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:2996:8: lv_operator_2_3= 'MOD'
            	            {
            	            lv_operator_2_3=(Token)match(input,80,FOLLOW_80_in_ruleTerm6492); 

            	                    newLeafNode(lv_operator_2_3, grammarAccess.getTermAccess().getOperatorMODKeyword_1_1_0_2());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getTermRule());
            	            	        }
            	                   		setWithLastConsumed(current, "operator", lv_operator_2_3, null);
            	            	    

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3011:2: ( (lv_right_3_0= rulePower_Expression ) )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3012:1: (lv_right_3_0= rulePower_Expression )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3012:1: (lv_right_3_0= rulePower_Expression )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3013:3: lv_right_3_0= rulePower_Expression
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getTermAccess().getRightPower_ExpressionParserRuleCall_1_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulePower_Expression_in_ruleTerm6529);
            	    lv_right_3_0=rulePower_Expression();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getTermRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"right",
            	            		lv_right_3_0, 
            	            		"Power_Expression");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop51;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTerm"


    // $ANTLR start "entryRulePower_Expression"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3037:1: entryRulePower_Expression returns [EObject current=null] : iv_rulePower_Expression= rulePower_Expression EOF ;
    public final EObject entryRulePower_Expression() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePower_Expression = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3038:2: (iv_rulePower_Expression= rulePower_Expression EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3039:2: iv_rulePower_Expression= rulePower_Expression EOF
            {
             newCompositeNode(grammarAccess.getPower_ExpressionRule()); 
            pushFollow(FOLLOW_rulePower_Expression_in_entryRulePower_Expression6567);
            iv_rulePower_Expression=rulePower_Expression();

            state._fsp--;

             current =iv_rulePower_Expression; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePower_Expression6577); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePower_Expression"


    // $ANTLR start "rulePower_Expression"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3046:1: rulePower_Expression returns [EObject current=null] : (this_Unary_Expression_0= ruleUnary_Expression ( () ( (lv_operator_2_0= '**' ) ) ( (lv_right_3_0= ruleUnary_Expression ) ) )* ) ;
    public final EObject rulePower_Expression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_0=null;
        EObject this_Unary_Expression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3049:28: ( (this_Unary_Expression_0= ruleUnary_Expression ( () ( (lv_operator_2_0= '**' ) ) ( (lv_right_3_0= ruleUnary_Expression ) ) )* ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3050:1: (this_Unary_Expression_0= ruleUnary_Expression ( () ( (lv_operator_2_0= '**' ) ) ( (lv_right_3_0= ruleUnary_Expression ) ) )* )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3050:1: (this_Unary_Expression_0= ruleUnary_Expression ( () ( (lv_operator_2_0= '**' ) ) ( (lv_right_3_0= ruleUnary_Expression ) ) )* )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3051:5: this_Unary_Expression_0= ruleUnary_Expression ( () ( (lv_operator_2_0= '**' ) ) ( (lv_right_3_0= ruleUnary_Expression ) ) )*
            {
             
                    newCompositeNode(grammarAccess.getPower_ExpressionAccess().getUnary_ExpressionParserRuleCall_0()); 
                
            pushFollow(FOLLOW_ruleUnary_Expression_in_rulePower_Expression6624);
            this_Unary_Expression_0=ruleUnary_Expression();

            state._fsp--;

             
                    current = this_Unary_Expression_0; 
                    afterParserOrEnumRuleCall();
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3059:1: ( () ( (lv_operator_2_0= '**' ) ) ( (lv_right_3_0= ruleUnary_Expression ) ) )*
            loop52:
            do {
                int alt52=2;
                int LA52_0 = input.LA(1);

                if ( (LA52_0==81) ) {
                    alt52=1;
                }


                switch (alt52) {
            	case 1 :
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3059:2: () ( (lv_operator_2_0= '**' ) ) ( (lv_right_3_0= ruleUnary_Expression ) )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3059:2: ()
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3060:5: 
            	    {

            	            current = forceCreateModelElementAndSet(
            	                grammarAccess.getPower_ExpressionAccess().getPower_ExpressionLeftAction_1_0(),
            	                current);
            	        

            	    }

            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3065:2: ( (lv_operator_2_0= '**' ) )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3066:1: (lv_operator_2_0= '**' )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3066:1: (lv_operator_2_0= '**' )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3067:3: lv_operator_2_0= '**'
            	    {
            	    lv_operator_2_0=(Token)match(input,81,FOLLOW_81_in_rulePower_Expression6651); 

            	            newLeafNode(lv_operator_2_0, grammarAccess.getPower_ExpressionAccess().getOperatorAsteriskAsteriskKeyword_1_1_0());
            	        

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getPower_ExpressionRule());
            	    	        }
            	           		setWithLastConsumed(current, "operator", lv_operator_2_0, "**");
            	    	    

            	    }


            	    }

            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3080:2: ( (lv_right_3_0= ruleUnary_Expression ) )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3081:1: (lv_right_3_0= ruleUnary_Expression )
            	    {
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3081:1: (lv_right_3_0= ruleUnary_Expression )
            	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3082:3: lv_right_3_0= ruleUnary_Expression
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getPower_ExpressionAccess().getRightUnary_ExpressionParserRuleCall_1_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleUnary_Expression_in_rulePower_Expression6685);
            	    lv_right_3_0=ruleUnary_Expression();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getPower_ExpressionRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"right",
            	            		lv_right_3_0, 
            	            		"Unary_Expression");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop52;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePower_Expression"


    // $ANTLR start "entryRuleUnary_Expression"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3106:1: entryRuleUnary_Expression returns [EObject current=null] : iv_ruleUnary_Expression= ruleUnary_Expression EOF ;
    public final EObject entryRuleUnary_Expression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnary_Expression = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3107:2: (iv_ruleUnary_Expression= ruleUnary_Expression EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3108:2: iv_ruleUnary_Expression= ruleUnary_Expression EOF
            {
             newCompositeNode(grammarAccess.getUnary_ExpressionRule()); 
            pushFollow(FOLLOW_ruleUnary_Expression_in_entryRuleUnary_Expression6723);
            iv_ruleUnary_Expression=ruleUnary_Expression();

            state._fsp--;

             current =iv_ruleUnary_Expression; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleUnary_Expression6733); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnary_Expression"


    // $ANTLR start "ruleUnary_Expression"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3115:1: ruleUnary_Expression returns [EObject current=null] : ( () ( (lv_operator_1_0= 'NOT' ) )? ( (lv_expression_2_0= rulePrimary_Expression ) ) ) ;
    public final EObject ruleUnary_Expression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_1_0=null;
        EObject lv_expression_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3118:28: ( ( () ( (lv_operator_1_0= 'NOT' ) )? ( (lv_expression_2_0= rulePrimary_Expression ) ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3119:1: ( () ( (lv_operator_1_0= 'NOT' ) )? ( (lv_expression_2_0= rulePrimary_Expression ) ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3119:1: ( () ( (lv_operator_1_0= 'NOT' ) )? ( (lv_expression_2_0= rulePrimary_Expression ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3119:2: () ( (lv_operator_1_0= 'NOT' ) )? ( (lv_expression_2_0= rulePrimary_Expression ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3119:2: ()
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3120:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getUnary_ExpressionAccess().getExpressionAction_0(),
                        current);
                

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3125:2: ( (lv_operator_1_0= 'NOT' ) )?
            int alt53=2;
            int LA53_0 = input.LA(1);

            if ( (LA53_0==82) ) {
                alt53=1;
            }
            switch (alt53) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3126:1: (lv_operator_1_0= 'NOT' )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3126:1: (lv_operator_1_0= 'NOT' )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3127:3: lv_operator_1_0= 'NOT'
                    {
                    lv_operator_1_0=(Token)match(input,82,FOLLOW_82_in_ruleUnary_Expression6785); 

                            newLeafNode(lv_operator_1_0, grammarAccess.getUnary_ExpressionAccess().getOperatorNOTKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getUnary_ExpressionRule());
                    	        }
                           		setWithLastConsumed(current, "operator", lv_operator_1_0, "NOT");
                    	    

                    }


                    }
                    break;

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3140:3: ( (lv_expression_2_0= rulePrimary_Expression ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3141:1: (lv_expression_2_0= rulePrimary_Expression )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3141:1: (lv_expression_2_0= rulePrimary_Expression )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3142:3: lv_expression_2_0= rulePrimary_Expression
            {
             
            	        newCompositeNode(grammarAccess.getUnary_ExpressionAccess().getExpressionPrimary_ExpressionParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_rulePrimary_Expression_in_ruleUnary_Expression6820);
            lv_expression_2_0=rulePrimary_Expression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getUnary_ExpressionRule());
            	        }
                   		set(
                   			current, 
                   			"expression",
                    		lv_expression_2_0, 
                    		"Primary_Expression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnary_Expression"


    // $ANTLR start "entryRulePrimary_Expression"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3166:1: entryRulePrimary_Expression returns [EObject current=null] : iv_rulePrimary_Expression= rulePrimary_Expression EOF ;
    public final EObject entryRulePrimary_Expression() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimary_Expression = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3167:2: (iv_rulePrimary_Expression= rulePrimary_Expression EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3168:2: iv_rulePrimary_Expression= rulePrimary_Expression EOF
            {
             newCompositeNode(grammarAccess.getPrimary_ExpressionRule()); 
            pushFollow(FOLLOW_rulePrimary_Expression_in_entryRulePrimary_Expression6856);
            iv_rulePrimary_Expression=rulePrimary_Expression();

            state._fsp--;

             current =iv_rulePrimary_Expression; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePrimary_Expression6866); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimary_Expression"


    // $ANTLR start "rulePrimary_Expression"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3175:1: rulePrimary_Expression returns [EObject current=null] : (this_Constant_0= ruleConstant | ( (otherlv_1= '-' )? this_Variable_2= ruleVariable ) | this_Func_Call_3= ruleFunc_Call | (otherlv_4= '(' this_Expression_5= ruleExpression otherlv_6= ')' ) ) ;
    public final EObject rulePrimary_Expression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject this_Constant_0 = null;

        EObject this_Variable_2 = null;

        EObject this_Func_Call_3 = null;

        EObject this_Expression_5 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3178:28: ( (this_Constant_0= ruleConstant | ( (otherlv_1= '-' )? this_Variable_2= ruleVariable ) | this_Func_Call_3= ruleFunc_Call | (otherlv_4= '(' this_Expression_5= ruleExpression otherlv_6= ')' ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3179:1: (this_Constant_0= ruleConstant | ( (otherlv_1= '-' )? this_Variable_2= ruleVariable ) | this_Func_Call_3= ruleFunc_Call | (otherlv_4= '(' this_Expression_5= ruleExpression otherlv_6= ')' ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3179:1: (this_Constant_0= ruleConstant | ( (otherlv_1= '-' )? this_Variable_2= ruleVariable ) | this_Func_Call_3= ruleFunc_Call | (otherlv_4= '(' this_Expression_5= ruleExpression otherlv_6= ')' ) )
            int alt55=4;
            switch ( input.LA(1) ) {
            case RULE_INT:
            case RULE_BINARY_INTEGER_VALUE:
            case RULE_OCTAL_INTEGER_VALUE:
            case RULE_HEX_INTEGER_VALUE:
            case RULE_REALVALUE:
            case RULE_S_BYTE_CHAR_STRING:
            case RULE_D_BYTE_CHAR_STRING:
            case 76:
            case 84:
            case 85:
            case 86:
            case 87:
            case 88:
            case 89:
            case 90:
            case 91:
            case 92:
            case 93:
            case 94:
            case 95:
            case 96:
            case 97:
            case 100:
            case 101:
            case 102:
            case 103:
            case 104:
            case 105:
            case 106:
            case 107:
            case 108:
            case 109:
            case 110:
            case 111:
            case 112:
            case 113:
            case 114:
                {
                alt55=1;
                }
                break;
            case 77:
                {
                int LA55_2 = input.LA(2);

                if ( (LA55_2==RULE_ID) ) {
                    alt55=2;
                }
                else if ( (LA55_2==RULE_INT||LA55_2==RULE_REALVALUE) ) {
                    alt55=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 55, 2, input);

                    throw nvae;
                }
                }
                break;
            case RULE_ID:
                {
                int LA55_3 = input.LA(2);

                if ( (LA55_3==34) ) {
                    alt55=3;
                }
                else if ( (LA55_3==EOF||LA55_3==31||(LA55_3>=35 && LA55_3<=36)||(LA55_3>=39 && LA55_3<=41)||LA55_3==45||LA55_3==47||(LA55_3>=57 && LA55_3<=59)||LA55_3==63||(LA55_3>=66 && LA55_3<=81)) ) {
                    alt55=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 55, 3, input);

                    throw nvae;
                }
                }
                break;
            case 34:
                {
                alt55=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 55, 0, input);

                throw nvae;
            }

            switch (alt55) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3180:5: this_Constant_0= ruleConstant
                    {
                     
                            newCompositeNode(grammarAccess.getPrimary_ExpressionAccess().getConstantParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleConstant_in_rulePrimary_Expression6913);
                    this_Constant_0=ruleConstant();

                    state._fsp--;

                     
                            current = this_Constant_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3189:6: ( (otherlv_1= '-' )? this_Variable_2= ruleVariable )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3189:6: ( (otherlv_1= '-' )? this_Variable_2= ruleVariable )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3189:7: (otherlv_1= '-' )? this_Variable_2= ruleVariable
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3189:7: (otherlv_1= '-' )?
                    int alt54=2;
                    int LA54_0 = input.LA(1);

                    if ( (LA54_0==77) ) {
                        alt54=1;
                    }
                    switch (alt54) {
                        case 1 :
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3189:9: otherlv_1= '-'
                            {
                            otherlv_1=(Token)match(input,77,FOLLOW_77_in_rulePrimary_Expression6932); 

                                	newLeafNode(otherlv_1, grammarAccess.getPrimary_ExpressionAccess().getHyphenMinusKeyword_1_0());
                                

                            }
                            break;

                    }

                     
                            newCompositeNode(grammarAccess.getPrimary_ExpressionAccess().getVariableParserRuleCall_1_1()); 
                        
                    pushFollow(FOLLOW_ruleVariable_in_rulePrimary_Expression6956);
                    this_Variable_2=ruleVariable();

                    state._fsp--;

                     
                            current = this_Variable_2; 
                            afterParserOrEnumRuleCall();
                        

                    }


                    }
                    break;
                case 3 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3204:5: this_Func_Call_3= ruleFunc_Call
                    {
                     
                            newCompositeNode(grammarAccess.getPrimary_ExpressionAccess().getFunc_CallParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_ruleFunc_Call_in_rulePrimary_Expression6984);
                    this_Func_Call_3=ruleFunc_Call();

                    state._fsp--;

                     
                            current = this_Func_Call_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3213:6: (otherlv_4= '(' this_Expression_5= ruleExpression otherlv_6= ')' )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3213:6: (otherlv_4= '(' this_Expression_5= ruleExpression otherlv_6= ')' )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3213:8: otherlv_4= '(' this_Expression_5= ruleExpression otherlv_6= ')'
                    {
                    otherlv_4=(Token)match(input,34,FOLLOW_34_in_rulePrimary_Expression7002); 

                        	newLeafNode(otherlv_4, grammarAccess.getPrimary_ExpressionAccess().getLeftParenthesisKeyword_3_0());
                        
                     
                            newCompositeNode(grammarAccess.getPrimary_ExpressionAccess().getExpressionParserRuleCall_3_1()); 
                        
                    pushFollow(FOLLOW_ruleExpression_in_rulePrimary_Expression7024);
                    this_Expression_5=ruleExpression();

                    state._fsp--;

                     
                            current = this_Expression_5; 
                            afterParserOrEnumRuleCall();
                        
                    otherlv_6=(Token)match(input,36,FOLLOW_36_in_rulePrimary_Expression7035); 

                        	newLeafNode(otherlv_6, grammarAccess.getPrimary_ExpressionAccess().getRightParenthesisKeyword_3_2());
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimary_Expression"


    // $ANTLR start "entryRuleFunc_Call"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3238:1: entryRuleFunc_Call returns [EObject current=null] : iv_ruleFunc_Call= ruleFunc_Call EOF ;
    public final EObject entryRuleFunc_Call() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunc_Call = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3239:2: (iv_ruleFunc_Call= ruleFunc_Call EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3240:2: iv_ruleFunc_Call= ruleFunc_Call EOF
            {
             newCompositeNode(grammarAccess.getFunc_CallRule()); 
            pushFollow(FOLLOW_ruleFunc_Call_in_entryRuleFunc_Call7072);
            iv_ruleFunc_Call=ruleFunc_Call();

            state._fsp--;

             current =iv_ruleFunc_Call; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFunc_Call7082); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunc_Call"


    // $ANTLR start "ruleFunc_Call"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3247:1: ruleFunc_Call returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( ( (lv_paramslist_2_0= ruleParam_Assign ) ) (otherlv_3= ',' ( (lv_paramslist_4_0= ruleParam_Assign ) ) )* )? otherlv_5= ')' ) ;
    public final EObject ruleFunc_Call() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_paramslist_2_0 = null;

        EObject lv_paramslist_4_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3250:28: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( ( (lv_paramslist_2_0= ruleParam_Assign ) ) (otherlv_3= ',' ( (lv_paramslist_4_0= ruleParam_Assign ) ) )* )? otherlv_5= ')' ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3251:1: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( ( (lv_paramslist_2_0= ruleParam_Assign ) ) (otherlv_3= ',' ( (lv_paramslist_4_0= ruleParam_Assign ) ) )* )? otherlv_5= ')' )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3251:1: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( ( (lv_paramslist_2_0= ruleParam_Assign ) ) (otherlv_3= ',' ( (lv_paramslist_4_0= ruleParam_Assign ) ) )* )? otherlv_5= ')' )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3251:2: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( ( (lv_paramslist_2_0= ruleParam_Assign ) ) (otherlv_3= ',' ( (lv_paramslist_4_0= ruleParam_Assign ) ) )* )? otherlv_5= ')'
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3251:2: ( (lv_name_0_0= RULE_ID ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3252:1: (lv_name_0_0= RULE_ID )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3252:1: (lv_name_0_0= RULE_ID )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3253:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleFunc_Call7124); 

            			newLeafNode(lv_name_0_0, grammarAccess.getFunc_CallAccess().getNameIDTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getFunc_CallRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"ID");
            	    

            }


            }

            otherlv_1=(Token)match(input,34,FOLLOW_34_in_ruleFunc_Call7141); 

                	newLeafNode(otherlv_1, grammarAccess.getFunc_CallAccess().getLeftParenthesisKeyword_1());
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3273:1: ( ( (lv_paramslist_2_0= ruleParam_Assign ) ) (otherlv_3= ',' ( (lv_paramslist_4_0= ruleParam_Assign ) ) )* )?
            int alt57=2;
            int LA57_0 = input.LA(1);

            if ( ((LA57_0>=RULE_ID && LA57_0<=RULE_D_BYTE_CHAR_STRING)||LA57_0==34||(LA57_0>=76 && LA57_0<=77)||LA57_0==82||(LA57_0>=84 && LA57_0<=97)||(LA57_0>=100 && LA57_0<=114)) ) {
                alt57=1;
            }
            switch (alt57) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3273:2: ( (lv_paramslist_2_0= ruleParam_Assign ) ) (otherlv_3= ',' ( (lv_paramslist_4_0= ruleParam_Assign ) ) )*
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3273:2: ( (lv_paramslist_2_0= ruleParam_Assign ) )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3274:1: (lv_paramslist_2_0= ruleParam_Assign )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3274:1: (lv_paramslist_2_0= ruleParam_Assign )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3275:3: lv_paramslist_2_0= ruleParam_Assign
                    {
                     
                    	        newCompositeNode(grammarAccess.getFunc_CallAccess().getParamslistParam_AssignParserRuleCall_2_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruleParam_Assign_in_ruleFunc_Call7163);
                    lv_paramslist_2_0=ruleParam_Assign();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getFunc_CallRule());
                    	        }
                           		add(
                           			current, 
                           			"paramslist",
                            		lv_paramslist_2_0, 
                            		"Param_Assign");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3291:2: (otherlv_3= ',' ( (lv_paramslist_4_0= ruleParam_Assign ) ) )*
                    loop56:
                    do {
                        int alt56=2;
                        int LA56_0 = input.LA(1);

                        if ( (LA56_0==35) ) {
                            alt56=1;
                        }


                        switch (alt56) {
                    	case 1 :
                    	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3291:4: otherlv_3= ',' ( (lv_paramslist_4_0= ruleParam_Assign ) )
                    	    {
                    	    otherlv_3=(Token)match(input,35,FOLLOW_35_in_ruleFunc_Call7176); 

                    	        	newLeafNode(otherlv_3, grammarAccess.getFunc_CallAccess().getCommaKeyword_2_1_0());
                    	        
                    	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3295:1: ( (lv_paramslist_4_0= ruleParam_Assign ) )
                    	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3296:1: (lv_paramslist_4_0= ruleParam_Assign )
                    	    {
                    	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3296:1: (lv_paramslist_4_0= ruleParam_Assign )
                    	    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3297:3: lv_paramslist_4_0= ruleParam_Assign
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getFunc_CallAccess().getParamslistParam_AssignParserRuleCall_2_1_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_ruleParam_Assign_in_ruleFunc_Call7197);
                    	    lv_paramslist_4_0=ruleParam_Assign();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getFunc_CallRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"paramslist",
                    	            		lv_paramslist_4_0, 
                    	            		"Param_Assign");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop56;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_5=(Token)match(input,36,FOLLOW_36_in_ruleFunc_Call7213); 

                	newLeafNode(otherlv_5, grammarAccess.getFunc_CallAccess().getRightParenthesisKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunc_Call"


    // $ANTLR start "entryRuleParam_Assign"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3325:1: entryRuleParam_Assign returns [EObject current=null] : iv_ruleParam_Assign= ruleParam_Assign EOF ;
    public final EObject entryRuleParam_Assign() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParam_Assign = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3326:2: (iv_ruleParam_Assign= ruleParam_Assign EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3327:2: iv_ruleParam_Assign= ruleParam_Assign EOF
            {
             newCompositeNode(grammarAccess.getParam_AssignRule()); 
            pushFollow(FOLLOW_ruleParam_Assign_in_entryRuleParam_Assign7249);
            iv_ruleParam_Assign=ruleParam_Assign();

            state._fsp--;

             current =iv_ruleParam_Assign; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleParam_Assign7259); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParam_Assign"


    // $ANTLR start "ruleParam_Assign"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3334:1: ruleParam_Assign returns [EObject current=null] : ( ( ( ( (lv_varname_0_0= RULE_ID ) ) otherlv_1= ':=' )? ( (lv_expr_2_0= ruleExpression ) ) ) | ( (otherlv_3= 'NOT' )? ( (lv_varname_4_0= RULE_ID ) ) otherlv_5= '=>' ( (lv_expr_6_0= ruleVariable ) ) ) ) ;
    public final EObject ruleParam_Assign() throws RecognitionException {
        EObject current = null;

        Token lv_varname_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_varname_4_0=null;
        Token otherlv_5=null;
        EObject lv_expr_2_0 = null;

        EObject lv_expr_6_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3337:28: ( ( ( ( ( (lv_varname_0_0= RULE_ID ) ) otherlv_1= ':=' )? ( (lv_expr_2_0= ruleExpression ) ) ) | ( (otherlv_3= 'NOT' )? ( (lv_varname_4_0= RULE_ID ) ) otherlv_5= '=>' ( (lv_expr_6_0= ruleVariable ) ) ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3338:1: ( ( ( ( (lv_varname_0_0= RULE_ID ) ) otherlv_1= ':=' )? ( (lv_expr_2_0= ruleExpression ) ) ) | ( (otherlv_3= 'NOT' )? ( (lv_varname_4_0= RULE_ID ) ) otherlv_5= '=>' ( (lv_expr_6_0= ruleVariable ) ) ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3338:1: ( ( ( ( (lv_varname_0_0= RULE_ID ) ) otherlv_1= ':=' )? ( (lv_expr_2_0= ruleExpression ) ) ) | ( (otherlv_3= 'NOT' )? ( (lv_varname_4_0= RULE_ID ) ) otherlv_5= '=>' ( (lv_expr_6_0= ruleVariable ) ) ) )
            int alt60=2;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                int LA60_1 = input.LA(2);

                if ( (LA60_1==EOF||(LA60_1>=34 && LA60_1<=37)||LA60_1==39||LA60_1==45||(LA60_1>=66 && LA60_1<=81)) ) {
                    alt60=1;
                }
                else if ( (LA60_1==83) ) {
                    alt60=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 60, 1, input);

                    throw nvae;
                }
                }
                break;
            case 82:
                {
                int LA60_2 = input.LA(2);

                if ( ((LA60_2>=RULE_INT && LA60_2<=RULE_D_BYTE_CHAR_STRING)||LA60_2==34||(LA60_2>=76 && LA60_2<=77)||(LA60_2>=84 && LA60_2<=97)||(LA60_2>=100 && LA60_2<=114)) ) {
                    alt60=1;
                }
                else if ( (LA60_2==RULE_ID) ) {
                    int LA60_5 = input.LA(3);

                    if ( (LA60_5==EOF||(LA60_5>=34 && LA60_5<=36)||LA60_5==39||LA60_5==45||(LA60_5>=66 && LA60_5<=81)) ) {
                        alt60=1;
                    }
                    else if ( (LA60_5==83) ) {
                        alt60=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 60, 5, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 60, 2, input);

                    throw nvae;
                }
                }
                break;
            case RULE_INT:
            case RULE_BINARY_INTEGER_VALUE:
            case RULE_OCTAL_INTEGER_VALUE:
            case RULE_HEX_INTEGER_VALUE:
            case RULE_REALVALUE:
            case RULE_S_BYTE_CHAR_STRING:
            case RULE_D_BYTE_CHAR_STRING:
            case 34:
            case 76:
            case 77:
            case 84:
            case 85:
            case 86:
            case 87:
            case 88:
            case 89:
            case 90:
            case 91:
            case 92:
            case 93:
            case 94:
            case 95:
            case 96:
            case 97:
            case 100:
            case 101:
            case 102:
            case 103:
            case 104:
            case 105:
            case 106:
            case 107:
            case 108:
            case 109:
            case 110:
            case 111:
            case 112:
            case 113:
            case 114:
                {
                alt60=1;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 60, 0, input);

                throw nvae;
            }

            switch (alt60) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3338:2: ( ( ( (lv_varname_0_0= RULE_ID ) ) otherlv_1= ':=' )? ( (lv_expr_2_0= ruleExpression ) ) )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3338:2: ( ( ( (lv_varname_0_0= RULE_ID ) ) otherlv_1= ':=' )? ( (lv_expr_2_0= ruleExpression ) ) )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3338:3: ( ( (lv_varname_0_0= RULE_ID ) ) otherlv_1= ':=' )? ( (lv_expr_2_0= ruleExpression ) )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3338:3: ( ( (lv_varname_0_0= RULE_ID ) ) otherlv_1= ':=' )?
                    int alt58=2;
                    int LA58_0 = input.LA(1);

                    if ( (LA58_0==RULE_ID) ) {
                        int LA58_1 = input.LA(2);

                        if ( (LA58_1==37) ) {
                            alt58=1;
                        }
                    }
                    switch (alt58) {
                        case 1 :
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3338:4: ( (lv_varname_0_0= RULE_ID ) ) otherlv_1= ':='
                            {
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3338:4: ( (lv_varname_0_0= RULE_ID ) )
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3339:1: (lv_varname_0_0= RULE_ID )
                            {
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3339:1: (lv_varname_0_0= RULE_ID )
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3340:3: lv_varname_0_0= RULE_ID
                            {
                            lv_varname_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleParam_Assign7303); 

                            			newLeafNode(lv_varname_0_0, grammarAccess.getParam_AssignAccess().getVarnameIDTerminalRuleCall_0_0_0_0()); 
                            		

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getParam_AssignRule());
                            	        }
                                   		setWithLastConsumed(
                                   			current, 
                                   			"varname",
                                    		lv_varname_0_0, 
                                    		"ID");
                            	    

                            }


                            }

                            otherlv_1=(Token)match(input,37,FOLLOW_37_in_ruleParam_Assign7320); 

                                	newLeafNode(otherlv_1, grammarAccess.getParam_AssignAccess().getColonEqualsSignKeyword_0_0_1());
                                

                            }
                            break;

                    }

                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3360:3: ( (lv_expr_2_0= ruleExpression ) )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3361:1: (lv_expr_2_0= ruleExpression )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3361:1: (lv_expr_2_0= ruleExpression )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3362:3: lv_expr_2_0= ruleExpression
                    {
                     
                    	        newCompositeNode(grammarAccess.getParam_AssignAccess().getExprExpressionParserRuleCall_0_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleExpression_in_ruleParam_Assign7343);
                    lv_expr_2_0=ruleExpression();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getParam_AssignRule());
                    	        }
                           		set(
                           			current, 
                           			"expr",
                            		lv_expr_2_0, 
                            		"Expression");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3379:6: ( (otherlv_3= 'NOT' )? ( (lv_varname_4_0= RULE_ID ) ) otherlv_5= '=>' ( (lv_expr_6_0= ruleVariable ) ) )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3379:6: ( (otherlv_3= 'NOT' )? ( (lv_varname_4_0= RULE_ID ) ) otherlv_5= '=>' ( (lv_expr_6_0= ruleVariable ) ) )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3379:7: (otherlv_3= 'NOT' )? ( (lv_varname_4_0= RULE_ID ) ) otherlv_5= '=>' ( (lv_expr_6_0= ruleVariable ) )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3379:7: (otherlv_3= 'NOT' )?
                    int alt59=2;
                    int LA59_0 = input.LA(1);

                    if ( (LA59_0==82) ) {
                        alt59=1;
                    }
                    switch (alt59) {
                        case 1 :
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3379:9: otherlv_3= 'NOT'
                            {
                            otherlv_3=(Token)match(input,82,FOLLOW_82_in_ruleParam_Assign7364); 

                                	newLeafNode(otherlv_3, grammarAccess.getParam_AssignAccess().getNOTKeyword_1_0());
                                

                            }
                            break;

                    }

                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3383:3: ( (lv_varname_4_0= RULE_ID ) )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3384:1: (lv_varname_4_0= RULE_ID )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3384:1: (lv_varname_4_0= RULE_ID )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3385:3: lv_varname_4_0= RULE_ID
                    {
                    lv_varname_4_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleParam_Assign7383); 

                    			newLeafNode(lv_varname_4_0, grammarAccess.getParam_AssignAccess().getVarnameIDTerminalRuleCall_1_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getParam_AssignRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"varname",
                            		lv_varname_4_0, 
                            		"ID");
                    	    

                    }


                    }

                    otherlv_5=(Token)match(input,83,FOLLOW_83_in_ruleParam_Assign7400); 

                        	newLeafNode(otherlv_5, grammarAccess.getParam_AssignAccess().getEqualsSignGreaterThanSignKeyword_1_2());
                        
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3405:1: ( (lv_expr_6_0= ruleVariable ) )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3406:1: (lv_expr_6_0= ruleVariable )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3406:1: (lv_expr_6_0= ruleVariable )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3407:3: lv_expr_6_0= ruleVariable
                    {
                     
                    	        newCompositeNode(grammarAccess.getParam_AssignAccess().getExprVariableParserRuleCall_1_3_0()); 
                    	    
                    pushFollow(FOLLOW_ruleVariable_in_ruleParam_Assign7421);
                    lv_expr_6_0=ruleVariable();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getParam_AssignRule());
                    	        }
                           		set(
                           			current, 
                           			"expr",
                            		lv_expr_6_0, 
                            		"Variable");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParam_Assign"


    // $ANTLR start "entryRuleConstant"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3431:1: entryRuleConstant returns [EObject current=null] : iv_ruleConstant= ruleConstant EOF ;
    public final EObject entryRuleConstant() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstant = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3432:2: (iv_ruleConstant= ruleConstant EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3433:2: iv_ruleConstant= ruleConstant EOF
            {
             newCompositeNode(grammarAccess.getConstantRule()); 
            pushFollow(FOLLOW_ruleConstant_in_entryRuleConstant7458);
            iv_ruleConstant=ruleConstant();

            state._fsp--;

             current =iv_ruleConstant; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleConstant7468); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstant"


    // $ANTLR start "ruleConstant"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3440:1: ruleConstant returns [EObject current=null] : (this_Numeric_Literal_0= ruleNumeric_Literal | this_Character_String_1= ruleCharacter_String | this_Boolean_Literal_2= ruleBoolean_Literal | this_Time_Literal_3= ruleTime_Literal ) ;
    public final EObject ruleConstant() throws RecognitionException {
        EObject current = null;

        EObject this_Numeric_Literal_0 = null;

        EObject this_Character_String_1 = null;

        EObject this_Boolean_Literal_2 = null;

        EObject this_Time_Literal_3 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3443:28: ( (this_Numeric_Literal_0= ruleNumeric_Literal | this_Character_String_1= ruleCharacter_String | this_Boolean_Literal_2= ruleBoolean_Literal | this_Time_Literal_3= ruleTime_Literal ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3444:1: (this_Numeric_Literal_0= ruleNumeric_Literal | this_Character_String_1= ruleCharacter_String | this_Boolean_Literal_2= ruleBoolean_Literal | this_Time_Literal_3= ruleTime_Literal )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3444:1: (this_Numeric_Literal_0= ruleNumeric_Literal | this_Character_String_1= ruleCharacter_String | this_Boolean_Literal_2= ruleBoolean_Literal | this_Time_Literal_3= ruleTime_Literal )
            int alt61=4;
            switch ( input.LA(1) ) {
            case RULE_INT:
            case RULE_BINARY_INTEGER_VALUE:
            case RULE_OCTAL_INTEGER_VALUE:
            case RULE_HEX_INTEGER_VALUE:
            case RULE_REALVALUE:
            case 76:
            case 77:
            case 84:
            case 85:
            case 86:
            case 87:
            case 88:
            case 89:
            case 90:
            case 91:
            case 92:
            case 93:
            case 94:
            case 95:
            case 96:
            case 97:
                {
                alt61=1;
                }
                break;
            case RULE_S_BYTE_CHAR_STRING:
            case RULE_D_BYTE_CHAR_STRING:
            case 105:
            case 106:
                {
                alt61=2;
                }
                break;
            case 100:
            case 101:
            case 102:
            case 103:
            case 104:
                {
                alt61=3;
                }
                break;
            case 107:
            case 108:
            case 109:
            case 110:
            case 111:
            case 112:
            case 113:
            case 114:
                {
                alt61=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 61, 0, input);

                throw nvae;
            }

            switch (alt61) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3445:5: this_Numeric_Literal_0= ruleNumeric_Literal
                    {
                     
                            newCompositeNode(grammarAccess.getConstantAccess().getNumeric_LiteralParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleNumeric_Literal_in_ruleConstant7515);
                    this_Numeric_Literal_0=ruleNumeric_Literal();

                    state._fsp--;

                     
                            current = this_Numeric_Literal_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3455:5: this_Character_String_1= ruleCharacter_String
                    {
                     
                            newCompositeNode(grammarAccess.getConstantAccess().getCharacter_StringParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleCharacter_String_in_ruleConstant7542);
                    this_Character_String_1=ruleCharacter_String();

                    state._fsp--;

                     
                            current = this_Character_String_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3465:5: this_Boolean_Literal_2= ruleBoolean_Literal
                    {
                     
                            newCompositeNode(grammarAccess.getConstantAccess().getBoolean_LiteralParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_ruleBoolean_Literal_in_ruleConstant7569);
                    this_Boolean_Literal_2=ruleBoolean_Literal();

                    state._fsp--;

                     
                            current = this_Boolean_Literal_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3475:5: this_Time_Literal_3= ruleTime_Literal
                    {
                     
                            newCompositeNode(grammarAccess.getConstantAccess().getTime_LiteralParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_ruleTime_Literal_in_ruleConstant7596);
                    this_Time_Literal_3=ruleTime_Literal();

                    state._fsp--;

                     
                            current = this_Time_Literal_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstant"


    // $ANTLR start "entryRuleNumeric_Literal"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3493:1: entryRuleNumeric_Literal returns [EObject current=null] : iv_ruleNumeric_Literal= ruleNumeric_Literal EOF ;
    public final EObject entryRuleNumeric_Literal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNumeric_Literal = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3494:2: (iv_ruleNumeric_Literal= ruleNumeric_Literal EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3495:2: iv_ruleNumeric_Literal= ruleNumeric_Literal EOF
            {
             newCompositeNode(grammarAccess.getNumeric_LiteralRule()); 
            pushFollow(FOLLOW_ruleNumeric_Literal_in_entryRuleNumeric_Literal7633);
            iv_ruleNumeric_Literal=ruleNumeric_Literal();

            state._fsp--;

             current =iv_ruleNumeric_Literal; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleNumeric_Literal7643); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNumeric_Literal"


    // $ANTLR start "ruleNumeric_Literal"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3502:1: ruleNumeric_Literal returns [EObject current=null] : (this_IntegerLiteral_0= ruleIntegerLiteral | this_Real_Literal_1= ruleReal_Literal ) ;
    public final EObject ruleNumeric_Literal() throws RecognitionException {
        EObject current = null;

        EObject this_IntegerLiteral_0 = null;

        EObject this_Real_Literal_1 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3505:28: ( (this_IntegerLiteral_0= ruleIntegerLiteral | this_Real_Literal_1= ruleReal_Literal ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3506:1: (this_IntegerLiteral_0= ruleIntegerLiteral | this_Real_Literal_1= ruleReal_Literal )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3506:1: (this_IntegerLiteral_0= ruleIntegerLiteral | this_Real_Literal_1= ruleReal_Literal )
            int alt62=2;
            switch ( input.LA(1) ) {
            case RULE_INT:
            case RULE_BINARY_INTEGER_VALUE:
            case RULE_OCTAL_INTEGER_VALUE:
            case RULE_HEX_INTEGER_VALUE:
            case 84:
            case 85:
            case 86:
            case 87:
            case 88:
            case 89:
            case 90:
            case 91:
            case 92:
            case 93:
            case 94:
            case 95:
                {
                alt62=1;
                }
                break;
            case 76:
                {
                int LA62_2 = input.LA(2);

                if ( (LA62_2==RULE_REALVALUE) ) {
                    alt62=2;
                }
                else if ( (LA62_2==RULE_INT) ) {
                    alt62=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 62, 2, input);

                    throw nvae;
                }
                }
                break;
            case 77:
                {
                int LA62_3 = input.LA(2);

                if ( (LA62_3==RULE_INT) ) {
                    alt62=1;
                }
                else if ( (LA62_3==RULE_REALVALUE) ) {
                    alt62=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 62, 3, input);

                    throw nvae;
                }
                }
                break;
            case RULE_REALVALUE:
            case 96:
            case 97:
                {
                alt62=2;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 62, 0, input);

                throw nvae;
            }

            switch (alt62) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3507:5: this_IntegerLiteral_0= ruleIntegerLiteral
                    {
                     
                            newCompositeNode(grammarAccess.getNumeric_LiteralAccess().getIntegerLiteralParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleIntegerLiteral_in_ruleNumeric_Literal7690);
                    this_IntegerLiteral_0=ruleIntegerLiteral();

                    state._fsp--;

                     
                            current = this_IntegerLiteral_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3517:5: this_Real_Literal_1= ruleReal_Literal
                    {
                     
                            newCompositeNode(grammarAccess.getNumeric_LiteralAccess().getReal_LiteralParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleReal_Literal_in_ruleNumeric_Literal7717);
                    this_Real_Literal_1=ruleReal_Literal();

                    state._fsp--;

                     
                            current = this_Real_Literal_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNumeric_Literal"


    // $ANTLR start "entryRuleIntegerLiteral"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3533:1: entryRuleIntegerLiteral returns [EObject current=null] : iv_ruleIntegerLiteral= ruleIntegerLiteral EOF ;
    public final EObject entryRuleIntegerLiteral() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerLiteral = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3534:2: (iv_ruleIntegerLiteral= ruleIntegerLiteral EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3535:2: iv_ruleIntegerLiteral= ruleIntegerLiteral EOF
            {
             newCompositeNode(grammarAccess.getIntegerLiteralRule()); 
            pushFollow(FOLLOW_ruleIntegerLiteral_in_entryRuleIntegerLiteral7752);
            iv_ruleIntegerLiteral=ruleIntegerLiteral();

            state._fsp--;

             current =iv_ruleIntegerLiteral; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntegerLiteral7762); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerLiteral"


    // $ANTLR start "ruleIntegerLiteral"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3542:1: ruleIntegerLiteral returns [EObject current=null] : ( ( ( (lv_type_0_1= 'UINT#' | lv_type_0_2= 'USINT#' | lv_type_0_3= 'UDINT#' | lv_type_0_4= 'ULINT#' | lv_type_0_5= 'BYTE#' | lv_type_0_6= 'WORD#' | lv_type_0_7= 'DWORD#' | lv_type_0_8= 'LWORD#' | lv_type_0_9= 'INT#' | lv_type_0_10= 'SINT#' | lv_type_0_11= 'DINT#' | lv_type_0_12= 'LINT#' ) ) )? ( ( (lv_value_1_1= ruleSignedInteger | lv_value_1_2= RULE_BINARY_INTEGER_VALUE | lv_value_1_3= RULE_OCTAL_INTEGER_VALUE | lv_value_1_4= RULE_HEX_INTEGER_VALUE ) ) ) ) ;
    public final EObject ruleIntegerLiteral() throws RecognitionException {
        EObject current = null;

        Token lv_type_0_1=null;
        Token lv_type_0_2=null;
        Token lv_type_0_3=null;
        Token lv_type_0_4=null;
        Token lv_type_0_5=null;
        Token lv_type_0_6=null;
        Token lv_type_0_7=null;
        Token lv_type_0_8=null;
        Token lv_type_0_9=null;
        Token lv_type_0_10=null;
        Token lv_type_0_11=null;
        Token lv_type_0_12=null;
        Token lv_value_1_2=null;
        Token lv_value_1_3=null;
        Token lv_value_1_4=null;
        AntlrDatatypeRuleToken lv_value_1_1 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3545:28: ( ( ( ( (lv_type_0_1= 'UINT#' | lv_type_0_2= 'USINT#' | lv_type_0_3= 'UDINT#' | lv_type_0_4= 'ULINT#' | lv_type_0_5= 'BYTE#' | lv_type_0_6= 'WORD#' | lv_type_0_7= 'DWORD#' | lv_type_0_8= 'LWORD#' | lv_type_0_9= 'INT#' | lv_type_0_10= 'SINT#' | lv_type_0_11= 'DINT#' | lv_type_0_12= 'LINT#' ) ) )? ( ( (lv_value_1_1= ruleSignedInteger | lv_value_1_2= RULE_BINARY_INTEGER_VALUE | lv_value_1_3= RULE_OCTAL_INTEGER_VALUE | lv_value_1_4= RULE_HEX_INTEGER_VALUE ) ) ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3546:1: ( ( ( (lv_type_0_1= 'UINT#' | lv_type_0_2= 'USINT#' | lv_type_0_3= 'UDINT#' | lv_type_0_4= 'ULINT#' | lv_type_0_5= 'BYTE#' | lv_type_0_6= 'WORD#' | lv_type_0_7= 'DWORD#' | lv_type_0_8= 'LWORD#' | lv_type_0_9= 'INT#' | lv_type_0_10= 'SINT#' | lv_type_0_11= 'DINT#' | lv_type_0_12= 'LINT#' ) ) )? ( ( (lv_value_1_1= ruleSignedInteger | lv_value_1_2= RULE_BINARY_INTEGER_VALUE | lv_value_1_3= RULE_OCTAL_INTEGER_VALUE | lv_value_1_4= RULE_HEX_INTEGER_VALUE ) ) ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3546:1: ( ( ( (lv_type_0_1= 'UINT#' | lv_type_0_2= 'USINT#' | lv_type_0_3= 'UDINT#' | lv_type_0_4= 'ULINT#' | lv_type_0_5= 'BYTE#' | lv_type_0_6= 'WORD#' | lv_type_0_7= 'DWORD#' | lv_type_0_8= 'LWORD#' | lv_type_0_9= 'INT#' | lv_type_0_10= 'SINT#' | lv_type_0_11= 'DINT#' | lv_type_0_12= 'LINT#' ) ) )? ( ( (lv_value_1_1= ruleSignedInteger | lv_value_1_2= RULE_BINARY_INTEGER_VALUE | lv_value_1_3= RULE_OCTAL_INTEGER_VALUE | lv_value_1_4= RULE_HEX_INTEGER_VALUE ) ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3546:2: ( ( (lv_type_0_1= 'UINT#' | lv_type_0_2= 'USINT#' | lv_type_0_3= 'UDINT#' | lv_type_0_4= 'ULINT#' | lv_type_0_5= 'BYTE#' | lv_type_0_6= 'WORD#' | lv_type_0_7= 'DWORD#' | lv_type_0_8= 'LWORD#' | lv_type_0_9= 'INT#' | lv_type_0_10= 'SINT#' | lv_type_0_11= 'DINT#' | lv_type_0_12= 'LINT#' ) ) )? ( ( (lv_value_1_1= ruleSignedInteger | lv_value_1_2= RULE_BINARY_INTEGER_VALUE | lv_value_1_3= RULE_OCTAL_INTEGER_VALUE | lv_value_1_4= RULE_HEX_INTEGER_VALUE ) ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3546:2: ( ( (lv_type_0_1= 'UINT#' | lv_type_0_2= 'USINT#' | lv_type_0_3= 'UDINT#' | lv_type_0_4= 'ULINT#' | lv_type_0_5= 'BYTE#' | lv_type_0_6= 'WORD#' | lv_type_0_7= 'DWORD#' | lv_type_0_8= 'LWORD#' | lv_type_0_9= 'INT#' | lv_type_0_10= 'SINT#' | lv_type_0_11= 'DINT#' | lv_type_0_12= 'LINT#' ) ) )?
            int alt64=2;
            int LA64_0 = input.LA(1);

            if ( ((LA64_0>=84 && LA64_0<=95)) ) {
                alt64=1;
            }
            switch (alt64) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3547:1: ( (lv_type_0_1= 'UINT#' | lv_type_0_2= 'USINT#' | lv_type_0_3= 'UDINT#' | lv_type_0_4= 'ULINT#' | lv_type_0_5= 'BYTE#' | lv_type_0_6= 'WORD#' | lv_type_0_7= 'DWORD#' | lv_type_0_8= 'LWORD#' | lv_type_0_9= 'INT#' | lv_type_0_10= 'SINT#' | lv_type_0_11= 'DINT#' | lv_type_0_12= 'LINT#' ) )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3547:1: ( (lv_type_0_1= 'UINT#' | lv_type_0_2= 'USINT#' | lv_type_0_3= 'UDINT#' | lv_type_0_4= 'ULINT#' | lv_type_0_5= 'BYTE#' | lv_type_0_6= 'WORD#' | lv_type_0_7= 'DWORD#' | lv_type_0_8= 'LWORD#' | lv_type_0_9= 'INT#' | lv_type_0_10= 'SINT#' | lv_type_0_11= 'DINT#' | lv_type_0_12= 'LINT#' ) )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3548:1: (lv_type_0_1= 'UINT#' | lv_type_0_2= 'USINT#' | lv_type_0_3= 'UDINT#' | lv_type_0_4= 'ULINT#' | lv_type_0_5= 'BYTE#' | lv_type_0_6= 'WORD#' | lv_type_0_7= 'DWORD#' | lv_type_0_8= 'LWORD#' | lv_type_0_9= 'INT#' | lv_type_0_10= 'SINT#' | lv_type_0_11= 'DINT#' | lv_type_0_12= 'LINT#' )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3548:1: (lv_type_0_1= 'UINT#' | lv_type_0_2= 'USINT#' | lv_type_0_3= 'UDINT#' | lv_type_0_4= 'ULINT#' | lv_type_0_5= 'BYTE#' | lv_type_0_6= 'WORD#' | lv_type_0_7= 'DWORD#' | lv_type_0_8= 'LWORD#' | lv_type_0_9= 'INT#' | lv_type_0_10= 'SINT#' | lv_type_0_11= 'DINT#' | lv_type_0_12= 'LINT#' )
                    int alt63=12;
                    switch ( input.LA(1) ) {
                    case 84:
                        {
                        alt63=1;
                        }
                        break;
                    case 85:
                        {
                        alt63=2;
                        }
                        break;
                    case 86:
                        {
                        alt63=3;
                        }
                        break;
                    case 87:
                        {
                        alt63=4;
                        }
                        break;
                    case 88:
                        {
                        alt63=5;
                        }
                        break;
                    case 89:
                        {
                        alt63=6;
                        }
                        break;
                    case 90:
                        {
                        alt63=7;
                        }
                        break;
                    case 91:
                        {
                        alt63=8;
                        }
                        break;
                    case 92:
                        {
                        alt63=9;
                        }
                        break;
                    case 93:
                        {
                        alt63=10;
                        }
                        break;
                    case 94:
                        {
                        alt63=11;
                        }
                        break;
                    case 95:
                        {
                        alt63=12;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 63, 0, input);

                        throw nvae;
                    }

                    switch (alt63) {
                        case 1 :
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3549:3: lv_type_0_1= 'UINT#'
                            {
                            lv_type_0_1=(Token)match(input,84,FOLLOW_84_in_ruleIntegerLiteral7807); 

                                    newLeafNode(lv_type_0_1, grammarAccess.getIntegerLiteralAccess().getTypeUINTKeyword_0_0_0());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getIntegerLiteralRule());
                            	        }
                                   		setWithLastConsumed(current, "type", lv_type_0_1, null);
                            	    

                            }
                            break;
                        case 2 :
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3561:8: lv_type_0_2= 'USINT#'
                            {
                            lv_type_0_2=(Token)match(input,85,FOLLOW_85_in_ruleIntegerLiteral7836); 

                                    newLeafNode(lv_type_0_2, grammarAccess.getIntegerLiteralAccess().getTypeUSINTKeyword_0_0_1());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getIntegerLiteralRule());
                            	        }
                                   		setWithLastConsumed(current, "type", lv_type_0_2, null);
                            	    

                            }
                            break;
                        case 3 :
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3573:8: lv_type_0_3= 'UDINT#'
                            {
                            lv_type_0_3=(Token)match(input,86,FOLLOW_86_in_ruleIntegerLiteral7865); 

                                    newLeafNode(lv_type_0_3, grammarAccess.getIntegerLiteralAccess().getTypeUDINTKeyword_0_0_2());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getIntegerLiteralRule());
                            	        }
                                   		setWithLastConsumed(current, "type", lv_type_0_3, null);
                            	    

                            }
                            break;
                        case 4 :
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3585:8: lv_type_0_4= 'ULINT#'
                            {
                            lv_type_0_4=(Token)match(input,87,FOLLOW_87_in_ruleIntegerLiteral7894); 

                                    newLeafNode(lv_type_0_4, grammarAccess.getIntegerLiteralAccess().getTypeULINTKeyword_0_0_3());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getIntegerLiteralRule());
                            	        }
                                   		setWithLastConsumed(current, "type", lv_type_0_4, null);
                            	    

                            }
                            break;
                        case 5 :
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3597:8: lv_type_0_5= 'BYTE#'
                            {
                            lv_type_0_5=(Token)match(input,88,FOLLOW_88_in_ruleIntegerLiteral7923); 

                                    newLeafNode(lv_type_0_5, grammarAccess.getIntegerLiteralAccess().getTypeBYTEKeyword_0_0_4());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getIntegerLiteralRule());
                            	        }
                                   		setWithLastConsumed(current, "type", lv_type_0_5, null);
                            	    

                            }
                            break;
                        case 6 :
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3609:8: lv_type_0_6= 'WORD#'
                            {
                            lv_type_0_6=(Token)match(input,89,FOLLOW_89_in_ruleIntegerLiteral7952); 

                                    newLeafNode(lv_type_0_6, grammarAccess.getIntegerLiteralAccess().getTypeWORDKeyword_0_0_5());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getIntegerLiteralRule());
                            	        }
                                   		setWithLastConsumed(current, "type", lv_type_0_6, null);
                            	    

                            }
                            break;
                        case 7 :
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3621:8: lv_type_0_7= 'DWORD#'
                            {
                            lv_type_0_7=(Token)match(input,90,FOLLOW_90_in_ruleIntegerLiteral7981); 

                                    newLeafNode(lv_type_0_7, grammarAccess.getIntegerLiteralAccess().getTypeDWORDKeyword_0_0_6());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getIntegerLiteralRule());
                            	        }
                                   		setWithLastConsumed(current, "type", lv_type_0_7, null);
                            	    

                            }
                            break;
                        case 8 :
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3633:8: lv_type_0_8= 'LWORD#'
                            {
                            lv_type_0_8=(Token)match(input,91,FOLLOW_91_in_ruleIntegerLiteral8010); 

                                    newLeafNode(lv_type_0_8, grammarAccess.getIntegerLiteralAccess().getTypeLWORDKeyword_0_0_7());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getIntegerLiteralRule());
                            	        }
                                   		setWithLastConsumed(current, "type", lv_type_0_8, null);
                            	    

                            }
                            break;
                        case 9 :
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3645:8: lv_type_0_9= 'INT#'
                            {
                            lv_type_0_9=(Token)match(input,92,FOLLOW_92_in_ruleIntegerLiteral8039); 

                                    newLeafNode(lv_type_0_9, grammarAccess.getIntegerLiteralAccess().getTypeINTKeyword_0_0_8());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getIntegerLiteralRule());
                            	        }
                                   		setWithLastConsumed(current, "type", lv_type_0_9, null);
                            	    

                            }
                            break;
                        case 10 :
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3657:8: lv_type_0_10= 'SINT#'
                            {
                            lv_type_0_10=(Token)match(input,93,FOLLOW_93_in_ruleIntegerLiteral8068); 

                                    newLeafNode(lv_type_0_10, grammarAccess.getIntegerLiteralAccess().getTypeSINTKeyword_0_0_9());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getIntegerLiteralRule());
                            	        }
                                   		setWithLastConsumed(current, "type", lv_type_0_10, null);
                            	    

                            }
                            break;
                        case 11 :
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3669:8: lv_type_0_11= 'DINT#'
                            {
                            lv_type_0_11=(Token)match(input,94,FOLLOW_94_in_ruleIntegerLiteral8097); 

                                    newLeafNode(lv_type_0_11, grammarAccess.getIntegerLiteralAccess().getTypeDINTKeyword_0_0_10());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getIntegerLiteralRule());
                            	        }
                                   		setWithLastConsumed(current, "type", lv_type_0_11, null);
                            	    

                            }
                            break;
                        case 12 :
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3681:8: lv_type_0_12= 'LINT#'
                            {
                            lv_type_0_12=(Token)match(input,95,FOLLOW_95_in_ruleIntegerLiteral8126); 

                                    newLeafNode(lv_type_0_12, grammarAccess.getIntegerLiteralAccess().getTypeLINTKeyword_0_0_11());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getIntegerLiteralRule());
                            	        }
                                   		setWithLastConsumed(current, "type", lv_type_0_12, null);
                            	    

                            }
                            break;

                    }


                    }


                    }
                    break;

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3696:3: ( ( (lv_value_1_1= ruleSignedInteger | lv_value_1_2= RULE_BINARY_INTEGER_VALUE | lv_value_1_3= RULE_OCTAL_INTEGER_VALUE | lv_value_1_4= RULE_HEX_INTEGER_VALUE ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3697:1: ( (lv_value_1_1= ruleSignedInteger | lv_value_1_2= RULE_BINARY_INTEGER_VALUE | lv_value_1_3= RULE_OCTAL_INTEGER_VALUE | lv_value_1_4= RULE_HEX_INTEGER_VALUE ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3697:1: ( (lv_value_1_1= ruleSignedInteger | lv_value_1_2= RULE_BINARY_INTEGER_VALUE | lv_value_1_3= RULE_OCTAL_INTEGER_VALUE | lv_value_1_4= RULE_HEX_INTEGER_VALUE ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3698:1: (lv_value_1_1= ruleSignedInteger | lv_value_1_2= RULE_BINARY_INTEGER_VALUE | lv_value_1_3= RULE_OCTAL_INTEGER_VALUE | lv_value_1_4= RULE_HEX_INTEGER_VALUE )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3698:1: (lv_value_1_1= ruleSignedInteger | lv_value_1_2= RULE_BINARY_INTEGER_VALUE | lv_value_1_3= RULE_OCTAL_INTEGER_VALUE | lv_value_1_4= RULE_HEX_INTEGER_VALUE )
            int alt65=4;
            switch ( input.LA(1) ) {
            case RULE_INT:
            case 76:
            case 77:
                {
                alt65=1;
                }
                break;
            case RULE_BINARY_INTEGER_VALUE:
                {
                alt65=2;
                }
                break;
            case RULE_OCTAL_INTEGER_VALUE:
                {
                alt65=3;
                }
                break;
            case RULE_HEX_INTEGER_VALUE:
                {
                alt65=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 65, 0, input);

                throw nvae;
            }

            switch (alt65) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3699:3: lv_value_1_1= ruleSignedInteger
                    {
                     
                    	        newCompositeNode(grammarAccess.getIntegerLiteralAccess().getValueSignedIntegerParserRuleCall_1_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruleSignedInteger_in_ruleIntegerLiteral8166);
                    lv_value_1_1=ruleSignedInteger();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getIntegerLiteralRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_1_1, 
                            		"SignedInteger");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3714:8: lv_value_1_2= RULE_BINARY_INTEGER_VALUE
                    {
                    lv_value_1_2=(Token)match(input,RULE_BINARY_INTEGER_VALUE,FOLLOW_RULE_BINARY_INTEGER_VALUE_in_ruleIntegerLiteral8181); 

                    			newLeafNode(lv_value_1_2, grammarAccess.getIntegerLiteralAccess().getValueBINARY_INTEGER_VALUETerminalRuleCall_1_0_1()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getIntegerLiteralRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"value",
                            		lv_value_1_2, 
                            		"BINARY_INTEGER_VALUE");
                    	    

                    }
                    break;
                case 3 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3729:8: lv_value_1_3= RULE_OCTAL_INTEGER_VALUE
                    {
                    lv_value_1_3=(Token)match(input,RULE_OCTAL_INTEGER_VALUE,FOLLOW_RULE_OCTAL_INTEGER_VALUE_in_ruleIntegerLiteral8201); 

                    			newLeafNode(lv_value_1_3, grammarAccess.getIntegerLiteralAccess().getValueOCTAL_INTEGER_VALUETerminalRuleCall_1_0_2()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getIntegerLiteralRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"value",
                            		lv_value_1_3, 
                            		"OCTAL_INTEGER_VALUE");
                    	    

                    }
                    break;
                case 4 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3744:8: lv_value_1_4= RULE_HEX_INTEGER_VALUE
                    {
                    lv_value_1_4=(Token)match(input,RULE_HEX_INTEGER_VALUE,FOLLOW_RULE_HEX_INTEGER_VALUE_in_ruleIntegerLiteral8221); 

                    			newLeafNode(lv_value_1_4, grammarAccess.getIntegerLiteralAccess().getValueHEX_INTEGER_VALUETerminalRuleCall_1_0_3()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getIntegerLiteralRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"value",
                            		lv_value_1_4, 
                            		"HEX_INTEGER_VALUE");
                    	    

                    }
                    break;

            }


            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerLiteral"


    // $ANTLR start "entryRuleSignedInteger"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3770:1: entryRuleSignedInteger returns [String current=null] : iv_ruleSignedInteger= ruleSignedInteger EOF ;
    public final String entryRuleSignedInteger() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleSignedInteger = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3771:2: (iv_ruleSignedInteger= ruleSignedInteger EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3772:2: iv_ruleSignedInteger= ruleSignedInteger EOF
            {
             newCompositeNode(grammarAccess.getSignedIntegerRule()); 
            pushFollow(FOLLOW_ruleSignedInteger_in_entryRuleSignedInteger8266);
            iv_ruleSignedInteger=ruleSignedInteger();

            state._fsp--;

             current =iv_ruleSignedInteger.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSignedInteger8277); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSignedInteger"


    // $ANTLR start "ruleSignedInteger"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3779:1: ruleSignedInteger returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '+' | kw= '-' )? this_INT_2= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleSignedInteger() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INT_2=null;

         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3782:28: ( ( (kw= '+' | kw= '-' )? this_INT_2= RULE_INT ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3783:1: ( (kw= '+' | kw= '-' )? this_INT_2= RULE_INT )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3783:1: ( (kw= '+' | kw= '-' )? this_INT_2= RULE_INT )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3783:2: (kw= '+' | kw= '-' )? this_INT_2= RULE_INT
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3783:2: (kw= '+' | kw= '-' )?
            int alt66=3;
            int LA66_0 = input.LA(1);

            if ( (LA66_0==76) ) {
                alt66=1;
            }
            else if ( (LA66_0==77) ) {
                alt66=2;
            }
            switch (alt66) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3784:2: kw= '+'
                    {
                    kw=(Token)match(input,76,FOLLOW_76_in_ruleSignedInteger8316); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getSignedIntegerAccess().getPlusSignKeyword_0_0()); 
                        

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3791:2: kw= '-'
                    {
                    kw=(Token)match(input,77,FOLLOW_77_in_ruleSignedInteger8335); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getSignedIntegerAccess().getHyphenMinusKeyword_0_1()); 
                        

                    }
                    break;

            }

            this_INT_2=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleSignedInteger8352); 

            		current.merge(this_INT_2);
                
             
                newLeafNode(this_INT_2, grammarAccess.getSignedIntegerAccess().getINTTerminalRuleCall_1()); 
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSignedInteger"


    // $ANTLR start "entryRuleReal_Literal"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3811:1: entryRuleReal_Literal returns [EObject current=null] : iv_ruleReal_Literal= ruleReal_Literal EOF ;
    public final EObject entryRuleReal_Literal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReal_Literal = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3812:2: (iv_ruleReal_Literal= ruleReal_Literal EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3813:2: iv_ruleReal_Literal= ruleReal_Literal EOF
            {
             newCompositeNode(grammarAccess.getReal_LiteralRule()); 
            pushFollow(FOLLOW_ruleReal_Literal_in_entryRuleReal_Literal8397);
            iv_ruleReal_Literal=ruleReal_Literal();

            state._fsp--;

             current =iv_ruleReal_Literal; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleReal_Literal8407); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReal_Literal"


    // $ANTLR start "ruleReal_Literal"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3820:1: ruleReal_Literal returns [EObject current=null] : ( ( ( (lv_type_0_1= 'REAL#' | lv_type_0_2= 'LREAL#' ) ) )? (otherlv_1= '+' | otherlv_2= '-' )? ( (lv_value_3_0= RULE_REALVALUE ) ) ( (otherlv_4= 'E' | otherlv_5= 'e' ) ( (lv_exponent_6_0= ruleSignedInteger ) ) )? ) ;
    public final EObject ruleReal_Literal() throws RecognitionException {
        EObject current = null;

        Token lv_type_0_1=null;
        Token lv_type_0_2=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_value_3_0=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_exponent_6_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3823:28: ( ( ( ( (lv_type_0_1= 'REAL#' | lv_type_0_2= 'LREAL#' ) ) )? (otherlv_1= '+' | otherlv_2= '-' )? ( (lv_value_3_0= RULE_REALVALUE ) ) ( (otherlv_4= 'E' | otherlv_5= 'e' ) ( (lv_exponent_6_0= ruleSignedInteger ) ) )? ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3824:1: ( ( ( (lv_type_0_1= 'REAL#' | lv_type_0_2= 'LREAL#' ) ) )? (otherlv_1= '+' | otherlv_2= '-' )? ( (lv_value_3_0= RULE_REALVALUE ) ) ( (otherlv_4= 'E' | otherlv_5= 'e' ) ( (lv_exponent_6_0= ruleSignedInteger ) ) )? )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3824:1: ( ( ( (lv_type_0_1= 'REAL#' | lv_type_0_2= 'LREAL#' ) ) )? (otherlv_1= '+' | otherlv_2= '-' )? ( (lv_value_3_0= RULE_REALVALUE ) ) ( (otherlv_4= 'E' | otherlv_5= 'e' ) ( (lv_exponent_6_0= ruleSignedInteger ) ) )? )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3824:2: ( ( (lv_type_0_1= 'REAL#' | lv_type_0_2= 'LREAL#' ) ) )? (otherlv_1= '+' | otherlv_2= '-' )? ( (lv_value_3_0= RULE_REALVALUE ) ) ( (otherlv_4= 'E' | otherlv_5= 'e' ) ( (lv_exponent_6_0= ruleSignedInteger ) ) )?
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3824:2: ( ( (lv_type_0_1= 'REAL#' | lv_type_0_2= 'LREAL#' ) ) )?
            int alt68=2;
            int LA68_0 = input.LA(1);

            if ( ((LA68_0>=96 && LA68_0<=97)) ) {
                alt68=1;
            }
            switch (alt68) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3825:1: ( (lv_type_0_1= 'REAL#' | lv_type_0_2= 'LREAL#' ) )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3825:1: ( (lv_type_0_1= 'REAL#' | lv_type_0_2= 'LREAL#' ) )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3826:1: (lv_type_0_1= 'REAL#' | lv_type_0_2= 'LREAL#' )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3826:1: (lv_type_0_1= 'REAL#' | lv_type_0_2= 'LREAL#' )
                    int alt67=2;
                    int LA67_0 = input.LA(1);

                    if ( (LA67_0==96) ) {
                        alt67=1;
                    }
                    else if ( (LA67_0==97) ) {
                        alt67=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 67, 0, input);

                        throw nvae;
                    }
                    switch (alt67) {
                        case 1 :
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3827:3: lv_type_0_1= 'REAL#'
                            {
                            lv_type_0_1=(Token)match(input,96,FOLLOW_96_in_ruleReal_Literal8452); 

                                    newLeafNode(lv_type_0_1, grammarAccess.getReal_LiteralAccess().getTypeREALKeyword_0_0_0());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getReal_LiteralRule());
                            	        }
                                   		setWithLastConsumed(current, "type", lv_type_0_1, null);
                            	    

                            }
                            break;
                        case 2 :
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3839:8: lv_type_0_2= 'LREAL#'
                            {
                            lv_type_0_2=(Token)match(input,97,FOLLOW_97_in_ruleReal_Literal8481); 

                                    newLeafNode(lv_type_0_2, grammarAccess.getReal_LiteralAccess().getTypeLREALKeyword_0_0_1());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getReal_LiteralRule());
                            	        }
                                   		setWithLastConsumed(current, "type", lv_type_0_2, null);
                            	    

                            }
                            break;

                    }


                    }


                    }
                    break;

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3854:3: (otherlv_1= '+' | otherlv_2= '-' )?
            int alt69=3;
            int LA69_0 = input.LA(1);

            if ( (LA69_0==76) ) {
                alt69=1;
            }
            else if ( (LA69_0==77) ) {
                alt69=2;
            }
            switch (alt69) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3854:5: otherlv_1= '+'
                    {
                    otherlv_1=(Token)match(input,76,FOLLOW_76_in_ruleReal_Literal8511); 

                        	newLeafNode(otherlv_1, grammarAccess.getReal_LiteralAccess().getPlusSignKeyword_1_0());
                        

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3859:7: otherlv_2= '-'
                    {
                    otherlv_2=(Token)match(input,77,FOLLOW_77_in_ruleReal_Literal8529); 

                        	newLeafNode(otherlv_2, grammarAccess.getReal_LiteralAccess().getHyphenMinusKeyword_1_1());
                        

                    }
                    break;

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3863:3: ( (lv_value_3_0= RULE_REALVALUE ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3864:1: (lv_value_3_0= RULE_REALVALUE )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3864:1: (lv_value_3_0= RULE_REALVALUE )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3865:3: lv_value_3_0= RULE_REALVALUE
            {
            lv_value_3_0=(Token)match(input,RULE_REALVALUE,FOLLOW_RULE_REALVALUE_in_ruleReal_Literal8548); 

            			newLeafNode(lv_value_3_0, grammarAccess.getReal_LiteralAccess().getValueREALVALUETerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getReal_LiteralRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_3_0, 
                    		"REALVALUE");
            	    

            }


            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3881:2: ( (otherlv_4= 'E' | otherlv_5= 'e' ) ( (lv_exponent_6_0= ruleSignedInteger ) ) )?
            int alt71=2;
            int LA71_0 = input.LA(1);

            if ( ((LA71_0>=98 && LA71_0<=99)) ) {
                alt71=1;
            }
            switch (alt71) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3881:3: (otherlv_4= 'E' | otherlv_5= 'e' ) ( (lv_exponent_6_0= ruleSignedInteger ) )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3881:3: (otherlv_4= 'E' | otherlv_5= 'e' )
                    int alt70=2;
                    int LA70_0 = input.LA(1);

                    if ( (LA70_0==98) ) {
                        alt70=1;
                    }
                    else if ( (LA70_0==99) ) {
                        alt70=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 70, 0, input);

                        throw nvae;
                    }
                    switch (alt70) {
                        case 1 :
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3881:5: otherlv_4= 'E'
                            {
                            otherlv_4=(Token)match(input,98,FOLLOW_98_in_ruleReal_Literal8567); 

                                	newLeafNode(otherlv_4, grammarAccess.getReal_LiteralAccess().getEKeyword_3_0_0());
                                

                            }
                            break;
                        case 2 :
                            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3886:7: otherlv_5= 'e'
                            {
                            otherlv_5=(Token)match(input,99,FOLLOW_99_in_ruleReal_Literal8585); 

                                	newLeafNode(otherlv_5, grammarAccess.getReal_LiteralAccess().getEKeyword_3_0_1());
                                

                            }
                            break;

                    }

                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3890:2: ( (lv_exponent_6_0= ruleSignedInteger ) )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3891:1: (lv_exponent_6_0= ruleSignedInteger )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3891:1: (lv_exponent_6_0= ruleSignedInteger )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3892:3: lv_exponent_6_0= ruleSignedInteger
                    {
                     
                    	        newCompositeNode(grammarAccess.getReal_LiteralAccess().getExponentSignedIntegerParserRuleCall_3_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleSignedInteger_in_ruleReal_Literal8607);
                    lv_exponent_6_0=ruleSignedInteger();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getReal_LiteralRule());
                    	        }
                           		set(
                           			current, 
                           			"exponent",
                            		lv_exponent_6_0, 
                            		"SignedInteger");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReal_Literal"


    // $ANTLR start "entryRuleBoolean_Literal"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3916:1: entryRuleBoolean_Literal returns [EObject current=null] : iv_ruleBoolean_Literal= ruleBoolean_Literal EOF ;
    public final EObject entryRuleBoolean_Literal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBoolean_Literal = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3917:2: (iv_ruleBoolean_Literal= ruleBoolean_Literal EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3918:2: iv_ruleBoolean_Literal= ruleBoolean_Literal EOF
            {
             newCompositeNode(grammarAccess.getBoolean_LiteralRule()); 
            pushFollow(FOLLOW_ruleBoolean_Literal_in_entryRuleBoolean_Literal8645);
            iv_ruleBoolean_Literal=ruleBoolean_Literal();

            state._fsp--;

             current =iv_ruleBoolean_Literal; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBoolean_Literal8655); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBoolean_Literal"


    // $ANTLR start "ruleBoolean_Literal"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3925:1: ruleBoolean_Literal returns [EObject current=null] : ( () ( (lv_type_1_0= 'BOOL#' ) )? ( ( (lv_value_2_1= 'TRUE' | lv_value_2_2= 'true' | lv_value_2_3= 'FALSE' | lv_value_2_4= 'false' ) ) ) ) ;
    public final EObject ruleBoolean_Literal() throws RecognitionException {
        EObject current = null;

        Token lv_type_1_0=null;
        Token lv_value_2_1=null;
        Token lv_value_2_2=null;
        Token lv_value_2_3=null;
        Token lv_value_2_4=null;

         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3928:28: ( ( () ( (lv_type_1_0= 'BOOL#' ) )? ( ( (lv_value_2_1= 'TRUE' | lv_value_2_2= 'true' | lv_value_2_3= 'FALSE' | lv_value_2_4= 'false' ) ) ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3929:1: ( () ( (lv_type_1_0= 'BOOL#' ) )? ( ( (lv_value_2_1= 'TRUE' | lv_value_2_2= 'true' | lv_value_2_3= 'FALSE' | lv_value_2_4= 'false' ) ) ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3929:1: ( () ( (lv_type_1_0= 'BOOL#' ) )? ( ( (lv_value_2_1= 'TRUE' | lv_value_2_2= 'true' | lv_value_2_3= 'FALSE' | lv_value_2_4= 'false' ) ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3929:2: () ( (lv_type_1_0= 'BOOL#' ) )? ( ( (lv_value_2_1= 'TRUE' | lv_value_2_2= 'true' | lv_value_2_3= 'FALSE' | lv_value_2_4= 'false' ) ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3929:2: ()
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3930:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getBoolean_LiteralAccess().getBoolean_LiteralAction_0(),
                        current);
                

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3935:2: ( (lv_type_1_0= 'BOOL#' ) )?
            int alt72=2;
            int LA72_0 = input.LA(1);

            if ( (LA72_0==100) ) {
                alt72=1;
            }
            switch (alt72) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3936:1: (lv_type_1_0= 'BOOL#' )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3936:1: (lv_type_1_0= 'BOOL#' )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3937:3: lv_type_1_0= 'BOOL#'
                    {
                    lv_type_1_0=(Token)match(input,100,FOLLOW_100_in_ruleBoolean_Literal8707); 

                            newLeafNode(lv_type_1_0, grammarAccess.getBoolean_LiteralAccess().getTypeBOOLKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBoolean_LiteralRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_1_0, "BOOL#");
                    	    

                    }


                    }
                    break;

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3950:3: ( ( (lv_value_2_1= 'TRUE' | lv_value_2_2= 'true' | lv_value_2_3= 'FALSE' | lv_value_2_4= 'false' ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3951:1: ( (lv_value_2_1= 'TRUE' | lv_value_2_2= 'true' | lv_value_2_3= 'FALSE' | lv_value_2_4= 'false' ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3951:1: ( (lv_value_2_1= 'TRUE' | lv_value_2_2= 'true' | lv_value_2_3= 'FALSE' | lv_value_2_4= 'false' ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3952:1: (lv_value_2_1= 'TRUE' | lv_value_2_2= 'true' | lv_value_2_3= 'FALSE' | lv_value_2_4= 'false' )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3952:1: (lv_value_2_1= 'TRUE' | lv_value_2_2= 'true' | lv_value_2_3= 'FALSE' | lv_value_2_4= 'false' )
            int alt73=4;
            switch ( input.LA(1) ) {
            case 101:
                {
                alt73=1;
                }
                break;
            case 102:
                {
                alt73=2;
                }
                break;
            case 103:
                {
                alt73=3;
                }
                break;
            case 104:
                {
                alt73=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 73, 0, input);

                throw nvae;
            }

            switch (alt73) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3953:3: lv_value_2_1= 'TRUE'
                    {
                    lv_value_2_1=(Token)match(input,101,FOLLOW_101_in_ruleBoolean_Literal8741); 

                            newLeafNode(lv_value_2_1, grammarAccess.getBoolean_LiteralAccess().getValueTRUEKeyword_2_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBoolean_LiteralRule());
                    	        }
                           		setWithLastConsumed(current, "value", lv_value_2_1, null);
                    	    

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3965:8: lv_value_2_2= 'true'
                    {
                    lv_value_2_2=(Token)match(input,102,FOLLOW_102_in_ruleBoolean_Literal8770); 

                            newLeafNode(lv_value_2_2, grammarAccess.getBoolean_LiteralAccess().getValueTrueKeyword_2_0_1());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBoolean_LiteralRule());
                    	        }
                           		setWithLastConsumed(current, "value", lv_value_2_2, null);
                    	    

                    }
                    break;
                case 3 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3977:8: lv_value_2_3= 'FALSE'
                    {
                    lv_value_2_3=(Token)match(input,103,FOLLOW_103_in_ruleBoolean_Literal8799); 

                            newLeafNode(lv_value_2_3, grammarAccess.getBoolean_LiteralAccess().getValueFALSEKeyword_2_0_2());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBoolean_LiteralRule());
                    	        }
                           		setWithLastConsumed(current, "value", lv_value_2_3, null);
                    	    

                    }
                    break;
                case 4 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:3989:8: lv_value_2_4= 'false'
                    {
                    lv_value_2_4=(Token)match(input,104,FOLLOW_104_in_ruleBoolean_Literal8828); 

                            newLeafNode(lv_value_2_4, grammarAccess.getBoolean_LiteralAccess().getValueFalseKeyword_2_0_3());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBoolean_LiteralRule());
                    	        }
                           		setWithLastConsumed(current, "value", lv_value_2_4, null);
                    	    

                    }
                    break;

            }


            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBoolean_Literal"


    // $ANTLR start "entryRuleCharacter_String"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4012:1: entryRuleCharacter_String returns [EObject current=null] : iv_ruleCharacter_String= ruleCharacter_String EOF ;
    public final EObject entryRuleCharacter_String() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCharacter_String = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4013:2: (iv_ruleCharacter_String= ruleCharacter_String EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4014:2: iv_ruleCharacter_String= ruleCharacter_String EOF
            {
             newCompositeNode(grammarAccess.getCharacter_StringRule()); 
            pushFollow(FOLLOW_ruleCharacter_String_in_entryRuleCharacter_String8880);
            iv_ruleCharacter_String=ruleCharacter_String();

            state._fsp--;

             current =iv_ruleCharacter_String; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCharacter_String8890); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCharacter_String"


    // $ANTLR start "ruleCharacter_String"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4021:1: ruleCharacter_String returns [EObject current=null] : (this_Single_Byte_Character_String_Literal_0= ruleSingle_Byte_Character_String_Literal | this_Double_Byte_Character_String_Literal_1= ruleDouble_Byte_Character_String_Literal ) ;
    public final EObject ruleCharacter_String() throws RecognitionException {
        EObject current = null;

        EObject this_Single_Byte_Character_String_Literal_0 = null;

        EObject this_Double_Byte_Character_String_Literal_1 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4024:28: ( (this_Single_Byte_Character_String_Literal_0= ruleSingle_Byte_Character_String_Literal | this_Double_Byte_Character_String_Literal_1= ruleDouble_Byte_Character_String_Literal ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4025:1: (this_Single_Byte_Character_String_Literal_0= ruleSingle_Byte_Character_String_Literal | this_Double_Byte_Character_String_Literal_1= ruleDouble_Byte_Character_String_Literal )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4025:1: (this_Single_Byte_Character_String_Literal_0= ruleSingle_Byte_Character_String_Literal | this_Double_Byte_Character_String_Literal_1= ruleDouble_Byte_Character_String_Literal )
            int alt74=2;
            int LA74_0 = input.LA(1);

            if ( (LA74_0==RULE_S_BYTE_CHAR_STRING||LA74_0==105) ) {
                alt74=1;
            }
            else if ( (LA74_0==RULE_D_BYTE_CHAR_STRING||LA74_0==106) ) {
                alt74=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 74, 0, input);

                throw nvae;
            }
            switch (alt74) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4026:5: this_Single_Byte_Character_String_Literal_0= ruleSingle_Byte_Character_String_Literal
                    {
                     
                            newCompositeNode(grammarAccess.getCharacter_StringAccess().getSingle_Byte_Character_String_LiteralParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleSingle_Byte_Character_String_Literal_in_ruleCharacter_String8937);
                    this_Single_Byte_Character_String_Literal_0=ruleSingle_Byte_Character_String_Literal();

                    state._fsp--;

                     
                            current = this_Single_Byte_Character_String_Literal_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4036:5: this_Double_Byte_Character_String_Literal_1= ruleDouble_Byte_Character_String_Literal
                    {
                     
                            newCompositeNode(grammarAccess.getCharacter_StringAccess().getDouble_Byte_Character_String_LiteralParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleDouble_Byte_Character_String_Literal_in_ruleCharacter_String8964);
                    this_Double_Byte_Character_String_Literal_1=ruleDouble_Byte_Character_String_Literal();

                    state._fsp--;

                     
                            current = this_Double_Byte_Character_String_Literal_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCharacter_String"


    // $ANTLR start "entryRuleSingle_Byte_Character_String_Literal"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4052:1: entryRuleSingle_Byte_Character_String_Literal returns [EObject current=null] : iv_ruleSingle_Byte_Character_String_Literal= ruleSingle_Byte_Character_String_Literal EOF ;
    public final EObject entryRuleSingle_Byte_Character_String_Literal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSingle_Byte_Character_String_Literal = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4053:2: (iv_ruleSingle_Byte_Character_String_Literal= ruleSingle_Byte_Character_String_Literal EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4054:2: iv_ruleSingle_Byte_Character_String_Literal= ruleSingle_Byte_Character_String_Literal EOF
            {
             newCompositeNode(grammarAccess.getSingle_Byte_Character_String_LiteralRule()); 
            pushFollow(FOLLOW_ruleSingle_Byte_Character_String_Literal_in_entryRuleSingle_Byte_Character_String_Literal8999);
            iv_ruleSingle_Byte_Character_String_Literal=ruleSingle_Byte_Character_String_Literal();

            state._fsp--;

             current =iv_ruleSingle_Byte_Character_String_Literal; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSingle_Byte_Character_String_Literal9009); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSingle_Byte_Character_String_Literal"


    // $ANTLR start "ruleSingle_Byte_Character_String_Literal"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4061:1: ruleSingle_Byte_Character_String_Literal returns [EObject current=null] : ( ( (lv_type_0_0= 'STRING#' ) )? ( (lv_value_1_0= RULE_S_BYTE_CHAR_STRING ) ) ) ;
    public final EObject ruleSingle_Byte_Character_String_Literal() throws RecognitionException {
        EObject current = null;

        Token lv_type_0_0=null;
        Token lv_value_1_0=null;

         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4064:28: ( ( ( (lv_type_0_0= 'STRING#' ) )? ( (lv_value_1_0= RULE_S_BYTE_CHAR_STRING ) ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4065:1: ( ( (lv_type_0_0= 'STRING#' ) )? ( (lv_value_1_0= RULE_S_BYTE_CHAR_STRING ) ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4065:1: ( ( (lv_type_0_0= 'STRING#' ) )? ( (lv_value_1_0= RULE_S_BYTE_CHAR_STRING ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4065:2: ( (lv_type_0_0= 'STRING#' ) )? ( (lv_value_1_0= RULE_S_BYTE_CHAR_STRING ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4065:2: ( (lv_type_0_0= 'STRING#' ) )?
            int alt75=2;
            int LA75_0 = input.LA(1);

            if ( (LA75_0==105) ) {
                alt75=1;
            }
            switch (alt75) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4066:1: (lv_type_0_0= 'STRING#' )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4066:1: (lv_type_0_0= 'STRING#' )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4067:3: lv_type_0_0= 'STRING#'
                    {
                    lv_type_0_0=(Token)match(input,105,FOLLOW_105_in_ruleSingle_Byte_Character_String_Literal9052); 

                            newLeafNode(lv_type_0_0, grammarAccess.getSingle_Byte_Character_String_LiteralAccess().getTypeSTRINGKeyword_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getSingle_Byte_Character_String_LiteralRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_0_0, "STRING#");
                    	    

                    }


                    }
                    break;

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4080:3: ( (lv_value_1_0= RULE_S_BYTE_CHAR_STRING ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4081:1: (lv_value_1_0= RULE_S_BYTE_CHAR_STRING )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4081:1: (lv_value_1_0= RULE_S_BYTE_CHAR_STRING )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4082:3: lv_value_1_0= RULE_S_BYTE_CHAR_STRING
            {
            lv_value_1_0=(Token)match(input,RULE_S_BYTE_CHAR_STRING,FOLLOW_RULE_S_BYTE_CHAR_STRING_in_ruleSingle_Byte_Character_String_Literal9083); 

            			newLeafNode(lv_value_1_0, grammarAccess.getSingle_Byte_Character_String_LiteralAccess().getValueS_BYTE_CHAR_STRINGTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getSingle_Byte_Character_String_LiteralRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_1_0, 
                    		"S_BYTE_CHAR_STRING");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSingle_Byte_Character_String_Literal"


    // $ANTLR start "entryRuleDouble_Byte_Character_String_Literal"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4106:1: entryRuleDouble_Byte_Character_String_Literal returns [EObject current=null] : iv_ruleDouble_Byte_Character_String_Literal= ruleDouble_Byte_Character_String_Literal EOF ;
    public final EObject entryRuleDouble_Byte_Character_String_Literal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDouble_Byte_Character_String_Literal = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4107:2: (iv_ruleDouble_Byte_Character_String_Literal= ruleDouble_Byte_Character_String_Literal EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4108:2: iv_ruleDouble_Byte_Character_String_Literal= ruleDouble_Byte_Character_String_Literal EOF
            {
             newCompositeNode(grammarAccess.getDouble_Byte_Character_String_LiteralRule()); 
            pushFollow(FOLLOW_ruleDouble_Byte_Character_String_Literal_in_entryRuleDouble_Byte_Character_String_Literal9124);
            iv_ruleDouble_Byte_Character_String_Literal=ruleDouble_Byte_Character_String_Literal();

            state._fsp--;

             current =iv_ruleDouble_Byte_Character_String_Literal; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDouble_Byte_Character_String_Literal9134); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDouble_Byte_Character_String_Literal"


    // $ANTLR start "ruleDouble_Byte_Character_String_Literal"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4115:1: ruleDouble_Byte_Character_String_Literal returns [EObject current=null] : ( ( (lv_type_0_0= 'WSTRING#' ) )? ( (lv_value_1_0= RULE_D_BYTE_CHAR_STRING ) ) ) ;
    public final EObject ruleDouble_Byte_Character_String_Literal() throws RecognitionException {
        EObject current = null;

        Token lv_type_0_0=null;
        Token lv_value_1_0=null;

         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4118:28: ( ( ( (lv_type_0_0= 'WSTRING#' ) )? ( (lv_value_1_0= RULE_D_BYTE_CHAR_STRING ) ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4119:1: ( ( (lv_type_0_0= 'WSTRING#' ) )? ( (lv_value_1_0= RULE_D_BYTE_CHAR_STRING ) ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4119:1: ( ( (lv_type_0_0= 'WSTRING#' ) )? ( (lv_value_1_0= RULE_D_BYTE_CHAR_STRING ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4119:2: ( (lv_type_0_0= 'WSTRING#' ) )? ( (lv_value_1_0= RULE_D_BYTE_CHAR_STRING ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4119:2: ( (lv_type_0_0= 'WSTRING#' ) )?
            int alt76=2;
            int LA76_0 = input.LA(1);

            if ( (LA76_0==106) ) {
                alt76=1;
            }
            switch (alt76) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4120:1: (lv_type_0_0= 'WSTRING#' )
                    {
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4120:1: (lv_type_0_0= 'WSTRING#' )
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4121:3: lv_type_0_0= 'WSTRING#'
                    {
                    lv_type_0_0=(Token)match(input,106,FOLLOW_106_in_ruleDouble_Byte_Character_String_Literal9177); 

                            newLeafNode(lv_type_0_0, grammarAccess.getDouble_Byte_Character_String_LiteralAccess().getTypeWSTRINGKeyword_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getDouble_Byte_Character_String_LiteralRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_0_0, "WSTRING#");
                    	    

                    }


                    }
                    break;

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4134:3: ( (lv_value_1_0= RULE_D_BYTE_CHAR_STRING ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4135:1: (lv_value_1_0= RULE_D_BYTE_CHAR_STRING )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4135:1: (lv_value_1_0= RULE_D_BYTE_CHAR_STRING )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4136:3: lv_value_1_0= RULE_D_BYTE_CHAR_STRING
            {
            lv_value_1_0=(Token)match(input,RULE_D_BYTE_CHAR_STRING,FOLLOW_RULE_D_BYTE_CHAR_STRING_in_ruleDouble_Byte_Character_String_Literal9208); 

            			newLeafNode(lv_value_1_0, grammarAccess.getDouble_Byte_Character_String_LiteralAccess().getValueD_BYTE_CHAR_STRINGTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getDouble_Byte_Character_String_LiteralRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_1_0, 
                    		"D_BYTE_CHAR_STRING");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDouble_Byte_Character_String_Literal"


    // $ANTLR start "entryRuleTime_Literal"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4160:1: entryRuleTime_Literal returns [EObject current=null] : iv_ruleTime_Literal= ruleTime_Literal EOF ;
    public final EObject entryRuleTime_Literal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTime_Literal = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4161:2: (iv_ruleTime_Literal= ruleTime_Literal EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4162:2: iv_ruleTime_Literal= ruleTime_Literal EOF
            {
             newCompositeNode(grammarAccess.getTime_LiteralRule()); 
            pushFollow(FOLLOW_ruleTime_Literal_in_entryRuleTime_Literal9249);
            iv_ruleTime_Literal=ruleTime_Literal();

            state._fsp--;

             current =iv_ruleTime_Literal; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTime_Literal9259); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTime_Literal"


    // $ANTLR start "ruleTime_Literal"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4169:1: ruleTime_Literal returns [EObject current=null] : (this_Duration_0= ruleDuration | this_Time_Of_Day_1= ruleTime_Of_Day | this_Date_2= ruleDate | this_Date_And_Time_3= ruleDate_And_Time ) ;
    public final EObject ruleTime_Literal() throws RecognitionException {
        EObject current = null;

        EObject this_Duration_0 = null;

        EObject this_Time_Of_Day_1 = null;

        EObject this_Date_2 = null;

        EObject this_Date_And_Time_3 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4172:28: ( (this_Duration_0= ruleDuration | this_Time_Of_Day_1= ruleTime_Of_Day | this_Date_2= ruleDate | this_Date_And_Time_3= ruleDate_And_Time ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4173:1: (this_Duration_0= ruleDuration | this_Time_Of_Day_1= ruleTime_Of_Day | this_Date_2= ruleDate | this_Date_And_Time_3= ruleDate_And_Time )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4173:1: (this_Duration_0= ruleDuration | this_Time_Of_Day_1= ruleTime_Of_Day | this_Date_2= ruleDate | this_Date_And_Time_3= ruleDate_And_Time )
            int alt77=4;
            switch ( input.LA(1) ) {
            case 107:
            case 108:
                {
                alt77=1;
                }
                break;
            case 109:
            case 110:
                {
                alt77=2;
                }
                break;
            case 111:
            case 112:
                {
                alt77=3;
                }
                break;
            case 113:
            case 114:
                {
                alt77=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 77, 0, input);

                throw nvae;
            }

            switch (alt77) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4174:5: this_Duration_0= ruleDuration
                    {
                     
                            newCompositeNode(grammarAccess.getTime_LiteralAccess().getDurationParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleDuration_in_ruleTime_Literal9306);
                    this_Duration_0=ruleDuration();

                    state._fsp--;

                     
                            current = this_Duration_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4184:5: this_Time_Of_Day_1= ruleTime_Of_Day
                    {
                     
                            newCompositeNode(grammarAccess.getTime_LiteralAccess().getTime_Of_DayParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleTime_Of_Day_in_ruleTime_Literal9333);
                    this_Time_Of_Day_1=ruleTime_Of_Day();

                    state._fsp--;

                     
                            current = this_Time_Of_Day_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4194:5: this_Date_2= ruleDate
                    {
                     
                            newCompositeNode(grammarAccess.getTime_LiteralAccess().getDateParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_ruleDate_in_ruleTime_Literal9360);
                    this_Date_2=ruleDate();

                    state._fsp--;

                     
                            current = this_Date_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4204:5: this_Date_And_Time_3= ruleDate_And_Time
                    {
                     
                            newCompositeNode(grammarAccess.getTime_LiteralAccess().getDate_And_TimeParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_ruleDate_And_Time_in_ruleTime_Literal9387);
                    this_Date_And_Time_3=ruleDate_And_Time();

                    state._fsp--;

                     
                            current = this_Date_And_Time_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTime_Literal"


    // $ANTLR start "entryRuleDuration"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4220:1: entryRuleDuration returns [EObject current=null] : iv_ruleDuration= ruleDuration EOF ;
    public final EObject entryRuleDuration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDuration = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4221:2: (iv_ruleDuration= ruleDuration EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4222:2: iv_ruleDuration= ruleDuration EOF
            {
             newCompositeNode(grammarAccess.getDurationRule()); 
            pushFollow(FOLLOW_ruleDuration_in_entryRuleDuration9422);
            iv_ruleDuration=ruleDuration();

            state._fsp--;

             current =iv_ruleDuration; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDuration9432); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDuration"


    // $ANTLR start "ruleDuration"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4229:1: ruleDuration returns [EObject current=null] : ( (otherlv_0= 'TIME#' | otherlv_1= 'T#' ) (otherlv_2= '+' | otherlv_3= '-' )? ( (lv_value_4_0= RULE_TIME ) ) ) ;
    public final EObject ruleDuration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_value_4_0=null;

         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4232:28: ( ( (otherlv_0= 'TIME#' | otherlv_1= 'T#' ) (otherlv_2= '+' | otherlv_3= '-' )? ( (lv_value_4_0= RULE_TIME ) ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4233:1: ( (otherlv_0= 'TIME#' | otherlv_1= 'T#' ) (otherlv_2= '+' | otherlv_3= '-' )? ( (lv_value_4_0= RULE_TIME ) ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4233:1: ( (otherlv_0= 'TIME#' | otherlv_1= 'T#' ) (otherlv_2= '+' | otherlv_3= '-' )? ( (lv_value_4_0= RULE_TIME ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4233:2: (otherlv_0= 'TIME#' | otherlv_1= 'T#' ) (otherlv_2= '+' | otherlv_3= '-' )? ( (lv_value_4_0= RULE_TIME ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4233:2: (otherlv_0= 'TIME#' | otherlv_1= 'T#' )
            int alt78=2;
            int LA78_0 = input.LA(1);

            if ( (LA78_0==107) ) {
                alt78=1;
            }
            else if ( (LA78_0==108) ) {
                alt78=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 78, 0, input);

                throw nvae;
            }
            switch (alt78) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4233:4: otherlv_0= 'TIME#'
                    {
                    otherlv_0=(Token)match(input,107,FOLLOW_107_in_ruleDuration9470); 

                        	newLeafNode(otherlv_0, grammarAccess.getDurationAccess().getTIMEKeyword_0_0());
                        

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4238:7: otherlv_1= 'T#'
                    {
                    otherlv_1=(Token)match(input,108,FOLLOW_108_in_ruleDuration9488); 

                        	newLeafNode(otherlv_1, grammarAccess.getDurationAccess().getTKeyword_0_1());
                        

                    }
                    break;

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4242:2: (otherlv_2= '+' | otherlv_3= '-' )?
            int alt79=3;
            int LA79_0 = input.LA(1);

            if ( (LA79_0==76) ) {
                alt79=1;
            }
            else if ( (LA79_0==77) ) {
                alt79=2;
            }
            switch (alt79) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4242:4: otherlv_2= '+'
                    {
                    otherlv_2=(Token)match(input,76,FOLLOW_76_in_ruleDuration9502); 

                        	newLeafNode(otherlv_2, grammarAccess.getDurationAccess().getPlusSignKeyword_1_0());
                        

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4247:7: otherlv_3= '-'
                    {
                    otherlv_3=(Token)match(input,77,FOLLOW_77_in_ruleDuration9520); 

                        	newLeafNode(otherlv_3, grammarAccess.getDurationAccess().getHyphenMinusKeyword_1_1());
                        

                    }
                    break;

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4251:3: ( (lv_value_4_0= RULE_TIME ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4252:1: (lv_value_4_0= RULE_TIME )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4252:1: (lv_value_4_0= RULE_TIME )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4253:3: lv_value_4_0= RULE_TIME
            {
            lv_value_4_0=(Token)match(input,RULE_TIME,FOLLOW_RULE_TIME_in_ruleDuration9539); 

            			newLeafNode(lv_value_4_0, grammarAccess.getDurationAccess().getValueTIMETerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getDurationRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_4_0, 
                    		"TIME");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDuration"


    // $ANTLR start "entryRuleFix_Point"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4277:1: entryRuleFix_Point returns [String current=null] : iv_ruleFix_Point= ruleFix_Point EOF ;
    public final String entryRuleFix_Point() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleFix_Point = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4278:2: (iv_ruleFix_Point= ruleFix_Point EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4279:2: iv_ruleFix_Point= ruleFix_Point EOF
            {
             newCompositeNode(grammarAccess.getFix_PointRule()); 
            pushFollow(FOLLOW_ruleFix_Point_in_entryRuleFix_Point9581);
            iv_ruleFix_Point=ruleFix_Point();

            state._fsp--;

             current =iv_ruleFix_Point.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFix_Point9592); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFix_Point"


    // $ANTLR start "ruleFix_Point"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4286:1: ruleFix_Point returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_INT_0= RULE_INT (kw= '.' this_INT_2= RULE_INT )? ) ;
    public final AntlrDatatypeRuleToken ruleFix_Point() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;
        Token kw=null;
        Token this_INT_2=null;

         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4289:28: ( (this_INT_0= RULE_INT (kw= '.' this_INT_2= RULE_INT )? ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4290:1: (this_INT_0= RULE_INT (kw= '.' this_INT_2= RULE_INT )? )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4290:1: (this_INT_0= RULE_INT (kw= '.' this_INT_2= RULE_INT )? )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4290:6: this_INT_0= RULE_INT (kw= '.' this_INT_2= RULE_INT )?
            {
            this_INT_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleFix_Point9632); 

            		current.merge(this_INT_0);
                
             
                newLeafNode(this_INT_0, grammarAccess.getFix_PointAccess().getINTTerminalRuleCall_0()); 
                
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4297:1: (kw= '.' this_INT_2= RULE_INT )?
            int alt80=2;
            int LA80_0 = input.LA(1);

            if ( (LA80_0==45) ) {
                alt80=1;
            }
            switch (alt80) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4298:2: kw= '.' this_INT_2= RULE_INT
                    {
                    kw=(Token)match(input,45,FOLLOW_45_in_ruleFix_Point9651); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getFix_PointAccess().getFullStopKeyword_1_0()); 
                        
                    this_INT_2=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleFix_Point9666); 

                    		current.merge(this_INT_2);
                        
                     
                        newLeafNode(this_INT_2, grammarAccess.getFix_PointAccess().getINTTerminalRuleCall_1_1()); 
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFix_Point"


    // $ANTLR start "entryRuleTime_Of_Day"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4320:1: entryRuleTime_Of_Day returns [EObject current=null] : iv_ruleTime_Of_Day= ruleTime_Of_Day EOF ;
    public final EObject entryRuleTime_Of_Day() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTime_Of_Day = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4321:2: (iv_ruleTime_Of_Day= ruleTime_Of_Day EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4322:2: iv_ruleTime_Of_Day= ruleTime_Of_Day EOF
            {
             newCompositeNode(grammarAccess.getTime_Of_DayRule()); 
            pushFollow(FOLLOW_ruleTime_Of_Day_in_entryRuleTime_Of_Day9715);
            iv_ruleTime_Of_Day=ruleTime_Of_Day();

            state._fsp--;

             current =iv_ruleTime_Of_Day; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTime_Of_Day9725); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTime_Of_Day"


    // $ANTLR start "ruleTime_Of_Day"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4329:1: ruleTime_Of_Day returns [EObject current=null] : ( (otherlv_0= 'TIME_OF_DAY#' | otherlv_1= 'TOD#' ) ( (lv_value_2_0= ruleDaytime ) ) ) ;
    public final EObject ruleTime_Of_Day() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_value_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4332:28: ( ( (otherlv_0= 'TIME_OF_DAY#' | otherlv_1= 'TOD#' ) ( (lv_value_2_0= ruleDaytime ) ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4333:1: ( (otherlv_0= 'TIME_OF_DAY#' | otherlv_1= 'TOD#' ) ( (lv_value_2_0= ruleDaytime ) ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4333:1: ( (otherlv_0= 'TIME_OF_DAY#' | otherlv_1= 'TOD#' ) ( (lv_value_2_0= ruleDaytime ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4333:2: (otherlv_0= 'TIME_OF_DAY#' | otherlv_1= 'TOD#' ) ( (lv_value_2_0= ruleDaytime ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4333:2: (otherlv_0= 'TIME_OF_DAY#' | otherlv_1= 'TOD#' )
            int alt81=2;
            int LA81_0 = input.LA(1);

            if ( (LA81_0==109) ) {
                alt81=1;
            }
            else if ( (LA81_0==110) ) {
                alt81=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 81, 0, input);

                throw nvae;
            }
            switch (alt81) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4333:4: otherlv_0= 'TIME_OF_DAY#'
                    {
                    otherlv_0=(Token)match(input,109,FOLLOW_109_in_ruleTime_Of_Day9763); 

                        	newLeafNode(otherlv_0, grammarAccess.getTime_Of_DayAccess().getTIME_OF_DAYKeyword_0_0());
                        

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4338:7: otherlv_1= 'TOD#'
                    {
                    otherlv_1=(Token)match(input,110,FOLLOW_110_in_ruleTime_Of_Day9781); 

                        	newLeafNode(otherlv_1, grammarAccess.getTime_Of_DayAccess().getTODKeyword_0_1());
                        

                    }
                    break;

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4342:2: ( (lv_value_2_0= ruleDaytime ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4343:1: (lv_value_2_0= ruleDaytime )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4343:1: (lv_value_2_0= ruleDaytime )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4344:3: lv_value_2_0= ruleDaytime
            {
             
            	        newCompositeNode(grammarAccess.getTime_Of_DayAccess().getValueDaytimeParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleDaytime_in_ruleTime_Of_Day9803);
            lv_value_2_0=ruleDaytime();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getTime_Of_DayRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_2_0, 
                    		"Daytime");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTime_Of_Day"


    // $ANTLR start "entryRuleDaytime"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4368:1: entryRuleDaytime returns [String current=null] : iv_ruleDaytime= ruleDaytime EOF ;
    public final String entryRuleDaytime() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleDaytime = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4369:2: (iv_ruleDaytime= ruleDaytime EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4370:2: iv_ruleDaytime= ruleDaytime EOF
            {
             newCompositeNode(grammarAccess.getDaytimeRule()); 
            pushFollow(FOLLOW_ruleDaytime_in_entryRuleDaytime9840);
            iv_ruleDaytime=ruleDaytime();

            state._fsp--;

             current =iv_ruleDaytime.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDaytime9851); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDaytime"


    // $ANTLR start "ruleDaytime"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4377:1: ruleDaytime returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_Day_Hour_0= ruleDay_Hour kw= ':' this_Day_Minute_2= ruleDay_Minute kw= ':' this_Day_Second_4= ruleDay_Second ) ;
    public final AntlrDatatypeRuleToken ruleDaytime() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_Day_Hour_0 = null;

        AntlrDatatypeRuleToken this_Day_Minute_2 = null;

        AntlrDatatypeRuleToken this_Day_Second_4 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4380:28: ( (this_Day_Hour_0= ruleDay_Hour kw= ':' this_Day_Minute_2= ruleDay_Minute kw= ':' this_Day_Second_4= ruleDay_Second ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4381:1: (this_Day_Hour_0= ruleDay_Hour kw= ':' this_Day_Minute_2= ruleDay_Minute kw= ':' this_Day_Second_4= ruleDay_Second )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4381:1: (this_Day_Hour_0= ruleDay_Hour kw= ':' this_Day_Minute_2= ruleDay_Minute kw= ':' this_Day_Second_4= ruleDay_Second )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4382:5: this_Day_Hour_0= ruleDay_Hour kw= ':' this_Day_Minute_2= ruleDay_Minute kw= ':' this_Day_Second_4= ruleDay_Second
            {
             
                    newCompositeNode(grammarAccess.getDaytimeAccess().getDay_HourParserRuleCall_0()); 
                
            pushFollow(FOLLOW_ruleDay_Hour_in_ruleDaytime9898);
            this_Day_Hour_0=ruleDay_Hour();

            state._fsp--;


            		current.merge(this_Day_Hour_0);
                
             
                    afterParserOrEnumRuleCall();
                
            kw=(Token)match(input,30,FOLLOW_30_in_ruleDaytime9916); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getDaytimeAccess().getColonKeyword_1()); 
                
             
                    newCompositeNode(grammarAccess.getDaytimeAccess().getDay_MinuteParserRuleCall_2()); 
                
            pushFollow(FOLLOW_ruleDay_Minute_in_ruleDaytime9938);
            this_Day_Minute_2=ruleDay_Minute();

            state._fsp--;


            		current.merge(this_Day_Minute_2);
                
             
                    afterParserOrEnumRuleCall();
                
            kw=(Token)match(input,30,FOLLOW_30_in_ruleDaytime9956); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getDaytimeAccess().getColonKeyword_3()); 
                
             
                    newCompositeNode(grammarAccess.getDaytimeAccess().getDay_SecondParserRuleCall_4()); 
                
            pushFollow(FOLLOW_ruleDay_Second_in_ruleDaytime9978);
            this_Day_Second_4=ruleDay_Second();

            state._fsp--;


            		current.merge(this_Day_Second_4);
                
             
                    afterParserOrEnumRuleCall();
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDaytime"


    // $ANTLR start "entryRuleDay_Hour"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4434:1: entryRuleDay_Hour returns [String current=null] : iv_ruleDay_Hour= ruleDay_Hour EOF ;
    public final String entryRuleDay_Hour() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleDay_Hour = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4435:2: (iv_ruleDay_Hour= ruleDay_Hour EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4436:2: iv_ruleDay_Hour= ruleDay_Hour EOF
            {
             newCompositeNode(grammarAccess.getDay_HourRule()); 
            pushFollow(FOLLOW_ruleDay_Hour_in_entryRuleDay_Hour10024);
            iv_ruleDay_Hour=ruleDay_Hour();

            state._fsp--;

             current =iv_ruleDay_Hour.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDay_Hour10035); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDay_Hour"


    // $ANTLR start "ruleDay_Hour"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4443:1: ruleDay_Hour returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_INT_0= RULE_INT ;
    public final AntlrDatatypeRuleToken ruleDay_Hour() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;

         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4446:28: (this_INT_0= RULE_INT )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4447:5: this_INT_0= RULE_INT
            {
            this_INT_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleDay_Hour10074); 

            		current.merge(this_INT_0);
                
             
                newLeafNode(this_INT_0, grammarAccess.getDay_HourAccess().getINTTerminalRuleCall()); 
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDay_Hour"


    // $ANTLR start "entryRuleDay_Minute"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4462:1: entryRuleDay_Minute returns [String current=null] : iv_ruleDay_Minute= ruleDay_Minute EOF ;
    public final String entryRuleDay_Minute() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleDay_Minute = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4463:2: (iv_ruleDay_Minute= ruleDay_Minute EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4464:2: iv_ruleDay_Minute= ruleDay_Minute EOF
            {
             newCompositeNode(grammarAccess.getDay_MinuteRule()); 
            pushFollow(FOLLOW_ruleDay_Minute_in_entryRuleDay_Minute10119);
            iv_ruleDay_Minute=ruleDay_Minute();

            state._fsp--;

             current =iv_ruleDay_Minute.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDay_Minute10130); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDay_Minute"


    // $ANTLR start "ruleDay_Minute"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4471:1: ruleDay_Minute returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_INT_0= RULE_INT ;
    public final AntlrDatatypeRuleToken ruleDay_Minute() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;

         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4474:28: (this_INT_0= RULE_INT )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4475:5: this_INT_0= RULE_INT
            {
            this_INT_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleDay_Minute10169); 

            		current.merge(this_INT_0);
                
             
                newLeafNode(this_INT_0, grammarAccess.getDay_MinuteAccess().getINTTerminalRuleCall()); 
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDay_Minute"


    // $ANTLR start "entryRuleDay_Second"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4490:1: entryRuleDay_Second returns [String current=null] : iv_ruleDay_Second= ruleDay_Second EOF ;
    public final String entryRuleDay_Second() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleDay_Second = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4491:2: (iv_ruleDay_Second= ruleDay_Second EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4492:2: iv_ruleDay_Second= ruleDay_Second EOF
            {
             newCompositeNode(grammarAccess.getDay_SecondRule()); 
            pushFollow(FOLLOW_ruleDay_Second_in_entryRuleDay_Second10214);
            iv_ruleDay_Second=ruleDay_Second();

            state._fsp--;

             current =iv_ruleDay_Second.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDay_Second10225); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDay_Second"


    // $ANTLR start "ruleDay_Second"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4499:1: ruleDay_Second returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_Fix_Point_0= ruleFix_Point ;
    public final AntlrDatatypeRuleToken ruleDay_Second() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        AntlrDatatypeRuleToken this_Fix_Point_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4502:28: (this_Fix_Point_0= ruleFix_Point )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4504:5: this_Fix_Point_0= ruleFix_Point
            {
             
                    newCompositeNode(grammarAccess.getDay_SecondAccess().getFix_PointParserRuleCall()); 
                
            pushFollow(FOLLOW_ruleFix_Point_in_ruleDay_Second10271);
            this_Fix_Point_0=ruleFix_Point();

            state._fsp--;


            		current.merge(this_Fix_Point_0);
                
             
                    afterParserOrEnumRuleCall();
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDay_Second"


    // $ANTLR start "entryRuleDate"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4522:1: entryRuleDate returns [EObject current=null] : iv_ruleDate= ruleDate EOF ;
    public final EObject entryRuleDate() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDate = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4523:2: (iv_ruleDate= ruleDate EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4524:2: iv_ruleDate= ruleDate EOF
            {
             newCompositeNode(grammarAccess.getDateRule()); 
            pushFollow(FOLLOW_ruleDate_in_entryRuleDate10315);
            iv_ruleDate=ruleDate();

            state._fsp--;

             current =iv_ruleDate; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDate10325); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDate"


    // $ANTLR start "ruleDate"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4531:1: ruleDate returns [EObject current=null] : ( (otherlv_0= 'DATE#' | otherlv_1= 'D#' ) ( (lv_value_2_0= ruleDate_Literal ) ) ) ;
    public final EObject ruleDate() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_value_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4534:28: ( ( (otherlv_0= 'DATE#' | otherlv_1= 'D#' ) ( (lv_value_2_0= ruleDate_Literal ) ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4535:1: ( (otherlv_0= 'DATE#' | otherlv_1= 'D#' ) ( (lv_value_2_0= ruleDate_Literal ) ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4535:1: ( (otherlv_0= 'DATE#' | otherlv_1= 'D#' ) ( (lv_value_2_0= ruleDate_Literal ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4535:2: (otherlv_0= 'DATE#' | otherlv_1= 'D#' ) ( (lv_value_2_0= ruleDate_Literal ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4535:2: (otherlv_0= 'DATE#' | otherlv_1= 'D#' )
            int alt82=2;
            int LA82_0 = input.LA(1);

            if ( (LA82_0==111) ) {
                alt82=1;
            }
            else if ( (LA82_0==112) ) {
                alt82=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 82, 0, input);

                throw nvae;
            }
            switch (alt82) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4535:4: otherlv_0= 'DATE#'
                    {
                    otherlv_0=(Token)match(input,111,FOLLOW_111_in_ruleDate10363); 

                        	newLeafNode(otherlv_0, grammarAccess.getDateAccess().getDATEKeyword_0_0());
                        

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4540:7: otherlv_1= 'D#'
                    {
                    otherlv_1=(Token)match(input,112,FOLLOW_112_in_ruleDate10381); 

                        	newLeafNode(otherlv_1, grammarAccess.getDateAccess().getDKeyword_0_1());
                        

                    }
                    break;

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4544:2: ( (lv_value_2_0= ruleDate_Literal ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4545:1: (lv_value_2_0= ruleDate_Literal )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4545:1: (lv_value_2_0= ruleDate_Literal )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4546:3: lv_value_2_0= ruleDate_Literal
            {
             
            	        newCompositeNode(grammarAccess.getDateAccess().getValueDate_LiteralParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleDate_Literal_in_ruleDate10403);
            lv_value_2_0=ruleDate_Literal();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getDateRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_2_0, 
                    		"Date_Literal");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDate"


    // $ANTLR start "entryRuleDate_Literal"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4570:1: entryRuleDate_Literal returns [String current=null] : iv_ruleDate_Literal= ruleDate_Literal EOF ;
    public final String entryRuleDate_Literal() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleDate_Literal = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4571:2: (iv_ruleDate_Literal= ruleDate_Literal EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4572:2: iv_ruleDate_Literal= ruleDate_Literal EOF
            {
             newCompositeNode(grammarAccess.getDate_LiteralRule()); 
            pushFollow(FOLLOW_ruleDate_Literal_in_entryRuleDate_Literal10440);
            iv_ruleDate_Literal=ruleDate_Literal();

            state._fsp--;

             current =iv_ruleDate_Literal.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDate_Literal10451); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDate_Literal"


    // $ANTLR start "ruleDate_Literal"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4579:1: ruleDate_Literal returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_Year_0= ruleYear kw= '-' this_Month_2= ruleMonth kw= '-' this_Day_4= ruleDay ) ;
    public final AntlrDatatypeRuleToken ruleDate_Literal() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_Year_0 = null;

        AntlrDatatypeRuleToken this_Month_2 = null;

        AntlrDatatypeRuleToken this_Day_4 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4582:28: ( (this_Year_0= ruleYear kw= '-' this_Month_2= ruleMonth kw= '-' this_Day_4= ruleDay ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4583:1: (this_Year_0= ruleYear kw= '-' this_Month_2= ruleMonth kw= '-' this_Day_4= ruleDay )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4583:1: (this_Year_0= ruleYear kw= '-' this_Month_2= ruleMonth kw= '-' this_Day_4= ruleDay )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4584:5: this_Year_0= ruleYear kw= '-' this_Month_2= ruleMonth kw= '-' this_Day_4= ruleDay
            {
             
                    newCompositeNode(grammarAccess.getDate_LiteralAccess().getYearParserRuleCall_0()); 
                
            pushFollow(FOLLOW_ruleYear_in_ruleDate_Literal10498);
            this_Year_0=ruleYear();

            state._fsp--;


            		current.merge(this_Year_0);
                
             
                    afterParserOrEnumRuleCall();
                
            kw=(Token)match(input,77,FOLLOW_77_in_ruleDate_Literal10516); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getDate_LiteralAccess().getHyphenMinusKeyword_1()); 
                
             
                    newCompositeNode(grammarAccess.getDate_LiteralAccess().getMonthParserRuleCall_2()); 
                
            pushFollow(FOLLOW_ruleMonth_in_ruleDate_Literal10538);
            this_Month_2=ruleMonth();

            state._fsp--;


            		current.merge(this_Month_2);
                
             
                    afterParserOrEnumRuleCall();
                
            kw=(Token)match(input,77,FOLLOW_77_in_ruleDate_Literal10556); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getDate_LiteralAccess().getHyphenMinusKeyword_3()); 
                
             
                    newCompositeNode(grammarAccess.getDate_LiteralAccess().getDayParserRuleCall_4()); 
                
            pushFollow(FOLLOW_ruleDay_in_ruleDate_Literal10578);
            this_Day_4=ruleDay();

            state._fsp--;


            		current.merge(this_Day_4);
                
             
                    afterParserOrEnumRuleCall();
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDate_Literal"


    // $ANTLR start "entryRuleYear"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4636:1: entryRuleYear returns [String current=null] : iv_ruleYear= ruleYear EOF ;
    public final String entryRuleYear() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleYear = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4637:2: (iv_ruleYear= ruleYear EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4638:2: iv_ruleYear= ruleYear EOF
            {
             newCompositeNode(grammarAccess.getYearRule()); 
            pushFollow(FOLLOW_ruleYear_in_entryRuleYear10624);
            iv_ruleYear=ruleYear();

            state._fsp--;

             current =iv_ruleYear.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleYear10635); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleYear"


    // $ANTLR start "ruleYear"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4645:1: ruleYear returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_INT_0= RULE_INT ;
    public final AntlrDatatypeRuleToken ruleYear() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;

         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4648:28: (this_INT_0= RULE_INT )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4649:5: this_INT_0= RULE_INT
            {
            this_INT_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleYear10674); 

            		current.merge(this_INT_0);
                
             
                newLeafNode(this_INT_0, grammarAccess.getYearAccess().getINTTerminalRuleCall()); 
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleYear"


    // $ANTLR start "entryRuleMonth"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4664:1: entryRuleMonth returns [String current=null] : iv_ruleMonth= ruleMonth EOF ;
    public final String entryRuleMonth() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleMonth = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4665:2: (iv_ruleMonth= ruleMonth EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4666:2: iv_ruleMonth= ruleMonth EOF
            {
             newCompositeNode(grammarAccess.getMonthRule()); 
            pushFollow(FOLLOW_ruleMonth_in_entryRuleMonth10719);
            iv_ruleMonth=ruleMonth();

            state._fsp--;

             current =iv_ruleMonth.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMonth10730); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMonth"


    // $ANTLR start "ruleMonth"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4673:1: ruleMonth returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_INT_0= RULE_INT ;
    public final AntlrDatatypeRuleToken ruleMonth() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;

         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4676:28: (this_INT_0= RULE_INT )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4677:5: this_INT_0= RULE_INT
            {
            this_INT_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleMonth10769); 

            		current.merge(this_INT_0);
                
             
                newLeafNode(this_INT_0, grammarAccess.getMonthAccess().getINTTerminalRuleCall()); 
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMonth"


    // $ANTLR start "entryRuleDay"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4692:1: entryRuleDay returns [String current=null] : iv_ruleDay= ruleDay EOF ;
    public final String entryRuleDay() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleDay = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4693:2: (iv_ruleDay= ruleDay EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4694:2: iv_ruleDay= ruleDay EOF
            {
             newCompositeNode(grammarAccess.getDayRule()); 
            pushFollow(FOLLOW_ruleDay_in_entryRuleDay10814);
            iv_ruleDay=ruleDay();

            state._fsp--;

             current =iv_ruleDay.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDay10825); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDay"


    // $ANTLR start "ruleDay"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4701:1: ruleDay returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_INT_0= RULE_INT ;
    public final AntlrDatatypeRuleToken ruleDay() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;

         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4704:28: (this_INT_0= RULE_INT )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4705:5: this_INT_0= RULE_INT
            {
            this_INT_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleDay10864); 

            		current.merge(this_INT_0);
                
             
                newLeafNode(this_INT_0, grammarAccess.getDayAccess().getINTTerminalRuleCall()); 
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDay"


    // $ANTLR start "entryRuleDate_And_Time"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4720:1: entryRuleDate_And_Time returns [EObject current=null] : iv_ruleDate_And_Time= ruleDate_And_Time EOF ;
    public final EObject entryRuleDate_And_Time() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDate_And_Time = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4721:2: (iv_ruleDate_And_Time= ruleDate_And_Time EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4722:2: iv_ruleDate_And_Time= ruleDate_And_Time EOF
            {
             newCompositeNode(grammarAccess.getDate_And_TimeRule()); 
            pushFollow(FOLLOW_ruleDate_And_Time_in_entryRuleDate_And_Time10908);
            iv_ruleDate_And_Time=ruleDate_And_Time();

            state._fsp--;

             current =iv_ruleDate_And_Time; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDate_And_Time10918); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDate_And_Time"


    // $ANTLR start "ruleDate_And_Time"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4729:1: ruleDate_And_Time returns [EObject current=null] : ( (otherlv_0= 'DATE_AND_TIME#' | otherlv_1= 'DT#' ) ( (lv_value_2_0= ruleDate_And_Time_Literal ) ) ) ;
    public final EObject ruleDate_And_Time() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_value_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4732:28: ( ( (otherlv_0= 'DATE_AND_TIME#' | otherlv_1= 'DT#' ) ( (lv_value_2_0= ruleDate_And_Time_Literal ) ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4733:1: ( (otherlv_0= 'DATE_AND_TIME#' | otherlv_1= 'DT#' ) ( (lv_value_2_0= ruleDate_And_Time_Literal ) ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4733:1: ( (otherlv_0= 'DATE_AND_TIME#' | otherlv_1= 'DT#' ) ( (lv_value_2_0= ruleDate_And_Time_Literal ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4733:2: (otherlv_0= 'DATE_AND_TIME#' | otherlv_1= 'DT#' ) ( (lv_value_2_0= ruleDate_And_Time_Literal ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4733:2: (otherlv_0= 'DATE_AND_TIME#' | otherlv_1= 'DT#' )
            int alt83=2;
            int LA83_0 = input.LA(1);

            if ( (LA83_0==113) ) {
                alt83=1;
            }
            else if ( (LA83_0==114) ) {
                alt83=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 83, 0, input);

                throw nvae;
            }
            switch (alt83) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4733:4: otherlv_0= 'DATE_AND_TIME#'
                    {
                    otherlv_0=(Token)match(input,113,FOLLOW_113_in_ruleDate_And_Time10956); 

                        	newLeafNode(otherlv_0, grammarAccess.getDate_And_TimeAccess().getDATE_AND_TIMEKeyword_0_0());
                        

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4738:7: otherlv_1= 'DT#'
                    {
                    otherlv_1=(Token)match(input,114,FOLLOW_114_in_ruleDate_And_Time10974); 

                        	newLeafNode(otherlv_1, grammarAccess.getDate_And_TimeAccess().getDTKeyword_0_1());
                        

                    }
                    break;

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4742:2: ( (lv_value_2_0= ruleDate_And_Time_Literal ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4743:1: (lv_value_2_0= ruleDate_And_Time_Literal )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4743:1: (lv_value_2_0= ruleDate_And_Time_Literal )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4744:3: lv_value_2_0= ruleDate_And_Time_Literal
            {
             
            	        newCompositeNode(grammarAccess.getDate_And_TimeAccess().getValueDate_And_Time_LiteralParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleDate_And_Time_Literal_in_ruleDate_And_Time10996);
            lv_value_2_0=ruleDate_And_Time_Literal();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getDate_And_TimeRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_2_0, 
                    		"Date_And_Time_Literal");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDate_And_Time"


    // $ANTLR start "entryRuleDate_And_Time_Literal"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4768:1: entryRuleDate_And_Time_Literal returns [String current=null] : iv_ruleDate_And_Time_Literal= ruleDate_And_Time_Literal EOF ;
    public final String entryRuleDate_And_Time_Literal() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleDate_And_Time_Literal = null;


        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4769:2: (iv_ruleDate_And_Time_Literal= ruleDate_And_Time_Literal EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4770:2: iv_ruleDate_And_Time_Literal= ruleDate_And_Time_Literal EOF
            {
             newCompositeNode(grammarAccess.getDate_And_Time_LiteralRule()); 
            pushFollow(FOLLOW_ruleDate_And_Time_Literal_in_entryRuleDate_And_Time_Literal11033);
            iv_ruleDate_And_Time_Literal=ruleDate_And_Time_Literal();

            state._fsp--;

             current =iv_ruleDate_And_Time_Literal.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDate_And_Time_Literal11044); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDate_And_Time_Literal"


    // $ANTLR start "ruleDate_And_Time_Literal"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4777:1: ruleDate_And_Time_Literal returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_Date_Literal_0= ruleDate_Literal kw= '-' this_Daytime_2= ruleDaytime ) ;
    public final AntlrDatatypeRuleToken ruleDate_And_Time_Literal() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_Date_Literal_0 = null;

        AntlrDatatypeRuleToken this_Daytime_2 = null;


         enterRule(); 
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4780:28: ( (this_Date_Literal_0= ruleDate_Literal kw= '-' this_Daytime_2= ruleDaytime ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4781:1: (this_Date_Literal_0= ruleDate_Literal kw= '-' this_Daytime_2= ruleDaytime )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4781:1: (this_Date_Literal_0= ruleDate_Literal kw= '-' this_Daytime_2= ruleDaytime )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4782:5: this_Date_Literal_0= ruleDate_Literal kw= '-' this_Daytime_2= ruleDaytime
            {
             
                    newCompositeNode(grammarAccess.getDate_And_Time_LiteralAccess().getDate_LiteralParserRuleCall_0()); 
                
            pushFollow(FOLLOW_ruleDate_Literal_in_ruleDate_And_Time_Literal11091);
            this_Date_Literal_0=ruleDate_Literal();

            state._fsp--;


            		current.merge(this_Date_Literal_0);
                
             
                    afterParserOrEnumRuleCall();
                
            kw=(Token)match(input,77,FOLLOW_77_in_ruleDate_And_Time_Literal11109); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getDate_And_Time_LiteralAccess().getHyphenMinusKeyword_1()); 
                
             
                    newCompositeNode(grammarAccess.getDate_And_Time_LiteralAccess().getDaytimeParserRuleCall_2()); 
                
            pushFollow(FOLLOW_ruleDaytime_in_ruleDate_And_Time_Literal11131);
            this_Daytime_2=ruleDaytime();

            state._fsp--;


            		current.merge(this_Daytime_2);
                
             
                    afterParserOrEnumRuleCall();
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDate_And_Time_Literal"


    // $ANTLR start "entryRuleNon_Generic_Type_Name"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4817:1: entryRuleNon_Generic_Type_Name returns [EObject current=null] : iv_ruleNon_Generic_Type_Name= ruleNon_Generic_Type_Name EOF ;
    public final EObject entryRuleNon_Generic_Type_Name() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNon_Generic_Type_Name = null;


         
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();
        	
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4821:2: (iv_ruleNon_Generic_Type_Name= ruleNon_Generic_Type_Name EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4822:2: iv_ruleNon_Generic_Type_Name= ruleNon_Generic_Type_Name EOF
            {
             newCompositeNode(grammarAccess.getNon_Generic_Type_NameRule()); 
            pushFollow(FOLLOW_ruleNon_Generic_Type_Name_in_entryRuleNon_Generic_Type_Name11182);
            iv_ruleNon_Generic_Type_Name=ruleNon_Generic_Type_Name();

            state._fsp--;

             current =iv_ruleNon_Generic_Type_Name; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleNon_Generic_Type_Name11192); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "entryRuleNon_Generic_Type_Name"


    // $ANTLR start "ruleNon_Generic_Type_Name"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4832:1: ruleNon_Generic_Type_Name returns [EObject current=null] : this_Elementary_Type_Name_0= ruleElementary_Type_Name ;
    public final EObject ruleNon_Generic_Type_Name() throws RecognitionException {
        EObject current = null;

        EObject this_Elementary_Type_Name_0 = null;


         enterRule(); 
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4836:28: (this_Elementary_Type_Name_0= ruleElementary_Type_Name )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4838:5: this_Elementary_Type_Name_0= ruleElementary_Type_Name
            {
             
                    newCompositeNode(grammarAccess.getNon_Generic_Type_NameAccess().getElementary_Type_NameParserRuleCall()); 
                
            pushFollow(FOLLOW_ruleElementary_Type_Name_in_ruleNon_Generic_Type_Name11242);
            this_Elementary_Type_Name_0=ruleElementary_Type_Name();

            state._fsp--;

             
                    current = this_Elementary_Type_Name_0; 
                    afterParserOrEnumRuleCall();
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "ruleNon_Generic_Type_Name"


    // $ANTLR start "entryRuleElementary_Type_Name"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4857:1: entryRuleElementary_Type_Name returns [EObject current=null] : iv_ruleElementary_Type_Name= ruleElementary_Type_Name EOF ;
    public final EObject entryRuleElementary_Type_Name() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleElementary_Type_Name = null;


         
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();
        	
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4861:2: (iv_ruleElementary_Type_Name= ruleElementary_Type_Name EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4862:2: iv_ruleElementary_Type_Name= ruleElementary_Type_Name EOF
            {
             newCompositeNode(grammarAccess.getElementary_Type_NameRule()); 
            pushFollow(FOLLOW_ruleElementary_Type_Name_in_entryRuleElementary_Type_Name11286);
            iv_ruleElementary_Type_Name=ruleElementary_Type_Name();

            state._fsp--;

             current =iv_ruleElementary_Type_Name; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleElementary_Type_Name11296); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "entryRuleElementary_Type_Name"


    // $ANTLR start "ruleElementary_Type_Name"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4872:1: ruleElementary_Type_Name returns [EObject current=null] : (this_Numeric_Type_Name_0= ruleNumeric_Type_Name | this_Date_Type_Name_1= ruleDate_Type_Name | this_Bit_String_Type_Name_2= ruleBit_String_Type_Name | this_Time_Type_Name_3= ruleTime_Type_Name ) ;
    public final EObject ruleElementary_Type_Name() throws RecognitionException {
        EObject current = null;

        EObject this_Numeric_Type_Name_0 = null;

        EObject this_Date_Type_Name_1 = null;

        EObject this_Bit_String_Type_Name_2 = null;

        EObject this_Time_Type_Name_3 = null;


         enterRule(); 
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4876:28: ( (this_Numeric_Type_Name_0= ruleNumeric_Type_Name | this_Date_Type_Name_1= ruleDate_Type_Name | this_Bit_String_Type_Name_2= ruleBit_String_Type_Name | this_Time_Type_Name_3= ruleTime_Type_Name ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4877:1: (this_Numeric_Type_Name_0= ruleNumeric_Type_Name | this_Date_Type_Name_1= ruleDate_Type_Name | this_Bit_String_Type_Name_2= ruleBit_String_Type_Name | this_Time_Type_Name_3= ruleTime_Type_Name )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4877:1: (this_Numeric_Type_Name_0= ruleNumeric_Type_Name | this_Date_Type_Name_1= ruleDate_Type_Name | this_Bit_String_Type_Name_2= ruleBit_String_Type_Name | this_Time_Type_Name_3= ruleTime_Type_Name )
            int alt84=4;
            switch ( input.LA(1) ) {
            case 115:
            case 116:
            case 117:
            case 118:
            case 119:
            case 120:
            case 121:
            case 122:
            case 123:
            case 124:
                {
                alt84=1;
                }
                break;
            case 132:
            case 133:
            case 134:
                {
                alt84=2;
                }
                break;
            case 125:
            case 126:
            case 127:
            case 128:
            case 129:
                {
                alt84=3;
                }
                break;
            case 130:
            case 131:
                {
                alt84=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 84, 0, input);

                throw nvae;
            }

            switch (alt84) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4878:5: this_Numeric_Type_Name_0= ruleNumeric_Type_Name
                    {
                     
                            newCompositeNode(grammarAccess.getElementary_Type_NameAccess().getNumeric_Type_NameParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleNumeric_Type_Name_in_ruleElementary_Type_Name11347);
                    this_Numeric_Type_Name_0=ruleNumeric_Type_Name();

                    state._fsp--;

                     
                            current = this_Numeric_Type_Name_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4888:5: this_Date_Type_Name_1= ruleDate_Type_Name
                    {
                     
                            newCompositeNode(grammarAccess.getElementary_Type_NameAccess().getDate_Type_NameParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleDate_Type_Name_in_ruleElementary_Type_Name11374);
                    this_Date_Type_Name_1=ruleDate_Type_Name();

                    state._fsp--;

                     
                            current = this_Date_Type_Name_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4898:5: this_Bit_String_Type_Name_2= ruleBit_String_Type_Name
                    {
                     
                            newCompositeNode(grammarAccess.getElementary_Type_NameAccess().getBit_String_Type_NameParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_ruleBit_String_Type_Name_in_ruleElementary_Type_Name11401);
                    this_Bit_String_Type_Name_2=ruleBit_String_Type_Name();

                    state._fsp--;

                     
                            current = this_Bit_String_Type_Name_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4908:5: this_Time_Type_Name_3= ruleTime_Type_Name
                    {
                     
                            newCompositeNode(grammarAccess.getElementary_Type_NameAccess().getTime_Type_NameParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_ruleTime_Type_Name_in_ruleElementary_Type_Name11428);
                    this_Time_Type_Name_3=ruleTime_Type_Name();

                    state._fsp--;

                     
                            current = this_Time_Type_Name_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "ruleElementary_Type_Name"


    // $ANTLR start "entryRuleNumeric_Type_Name"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4927:1: entryRuleNumeric_Type_Name returns [EObject current=null] : iv_ruleNumeric_Type_Name= ruleNumeric_Type_Name EOF ;
    public final EObject entryRuleNumeric_Type_Name() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNumeric_Type_Name = null;


         
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();
        	
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4931:2: (iv_ruleNumeric_Type_Name= ruleNumeric_Type_Name EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4932:2: iv_ruleNumeric_Type_Name= ruleNumeric_Type_Name EOF
            {
             newCompositeNode(grammarAccess.getNumeric_Type_NameRule()); 
            pushFollow(FOLLOW_ruleNumeric_Type_Name_in_entryRuleNumeric_Type_Name11473);
            iv_ruleNumeric_Type_Name=ruleNumeric_Type_Name();

            state._fsp--;

             current =iv_ruleNumeric_Type_Name; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleNumeric_Type_Name11483); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "entryRuleNumeric_Type_Name"


    // $ANTLR start "ruleNumeric_Type_Name"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4942:1: ruleNumeric_Type_Name returns [EObject current=null] : (this_Integer_Type_Name_0= ruleInteger_Type_Name | this_Real_Type_Name_1= ruleReal_Type_Name ) ;
    public final EObject ruleNumeric_Type_Name() throws RecognitionException {
        EObject current = null;

        EObject this_Integer_Type_Name_0 = null;

        EObject this_Real_Type_Name_1 = null;


         enterRule(); 
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4946:28: ( (this_Integer_Type_Name_0= ruleInteger_Type_Name | this_Real_Type_Name_1= ruleReal_Type_Name ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4947:1: (this_Integer_Type_Name_0= ruleInteger_Type_Name | this_Real_Type_Name_1= ruleReal_Type_Name )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4947:1: (this_Integer_Type_Name_0= ruleInteger_Type_Name | this_Real_Type_Name_1= ruleReal_Type_Name )
            int alt85=2;
            int LA85_0 = input.LA(1);

            if ( ((LA85_0>=115 && LA85_0<=122)) ) {
                alt85=1;
            }
            else if ( ((LA85_0>=123 && LA85_0<=124)) ) {
                alt85=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 85, 0, input);

                throw nvae;
            }
            switch (alt85) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4948:5: this_Integer_Type_Name_0= ruleInteger_Type_Name
                    {
                     
                            newCompositeNode(grammarAccess.getNumeric_Type_NameAccess().getInteger_Type_NameParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleInteger_Type_Name_in_ruleNumeric_Type_Name11534);
                    this_Integer_Type_Name_0=ruleInteger_Type_Name();

                    state._fsp--;

                     
                            current = this_Integer_Type_Name_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4958:5: this_Real_Type_Name_1= ruleReal_Type_Name
                    {
                     
                            newCompositeNode(grammarAccess.getNumeric_Type_NameAccess().getReal_Type_NameParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleReal_Type_Name_in_ruleNumeric_Type_Name11561);
                    this_Real_Type_Name_1=ruleReal_Type_Name();

                    state._fsp--;

                     
                            current = this_Real_Type_Name_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "ruleNumeric_Type_Name"


    // $ANTLR start "entryRuleInteger_Type_Name"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4977:1: entryRuleInteger_Type_Name returns [EObject current=null] : iv_ruleInteger_Type_Name= ruleInteger_Type_Name EOF ;
    public final EObject entryRuleInteger_Type_Name() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInteger_Type_Name = null;


         
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();
        	
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4981:2: (iv_ruleInteger_Type_Name= ruleInteger_Type_Name EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4982:2: iv_ruleInteger_Type_Name= ruleInteger_Type_Name EOF
            {
             newCompositeNode(grammarAccess.getInteger_Type_NameRule()); 
            pushFollow(FOLLOW_ruleInteger_Type_Name_in_entryRuleInteger_Type_Name11606);
            iv_ruleInteger_Type_Name=ruleInteger_Type_Name();

            state._fsp--;

             current =iv_ruleInteger_Type_Name; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInteger_Type_Name11616); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "entryRuleInteger_Type_Name"


    // $ANTLR start "ruleInteger_Type_Name"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4992:1: ruleInteger_Type_Name returns [EObject current=null] : (this_Signed_Integer_Type_Name_0= ruleSigned_Integer_Type_Name | this_Unsigned_Integer_Type_Name_1= ruleUnsigned_Integer_Type_Name ) ;
    public final EObject ruleInteger_Type_Name() throws RecognitionException {
        EObject current = null;

        EObject this_Signed_Integer_Type_Name_0 = null;

        EObject this_Unsigned_Integer_Type_Name_1 = null;


         enterRule(); 
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4996:28: ( (this_Signed_Integer_Type_Name_0= ruleSigned_Integer_Type_Name | this_Unsigned_Integer_Type_Name_1= ruleUnsigned_Integer_Type_Name ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4997:1: (this_Signed_Integer_Type_Name_0= ruleSigned_Integer_Type_Name | this_Unsigned_Integer_Type_Name_1= ruleUnsigned_Integer_Type_Name )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4997:1: (this_Signed_Integer_Type_Name_0= ruleSigned_Integer_Type_Name | this_Unsigned_Integer_Type_Name_1= ruleUnsigned_Integer_Type_Name )
            int alt86=2;
            int LA86_0 = input.LA(1);

            if ( ((LA86_0>=115 && LA86_0<=118)) ) {
                alt86=1;
            }
            else if ( ((LA86_0>=119 && LA86_0<=122)) ) {
                alt86=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 86, 0, input);

                throw nvae;
            }
            switch (alt86) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:4998:5: this_Signed_Integer_Type_Name_0= ruleSigned_Integer_Type_Name
                    {
                     
                            newCompositeNode(grammarAccess.getInteger_Type_NameAccess().getSigned_Integer_Type_NameParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleSigned_Integer_Type_Name_in_ruleInteger_Type_Name11667);
                    this_Signed_Integer_Type_Name_0=ruleSigned_Integer_Type_Name();

                    state._fsp--;

                     
                            current = this_Signed_Integer_Type_Name_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5008:5: this_Unsigned_Integer_Type_Name_1= ruleUnsigned_Integer_Type_Name
                    {
                     
                            newCompositeNode(grammarAccess.getInteger_Type_NameAccess().getUnsigned_Integer_Type_NameParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleUnsigned_Integer_Type_Name_in_ruleInteger_Type_Name11694);
                    this_Unsigned_Integer_Type_Name_1=ruleUnsigned_Integer_Type_Name();

                    state._fsp--;

                     
                            current = this_Unsigned_Integer_Type_Name_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "ruleInteger_Type_Name"


    // $ANTLR start "entryRuleSigned_Integer_Type_Name"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5027:1: entryRuleSigned_Integer_Type_Name returns [EObject current=null] : iv_ruleSigned_Integer_Type_Name= ruleSigned_Integer_Type_Name EOF ;
    public final EObject entryRuleSigned_Integer_Type_Name() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSigned_Integer_Type_Name = null;


         
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();
        	
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5031:2: (iv_ruleSigned_Integer_Type_Name= ruleSigned_Integer_Type_Name EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5032:2: iv_ruleSigned_Integer_Type_Name= ruleSigned_Integer_Type_Name EOF
            {
             newCompositeNode(grammarAccess.getSigned_Integer_Type_NameRule()); 
            pushFollow(FOLLOW_ruleSigned_Integer_Type_Name_in_entryRuleSigned_Integer_Type_Name11739);
            iv_ruleSigned_Integer_Type_Name=ruleSigned_Integer_Type_Name();

            state._fsp--;

             current =iv_ruleSigned_Integer_Type_Name; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSigned_Integer_Type_Name11749); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "entryRuleSigned_Integer_Type_Name"


    // $ANTLR start "ruleSigned_Integer_Type_Name"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5042:1: ruleSigned_Integer_Type_Name returns [EObject current=null] : ( () (otherlv_1= 'DINT' | otherlv_2= 'INT' | otherlv_3= 'SINT' | otherlv_4= 'LINT' ) ) ;
    public final EObject ruleSigned_Integer_Type_Name() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;

         enterRule(); 
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5046:28: ( ( () (otherlv_1= 'DINT' | otherlv_2= 'INT' | otherlv_3= 'SINT' | otherlv_4= 'LINT' ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5047:1: ( () (otherlv_1= 'DINT' | otherlv_2= 'INT' | otherlv_3= 'SINT' | otherlv_4= 'LINT' ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5047:1: ( () (otherlv_1= 'DINT' | otherlv_2= 'INT' | otherlv_3= 'SINT' | otherlv_4= 'LINT' ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5047:2: () (otherlv_1= 'DINT' | otherlv_2= 'INT' | otherlv_3= 'SINT' | otherlv_4= 'LINT' )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5047:2: ()
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5048:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getSigned_Integer_Type_NameAccess().getSigned_Integer_Type_NameAction_0(),
                        current);
                

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5053:2: (otherlv_1= 'DINT' | otherlv_2= 'INT' | otherlv_3= 'SINT' | otherlv_4= 'LINT' )
            int alt87=4;
            switch ( input.LA(1) ) {
            case 115:
                {
                alt87=1;
                }
                break;
            case 116:
                {
                alt87=2;
                }
                break;
            case 117:
                {
                alt87=3;
                }
                break;
            case 118:
                {
                alt87=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 87, 0, input);

                throw nvae;
            }

            switch (alt87) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5053:4: otherlv_1= 'DINT'
                    {
                    otherlv_1=(Token)match(input,115,FOLLOW_115_in_ruleSigned_Integer_Type_Name11800); 

                        	newLeafNode(otherlv_1, grammarAccess.getSigned_Integer_Type_NameAccess().getDINTKeyword_1_0());
                        

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5058:7: otherlv_2= 'INT'
                    {
                    otherlv_2=(Token)match(input,116,FOLLOW_116_in_ruleSigned_Integer_Type_Name11818); 

                        	newLeafNode(otherlv_2, grammarAccess.getSigned_Integer_Type_NameAccess().getINTKeyword_1_1());
                        

                    }
                    break;
                case 3 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5063:7: otherlv_3= 'SINT'
                    {
                    otherlv_3=(Token)match(input,117,FOLLOW_117_in_ruleSigned_Integer_Type_Name11836); 

                        	newLeafNode(otherlv_3, grammarAccess.getSigned_Integer_Type_NameAccess().getSINTKeyword_1_2());
                        

                    }
                    break;
                case 4 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5068:7: otherlv_4= 'LINT'
                    {
                    otherlv_4=(Token)match(input,118,FOLLOW_118_in_ruleSigned_Integer_Type_Name11854); 

                        	newLeafNode(otherlv_4, grammarAccess.getSigned_Integer_Type_NameAccess().getLINTKeyword_1_3());
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "ruleSigned_Integer_Type_Name"


    // $ANTLR start "entryRuleUnsigned_Integer_Type_Name"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5083:1: entryRuleUnsigned_Integer_Type_Name returns [EObject current=null] : iv_ruleUnsigned_Integer_Type_Name= ruleUnsigned_Integer_Type_Name EOF ;
    public final EObject entryRuleUnsigned_Integer_Type_Name() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnsigned_Integer_Type_Name = null;


         
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();
        	
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5087:2: (iv_ruleUnsigned_Integer_Type_Name= ruleUnsigned_Integer_Type_Name EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5088:2: iv_ruleUnsigned_Integer_Type_Name= ruleUnsigned_Integer_Type_Name EOF
            {
             newCompositeNode(grammarAccess.getUnsigned_Integer_Type_NameRule()); 
            pushFollow(FOLLOW_ruleUnsigned_Integer_Type_Name_in_entryRuleUnsigned_Integer_Type_Name11901);
            iv_ruleUnsigned_Integer_Type_Name=ruleUnsigned_Integer_Type_Name();

            state._fsp--;

             current =iv_ruleUnsigned_Integer_Type_Name; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleUnsigned_Integer_Type_Name11911); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "entryRuleUnsigned_Integer_Type_Name"


    // $ANTLR start "ruleUnsigned_Integer_Type_Name"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5098:1: ruleUnsigned_Integer_Type_Name returns [EObject current=null] : ( () (otherlv_1= 'UNIT' | otherlv_2= 'USINT' | otherlv_3= 'UDINT' | otherlv_4= 'ULINT' ) ) ;
    public final EObject ruleUnsigned_Integer_Type_Name() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;

         enterRule(); 
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5102:28: ( ( () (otherlv_1= 'UNIT' | otherlv_2= 'USINT' | otherlv_3= 'UDINT' | otherlv_4= 'ULINT' ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5103:1: ( () (otherlv_1= 'UNIT' | otherlv_2= 'USINT' | otherlv_3= 'UDINT' | otherlv_4= 'ULINT' ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5103:1: ( () (otherlv_1= 'UNIT' | otherlv_2= 'USINT' | otherlv_3= 'UDINT' | otherlv_4= 'ULINT' ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5103:2: () (otherlv_1= 'UNIT' | otherlv_2= 'USINT' | otherlv_3= 'UDINT' | otherlv_4= 'ULINT' )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5103:2: ()
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5104:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getUnsigned_Integer_Type_NameAccess().getUnsigned_Integer_Type_NameAction_0(),
                        current);
                

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5109:2: (otherlv_1= 'UNIT' | otherlv_2= 'USINT' | otherlv_3= 'UDINT' | otherlv_4= 'ULINT' )
            int alt88=4;
            switch ( input.LA(1) ) {
            case 119:
                {
                alt88=1;
                }
                break;
            case 120:
                {
                alt88=2;
                }
                break;
            case 121:
                {
                alt88=3;
                }
                break;
            case 122:
                {
                alt88=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 88, 0, input);

                throw nvae;
            }

            switch (alt88) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5109:4: otherlv_1= 'UNIT'
                    {
                    otherlv_1=(Token)match(input,119,FOLLOW_119_in_ruleUnsigned_Integer_Type_Name11962); 

                        	newLeafNode(otherlv_1, grammarAccess.getUnsigned_Integer_Type_NameAccess().getUNITKeyword_1_0());
                        

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5114:7: otherlv_2= 'USINT'
                    {
                    otherlv_2=(Token)match(input,120,FOLLOW_120_in_ruleUnsigned_Integer_Type_Name11980); 

                        	newLeafNode(otherlv_2, grammarAccess.getUnsigned_Integer_Type_NameAccess().getUSINTKeyword_1_1());
                        

                    }
                    break;
                case 3 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5119:7: otherlv_3= 'UDINT'
                    {
                    otherlv_3=(Token)match(input,121,FOLLOW_121_in_ruleUnsigned_Integer_Type_Name11998); 

                        	newLeafNode(otherlv_3, grammarAccess.getUnsigned_Integer_Type_NameAccess().getUDINTKeyword_1_2());
                        

                    }
                    break;
                case 4 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5124:7: otherlv_4= 'ULINT'
                    {
                    otherlv_4=(Token)match(input,122,FOLLOW_122_in_ruleUnsigned_Integer_Type_Name12016); 

                        	newLeafNode(otherlv_4, grammarAccess.getUnsigned_Integer_Type_NameAccess().getULINTKeyword_1_3());
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "ruleUnsigned_Integer_Type_Name"


    // $ANTLR start "entryRuleReal_Type_Name"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5139:1: entryRuleReal_Type_Name returns [EObject current=null] : iv_ruleReal_Type_Name= ruleReal_Type_Name EOF ;
    public final EObject entryRuleReal_Type_Name() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReal_Type_Name = null;


         
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();
        	
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5143:2: (iv_ruleReal_Type_Name= ruleReal_Type_Name EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5144:2: iv_ruleReal_Type_Name= ruleReal_Type_Name EOF
            {
             newCompositeNode(grammarAccess.getReal_Type_NameRule()); 
            pushFollow(FOLLOW_ruleReal_Type_Name_in_entryRuleReal_Type_Name12063);
            iv_ruleReal_Type_Name=ruleReal_Type_Name();

            state._fsp--;

             current =iv_ruleReal_Type_Name; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleReal_Type_Name12073); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "entryRuleReal_Type_Name"


    // $ANTLR start "ruleReal_Type_Name"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5154:1: ruleReal_Type_Name returns [EObject current=null] : ( () (otherlv_1= 'REAL' | otherlv_2= 'LREAL' ) ) ;
    public final EObject ruleReal_Type_Name() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;

         enterRule(); 
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5158:28: ( ( () (otherlv_1= 'REAL' | otherlv_2= 'LREAL' ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5159:1: ( () (otherlv_1= 'REAL' | otherlv_2= 'LREAL' ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5159:1: ( () (otherlv_1= 'REAL' | otherlv_2= 'LREAL' ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5159:2: () (otherlv_1= 'REAL' | otherlv_2= 'LREAL' )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5159:2: ()
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5160:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getReal_Type_NameAccess().getReal_Type_NameAction_0(),
                        current);
                

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5165:2: (otherlv_1= 'REAL' | otherlv_2= 'LREAL' )
            int alt89=2;
            int LA89_0 = input.LA(1);

            if ( (LA89_0==123) ) {
                alt89=1;
            }
            else if ( (LA89_0==124) ) {
                alt89=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 89, 0, input);

                throw nvae;
            }
            switch (alt89) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5165:4: otherlv_1= 'REAL'
                    {
                    otherlv_1=(Token)match(input,123,FOLLOW_123_in_ruleReal_Type_Name12124); 

                        	newLeafNode(otherlv_1, grammarAccess.getReal_Type_NameAccess().getREALKeyword_1_0());
                        

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5170:7: otherlv_2= 'LREAL'
                    {
                    otherlv_2=(Token)match(input,124,FOLLOW_124_in_ruleReal_Type_Name12142); 

                        	newLeafNode(otherlv_2, grammarAccess.getReal_Type_NameAccess().getLREALKeyword_1_1());
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "ruleReal_Type_Name"


    // $ANTLR start "entryRuleBit_String_Type_Name"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5185:1: entryRuleBit_String_Type_Name returns [EObject current=null] : iv_ruleBit_String_Type_Name= ruleBit_String_Type_Name EOF ;
    public final EObject entryRuleBit_String_Type_Name() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBit_String_Type_Name = null;


         
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();
        	
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5189:2: (iv_ruleBit_String_Type_Name= ruleBit_String_Type_Name EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5190:2: iv_ruleBit_String_Type_Name= ruleBit_String_Type_Name EOF
            {
             newCompositeNode(grammarAccess.getBit_String_Type_NameRule()); 
            pushFollow(FOLLOW_ruleBit_String_Type_Name_in_entryRuleBit_String_Type_Name12189);
            iv_ruleBit_String_Type_Name=ruleBit_String_Type_Name();

            state._fsp--;

             current =iv_ruleBit_String_Type_Name; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBit_String_Type_Name12199); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "entryRuleBit_String_Type_Name"


    // $ANTLR start "ruleBit_String_Type_Name"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5200:1: ruleBit_String_Type_Name returns [EObject current=null] : ( () (otherlv_1= 'BYTE' | otherlv_2= 'WORD' | otherlv_3= 'DWORD' | otherlv_4= 'BOOL' | otherlv_5= 'LWORD' ) ) ;
    public final EObject ruleBit_String_Type_Name() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;

         enterRule(); 
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5204:28: ( ( () (otherlv_1= 'BYTE' | otherlv_2= 'WORD' | otherlv_3= 'DWORD' | otherlv_4= 'BOOL' | otherlv_5= 'LWORD' ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5205:1: ( () (otherlv_1= 'BYTE' | otherlv_2= 'WORD' | otherlv_3= 'DWORD' | otherlv_4= 'BOOL' | otherlv_5= 'LWORD' ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5205:1: ( () (otherlv_1= 'BYTE' | otherlv_2= 'WORD' | otherlv_3= 'DWORD' | otherlv_4= 'BOOL' | otherlv_5= 'LWORD' ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5205:2: () (otherlv_1= 'BYTE' | otherlv_2= 'WORD' | otherlv_3= 'DWORD' | otherlv_4= 'BOOL' | otherlv_5= 'LWORD' )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5205:2: ()
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5206:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getBit_String_Type_NameAccess().getBit_String_Type_NameAction_0(),
                        current);
                

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5211:2: (otherlv_1= 'BYTE' | otherlv_2= 'WORD' | otherlv_3= 'DWORD' | otherlv_4= 'BOOL' | otherlv_5= 'LWORD' )
            int alt90=5;
            switch ( input.LA(1) ) {
            case 125:
                {
                alt90=1;
                }
                break;
            case 126:
                {
                alt90=2;
                }
                break;
            case 127:
                {
                alt90=3;
                }
                break;
            case 128:
                {
                alt90=4;
                }
                break;
            case 129:
                {
                alt90=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 90, 0, input);

                throw nvae;
            }

            switch (alt90) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5211:4: otherlv_1= 'BYTE'
                    {
                    otherlv_1=(Token)match(input,125,FOLLOW_125_in_ruleBit_String_Type_Name12250); 

                        	newLeafNode(otherlv_1, grammarAccess.getBit_String_Type_NameAccess().getBYTEKeyword_1_0());
                        

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5216:7: otherlv_2= 'WORD'
                    {
                    otherlv_2=(Token)match(input,126,FOLLOW_126_in_ruleBit_String_Type_Name12268); 

                        	newLeafNode(otherlv_2, grammarAccess.getBit_String_Type_NameAccess().getWORDKeyword_1_1());
                        

                    }
                    break;
                case 3 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5221:7: otherlv_3= 'DWORD'
                    {
                    otherlv_3=(Token)match(input,127,FOLLOW_127_in_ruleBit_String_Type_Name12286); 

                        	newLeafNode(otherlv_3, grammarAccess.getBit_String_Type_NameAccess().getDWORDKeyword_1_2());
                        

                    }
                    break;
                case 4 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5226:7: otherlv_4= 'BOOL'
                    {
                    otherlv_4=(Token)match(input,128,FOLLOW_128_in_ruleBit_String_Type_Name12304); 

                        	newLeafNode(otherlv_4, grammarAccess.getBit_String_Type_NameAccess().getBOOLKeyword_1_3());
                        

                    }
                    break;
                case 5 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5231:7: otherlv_5= 'LWORD'
                    {
                    otherlv_5=(Token)match(input,129,FOLLOW_129_in_ruleBit_String_Type_Name12322); 

                        	newLeafNode(otherlv_5, grammarAccess.getBit_String_Type_NameAccess().getLWORDKeyword_1_4());
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "ruleBit_String_Type_Name"


    // $ANTLR start "entryRuleTime_Type_Name"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5246:1: entryRuleTime_Type_Name returns [EObject current=null] : iv_ruleTime_Type_Name= ruleTime_Type_Name EOF ;
    public final EObject entryRuleTime_Type_Name() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTime_Type_Name = null;


         
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();
        	
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5250:2: (iv_ruleTime_Type_Name= ruleTime_Type_Name EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5251:2: iv_ruleTime_Type_Name= ruleTime_Type_Name EOF
            {
             newCompositeNode(grammarAccess.getTime_Type_NameRule()); 
            pushFollow(FOLLOW_ruleTime_Type_Name_in_entryRuleTime_Type_Name12369);
            iv_ruleTime_Type_Name=ruleTime_Type_Name();

            state._fsp--;

             current =iv_ruleTime_Type_Name; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTime_Type_Name12379); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "entryRuleTime_Type_Name"


    // $ANTLR start "ruleTime_Type_Name"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5261:1: ruleTime_Type_Name returns [EObject current=null] : ( () (otherlv_1= 'TIME' | otherlv_2= 'LTIME' ) ) ;
    public final EObject ruleTime_Type_Name() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;

         enterRule(); 
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5265:28: ( ( () (otherlv_1= 'TIME' | otherlv_2= 'LTIME' ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5266:1: ( () (otherlv_1= 'TIME' | otherlv_2= 'LTIME' ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5266:1: ( () (otherlv_1= 'TIME' | otherlv_2= 'LTIME' ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5266:2: () (otherlv_1= 'TIME' | otherlv_2= 'LTIME' )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5266:2: ()
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5267:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getTime_Type_NameAccess().getTime_Type_NameAction_0(),
                        current);
                

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5272:2: (otherlv_1= 'TIME' | otherlv_2= 'LTIME' )
            int alt91=2;
            int LA91_0 = input.LA(1);

            if ( (LA91_0==130) ) {
                alt91=1;
            }
            else if ( (LA91_0==131) ) {
                alt91=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 91, 0, input);

                throw nvae;
            }
            switch (alt91) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5272:4: otherlv_1= 'TIME'
                    {
                    otherlv_1=(Token)match(input,130,FOLLOW_130_in_ruleTime_Type_Name12430); 

                        	newLeafNode(otherlv_1, grammarAccess.getTime_Type_NameAccess().getTIMEKeyword_1_0());
                        

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5277:7: otherlv_2= 'LTIME'
                    {
                    otherlv_2=(Token)match(input,131,FOLLOW_131_in_ruleTime_Type_Name12448); 

                        	newLeafNode(otherlv_2, grammarAccess.getTime_Type_NameAccess().getLTIMEKeyword_1_1());
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "ruleTime_Type_Name"


    // $ANTLR start "entryRuleDate_Type_Name"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5292:1: entryRuleDate_Type_Name returns [EObject current=null] : iv_ruleDate_Type_Name= ruleDate_Type_Name EOF ;
    public final EObject entryRuleDate_Type_Name() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDate_Type_Name = null;


         
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();
        	
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5296:2: (iv_ruleDate_Type_Name= ruleDate_Type_Name EOF )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5297:2: iv_ruleDate_Type_Name= ruleDate_Type_Name EOF
            {
             newCompositeNode(grammarAccess.getDate_Type_NameRule()); 
            pushFollow(FOLLOW_ruleDate_Type_Name_in_entryRuleDate_Type_Name12495);
            iv_ruleDate_Type_Name=ruleDate_Type_Name();

            state._fsp--;

             current =iv_ruleDate_Type_Name; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDate_Type_Name12505); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "entryRuleDate_Type_Name"


    // $ANTLR start "ruleDate_Type_Name"
    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5307:1: ruleDate_Type_Name returns [EObject current=null] : ( () (otherlv_1= 'DATE' | otherlv_2= 'TIME_OF_DAY' | otherlv_3= 'DATE_AND_TIME' ) ) ;
    public final EObject ruleDate_Type_Name() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;

         enterRule(); 
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();
            
        try {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5311:28: ( ( () (otherlv_1= 'DATE' | otherlv_2= 'TIME_OF_DAY' | otherlv_3= 'DATE_AND_TIME' ) ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5312:1: ( () (otherlv_1= 'DATE' | otherlv_2= 'TIME_OF_DAY' | otherlv_3= 'DATE_AND_TIME' ) )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5312:1: ( () (otherlv_1= 'DATE' | otherlv_2= 'TIME_OF_DAY' | otherlv_3= 'DATE_AND_TIME' ) )
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5312:2: () (otherlv_1= 'DATE' | otherlv_2= 'TIME_OF_DAY' | otherlv_3= 'DATE_AND_TIME' )
            {
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5312:2: ()
            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5313:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getDate_Type_NameAccess().getDate_Type_NameAction_0(),
                        current);
                

            }

            // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5318:2: (otherlv_1= 'DATE' | otherlv_2= 'TIME_OF_DAY' | otherlv_3= 'DATE_AND_TIME' )
            int alt92=3;
            switch ( input.LA(1) ) {
            case 132:
                {
                alt92=1;
                }
                break;
            case 133:
                {
                alt92=2;
                }
                break;
            case 134:
                {
                alt92=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 92, 0, input);

                throw nvae;
            }

            switch (alt92) {
                case 1 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5318:4: otherlv_1= 'DATE'
                    {
                    otherlv_1=(Token)match(input,132,FOLLOW_132_in_ruleDate_Type_Name12556); 

                        	newLeafNode(otherlv_1, grammarAccess.getDate_Type_NameAccess().getDATEKeyword_1_0());
                        

                    }
                    break;
                case 2 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5323:7: otherlv_2= 'TIME_OF_DAY'
                    {
                    otherlv_2=(Token)match(input,133,FOLLOW_133_in_ruleDate_Type_Name12574); 

                        	newLeafNode(otherlv_2, grammarAccess.getDate_Type_NameAccess().getTIME_OF_DAYKeyword_1_1());
                        

                    }
                    break;
                case 3 :
                    // ../org.fordiac.ide.model/src-gen/org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredText.g:5328:7: otherlv_3= 'DATE_AND_TIME'
                    {
                    otherlv_3=(Token)match(input,134,FOLLOW_134_in_ruleDate_Type_Name12592); 

                        	newLeafNode(otherlv_3, grammarAccess.getDate_Type_NameAccess().getDATE_AND_TIMEKeyword_1_2());
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "ruleDate_Type_Name"

    // Delegated rules


 

    public static final BitSet FOLLOW_rulestructuredTextAlgorithm_in_entryRulestructuredTextAlgorithm75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulestructuredTextAlgorithm85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAdapterDefinition_in_rulestructuredTextAlgorithm140 = new BitSet(new long[]{0x0000000100200000L});
    public static final BitSet FOLLOW_21_in_rulestructuredTextAlgorithm153 = new BitSet(new long[]{0x0000000000400010L});
    public static final BitSet FOLLOW_ruleVarDeclaration_in_rulestructuredTextAlgorithm174 = new BitSet(new long[]{0x0000000000400010L});
    public static final BitSet FOLLOW_22_in_rulestructuredTextAlgorithm187 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_rulestructuredTextAlgorithm199 = new BitSet(new long[]{0x0000000000400010L});
    public static final BitSet FOLLOW_ruleVarDeclaration_in_rulestructuredTextAlgorithm220 = new BitSet(new long[]{0x0000000000400010L});
    public static final BitSet FOLLOW_22_in_rulestructuredTextAlgorithm233 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_24_in_rulestructuredTextAlgorithm245 = new BitSet(new long[]{0x0000000000400010L});
    public static final BitSet FOLLOW_ruleVarDeclaration_in_rulestructuredTextAlgorithm266 = new BitSet(new long[]{0x0000000000400010L});
    public static final BitSet FOLLOW_22_in_rulestructuredTextAlgorithm279 = new BitSet(new long[]{0x21E84000AA000010L,0x0000000000000001L});
    public static final BitSet FOLLOW_25_in_rulestructuredTextAlgorithm292 = new BitSet(new long[]{0x0000000004000010L});
    public static final BitSet FOLLOW_ruleAdapterDeclaration_in_rulestructuredTextAlgorithm313 = new BitSet(new long[]{0x0000000004000010L});
    public static final BitSet FOLLOW_26_in_rulestructuredTextAlgorithm326 = new BitSet(new long[]{0x21E84000A8000010L,0x0000000000000001L});
    public static final BitSet FOLLOW_27_in_rulestructuredTextAlgorithm341 = new BitSet(new long[]{0x0000000010000010L});
    public static final BitSet FOLLOW_ruleAdapterDeclaration_in_rulestructuredTextAlgorithm362 = new BitSet(new long[]{0x0000000010000010L});
    public static final BitSet FOLLOW_28_in_rulestructuredTextAlgorithm375 = new BitSet(new long[]{0x21E84000A0000010L,0x0000000000000001L});
    public static final BitSet FOLLOW_29_in_rulestructuredTextAlgorithm390 = new BitSet(new long[]{0x0000000000400010L});
    public static final BitSet FOLLOW_ruleVarDeclaration_in_rulestructuredTextAlgorithm411 = new BitSet(new long[]{0x0000000000400010L});
    public static final BitSet FOLLOW_22_in_rulestructuredTextAlgorithm424 = new BitSet(new long[]{0x21E8400080000010L,0x0000000000000001L});
    public static final BitSet FOLLOW_ruleStatementList_in_rulestructuredTextAlgorithm447 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVarDeclaration_in_entryRuleVarDeclaration483 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVarDeclaration493 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleVarDeclaration535 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_30_in_ruleVarDeclaration552 = new BitSet(new long[]{0x0000184000000000L,0xFFF8000000000000L,0x000000000000007FL});
    public static final BitSet FOLLOW_ruleSimple_Spec_Init_in_ruleVarDeclaration575 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_ruleArray_Spec_Init_in_ruleVarDeclaration594 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_ruleS_Byte_String_Spec_in_ruleVarDeclaration613 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_ruleD_Byte_String_Spec_in_ruleVarDeclaration632 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_ruleVarDeclaration647 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAdapterDefinition_in_entryRuleAdapterDefinition683 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAdapterDefinition693 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_ruleAdapterDefinition730 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleAdapterDefinition747 = new BitSet(new long[]{0x0000000200A00000L});
    public static final BitSet FOLLOW_21_in_ruleAdapterDefinition765 = new BitSet(new long[]{0x0000000000400010L});
    public static final BitSet FOLLOW_ruleVarDeclaration_in_ruleAdapterDefinition786 = new BitSet(new long[]{0x0000000000400010L});
    public static final BitSet FOLLOW_22_in_ruleAdapterDefinition799 = new BitSet(new long[]{0x0000000200800000L});
    public static final BitSet FOLLOW_23_in_ruleAdapterDefinition814 = new BitSet(new long[]{0x0000000000400010L});
    public static final BitSet FOLLOW_ruleVarDeclaration_in_ruleAdapterDefinition835 = new BitSet(new long[]{0x0000000000400010L});
    public static final BitSet FOLLOW_22_in_ruleAdapterDefinition848 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_33_in_ruleAdapterDefinition862 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAdapterDeclaration_in_entryRuleAdapterDeclaration898 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAdapterDeclaration908 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleAdapterDeclaration950 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_30_in_ruleAdapterDeclaration967 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleAdapterDeclaration987 = new BitSet(new long[]{0x0000000480000000L});
    public static final BitSet FOLLOW_34_in_ruleAdapterDeclaration1000 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleParameter_in_ruleAdapterDeclaration1021 = new BitSet(new long[]{0x0000001800000000L});
    public static final BitSet FOLLOW_35_in_ruleAdapterDeclaration1034 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleParameter_in_ruleAdapterDeclaration1055 = new BitSet(new long[]{0x0000001800000000L});
    public static final BitSet FOLLOW_36_in_ruleAdapterDeclaration1069 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_ruleAdapterDeclaration1083 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParameter_in_entryRuleParameter1119 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleParameter1129 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleParameter1174 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_37_in_ruleParameter1186 = new BitSet(new long[]{0x0000008000000FE0L,0x0007FFF3FFF03000L});
    public static final BitSet FOLLOW_ruleConstant_in_ruleParameter1209 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_ruleArray_Initialization_in_ruleParameter1228 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_ruleParameter1243 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSimple_Spec_Init_in_entryRuleSimple_Spec_Init1279 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSimple_Spec_Init1289 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleElementary_Type_Name_in_ruleSimple_Spec_Init1335 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_37_in_ruleSimple_Spec_Init1348 = new BitSet(new long[]{0x0000000000000FE0L,0x0007FFF3FFF03000L});
    public static final BitSet FOLLOW_ruleConstant_in_ruleSimple_Spec_Init1369 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArray_Spec_Init_in_entryRuleArray_Spec_Init1407 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleArray_Spec_Init1417 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArray_Specification_in_ruleArray_Spec_Init1464 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_37_in_ruleArray_Spec_Init1476 = new BitSet(new long[]{0x0000008000000FE0L,0x0007FFF3FFF03000L});
    public static final BitSet FOLLOW_ruleArray_Initialization_in_ruleArray_Spec_Init1497 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArray_Specification_in_entryRuleArray_Specification1535 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleArray_Specification1545 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_ruleArray_Specification1582 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_39_in_ruleArray_Specification1594 = new BitSet(new long[]{0x0000000000000020L,0x0000000000003000L});
    public static final BitSet FOLLOW_ruleSubrange_in_ruleArray_Specification1615 = new BitSet(new long[]{0x0000010800000000L});
    public static final BitSet FOLLOW_35_in_ruleArray_Specification1628 = new BitSet(new long[]{0x0000000000000020L,0x0000000000003000L});
    public static final BitSet FOLLOW_ruleSubrange_in_ruleArray_Specification1649 = new BitSet(new long[]{0x0000010800000000L});
    public static final BitSet FOLLOW_40_in_ruleArray_Specification1663 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_ruleArray_Specification1675 = new BitSet(new long[]{0x0000000000000000L,0xFFF8000000000000L,0x000000000000007FL});
    public static final BitSet FOLLOW_ruleNon_Generic_Type_Name_in_ruleArray_Specification1696 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSubrange_in_entryRuleSubrange1732 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSubrange1742 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSignedInteger_in_ruleSubrange1788 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_42_in_ruleSubrange1800 = new BitSet(new long[]{0x0000000000000020L,0x0000000000003000L});
    public static final BitSet FOLLOW_ruleSignedInteger_in_ruleSubrange1821 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArray_Initialization_in_entryRuleArray_Initialization1857 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleArray_Initialization1867 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_39_in_ruleArray_Initialization1904 = new BitSet(new long[]{0x0000000000000FE0L,0x0007FFF3FFF03000L});
    public static final BitSet FOLLOW_ruleArray_Initial_Elements_in_ruleArray_Initialization1925 = new BitSet(new long[]{0x0000010800000000L});
    public static final BitSet FOLLOW_35_in_ruleArray_Initialization1938 = new BitSet(new long[]{0x0000000000000FE0L,0x0007FFF3FFF03000L});
    public static final BitSet FOLLOW_ruleArray_Initial_Elements_in_ruleArray_Initialization1959 = new BitSet(new long[]{0x0000010800000000L});
    public static final BitSet FOLLOW_40_in_ruleArray_Initialization1973 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArray_Initial_Elements_in_entryRuleArray_Initial_Elements2009 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleArray_Initial_Elements2019 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleConstant_in_ruleArray_Initial_Elements2066 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleArray_Initial_Elements2089 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_34_in_ruleArray_Initial_Elements2106 = new BitSet(new long[]{0x0000001000000FE0L,0x0007FFF3FFF03000L});
    public static final BitSet FOLLOW_ruleConstant_in_ruleArray_Initial_Elements2127 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_36_in_ruleArray_Initial_Elements2140 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleS_Byte_String_Spec_in_entryRuleS_Byte_String_Spec2179 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleS_Byte_String_Spec2189 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_43_in_ruleS_Byte_String_Spec2232 = new BitSet(new long[]{0x000000A000000002L});
    public static final BitSet FOLLOW_39_in_ruleS_Byte_String_Spec2258 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleS_Byte_String_Spec2275 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_40_in_ruleS_Byte_String_Spec2292 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_37_in_ruleS_Byte_String_Spec2307 = new BitSet(new long[]{0x0000000000000400L,0x0000020000000000L});
    public static final BitSet FOLLOW_ruleSingle_Byte_Character_String_Literal_in_ruleS_Byte_String_Spec2328 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleD_Byte_String_Spec_in_entryRuleD_Byte_String_Spec2366 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleD_Byte_String_Spec2376 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_44_in_ruleD_Byte_String_Spec2419 = new BitSet(new long[]{0x000000A000000002L});
    public static final BitSet FOLLOW_39_in_ruleD_Byte_String_Spec2445 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleD_Byte_String_Spec2462 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_40_in_ruleD_Byte_String_Spec2479 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_37_in_ruleD_Byte_String_Spec2494 = new BitSet(new long[]{0x0000000000000C00L,0x0000060000000000L});
    public static final BitSet FOLLOW_ruleDouble_Byte_Character_String_Literal_in_ruleD_Byte_String_Spec2515 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStatementList_in_entryRuleStatementList2553 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleStatementList2563 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStatement_in_ruleStatementList2619 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_ruleStatementList2632 = new BitSet(new long[]{0x21E8400080000012L,0x0000000000000001L});
    public static final BitSet FOLLOW_ruleStatement_in_entryRuleStatement2670 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleStatement2680 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAssignment_Statement_in_ruleStatement2727 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSelection_Statement_in_ruleStatement2754 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIterationControl_Stmt_in_ruleStatement2781 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFor_Statement_in_ruleStatement2808 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRepeat_Statement_in_ruleStatement2835 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWhile_Statement_in_ruleStatement2862 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSubprog_Ctrl_Stmt_in_ruleStatement2889 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAssignment_Statement_in_entryRuleAssignment_Statement2924 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAssignment_Statement2934 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariable_in_ruleAssignment_Statement2980 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_37_in_ruleAssignment_Statement2992 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleAssignment_Statement3013 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariable_in_entryRuleVariable3049 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVariable3059 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVar_Variable_in_ruleVariable3106 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArray_Variable_in_ruleVariable3133 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAdapter_Variable_in_ruleVariable3160 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVar_Variable_in_entryRuleVar_Variable3195 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVar_Variable3205 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleVar_Variable3249 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAdapter_Variable_in_entryRuleAdapter_Variable3284 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAdapter_Variable3294 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleAdapter_Variable3339 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_45_in_ruleAdapter_Variable3351 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleAdapter_Variable3371 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArray_Variable_in_entryRuleArray_Variable3407 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleArray_Variable3417 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleArray_Variable3462 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_39_in_ruleArray_Variable3474 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleArray_Variable3495 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_40_in_ruleArray_Variable3507 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSelection_Statement_in_entryRuleSelection_Statement3543 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSelection_Statement3553 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIf_Statement_in_ruleSelection_Statement3600 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCase_Statement_in_ruleSelection_Statement3627 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIf_Statement_in_entryRuleIf_Statement3662 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIf_Statement3672 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_46_in_ruleIf_Statement3709 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleIf_Statement3730 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_ruleIf_Statement3742 = new BitSet(new long[]{0x21EF400080000010L,0x0000000000000001L});
    public static final BitSet FOLLOW_ruleStatementList_in_ruleIf_Statement3763 = new BitSet(new long[]{0x0007000000000000L});
    public static final BitSet FOLLOW_ruleElseIf_Statement_in_ruleIf_Statement3784 = new BitSet(new long[]{0x0003000000000000L});
    public static final BitSet FOLLOW_48_in_ruleIf_Statement3798 = new BitSet(new long[]{0x21EA400080000010L,0x0000000000000001L});
    public static final BitSet FOLLOW_ruleStatementList_in_ruleIf_Statement3819 = new BitSet(new long[]{0x0002000000000000L});
    public static final BitSet FOLLOW_49_in_ruleIf_Statement3833 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleElseIf_Statement_in_entryRuleElseIf_Statement3869 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleElseIf_Statement3879 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_50_in_ruleElseIf_Statement3916 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleElseIf_Statement3937 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_ruleElseIf_Statement3949 = new BitSet(new long[]{0x21E8400080000010L,0x0000000000000001L});
    public static final BitSet FOLLOW_ruleStatementList_in_ruleElseIf_Statement3970 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCase_Statement_in_entryRuleCase_Statement4006 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCase_Statement4016 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_51_in_ruleCase_Statement4053 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleCase_Statement4074 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_ruleCase_Statement4086 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ruleCase_Element_in_ruleCase_Statement4107 = new BitSet(new long[]{0x0011000000000020L});
    public static final BitSet FOLLOW_ruleCase_Else_in_ruleCase_Statement4129 = new BitSet(new long[]{0x0010000000000000L});
    public static final BitSet FOLLOW_52_in_ruleCase_Statement4142 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCase_Element_in_entryRuleCase_Element4178 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCase_Element4188 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleCase_Element4230 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_30_in_ruleCase_Element4247 = new BitSet(new long[]{0x21E8400080000010L,0x0000000000000001L});
    public static final BitSet FOLLOW_ruleStatementList_in_ruleCase_Element4268 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCase_Else_in_entryRuleCase_Else4304 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCase_Else4314 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_48_in_ruleCase_Else4360 = new BitSet(new long[]{0x21E8400080000010L,0x0000000000000001L});
    public static final BitSet FOLLOW_ruleStatementList_in_ruleCase_Else4381 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIterationControl_Stmt_in_entryRuleIterationControl_Stmt4417 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIterationControl_Stmt4427 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_53_in_ruleIterationControl_Stmt4471 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_54_in_ruleIterationControl_Stmt4500 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_55_in_ruleIterationControl_Stmt4529 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFor_Statement_in_entryRuleFor_Statement4580 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFor_Statement4590 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_56_in_ruleFor_Statement4627 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleFor_Statement4647 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_37_in_ruleFor_Statement4659 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleFor_Statement4680 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_57_in_ruleFor_Statement4692 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleFor_Statement4713 = new BitSet(new long[]{0x0C00000000000000L});
    public static final BitSet FOLLOW_58_in_ruleFor_Statement4726 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleFor_Statement4747 = new BitSet(new long[]{0x0800000000000000L});
    public static final BitSet FOLLOW_59_in_ruleFor_Statement4761 = new BitSet(new long[]{0x31E8400080000010L,0x0000000000000001L});
    public static final BitSet FOLLOW_ruleStatementList_in_ruleFor_Statement4782 = new BitSet(new long[]{0x1000000000000000L});
    public static final BitSet FOLLOW_60_in_ruleFor_Statement4794 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRepeat_Statement_in_entryRuleRepeat_Statement4830 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRepeat_Statement4840 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_61_in_ruleRepeat_Statement4877 = new BitSet(new long[]{0x61E8400080000010L,0x0000000000000001L});
    public static final BitSet FOLLOW_ruleStatementList_in_ruleRepeat_Statement4898 = new BitSet(new long[]{0x4000000000000000L});
    public static final BitSet FOLLOW_62_in_ruleRepeat_Statement4910 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleRepeat_Statement4931 = new BitSet(new long[]{0x8000000000000000L});
    public static final BitSet FOLLOW_63_in_ruleRepeat_Statement4943 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWhile_Statement_in_entryRuleWhile_Statement4979 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWhile_Statement4989 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_64_in_ruleWhile_Statement5026 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleWhile_Statement5047 = new BitSet(new long[]{0x0800000000000000L});
    public static final BitSet FOLLOW_59_in_ruleWhile_Statement5059 = new BitSet(new long[]{0x21E8400080000010L,0x0000000000000003L});
    public static final BitSet FOLLOW_ruleStatementList_in_ruleWhile_Statement5080 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000002L});
    public static final BitSet FOLLOW_65_in_ruleWhile_Statement5092 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSubprog_Ctrl_Stmt_in_entryRuleSubprog_Ctrl_Stmt5128 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSubprog_Ctrl_Stmt5138 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFunc_Call_in_ruleSubprog_Ctrl_Stmt5183 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_entryRuleExpression5218 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleExpression5228 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXor_Expression_in_ruleExpression5275 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000004L});
    public static final BitSet FOLLOW_66_in_ruleExpression5302 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_ruleXor_Expression_in_ruleExpression5336 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000004L});
    public static final BitSet FOLLOW_ruleXor_Expression_in_entryRuleXor_Expression5374 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXor_Expression5384 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAnd_Expression_in_ruleXor_Expression5431 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000008L});
    public static final BitSet FOLLOW_67_in_ruleXor_Expression5458 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_ruleAnd_Expression_in_ruleXor_Expression5492 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000008L});
    public static final BitSet FOLLOW_ruleAnd_Expression_in_entryRuleAnd_Expression5530 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAnd_Expression5540 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleComparison_in_ruleAnd_Expression5587 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000030L});
    public static final BitSet FOLLOW_68_in_ruleAnd_Expression5616 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_69_in_ruleAnd_Expression5645 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_ruleComparison_in_ruleAnd_Expression5682 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000030L});
    public static final BitSet FOLLOW_ruleComparison_in_entryRuleComparison5720 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleComparison5730 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEqu_Expression_in_ruleComparison5777 = new BitSet(new long[]{0x0000000000000002L,0x00000000000000C0L});
    public static final BitSet FOLLOW_70_in_ruleComparison5806 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_71_in_ruleComparison5835 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_ruleEqu_Expression_in_ruleComparison5872 = new BitSet(new long[]{0x0000000000000002L,0x00000000000000C0L});
    public static final BitSet FOLLOW_ruleEqu_Expression_in_entryRuleEqu_Expression5910 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEqu_Expression5920 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAdd_Expression_in_ruleEqu_Expression5967 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000F00L});
    public static final BitSet FOLLOW_72_in_ruleEqu_Expression5996 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_73_in_ruleEqu_Expression6025 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_74_in_ruleEqu_Expression6054 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_75_in_ruleEqu_Expression6083 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_ruleAdd_Expression_in_ruleEqu_Expression6120 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000F00L});
    public static final BitSet FOLLOW_ruleAdd_Expression_in_entryRuleAdd_Expression6158 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAdd_Expression6168 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTerm_in_ruleAdd_Expression6215 = new BitSet(new long[]{0x0000000000000002L,0x0000000000003000L});
    public static final BitSet FOLLOW_76_in_ruleAdd_Expression6244 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_77_in_ruleAdd_Expression6273 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_ruleTerm_in_ruleAdd_Expression6310 = new BitSet(new long[]{0x0000000000000002L,0x0000000000003000L});
    public static final BitSet FOLLOW_ruleTerm_in_entryRuleTerm6348 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTerm6358 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePower_Expression_in_ruleTerm6405 = new BitSet(new long[]{0x0000000000000002L,0x000000000001C000L});
    public static final BitSet FOLLOW_78_in_ruleTerm6434 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_79_in_ruleTerm6463 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_80_in_ruleTerm6492 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_rulePower_Expression_in_ruleTerm6529 = new BitSet(new long[]{0x0000000000000002L,0x000000000001C000L});
    public static final BitSet FOLLOW_rulePower_Expression_in_entryRulePower_Expression6567 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePower_Expression6577 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUnary_Expression_in_rulePower_Expression6624 = new BitSet(new long[]{0x0000000000000002L,0x0000000000020000L});
    public static final BitSet FOLLOW_81_in_rulePower_Expression6651 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_ruleUnary_Expression_in_rulePower_Expression6685 = new BitSet(new long[]{0x0000000000000002L,0x0000000000020000L});
    public static final BitSet FOLLOW_ruleUnary_Expression_in_entryRuleUnary_Expression6723 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleUnary_Expression6733 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_82_in_ruleUnary_Expression6785 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_rulePrimary_Expression_in_ruleUnary_Expression6820 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrimary_Expression_in_entryRulePrimary_Expression6856 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePrimary_Expression6866 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleConstant_in_rulePrimary_Expression6913 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_77_in_rulePrimary_Expression6932 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleVariable_in_rulePrimary_Expression6956 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFunc_Call_in_rulePrimary_Expression6984 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_rulePrimary_Expression7002 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_ruleExpression_in_rulePrimary_Expression7024 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_36_in_rulePrimary_Expression7035 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFunc_Call_in_entryRuleFunc_Call7072 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFunc_Call7082 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleFunc_Call7124 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_34_in_ruleFunc_Call7141 = new BitSet(new long[]{0x21E8401400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_ruleParam_Assign_in_ruleFunc_Call7163 = new BitSet(new long[]{0x0000001800000000L});
    public static final BitSet FOLLOW_35_in_ruleFunc_Call7176 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_ruleParam_Assign_in_ruleFunc_Call7197 = new BitSet(new long[]{0x0000001800000000L});
    public static final BitSet FOLLOW_36_in_ruleFunc_Call7213 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParam_Assign_in_entryRuleParam_Assign7249 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleParam_Assign7259 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleParam_Assign7303 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_37_in_ruleParam_Assign7320 = new BitSet(new long[]{0x21E8400400000FF0L,0x0007FFF3FFF43001L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleParam_Assign7343 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_82_in_ruleParam_Assign7364 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleParam_Assign7383 = new BitSet(new long[]{0x0000000000000000L,0x0000000000080000L});
    public static final BitSet FOLLOW_83_in_ruleParam_Assign7400 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleVariable_in_ruleParam_Assign7421 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleConstant_in_entryRuleConstant7458 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleConstant7468 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumeric_Literal_in_ruleConstant7515 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCharacter_String_in_ruleConstant7542 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBoolean_Literal_in_ruleConstant7569 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTime_Literal_in_ruleConstant7596 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumeric_Literal_in_entryRuleNumeric_Literal7633 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNumeric_Literal7643 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerLiteral_in_ruleNumeric_Literal7690 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReal_Literal_in_ruleNumeric_Literal7717 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerLiteral_in_entryRuleIntegerLiteral7752 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntegerLiteral7762 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_84_in_ruleIntegerLiteral7807 = new BitSet(new long[]{0x00000000000001E0L,0x0000000000003000L});
    public static final BitSet FOLLOW_85_in_ruleIntegerLiteral7836 = new BitSet(new long[]{0x00000000000001E0L,0x0000000000003000L});
    public static final BitSet FOLLOW_86_in_ruleIntegerLiteral7865 = new BitSet(new long[]{0x00000000000001E0L,0x0000000000003000L});
    public static final BitSet FOLLOW_87_in_ruleIntegerLiteral7894 = new BitSet(new long[]{0x00000000000001E0L,0x0000000000003000L});
    public static final BitSet FOLLOW_88_in_ruleIntegerLiteral7923 = new BitSet(new long[]{0x00000000000001E0L,0x0000000000003000L});
    public static final BitSet FOLLOW_89_in_ruleIntegerLiteral7952 = new BitSet(new long[]{0x00000000000001E0L,0x0000000000003000L});
    public static final BitSet FOLLOW_90_in_ruleIntegerLiteral7981 = new BitSet(new long[]{0x00000000000001E0L,0x0000000000003000L});
    public static final BitSet FOLLOW_91_in_ruleIntegerLiteral8010 = new BitSet(new long[]{0x00000000000001E0L,0x0000000000003000L});
    public static final BitSet FOLLOW_92_in_ruleIntegerLiteral8039 = new BitSet(new long[]{0x00000000000001E0L,0x0000000000003000L});
    public static final BitSet FOLLOW_93_in_ruleIntegerLiteral8068 = new BitSet(new long[]{0x00000000000001E0L,0x0000000000003000L});
    public static final BitSet FOLLOW_94_in_ruleIntegerLiteral8097 = new BitSet(new long[]{0x00000000000001E0L,0x0000000000003000L});
    public static final BitSet FOLLOW_95_in_ruleIntegerLiteral8126 = new BitSet(new long[]{0x00000000000001E0L,0x0000000000003000L});
    public static final BitSet FOLLOW_ruleSignedInteger_in_ruleIntegerLiteral8166 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_BINARY_INTEGER_VALUE_in_ruleIntegerLiteral8181 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_OCTAL_INTEGER_VALUE_in_ruleIntegerLiteral8201 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_HEX_INTEGER_VALUE_in_ruleIntegerLiteral8221 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSignedInteger_in_entryRuleSignedInteger8266 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSignedInteger8277 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_76_in_ruleSignedInteger8316 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_77_in_ruleSignedInteger8335 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleSignedInteger8352 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReal_Literal_in_entryRuleReal_Literal8397 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleReal_Literal8407 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_96_in_ruleReal_Literal8452 = new BitSet(new long[]{0x0000000000000200L,0x0000000000003000L});
    public static final BitSet FOLLOW_97_in_ruleReal_Literal8481 = new BitSet(new long[]{0x0000000000000200L,0x0000000000003000L});
    public static final BitSet FOLLOW_76_in_ruleReal_Literal8511 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_77_in_ruleReal_Literal8529 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_RULE_REALVALUE_in_ruleReal_Literal8548 = new BitSet(new long[]{0x0000000000000002L,0x0000000C00000000L});
    public static final BitSet FOLLOW_98_in_ruleReal_Literal8567 = new BitSet(new long[]{0x0000000000000020L,0x0000000000003000L});
    public static final BitSet FOLLOW_99_in_ruleReal_Literal8585 = new BitSet(new long[]{0x0000000000000020L,0x0000000000003000L});
    public static final BitSet FOLLOW_ruleSignedInteger_in_ruleReal_Literal8607 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBoolean_Literal_in_entryRuleBoolean_Literal8645 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBoolean_Literal8655 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_100_in_ruleBoolean_Literal8707 = new BitSet(new long[]{0x0000000000000000L,0x000001E000000000L});
    public static final BitSet FOLLOW_101_in_ruleBoolean_Literal8741 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_102_in_ruleBoolean_Literal8770 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_103_in_ruleBoolean_Literal8799 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_104_in_ruleBoolean_Literal8828 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCharacter_String_in_entryRuleCharacter_String8880 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCharacter_String8890 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSingle_Byte_Character_String_Literal_in_ruleCharacter_String8937 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDouble_Byte_Character_String_Literal_in_ruleCharacter_String8964 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSingle_Byte_Character_String_Literal_in_entryRuleSingle_Byte_Character_String_Literal8999 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSingle_Byte_Character_String_Literal9009 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_105_in_ruleSingle_Byte_Character_String_Literal9052 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_RULE_S_BYTE_CHAR_STRING_in_ruleSingle_Byte_Character_String_Literal9083 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDouble_Byte_Character_String_Literal_in_entryRuleDouble_Byte_Character_String_Literal9124 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDouble_Byte_Character_String_Literal9134 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_106_in_ruleDouble_Byte_Character_String_Literal9177 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_RULE_D_BYTE_CHAR_STRING_in_ruleDouble_Byte_Character_String_Literal9208 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTime_Literal_in_entryRuleTime_Literal9249 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTime_Literal9259 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDuration_in_ruleTime_Literal9306 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTime_Of_Day_in_ruleTime_Literal9333 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDate_in_ruleTime_Literal9360 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDate_And_Time_in_ruleTime_Literal9387 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDuration_in_entryRuleDuration9422 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDuration9432 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_107_in_ruleDuration9470 = new BitSet(new long[]{0x0000000000001000L,0x0000000000003000L});
    public static final BitSet FOLLOW_108_in_ruleDuration9488 = new BitSet(new long[]{0x0000000000001000L,0x0000000000003000L});
    public static final BitSet FOLLOW_76_in_ruleDuration9502 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_77_in_ruleDuration9520 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_RULE_TIME_in_ruleDuration9539 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFix_Point_in_entryRuleFix_Point9581 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFix_Point9592 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleFix_Point9632 = new BitSet(new long[]{0x0000200000000002L});
    public static final BitSet FOLLOW_45_in_ruleFix_Point9651 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleFix_Point9666 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTime_Of_Day_in_entryRuleTime_Of_Day9715 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTime_Of_Day9725 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_109_in_ruleTime_Of_Day9763 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_110_in_ruleTime_Of_Day9781 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ruleDaytime_in_ruleTime_Of_Day9803 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDaytime_in_entryRuleDaytime9840 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDaytime9851 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDay_Hour_in_ruleDaytime9898 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_30_in_ruleDaytime9916 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ruleDay_Minute_in_ruleDaytime9938 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_30_in_ruleDaytime9956 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ruleDay_Second_in_ruleDaytime9978 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDay_Hour_in_entryRuleDay_Hour10024 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDay_Hour10035 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleDay_Hour10074 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDay_Minute_in_entryRuleDay_Minute10119 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDay_Minute10130 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleDay_Minute10169 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDay_Second_in_entryRuleDay_Second10214 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDay_Second10225 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFix_Point_in_ruleDay_Second10271 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDate_in_entryRuleDate10315 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDate10325 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_111_in_ruleDate10363 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_112_in_ruleDate10381 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ruleDate_Literal_in_ruleDate10403 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDate_Literal_in_entryRuleDate_Literal10440 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDate_Literal10451 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleYear_in_ruleDate_Literal10498 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002000L});
    public static final BitSet FOLLOW_77_in_ruleDate_Literal10516 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ruleMonth_in_ruleDate_Literal10538 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002000L});
    public static final BitSet FOLLOW_77_in_ruleDate_Literal10556 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ruleDay_in_ruleDate_Literal10578 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleYear_in_entryRuleYear10624 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleYear10635 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleYear10674 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMonth_in_entryRuleMonth10719 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMonth10730 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleMonth10769 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDay_in_entryRuleDay10814 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDay10825 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleDay10864 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDate_And_Time_in_entryRuleDate_And_Time10908 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDate_And_Time10918 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_113_in_ruleDate_And_Time10956 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_114_in_ruleDate_And_Time10974 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ruleDate_And_Time_Literal_in_ruleDate_And_Time10996 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDate_And_Time_Literal_in_entryRuleDate_And_Time_Literal11033 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDate_And_Time_Literal11044 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDate_Literal_in_ruleDate_And_Time_Literal11091 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002000L});
    public static final BitSet FOLLOW_77_in_ruleDate_And_Time_Literal11109 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ruleDaytime_in_ruleDate_And_Time_Literal11131 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNon_Generic_Type_Name_in_entryRuleNon_Generic_Type_Name11182 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNon_Generic_Type_Name11192 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleElementary_Type_Name_in_ruleNon_Generic_Type_Name11242 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleElementary_Type_Name_in_entryRuleElementary_Type_Name11286 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleElementary_Type_Name11296 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumeric_Type_Name_in_ruleElementary_Type_Name11347 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDate_Type_Name_in_ruleElementary_Type_Name11374 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBit_String_Type_Name_in_ruleElementary_Type_Name11401 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTime_Type_Name_in_ruleElementary_Type_Name11428 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumeric_Type_Name_in_entryRuleNumeric_Type_Name11473 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNumeric_Type_Name11483 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInteger_Type_Name_in_ruleNumeric_Type_Name11534 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReal_Type_Name_in_ruleNumeric_Type_Name11561 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInteger_Type_Name_in_entryRuleInteger_Type_Name11606 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInteger_Type_Name11616 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSigned_Integer_Type_Name_in_ruleInteger_Type_Name11667 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUnsigned_Integer_Type_Name_in_ruleInteger_Type_Name11694 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSigned_Integer_Type_Name_in_entryRuleSigned_Integer_Type_Name11739 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSigned_Integer_Type_Name11749 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_115_in_ruleSigned_Integer_Type_Name11800 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_116_in_ruleSigned_Integer_Type_Name11818 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_117_in_ruleSigned_Integer_Type_Name11836 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_118_in_ruleSigned_Integer_Type_Name11854 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUnsigned_Integer_Type_Name_in_entryRuleUnsigned_Integer_Type_Name11901 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleUnsigned_Integer_Type_Name11911 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_119_in_ruleUnsigned_Integer_Type_Name11962 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_120_in_ruleUnsigned_Integer_Type_Name11980 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_121_in_ruleUnsigned_Integer_Type_Name11998 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_122_in_ruleUnsigned_Integer_Type_Name12016 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReal_Type_Name_in_entryRuleReal_Type_Name12063 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleReal_Type_Name12073 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_123_in_ruleReal_Type_Name12124 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_124_in_ruleReal_Type_Name12142 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBit_String_Type_Name_in_entryRuleBit_String_Type_Name12189 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBit_String_Type_Name12199 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_125_in_ruleBit_String_Type_Name12250 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_126_in_ruleBit_String_Type_Name12268 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_127_in_ruleBit_String_Type_Name12286 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_128_in_ruleBit_String_Type_Name12304 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_129_in_ruleBit_String_Type_Name12322 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTime_Type_Name_in_entryRuleTime_Type_Name12369 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTime_Type_Name12379 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_130_in_ruleTime_Type_Name12430 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_131_in_ruleTime_Type_Name12448 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDate_Type_Name_in_entryRuleDate_Type_Name12495 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDate_Type_Name12505 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_132_in_ruleDate_Type_Name12556 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_133_in_ruleDate_Type_Name12574 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_134_in_ruleDate_Type_Name12592 = new BitSet(new long[]{0x0000000000000002L});

}