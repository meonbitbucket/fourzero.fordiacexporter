/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Integer Type Name</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getInteger_Type_Name()
 * @model
 * @generated
 */
public interface Integer_Type_Name extends Numeric_Type_Name
{
} // Integer_Type_Name
