/**
 */
package org.fordiac.ide.model.structuredtext.structuredText.impl;

import org.eclipse.emf.ecore.EClass;

import org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage;
import org.fordiac.ide.model.structuredtext.structuredText.Time_Of_Day;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Time Of Day</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class Time_Of_DayImpl extends Time_LiteralImpl implements Time_Of_Day
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected Time_Of_DayImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return StructuredTextPackage.Literals.TIME_OF_DAY;
  }

} //Time_Of_DayImpl
