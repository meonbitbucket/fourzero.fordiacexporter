/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>For Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.For_Statement#getControlVariable <em>Control Variable</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.For_Statement#getExpressionFrom <em>Expression From</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.For_Statement#getExpressionTo <em>Expression To</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.For_Statement#getExpressionBy <em>Expression By</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.For_Statement#getStatements <em>Statements</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getFor_Statement()
 * @model
 * @generated
 */
public interface For_Statement extends Statement
{
  /**
   * Returns the value of the '<em><b>Control Variable</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Control Variable</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Control Variable</em>' reference.
   * @see #setControlVariable(VarDeclaration)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getFor_Statement_ControlVariable()
   * @model
   * @generated
   */
  VarDeclaration getControlVariable();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.For_Statement#getControlVariable <em>Control Variable</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Control Variable</em>' reference.
   * @see #getControlVariable()
   * @generated
   */
  void setControlVariable(VarDeclaration value);

  /**
   * Returns the value of the '<em><b>Expression From</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expression From</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expression From</em>' containment reference.
   * @see #setExpressionFrom(Expression)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getFor_Statement_ExpressionFrom()
   * @model containment="true"
   * @generated
   */
  Expression getExpressionFrom();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.For_Statement#getExpressionFrom <em>Expression From</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Expression From</em>' containment reference.
   * @see #getExpressionFrom()
   * @generated
   */
  void setExpressionFrom(Expression value);

  /**
   * Returns the value of the '<em><b>Expression To</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expression To</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expression To</em>' containment reference.
   * @see #setExpressionTo(Expression)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getFor_Statement_ExpressionTo()
   * @model containment="true"
   * @generated
   */
  Expression getExpressionTo();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.For_Statement#getExpressionTo <em>Expression To</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Expression To</em>' containment reference.
   * @see #getExpressionTo()
   * @generated
   */
  void setExpressionTo(Expression value);

  /**
   * Returns the value of the '<em><b>Expression By</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expression By</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expression By</em>' containment reference.
   * @see #setExpressionBy(Expression)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getFor_Statement_ExpressionBy()
   * @model containment="true"
   * @generated
   */
  Expression getExpressionBy();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.For_Statement#getExpressionBy <em>Expression By</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Expression By</em>' containment reference.
   * @see #getExpressionBy()
   * @generated
   */
  void setExpressionBy(Expression value);

  /**
   * Returns the value of the '<em><b>Statements</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Statements</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Statements</em>' containment reference.
   * @see #setStatements(StatementList)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getFor_Statement_Statements()
   * @model containment="true"
   * @generated
   */
  StatementList getStatements();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.For_Statement#getStatements <em>Statements</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Statements</em>' containment reference.
   * @see #getStatements()
   * @generated
   */
  void setStatements(StatementList value);

} // For_Statement
