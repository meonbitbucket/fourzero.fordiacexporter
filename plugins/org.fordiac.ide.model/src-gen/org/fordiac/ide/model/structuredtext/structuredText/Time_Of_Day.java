/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Time Of Day</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getTime_Of_Day()
 * @model
 * @generated
 */
public interface Time_Of_Day extends Time_Literal
{
} // Time_Of_Day
