/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Iteration Control Stmt</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.IterationControl_Stmt#getStatement <em>Statement</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getIterationControl_Stmt()
 * @model
 * @generated
 */
public interface IterationControl_Stmt extends Statement
{
  /**
   * Returns the value of the '<em><b>Statement</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Statement</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Statement</em>' attribute.
   * @see #setStatement(String)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getIterationControl_Stmt_Statement()
   * @model
   * @generated
   */
  String getStatement();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.IterationControl_Stmt#getStatement <em>Statement</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Statement</em>' attribute.
   * @see #getStatement()
   * @generated
   */
  void setStatement(String value);

} // IterationControl_Stmt
