/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bit String Type Name</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getBit_String_Type_Name()
 * @model
 * @generated
 */
public interface Bit_String_Type_Name extends Elementary_Type_Name
{
} // Bit_String_Type_Name
