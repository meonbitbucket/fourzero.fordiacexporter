/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage
 * @generated
 */
public interface StructuredTextFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  StructuredTextFactory eINSTANCE = org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextFactoryImpl.init();

  /**
   * Returns a new object of class '<em>structured Text Algorithm</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>structured Text Algorithm</em>'.
   * @generated
   */
  structuredTextAlgorithm createstructuredTextAlgorithm();

  /**
   * Returns a new object of class '<em>Var Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Var Declaration</em>'.
   * @generated
   */
  VarDeclaration createVarDeclaration();

  /**
   * Returns a new object of class '<em>Adapter Definition</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Adapter Definition</em>'.
   * @generated
   */
  AdapterDefinition createAdapterDefinition();

  /**
   * Returns a new object of class '<em>Adapter Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Adapter Declaration</em>'.
   * @generated
   */
  AdapterDeclaration createAdapterDeclaration();

  /**
   * Returns a new object of class '<em>Parameter</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Parameter</em>'.
   * @generated
   */
  Parameter createParameter();

  /**
   * Returns a new object of class '<em>Simple Spec Init</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Simple Spec Init</em>'.
   * @generated
   */
  Simple_Spec_Init createSimple_Spec_Init();

  /**
   * Returns a new object of class '<em>Array Spec Init</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Array Spec Init</em>'.
   * @generated
   */
  Array_Spec_Init createArray_Spec_Init();

  /**
   * Returns a new object of class '<em>Array Specification</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Array Specification</em>'.
   * @generated
   */
  Array_Specification createArray_Specification();

  /**
   * Returns a new object of class '<em>Subrange</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Subrange</em>'.
   * @generated
   */
  Subrange createSubrange();

  /**
   * Returns a new object of class '<em>Array Initialization</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Array Initialization</em>'.
   * @generated
   */
  Array_Initialization createArray_Initialization();

  /**
   * Returns a new object of class '<em>Array Initial Elements</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Array Initial Elements</em>'.
   * @generated
   */
  Array_Initial_Elements createArray_Initial_Elements();

  /**
   * Returns a new object of class '<em>Structure Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Structure Spec</em>'.
   * @generated
   */
  Structure_Spec createStructure_Spec();

  /**
   * Returns a new object of class '<em>SByte String Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>SByte String Spec</em>'.
   * @generated
   */
  S_Byte_String_Spec createS_Byte_String_Spec();

  /**
   * Returns a new object of class '<em>DByte String Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>DByte String Spec</em>'.
   * @generated
   */
  D_Byte_String_Spec createD_Byte_String_Spec();

  /**
   * Returns a new object of class '<em>Statement List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Statement List</em>'.
   * @generated
   */
  StatementList createStatementList();

  /**
   * Returns a new object of class '<em>Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Statement</em>'.
   * @generated
   */
  Statement createStatement();

  /**
   * Returns a new object of class '<em>Assignment Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Assignment Statement</em>'.
   * @generated
   */
  Assignment_Statement createAssignment_Statement();

  /**
   * Returns a new object of class '<em>Variable</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Variable</em>'.
   * @generated
   */
  Variable createVariable();

  /**
   * Returns a new object of class '<em>Var Variable</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Var Variable</em>'.
   * @generated
   */
  Var_Variable createVar_Variable();

  /**
   * Returns a new object of class '<em>Adapter Variable</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Adapter Variable</em>'.
   * @generated
   */
  Adapter_Variable createAdapter_Variable();

  /**
   * Returns a new object of class '<em>Array Variable</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Array Variable</em>'.
   * @generated
   */
  Array_Variable createArray_Variable();

  /**
   * Returns a new object of class '<em>Selection Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Selection Statement</em>'.
   * @generated
   */
  Selection_Statement createSelection_Statement();

  /**
   * Returns a new object of class '<em>If Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>If Statement</em>'.
   * @generated
   */
  If_Statement createIf_Statement();

  /**
   * Returns a new object of class '<em>Else If Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Else If Statement</em>'.
   * @generated
   */
  ElseIf_Statement createElseIf_Statement();

  /**
   * Returns a new object of class '<em>Case Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Case Statement</em>'.
   * @generated
   */
  Case_Statement createCase_Statement();

  /**
   * Returns a new object of class '<em>Case Element</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Case Element</em>'.
   * @generated
   */
  Case_Element createCase_Element();

  /**
   * Returns a new object of class '<em>Case Else</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Case Else</em>'.
   * @generated
   */
  Case_Else createCase_Else();

  /**
   * Returns a new object of class '<em>Iteration Control Stmt</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Iteration Control Stmt</em>'.
   * @generated
   */
  IterationControl_Stmt createIterationControl_Stmt();

  /**
   * Returns a new object of class '<em>For Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>For Statement</em>'.
   * @generated
   */
  For_Statement createFor_Statement();

  /**
   * Returns a new object of class '<em>Repeat Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Repeat Statement</em>'.
   * @generated
   */
  Repeat_Statement createRepeat_Statement();

  /**
   * Returns a new object of class '<em>While Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>While Statement</em>'.
   * @generated
   */
  While_Statement createWhile_Statement();

  /**
   * Returns a new object of class '<em>Subprog Ctrl Stmt</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Subprog Ctrl Stmt</em>'.
   * @generated
   */
  Subprog_Ctrl_Stmt createSubprog_Ctrl_Stmt();

  /**
   * Returns a new object of class '<em>Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Expression</em>'.
   * @generated
   */
  Expression createExpression();

  /**
   * Returns a new object of class '<em>Func Call</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Func Call</em>'.
   * @generated
   */
  Func_Call createFunc_Call();

  /**
   * Returns a new object of class '<em>Param Assign</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Param Assign</em>'.
   * @generated
   */
  Param_Assign createParam_Assign();

  /**
   * Returns a new object of class '<em>Constant</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Constant</em>'.
   * @generated
   */
  Constant createConstant();

  /**
   * Returns a new object of class '<em>Numeric Literal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Numeric Literal</em>'.
   * @generated
   */
  Numeric_Literal createNumeric_Literal();

  /**
   * Returns a new object of class '<em>Integer Literal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Integer Literal</em>'.
   * @generated
   */
  IntegerLiteral createIntegerLiteral();

  /**
   * Returns a new object of class '<em>Real Literal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Real Literal</em>'.
   * @generated
   */
  Real_Literal createReal_Literal();

  /**
   * Returns a new object of class '<em>Boolean Literal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Boolean Literal</em>'.
   * @generated
   */
  Boolean_Literal createBoolean_Literal();

  /**
   * Returns a new object of class '<em>Character String</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Character String</em>'.
   * @generated
   */
  Character_String createCharacter_String();

  /**
   * Returns a new object of class '<em>Single Byte Character String Literal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Single Byte Character String Literal</em>'.
   * @generated
   */
  Single_Byte_Character_String_Literal createSingle_Byte_Character_String_Literal();

  /**
   * Returns a new object of class '<em>Double Byte Character String Literal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Double Byte Character String Literal</em>'.
   * @generated
   */
  Double_Byte_Character_String_Literal createDouble_Byte_Character_String_Literal();

  /**
   * Returns a new object of class '<em>Time Literal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Time Literal</em>'.
   * @generated
   */
  Time_Literal createTime_Literal();

  /**
   * Returns a new object of class '<em>Duration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Duration</em>'.
   * @generated
   */
  Duration createDuration();

  /**
   * Returns a new object of class '<em>Time Of Day</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Time Of Day</em>'.
   * @generated
   */
  Time_Of_Day createTime_Of_Day();

  /**
   * Returns a new object of class '<em>Date</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Date</em>'.
   * @generated
   */
  Date createDate();

  /**
   * Returns a new object of class '<em>Date And Time</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Date And Time</em>'.
   * @generated
   */
  Date_And_Time createDate_And_Time();

  /**
   * Returns a new object of class '<em>Non Generic Type Name</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Non Generic Type Name</em>'.
   * @generated
   */
  Non_Generic_Type_Name createNon_Generic_Type_Name();

  /**
   * Returns a new object of class '<em>Elementary Type Name</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Elementary Type Name</em>'.
   * @generated
   */
  Elementary_Type_Name createElementary_Type_Name();

  /**
   * Returns a new object of class '<em>Numeric Type Name</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Numeric Type Name</em>'.
   * @generated
   */
  Numeric_Type_Name createNumeric_Type_Name();

  /**
   * Returns a new object of class '<em>Integer Type Name</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Integer Type Name</em>'.
   * @generated
   */
  Integer_Type_Name createInteger_Type_Name();

  /**
   * Returns a new object of class '<em>Signed Integer Type Name</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Signed Integer Type Name</em>'.
   * @generated
   */
  Signed_Integer_Type_Name createSigned_Integer_Type_Name();

  /**
   * Returns a new object of class '<em>Unsigned Integer Type Name</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Unsigned Integer Type Name</em>'.
   * @generated
   */
  Unsigned_Integer_Type_Name createUnsigned_Integer_Type_Name();

  /**
   * Returns a new object of class '<em>Real Type Name</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Real Type Name</em>'.
   * @generated
   */
  Real_Type_Name createReal_Type_Name();

  /**
   * Returns a new object of class '<em>Bit String Type Name</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Bit String Type Name</em>'.
   * @generated
   */
  Bit_String_Type_Name createBit_String_Type_Name();

  /**
   * Returns a new object of class '<em>Time Type Name</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Time Type Name</em>'.
   * @generated
   */
  Time_Type_Name createTime_Type_Name();

  /**
   * Returns a new object of class '<em>Date Type Name</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Date Type Name</em>'.
   * @generated
   */
  Date_Type_Name createDate_Type_Name();

  /**
   * Returns a new object of class '<em>Xor Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Xor Expression</em>'.
   * @generated
   */
  Xor_Expression createXor_Expression();

  /**
   * Returns a new object of class '<em>And Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>And Expression</em>'.
   * @generated
   */
  And_Expression createAnd_Expression();

  /**
   * Returns a new object of class '<em>Comparison</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Comparison</em>'.
   * @generated
   */
  Comparison createComparison();

  /**
   * Returns a new object of class '<em>Equ Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Equ Expression</em>'.
   * @generated
   */
  Equ_Expression createEqu_Expression();

  /**
   * Returns a new object of class '<em>Add Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Add Expression</em>'.
   * @generated
   */
  Add_Expression createAdd_Expression();

  /**
   * Returns a new object of class '<em>Term</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Term</em>'.
   * @generated
   */
  Term createTerm();

  /**
   * Returns a new object of class '<em>Power Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Power Expression</em>'.
   * @generated
   */
  Power_Expression createPower_Expression();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  StructuredTextPackage getStructuredTextPackage();

} //StructuredTextFactory
