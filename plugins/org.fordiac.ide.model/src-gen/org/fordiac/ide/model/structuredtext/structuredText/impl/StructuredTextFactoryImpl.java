/**
 */
package org.fordiac.ide.model.structuredtext.structuredText.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.fordiac.ide.model.structuredtext.structuredText.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class StructuredTextFactoryImpl extends EFactoryImpl implements StructuredTextFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static StructuredTextFactory init()
  {
    try
    {
      StructuredTextFactory theStructuredTextFactory = (StructuredTextFactory)EPackage.Registry.INSTANCE.getEFactory(StructuredTextPackage.eNS_URI);
      if (theStructuredTextFactory != null)
      {
        return theStructuredTextFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new StructuredTextFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StructuredTextFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case StructuredTextPackage.STRUCTURED_TEXT_ALGORITHM: return createstructuredTextAlgorithm();
      case StructuredTextPackage.VAR_DECLARATION: return createVarDeclaration();
      case StructuredTextPackage.ADAPTER_DEFINITION: return createAdapterDefinition();
      case StructuredTextPackage.ADAPTER_DECLARATION: return createAdapterDeclaration();
      case StructuredTextPackage.PARAMETER: return createParameter();
      case StructuredTextPackage.SIMPLE_SPEC_INIT: return createSimple_Spec_Init();
      case StructuredTextPackage.ARRAY_SPEC_INIT: return createArray_Spec_Init();
      case StructuredTextPackage.ARRAY_SPECIFICATION: return createArray_Specification();
      case StructuredTextPackage.SUBRANGE: return createSubrange();
      case StructuredTextPackage.ARRAY_INITIALIZATION: return createArray_Initialization();
      case StructuredTextPackage.ARRAY_INITIAL_ELEMENTS: return createArray_Initial_Elements();
      case StructuredTextPackage.STRUCTURE_SPEC: return createStructure_Spec();
      case StructuredTextPackage.SBYTE_STRING_SPEC: return createS_Byte_String_Spec();
      case StructuredTextPackage.DBYTE_STRING_SPEC: return createD_Byte_String_Spec();
      case StructuredTextPackage.STATEMENT_LIST: return createStatementList();
      case StructuredTextPackage.STATEMENT: return createStatement();
      case StructuredTextPackage.ASSIGNMENT_STATEMENT: return createAssignment_Statement();
      case StructuredTextPackage.VARIABLE: return createVariable();
      case StructuredTextPackage.VAR_VARIABLE: return createVar_Variable();
      case StructuredTextPackage.ADAPTER_VARIABLE: return createAdapter_Variable();
      case StructuredTextPackage.ARRAY_VARIABLE: return createArray_Variable();
      case StructuredTextPackage.SELECTION_STATEMENT: return createSelection_Statement();
      case StructuredTextPackage.IF_STATEMENT: return createIf_Statement();
      case StructuredTextPackage.ELSE_IF_STATEMENT: return createElseIf_Statement();
      case StructuredTextPackage.CASE_STATEMENT: return createCase_Statement();
      case StructuredTextPackage.CASE_ELEMENT: return createCase_Element();
      case StructuredTextPackage.CASE_ELSE: return createCase_Else();
      case StructuredTextPackage.ITERATION_CONTROL_STMT: return createIterationControl_Stmt();
      case StructuredTextPackage.FOR_STATEMENT: return createFor_Statement();
      case StructuredTextPackage.REPEAT_STATEMENT: return createRepeat_Statement();
      case StructuredTextPackage.WHILE_STATEMENT: return createWhile_Statement();
      case StructuredTextPackage.SUBPROG_CTRL_STMT: return createSubprog_Ctrl_Stmt();
      case StructuredTextPackage.EXPRESSION: return createExpression();
      case StructuredTextPackage.FUNC_CALL: return createFunc_Call();
      case StructuredTextPackage.PARAM_ASSIGN: return createParam_Assign();
      case StructuredTextPackage.CONSTANT: return createConstant();
      case StructuredTextPackage.NUMERIC_LITERAL: return createNumeric_Literal();
      case StructuredTextPackage.INTEGER_LITERAL: return createIntegerLiteral();
      case StructuredTextPackage.REAL_LITERAL: return createReal_Literal();
      case StructuredTextPackage.BOOLEAN_LITERAL: return createBoolean_Literal();
      case StructuredTextPackage.CHARACTER_STRING: return createCharacter_String();
      case StructuredTextPackage.SINGLE_BYTE_CHARACTER_STRING_LITERAL: return createSingle_Byte_Character_String_Literal();
      case StructuredTextPackage.DOUBLE_BYTE_CHARACTER_STRING_LITERAL: return createDouble_Byte_Character_String_Literal();
      case StructuredTextPackage.TIME_LITERAL: return createTime_Literal();
      case StructuredTextPackage.DURATION: return createDuration();
      case StructuredTextPackage.TIME_OF_DAY: return createTime_Of_Day();
      case StructuredTextPackage.DATE: return createDate();
      case StructuredTextPackage.DATE_AND_TIME: return createDate_And_Time();
      case StructuredTextPackage.NON_GENERIC_TYPE_NAME: return createNon_Generic_Type_Name();
      case StructuredTextPackage.ELEMENTARY_TYPE_NAME: return createElementary_Type_Name();
      case StructuredTextPackage.NUMERIC_TYPE_NAME: return createNumeric_Type_Name();
      case StructuredTextPackage.INTEGER_TYPE_NAME: return createInteger_Type_Name();
      case StructuredTextPackage.SIGNED_INTEGER_TYPE_NAME: return createSigned_Integer_Type_Name();
      case StructuredTextPackage.UNSIGNED_INTEGER_TYPE_NAME: return createUnsigned_Integer_Type_Name();
      case StructuredTextPackage.REAL_TYPE_NAME: return createReal_Type_Name();
      case StructuredTextPackage.BIT_STRING_TYPE_NAME: return createBit_String_Type_Name();
      case StructuredTextPackage.TIME_TYPE_NAME: return createTime_Type_Name();
      case StructuredTextPackage.DATE_TYPE_NAME: return createDate_Type_Name();
      case StructuredTextPackage.XOR_EXPRESSION: return createXor_Expression();
      case StructuredTextPackage.AND_EXPRESSION: return createAnd_Expression();
      case StructuredTextPackage.COMPARISON: return createComparison();
      case StructuredTextPackage.EQU_EXPRESSION: return createEqu_Expression();
      case StructuredTextPackage.ADD_EXPRESSION: return createAdd_Expression();
      case StructuredTextPackage.TERM: return createTerm();
      case StructuredTextPackage.POWER_EXPRESSION: return createPower_Expression();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public structuredTextAlgorithm createstructuredTextAlgorithm()
  {
    structuredTextAlgorithmImpl structuredTextAlgorithm = new structuredTextAlgorithmImpl();
    return structuredTextAlgorithm;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VarDeclaration createVarDeclaration()
  {
    VarDeclarationImpl varDeclaration = new VarDeclarationImpl();
    return varDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AdapterDefinition createAdapterDefinition()
  {
    AdapterDefinitionImpl adapterDefinition = new AdapterDefinitionImpl();
    return adapterDefinition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AdapterDeclaration createAdapterDeclaration()
  {
    AdapterDeclarationImpl adapterDeclaration = new AdapterDeclarationImpl();
    return adapterDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Parameter createParameter()
  {
    ParameterImpl parameter = new ParameterImpl();
    return parameter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Simple_Spec_Init createSimple_Spec_Init()
  {
    Simple_Spec_InitImpl simple_Spec_Init = new Simple_Spec_InitImpl();
    return simple_Spec_Init;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Array_Spec_Init createArray_Spec_Init()
  {
    Array_Spec_InitImpl array_Spec_Init = new Array_Spec_InitImpl();
    return array_Spec_Init;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Array_Specification createArray_Specification()
  {
    Array_SpecificationImpl array_Specification = new Array_SpecificationImpl();
    return array_Specification;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Subrange createSubrange()
  {
    SubrangeImpl subrange = new SubrangeImpl();
    return subrange;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Array_Initialization createArray_Initialization()
  {
    Array_InitializationImpl array_Initialization = new Array_InitializationImpl();
    return array_Initialization;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Array_Initial_Elements createArray_Initial_Elements()
  {
    Array_Initial_ElementsImpl array_Initial_Elements = new Array_Initial_ElementsImpl();
    return array_Initial_Elements;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Structure_Spec createStructure_Spec()
  {
    Structure_SpecImpl structure_Spec = new Structure_SpecImpl();
    return structure_Spec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public S_Byte_String_Spec createS_Byte_String_Spec()
  {
    S_Byte_String_SpecImpl s_Byte_String_Spec = new S_Byte_String_SpecImpl();
    return s_Byte_String_Spec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public D_Byte_String_Spec createD_Byte_String_Spec()
  {
    D_Byte_String_SpecImpl d_Byte_String_Spec = new D_Byte_String_SpecImpl();
    return d_Byte_String_Spec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StatementList createStatementList()
  {
    StatementListImpl statementList = new StatementListImpl();
    return statementList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Statement createStatement()
  {
    StatementImpl statement = new StatementImpl();
    return statement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Assignment_Statement createAssignment_Statement()
  {
    Assignment_StatementImpl assignment_Statement = new Assignment_StatementImpl();
    return assignment_Statement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Variable createVariable()
  {
    VariableImpl variable = new VariableImpl();
    return variable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Var_Variable createVar_Variable()
  {
    Var_VariableImpl var_Variable = new Var_VariableImpl();
    return var_Variable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Adapter_Variable createAdapter_Variable()
  {
    Adapter_VariableImpl adapter_Variable = new Adapter_VariableImpl();
    return adapter_Variable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Array_Variable createArray_Variable()
  {
    Array_VariableImpl array_Variable = new Array_VariableImpl();
    return array_Variable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Selection_Statement createSelection_Statement()
  {
    Selection_StatementImpl selection_Statement = new Selection_StatementImpl();
    return selection_Statement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public If_Statement createIf_Statement()
  {
    If_StatementImpl if_Statement = new If_StatementImpl();
    return if_Statement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ElseIf_Statement createElseIf_Statement()
  {
    ElseIf_StatementImpl elseIf_Statement = new ElseIf_StatementImpl();
    return elseIf_Statement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Case_Statement createCase_Statement()
  {
    Case_StatementImpl case_Statement = new Case_StatementImpl();
    return case_Statement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Case_Element createCase_Element()
  {
    Case_ElementImpl case_Element = new Case_ElementImpl();
    return case_Element;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Case_Else createCase_Else()
  {
    Case_ElseImpl case_Else = new Case_ElseImpl();
    return case_Else;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IterationControl_Stmt createIterationControl_Stmt()
  {
    IterationControl_StmtImpl iterationControl_Stmt = new IterationControl_StmtImpl();
    return iterationControl_Stmt;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public For_Statement createFor_Statement()
  {
    For_StatementImpl for_Statement = new For_StatementImpl();
    return for_Statement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Repeat_Statement createRepeat_Statement()
  {
    Repeat_StatementImpl repeat_Statement = new Repeat_StatementImpl();
    return repeat_Statement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public While_Statement createWhile_Statement()
  {
    While_StatementImpl while_Statement = new While_StatementImpl();
    return while_Statement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Subprog_Ctrl_Stmt createSubprog_Ctrl_Stmt()
  {
    Subprog_Ctrl_StmtImpl subprog_Ctrl_Stmt = new Subprog_Ctrl_StmtImpl();
    return subprog_Ctrl_Stmt;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Expression createExpression()
  {
    ExpressionImpl expression = new ExpressionImpl();
    return expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Func_Call createFunc_Call()
  {
    Func_CallImpl func_Call = new Func_CallImpl();
    return func_Call;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Param_Assign createParam_Assign()
  {
    Param_AssignImpl param_Assign = new Param_AssignImpl();
    return param_Assign;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Constant createConstant()
  {
    ConstantImpl constant = new ConstantImpl();
    return constant;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Numeric_Literal createNumeric_Literal()
  {
    Numeric_LiteralImpl numeric_Literal = new Numeric_LiteralImpl();
    return numeric_Literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntegerLiteral createIntegerLiteral()
  {
    IntegerLiteralImpl integerLiteral = new IntegerLiteralImpl();
    return integerLiteral;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Real_Literal createReal_Literal()
  {
    Real_LiteralImpl real_Literal = new Real_LiteralImpl();
    return real_Literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Boolean_Literal createBoolean_Literal()
  {
    Boolean_LiteralImpl boolean_Literal = new Boolean_LiteralImpl();
    return boolean_Literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Character_String createCharacter_String()
  {
    Character_StringImpl character_String = new Character_StringImpl();
    return character_String;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Single_Byte_Character_String_Literal createSingle_Byte_Character_String_Literal()
  {
    Single_Byte_Character_String_LiteralImpl single_Byte_Character_String_Literal = new Single_Byte_Character_String_LiteralImpl();
    return single_Byte_Character_String_Literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Double_Byte_Character_String_Literal createDouble_Byte_Character_String_Literal()
  {
    Double_Byte_Character_String_LiteralImpl double_Byte_Character_String_Literal = new Double_Byte_Character_String_LiteralImpl();
    return double_Byte_Character_String_Literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Time_Literal createTime_Literal()
  {
    Time_LiteralImpl time_Literal = new Time_LiteralImpl();
    return time_Literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Duration createDuration()
  {
    DurationImpl duration = new DurationImpl();
    return duration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Time_Of_Day createTime_Of_Day()
  {
    Time_Of_DayImpl time_Of_Day = new Time_Of_DayImpl();
    return time_Of_Day;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Date createDate()
  {
    DateImpl date = new DateImpl();
    return date;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Date_And_Time createDate_And_Time()
  {
    Date_And_TimeImpl date_And_Time = new Date_And_TimeImpl();
    return date_And_Time;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Non_Generic_Type_Name createNon_Generic_Type_Name()
  {
    Non_Generic_Type_NameImpl non_Generic_Type_Name = new Non_Generic_Type_NameImpl();
    return non_Generic_Type_Name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Elementary_Type_Name createElementary_Type_Name()
  {
    Elementary_Type_NameImpl elementary_Type_Name = new Elementary_Type_NameImpl();
    return elementary_Type_Name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Numeric_Type_Name createNumeric_Type_Name()
  {
    Numeric_Type_NameImpl numeric_Type_Name = new Numeric_Type_NameImpl();
    return numeric_Type_Name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Integer_Type_Name createInteger_Type_Name()
  {
    Integer_Type_NameImpl integer_Type_Name = new Integer_Type_NameImpl();
    return integer_Type_Name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Signed_Integer_Type_Name createSigned_Integer_Type_Name()
  {
    Signed_Integer_Type_NameImpl signed_Integer_Type_Name = new Signed_Integer_Type_NameImpl();
    return signed_Integer_Type_Name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Unsigned_Integer_Type_Name createUnsigned_Integer_Type_Name()
  {
    Unsigned_Integer_Type_NameImpl unsigned_Integer_Type_Name = new Unsigned_Integer_Type_NameImpl();
    return unsigned_Integer_Type_Name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Real_Type_Name createReal_Type_Name()
  {
    Real_Type_NameImpl real_Type_Name = new Real_Type_NameImpl();
    return real_Type_Name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Bit_String_Type_Name createBit_String_Type_Name()
  {
    Bit_String_Type_NameImpl bit_String_Type_Name = new Bit_String_Type_NameImpl();
    return bit_String_Type_Name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Time_Type_Name createTime_Type_Name()
  {
    Time_Type_NameImpl time_Type_Name = new Time_Type_NameImpl();
    return time_Type_Name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Date_Type_Name createDate_Type_Name()
  {
    Date_Type_NameImpl date_Type_Name = new Date_Type_NameImpl();
    return date_Type_Name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Xor_Expression createXor_Expression()
  {
    Xor_ExpressionImpl xor_Expression = new Xor_ExpressionImpl();
    return xor_Expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public And_Expression createAnd_Expression()
  {
    And_ExpressionImpl and_Expression = new And_ExpressionImpl();
    return and_Expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Comparison createComparison()
  {
    ComparisonImpl comparison = new ComparisonImpl();
    return comparison;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Equ_Expression createEqu_Expression()
  {
    Equ_ExpressionImpl equ_Expression = new Equ_ExpressionImpl();
    return equ_Expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Add_Expression createAdd_Expression()
  {
    Add_ExpressionImpl add_Expression = new Add_ExpressionImpl();
    return add_Expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Term createTerm()
  {
    TermImpl term = new TermImpl();
    return term;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Power_Expression createPower_Expression()
  {
    Power_ExpressionImpl power_Expression = new Power_ExpressionImpl();
    return power_Expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StructuredTextPackage getStructuredTextPackage()
  {
    return (StructuredTextPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static StructuredTextPackage getPackage()
  {
    return StructuredTextPackage.eINSTANCE;
  }

} //StructuredTextFactoryImpl
