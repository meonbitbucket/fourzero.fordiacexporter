/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Double Byte Character String Literal</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getDouble_Byte_Character_String_Literal()
 * @model
 * @generated
 */
public interface Double_Byte_Character_String_Literal extends Character_String
{
} // Double_Byte_Character_String_Literal
