/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Structure Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.Structure_Spec#getName <em>Name</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.Structure_Spec#getElement <em>Element</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getStructure_Spec()
 * @model
 * @generated
 */
public interface Structure_Spec extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getStructure_Spec_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.Structure_Spec#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Element</b></em>' containment reference list.
   * The list contents are of type {@link org.fordiac.ide.model.structuredtext.structuredText.VarDeclaration}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Element</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Element</em>' containment reference list.
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getStructure_Spec_Element()
   * @model containment="true"
   * @generated
   */
  EList<VarDeclaration> getElement();

} // Structure_Spec
