/**
 */
package org.fordiac.ide.model.structuredtext.structuredText.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.fordiac.ide.model.structuredtext.structuredText.Array_Initialization;
import org.fordiac.ide.model.structuredtext.structuredText.Array_Specification;
import org.fordiac.ide.model.structuredtext.structuredText.Non_Generic_Type_Name;
import org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage;
import org.fordiac.ide.model.structuredtext.structuredText.Subrange;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Array Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Array_SpecificationImpl#getInitialization <em>Initialization</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Array_SpecificationImpl#getSubrange <em>Subrange</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Array_SpecificationImpl#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Array_SpecificationImpl extends Array_Spec_InitImpl implements Array_Specification
{
  /**
   * The cached value of the '{@link #getInitialization() <em>Initialization</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInitialization()
   * @generated
   * @ordered
   */
  protected Array_Initialization initialization;

  /**
   * The cached value of the '{@link #getSubrange() <em>Subrange</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubrange()
   * @generated
   * @ordered
   */
  protected EList<Subrange> subrange;

  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected Non_Generic_Type_Name type;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected Array_SpecificationImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return StructuredTextPackage.Literals.ARRAY_SPECIFICATION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Array_Initialization getInitialization()
  {
    return initialization;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetInitialization(Array_Initialization newInitialization, NotificationChain msgs)
  {
    Array_Initialization oldInitialization = initialization;
    initialization = newInitialization;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StructuredTextPackage.ARRAY_SPECIFICATION__INITIALIZATION, oldInitialization, newInitialization);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setInitialization(Array_Initialization newInitialization)
  {
    if (newInitialization != initialization)
    {
      NotificationChain msgs = null;
      if (initialization != null)
        msgs = ((InternalEObject)initialization).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StructuredTextPackage.ARRAY_SPECIFICATION__INITIALIZATION, null, msgs);
      if (newInitialization != null)
        msgs = ((InternalEObject)newInitialization).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StructuredTextPackage.ARRAY_SPECIFICATION__INITIALIZATION, null, msgs);
      msgs = basicSetInitialization(newInitialization, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, StructuredTextPackage.ARRAY_SPECIFICATION__INITIALIZATION, newInitialization, newInitialization));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Subrange> getSubrange()
  {
    if (subrange == null)
    {
      subrange = new EObjectContainmentEList<Subrange>(Subrange.class, this, StructuredTextPackage.ARRAY_SPECIFICATION__SUBRANGE);
    }
    return subrange;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Non_Generic_Type_Name getType()
  {
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetType(Non_Generic_Type_Name newType, NotificationChain msgs)
  {
    Non_Generic_Type_Name oldType = type;
    type = newType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StructuredTextPackage.ARRAY_SPECIFICATION__TYPE, oldType, newType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setType(Non_Generic_Type_Name newType)
  {
    if (newType != type)
    {
      NotificationChain msgs = null;
      if (type != null)
        msgs = ((InternalEObject)type).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StructuredTextPackage.ARRAY_SPECIFICATION__TYPE, null, msgs);
      if (newType != null)
        msgs = ((InternalEObject)newType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StructuredTextPackage.ARRAY_SPECIFICATION__TYPE, null, msgs);
      msgs = basicSetType(newType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, StructuredTextPackage.ARRAY_SPECIFICATION__TYPE, newType, newType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case StructuredTextPackage.ARRAY_SPECIFICATION__INITIALIZATION:
        return basicSetInitialization(null, msgs);
      case StructuredTextPackage.ARRAY_SPECIFICATION__SUBRANGE:
        return ((InternalEList<?>)getSubrange()).basicRemove(otherEnd, msgs);
      case StructuredTextPackage.ARRAY_SPECIFICATION__TYPE:
        return basicSetType(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case StructuredTextPackage.ARRAY_SPECIFICATION__INITIALIZATION:
        return getInitialization();
      case StructuredTextPackage.ARRAY_SPECIFICATION__SUBRANGE:
        return getSubrange();
      case StructuredTextPackage.ARRAY_SPECIFICATION__TYPE:
        return getType();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case StructuredTextPackage.ARRAY_SPECIFICATION__INITIALIZATION:
        setInitialization((Array_Initialization)newValue);
        return;
      case StructuredTextPackage.ARRAY_SPECIFICATION__SUBRANGE:
        getSubrange().clear();
        getSubrange().addAll((Collection<? extends Subrange>)newValue);
        return;
      case StructuredTextPackage.ARRAY_SPECIFICATION__TYPE:
        setType((Non_Generic_Type_Name)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case StructuredTextPackage.ARRAY_SPECIFICATION__INITIALIZATION:
        setInitialization((Array_Initialization)null);
        return;
      case StructuredTextPackage.ARRAY_SPECIFICATION__SUBRANGE:
        getSubrange().clear();
        return;
      case StructuredTextPackage.ARRAY_SPECIFICATION__TYPE:
        setType((Non_Generic_Type_Name)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case StructuredTextPackage.ARRAY_SPECIFICATION__INITIALIZATION:
        return initialization != null;
      case StructuredTextPackage.ARRAY_SPECIFICATION__SUBRANGE:
        return subrange != null && !subrange.isEmpty();
      case StructuredTextPackage.ARRAY_SPECIFICATION__TYPE:
        return type != null;
    }
    return super.eIsSet(featureID);
  }

} //Array_SpecificationImpl
