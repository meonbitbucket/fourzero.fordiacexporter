/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Case Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.Case_Statement#getCaseElements <em>Case Elements</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.Case_Statement#getCaseElse <em>Case Else</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getCase_Statement()
 * @model
 * @generated
 */
public interface Case_Statement extends Selection_Statement
{
  /**
   * Returns the value of the '<em><b>Case Elements</b></em>' containment reference list.
   * The list contents are of type {@link org.fordiac.ide.model.structuredtext.structuredText.Case_Element}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Case Elements</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Case Elements</em>' containment reference list.
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getCase_Statement_CaseElements()
   * @model containment="true"
   * @generated
   */
  EList<Case_Element> getCaseElements();

  /**
   * Returns the value of the '<em><b>Case Else</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Case Else</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Case Else</em>' containment reference.
   * @see #setCaseElse(Case_Else)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getCase_Statement_CaseElse()
   * @model containment="true"
   * @generated
   */
  Case_Else getCaseElse();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.Case_Statement#getCaseElse <em>Case Else</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Case Else</em>' containment reference.
   * @see #getCaseElse()
   * @generated
   */
  void setCaseElse(Case_Else value);

} // Case_Statement
