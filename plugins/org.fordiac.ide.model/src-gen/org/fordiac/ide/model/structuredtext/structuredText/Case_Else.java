/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Case Else</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.Case_Else#getStatements <em>Statements</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getCase_Else()
 * @model
 * @generated
 */
public interface Case_Else extends EObject
{
  /**
   * Returns the value of the '<em><b>Statements</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Statements</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Statements</em>' containment reference.
   * @see #setStatements(StatementList)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getCase_Else_Statements()
   * @model containment="true"
   * @generated
   */
  StatementList getStatements();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.Case_Else#getStatements <em>Statements</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Statements</em>' containment reference.
   * @see #getStatements()
   * @generated
   */
  void setStatements(StatementList value);

} // Case_Else
