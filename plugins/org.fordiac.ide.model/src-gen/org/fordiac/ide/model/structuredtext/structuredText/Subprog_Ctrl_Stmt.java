/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Subprog Ctrl Stmt</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.Subprog_Ctrl_Stmt#getCall <em>Call</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getSubprog_Ctrl_Stmt()
 * @model
 * @generated
 */
public interface Subprog_Ctrl_Stmt extends Statement
{
  /**
   * Returns the value of the '<em><b>Call</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Call</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Call</em>' containment reference.
   * @see #setCall(Func_Call)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getSubprog_Ctrl_Stmt_Call()
   * @model containment="true"
   * @generated
   */
  Func_Call getCall();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.Subprog_Ctrl_Stmt#getCall <em>Call</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Call</em>' containment reference.
   * @see #getCall()
   * @generated
   */
  void setCall(Func_Call value);

} // Subprog_Ctrl_Stmt
