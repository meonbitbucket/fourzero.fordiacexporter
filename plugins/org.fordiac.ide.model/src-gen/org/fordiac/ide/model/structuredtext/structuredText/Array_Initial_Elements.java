/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Array Initial Elements</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Initial_Elements#getNum <em>Num</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Initial_Elements#getArrayElement <em>Array Element</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getArray_Initial_Elements()
 * @model
 * @generated
 */
public interface Array_Initial_Elements extends EObject
{
  /**
   * Returns the value of the '<em><b>Num</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Num</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Num</em>' attribute.
   * @see #setNum(int)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getArray_Initial_Elements_Num()
   * @model
   * @generated
   */
  int getNum();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Initial_Elements#getNum <em>Num</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Num</em>' attribute.
   * @see #getNum()
   * @generated
   */
  void setNum(int value);

  /**
   * Returns the value of the '<em><b>Array Element</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Array Element</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Array Element</em>' containment reference.
   * @see #setArrayElement(Constant)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getArray_Initial_Elements_ArrayElement()
   * @model containment="true"
   * @generated
   */
  Constant getArrayElement();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Initial_Elements#getArrayElement <em>Array Element</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Array Element</em>' containment reference.
   * @see #getArrayElement()
   * @generated
   */
  void setArrayElement(Constant value);

} // Array_Initial_Elements
