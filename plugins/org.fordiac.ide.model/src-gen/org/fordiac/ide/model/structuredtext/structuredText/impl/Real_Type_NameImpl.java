/**
 */
package org.fordiac.ide.model.structuredtext.structuredText.impl;

import org.eclipse.emf.ecore.EClass;

import org.fordiac.ide.model.structuredtext.structuredText.Real_Type_Name;
import org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Real Type Name</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class Real_Type_NameImpl extends Numeric_Type_NameImpl implements Real_Type_Name
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected Real_Type_NameImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return StructuredTextPackage.Literals.REAL_TYPE_NAME;
  }

} //Real_Type_NameImpl
