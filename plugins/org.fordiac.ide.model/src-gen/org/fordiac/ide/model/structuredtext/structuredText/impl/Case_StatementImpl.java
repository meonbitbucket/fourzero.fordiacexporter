/**
 */
package org.fordiac.ide.model.structuredtext.structuredText.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.fordiac.ide.model.structuredtext.structuredText.Case_Element;
import org.fordiac.ide.model.structuredtext.structuredText.Case_Else;
import org.fordiac.ide.model.structuredtext.structuredText.Case_Statement;
import org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Case Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Case_StatementImpl#getCaseElements <em>Case Elements</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Case_StatementImpl#getCaseElse <em>Case Else</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Case_StatementImpl extends Selection_StatementImpl implements Case_Statement
{
  /**
   * The cached value of the '{@link #getCaseElements() <em>Case Elements</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCaseElements()
   * @generated
   * @ordered
   */
  protected EList<Case_Element> caseElements;

  /**
   * The cached value of the '{@link #getCaseElse() <em>Case Else</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCaseElse()
   * @generated
   * @ordered
   */
  protected Case_Else caseElse;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected Case_StatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return StructuredTextPackage.Literals.CASE_STATEMENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Case_Element> getCaseElements()
  {
    if (caseElements == null)
    {
      caseElements = new EObjectContainmentEList<Case_Element>(Case_Element.class, this, StructuredTextPackage.CASE_STATEMENT__CASE_ELEMENTS);
    }
    return caseElements;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Case_Else getCaseElse()
  {
    return caseElse;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCaseElse(Case_Else newCaseElse, NotificationChain msgs)
  {
    Case_Else oldCaseElse = caseElse;
    caseElse = newCaseElse;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StructuredTextPackage.CASE_STATEMENT__CASE_ELSE, oldCaseElse, newCaseElse);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCaseElse(Case_Else newCaseElse)
  {
    if (newCaseElse != caseElse)
    {
      NotificationChain msgs = null;
      if (caseElse != null)
        msgs = ((InternalEObject)caseElse).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StructuredTextPackage.CASE_STATEMENT__CASE_ELSE, null, msgs);
      if (newCaseElse != null)
        msgs = ((InternalEObject)newCaseElse).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StructuredTextPackage.CASE_STATEMENT__CASE_ELSE, null, msgs);
      msgs = basicSetCaseElse(newCaseElse, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, StructuredTextPackage.CASE_STATEMENT__CASE_ELSE, newCaseElse, newCaseElse));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case StructuredTextPackage.CASE_STATEMENT__CASE_ELEMENTS:
        return ((InternalEList<?>)getCaseElements()).basicRemove(otherEnd, msgs);
      case StructuredTextPackage.CASE_STATEMENT__CASE_ELSE:
        return basicSetCaseElse(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case StructuredTextPackage.CASE_STATEMENT__CASE_ELEMENTS:
        return getCaseElements();
      case StructuredTextPackage.CASE_STATEMENT__CASE_ELSE:
        return getCaseElse();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case StructuredTextPackage.CASE_STATEMENT__CASE_ELEMENTS:
        getCaseElements().clear();
        getCaseElements().addAll((Collection<? extends Case_Element>)newValue);
        return;
      case StructuredTextPackage.CASE_STATEMENT__CASE_ELSE:
        setCaseElse((Case_Else)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case StructuredTextPackage.CASE_STATEMENT__CASE_ELEMENTS:
        getCaseElements().clear();
        return;
      case StructuredTextPackage.CASE_STATEMENT__CASE_ELSE:
        setCaseElse((Case_Else)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case StructuredTextPackage.CASE_STATEMENT__CASE_ELEMENTS:
        return caseElements != null && !caseElements.isEmpty();
      case StructuredTextPackage.CASE_STATEMENT__CASE_ELSE:
        return caseElse != null;
    }
    return super.eIsSet(featureID);
  }

} //Case_StatementImpl
