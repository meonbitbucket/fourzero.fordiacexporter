/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Signed Integer Type Name</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getSigned_Integer_Type_Name()
 * @model
 * @generated
 */
public interface Signed_Integer_Type_Name extends Integer_Type_Name
{
} // Signed_Integer_Type_Name
