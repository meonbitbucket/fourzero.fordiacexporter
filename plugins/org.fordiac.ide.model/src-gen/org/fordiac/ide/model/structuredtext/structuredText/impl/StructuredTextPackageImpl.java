/**
 */
package org.fordiac.ide.model.structuredtext.structuredText.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.fordiac.ide.model.structuredtext.structuredText.AdapterDeclaration;
import org.fordiac.ide.model.structuredtext.structuredText.AdapterDefinition;
import org.fordiac.ide.model.structuredtext.structuredText.Adapter_Variable;
import org.fordiac.ide.model.structuredtext.structuredText.Add_Expression;
import org.fordiac.ide.model.structuredtext.structuredText.And_Expression;
import org.fordiac.ide.model.structuredtext.structuredText.Array_Initial_Elements;
import org.fordiac.ide.model.structuredtext.structuredText.Array_Initialization;
import org.fordiac.ide.model.structuredtext.structuredText.Array_Spec_Init;
import org.fordiac.ide.model.structuredtext.structuredText.Array_Specification;
import org.fordiac.ide.model.structuredtext.structuredText.Array_Variable;
import org.fordiac.ide.model.structuredtext.structuredText.Assignment_Statement;
import org.fordiac.ide.model.structuredtext.structuredText.Bit_String_Type_Name;
import org.fordiac.ide.model.structuredtext.structuredText.Boolean_Literal;
import org.fordiac.ide.model.structuredtext.structuredText.Case_Element;
import org.fordiac.ide.model.structuredtext.structuredText.Case_Else;
import org.fordiac.ide.model.structuredtext.structuredText.Case_Statement;
import org.fordiac.ide.model.structuredtext.structuredText.Character_String;
import org.fordiac.ide.model.structuredtext.structuredText.Comparison;
import org.fordiac.ide.model.structuredtext.structuredText.Constant;
import org.fordiac.ide.model.structuredtext.structuredText.D_Byte_String_Spec;
import org.fordiac.ide.model.structuredtext.structuredText.Date;
import org.fordiac.ide.model.structuredtext.structuredText.Date_And_Time;
import org.fordiac.ide.model.structuredtext.structuredText.Date_Type_Name;
import org.fordiac.ide.model.structuredtext.structuredText.Double_Byte_Character_String_Literal;
import org.fordiac.ide.model.structuredtext.structuredText.Duration;
import org.fordiac.ide.model.structuredtext.structuredText.Elementary_Type_Name;
import org.fordiac.ide.model.structuredtext.structuredText.ElseIf_Statement;
import org.fordiac.ide.model.structuredtext.structuredText.Equ_Expression;
import org.fordiac.ide.model.structuredtext.structuredText.Expression;
import org.fordiac.ide.model.structuredtext.structuredText.For_Statement;
import org.fordiac.ide.model.structuredtext.structuredText.Func_Call;
import org.fordiac.ide.model.structuredtext.structuredText.If_Statement;
import org.fordiac.ide.model.structuredtext.structuredText.IntegerLiteral;
import org.fordiac.ide.model.structuredtext.structuredText.Integer_Type_Name;
import org.fordiac.ide.model.structuredtext.structuredText.IterationControl_Stmt;
import org.fordiac.ide.model.structuredtext.structuredText.Non_Generic_Type_Name;
import org.fordiac.ide.model.structuredtext.structuredText.Numeric_Literal;
import org.fordiac.ide.model.structuredtext.structuredText.Numeric_Type_Name;
import org.fordiac.ide.model.structuredtext.structuredText.Param_Assign;
import org.fordiac.ide.model.structuredtext.structuredText.Parameter;
import org.fordiac.ide.model.structuredtext.structuredText.Power_Expression;
import org.fordiac.ide.model.structuredtext.structuredText.Real_Literal;
import org.fordiac.ide.model.structuredtext.structuredText.Real_Type_Name;
import org.fordiac.ide.model.structuredtext.structuredText.Repeat_Statement;
import org.fordiac.ide.model.structuredtext.structuredText.S_Byte_String_Spec;
import org.fordiac.ide.model.structuredtext.structuredText.Selection_Statement;
import org.fordiac.ide.model.structuredtext.structuredText.Signed_Integer_Type_Name;
import org.fordiac.ide.model.structuredtext.structuredText.Simple_Spec_Init;
import org.fordiac.ide.model.structuredtext.structuredText.Single_Byte_Character_String_Literal;
import org.fordiac.ide.model.structuredtext.structuredText.Statement;
import org.fordiac.ide.model.structuredtext.structuredText.StatementList;
import org.fordiac.ide.model.structuredtext.structuredText.Structure_Spec;
import org.fordiac.ide.model.structuredtext.structuredText.StructuredTextFactory;
import org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage;
import org.fordiac.ide.model.structuredtext.structuredText.Subprog_Ctrl_Stmt;
import org.fordiac.ide.model.structuredtext.structuredText.Subrange;
import org.fordiac.ide.model.structuredtext.structuredText.Term;
import org.fordiac.ide.model.structuredtext.structuredText.Time_Literal;
import org.fordiac.ide.model.structuredtext.structuredText.Time_Of_Day;
import org.fordiac.ide.model.structuredtext.structuredText.Time_Type_Name;
import org.fordiac.ide.model.structuredtext.structuredText.Unsigned_Integer_Type_Name;
import org.fordiac.ide.model.structuredtext.structuredText.VarDeclaration;
import org.fordiac.ide.model.structuredtext.structuredText.Var_Variable;
import org.fordiac.ide.model.structuredtext.structuredText.Variable;
import org.fordiac.ide.model.structuredtext.structuredText.While_Statement;
import org.fordiac.ide.model.structuredtext.structuredText.Xor_Expression;
import org.fordiac.ide.model.structuredtext.structuredText.structuredTextAlgorithm;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class StructuredTextPackageImpl extends EPackageImpl implements StructuredTextPackage
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass structuredTextAlgorithmEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass varDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass adapterDefinitionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass adapterDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass parameterEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass simple_Spec_InitEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass array_Spec_InitEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass array_SpecificationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass subrangeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass array_InitializationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass array_Initial_ElementsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass structure_SpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass s_Byte_String_SpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass d_Byte_String_SpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass statementListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass statementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass assignment_StatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass variableEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass var_VariableEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass adapter_VariableEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass array_VariableEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass selection_StatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass if_StatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass elseIf_StatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass case_StatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass case_ElementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass case_ElseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass iterationControl_StmtEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass for_StatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass repeat_StatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass while_StatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass subprog_Ctrl_StmtEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass expressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass func_CallEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass param_AssignEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass constantEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass numeric_LiteralEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass integerLiteralEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass real_LiteralEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass boolean_LiteralEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass character_StringEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass single_Byte_Character_String_LiteralEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass double_Byte_Character_String_LiteralEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass time_LiteralEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass durationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass time_Of_DayEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass dateEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass date_And_TimeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass non_Generic_Type_NameEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass elementary_Type_NameEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass numeric_Type_NameEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass integer_Type_NameEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass signed_Integer_Type_NameEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unsigned_Integer_Type_NameEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass real_Type_NameEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass bit_String_Type_NameEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass time_Type_NameEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass date_Type_NameEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass xor_ExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass and_ExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass comparisonEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass equ_ExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass add_ExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass termEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass power_ExpressionEClass = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private StructuredTextPackageImpl()
  {
    super(eNS_URI, StructuredTextFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link StructuredTextPackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static StructuredTextPackage init()
  {
    if (isInited) return (StructuredTextPackage)EPackage.Registry.INSTANCE.getEPackage(StructuredTextPackage.eNS_URI);

    // Obtain or create and register package
    StructuredTextPackageImpl theStructuredTextPackage = (StructuredTextPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof StructuredTextPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new StructuredTextPackageImpl());

    isInited = true;

    // Create package meta-data objects
    theStructuredTextPackage.createPackageContents();

    // Initialize created meta-data
    theStructuredTextPackage.initializePackageContents();

    // Mark meta-data to indicate it can't be changed
    theStructuredTextPackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(StructuredTextPackage.eNS_URI, theStructuredTextPackage);
    return theStructuredTextPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getstructuredTextAlgorithm()
  {
    return structuredTextAlgorithmEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getstructuredTextAlgorithm_Adapters()
  {
    return (EReference)structuredTextAlgorithmEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getstructuredTextAlgorithm_VarDeclarations()
  {
    return (EReference)structuredTextAlgorithmEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getstructuredTextAlgorithm_Statements()
  {
    return (EReference)structuredTextAlgorithmEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getVarDeclaration()
  {
    return varDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getVarDeclaration_Name()
  {
    return (EAttribute)varDeclarationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getVarDeclaration_Specification()
  {
    return (EReference)varDeclarationEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAdapterDefinition()
  {
    return adapterDefinitionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getAdapterDefinition_Name()
  {
    return (EAttribute)adapterDefinitionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAdapterDefinition_VarDeclarations()
  {
    return (EReference)adapterDefinitionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAdapterDeclaration()
  {
    return adapterDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getAdapterDeclaration_Name()
  {
    return (EAttribute)adapterDeclarationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAdapterDeclaration_Type()
  {
    return (EReference)adapterDeclarationEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAdapterDeclaration_Parameters()
  {
    return (EReference)adapterDeclarationEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getParameter()
  {
    return parameterEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getParameter_Param()
  {
    return (EReference)parameterEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getParameter_Value()
  {
    return (EReference)parameterEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSimple_Spec_Init()
  {
    return simple_Spec_InitEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSimple_Spec_Init_Type()
  {
    return (EReference)simple_Spec_InitEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSimple_Spec_Init_Constant()
  {
    return (EReference)simple_Spec_InitEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getArray_Spec_Init()
  {
    return array_Spec_InitEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getArray_Specification()
  {
    return array_SpecificationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getArray_Specification_Initialization()
  {
    return (EReference)array_SpecificationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getArray_Specification_Subrange()
  {
    return (EReference)array_SpecificationEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getArray_Specification_Type()
  {
    return (EReference)array_SpecificationEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSubrange()
  {
    return subrangeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSubrange_Min()
  {
    return (EAttribute)subrangeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSubrange_Max()
  {
    return (EAttribute)subrangeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getArray_Initialization()
  {
    return array_InitializationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getArray_Initialization_InitElement()
  {
    return (EReference)array_InitializationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getArray_Initial_Elements()
  {
    return array_Initial_ElementsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getArray_Initial_Elements_Num()
  {
    return (EAttribute)array_Initial_ElementsEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getArray_Initial_Elements_ArrayElement()
  {
    return (EReference)array_Initial_ElementsEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStructure_Spec()
  {
    return structure_SpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getStructure_Spec_Name()
  {
    return (EAttribute)structure_SpecEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStructure_Spec_Element()
  {
    return (EReference)structure_SpecEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getS_Byte_String_Spec()
  {
    return s_Byte_String_SpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getS_Byte_String_Spec_Type()
  {
    return (EAttribute)s_Byte_String_SpecEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getS_Byte_String_Spec_Size()
  {
    return (EAttribute)s_Byte_String_SpecEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getS_Byte_String_Spec_String()
  {
    return (EReference)s_Byte_String_SpecEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getD_Byte_String_Spec()
  {
    return d_Byte_String_SpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getD_Byte_String_Spec_Type()
  {
    return (EAttribute)d_Byte_String_SpecEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getD_Byte_String_Spec_Size()
  {
    return (EAttribute)d_Byte_String_SpecEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getD_Byte_String_Spec_String()
  {
    return (EReference)d_Byte_String_SpecEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStatementList()
  {
    return statementListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStatementList_Statements()
  {
    return (EReference)statementListEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStatement()
  {
    return statementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAssignment_Statement()
  {
    return assignment_StatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAssignment_Statement_Variable()
  {
    return (EReference)assignment_StatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAssignment_Statement_Expression()
  {
    return (EReference)assignment_StatementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getVariable()
  {
    return variableEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getVariable_Var()
  {
    return (EReference)variableEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getVar_Variable()
  {
    return var_VariableEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAdapter_Variable()
  {
    return adapter_VariableEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAdapter_Variable_Adapter()
  {
    return (EReference)adapter_VariableEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getArray_Variable()
  {
    return array_VariableEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getArray_Variable_Num()
  {
    return (EReference)array_VariableEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSelection_Statement()
  {
    return selection_StatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSelection_Statement_Expression()
  {
    return (EReference)selection_StatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIf_Statement()
  {
    return if_StatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIf_Statement_Statments()
  {
    return (EReference)if_StatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIf_Statement_Elseif()
  {
    return (EReference)if_StatementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIf_Statement_ElseStatements()
  {
    return (EReference)if_StatementEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getElseIf_Statement()
  {
    return elseIf_StatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getElseIf_Statement_Expression()
  {
    return (EReference)elseIf_StatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getElseIf_Statement_Statements()
  {
    return (EReference)elseIf_StatementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCase_Statement()
  {
    return case_StatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCase_Statement_CaseElements()
  {
    return (EReference)case_StatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCase_Statement_CaseElse()
  {
    return (EReference)case_StatementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCase_Element()
  {
    return case_ElementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getCase_Element_Case()
  {
    return (EAttribute)case_ElementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCase_Element_Statements()
  {
    return (EReference)case_ElementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCase_Else()
  {
    return case_ElseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCase_Else_Statements()
  {
    return (EReference)case_ElseEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIterationControl_Stmt()
  {
    return iterationControl_StmtEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIterationControl_Stmt_Statement()
  {
    return (EAttribute)iterationControl_StmtEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFor_Statement()
  {
    return for_StatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFor_Statement_ControlVariable()
  {
    return (EReference)for_StatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFor_Statement_ExpressionFrom()
  {
    return (EReference)for_StatementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFor_Statement_ExpressionTo()
  {
    return (EReference)for_StatementEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFor_Statement_ExpressionBy()
  {
    return (EReference)for_StatementEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFor_Statement_Statements()
  {
    return (EReference)for_StatementEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRepeat_Statement()
  {
    return repeat_StatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRepeat_Statement_Statements()
  {
    return (EReference)repeat_StatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRepeat_Statement_Expression()
  {
    return (EReference)repeat_StatementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getWhile_Statement()
  {
    return while_StatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getWhile_Statement_Expression()
  {
    return (EReference)while_StatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getWhile_Statement_Statements()
  {
    return (EReference)while_StatementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSubprog_Ctrl_Stmt()
  {
    return subprog_Ctrl_StmtEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSubprog_Ctrl_Stmt_Call()
  {
    return (EReference)subprog_Ctrl_StmtEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExpression()
  {
    return expressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExpression_Left()
  {
    return (EReference)expressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getExpression_Operator()
  {
    return (EAttribute)expressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExpression_Right()
  {
    return (EReference)expressionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExpression_Expression()
  {
    return (EReference)expressionEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunc_Call()
  {
    return func_CallEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFunc_Call_Name()
  {
    return (EAttribute)func_CallEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunc_Call_Paramslist()
  {
    return (EReference)func_CallEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getParam_Assign()
  {
    return param_AssignEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getParam_Assign_Varname()
  {
    return (EAttribute)param_AssignEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getParam_Assign_Expr()
  {
    return (EReference)param_AssignEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getConstant()
  {
    return constantEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getConstant_Value()
  {
    return (EAttribute)constantEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNumeric_Literal()
  {
    return numeric_LiteralEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getNumeric_Literal_Type()
  {
    return (EAttribute)numeric_LiteralEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIntegerLiteral()
  {
    return integerLiteralEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getReal_Literal()
  {
    return real_LiteralEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getReal_Literal_Exponent()
  {
    return (EAttribute)real_LiteralEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBoolean_Literal()
  {
    return boolean_LiteralEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getBoolean_Literal_Type()
  {
    return (EAttribute)boolean_LiteralEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCharacter_String()
  {
    return character_StringEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getCharacter_String_Type()
  {
    return (EAttribute)character_StringEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSingle_Byte_Character_String_Literal()
  {
    return single_Byte_Character_String_LiteralEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDouble_Byte_Character_String_Literal()
  {
    return double_Byte_Character_String_LiteralEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTime_Literal()
  {
    return time_LiteralEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDuration()
  {
    return durationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTime_Of_Day()
  {
    return time_Of_DayEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDate()
  {
    return dateEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDate_And_Time()
  {
    return date_And_TimeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNon_Generic_Type_Name()
  {
    return non_Generic_Type_NameEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getElementary_Type_Name()
  {
    return elementary_Type_NameEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNumeric_Type_Name()
  {
    return numeric_Type_NameEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getInteger_Type_Name()
  {
    return integer_Type_NameEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSigned_Integer_Type_Name()
  {
    return signed_Integer_Type_NameEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUnsigned_Integer_Type_Name()
  {
    return unsigned_Integer_Type_NameEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getReal_Type_Name()
  {
    return real_Type_NameEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBit_String_Type_Name()
  {
    return bit_String_Type_NameEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTime_Type_Name()
  {
    return time_Type_NameEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDate_Type_Name()
  {
    return date_Type_NameEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getXor_Expression()
  {
    return xor_ExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAnd_Expression()
  {
    return and_ExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComparison()
  {
    return comparisonEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getEqu_Expression()
  {
    return equ_ExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAdd_Expression()
  {
    return add_ExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTerm()
  {
    return termEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPower_Expression()
  {
    return power_ExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StructuredTextFactory getStructuredTextFactory()
  {
    return (StructuredTextFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents()
  {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    structuredTextAlgorithmEClass = createEClass(STRUCTURED_TEXT_ALGORITHM);
    createEReference(structuredTextAlgorithmEClass, STRUCTURED_TEXT_ALGORITHM__ADAPTERS);
    createEReference(structuredTextAlgorithmEClass, STRUCTURED_TEXT_ALGORITHM__VAR_DECLARATIONS);
    createEReference(structuredTextAlgorithmEClass, STRUCTURED_TEXT_ALGORITHM__STATEMENTS);

    varDeclarationEClass = createEClass(VAR_DECLARATION);
    createEAttribute(varDeclarationEClass, VAR_DECLARATION__NAME);
    createEReference(varDeclarationEClass, VAR_DECLARATION__SPECIFICATION);

    adapterDefinitionEClass = createEClass(ADAPTER_DEFINITION);
    createEAttribute(adapterDefinitionEClass, ADAPTER_DEFINITION__NAME);
    createEReference(adapterDefinitionEClass, ADAPTER_DEFINITION__VAR_DECLARATIONS);

    adapterDeclarationEClass = createEClass(ADAPTER_DECLARATION);
    createEAttribute(adapterDeclarationEClass, ADAPTER_DECLARATION__NAME);
    createEReference(adapterDeclarationEClass, ADAPTER_DECLARATION__TYPE);
    createEReference(adapterDeclarationEClass, ADAPTER_DECLARATION__PARAMETERS);

    parameterEClass = createEClass(PARAMETER);
    createEReference(parameterEClass, PARAMETER__PARAM);
    createEReference(parameterEClass, PARAMETER__VALUE);

    simple_Spec_InitEClass = createEClass(SIMPLE_SPEC_INIT);
    createEReference(simple_Spec_InitEClass, SIMPLE_SPEC_INIT__TYPE);
    createEReference(simple_Spec_InitEClass, SIMPLE_SPEC_INIT__CONSTANT);

    array_Spec_InitEClass = createEClass(ARRAY_SPEC_INIT);

    array_SpecificationEClass = createEClass(ARRAY_SPECIFICATION);
    createEReference(array_SpecificationEClass, ARRAY_SPECIFICATION__INITIALIZATION);
    createEReference(array_SpecificationEClass, ARRAY_SPECIFICATION__SUBRANGE);
    createEReference(array_SpecificationEClass, ARRAY_SPECIFICATION__TYPE);

    subrangeEClass = createEClass(SUBRANGE);
    createEAttribute(subrangeEClass, SUBRANGE__MIN);
    createEAttribute(subrangeEClass, SUBRANGE__MAX);

    array_InitializationEClass = createEClass(ARRAY_INITIALIZATION);
    createEReference(array_InitializationEClass, ARRAY_INITIALIZATION__INIT_ELEMENT);

    array_Initial_ElementsEClass = createEClass(ARRAY_INITIAL_ELEMENTS);
    createEAttribute(array_Initial_ElementsEClass, ARRAY_INITIAL_ELEMENTS__NUM);
    createEReference(array_Initial_ElementsEClass, ARRAY_INITIAL_ELEMENTS__ARRAY_ELEMENT);

    structure_SpecEClass = createEClass(STRUCTURE_SPEC);
    createEAttribute(structure_SpecEClass, STRUCTURE_SPEC__NAME);
    createEReference(structure_SpecEClass, STRUCTURE_SPEC__ELEMENT);

    s_Byte_String_SpecEClass = createEClass(SBYTE_STRING_SPEC);
    createEAttribute(s_Byte_String_SpecEClass, SBYTE_STRING_SPEC__TYPE);
    createEAttribute(s_Byte_String_SpecEClass, SBYTE_STRING_SPEC__SIZE);
    createEReference(s_Byte_String_SpecEClass, SBYTE_STRING_SPEC__STRING);

    d_Byte_String_SpecEClass = createEClass(DBYTE_STRING_SPEC);
    createEAttribute(d_Byte_String_SpecEClass, DBYTE_STRING_SPEC__TYPE);
    createEAttribute(d_Byte_String_SpecEClass, DBYTE_STRING_SPEC__SIZE);
    createEReference(d_Byte_String_SpecEClass, DBYTE_STRING_SPEC__STRING);

    statementListEClass = createEClass(STATEMENT_LIST);
    createEReference(statementListEClass, STATEMENT_LIST__STATEMENTS);

    statementEClass = createEClass(STATEMENT);

    assignment_StatementEClass = createEClass(ASSIGNMENT_STATEMENT);
    createEReference(assignment_StatementEClass, ASSIGNMENT_STATEMENT__VARIABLE);
    createEReference(assignment_StatementEClass, ASSIGNMENT_STATEMENT__EXPRESSION);

    variableEClass = createEClass(VARIABLE);
    createEReference(variableEClass, VARIABLE__VAR);

    var_VariableEClass = createEClass(VAR_VARIABLE);

    adapter_VariableEClass = createEClass(ADAPTER_VARIABLE);
    createEReference(adapter_VariableEClass, ADAPTER_VARIABLE__ADAPTER);

    array_VariableEClass = createEClass(ARRAY_VARIABLE);
    createEReference(array_VariableEClass, ARRAY_VARIABLE__NUM);

    selection_StatementEClass = createEClass(SELECTION_STATEMENT);
    createEReference(selection_StatementEClass, SELECTION_STATEMENT__EXPRESSION);

    if_StatementEClass = createEClass(IF_STATEMENT);
    createEReference(if_StatementEClass, IF_STATEMENT__STATMENTS);
    createEReference(if_StatementEClass, IF_STATEMENT__ELSEIF);
    createEReference(if_StatementEClass, IF_STATEMENT__ELSE_STATEMENTS);

    elseIf_StatementEClass = createEClass(ELSE_IF_STATEMENT);
    createEReference(elseIf_StatementEClass, ELSE_IF_STATEMENT__EXPRESSION);
    createEReference(elseIf_StatementEClass, ELSE_IF_STATEMENT__STATEMENTS);

    case_StatementEClass = createEClass(CASE_STATEMENT);
    createEReference(case_StatementEClass, CASE_STATEMENT__CASE_ELEMENTS);
    createEReference(case_StatementEClass, CASE_STATEMENT__CASE_ELSE);

    case_ElementEClass = createEClass(CASE_ELEMENT);
    createEAttribute(case_ElementEClass, CASE_ELEMENT__CASE);
    createEReference(case_ElementEClass, CASE_ELEMENT__STATEMENTS);

    case_ElseEClass = createEClass(CASE_ELSE);
    createEReference(case_ElseEClass, CASE_ELSE__STATEMENTS);

    iterationControl_StmtEClass = createEClass(ITERATION_CONTROL_STMT);
    createEAttribute(iterationControl_StmtEClass, ITERATION_CONTROL_STMT__STATEMENT);

    for_StatementEClass = createEClass(FOR_STATEMENT);
    createEReference(for_StatementEClass, FOR_STATEMENT__CONTROL_VARIABLE);
    createEReference(for_StatementEClass, FOR_STATEMENT__EXPRESSION_FROM);
    createEReference(for_StatementEClass, FOR_STATEMENT__EXPRESSION_TO);
    createEReference(for_StatementEClass, FOR_STATEMENT__EXPRESSION_BY);
    createEReference(for_StatementEClass, FOR_STATEMENT__STATEMENTS);

    repeat_StatementEClass = createEClass(REPEAT_STATEMENT);
    createEReference(repeat_StatementEClass, REPEAT_STATEMENT__STATEMENTS);
    createEReference(repeat_StatementEClass, REPEAT_STATEMENT__EXPRESSION);

    while_StatementEClass = createEClass(WHILE_STATEMENT);
    createEReference(while_StatementEClass, WHILE_STATEMENT__EXPRESSION);
    createEReference(while_StatementEClass, WHILE_STATEMENT__STATEMENTS);

    subprog_Ctrl_StmtEClass = createEClass(SUBPROG_CTRL_STMT);
    createEReference(subprog_Ctrl_StmtEClass, SUBPROG_CTRL_STMT__CALL);

    expressionEClass = createEClass(EXPRESSION);
    createEReference(expressionEClass, EXPRESSION__LEFT);
    createEAttribute(expressionEClass, EXPRESSION__OPERATOR);
    createEReference(expressionEClass, EXPRESSION__RIGHT);
    createEReference(expressionEClass, EXPRESSION__EXPRESSION);

    func_CallEClass = createEClass(FUNC_CALL);
    createEAttribute(func_CallEClass, FUNC_CALL__NAME);
    createEReference(func_CallEClass, FUNC_CALL__PARAMSLIST);

    param_AssignEClass = createEClass(PARAM_ASSIGN);
    createEAttribute(param_AssignEClass, PARAM_ASSIGN__VARNAME);
    createEReference(param_AssignEClass, PARAM_ASSIGN__EXPR);

    constantEClass = createEClass(CONSTANT);
    createEAttribute(constantEClass, CONSTANT__VALUE);

    numeric_LiteralEClass = createEClass(NUMERIC_LITERAL);
    createEAttribute(numeric_LiteralEClass, NUMERIC_LITERAL__TYPE);

    integerLiteralEClass = createEClass(INTEGER_LITERAL);

    real_LiteralEClass = createEClass(REAL_LITERAL);
    createEAttribute(real_LiteralEClass, REAL_LITERAL__EXPONENT);

    boolean_LiteralEClass = createEClass(BOOLEAN_LITERAL);
    createEAttribute(boolean_LiteralEClass, BOOLEAN_LITERAL__TYPE);

    character_StringEClass = createEClass(CHARACTER_STRING);
    createEAttribute(character_StringEClass, CHARACTER_STRING__TYPE);

    single_Byte_Character_String_LiteralEClass = createEClass(SINGLE_BYTE_CHARACTER_STRING_LITERAL);

    double_Byte_Character_String_LiteralEClass = createEClass(DOUBLE_BYTE_CHARACTER_STRING_LITERAL);

    time_LiteralEClass = createEClass(TIME_LITERAL);

    durationEClass = createEClass(DURATION);

    time_Of_DayEClass = createEClass(TIME_OF_DAY);

    dateEClass = createEClass(DATE);

    date_And_TimeEClass = createEClass(DATE_AND_TIME);

    non_Generic_Type_NameEClass = createEClass(NON_GENERIC_TYPE_NAME);

    elementary_Type_NameEClass = createEClass(ELEMENTARY_TYPE_NAME);

    numeric_Type_NameEClass = createEClass(NUMERIC_TYPE_NAME);

    integer_Type_NameEClass = createEClass(INTEGER_TYPE_NAME);

    signed_Integer_Type_NameEClass = createEClass(SIGNED_INTEGER_TYPE_NAME);

    unsigned_Integer_Type_NameEClass = createEClass(UNSIGNED_INTEGER_TYPE_NAME);

    real_Type_NameEClass = createEClass(REAL_TYPE_NAME);

    bit_String_Type_NameEClass = createEClass(BIT_STRING_TYPE_NAME);

    time_Type_NameEClass = createEClass(TIME_TYPE_NAME);

    date_Type_NameEClass = createEClass(DATE_TYPE_NAME);

    xor_ExpressionEClass = createEClass(XOR_EXPRESSION);

    and_ExpressionEClass = createEClass(AND_EXPRESSION);

    comparisonEClass = createEClass(COMPARISON);

    equ_ExpressionEClass = createEClass(EQU_EXPRESSION);

    add_ExpressionEClass = createEClass(ADD_EXPRESSION);

    termEClass = createEClass(TERM);

    power_ExpressionEClass = createEClass(POWER_EXPRESSION);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents()
  {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes
    array_SpecificationEClass.getESuperTypes().add(this.getArray_Spec_Init());
    assignment_StatementEClass.getESuperTypes().add(this.getStatement());
    variableEClass.getESuperTypes().add(this.getExpression());
    var_VariableEClass.getESuperTypes().add(this.getVariable());
    adapter_VariableEClass.getESuperTypes().add(this.getVariable());
    array_VariableEClass.getESuperTypes().add(this.getVariable());
    selection_StatementEClass.getESuperTypes().add(this.getStatement());
    if_StatementEClass.getESuperTypes().add(this.getSelection_Statement());
    case_StatementEClass.getESuperTypes().add(this.getSelection_Statement());
    iterationControl_StmtEClass.getESuperTypes().add(this.getStatement());
    for_StatementEClass.getESuperTypes().add(this.getStatement());
    repeat_StatementEClass.getESuperTypes().add(this.getStatement());
    while_StatementEClass.getESuperTypes().add(this.getStatement());
    subprog_Ctrl_StmtEClass.getESuperTypes().add(this.getStatement());
    func_CallEClass.getESuperTypes().add(this.getExpression());
    constantEClass.getESuperTypes().add(this.getArray_Initial_Elements());
    constantEClass.getESuperTypes().add(this.getExpression());
    numeric_LiteralEClass.getESuperTypes().add(this.getConstant());
    integerLiteralEClass.getESuperTypes().add(this.getNumeric_Literal());
    real_LiteralEClass.getESuperTypes().add(this.getNumeric_Literal());
    boolean_LiteralEClass.getESuperTypes().add(this.getConstant());
    character_StringEClass.getESuperTypes().add(this.getConstant());
    single_Byte_Character_String_LiteralEClass.getESuperTypes().add(this.getCharacter_String());
    double_Byte_Character_String_LiteralEClass.getESuperTypes().add(this.getCharacter_String());
    time_LiteralEClass.getESuperTypes().add(this.getConstant());
    durationEClass.getESuperTypes().add(this.getTime_Literal());
    time_Of_DayEClass.getESuperTypes().add(this.getTime_Literal());
    dateEClass.getESuperTypes().add(this.getTime_Literal());
    date_And_TimeEClass.getESuperTypes().add(this.getTime_Literal());
    elementary_Type_NameEClass.getESuperTypes().add(this.getNon_Generic_Type_Name());
    numeric_Type_NameEClass.getESuperTypes().add(this.getElementary_Type_Name());
    integer_Type_NameEClass.getESuperTypes().add(this.getNumeric_Type_Name());
    signed_Integer_Type_NameEClass.getESuperTypes().add(this.getInteger_Type_Name());
    unsigned_Integer_Type_NameEClass.getESuperTypes().add(this.getInteger_Type_Name());
    real_Type_NameEClass.getESuperTypes().add(this.getNumeric_Type_Name());
    bit_String_Type_NameEClass.getESuperTypes().add(this.getElementary_Type_Name());
    time_Type_NameEClass.getESuperTypes().add(this.getElementary_Type_Name());
    date_Type_NameEClass.getESuperTypes().add(this.getElementary_Type_Name());
    xor_ExpressionEClass.getESuperTypes().add(this.getExpression());
    and_ExpressionEClass.getESuperTypes().add(this.getExpression());
    comparisonEClass.getESuperTypes().add(this.getExpression());
    equ_ExpressionEClass.getESuperTypes().add(this.getExpression());
    add_ExpressionEClass.getESuperTypes().add(this.getExpression());
    termEClass.getESuperTypes().add(this.getExpression());
    power_ExpressionEClass.getESuperTypes().add(this.getExpression());

    // Initialize classes and features; add operations and parameters
    initEClass(structuredTextAlgorithmEClass, structuredTextAlgorithm.class, "structuredTextAlgorithm", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getstructuredTextAlgorithm_Adapters(), this.getAdapterDefinition(), null, "adapters", null, 0, -1, structuredTextAlgorithm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getstructuredTextAlgorithm_VarDeclarations(), ecorePackage.getEObject(), null, "varDeclarations", null, 0, -1, structuredTextAlgorithm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getstructuredTextAlgorithm_Statements(), this.getStatementList(), null, "statements", null, 0, 1, structuredTextAlgorithm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(varDeclarationEClass, VarDeclaration.class, "VarDeclaration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getVarDeclaration_Name(), ecorePackage.getEString(), "name", null, 0, 1, VarDeclaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getVarDeclaration_Specification(), ecorePackage.getEObject(), null, "specification", null, 0, 1, VarDeclaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(adapterDefinitionEClass, AdapterDefinition.class, "AdapterDefinition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getAdapterDefinition_Name(), ecorePackage.getEString(), "name", null, 0, 1, AdapterDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getAdapterDefinition_VarDeclarations(), this.getVarDeclaration(), null, "varDeclarations", null, 0, -1, AdapterDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(adapterDeclarationEClass, AdapterDeclaration.class, "AdapterDeclaration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getAdapterDeclaration_Name(), ecorePackage.getEString(), "name", null, 0, 1, AdapterDeclaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getAdapterDeclaration_Type(), this.getAdapterDefinition(), null, "type", null, 0, 1, AdapterDeclaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getAdapterDeclaration_Parameters(), this.getParameter(), null, "parameters", null, 0, -1, AdapterDeclaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(parameterEClass, Parameter.class, "Parameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getParameter_Param(), this.getVarDeclaration(), null, "param", null, 0, 1, Parameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getParameter_Value(), ecorePackage.getEObject(), null, "value", null, 0, 1, Parameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(simple_Spec_InitEClass, Simple_Spec_Init.class, "Simple_Spec_Init", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSimple_Spec_Init_Type(), this.getElementary_Type_Name(), null, "type", null, 0, 1, Simple_Spec_Init.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSimple_Spec_Init_Constant(), this.getConstant(), null, "constant", null, 0, 1, Simple_Spec_Init.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(array_Spec_InitEClass, Array_Spec_Init.class, "Array_Spec_Init", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(array_SpecificationEClass, Array_Specification.class, "Array_Specification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getArray_Specification_Initialization(), this.getArray_Initialization(), null, "initialization", null, 0, 1, Array_Specification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getArray_Specification_Subrange(), this.getSubrange(), null, "subrange", null, 0, -1, Array_Specification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getArray_Specification_Type(), this.getNon_Generic_Type_Name(), null, "type", null, 0, 1, Array_Specification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(subrangeEClass, Subrange.class, "Subrange", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getSubrange_Min(), ecorePackage.getEString(), "min", null, 0, 1, Subrange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getSubrange_Max(), ecorePackage.getEString(), "max", null, 0, 1, Subrange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(array_InitializationEClass, Array_Initialization.class, "Array_Initialization", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getArray_Initialization_InitElement(), this.getArray_Initial_Elements(), null, "initElement", null, 0, -1, Array_Initialization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(array_Initial_ElementsEClass, Array_Initial_Elements.class, "Array_Initial_Elements", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getArray_Initial_Elements_Num(), ecorePackage.getEInt(), "num", null, 0, 1, Array_Initial_Elements.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getArray_Initial_Elements_ArrayElement(), this.getConstant(), null, "arrayElement", null, 0, 1, Array_Initial_Elements.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(structure_SpecEClass, Structure_Spec.class, "Structure_Spec", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getStructure_Spec_Name(), ecorePackage.getEString(), "name", null, 0, 1, Structure_Spec.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getStructure_Spec_Element(), this.getVarDeclaration(), null, "element", null, 0, -1, Structure_Spec.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(s_Byte_String_SpecEClass, S_Byte_String_Spec.class, "S_Byte_String_Spec", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getS_Byte_String_Spec_Type(), ecorePackage.getEString(), "type", null, 0, 1, S_Byte_String_Spec.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getS_Byte_String_Spec_Size(), ecorePackage.getEInt(), "size", null, 0, 1, S_Byte_String_Spec.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getS_Byte_String_Spec_String(), this.getSingle_Byte_Character_String_Literal(), null, "string", null, 0, 1, S_Byte_String_Spec.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(d_Byte_String_SpecEClass, D_Byte_String_Spec.class, "D_Byte_String_Spec", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getD_Byte_String_Spec_Type(), ecorePackage.getEString(), "type", null, 0, 1, D_Byte_String_Spec.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getD_Byte_String_Spec_Size(), ecorePackage.getEInt(), "size", null, 0, 1, D_Byte_String_Spec.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getD_Byte_String_Spec_String(), this.getDouble_Byte_Character_String_Literal(), null, "string", null, 0, 1, D_Byte_String_Spec.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(statementListEClass, StatementList.class, "StatementList", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getStatementList_Statements(), this.getStatement(), null, "statements", null, 0, -1, StatementList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(statementEClass, Statement.class, "Statement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(assignment_StatementEClass, Assignment_Statement.class, "Assignment_Statement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getAssignment_Statement_Variable(), this.getVariable(), null, "variable", null, 0, 1, Assignment_Statement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getAssignment_Statement_Expression(), this.getExpression(), null, "expression", null, 0, 1, Assignment_Statement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(variableEClass, Variable.class, "Variable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getVariable_Var(), this.getVarDeclaration(), null, "var", null, 0, 1, Variable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(var_VariableEClass, Var_Variable.class, "Var_Variable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(adapter_VariableEClass, Adapter_Variable.class, "Adapter_Variable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getAdapter_Variable_Adapter(), this.getAdapterDeclaration(), null, "adapter", null, 0, 1, Adapter_Variable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(array_VariableEClass, Array_Variable.class, "Array_Variable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getArray_Variable_Num(), this.getExpression(), null, "num", null, 0, 1, Array_Variable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(selection_StatementEClass, Selection_Statement.class, "Selection_Statement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSelection_Statement_Expression(), this.getExpression(), null, "expression", null, 0, 1, Selection_Statement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(if_StatementEClass, If_Statement.class, "If_Statement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getIf_Statement_Statments(), this.getStatementList(), null, "statments", null, 0, 1, If_Statement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getIf_Statement_Elseif(), this.getElseIf_Statement(), null, "elseif", null, 0, -1, If_Statement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getIf_Statement_ElseStatements(), this.getStatementList(), null, "elseStatements", null, 0, 1, If_Statement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(elseIf_StatementEClass, ElseIf_Statement.class, "ElseIf_Statement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getElseIf_Statement_Expression(), this.getExpression(), null, "expression", null, 0, 1, ElseIf_Statement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getElseIf_Statement_Statements(), this.getStatementList(), null, "statements", null, 0, 1, ElseIf_Statement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(case_StatementEClass, Case_Statement.class, "Case_Statement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getCase_Statement_CaseElements(), this.getCase_Element(), null, "caseElements", null, 0, -1, Case_Statement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getCase_Statement_CaseElse(), this.getCase_Else(), null, "caseElse", null, 0, 1, Case_Statement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(case_ElementEClass, Case_Element.class, "Case_Element", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getCase_Element_Case(), ecorePackage.getEInt(), "case", null, 0, 1, Case_Element.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getCase_Element_Statements(), this.getStatementList(), null, "statements", null, 0, 1, Case_Element.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(case_ElseEClass, Case_Else.class, "Case_Else", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getCase_Else_Statements(), this.getStatementList(), null, "statements", null, 0, 1, Case_Else.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(iterationControl_StmtEClass, IterationControl_Stmt.class, "IterationControl_Stmt", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getIterationControl_Stmt_Statement(), ecorePackage.getEString(), "statement", null, 0, 1, IterationControl_Stmt.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(for_StatementEClass, For_Statement.class, "For_Statement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getFor_Statement_ControlVariable(), this.getVarDeclaration(), null, "controlVariable", null, 0, 1, For_Statement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getFor_Statement_ExpressionFrom(), this.getExpression(), null, "expressionFrom", null, 0, 1, For_Statement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getFor_Statement_ExpressionTo(), this.getExpression(), null, "expressionTo", null, 0, 1, For_Statement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getFor_Statement_ExpressionBy(), this.getExpression(), null, "expressionBy", null, 0, 1, For_Statement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getFor_Statement_Statements(), this.getStatementList(), null, "statements", null, 0, 1, For_Statement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(repeat_StatementEClass, Repeat_Statement.class, "Repeat_Statement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getRepeat_Statement_Statements(), this.getStatementList(), null, "statements", null, 0, 1, Repeat_Statement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getRepeat_Statement_Expression(), this.getExpression(), null, "expression", null, 0, 1, Repeat_Statement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(while_StatementEClass, While_Statement.class, "While_Statement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getWhile_Statement_Expression(), this.getExpression(), null, "expression", null, 0, 1, While_Statement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getWhile_Statement_Statements(), this.getStatementList(), null, "statements", null, 0, 1, While_Statement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(subprog_Ctrl_StmtEClass, Subprog_Ctrl_Stmt.class, "Subprog_Ctrl_Stmt", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSubprog_Ctrl_Stmt_Call(), this.getFunc_Call(), null, "call", null, 0, 1, Subprog_Ctrl_Stmt.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(expressionEClass, Expression.class, "Expression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getExpression_Left(), this.getExpression(), null, "left", null, 0, 1, Expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getExpression_Operator(), ecorePackage.getEString(), "operator", null, 0, 1, Expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getExpression_Right(), this.getExpression(), null, "right", null, 0, 1, Expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getExpression_Expression(), this.getExpression(), null, "expression", null, 0, 1, Expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(func_CallEClass, Func_Call.class, "Func_Call", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getFunc_Call_Name(), ecorePackage.getEString(), "name", null, 0, 1, Func_Call.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getFunc_Call_Paramslist(), this.getParam_Assign(), null, "paramslist", null, 0, -1, Func_Call.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(param_AssignEClass, Param_Assign.class, "Param_Assign", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getParam_Assign_Varname(), ecorePackage.getEString(), "varname", null, 0, 1, Param_Assign.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getParam_Assign_Expr(), this.getExpression(), null, "expr", null, 0, 1, Param_Assign.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(constantEClass, Constant.class, "Constant", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getConstant_Value(), ecorePackage.getEString(), "value", null, 0, 1, Constant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(numeric_LiteralEClass, Numeric_Literal.class, "Numeric_Literal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getNumeric_Literal_Type(), ecorePackage.getEString(), "type", null, 0, 1, Numeric_Literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(integerLiteralEClass, IntegerLiteral.class, "IntegerLiteral", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(real_LiteralEClass, Real_Literal.class, "Real_Literal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getReal_Literal_Exponent(), ecorePackage.getEString(), "exponent", null, 0, 1, Real_Literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(boolean_LiteralEClass, Boolean_Literal.class, "Boolean_Literal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getBoolean_Literal_Type(), ecorePackage.getEString(), "type", null, 0, 1, Boolean_Literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(character_StringEClass, Character_String.class, "Character_String", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getCharacter_String_Type(), ecorePackage.getEString(), "type", null, 0, 1, Character_String.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(single_Byte_Character_String_LiteralEClass, Single_Byte_Character_String_Literal.class, "Single_Byte_Character_String_Literal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(double_Byte_Character_String_LiteralEClass, Double_Byte_Character_String_Literal.class, "Double_Byte_Character_String_Literal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(time_LiteralEClass, Time_Literal.class, "Time_Literal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(durationEClass, Duration.class, "Duration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(time_Of_DayEClass, Time_Of_Day.class, "Time_Of_Day", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(dateEClass, Date.class, "Date", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(date_And_TimeEClass, Date_And_Time.class, "Date_And_Time", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(non_Generic_Type_NameEClass, Non_Generic_Type_Name.class, "Non_Generic_Type_Name", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(elementary_Type_NameEClass, Elementary_Type_Name.class, "Elementary_Type_Name", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(numeric_Type_NameEClass, Numeric_Type_Name.class, "Numeric_Type_Name", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(integer_Type_NameEClass, Integer_Type_Name.class, "Integer_Type_Name", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(signed_Integer_Type_NameEClass, Signed_Integer_Type_Name.class, "Signed_Integer_Type_Name", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(unsigned_Integer_Type_NameEClass, Unsigned_Integer_Type_Name.class, "Unsigned_Integer_Type_Name", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(real_Type_NameEClass, Real_Type_Name.class, "Real_Type_Name", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(bit_String_Type_NameEClass, Bit_String_Type_Name.class, "Bit_String_Type_Name", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(time_Type_NameEClass, Time_Type_Name.class, "Time_Type_Name", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(date_Type_NameEClass, Date_Type_Name.class, "Date_Type_Name", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(xor_ExpressionEClass, Xor_Expression.class, "Xor_Expression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(and_ExpressionEClass, And_Expression.class, "And_Expression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(comparisonEClass, Comparison.class, "Comparison", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(equ_ExpressionEClass, Equ_Expression.class, "Equ_Expression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(add_ExpressionEClass, Add_Expression.class, "Add_Expression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(termEClass, Term.class, "Term", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(power_ExpressionEClass, Power_Expression.class, "Power_Expression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    // Create resource
    createResource(eNS_URI);
  }

} //StructuredTextPackageImpl
