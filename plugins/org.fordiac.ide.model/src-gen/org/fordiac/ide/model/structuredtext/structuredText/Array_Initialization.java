/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Array Initialization</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Initialization#getInitElement <em>Init Element</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getArray_Initialization()
 * @model
 * @generated
 */
public interface Array_Initialization extends EObject
{
  /**
   * Returns the value of the '<em><b>Init Element</b></em>' containment reference list.
   * The list contents are of type {@link org.fordiac.ide.model.structuredtext.structuredText.Array_Initial_Elements}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Init Element</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Init Element</em>' containment reference list.
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getArray_Initialization_InitElement()
   * @model containment="true"
   * @generated
   */
  EList<Array_Initial_Elements> getInitElement();

} // Array_Initialization
