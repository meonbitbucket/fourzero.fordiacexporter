package org.fordiac.ide.model.structuredtext.serializer;

import com.google.inject.Inject;
import com.google.inject.Provider;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.acceptor.ISemanticSequenceAcceptor;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.diagnostic.ISemanticSequencerDiagnosticProvider;
import org.eclipse.xtext.serializer.diagnostic.ISerializationDiagnostic.Acceptor;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.GenericSequencer;
import org.eclipse.xtext.serializer.sequencer.ISemanticNodeProvider.INodesForEObjectProvider;
import org.eclipse.xtext.serializer.sequencer.ISemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;
import org.fordiac.ide.model.structuredtext.services.StructuredTextGrammarAccess;
import org.fordiac.ide.model.structuredtext.structuredText.AdapterDeclaration;
import org.fordiac.ide.model.structuredtext.structuredText.AdapterDefinition;
import org.fordiac.ide.model.structuredtext.structuredText.Adapter_Variable;
import org.fordiac.ide.model.structuredtext.structuredText.Add_Expression;
import org.fordiac.ide.model.structuredtext.structuredText.And_Expression;
import org.fordiac.ide.model.structuredtext.structuredText.Array_Initial_Elements;
import org.fordiac.ide.model.structuredtext.structuredText.Array_Initialization;
import org.fordiac.ide.model.structuredtext.structuredText.Array_Specification;
import org.fordiac.ide.model.structuredtext.structuredText.Array_Variable;
import org.fordiac.ide.model.structuredtext.structuredText.Assignment_Statement;
import org.fordiac.ide.model.structuredtext.structuredText.Bit_String_Type_Name;
import org.fordiac.ide.model.structuredtext.structuredText.Boolean_Literal;
import org.fordiac.ide.model.structuredtext.structuredText.Case_Element;
import org.fordiac.ide.model.structuredtext.structuredText.Case_Else;
import org.fordiac.ide.model.structuredtext.structuredText.Case_Statement;
import org.fordiac.ide.model.structuredtext.structuredText.Comparison;
import org.fordiac.ide.model.structuredtext.structuredText.D_Byte_String_Spec;
import org.fordiac.ide.model.structuredtext.structuredText.Date;
import org.fordiac.ide.model.structuredtext.structuredText.Date_And_Time;
import org.fordiac.ide.model.structuredtext.structuredText.Date_Type_Name;
import org.fordiac.ide.model.structuredtext.structuredText.Double_Byte_Character_String_Literal;
import org.fordiac.ide.model.structuredtext.structuredText.Duration;
import org.fordiac.ide.model.structuredtext.structuredText.ElseIf_Statement;
import org.fordiac.ide.model.structuredtext.structuredText.Equ_Expression;
import org.fordiac.ide.model.structuredtext.structuredText.Expression;
import org.fordiac.ide.model.structuredtext.structuredText.For_Statement;
import org.fordiac.ide.model.structuredtext.structuredText.Func_Call;
import org.fordiac.ide.model.structuredtext.structuredText.If_Statement;
import org.fordiac.ide.model.structuredtext.structuredText.IntegerLiteral;
import org.fordiac.ide.model.structuredtext.structuredText.IterationControl_Stmt;
import org.fordiac.ide.model.structuredtext.structuredText.Param_Assign;
import org.fordiac.ide.model.structuredtext.structuredText.Parameter;
import org.fordiac.ide.model.structuredtext.structuredText.Power_Expression;
import org.fordiac.ide.model.structuredtext.structuredText.Real_Literal;
import org.fordiac.ide.model.structuredtext.structuredText.Real_Type_Name;
import org.fordiac.ide.model.structuredtext.structuredText.Repeat_Statement;
import org.fordiac.ide.model.structuredtext.structuredText.S_Byte_String_Spec;
import org.fordiac.ide.model.structuredtext.structuredText.Signed_Integer_Type_Name;
import org.fordiac.ide.model.structuredtext.structuredText.Simple_Spec_Init;
import org.fordiac.ide.model.structuredtext.structuredText.Single_Byte_Character_String_Literal;
import org.fordiac.ide.model.structuredtext.structuredText.StatementList;
import org.fordiac.ide.model.structuredtext.structuredText.Structure_Spec;
import org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage;
import org.fordiac.ide.model.structuredtext.structuredText.Subprog_Ctrl_Stmt;
import org.fordiac.ide.model.structuredtext.structuredText.Subrange;
import org.fordiac.ide.model.structuredtext.structuredText.Term;
import org.fordiac.ide.model.structuredtext.structuredText.Time_Of_Day;
import org.fordiac.ide.model.structuredtext.structuredText.Time_Type_Name;
import org.fordiac.ide.model.structuredtext.structuredText.Unsigned_Integer_Type_Name;
import org.fordiac.ide.model.structuredtext.structuredText.VarDeclaration;
import org.fordiac.ide.model.structuredtext.structuredText.Var_Variable;
import org.fordiac.ide.model.structuredtext.structuredText.While_Statement;
import org.fordiac.ide.model.structuredtext.structuredText.Xor_Expression;
import org.fordiac.ide.model.structuredtext.structuredText.structuredTextAlgorithm;

@SuppressWarnings("all")
public class StructuredTextSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private StructuredTextGrammarAccess grammarAccess;
	
	public void createSequence(EObject context, EObject semanticObject) {
		if(semanticObject.eClass().getEPackage() == StructuredTextPackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case StructuredTextPackage.ADAPTER_DECLARATION:
				if(context == grammarAccess.getAdapterDeclarationRule()) {
					sequence_AdapterDeclaration(context, (AdapterDeclaration) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.ADAPTER_DEFINITION:
				if(context == grammarAccess.getAdapterDefinitionRule()) {
					sequence_AdapterDefinition(context, (AdapterDefinition) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.ADAPTER_VARIABLE:
				if(context == grammarAccess.getAdapter_VariableRule() ||
				   context == grammarAccess.getPrimary_ExpressionRule() ||
				   context == grammarAccess.getVariableRule()) {
					sequence_Adapter_Variable(context, (Adapter_Variable) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.ADD_EXPRESSION:
				if(context == grammarAccess.getAdd_ExpressionRule() ||
				   context == grammarAccess.getAdd_ExpressionAccess().getAdd_ExpressionLeftAction_1_0() ||
				   context == grammarAccess.getAnd_ExpressionRule() ||
				   context == grammarAccess.getAnd_ExpressionAccess().getAnd_ExpressionLeftAction_1_0() ||
				   context == grammarAccess.getComparisonRule() ||
				   context == grammarAccess.getComparisonAccess().getComparisonLeftAction_1_0() ||
				   context == grammarAccess.getEqu_ExpressionRule() ||
				   context == grammarAccess.getEqu_ExpressionAccess().getEqu_ExpressionLeftAction_1_0() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getExpressionLeftAction_1_0() ||
				   context == grammarAccess.getPrimary_ExpressionRule() ||
				   context == grammarAccess.getXor_ExpressionRule() ||
				   context == grammarAccess.getXor_ExpressionAccess().getXor_ExpressionLeftAction_1_0()) {
					sequence_Add_Expression(context, (Add_Expression) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.AND_EXPRESSION:
				if(context == grammarAccess.getAnd_ExpressionRule() ||
				   context == grammarAccess.getAnd_ExpressionAccess().getAnd_ExpressionLeftAction_1_0() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getExpressionLeftAction_1_0() ||
				   context == grammarAccess.getPrimary_ExpressionRule() ||
				   context == grammarAccess.getXor_ExpressionRule() ||
				   context == grammarAccess.getXor_ExpressionAccess().getXor_ExpressionLeftAction_1_0()) {
					sequence_And_Expression(context, (And_Expression) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.ARRAY_INITIAL_ELEMENTS:
				if(context == grammarAccess.getArray_Initial_ElementsRule()) {
					sequence_Array_Initial_Elements(context, (Array_Initial_Elements) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.ARRAY_INITIALIZATION:
				if(context == grammarAccess.getArray_InitializationRule()) {
					sequence_Array_Initialization(context, (Array_Initialization) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.ARRAY_SPECIFICATION:
				if(context == grammarAccess.getArray_Spec_InitRule()) {
					sequence_Array_Spec_Init_Array_Specification(context, (Array_Specification) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getArray_SpecificationRule()) {
					sequence_Array_Specification(context, (Array_Specification) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.ARRAY_VARIABLE:
				if(context == grammarAccess.getArray_VariableRule() ||
				   context == grammarAccess.getPrimary_ExpressionRule() ||
				   context == grammarAccess.getVariableRule()) {
					sequence_Array_Variable(context, (Array_Variable) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.ASSIGNMENT_STATEMENT:
				if(context == grammarAccess.getAssignment_StatementRule() ||
				   context == grammarAccess.getStatementRule()) {
					sequence_Assignment_Statement(context, (Assignment_Statement) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.BIT_STRING_TYPE_NAME:
				if(context == grammarAccess.getBit_String_Type_NameRule() ||
				   context == grammarAccess.getElementary_Type_NameRule() ||
				   context == grammarAccess.getNon_Generic_Type_NameRule()) {
					sequence_Bit_String_Type_Name(context, (Bit_String_Type_Name) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.BOOLEAN_LITERAL:
				if(context == grammarAccess.getArray_Initial_ElementsRule() ||
				   context == grammarAccess.getBoolean_LiteralRule() ||
				   context == grammarAccess.getConstantRule() ||
				   context == grammarAccess.getPrimary_ExpressionRule()) {
					sequence_Boolean_Literal(context, (Boolean_Literal) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.CASE_ELEMENT:
				if(context == grammarAccess.getCase_ElementRule()) {
					sequence_Case_Element(context, (Case_Element) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.CASE_ELSE:
				if(context == grammarAccess.getCase_ElseRule()) {
					sequence_Case_Else(context, (Case_Else) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.CASE_STATEMENT:
				if(context == grammarAccess.getCase_StatementRule() ||
				   context == grammarAccess.getSelection_StatementRule() ||
				   context == grammarAccess.getStatementRule()) {
					sequence_Case_Statement(context, (Case_Statement) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.COMPARISON:
				if(context == grammarAccess.getAnd_ExpressionRule() ||
				   context == grammarAccess.getAnd_ExpressionAccess().getAnd_ExpressionLeftAction_1_0() ||
				   context == grammarAccess.getComparisonRule() ||
				   context == grammarAccess.getComparisonAccess().getComparisonLeftAction_1_0() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getExpressionLeftAction_1_0() ||
				   context == grammarAccess.getPrimary_ExpressionRule() ||
				   context == grammarAccess.getXor_ExpressionRule() ||
				   context == grammarAccess.getXor_ExpressionAccess().getXor_ExpressionLeftAction_1_0()) {
					sequence_Comparison(context, (Comparison) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.DBYTE_STRING_SPEC:
				if(context == grammarAccess.getD_Byte_String_SpecRule()) {
					sequence_D_Byte_String_Spec(context, (D_Byte_String_Spec) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.DATE:
				if(context == grammarAccess.getArray_Initial_ElementsRule() ||
				   context == grammarAccess.getConstantRule() ||
				   context == grammarAccess.getDateRule() ||
				   context == grammarAccess.getPrimary_ExpressionRule() ||
				   context == grammarAccess.getTime_LiteralRule()) {
					sequence_Date(context, (Date) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.DATE_AND_TIME:
				if(context == grammarAccess.getArray_Initial_ElementsRule() ||
				   context == grammarAccess.getConstantRule() ||
				   context == grammarAccess.getDate_And_TimeRule() ||
				   context == grammarAccess.getPrimary_ExpressionRule() ||
				   context == grammarAccess.getTime_LiteralRule()) {
					sequence_Date_And_Time(context, (Date_And_Time) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.DATE_TYPE_NAME:
				if(context == grammarAccess.getDate_Type_NameRule() ||
				   context == grammarAccess.getElementary_Type_NameRule() ||
				   context == grammarAccess.getNon_Generic_Type_NameRule()) {
					sequence_Date_Type_Name(context, (Date_Type_Name) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.DOUBLE_BYTE_CHARACTER_STRING_LITERAL:
				if(context == grammarAccess.getArray_Initial_ElementsRule() ||
				   context == grammarAccess.getCharacter_StringRule() ||
				   context == grammarAccess.getConstantRule() ||
				   context == grammarAccess.getDouble_Byte_Character_String_LiteralRule() ||
				   context == grammarAccess.getPrimary_ExpressionRule()) {
					sequence_Double_Byte_Character_String_Literal(context, (Double_Byte_Character_String_Literal) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.DURATION:
				if(context == grammarAccess.getArray_Initial_ElementsRule() ||
				   context == grammarAccess.getConstantRule() ||
				   context == grammarAccess.getDurationRule() ||
				   context == grammarAccess.getPrimary_ExpressionRule() ||
				   context == grammarAccess.getTime_LiteralRule()) {
					sequence_Duration(context, (Duration) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.ELSE_IF_STATEMENT:
				if(context == grammarAccess.getElseIf_StatementRule()) {
					sequence_ElseIf_Statement(context, (ElseIf_Statement) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.EQU_EXPRESSION:
				if(context == grammarAccess.getAnd_ExpressionRule() ||
				   context == grammarAccess.getAnd_ExpressionAccess().getAnd_ExpressionLeftAction_1_0() ||
				   context == grammarAccess.getComparisonRule() ||
				   context == grammarAccess.getComparisonAccess().getComparisonLeftAction_1_0() ||
				   context == grammarAccess.getEqu_ExpressionRule() ||
				   context == grammarAccess.getEqu_ExpressionAccess().getEqu_ExpressionLeftAction_1_0() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getExpressionLeftAction_1_0() ||
				   context == grammarAccess.getPrimary_ExpressionRule() ||
				   context == grammarAccess.getXor_ExpressionRule() ||
				   context == grammarAccess.getXor_ExpressionAccess().getXor_ExpressionLeftAction_1_0()) {
					sequence_Equ_Expression(context, (Equ_Expression) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.EXPRESSION:
				if(context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getExpressionLeftAction_1_0() ||
				   context == grammarAccess.getPrimary_ExpressionRule()) {
					sequence_Expression_Unary_Expression(context, (Expression) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getAdd_ExpressionRule() ||
				   context == grammarAccess.getAdd_ExpressionAccess().getAdd_ExpressionLeftAction_1_0() ||
				   context == grammarAccess.getAnd_ExpressionRule() ||
				   context == grammarAccess.getAnd_ExpressionAccess().getAnd_ExpressionLeftAction_1_0() ||
				   context == grammarAccess.getComparisonRule() ||
				   context == grammarAccess.getComparisonAccess().getComparisonLeftAction_1_0() ||
				   context == grammarAccess.getEqu_ExpressionRule() ||
				   context == grammarAccess.getEqu_ExpressionAccess().getEqu_ExpressionLeftAction_1_0() ||
				   context == grammarAccess.getPower_ExpressionRule() ||
				   context == grammarAccess.getPower_ExpressionAccess().getPower_ExpressionLeftAction_1_0() ||
				   context == grammarAccess.getTermRule() ||
				   context == grammarAccess.getTermAccess().getTermLeftAction_1_0() ||
				   context == grammarAccess.getUnary_ExpressionRule() ||
				   context == grammarAccess.getXor_ExpressionRule() ||
				   context == grammarAccess.getXor_ExpressionAccess().getXor_ExpressionLeftAction_1_0()) {
					sequence_Unary_Expression(context, (Expression) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.FOR_STATEMENT:
				if(context == grammarAccess.getFor_StatementRule() ||
				   context == grammarAccess.getStatementRule()) {
					sequence_For_Statement(context, (For_Statement) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.FUNC_CALL:
				if(context == grammarAccess.getFunc_CallRule() ||
				   context == grammarAccess.getPrimary_ExpressionRule()) {
					sequence_Func_Call(context, (Func_Call) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.IF_STATEMENT:
				if(context == grammarAccess.getIf_StatementRule() ||
				   context == grammarAccess.getSelection_StatementRule() ||
				   context == grammarAccess.getStatementRule()) {
					sequence_If_Statement(context, (If_Statement) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.INTEGER_LITERAL:
				if(context == grammarAccess.getArray_Initial_ElementsRule() ||
				   context == grammarAccess.getConstantRule() ||
				   context == grammarAccess.getIntegerLiteralRule() ||
				   context == grammarAccess.getNumeric_LiteralRule() ||
				   context == grammarAccess.getPrimary_ExpressionRule()) {
					sequence_IntegerLiteral(context, (IntegerLiteral) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.ITERATION_CONTROL_STMT:
				if(context == grammarAccess.getIterationControl_StmtRule() ||
				   context == grammarAccess.getStatementRule()) {
					sequence_IterationControl_Stmt(context, (IterationControl_Stmt) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.PARAM_ASSIGN:
				if(context == grammarAccess.getParam_AssignRule()) {
					sequence_Param_Assign(context, (Param_Assign) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.PARAMETER:
				if(context == grammarAccess.getParameterRule()) {
					sequence_Parameter(context, (Parameter) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.POWER_EXPRESSION:
				if(context == grammarAccess.getAdd_ExpressionRule() ||
				   context == grammarAccess.getAdd_ExpressionAccess().getAdd_ExpressionLeftAction_1_0() ||
				   context == grammarAccess.getAnd_ExpressionRule() ||
				   context == grammarAccess.getAnd_ExpressionAccess().getAnd_ExpressionLeftAction_1_0() ||
				   context == grammarAccess.getComparisonRule() ||
				   context == grammarAccess.getComparisonAccess().getComparisonLeftAction_1_0() ||
				   context == grammarAccess.getEqu_ExpressionRule() ||
				   context == grammarAccess.getEqu_ExpressionAccess().getEqu_ExpressionLeftAction_1_0() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getExpressionLeftAction_1_0() ||
				   context == grammarAccess.getPower_ExpressionRule() ||
				   context == grammarAccess.getPower_ExpressionAccess().getPower_ExpressionLeftAction_1_0() ||
				   context == grammarAccess.getPrimary_ExpressionRule() ||
				   context == grammarAccess.getTermRule() ||
				   context == grammarAccess.getTermAccess().getTermLeftAction_1_0() ||
				   context == grammarAccess.getXor_ExpressionRule() ||
				   context == grammarAccess.getXor_ExpressionAccess().getXor_ExpressionLeftAction_1_0()) {
					sequence_Power_Expression(context, (Power_Expression) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.REAL_LITERAL:
				if(context == grammarAccess.getArray_Initial_ElementsRule() ||
				   context == grammarAccess.getConstantRule() ||
				   context == grammarAccess.getNumeric_LiteralRule() ||
				   context == grammarAccess.getPrimary_ExpressionRule() ||
				   context == grammarAccess.getReal_LiteralRule()) {
					sequence_Real_Literal(context, (Real_Literal) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.REAL_TYPE_NAME:
				if(context == grammarAccess.getElementary_Type_NameRule() ||
				   context == grammarAccess.getNon_Generic_Type_NameRule() ||
				   context == grammarAccess.getNumeric_Type_NameRule() ||
				   context == grammarAccess.getReal_Type_NameRule()) {
					sequence_Real_Type_Name(context, (Real_Type_Name) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.REPEAT_STATEMENT:
				if(context == grammarAccess.getRepeat_StatementRule() ||
				   context == grammarAccess.getStatementRule()) {
					sequence_Repeat_Statement(context, (Repeat_Statement) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.SBYTE_STRING_SPEC:
				if(context == grammarAccess.getS_Byte_String_SpecRule()) {
					sequence_S_Byte_String_Spec(context, (S_Byte_String_Spec) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.SIGNED_INTEGER_TYPE_NAME:
				if(context == grammarAccess.getElementary_Type_NameRule() ||
				   context == grammarAccess.getInteger_Type_NameRule() ||
				   context == grammarAccess.getNon_Generic_Type_NameRule() ||
				   context == grammarAccess.getNumeric_Type_NameRule() ||
				   context == grammarAccess.getSigned_Integer_Type_NameRule()) {
					sequence_Signed_Integer_Type_Name(context, (Signed_Integer_Type_Name) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.SIMPLE_SPEC_INIT:
				if(context == grammarAccess.getSimple_Spec_InitRule()) {
					sequence_Simple_Spec_Init(context, (Simple_Spec_Init) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.SINGLE_BYTE_CHARACTER_STRING_LITERAL:
				if(context == grammarAccess.getArray_Initial_ElementsRule() ||
				   context == grammarAccess.getCharacter_StringRule() ||
				   context == grammarAccess.getConstantRule() ||
				   context == grammarAccess.getPrimary_ExpressionRule() ||
				   context == grammarAccess.getSingle_Byte_Character_String_LiteralRule()) {
					sequence_Single_Byte_Character_String_Literal(context, (Single_Byte_Character_String_Literal) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.STATEMENT_LIST:
				if(context == grammarAccess.getStatementListRule()) {
					sequence_StatementList(context, (StatementList) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.STRUCTURE_SPEC:
				if(context == grammarAccess.getStructure_SpecRule()) {
					sequence_Structure_Spec(context, (Structure_Spec) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.SUBPROG_CTRL_STMT:
				if(context == grammarAccess.getStatementRule() ||
				   context == grammarAccess.getSubprog_Ctrl_StmtRule()) {
					sequence_Subprog_Ctrl_Stmt(context, (Subprog_Ctrl_Stmt) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.SUBRANGE:
				if(context == grammarAccess.getSubrangeRule()) {
					sequence_Subrange(context, (Subrange) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.TERM:
				if(context == grammarAccess.getAdd_ExpressionRule() ||
				   context == grammarAccess.getAdd_ExpressionAccess().getAdd_ExpressionLeftAction_1_0() ||
				   context == grammarAccess.getAnd_ExpressionRule() ||
				   context == grammarAccess.getAnd_ExpressionAccess().getAnd_ExpressionLeftAction_1_0() ||
				   context == grammarAccess.getComparisonRule() ||
				   context == grammarAccess.getComparisonAccess().getComparisonLeftAction_1_0() ||
				   context == grammarAccess.getEqu_ExpressionRule() ||
				   context == grammarAccess.getEqu_ExpressionAccess().getEqu_ExpressionLeftAction_1_0() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getExpressionLeftAction_1_0() ||
				   context == grammarAccess.getPrimary_ExpressionRule() ||
				   context == grammarAccess.getTermRule() ||
				   context == grammarAccess.getTermAccess().getTermLeftAction_1_0() ||
				   context == grammarAccess.getXor_ExpressionRule() ||
				   context == grammarAccess.getXor_ExpressionAccess().getXor_ExpressionLeftAction_1_0()) {
					sequence_Term(context, (Term) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.TIME_OF_DAY:
				if(context == grammarAccess.getArray_Initial_ElementsRule() ||
				   context == grammarAccess.getConstantRule() ||
				   context == grammarAccess.getPrimary_ExpressionRule() ||
				   context == grammarAccess.getTime_LiteralRule() ||
				   context == grammarAccess.getTime_Of_DayRule()) {
					sequence_Time_Of_Day(context, (Time_Of_Day) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.TIME_TYPE_NAME:
				if(context == grammarAccess.getElementary_Type_NameRule() ||
				   context == grammarAccess.getNon_Generic_Type_NameRule() ||
				   context == grammarAccess.getTime_Type_NameRule()) {
					sequence_Time_Type_Name(context, (Time_Type_Name) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.UNSIGNED_INTEGER_TYPE_NAME:
				if(context == grammarAccess.getElementary_Type_NameRule() ||
				   context == grammarAccess.getInteger_Type_NameRule() ||
				   context == grammarAccess.getNon_Generic_Type_NameRule() ||
				   context == grammarAccess.getNumeric_Type_NameRule() ||
				   context == grammarAccess.getUnsigned_Integer_Type_NameRule()) {
					sequence_Unsigned_Integer_Type_Name(context, (Unsigned_Integer_Type_Name) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.VAR_DECLARATION:
				if(context == grammarAccess.getVarDeclarationRule()) {
					sequence_VarDeclaration(context, (VarDeclaration) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.VAR_VARIABLE:
				if(context == grammarAccess.getPrimary_ExpressionRule() ||
				   context == grammarAccess.getVar_VariableRule() ||
				   context == grammarAccess.getVariableRule()) {
					sequence_Var_Variable(context, (Var_Variable) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.WHILE_STATEMENT:
				if(context == grammarAccess.getStatementRule() ||
				   context == grammarAccess.getWhile_StatementRule()) {
					sequence_While_Statement(context, (While_Statement) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.XOR_EXPRESSION:
				if(context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getExpressionLeftAction_1_0() ||
				   context == grammarAccess.getPrimary_ExpressionRule() ||
				   context == grammarAccess.getXor_ExpressionRule() ||
				   context == grammarAccess.getXor_ExpressionAccess().getXor_ExpressionLeftAction_1_0()) {
					sequence_Xor_Expression(context, (Xor_Expression) semanticObject); 
					return; 
				}
				else break;
			case StructuredTextPackage.STRUCTURED_TEXT_ALGORITHM:
				if(context == grammarAccess.getStructuredTextAlgorithmRule()) {
					sequence_structuredTextAlgorithm(context, (structuredTextAlgorithm) semanticObject); 
					return; 
				}
				else break;
			}
		if (errorAcceptor != null) errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Constraint:
	 *     (name=ID type=[AdapterDefinition|ID] (parameters+=Parameter parameters+=Parameter*)?)
	 */
	protected void sequence_AdapterDeclaration(EObject context, AdapterDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID varDeclarations+=VarDeclaration* varDeclarations+=VarDeclaration*)
	 */
	protected void sequence_AdapterDefinition(EObject context, AdapterDefinition semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (adapter=[AdapterDeclaration|ID] var=[VarDeclaration|ID])
	 */
	protected void sequence_Adapter_Variable(EObject context, Adapter_Variable semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=Add_Expression_Add_Expression_1_0 (operator='+' | operator='-') right=Term)
	 */
	protected void sequence_Add_Expression(EObject context, Add_Expression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=And_Expression_And_Expression_1_0 (operator='&' | operator='AND') right=Comparison)
	 */
	protected void sequence_And_Expression(EObject context, And_Expression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (num=INT arrayElement=Constant?)
	 */
	protected void sequence_Array_Initial_Elements(EObject context, Array_Initial_Elements semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (initElement+=Array_Initial_Elements initElement+=Array_Initial_Elements*)
	 */
	protected void sequence_Array_Initialization(EObject context, Array_Initialization semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (subrange+=Subrange subrange+=Subrange* type=Non_Generic_Type_Name initialization=Array_Initialization?)
	 */
	protected void sequence_Array_Spec_Init_Array_Specification(EObject context, Array_Specification semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (subrange+=Subrange subrange+=Subrange* type=Non_Generic_Type_Name)
	 */
	protected void sequence_Array_Specification(EObject context, Array_Specification semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (var=[VarDeclaration|ID] num=Expression)
	 */
	protected void sequence_Array_Variable(EObject context, Array_Variable semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (variable=Variable expression=Expression)
	 */
	protected void sequence_Assignment_Statement(EObject context, Assignment_Statement semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, StructuredTextPackage.Literals.ASSIGNMENT_STATEMENT__VARIABLE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, StructuredTextPackage.Literals.ASSIGNMENT_STATEMENT__VARIABLE));
			if(transientValues.isValueTransient(semanticObject, StructuredTextPackage.Literals.ASSIGNMENT_STATEMENT__EXPRESSION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, StructuredTextPackage.Literals.ASSIGNMENT_STATEMENT__EXPRESSION));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getAssignment_StatementAccess().getVariableVariableParserRuleCall_0_0(), semanticObject.getVariable());
		feeder.accept(grammarAccess.getAssignment_StatementAccess().getExpressionExpressionParserRuleCall_2_0(), semanticObject.getExpression());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     {Bit_String_Type_Name}
	 */
	protected void sequence_Bit_String_Type_Name(EObject context, Bit_String_Type_Name semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (type='BOOL#'? (value='TRUE' | value='true' | value='FALSE' | value='false'))
	 */
	protected void sequence_Boolean_Literal(EObject context, Boolean_Literal semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (case=INT statements=StatementList)
	 */
	protected void sequence_Case_Element(EObject context, Case_Element semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, StructuredTextPackage.Literals.CASE_ELEMENT__CASE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, StructuredTextPackage.Literals.CASE_ELEMENT__CASE));
			if(transientValues.isValueTransient(semanticObject, StructuredTextPackage.Literals.CASE_ELEMENT__STATEMENTS) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, StructuredTextPackage.Literals.CASE_ELEMENT__STATEMENTS));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getCase_ElementAccess().getCaseINTTerminalRuleCall_0_0(), semanticObject.getCase());
		feeder.accept(grammarAccess.getCase_ElementAccess().getStatementsStatementListParserRuleCall_2_0(), semanticObject.getStatements());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     statements=StatementList
	 */
	protected void sequence_Case_Else(EObject context, Case_Else semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, StructuredTextPackage.Literals.CASE_ELSE__STATEMENTS) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, StructuredTextPackage.Literals.CASE_ELSE__STATEMENTS));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getCase_ElseAccess().getStatementsStatementListParserRuleCall_2_0(), semanticObject.getStatements());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (expression=Expression caseElements+=Case_Element+ caseElse=Case_Else?)
	 */
	protected void sequence_Case_Statement(EObject context, Case_Statement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=Comparison_Comparison_1_0 (operator='=' | operator='<>') right=Equ_Expression)
	 */
	protected void sequence_Comparison(EObject context, Comparison semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (type='WSTRING' size=INT? string=Double_Byte_Character_String_Literal?)
	 */
	protected void sequence_D_Byte_String_Spec(EObject context, D_Byte_String_Spec semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     value=Date_And_Time_Literal
	 */
	protected void sequence_Date_And_Time(EObject context, Date_And_Time semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     value=Date_Literal
	 */
	protected void sequence_Date(EObject context, Date semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     {Date_Type_Name}
	 */
	protected void sequence_Date_Type_Name(EObject context, Date_Type_Name semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (type='WSTRING#'? value=D_BYTE_CHAR_STRING)
	 */
	protected void sequence_Double_Byte_Character_String_Literal(EObject context, Double_Byte_Character_String_Literal semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     value=TIME
	 */
	protected void sequence_Duration(EObject context, Duration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (expression=Expression statements=StatementList)
	 */
	protected void sequence_ElseIf_Statement(EObject context, ElseIf_Statement semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, StructuredTextPackage.Literals.ELSE_IF_STATEMENT__EXPRESSION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, StructuredTextPackage.Literals.ELSE_IF_STATEMENT__EXPRESSION));
			if(transientValues.isValueTransient(semanticObject, StructuredTextPackage.Literals.ELSE_IF_STATEMENT__STATEMENTS) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, StructuredTextPackage.Literals.ELSE_IF_STATEMENT__STATEMENTS));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getElseIf_StatementAccess().getExpressionExpressionParserRuleCall_1_0(), semanticObject.getExpression());
		feeder.accept(grammarAccess.getElseIf_StatementAccess().getStatementsStatementListParserRuleCall_3_0(), semanticObject.getStatements());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (left=Equ_Expression_Equ_Expression_1_0 (operator='<' | operator='<=' | operator='>' | operator='>=') right=Add_Expression)
	 */
	protected void sequence_Equ_Expression(EObject context, Equ_Expression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     ((left=Expression_Expression_1_0 operator='OR' right=Xor_Expression) | (operator='NOT'? expression=Primary_Expression))
	 */
	protected void sequence_Expression_Unary_Expression(EObject context, Expression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (controlVariable=[VarDeclaration|ID] expressionFrom=Expression expressionTo=Expression expressionBy=Expression? statements=StatementList)
	 */
	protected void sequence_For_Statement(EObject context, For_Statement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID (paramslist+=Param_Assign paramslist+=Param_Assign*)?)
	 */
	protected void sequence_Func_Call(EObject context, Func_Call semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (expression=Expression statments=StatementList elseif+=ElseIf_Statement? elseStatements=StatementList?)
	 */
	protected void sequence_If_Statement(EObject context, If_Statement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         (
	 *             type='UINT#' | 
	 *             type='USINT#' | 
	 *             type='UDINT#' | 
	 *             type='ULINT#' | 
	 *             type='BYTE#' | 
	 *             type='WORD#' | 
	 *             type='DWORD#' | 
	 *             type='LWORD#' | 
	 *             type='INT#' | 
	 *             type='SINT#' | 
	 *             type='DINT#' | 
	 *             type='LINT#'
	 *         )? 
	 *         (value=SignedInteger | value=BINARY_INTEGER_VALUE | value=OCTAL_INTEGER_VALUE | value=HEX_INTEGER_VALUE)
	 *     )
	 */
	protected void sequence_IntegerLiteral(EObject context, IntegerLiteral semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (statement='EXIT' | statement='CONTINUE' | statement='RETURN')
	 */
	protected void sequence_IterationControl_Stmt(EObject context, IterationControl_Stmt semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     ((varname=ID? expr=Expression) | (varname=ID expr=Variable))
	 */
	protected void sequence_Param_Assign(EObject context, Param_Assign semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (param=[VarDeclaration|ID] (value=Constant | value=Array_Initialization))
	 */
	protected void sequence_Parameter(EObject context, Parameter semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=Power_Expression_Power_Expression_1_0 operator='**' right=Unary_Expression)
	 */
	protected void sequence_Power_Expression(EObject context, Power_Expression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     ((type='REAL#' | type='LREAL#')? value=REALVALUE exponent=SignedInteger?)
	 */
	protected void sequence_Real_Literal(EObject context, Real_Literal semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     {Real_Type_Name}
	 */
	protected void sequence_Real_Type_Name(EObject context, Real_Type_Name semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (statements=StatementList expression=Expression)
	 */
	protected void sequence_Repeat_Statement(EObject context, Repeat_Statement semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, StructuredTextPackage.Literals.REPEAT_STATEMENT__STATEMENTS) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, StructuredTextPackage.Literals.REPEAT_STATEMENT__STATEMENTS));
			if(transientValues.isValueTransient(semanticObject, StructuredTextPackage.Literals.REPEAT_STATEMENT__EXPRESSION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, StructuredTextPackage.Literals.REPEAT_STATEMENT__EXPRESSION));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getRepeat_StatementAccess().getStatementsStatementListParserRuleCall_1_0(), semanticObject.getStatements());
		feeder.accept(grammarAccess.getRepeat_StatementAccess().getExpressionExpressionParserRuleCall_3_0(), semanticObject.getExpression());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (type='STRING' size=INT? string=Single_Byte_Character_String_Literal?)
	 */
	protected void sequence_S_Byte_String_Spec(EObject context, S_Byte_String_Spec semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     {Signed_Integer_Type_Name}
	 */
	protected void sequence_Signed_Integer_Type_Name(EObject context, Signed_Integer_Type_Name semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (type=Elementary_Type_Name constant=Constant?)
	 */
	protected void sequence_Simple_Spec_Init(EObject context, Simple_Spec_Init semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (type='STRING#'? value=S_BYTE_CHAR_STRING)
	 */
	protected void sequence_Single_Byte_Character_String_Literal(EObject context, Single_Byte_Character_String_Literal semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (statements+=Statement*)
	 */
	protected void sequence_StatementList(EObject context, StatementList semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID element+=VarDeclaration*)
	 */
	protected void sequence_Structure_Spec(EObject context, Structure_Spec semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     call=Func_Call
	 */
	protected void sequence_Subprog_Ctrl_Stmt(EObject context, Subprog_Ctrl_Stmt semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, StructuredTextPackage.Literals.SUBPROG_CTRL_STMT__CALL) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, StructuredTextPackage.Literals.SUBPROG_CTRL_STMT__CALL));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getSubprog_Ctrl_StmtAccess().getCallFunc_CallParserRuleCall_0(), semanticObject.getCall());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (min=SignedInteger max=SignedInteger)
	 */
	protected void sequence_Subrange(EObject context, Subrange semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, StructuredTextPackage.Literals.SUBRANGE__MIN) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, StructuredTextPackage.Literals.SUBRANGE__MIN));
			if(transientValues.isValueTransient(semanticObject, StructuredTextPackage.Literals.SUBRANGE__MAX) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, StructuredTextPackage.Literals.SUBRANGE__MAX));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getSubrangeAccess().getMinSignedIntegerParserRuleCall_0_0(), semanticObject.getMin());
		feeder.accept(grammarAccess.getSubrangeAccess().getMaxSignedIntegerParserRuleCall_2_0(), semanticObject.getMax());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (left=Term_Term_1_0 (operator='*' | operator='/' | operator='MOD') right=Power_Expression)
	 */
	protected void sequence_Term(EObject context, Term semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     value=Daytime
	 */
	protected void sequence_Time_Of_Day(EObject context, Time_Of_Day semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     {Time_Type_Name}
	 */
	protected void sequence_Time_Type_Name(EObject context, Time_Type_Name semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (operator='NOT'? expression=Primary_Expression)
	 */
	protected void sequence_Unary_Expression(EObject context, Expression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     {Unsigned_Integer_Type_Name}
	 */
	protected void sequence_Unsigned_Integer_Type_Name(EObject context, Unsigned_Integer_Type_Name semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         name=ID 
	 *         (specification=Simple_Spec_Init | specification=Array_Spec_Init | specification=S_Byte_String_Spec | specification=D_Byte_String_Spec)
	 *     )
	 */
	protected void sequence_VarDeclaration(EObject context, VarDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     var=[VarDeclaration|ID]
	 */
	protected void sequence_Var_Variable(EObject context, Var_Variable semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (expression=Expression statements=StatementList)
	 */
	protected void sequence_While_Statement(EObject context, While_Statement semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, StructuredTextPackage.Literals.WHILE_STATEMENT__EXPRESSION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, StructuredTextPackage.Literals.WHILE_STATEMENT__EXPRESSION));
			if(transientValues.isValueTransient(semanticObject, StructuredTextPackage.Literals.WHILE_STATEMENT__STATEMENTS) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, StructuredTextPackage.Literals.WHILE_STATEMENT__STATEMENTS));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getWhile_StatementAccess().getExpressionExpressionParserRuleCall_1_0(), semanticObject.getExpression());
		feeder.accept(grammarAccess.getWhile_StatementAccess().getStatementsStatementListParserRuleCall_3_0(), semanticObject.getStatements());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (left=Xor_Expression_Xor_Expression_1_0 operator='XOR' right=And_Expression)
	 */
	protected void sequence_Xor_Expression(EObject context, Xor_Expression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         adapters+=AdapterDefinition* 
	 *         varDeclarations+=VarDeclaration* 
	 *         varDeclarations+=VarDeclaration* 
	 *         varDeclarations+=VarDeclaration* 
	 *         varDeclarations+=AdapterDeclaration* 
	 *         varDeclarations+=AdapterDeclaration* 
	 *         varDeclarations+=VarDeclaration* 
	 *         statements=StatementList
	 *     )
	 */
	protected void sequence_structuredTextAlgorithm(EObject context, structuredTextAlgorithm semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
}
