/**
 */
package org.fordiac.ide.model.structuredtext.structuredText.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.fordiac.ide.model.structuredtext.structuredText.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage
 * @generated
 */
public class StructuredTextAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static StructuredTextPackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StructuredTextAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = StructuredTextPackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected StructuredTextSwitch<Adapter> modelSwitch =
    new StructuredTextSwitch<Adapter>()
    {
      @Override
      public Adapter casestructuredTextAlgorithm(structuredTextAlgorithm object)
      {
        return createstructuredTextAlgorithmAdapter();
      }
      @Override
      public Adapter caseVarDeclaration(VarDeclaration object)
      {
        return createVarDeclarationAdapter();
      }
      @Override
      public Adapter caseAdapterDefinition(AdapterDefinition object)
      {
        return createAdapterDefinitionAdapter();
      }
      @Override
      public Adapter caseAdapterDeclaration(AdapterDeclaration object)
      {
        return createAdapterDeclarationAdapter();
      }
      @Override
      public Adapter caseParameter(Parameter object)
      {
        return createParameterAdapter();
      }
      @Override
      public Adapter caseSimple_Spec_Init(Simple_Spec_Init object)
      {
        return createSimple_Spec_InitAdapter();
      }
      @Override
      public Adapter caseArray_Spec_Init(Array_Spec_Init object)
      {
        return createArray_Spec_InitAdapter();
      }
      @Override
      public Adapter caseArray_Specification(Array_Specification object)
      {
        return createArray_SpecificationAdapter();
      }
      @Override
      public Adapter caseSubrange(Subrange object)
      {
        return createSubrangeAdapter();
      }
      @Override
      public Adapter caseArray_Initialization(Array_Initialization object)
      {
        return createArray_InitializationAdapter();
      }
      @Override
      public Adapter caseArray_Initial_Elements(Array_Initial_Elements object)
      {
        return createArray_Initial_ElementsAdapter();
      }
      @Override
      public Adapter caseStructure_Spec(Structure_Spec object)
      {
        return createStructure_SpecAdapter();
      }
      @Override
      public Adapter caseS_Byte_String_Spec(S_Byte_String_Spec object)
      {
        return createS_Byte_String_SpecAdapter();
      }
      @Override
      public Adapter caseD_Byte_String_Spec(D_Byte_String_Spec object)
      {
        return createD_Byte_String_SpecAdapter();
      }
      @Override
      public Adapter caseStatementList(StatementList object)
      {
        return createStatementListAdapter();
      }
      @Override
      public Adapter caseStatement(Statement object)
      {
        return createStatementAdapter();
      }
      @Override
      public Adapter caseAssignment_Statement(Assignment_Statement object)
      {
        return createAssignment_StatementAdapter();
      }
      @Override
      public Adapter caseVariable(Variable object)
      {
        return createVariableAdapter();
      }
      @Override
      public Adapter caseVar_Variable(Var_Variable object)
      {
        return createVar_VariableAdapter();
      }
      @Override
      public Adapter caseAdapter_Variable(Adapter_Variable object)
      {
        return createAdapter_VariableAdapter();
      }
      @Override
      public Adapter caseArray_Variable(Array_Variable object)
      {
        return createArray_VariableAdapter();
      }
      @Override
      public Adapter caseSelection_Statement(Selection_Statement object)
      {
        return createSelection_StatementAdapter();
      }
      @Override
      public Adapter caseIf_Statement(If_Statement object)
      {
        return createIf_StatementAdapter();
      }
      @Override
      public Adapter caseElseIf_Statement(ElseIf_Statement object)
      {
        return createElseIf_StatementAdapter();
      }
      @Override
      public Adapter caseCase_Statement(Case_Statement object)
      {
        return createCase_StatementAdapter();
      }
      @Override
      public Adapter caseCase_Element(Case_Element object)
      {
        return createCase_ElementAdapter();
      }
      @Override
      public Adapter caseCase_Else(Case_Else object)
      {
        return createCase_ElseAdapter();
      }
      @Override
      public Adapter caseIterationControl_Stmt(IterationControl_Stmt object)
      {
        return createIterationControl_StmtAdapter();
      }
      @Override
      public Adapter caseFor_Statement(For_Statement object)
      {
        return createFor_StatementAdapter();
      }
      @Override
      public Adapter caseRepeat_Statement(Repeat_Statement object)
      {
        return createRepeat_StatementAdapter();
      }
      @Override
      public Adapter caseWhile_Statement(While_Statement object)
      {
        return createWhile_StatementAdapter();
      }
      @Override
      public Adapter caseSubprog_Ctrl_Stmt(Subprog_Ctrl_Stmt object)
      {
        return createSubprog_Ctrl_StmtAdapter();
      }
      @Override
      public Adapter caseExpression(Expression object)
      {
        return createExpressionAdapter();
      }
      @Override
      public Adapter caseFunc_Call(Func_Call object)
      {
        return createFunc_CallAdapter();
      }
      @Override
      public Adapter caseParam_Assign(Param_Assign object)
      {
        return createParam_AssignAdapter();
      }
      @Override
      public Adapter caseConstant(Constant object)
      {
        return createConstantAdapter();
      }
      @Override
      public Adapter caseNumeric_Literal(Numeric_Literal object)
      {
        return createNumeric_LiteralAdapter();
      }
      @Override
      public Adapter caseIntegerLiteral(IntegerLiteral object)
      {
        return createIntegerLiteralAdapter();
      }
      @Override
      public Adapter caseReal_Literal(Real_Literal object)
      {
        return createReal_LiteralAdapter();
      }
      @Override
      public Adapter caseBoolean_Literal(Boolean_Literal object)
      {
        return createBoolean_LiteralAdapter();
      }
      @Override
      public Adapter caseCharacter_String(Character_String object)
      {
        return createCharacter_StringAdapter();
      }
      @Override
      public Adapter caseSingle_Byte_Character_String_Literal(Single_Byte_Character_String_Literal object)
      {
        return createSingle_Byte_Character_String_LiteralAdapter();
      }
      @Override
      public Adapter caseDouble_Byte_Character_String_Literal(Double_Byte_Character_String_Literal object)
      {
        return createDouble_Byte_Character_String_LiteralAdapter();
      }
      @Override
      public Adapter caseTime_Literal(Time_Literal object)
      {
        return createTime_LiteralAdapter();
      }
      @Override
      public Adapter caseDuration(Duration object)
      {
        return createDurationAdapter();
      }
      @Override
      public Adapter caseTime_Of_Day(Time_Of_Day object)
      {
        return createTime_Of_DayAdapter();
      }
      @Override
      public Adapter caseDate(Date object)
      {
        return createDateAdapter();
      }
      @Override
      public Adapter caseDate_And_Time(Date_And_Time object)
      {
        return createDate_And_TimeAdapter();
      }
      @Override
      public Adapter caseNon_Generic_Type_Name(Non_Generic_Type_Name object)
      {
        return createNon_Generic_Type_NameAdapter();
      }
      @Override
      public Adapter caseElementary_Type_Name(Elementary_Type_Name object)
      {
        return createElementary_Type_NameAdapter();
      }
      @Override
      public Adapter caseNumeric_Type_Name(Numeric_Type_Name object)
      {
        return createNumeric_Type_NameAdapter();
      }
      @Override
      public Adapter caseInteger_Type_Name(Integer_Type_Name object)
      {
        return createInteger_Type_NameAdapter();
      }
      @Override
      public Adapter caseSigned_Integer_Type_Name(Signed_Integer_Type_Name object)
      {
        return createSigned_Integer_Type_NameAdapter();
      }
      @Override
      public Adapter caseUnsigned_Integer_Type_Name(Unsigned_Integer_Type_Name object)
      {
        return createUnsigned_Integer_Type_NameAdapter();
      }
      @Override
      public Adapter caseReal_Type_Name(Real_Type_Name object)
      {
        return createReal_Type_NameAdapter();
      }
      @Override
      public Adapter caseBit_String_Type_Name(Bit_String_Type_Name object)
      {
        return createBit_String_Type_NameAdapter();
      }
      @Override
      public Adapter caseTime_Type_Name(Time_Type_Name object)
      {
        return createTime_Type_NameAdapter();
      }
      @Override
      public Adapter caseDate_Type_Name(Date_Type_Name object)
      {
        return createDate_Type_NameAdapter();
      }
      @Override
      public Adapter caseXor_Expression(Xor_Expression object)
      {
        return createXor_ExpressionAdapter();
      }
      @Override
      public Adapter caseAnd_Expression(And_Expression object)
      {
        return createAnd_ExpressionAdapter();
      }
      @Override
      public Adapter caseComparison(Comparison object)
      {
        return createComparisonAdapter();
      }
      @Override
      public Adapter caseEqu_Expression(Equ_Expression object)
      {
        return createEqu_ExpressionAdapter();
      }
      @Override
      public Adapter caseAdd_Expression(Add_Expression object)
      {
        return createAdd_ExpressionAdapter();
      }
      @Override
      public Adapter caseTerm(Term object)
      {
        return createTermAdapter();
      }
      @Override
      public Adapter casePower_Expression(Power_Expression object)
      {
        return createPower_ExpressionAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.structuredTextAlgorithm <em>structured Text Algorithm</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.structuredTextAlgorithm
   * @generated
   */
  public Adapter createstructuredTextAlgorithmAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.VarDeclaration <em>Var Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.VarDeclaration
   * @generated
   */
  public Adapter createVarDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.AdapterDefinition <em>Adapter Definition</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.AdapterDefinition
   * @generated
   */
  public Adapter createAdapterDefinitionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.AdapterDeclaration <em>Adapter Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.AdapterDeclaration
   * @generated
   */
  public Adapter createAdapterDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Parameter <em>Parameter</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Parameter
   * @generated
   */
  public Adapter createParameterAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Simple_Spec_Init <em>Simple Spec Init</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Simple_Spec_Init
   * @generated
   */
  public Adapter createSimple_Spec_InitAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Spec_Init <em>Array Spec Init</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Array_Spec_Init
   * @generated
   */
  public Adapter createArray_Spec_InitAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Specification <em>Array Specification</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Array_Specification
   * @generated
   */
  public Adapter createArray_SpecificationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Subrange <em>Subrange</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Subrange
   * @generated
   */
  public Adapter createSubrangeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Initialization <em>Array Initialization</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Array_Initialization
   * @generated
   */
  public Adapter createArray_InitializationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Initial_Elements <em>Array Initial Elements</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Array_Initial_Elements
   * @generated
   */
  public Adapter createArray_Initial_ElementsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Structure_Spec <em>Structure Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Structure_Spec
   * @generated
   */
  public Adapter createStructure_SpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.S_Byte_String_Spec <em>SByte String Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.S_Byte_String_Spec
   * @generated
   */
  public Adapter createS_Byte_String_SpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.D_Byte_String_Spec <em>DByte String Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.D_Byte_String_Spec
   * @generated
   */
  public Adapter createD_Byte_String_SpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.StatementList <em>Statement List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.StatementList
   * @generated
   */
  public Adapter createStatementListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Statement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Statement
   * @generated
   */
  public Adapter createStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Assignment_Statement <em>Assignment Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Assignment_Statement
   * @generated
   */
  public Adapter createAssignment_StatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Variable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Variable
   * @generated
   */
  public Adapter createVariableAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Var_Variable <em>Var Variable</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Var_Variable
   * @generated
   */
  public Adapter createVar_VariableAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Adapter_Variable <em>Adapter Variable</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Adapter_Variable
   * @generated
   */
  public Adapter createAdapter_VariableAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Variable <em>Array Variable</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Array_Variable
   * @generated
   */
  public Adapter createArray_VariableAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Selection_Statement <em>Selection Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Selection_Statement
   * @generated
   */
  public Adapter createSelection_StatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.If_Statement <em>If Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.If_Statement
   * @generated
   */
  public Adapter createIf_StatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.ElseIf_Statement <em>Else If Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.ElseIf_Statement
   * @generated
   */
  public Adapter createElseIf_StatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Case_Statement <em>Case Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Case_Statement
   * @generated
   */
  public Adapter createCase_StatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Case_Element <em>Case Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Case_Element
   * @generated
   */
  public Adapter createCase_ElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Case_Else <em>Case Else</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Case_Else
   * @generated
   */
  public Adapter createCase_ElseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.IterationControl_Stmt <em>Iteration Control Stmt</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.IterationControl_Stmt
   * @generated
   */
  public Adapter createIterationControl_StmtAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.For_Statement <em>For Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.For_Statement
   * @generated
   */
  public Adapter createFor_StatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Repeat_Statement <em>Repeat Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Repeat_Statement
   * @generated
   */
  public Adapter createRepeat_StatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.While_Statement <em>While Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.While_Statement
   * @generated
   */
  public Adapter createWhile_StatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Subprog_Ctrl_Stmt <em>Subprog Ctrl Stmt</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Subprog_Ctrl_Stmt
   * @generated
   */
  public Adapter createSubprog_Ctrl_StmtAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Expression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Expression
   * @generated
   */
  public Adapter createExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Func_Call <em>Func Call</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Func_Call
   * @generated
   */
  public Adapter createFunc_CallAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Param_Assign <em>Param Assign</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Param_Assign
   * @generated
   */
  public Adapter createParam_AssignAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Constant <em>Constant</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Constant
   * @generated
   */
  public Adapter createConstantAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Numeric_Literal <em>Numeric Literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Numeric_Literal
   * @generated
   */
  public Adapter createNumeric_LiteralAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.IntegerLiteral <em>Integer Literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.IntegerLiteral
   * @generated
   */
  public Adapter createIntegerLiteralAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Real_Literal <em>Real Literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Real_Literal
   * @generated
   */
  public Adapter createReal_LiteralAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Boolean_Literal <em>Boolean Literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Boolean_Literal
   * @generated
   */
  public Adapter createBoolean_LiteralAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Character_String <em>Character String</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Character_String
   * @generated
   */
  public Adapter createCharacter_StringAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Single_Byte_Character_String_Literal <em>Single Byte Character String Literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Single_Byte_Character_String_Literal
   * @generated
   */
  public Adapter createSingle_Byte_Character_String_LiteralAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Double_Byte_Character_String_Literal <em>Double Byte Character String Literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Double_Byte_Character_String_Literal
   * @generated
   */
  public Adapter createDouble_Byte_Character_String_LiteralAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Time_Literal <em>Time Literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Time_Literal
   * @generated
   */
  public Adapter createTime_LiteralAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Duration <em>Duration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Duration
   * @generated
   */
  public Adapter createDurationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Time_Of_Day <em>Time Of Day</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Time_Of_Day
   * @generated
   */
  public Adapter createTime_Of_DayAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Date <em>Date</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Date
   * @generated
   */
  public Adapter createDateAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Date_And_Time <em>Date And Time</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Date_And_Time
   * @generated
   */
  public Adapter createDate_And_TimeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Non_Generic_Type_Name <em>Non Generic Type Name</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Non_Generic_Type_Name
   * @generated
   */
  public Adapter createNon_Generic_Type_NameAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Elementary_Type_Name <em>Elementary Type Name</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Elementary_Type_Name
   * @generated
   */
  public Adapter createElementary_Type_NameAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Numeric_Type_Name <em>Numeric Type Name</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Numeric_Type_Name
   * @generated
   */
  public Adapter createNumeric_Type_NameAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Integer_Type_Name <em>Integer Type Name</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Integer_Type_Name
   * @generated
   */
  public Adapter createInteger_Type_NameAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Signed_Integer_Type_Name <em>Signed Integer Type Name</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Signed_Integer_Type_Name
   * @generated
   */
  public Adapter createSigned_Integer_Type_NameAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Unsigned_Integer_Type_Name <em>Unsigned Integer Type Name</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Unsigned_Integer_Type_Name
   * @generated
   */
  public Adapter createUnsigned_Integer_Type_NameAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Real_Type_Name <em>Real Type Name</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Real_Type_Name
   * @generated
   */
  public Adapter createReal_Type_NameAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Bit_String_Type_Name <em>Bit String Type Name</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Bit_String_Type_Name
   * @generated
   */
  public Adapter createBit_String_Type_NameAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Time_Type_Name <em>Time Type Name</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Time_Type_Name
   * @generated
   */
  public Adapter createTime_Type_NameAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Date_Type_Name <em>Date Type Name</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Date_Type_Name
   * @generated
   */
  public Adapter createDate_Type_NameAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Xor_Expression <em>Xor Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Xor_Expression
   * @generated
   */
  public Adapter createXor_ExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.And_Expression <em>And Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.And_Expression
   * @generated
   */
  public Adapter createAnd_ExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Comparison <em>Comparison</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Comparison
   * @generated
   */
  public Adapter createComparisonAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Equ_Expression <em>Equ Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Equ_Expression
   * @generated
   */
  public Adapter createEqu_ExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Add_Expression <em>Add Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Add_Expression
   * @generated
   */
  public Adapter createAdd_ExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Term <em>Term</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Term
   * @generated
   */
  public Adapter createTermAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.structuredtext.structuredText.Power_Expression <em>Power Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Power_Expression
   * @generated
   */
  public Adapter createPower_ExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //StructuredTextAdapterFactory
