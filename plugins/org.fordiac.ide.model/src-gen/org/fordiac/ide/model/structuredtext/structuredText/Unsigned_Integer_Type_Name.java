/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unsigned Integer Type Name</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getUnsigned_Integer_Type_Name()
 * @model
 * @generated
 */
public interface Unsigned_Integer_Type_Name extends Integer_Type_Name
{
} // Unsigned_Integer_Type_Name
