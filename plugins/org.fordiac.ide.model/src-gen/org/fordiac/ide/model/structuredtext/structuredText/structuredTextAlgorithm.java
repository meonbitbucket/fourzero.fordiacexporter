/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>structured Text Algorithm</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.structuredTextAlgorithm#getAdapters <em>Adapters</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.structuredTextAlgorithm#getVarDeclarations <em>Var Declarations</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.structuredTextAlgorithm#getStatements <em>Statements</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getstructuredTextAlgorithm()
 * @model
 * @generated
 */
public interface structuredTextAlgorithm extends EObject
{
  /**
   * Returns the value of the '<em><b>Adapters</b></em>' containment reference list.
   * The list contents are of type {@link org.fordiac.ide.model.structuredtext.structuredText.AdapterDefinition}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Adapters</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Adapters</em>' containment reference list.
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getstructuredTextAlgorithm_Adapters()
   * @model containment="true"
   * @generated
   */
  EList<AdapterDefinition> getAdapters();

  /**
   * Returns the value of the '<em><b>Var Declarations</b></em>' containment reference list.
   * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Var Declarations</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Var Declarations</em>' containment reference list.
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getstructuredTextAlgorithm_VarDeclarations()
   * @model containment="true"
   * @generated
   */
  EList<EObject> getVarDeclarations();

  /**
   * Returns the value of the '<em><b>Statements</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Statements</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Statements</em>' containment reference.
   * @see #setStatements(StatementList)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getstructuredTextAlgorithm_Statements()
   * @model containment="true"
   * @generated
   */
  StatementList getStatements();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.structuredTextAlgorithm#getStatements <em>Statements</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Statements</em>' containment reference.
   * @see #getStatements()
   * @generated
   */
  void setStatements(StatementList value);

} // structuredTextAlgorithm
