/**
 */
package org.fordiac.ide.model.structuredtext.structuredText.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.fordiac.ide.model.structuredtext.structuredText.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage
 * @generated
 */
public class StructuredTextSwitch<T> extends Switch<T>
{
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static StructuredTextPackage modelPackage;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StructuredTextSwitch()
  {
    if (modelPackage == null)
    {
      modelPackage = StructuredTextPackage.eINSTANCE;
    }
  }

  /**
   * Checks whether this is a switch for the given package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @parameter ePackage the package in question.
   * @return whether this is a switch for the given package.
   * @generated
   */
  @Override
  protected boolean isSwitchFor(EPackage ePackage)
  {
    return ePackage == modelPackage;
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  @Override
  protected T doSwitch(int classifierID, EObject theEObject)
  {
    switch (classifierID)
    {
      case StructuredTextPackage.STRUCTURED_TEXT_ALGORITHM:
      {
        structuredTextAlgorithm structuredTextAlgorithm = (structuredTextAlgorithm)theEObject;
        T result = casestructuredTextAlgorithm(structuredTextAlgorithm);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.VAR_DECLARATION:
      {
        VarDeclaration varDeclaration = (VarDeclaration)theEObject;
        T result = caseVarDeclaration(varDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.ADAPTER_DEFINITION:
      {
        AdapterDefinition adapterDefinition = (AdapterDefinition)theEObject;
        T result = caseAdapterDefinition(adapterDefinition);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.ADAPTER_DECLARATION:
      {
        AdapterDeclaration adapterDeclaration = (AdapterDeclaration)theEObject;
        T result = caseAdapterDeclaration(adapterDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.PARAMETER:
      {
        Parameter parameter = (Parameter)theEObject;
        T result = caseParameter(parameter);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.SIMPLE_SPEC_INIT:
      {
        Simple_Spec_Init simple_Spec_Init = (Simple_Spec_Init)theEObject;
        T result = caseSimple_Spec_Init(simple_Spec_Init);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.ARRAY_SPEC_INIT:
      {
        Array_Spec_Init array_Spec_Init = (Array_Spec_Init)theEObject;
        T result = caseArray_Spec_Init(array_Spec_Init);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.ARRAY_SPECIFICATION:
      {
        Array_Specification array_Specification = (Array_Specification)theEObject;
        T result = caseArray_Specification(array_Specification);
        if (result == null) result = caseArray_Spec_Init(array_Specification);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.SUBRANGE:
      {
        Subrange subrange = (Subrange)theEObject;
        T result = caseSubrange(subrange);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.ARRAY_INITIALIZATION:
      {
        Array_Initialization array_Initialization = (Array_Initialization)theEObject;
        T result = caseArray_Initialization(array_Initialization);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.ARRAY_INITIAL_ELEMENTS:
      {
        Array_Initial_Elements array_Initial_Elements = (Array_Initial_Elements)theEObject;
        T result = caseArray_Initial_Elements(array_Initial_Elements);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.STRUCTURE_SPEC:
      {
        Structure_Spec structure_Spec = (Structure_Spec)theEObject;
        T result = caseStructure_Spec(structure_Spec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.SBYTE_STRING_SPEC:
      {
        S_Byte_String_Spec s_Byte_String_Spec = (S_Byte_String_Spec)theEObject;
        T result = caseS_Byte_String_Spec(s_Byte_String_Spec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.DBYTE_STRING_SPEC:
      {
        D_Byte_String_Spec d_Byte_String_Spec = (D_Byte_String_Spec)theEObject;
        T result = caseD_Byte_String_Spec(d_Byte_String_Spec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.STATEMENT_LIST:
      {
        StatementList statementList = (StatementList)theEObject;
        T result = caseStatementList(statementList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.STATEMENT:
      {
        Statement statement = (Statement)theEObject;
        T result = caseStatement(statement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.ASSIGNMENT_STATEMENT:
      {
        Assignment_Statement assignment_Statement = (Assignment_Statement)theEObject;
        T result = caseAssignment_Statement(assignment_Statement);
        if (result == null) result = caseStatement(assignment_Statement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.VARIABLE:
      {
        Variable variable = (Variable)theEObject;
        T result = caseVariable(variable);
        if (result == null) result = caseExpression(variable);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.VAR_VARIABLE:
      {
        Var_Variable var_Variable = (Var_Variable)theEObject;
        T result = caseVar_Variable(var_Variable);
        if (result == null) result = caseVariable(var_Variable);
        if (result == null) result = caseExpression(var_Variable);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.ADAPTER_VARIABLE:
      {
        Adapter_Variable adapter_Variable = (Adapter_Variable)theEObject;
        T result = caseAdapter_Variable(adapter_Variable);
        if (result == null) result = caseVariable(adapter_Variable);
        if (result == null) result = caseExpression(adapter_Variable);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.ARRAY_VARIABLE:
      {
        Array_Variable array_Variable = (Array_Variable)theEObject;
        T result = caseArray_Variable(array_Variable);
        if (result == null) result = caseVariable(array_Variable);
        if (result == null) result = caseExpression(array_Variable);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.SELECTION_STATEMENT:
      {
        Selection_Statement selection_Statement = (Selection_Statement)theEObject;
        T result = caseSelection_Statement(selection_Statement);
        if (result == null) result = caseStatement(selection_Statement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.IF_STATEMENT:
      {
        If_Statement if_Statement = (If_Statement)theEObject;
        T result = caseIf_Statement(if_Statement);
        if (result == null) result = caseSelection_Statement(if_Statement);
        if (result == null) result = caseStatement(if_Statement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.ELSE_IF_STATEMENT:
      {
        ElseIf_Statement elseIf_Statement = (ElseIf_Statement)theEObject;
        T result = caseElseIf_Statement(elseIf_Statement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.CASE_STATEMENT:
      {
        Case_Statement case_Statement = (Case_Statement)theEObject;
        T result = caseCase_Statement(case_Statement);
        if (result == null) result = caseSelection_Statement(case_Statement);
        if (result == null) result = caseStatement(case_Statement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.CASE_ELEMENT:
      {
        Case_Element case_Element = (Case_Element)theEObject;
        T result = caseCase_Element(case_Element);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.CASE_ELSE:
      {
        Case_Else case_Else = (Case_Else)theEObject;
        T result = caseCase_Else(case_Else);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.ITERATION_CONTROL_STMT:
      {
        IterationControl_Stmt iterationControl_Stmt = (IterationControl_Stmt)theEObject;
        T result = caseIterationControl_Stmt(iterationControl_Stmt);
        if (result == null) result = caseStatement(iterationControl_Stmt);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.FOR_STATEMENT:
      {
        For_Statement for_Statement = (For_Statement)theEObject;
        T result = caseFor_Statement(for_Statement);
        if (result == null) result = caseStatement(for_Statement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.REPEAT_STATEMENT:
      {
        Repeat_Statement repeat_Statement = (Repeat_Statement)theEObject;
        T result = caseRepeat_Statement(repeat_Statement);
        if (result == null) result = caseStatement(repeat_Statement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.WHILE_STATEMENT:
      {
        While_Statement while_Statement = (While_Statement)theEObject;
        T result = caseWhile_Statement(while_Statement);
        if (result == null) result = caseStatement(while_Statement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.SUBPROG_CTRL_STMT:
      {
        Subprog_Ctrl_Stmt subprog_Ctrl_Stmt = (Subprog_Ctrl_Stmt)theEObject;
        T result = caseSubprog_Ctrl_Stmt(subprog_Ctrl_Stmt);
        if (result == null) result = caseStatement(subprog_Ctrl_Stmt);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.EXPRESSION:
      {
        Expression expression = (Expression)theEObject;
        T result = caseExpression(expression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.FUNC_CALL:
      {
        Func_Call func_Call = (Func_Call)theEObject;
        T result = caseFunc_Call(func_Call);
        if (result == null) result = caseExpression(func_Call);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.PARAM_ASSIGN:
      {
        Param_Assign param_Assign = (Param_Assign)theEObject;
        T result = caseParam_Assign(param_Assign);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.CONSTANT:
      {
        Constant constant = (Constant)theEObject;
        T result = caseConstant(constant);
        if (result == null) result = caseArray_Initial_Elements(constant);
        if (result == null) result = caseExpression(constant);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.NUMERIC_LITERAL:
      {
        Numeric_Literal numeric_Literal = (Numeric_Literal)theEObject;
        T result = caseNumeric_Literal(numeric_Literal);
        if (result == null) result = caseConstant(numeric_Literal);
        if (result == null) result = caseArray_Initial_Elements(numeric_Literal);
        if (result == null) result = caseExpression(numeric_Literal);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.INTEGER_LITERAL:
      {
        IntegerLiteral integerLiteral = (IntegerLiteral)theEObject;
        T result = caseIntegerLiteral(integerLiteral);
        if (result == null) result = caseNumeric_Literal(integerLiteral);
        if (result == null) result = caseConstant(integerLiteral);
        if (result == null) result = caseArray_Initial_Elements(integerLiteral);
        if (result == null) result = caseExpression(integerLiteral);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.REAL_LITERAL:
      {
        Real_Literal real_Literal = (Real_Literal)theEObject;
        T result = caseReal_Literal(real_Literal);
        if (result == null) result = caseNumeric_Literal(real_Literal);
        if (result == null) result = caseConstant(real_Literal);
        if (result == null) result = caseArray_Initial_Elements(real_Literal);
        if (result == null) result = caseExpression(real_Literal);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.BOOLEAN_LITERAL:
      {
        Boolean_Literal boolean_Literal = (Boolean_Literal)theEObject;
        T result = caseBoolean_Literal(boolean_Literal);
        if (result == null) result = caseConstant(boolean_Literal);
        if (result == null) result = caseArray_Initial_Elements(boolean_Literal);
        if (result == null) result = caseExpression(boolean_Literal);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.CHARACTER_STRING:
      {
        Character_String character_String = (Character_String)theEObject;
        T result = caseCharacter_String(character_String);
        if (result == null) result = caseConstant(character_String);
        if (result == null) result = caseArray_Initial_Elements(character_String);
        if (result == null) result = caseExpression(character_String);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.SINGLE_BYTE_CHARACTER_STRING_LITERAL:
      {
        Single_Byte_Character_String_Literal single_Byte_Character_String_Literal = (Single_Byte_Character_String_Literal)theEObject;
        T result = caseSingle_Byte_Character_String_Literal(single_Byte_Character_String_Literal);
        if (result == null) result = caseCharacter_String(single_Byte_Character_String_Literal);
        if (result == null) result = caseConstant(single_Byte_Character_String_Literal);
        if (result == null) result = caseArray_Initial_Elements(single_Byte_Character_String_Literal);
        if (result == null) result = caseExpression(single_Byte_Character_String_Literal);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.DOUBLE_BYTE_CHARACTER_STRING_LITERAL:
      {
        Double_Byte_Character_String_Literal double_Byte_Character_String_Literal = (Double_Byte_Character_String_Literal)theEObject;
        T result = caseDouble_Byte_Character_String_Literal(double_Byte_Character_String_Literal);
        if (result == null) result = caseCharacter_String(double_Byte_Character_String_Literal);
        if (result == null) result = caseConstant(double_Byte_Character_String_Literal);
        if (result == null) result = caseArray_Initial_Elements(double_Byte_Character_String_Literal);
        if (result == null) result = caseExpression(double_Byte_Character_String_Literal);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.TIME_LITERAL:
      {
        Time_Literal time_Literal = (Time_Literal)theEObject;
        T result = caseTime_Literal(time_Literal);
        if (result == null) result = caseConstant(time_Literal);
        if (result == null) result = caseArray_Initial_Elements(time_Literal);
        if (result == null) result = caseExpression(time_Literal);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.DURATION:
      {
        Duration duration = (Duration)theEObject;
        T result = caseDuration(duration);
        if (result == null) result = caseTime_Literal(duration);
        if (result == null) result = caseConstant(duration);
        if (result == null) result = caseArray_Initial_Elements(duration);
        if (result == null) result = caseExpression(duration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.TIME_OF_DAY:
      {
        Time_Of_Day time_Of_Day = (Time_Of_Day)theEObject;
        T result = caseTime_Of_Day(time_Of_Day);
        if (result == null) result = caseTime_Literal(time_Of_Day);
        if (result == null) result = caseConstant(time_Of_Day);
        if (result == null) result = caseArray_Initial_Elements(time_Of_Day);
        if (result == null) result = caseExpression(time_Of_Day);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.DATE:
      {
        Date date = (Date)theEObject;
        T result = caseDate(date);
        if (result == null) result = caseTime_Literal(date);
        if (result == null) result = caseConstant(date);
        if (result == null) result = caseArray_Initial_Elements(date);
        if (result == null) result = caseExpression(date);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.DATE_AND_TIME:
      {
        Date_And_Time date_And_Time = (Date_And_Time)theEObject;
        T result = caseDate_And_Time(date_And_Time);
        if (result == null) result = caseTime_Literal(date_And_Time);
        if (result == null) result = caseConstant(date_And_Time);
        if (result == null) result = caseArray_Initial_Elements(date_And_Time);
        if (result == null) result = caseExpression(date_And_Time);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.NON_GENERIC_TYPE_NAME:
      {
        Non_Generic_Type_Name non_Generic_Type_Name = (Non_Generic_Type_Name)theEObject;
        T result = caseNon_Generic_Type_Name(non_Generic_Type_Name);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.ELEMENTARY_TYPE_NAME:
      {
        Elementary_Type_Name elementary_Type_Name = (Elementary_Type_Name)theEObject;
        T result = caseElementary_Type_Name(elementary_Type_Name);
        if (result == null) result = caseNon_Generic_Type_Name(elementary_Type_Name);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.NUMERIC_TYPE_NAME:
      {
        Numeric_Type_Name numeric_Type_Name = (Numeric_Type_Name)theEObject;
        T result = caseNumeric_Type_Name(numeric_Type_Name);
        if (result == null) result = caseElementary_Type_Name(numeric_Type_Name);
        if (result == null) result = caseNon_Generic_Type_Name(numeric_Type_Name);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.INTEGER_TYPE_NAME:
      {
        Integer_Type_Name integer_Type_Name = (Integer_Type_Name)theEObject;
        T result = caseInteger_Type_Name(integer_Type_Name);
        if (result == null) result = caseNumeric_Type_Name(integer_Type_Name);
        if (result == null) result = caseElementary_Type_Name(integer_Type_Name);
        if (result == null) result = caseNon_Generic_Type_Name(integer_Type_Name);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.SIGNED_INTEGER_TYPE_NAME:
      {
        Signed_Integer_Type_Name signed_Integer_Type_Name = (Signed_Integer_Type_Name)theEObject;
        T result = caseSigned_Integer_Type_Name(signed_Integer_Type_Name);
        if (result == null) result = caseInteger_Type_Name(signed_Integer_Type_Name);
        if (result == null) result = caseNumeric_Type_Name(signed_Integer_Type_Name);
        if (result == null) result = caseElementary_Type_Name(signed_Integer_Type_Name);
        if (result == null) result = caseNon_Generic_Type_Name(signed_Integer_Type_Name);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.UNSIGNED_INTEGER_TYPE_NAME:
      {
        Unsigned_Integer_Type_Name unsigned_Integer_Type_Name = (Unsigned_Integer_Type_Name)theEObject;
        T result = caseUnsigned_Integer_Type_Name(unsigned_Integer_Type_Name);
        if (result == null) result = caseInteger_Type_Name(unsigned_Integer_Type_Name);
        if (result == null) result = caseNumeric_Type_Name(unsigned_Integer_Type_Name);
        if (result == null) result = caseElementary_Type_Name(unsigned_Integer_Type_Name);
        if (result == null) result = caseNon_Generic_Type_Name(unsigned_Integer_Type_Name);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.REAL_TYPE_NAME:
      {
        Real_Type_Name real_Type_Name = (Real_Type_Name)theEObject;
        T result = caseReal_Type_Name(real_Type_Name);
        if (result == null) result = caseNumeric_Type_Name(real_Type_Name);
        if (result == null) result = caseElementary_Type_Name(real_Type_Name);
        if (result == null) result = caseNon_Generic_Type_Name(real_Type_Name);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.BIT_STRING_TYPE_NAME:
      {
        Bit_String_Type_Name bit_String_Type_Name = (Bit_String_Type_Name)theEObject;
        T result = caseBit_String_Type_Name(bit_String_Type_Name);
        if (result == null) result = caseElementary_Type_Name(bit_String_Type_Name);
        if (result == null) result = caseNon_Generic_Type_Name(bit_String_Type_Name);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.TIME_TYPE_NAME:
      {
        Time_Type_Name time_Type_Name = (Time_Type_Name)theEObject;
        T result = caseTime_Type_Name(time_Type_Name);
        if (result == null) result = caseElementary_Type_Name(time_Type_Name);
        if (result == null) result = caseNon_Generic_Type_Name(time_Type_Name);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.DATE_TYPE_NAME:
      {
        Date_Type_Name date_Type_Name = (Date_Type_Name)theEObject;
        T result = caseDate_Type_Name(date_Type_Name);
        if (result == null) result = caseElementary_Type_Name(date_Type_Name);
        if (result == null) result = caseNon_Generic_Type_Name(date_Type_Name);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.XOR_EXPRESSION:
      {
        Xor_Expression xor_Expression = (Xor_Expression)theEObject;
        T result = caseXor_Expression(xor_Expression);
        if (result == null) result = caseExpression(xor_Expression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.AND_EXPRESSION:
      {
        And_Expression and_Expression = (And_Expression)theEObject;
        T result = caseAnd_Expression(and_Expression);
        if (result == null) result = caseExpression(and_Expression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.COMPARISON:
      {
        Comparison comparison = (Comparison)theEObject;
        T result = caseComparison(comparison);
        if (result == null) result = caseExpression(comparison);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.EQU_EXPRESSION:
      {
        Equ_Expression equ_Expression = (Equ_Expression)theEObject;
        T result = caseEqu_Expression(equ_Expression);
        if (result == null) result = caseExpression(equ_Expression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.ADD_EXPRESSION:
      {
        Add_Expression add_Expression = (Add_Expression)theEObject;
        T result = caseAdd_Expression(add_Expression);
        if (result == null) result = caseExpression(add_Expression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.TERM:
      {
        Term term = (Term)theEObject;
        T result = caseTerm(term);
        if (result == null) result = caseExpression(term);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case StructuredTextPackage.POWER_EXPRESSION:
      {
        Power_Expression power_Expression = (Power_Expression)theEObject;
        T result = casePower_Expression(power_Expression);
        if (result == null) result = caseExpression(power_Expression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      default: return defaultCase(theEObject);
    }
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>structured Text Algorithm</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>structured Text Algorithm</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casestructuredTextAlgorithm(structuredTextAlgorithm object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Var Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Var Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseVarDeclaration(VarDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Adapter Definition</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Adapter Definition</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAdapterDefinition(AdapterDefinition object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Adapter Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Adapter Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAdapterDeclaration(AdapterDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Parameter</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Parameter</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseParameter(Parameter object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Simple Spec Init</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Simple Spec Init</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSimple_Spec_Init(Simple_Spec_Init object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Array Spec Init</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Array Spec Init</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseArray_Spec_Init(Array_Spec_Init object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Array Specification</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Array Specification</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseArray_Specification(Array_Specification object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Subrange</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Subrange</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSubrange(Subrange object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Array Initialization</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Array Initialization</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseArray_Initialization(Array_Initialization object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Array Initial Elements</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Array Initial Elements</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseArray_Initial_Elements(Array_Initial_Elements object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Structure Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Structure Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStructure_Spec(Structure_Spec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>SByte String Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>SByte String Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseS_Byte_String_Spec(S_Byte_String_Spec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>DByte String Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>DByte String Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseD_Byte_String_Spec(D_Byte_String_Spec object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Statement List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Statement List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStatementList(StatementList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStatement(Statement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Assignment Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Assignment Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAssignment_Statement(Assignment_Statement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Variable</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Variable</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseVariable(Variable object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Var Variable</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Var Variable</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseVar_Variable(Var_Variable object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Adapter Variable</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Adapter Variable</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAdapter_Variable(Adapter_Variable object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Array Variable</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Array Variable</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseArray_Variable(Array_Variable object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Selection Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Selection Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSelection_Statement(Selection_Statement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>If Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>If Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIf_Statement(If_Statement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Else If Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Else If Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseElseIf_Statement(ElseIf_Statement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Case Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Case Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCase_Statement(Case_Statement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Case Element</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Case Element</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCase_Element(Case_Element object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Case Else</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Case Else</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCase_Else(Case_Else object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Iteration Control Stmt</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Iteration Control Stmt</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIterationControl_Stmt(IterationControl_Stmt object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>For Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>For Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFor_Statement(For_Statement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Repeat Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Repeat Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRepeat_Statement(Repeat_Statement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>While Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>While Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseWhile_Statement(While_Statement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Subprog Ctrl Stmt</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Subprog Ctrl Stmt</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSubprog_Ctrl_Stmt(Subprog_Ctrl_Stmt object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExpression(Expression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Func Call</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Func Call</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFunc_Call(Func_Call object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Param Assign</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Param Assign</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseParam_Assign(Param_Assign object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Constant</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Constant</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseConstant(Constant object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Numeric Literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Numeric Literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNumeric_Literal(Numeric_Literal object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Integer Literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Integer Literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIntegerLiteral(IntegerLiteral object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Real Literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Real Literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseReal_Literal(Real_Literal object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Boolean Literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Boolean Literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBoolean_Literal(Boolean_Literal object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Character String</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Character String</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCharacter_String(Character_String object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Single Byte Character String Literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Single Byte Character String Literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSingle_Byte_Character_String_Literal(Single_Byte_Character_String_Literal object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Double Byte Character String Literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Double Byte Character String Literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDouble_Byte_Character_String_Literal(Double_Byte_Character_String_Literal object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Time Literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Time Literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTime_Literal(Time_Literal object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Duration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Duration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDuration(Duration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Time Of Day</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Time Of Day</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTime_Of_Day(Time_Of_Day object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Date</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Date</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDate(Date object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Date And Time</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Date And Time</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDate_And_Time(Date_And_Time object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Non Generic Type Name</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Non Generic Type Name</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNon_Generic_Type_Name(Non_Generic_Type_Name object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Elementary Type Name</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Elementary Type Name</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseElementary_Type_Name(Elementary_Type_Name object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Numeric Type Name</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Numeric Type Name</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNumeric_Type_Name(Numeric_Type_Name object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Integer Type Name</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Integer Type Name</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseInteger_Type_Name(Integer_Type_Name object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Signed Integer Type Name</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Signed Integer Type Name</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSigned_Integer_Type_Name(Signed_Integer_Type_Name object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Unsigned Integer Type Name</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Unsigned Integer Type Name</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUnsigned_Integer_Type_Name(Unsigned_Integer_Type_Name object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Real Type Name</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Real Type Name</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseReal_Type_Name(Real_Type_Name object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Bit String Type Name</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Bit String Type Name</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBit_String_Type_Name(Bit_String_Type_Name object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Time Type Name</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Time Type Name</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTime_Type_Name(Time_Type_Name object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Date Type Name</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Date Type Name</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDate_Type_Name(Date_Type_Name object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Xor Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Xor Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseXor_Expression(Xor_Expression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>And Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>And Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAnd_Expression(And_Expression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Comparison</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Comparison</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseComparison(Comparison object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Equ Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Equ Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseEqu_Expression(Equ_Expression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Add Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Add Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAdd_Expression(Add_Expression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Term</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Term</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTerm(Term object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Power Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Power Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePower_Expression(Power_Expression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject)
   * @generated
   */
  @Override
  public T defaultCase(EObject object)
  {
    return null;
  }

} //StructuredTextSwitch
