/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Date Type Name</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getDate_Type_Name()
 * @model
 * @generated
 */
public interface Date_Type_Name extends Elementary_Type_Name
{
} // Date_Type_Name
