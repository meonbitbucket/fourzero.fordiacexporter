/**
 */
package org.fordiac.ide.model.structuredtext.structuredText.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.fordiac.ide.model.structuredtext.structuredText.AdapterDefinition;
import org.fordiac.ide.model.structuredtext.structuredText.StatementList;
import org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage;
import org.fordiac.ide.model.structuredtext.structuredText.structuredTextAlgorithm;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>structured Text Algorithm</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.impl.structuredTextAlgorithmImpl#getAdapters <em>Adapters</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.impl.structuredTextAlgorithmImpl#getVarDeclarations <em>Var Declarations</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.impl.structuredTextAlgorithmImpl#getStatements <em>Statements</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class structuredTextAlgorithmImpl extends MinimalEObjectImpl.Container implements structuredTextAlgorithm
{
  /**
   * The cached value of the '{@link #getAdapters() <em>Adapters</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAdapters()
   * @generated
   * @ordered
   */
  protected EList<AdapterDefinition> adapters;

  /**
   * The cached value of the '{@link #getVarDeclarations() <em>Var Declarations</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVarDeclarations()
   * @generated
   * @ordered
   */
  protected EList<EObject> varDeclarations;

  /**
   * The cached value of the '{@link #getStatements() <em>Statements</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStatements()
   * @generated
   * @ordered
   */
  protected StatementList statements;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected structuredTextAlgorithmImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return StructuredTextPackage.Literals.STRUCTURED_TEXT_ALGORITHM;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<AdapterDefinition> getAdapters()
  {
    if (adapters == null)
    {
      adapters = new EObjectContainmentEList<AdapterDefinition>(AdapterDefinition.class, this, StructuredTextPackage.STRUCTURED_TEXT_ALGORITHM__ADAPTERS);
    }
    return adapters;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<EObject> getVarDeclarations()
  {
    if (varDeclarations == null)
    {
      varDeclarations = new EObjectContainmentEList<EObject>(EObject.class, this, StructuredTextPackage.STRUCTURED_TEXT_ALGORITHM__VAR_DECLARATIONS);
    }
    return varDeclarations;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StatementList getStatements()
  {
    return statements;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetStatements(StatementList newStatements, NotificationChain msgs)
  {
    StatementList oldStatements = statements;
    statements = newStatements;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StructuredTextPackage.STRUCTURED_TEXT_ALGORITHM__STATEMENTS, oldStatements, newStatements);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setStatements(StatementList newStatements)
  {
    if (newStatements != statements)
    {
      NotificationChain msgs = null;
      if (statements != null)
        msgs = ((InternalEObject)statements).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StructuredTextPackage.STRUCTURED_TEXT_ALGORITHM__STATEMENTS, null, msgs);
      if (newStatements != null)
        msgs = ((InternalEObject)newStatements).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StructuredTextPackage.STRUCTURED_TEXT_ALGORITHM__STATEMENTS, null, msgs);
      msgs = basicSetStatements(newStatements, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, StructuredTextPackage.STRUCTURED_TEXT_ALGORITHM__STATEMENTS, newStatements, newStatements));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case StructuredTextPackage.STRUCTURED_TEXT_ALGORITHM__ADAPTERS:
        return ((InternalEList<?>)getAdapters()).basicRemove(otherEnd, msgs);
      case StructuredTextPackage.STRUCTURED_TEXT_ALGORITHM__VAR_DECLARATIONS:
        return ((InternalEList<?>)getVarDeclarations()).basicRemove(otherEnd, msgs);
      case StructuredTextPackage.STRUCTURED_TEXT_ALGORITHM__STATEMENTS:
        return basicSetStatements(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case StructuredTextPackage.STRUCTURED_TEXT_ALGORITHM__ADAPTERS:
        return getAdapters();
      case StructuredTextPackage.STRUCTURED_TEXT_ALGORITHM__VAR_DECLARATIONS:
        return getVarDeclarations();
      case StructuredTextPackage.STRUCTURED_TEXT_ALGORITHM__STATEMENTS:
        return getStatements();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case StructuredTextPackage.STRUCTURED_TEXT_ALGORITHM__ADAPTERS:
        getAdapters().clear();
        getAdapters().addAll((Collection<? extends AdapterDefinition>)newValue);
        return;
      case StructuredTextPackage.STRUCTURED_TEXT_ALGORITHM__VAR_DECLARATIONS:
        getVarDeclarations().clear();
        getVarDeclarations().addAll((Collection<? extends EObject>)newValue);
        return;
      case StructuredTextPackage.STRUCTURED_TEXT_ALGORITHM__STATEMENTS:
        setStatements((StatementList)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case StructuredTextPackage.STRUCTURED_TEXT_ALGORITHM__ADAPTERS:
        getAdapters().clear();
        return;
      case StructuredTextPackage.STRUCTURED_TEXT_ALGORITHM__VAR_DECLARATIONS:
        getVarDeclarations().clear();
        return;
      case StructuredTextPackage.STRUCTURED_TEXT_ALGORITHM__STATEMENTS:
        setStatements((StatementList)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case StructuredTextPackage.STRUCTURED_TEXT_ALGORITHM__ADAPTERS:
        return adapters != null && !adapters.isEmpty();
      case StructuredTextPackage.STRUCTURED_TEXT_ALGORITHM__VAR_DECLARATIONS:
        return varDeclarations != null && !varDeclarations.isEmpty();
      case StructuredTextPackage.STRUCTURED_TEXT_ALGORITHM__STATEMENTS:
        return statements != null;
    }
    return super.eIsSet(featureID);
  }

} //structuredTextAlgorithmImpl
