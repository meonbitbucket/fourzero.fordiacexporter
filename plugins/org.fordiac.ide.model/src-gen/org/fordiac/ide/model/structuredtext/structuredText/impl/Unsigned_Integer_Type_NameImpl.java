/**
 */
package org.fordiac.ide.model.structuredtext.structuredText.impl;

import org.eclipse.emf.ecore.EClass;

import org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage;
import org.fordiac.ide.model.structuredtext.structuredText.Unsigned_Integer_Type_Name;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Unsigned Integer Type Name</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class Unsigned_Integer_Type_NameImpl extends Integer_Type_NameImpl implements Unsigned_Integer_Type_Name
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected Unsigned_Integer_Type_NameImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return StructuredTextPackage.Literals.UNSIGNED_INTEGER_TYPE_NAME;
  }

} //Unsigned_Integer_Type_NameImpl
