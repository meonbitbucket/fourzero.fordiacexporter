/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Character String</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.Character_String#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getCharacter_String()
 * @model
 * @generated
 */
public interface Character_String extends Constant
{
  /**
   * Returns the value of the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' attribute.
   * @see #setType(String)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getCharacter_String_Type()
   * @model
   * @generated
   */
  String getType();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.Character_String#getType <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' attribute.
   * @see #getType()
   * @generated
   */
  void setType(String value);

} // Character_String
