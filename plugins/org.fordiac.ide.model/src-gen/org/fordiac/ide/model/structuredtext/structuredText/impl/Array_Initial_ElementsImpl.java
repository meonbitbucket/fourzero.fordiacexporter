/**
 */
package org.fordiac.ide.model.structuredtext.structuredText.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.fordiac.ide.model.structuredtext.structuredText.Array_Initial_Elements;
import org.fordiac.ide.model.structuredtext.structuredText.Constant;
import org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Array Initial Elements</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Array_Initial_ElementsImpl#getNum <em>Num</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Array_Initial_ElementsImpl#getArrayElement <em>Array Element</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Array_Initial_ElementsImpl extends MinimalEObjectImpl.Container implements Array_Initial_Elements
{
  /**
   * The default value of the '{@link #getNum() <em>Num</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNum()
   * @generated
   * @ordered
   */
  protected static final int NUM_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getNum() <em>Num</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNum()
   * @generated
   * @ordered
   */
  protected int num = NUM_EDEFAULT;

  /**
   * The cached value of the '{@link #getArrayElement() <em>Array Element</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getArrayElement()
   * @generated
   * @ordered
   */
  protected Constant arrayElement;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected Array_Initial_ElementsImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return StructuredTextPackage.Literals.ARRAY_INITIAL_ELEMENTS;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getNum()
  {
    return num;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNum(int newNum)
  {
    int oldNum = num;
    num = newNum;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, StructuredTextPackage.ARRAY_INITIAL_ELEMENTS__NUM, oldNum, num));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Constant getArrayElement()
  {
    return arrayElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetArrayElement(Constant newArrayElement, NotificationChain msgs)
  {
    Constant oldArrayElement = arrayElement;
    arrayElement = newArrayElement;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StructuredTextPackage.ARRAY_INITIAL_ELEMENTS__ARRAY_ELEMENT, oldArrayElement, newArrayElement);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setArrayElement(Constant newArrayElement)
  {
    if (newArrayElement != arrayElement)
    {
      NotificationChain msgs = null;
      if (arrayElement != null)
        msgs = ((InternalEObject)arrayElement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StructuredTextPackage.ARRAY_INITIAL_ELEMENTS__ARRAY_ELEMENT, null, msgs);
      if (newArrayElement != null)
        msgs = ((InternalEObject)newArrayElement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StructuredTextPackage.ARRAY_INITIAL_ELEMENTS__ARRAY_ELEMENT, null, msgs);
      msgs = basicSetArrayElement(newArrayElement, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, StructuredTextPackage.ARRAY_INITIAL_ELEMENTS__ARRAY_ELEMENT, newArrayElement, newArrayElement));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case StructuredTextPackage.ARRAY_INITIAL_ELEMENTS__ARRAY_ELEMENT:
        return basicSetArrayElement(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case StructuredTextPackage.ARRAY_INITIAL_ELEMENTS__NUM:
        return getNum();
      case StructuredTextPackage.ARRAY_INITIAL_ELEMENTS__ARRAY_ELEMENT:
        return getArrayElement();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case StructuredTextPackage.ARRAY_INITIAL_ELEMENTS__NUM:
        setNum((Integer)newValue);
        return;
      case StructuredTextPackage.ARRAY_INITIAL_ELEMENTS__ARRAY_ELEMENT:
        setArrayElement((Constant)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case StructuredTextPackage.ARRAY_INITIAL_ELEMENTS__NUM:
        setNum(NUM_EDEFAULT);
        return;
      case StructuredTextPackage.ARRAY_INITIAL_ELEMENTS__ARRAY_ELEMENT:
        setArrayElement((Constant)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case StructuredTextPackage.ARRAY_INITIAL_ELEMENTS__NUM:
        return num != NUM_EDEFAULT;
      case StructuredTextPackage.ARRAY_INITIAL_ELEMENTS__ARRAY_ELEMENT:
        return arrayElement != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (num: ");
    result.append(num);
    result.append(')');
    return result.toString();
  }

} //Array_Initial_ElementsImpl
