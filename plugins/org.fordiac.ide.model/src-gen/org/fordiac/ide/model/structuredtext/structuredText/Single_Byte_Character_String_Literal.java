/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Single Byte Character String Literal</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getSingle_Byte_Character_String_Literal()
 * @model
 * @generated
 */
public interface Single_Byte_Character_String_Literal extends Character_String
{
} // Single_Byte_Character_String_Literal
