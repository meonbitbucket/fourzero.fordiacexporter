/**
 */
package org.fordiac.ide.model.structuredtext.structuredText.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.fordiac.ide.model.structuredtext.structuredText.Expression;
import org.fordiac.ide.model.structuredtext.structuredText.For_Statement;
import org.fordiac.ide.model.structuredtext.structuredText.StatementList;
import org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage;
import org.fordiac.ide.model.structuredtext.structuredText.VarDeclaration;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>For Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.impl.For_StatementImpl#getControlVariable <em>Control Variable</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.impl.For_StatementImpl#getExpressionFrom <em>Expression From</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.impl.For_StatementImpl#getExpressionTo <em>Expression To</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.impl.For_StatementImpl#getExpressionBy <em>Expression By</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.impl.For_StatementImpl#getStatements <em>Statements</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class For_StatementImpl extends StatementImpl implements For_Statement
{
  /**
   * The cached value of the '{@link #getControlVariable() <em>Control Variable</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getControlVariable()
   * @generated
   * @ordered
   */
  protected VarDeclaration controlVariable;

  /**
   * The cached value of the '{@link #getExpressionFrom() <em>Expression From</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExpressionFrom()
   * @generated
   * @ordered
   */
  protected Expression expressionFrom;

  /**
   * The cached value of the '{@link #getExpressionTo() <em>Expression To</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExpressionTo()
   * @generated
   * @ordered
   */
  protected Expression expressionTo;

  /**
   * The cached value of the '{@link #getExpressionBy() <em>Expression By</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExpressionBy()
   * @generated
   * @ordered
   */
  protected Expression expressionBy;

  /**
   * The cached value of the '{@link #getStatements() <em>Statements</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStatements()
   * @generated
   * @ordered
   */
  protected StatementList statements;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected For_StatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return StructuredTextPackage.Literals.FOR_STATEMENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VarDeclaration getControlVariable()
  {
    if (controlVariable != null && controlVariable.eIsProxy())
    {
      InternalEObject oldControlVariable = (InternalEObject)controlVariable;
      controlVariable = (VarDeclaration)eResolveProxy(oldControlVariable);
      if (controlVariable != oldControlVariable)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, StructuredTextPackage.FOR_STATEMENT__CONTROL_VARIABLE, oldControlVariable, controlVariable));
      }
    }
    return controlVariable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VarDeclaration basicGetControlVariable()
  {
    return controlVariable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setControlVariable(VarDeclaration newControlVariable)
  {
    VarDeclaration oldControlVariable = controlVariable;
    controlVariable = newControlVariable;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, StructuredTextPackage.FOR_STATEMENT__CONTROL_VARIABLE, oldControlVariable, controlVariable));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Expression getExpressionFrom()
  {
    return expressionFrom;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetExpressionFrom(Expression newExpressionFrom, NotificationChain msgs)
  {
    Expression oldExpressionFrom = expressionFrom;
    expressionFrom = newExpressionFrom;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StructuredTextPackage.FOR_STATEMENT__EXPRESSION_FROM, oldExpressionFrom, newExpressionFrom);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExpressionFrom(Expression newExpressionFrom)
  {
    if (newExpressionFrom != expressionFrom)
    {
      NotificationChain msgs = null;
      if (expressionFrom != null)
        msgs = ((InternalEObject)expressionFrom).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StructuredTextPackage.FOR_STATEMENT__EXPRESSION_FROM, null, msgs);
      if (newExpressionFrom != null)
        msgs = ((InternalEObject)newExpressionFrom).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StructuredTextPackage.FOR_STATEMENT__EXPRESSION_FROM, null, msgs);
      msgs = basicSetExpressionFrom(newExpressionFrom, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, StructuredTextPackage.FOR_STATEMENT__EXPRESSION_FROM, newExpressionFrom, newExpressionFrom));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Expression getExpressionTo()
  {
    return expressionTo;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetExpressionTo(Expression newExpressionTo, NotificationChain msgs)
  {
    Expression oldExpressionTo = expressionTo;
    expressionTo = newExpressionTo;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StructuredTextPackage.FOR_STATEMENT__EXPRESSION_TO, oldExpressionTo, newExpressionTo);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExpressionTo(Expression newExpressionTo)
  {
    if (newExpressionTo != expressionTo)
    {
      NotificationChain msgs = null;
      if (expressionTo != null)
        msgs = ((InternalEObject)expressionTo).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StructuredTextPackage.FOR_STATEMENT__EXPRESSION_TO, null, msgs);
      if (newExpressionTo != null)
        msgs = ((InternalEObject)newExpressionTo).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StructuredTextPackage.FOR_STATEMENT__EXPRESSION_TO, null, msgs);
      msgs = basicSetExpressionTo(newExpressionTo, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, StructuredTextPackage.FOR_STATEMENT__EXPRESSION_TO, newExpressionTo, newExpressionTo));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Expression getExpressionBy()
  {
    return expressionBy;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetExpressionBy(Expression newExpressionBy, NotificationChain msgs)
  {
    Expression oldExpressionBy = expressionBy;
    expressionBy = newExpressionBy;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StructuredTextPackage.FOR_STATEMENT__EXPRESSION_BY, oldExpressionBy, newExpressionBy);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExpressionBy(Expression newExpressionBy)
  {
    if (newExpressionBy != expressionBy)
    {
      NotificationChain msgs = null;
      if (expressionBy != null)
        msgs = ((InternalEObject)expressionBy).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StructuredTextPackage.FOR_STATEMENT__EXPRESSION_BY, null, msgs);
      if (newExpressionBy != null)
        msgs = ((InternalEObject)newExpressionBy).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StructuredTextPackage.FOR_STATEMENT__EXPRESSION_BY, null, msgs);
      msgs = basicSetExpressionBy(newExpressionBy, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, StructuredTextPackage.FOR_STATEMENT__EXPRESSION_BY, newExpressionBy, newExpressionBy));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StatementList getStatements()
  {
    return statements;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetStatements(StatementList newStatements, NotificationChain msgs)
  {
    StatementList oldStatements = statements;
    statements = newStatements;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StructuredTextPackage.FOR_STATEMENT__STATEMENTS, oldStatements, newStatements);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setStatements(StatementList newStatements)
  {
    if (newStatements != statements)
    {
      NotificationChain msgs = null;
      if (statements != null)
        msgs = ((InternalEObject)statements).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StructuredTextPackage.FOR_STATEMENT__STATEMENTS, null, msgs);
      if (newStatements != null)
        msgs = ((InternalEObject)newStatements).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StructuredTextPackage.FOR_STATEMENT__STATEMENTS, null, msgs);
      msgs = basicSetStatements(newStatements, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, StructuredTextPackage.FOR_STATEMENT__STATEMENTS, newStatements, newStatements));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case StructuredTextPackage.FOR_STATEMENT__EXPRESSION_FROM:
        return basicSetExpressionFrom(null, msgs);
      case StructuredTextPackage.FOR_STATEMENT__EXPRESSION_TO:
        return basicSetExpressionTo(null, msgs);
      case StructuredTextPackage.FOR_STATEMENT__EXPRESSION_BY:
        return basicSetExpressionBy(null, msgs);
      case StructuredTextPackage.FOR_STATEMENT__STATEMENTS:
        return basicSetStatements(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case StructuredTextPackage.FOR_STATEMENT__CONTROL_VARIABLE:
        if (resolve) return getControlVariable();
        return basicGetControlVariable();
      case StructuredTextPackage.FOR_STATEMENT__EXPRESSION_FROM:
        return getExpressionFrom();
      case StructuredTextPackage.FOR_STATEMENT__EXPRESSION_TO:
        return getExpressionTo();
      case StructuredTextPackage.FOR_STATEMENT__EXPRESSION_BY:
        return getExpressionBy();
      case StructuredTextPackage.FOR_STATEMENT__STATEMENTS:
        return getStatements();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case StructuredTextPackage.FOR_STATEMENT__CONTROL_VARIABLE:
        setControlVariable((VarDeclaration)newValue);
        return;
      case StructuredTextPackage.FOR_STATEMENT__EXPRESSION_FROM:
        setExpressionFrom((Expression)newValue);
        return;
      case StructuredTextPackage.FOR_STATEMENT__EXPRESSION_TO:
        setExpressionTo((Expression)newValue);
        return;
      case StructuredTextPackage.FOR_STATEMENT__EXPRESSION_BY:
        setExpressionBy((Expression)newValue);
        return;
      case StructuredTextPackage.FOR_STATEMENT__STATEMENTS:
        setStatements((StatementList)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case StructuredTextPackage.FOR_STATEMENT__CONTROL_VARIABLE:
        setControlVariable((VarDeclaration)null);
        return;
      case StructuredTextPackage.FOR_STATEMENT__EXPRESSION_FROM:
        setExpressionFrom((Expression)null);
        return;
      case StructuredTextPackage.FOR_STATEMENT__EXPRESSION_TO:
        setExpressionTo((Expression)null);
        return;
      case StructuredTextPackage.FOR_STATEMENT__EXPRESSION_BY:
        setExpressionBy((Expression)null);
        return;
      case StructuredTextPackage.FOR_STATEMENT__STATEMENTS:
        setStatements((StatementList)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case StructuredTextPackage.FOR_STATEMENT__CONTROL_VARIABLE:
        return controlVariable != null;
      case StructuredTextPackage.FOR_STATEMENT__EXPRESSION_FROM:
        return expressionFrom != null;
      case StructuredTextPackage.FOR_STATEMENT__EXPRESSION_TO:
        return expressionTo != null;
      case StructuredTextPackage.FOR_STATEMENT__EXPRESSION_BY:
        return expressionBy != null;
      case StructuredTextPackage.FOR_STATEMENT__STATEMENTS:
        return statements != null;
    }
    return super.eIsSet(featureID);
  }

} //For_StatementImpl
