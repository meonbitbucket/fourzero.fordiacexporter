/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Func Call</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.Func_Call#getName <em>Name</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.Func_Call#getParamslist <em>Paramslist</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getFunc_Call()
 * @model
 * @generated
 */
public interface Func_Call extends Expression
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getFunc_Call_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.Func_Call#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Paramslist</b></em>' containment reference list.
   * The list contents are of type {@link org.fordiac.ide.model.structuredtext.structuredText.Param_Assign}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Paramslist</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Paramslist</em>' containment reference list.
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getFunc_Call_Paramslist()
   * @model containment="true"
   * @generated
   */
  EList<Param_Assign> getParamslist();

} // Func_Call
