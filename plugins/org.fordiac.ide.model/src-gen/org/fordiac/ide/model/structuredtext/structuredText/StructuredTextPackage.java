/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextFactory
 * @model kind="package"
 * @generated
 */
public interface StructuredTextPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "structuredText";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.fordiac.org/structuredtext/StructuredText";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "structuredText";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  StructuredTextPackage eINSTANCE = org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl.init();

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.structuredTextAlgorithmImpl <em>structured Text Algorithm</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.structuredTextAlgorithmImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getstructuredTextAlgorithm()
   * @generated
   */
  int STRUCTURED_TEXT_ALGORITHM = 0;

  /**
   * The feature id for the '<em><b>Adapters</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCTURED_TEXT_ALGORITHM__ADAPTERS = 0;

  /**
   * The feature id for the '<em><b>Var Declarations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCTURED_TEXT_ALGORITHM__VAR_DECLARATIONS = 1;

  /**
   * The feature id for the '<em><b>Statements</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCTURED_TEXT_ALGORITHM__STATEMENTS = 2;

  /**
   * The number of structural features of the '<em>structured Text Algorithm</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCTURED_TEXT_ALGORITHM_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.VarDeclarationImpl <em>Var Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.VarDeclarationImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getVarDeclaration()
   * @generated
   */
  int VAR_DECLARATION = 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_DECLARATION__NAME = 0;

  /**
   * The feature id for the '<em><b>Specification</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_DECLARATION__SPECIFICATION = 1;

  /**
   * The number of structural features of the '<em>Var Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_DECLARATION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.AdapterDefinitionImpl <em>Adapter Definition</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.AdapterDefinitionImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getAdapterDefinition()
   * @generated
   */
  int ADAPTER_DEFINITION = 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADAPTER_DEFINITION__NAME = 0;

  /**
   * The feature id for the '<em><b>Var Declarations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADAPTER_DEFINITION__VAR_DECLARATIONS = 1;

  /**
   * The number of structural features of the '<em>Adapter Definition</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADAPTER_DEFINITION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.AdapterDeclarationImpl <em>Adapter Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.AdapterDeclarationImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getAdapterDeclaration()
   * @generated
   */
  int ADAPTER_DECLARATION = 3;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADAPTER_DECLARATION__NAME = 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADAPTER_DECLARATION__TYPE = 1;

  /**
   * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADAPTER_DECLARATION__PARAMETERS = 2;

  /**
   * The number of structural features of the '<em>Adapter Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADAPTER_DECLARATION_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.ParameterImpl <em>Parameter</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.ParameterImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getParameter()
   * @generated
   */
  int PARAMETER = 4;

  /**
   * The feature id for the '<em><b>Param</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER__PARAM = 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER__VALUE = 1;

  /**
   * The number of structural features of the '<em>Parameter</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Simple_Spec_InitImpl <em>Simple Spec Init</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Simple_Spec_InitImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getSimple_Spec_Init()
   * @generated
   */
  int SIMPLE_SPEC_INIT = 5;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_SPEC_INIT__TYPE = 0;

  /**
   * The feature id for the '<em><b>Constant</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_SPEC_INIT__CONSTANT = 1;

  /**
   * The number of structural features of the '<em>Simple Spec Init</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_SPEC_INIT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Array_Spec_InitImpl <em>Array Spec Init</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Array_Spec_InitImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getArray_Spec_Init()
   * @generated
   */
  int ARRAY_SPEC_INIT = 6;

  /**
   * The number of structural features of the '<em>Array Spec Init</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_SPEC_INIT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Array_SpecificationImpl <em>Array Specification</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Array_SpecificationImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getArray_Specification()
   * @generated
   */
  int ARRAY_SPECIFICATION = 7;

  /**
   * The feature id for the '<em><b>Initialization</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_SPECIFICATION__INITIALIZATION = ARRAY_SPEC_INIT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Subrange</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_SPECIFICATION__SUBRANGE = ARRAY_SPEC_INIT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_SPECIFICATION__TYPE = ARRAY_SPEC_INIT_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Array Specification</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_SPECIFICATION_FEATURE_COUNT = ARRAY_SPEC_INIT_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.SubrangeImpl <em>Subrange</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.SubrangeImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getSubrange()
   * @generated
   */
  int SUBRANGE = 8;

  /**
   * The feature id for the '<em><b>Min</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUBRANGE__MIN = 0;

  /**
   * The feature id for the '<em><b>Max</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUBRANGE__MAX = 1;

  /**
   * The number of structural features of the '<em>Subrange</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUBRANGE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Array_InitializationImpl <em>Array Initialization</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Array_InitializationImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getArray_Initialization()
   * @generated
   */
  int ARRAY_INITIALIZATION = 9;

  /**
   * The feature id for the '<em><b>Init Element</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_INITIALIZATION__INIT_ELEMENT = 0;

  /**
   * The number of structural features of the '<em>Array Initialization</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_INITIALIZATION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Array_Initial_ElementsImpl <em>Array Initial Elements</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Array_Initial_ElementsImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getArray_Initial_Elements()
   * @generated
   */
  int ARRAY_INITIAL_ELEMENTS = 10;

  /**
   * The feature id for the '<em><b>Num</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_INITIAL_ELEMENTS__NUM = 0;

  /**
   * The feature id for the '<em><b>Array Element</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_INITIAL_ELEMENTS__ARRAY_ELEMENT = 1;

  /**
   * The number of structural features of the '<em>Array Initial Elements</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_INITIAL_ELEMENTS_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Structure_SpecImpl <em>Structure Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Structure_SpecImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getStructure_Spec()
   * @generated
   */
  int STRUCTURE_SPEC = 11;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCTURE_SPEC__NAME = 0;

  /**
   * The feature id for the '<em><b>Element</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCTURE_SPEC__ELEMENT = 1;

  /**
   * The number of structural features of the '<em>Structure Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRUCTURE_SPEC_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.S_Byte_String_SpecImpl <em>SByte String Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.S_Byte_String_SpecImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getS_Byte_String_Spec()
   * @generated
   */
  int SBYTE_STRING_SPEC = 12;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SBYTE_STRING_SPEC__TYPE = 0;

  /**
   * The feature id for the '<em><b>Size</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SBYTE_STRING_SPEC__SIZE = 1;

  /**
   * The feature id for the '<em><b>String</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SBYTE_STRING_SPEC__STRING = 2;

  /**
   * The number of structural features of the '<em>SByte String Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SBYTE_STRING_SPEC_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.D_Byte_String_SpecImpl <em>DByte String Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.D_Byte_String_SpecImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getD_Byte_String_Spec()
   * @generated
   */
  int DBYTE_STRING_SPEC = 13;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DBYTE_STRING_SPEC__TYPE = 0;

  /**
   * The feature id for the '<em><b>Size</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DBYTE_STRING_SPEC__SIZE = 1;

  /**
   * The feature id for the '<em><b>String</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DBYTE_STRING_SPEC__STRING = 2;

  /**
   * The number of structural features of the '<em>DByte String Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DBYTE_STRING_SPEC_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.StatementListImpl <em>Statement List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StatementListImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getStatementList()
   * @generated
   */
  int STATEMENT_LIST = 14;

  /**
   * The feature id for the '<em><b>Statements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_LIST__STATEMENTS = 0;

  /**
   * The number of structural features of the '<em>Statement List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.StatementImpl <em>Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StatementImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getStatement()
   * @generated
   */
  int STATEMENT = 15;

  /**
   * The number of structural features of the '<em>Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Assignment_StatementImpl <em>Assignment Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Assignment_StatementImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getAssignment_Statement()
   * @generated
   */
  int ASSIGNMENT_STATEMENT = 16;

  /**
   * The feature id for the '<em><b>Variable</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGNMENT_STATEMENT__VARIABLE = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGNMENT_STATEMENT__EXPRESSION = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Assignment Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGNMENT_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.ExpressionImpl <em>Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.ExpressionImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getExpression()
   * @generated
   */
  int EXPRESSION = 32;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION__LEFT = 0;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION__OPERATOR = 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION__RIGHT = 2;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION__EXPRESSION = 3;

  /**
   * The number of structural features of the '<em>Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.VariableImpl <em>Variable</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.VariableImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getVariable()
   * @generated
   */
  int VARIABLE = 17;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE__LEFT = EXPRESSION__LEFT;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE__OPERATOR = EXPRESSION__OPERATOR;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE__RIGHT = EXPRESSION__RIGHT;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE__EXPRESSION = EXPRESSION__EXPRESSION;

  /**
   * The feature id for the '<em><b>Var</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE__VAR = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Variable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Var_VariableImpl <em>Var Variable</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Var_VariableImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getVar_Variable()
   * @generated
   */
  int VAR_VARIABLE = 18;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_VARIABLE__LEFT = VARIABLE__LEFT;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_VARIABLE__OPERATOR = VARIABLE__OPERATOR;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_VARIABLE__RIGHT = VARIABLE__RIGHT;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_VARIABLE__EXPRESSION = VARIABLE__EXPRESSION;

  /**
   * The feature id for the '<em><b>Var</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_VARIABLE__VAR = VARIABLE__VAR;

  /**
   * The number of structural features of the '<em>Var Variable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_VARIABLE_FEATURE_COUNT = VARIABLE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Adapter_VariableImpl <em>Adapter Variable</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Adapter_VariableImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getAdapter_Variable()
   * @generated
   */
  int ADAPTER_VARIABLE = 19;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADAPTER_VARIABLE__LEFT = VARIABLE__LEFT;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADAPTER_VARIABLE__OPERATOR = VARIABLE__OPERATOR;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADAPTER_VARIABLE__RIGHT = VARIABLE__RIGHT;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADAPTER_VARIABLE__EXPRESSION = VARIABLE__EXPRESSION;

  /**
   * The feature id for the '<em><b>Var</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADAPTER_VARIABLE__VAR = VARIABLE__VAR;

  /**
   * The feature id for the '<em><b>Adapter</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADAPTER_VARIABLE__ADAPTER = VARIABLE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Adapter Variable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADAPTER_VARIABLE_FEATURE_COUNT = VARIABLE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Array_VariableImpl <em>Array Variable</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Array_VariableImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getArray_Variable()
   * @generated
   */
  int ARRAY_VARIABLE = 20;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_VARIABLE__LEFT = VARIABLE__LEFT;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_VARIABLE__OPERATOR = VARIABLE__OPERATOR;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_VARIABLE__RIGHT = VARIABLE__RIGHT;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_VARIABLE__EXPRESSION = VARIABLE__EXPRESSION;

  /**
   * The feature id for the '<em><b>Var</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_VARIABLE__VAR = VARIABLE__VAR;

  /**
   * The feature id for the '<em><b>Num</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_VARIABLE__NUM = VARIABLE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Array Variable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_VARIABLE_FEATURE_COUNT = VARIABLE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Selection_StatementImpl <em>Selection Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Selection_StatementImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getSelection_Statement()
   * @generated
   */
  int SELECTION_STATEMENT = 21;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECTION_STATEMENT__EXPRESSION = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Selection Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECTION_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.If_StatementImpl <em>If Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.If_StatementImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getIf_Statement()
   * @generated
   */
  int IF_STATEMENT = 22;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_STATEMENT__EXPRESSION = SELECTION_STATEMENT__EXPRESSION;

  /**
   * The feature id for the '<em><b>Statments</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_STATEMENT__STATMENTS = SELECTION_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Elseif</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_STATEMENT__ELSEIF = SELECTION_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Else Statements</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_STATEMENT__ELSE_STATEMENTS = SELECTION_STATEMENT_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>If Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_STATEMENT_FEATURE_COUNT = SELECTION_STATEMENT_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.ElseIf_StatementImpl <em>Else If Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.ElseIf_StatementImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getElseIf_Statement()
   * @generated
   */
  int ELSE_IF_STATEMENT = 23;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELSE_IF_STATEMENT__EXPRESSION = 0;

  /**
   * The feature id for the '<em><b>Statements</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELSE_IF_STATEMENT__STATEMENTS = 1;

  /**
   * The number of structural features of the '<em>Else If Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELSE_IF_STATEMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Case_StatementImpl <em>Case Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Case_StatementImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getCase_Statement()
   * @generated
   */
  int CASE_STATEMENT = 24;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CASE_STATEMENT__EXPRESSION = SELECTION_STATEMENT__EXPRESSION;

  /**
   * The feature id for the '<em><b>Case Elements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CASE_STATEMENT__CASE_ELEMENTS = SELECTION_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Case Else</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CASE_STATEMENT__CASE_ELSE = SELECTION_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Case Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CASE_STATEMENT_FEATURE_COUNT = SELECTION_STATEMENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Case_ElementImpl <em>Case Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Case_ElementImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getCase_Element()
   * @generated
   */
  int CASE_ELEMENT = 25;

  /**
   * The feature id for the '<em><b>Case</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CASE_ELEMENT__CASE = 0;

  /**
   * The feature id for the '<em><b>Statements</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CASE_ELEMENT__STATEMENTS = 1;

  /**
   * The number of structural features of the '<em>Case Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CASE_ELEMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Case_ElseImpl <em>Case Else</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Case_ElseImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getCase_Else()
   * @generated
   */
  int CASE_ELSE = 26;

  /**
   * The feature id for the '<em><b>Statements</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CASE_ELSE__STATEMENTS = 0;

  /**
   * The number of structural features of the '<em>Case Else</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CASE_ELSE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.IterationControl_StmtImpl <em>Iteration Control Stmt</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.IterationControl_StmtImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getIterationControl_Stmt()
   * @generated
   */
  int ITERATION_CONTROL_STMT = 27;

  /**
   * The feature id for the '<em><b>Statement</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ITERATION_CONTROL_STMT__STATEMENT = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Iteration Control Stmt</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ITERATION_CONTROL_STMT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.For_StatementImpl <em>For Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.For_StatementImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getFor_Statement()
   * @generated
   */
  int FOR_STATEMENT = 28;

  /**
   * The feature id for the '<em><b>Control Variable</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR_STATEMENT__CONTROL_VARIABLE = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Expression From</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR_STATEMENT__EXPRESSION_FROM = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Expression To</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR_STATEMENT__EXPRESSION_TO = STATEMENT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Expression By</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR_STATEMENT__EXPRESSION_BY = STATEMENT_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Statements</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR_STATEMENT__STATEMENTS = STATEMENT_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>For Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 5;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Repeat_StatementImpl <em>Repeat Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Repeat_StatementImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getRepeat_Statement()
   * @generated
   */
  int REPEAT_STATEMENT = 29;

  /**
   * The feature id for the '<em><b>Statements</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPEAT_STATEMENT__STATEMENTS = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPEAT_STATEMENT__EXPRESSION = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Repeat Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPEAT_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.While_StatementImpl <em>While Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.While_StatementImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getWhile_Statement()
   * @generated
   */
  int WHILE_STATEMENT = 30;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHILE_STATEMENT__EXPRESSION = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Statements</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHILE_STATEMENT__STATEMENTS = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>While Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHILE_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Subprog_Ctrl_StmtImpl <em>Subprog Ctrl Stmt</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Subprog_Ctrl_StmtImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getSubprog_Ctrl_Stmt()
   * @generated
   */
  int SUBPROG_CTRL_STMT = 31;

  /**
   * The feature id for the '<em><b>Call</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUBPROG_CTRL_STMT__CALL = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Subprog Ctrl Stmt</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUBPROG_CTRL_STMT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Func_CallImpl <em>Func Call</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Func_CallImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getFunc_Call()
   * @generated
   */
  int FUNC_CALL = 33;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNC_CALL__LEFT = EXPRESSION__LEFT;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNC_CALL__OPERATOR = EXPRESSION__OPERATOR;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNC_CALL__RIGHT = EXPRESSION__RIGHT;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNC_CALL__EXPRESSION = EXPRESSION__EXPRESSION;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNC_CALL__NAME = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Paramslist</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNC_CALL__PARAMSLIST = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Func Call</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNC_CALL_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Param_AssignImpl <em>Param Assign</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Param_AssignImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getParam_Assign()
   * @generated
   */
  int PARAM_ASSIGN = 34;

  /**
   * The feature id for the '<em><b>Varname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAM_ASSIGN__VARNAME = 0;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAM_ASSIGN__EXPR = 1;

  /**
   * The number of structural features of the '<em>Param Assign</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAM_ASSIGN_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.ConstantImpl <em>Constant</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.ConstantImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getConstant()
   * @generated
   */
  int CONSTANT = 35;

  /**
   * The feature id for the '<em><b>Num</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT__NUM = ARRAY_INITIAL_ELEMENTS__NUM;

  /**
   * The feature id for the '<em><b>Array Element</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT__ARRAY_ELEMENT = ARRAY_INITIAL_ELEMENTS__ARRAY_ELEMENT;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT__LEFT = ARRAY_INITIAL_ELEMENTS_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT__OPERATOR = ARRAY_INITIAL_ELEMENTS_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT__RIGHT = ARRAY_INITIAL_ELEMENTS_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT__EXPRESSION = ARRAY_INITIAL_ELEMENTS_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT__VALUE = ARRAY_INITIAL_ELEMENTS_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>Constant</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT_FEATURE_COUNT = ARRAY_INITIAL_ELEMENTS_FEATURE_COUNT + 5;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Numeric_LiteralImpl <em>Numeric Literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Numeric_LiteralImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getNumeric_Literal()
   * @generated
   */
  int NUMERIC_LITERAL = 36;

  /**
   * The feature id for the '<em><b>Num</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMERIC_LITERAL__NUM = CONSTANT__NUM;

  /**
   * The feature id for the '<em><b>Array Element</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMERIC_LITERAL__ARRAY_ELEMENT = CONSTANT__ARRAY_ELEMENT;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMERIC_LITERAL__LEFT = CONSTANT__LEFT;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMERIC_LITERAL__OPERATOR = CONSTANT__OPERATOR;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMERIC_LITERAL__RIGHT = CONSTANT__RIGHT;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMERIC_LITERAL__EXPRESSION = CONSTANT__EXPRESSION;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMERIC_LITERAL__VALUE = CONSTANT__VALUE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMERIC_LITERAL__TYPE = CONSTANT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Numeric Literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMERIC_LITERAL_FEATURE_COUNT = CONSTANT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.IntegerLiteralImpl <em>Integer Literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.IntegerLiteralImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getIntegerLiteral()
   * @generated
   */
  int INTEGER_LITERAL = 37;

  /**
   * The feature id for the '<em><b>Num</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_LITERAL__NUM = NUMERIC_LITERAL__NUM;

  /**
   * The feature id for the '<em><b>Array Element</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_LITERAL__ARRAY_ELEMENT = NUMERIC_LITERAL__ARRAY_ELEMENT;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_LITERAL__LEFT = NUMERIC_LITERAL__LEFT;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_LITERAL__OPERATOR = NUMERIC_LITERAL__OPERATOR;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_LITERAL__RIGHT = NUMERIC_LITERAL__RIGHT;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_LITERAL__EXPRESSION = NUMERIC_LITERAL__EXPRESSION;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_LITERAL__VALUE = NUMERIC_LITERAL__VALUE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_LITERAL__TYPE = NUMERIC_LITERAL__TYPE;

  /**
   * The number of structural features of the '<em>Integer Literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_LITERAL_FEATURE_COUNT = NUMERIC_LITERAL_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Real_LiteralImpl <em>Real Literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Real_LiteralImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getReal_Literal()
   * @generated
   */
  int REAL_LITERAL = 38;

  /**
   * The feature id for the '<em><b>Num</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REAL_LITERAL__NUM = NUMERIC_LITERAL__NUM;

  /**
   * The feature id for the '<em><b>Array Element</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REAL_LITERAL__ARRAY_ELEMENT = NUMERIC_LITERAL__ARRAY_ELEMENT;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REAL_LITERAL__LEFT = NUMERIC_LITERAL__LEFT;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REAL_LITERAL__OPERATOR = NUMERIC_LITERAL__OPERATOR;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REAL_LITERAL__RIGHT = NUMERIC_LITERAL__RIGHT;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REAL_LITERAL__EXPRESSION = NUMERIC_LITERAL__EXPRESSION;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REAL_LITERAL__VALUE = NUMERIC_LITERAL__VALUE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REAL_LITERAL__TYPE = NUMERIC_LITERAL__TYPE;

  /**
   * The feature id for the '<em><b>Exponent</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REAL_LITERAL__EXPONENT = NUMERIC_LITERAL_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Real Literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REAL_LITERAL_FEATURE_COUNT = NUMERIC_LITERAL_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Boolean_LiteralImpl <em>Boolean Literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Boolean_LiteralImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getBoolean_Literal()
   * @generated
   */
  int BOOLEAN_LITERAL = 39;

  /**
   * The feature id for the '<em><b>Num</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_LITERAL__NUM = CONSTANT__NUM;

  /**
   * The feature id for the '<em><b>Array Element</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_LITERAL__ARRAY_ELEMENT = CONSTANT__ARRAY_ELEMENT;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_LITERAL__LEFT = CONSTANT__LEFT;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_LITERAL__OPERATOR = CONSTANT__OPERATOR;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_LITERAL__RIGHT = CONSTANT__RIGHT;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_LITERAL__EXPRESSION = CONSTANT__EXPRESSION;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_LITERAL__VALUE = CONSTANT__VALUE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_LITERAL__TYPE = CONSTANT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Boolean Literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_LITERAL_FEATURE_COUNT = CONSTANT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Character_StringImpl <em>Character String</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Character_StringImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getCharacter_String()
   * @generated
   */
  int CHARACTER_STRING = 40;

  /**
   * The feature id for the '<em><b>Num</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHARACTER_STRING__NUM = CONSTANT__NUM;

  /**
   * The feature id for the '<em><b>Array Element</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHARACTER_STRING__ARRAY_ELEMENT = CONSTANT__ARRAY_ELEMENT;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHARACTER_STRING__LEFT = CONSTANT__LEFT;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHARACTER_STRING__OPERATOR = CONSTANT__OPERATOR;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHARACTER_STRING__RIGHT = CONSTANT__RIGHT;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHARACTER_STRING__EXPRESSION = CONSTANT__EXPRESSION;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHARACTER_STRING__VALUE = CONSTANT__VALUE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHARACTER_STRING__TYPE = CONSTANT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Character String</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHARACTER_STRING_FEATURE_COUNT = CONSTANT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Single_Byte_Character_String_LiteralImpl <em>Single Byte Character String Literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Single_Byte_Character_String_LiteralImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getSingle_Byte_Character_String_Literal()
   * @generated
   */
  int SINGLE_BYTE_CHARACTER_STRING_LITERAL = 41;

  /**
   * The feature id for the '<em><b>Num</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_BYTE_CHARACTER_STRING_LITERAL__NUM = CHARACTER_STRING__NUM;

  /**
   * The feature id for the '<em><b>Array Element</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_BYTE_CHARACTER_STRING_LITERAL__ARRAY_ELEMENT = CHARACTER_STRING__ARRAY_ELEMENT;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_BYTE_CHARACTER_STRING_LITERAL__LEFT = CHARACTER_STRING__LEFT;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_BYTE_CHARACTER_STRING_LITERAL__OPERATOR = CHARACTER_STRING__OPERATOR;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_BYTE_CHARACTER_STRING_LITERAL__RIGHT = CHARACTER_STRING__RIGHT;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_BYTE_CHARACTER_STRING_LITERAL__EXPRESSION = CHARACTER_STRING__EXPRESSION;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_BYTE_CHARACTER_STRING_LITERAL__VALUE = CHARACTER_STRING__VALUE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_BYTE_CHARACTER_STRING_LITERAL__TYPE = CHARACTER_STRING__TYPE;

  /**
   * The number of structural features of the '<em>Single Byte Character String Literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_BYTE_CHARACTER_STRING_LITERAL_FEATURE_COUNT = CHARACTER_STRING_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Double_Byte_Character_String_LiteralImpl <em>Double Byte Character String Literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Double_Byte_Character_String_LiteralImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getDouble_Byte_Character_String_Literal()
   * @generated
   */
  int DOUBLE_BYTE_CHARACTER_STRING_LITERAL = 42;

  /**
   * The feature id for the '<em><b>Num</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOUBLE_BYTE_CHARACTER_STRING_LITERAL__NUM = CHARACTER_STRING__NUM;

  /**
   * The feature id for the '<em><b>Array Element</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOUBLE_BYTE_CHARACTER_STRING_LITERAL__ARRAY_ELEMENT = CHARACTER_STRING__ARRAY_ELEMENT;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOUBLE_BYTE_CHARACTER_STRING_LITERAL__LEFT = CHARACTER_STRING__LEFT;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOUBLE_BYTE_CHARACTER_STRING_LITERAL__OPERATOR = CHARACTER_STRING__OPERATOR;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOUBLE_BYTE_CHARACTER_STRING_LITERAL__RIGHT = CHARACTER_STRING__RIGHT;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOUBLE_BYTE_CHARACTER_STRING_LITERAL__EXPRESSION = CHARACTER_STRING__EXPRESSION;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOUBLE_BYTE_CHARACTER_STRING_LITERAL__VALUE = CHARACTER_STRING__VALUE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOUBLE_BYTE_CHARACTER_STRING_LITERAL__TYPE = CHARACTER_STRING__TYPE;

  /**
   * The number of structural features of the '<em>Double Byte Character String Literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOUBLE_BYTE_CHARACTER_STRING_LITERAL_FEATURE_COUNT = CHARACTER_STRING_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Time_LiteralImpl <em>Time Literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Time_LiteralImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getTime_Literal()
   * @generated
   */
  int TIME_LITERAL = 43;

  /**
   * The feature id for the '<em><b>Num</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIME_LITERAL__NUM = CONSTANT__NUM;

  /**
   * The feature id for the '<em><b>Array Element</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIME_LITERAL__ARRAY_ELEMENT = CONSTANT__ARRAY_ELEMENT;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIME_LITERAL__LEFT = CONSTANT__LEFT;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIME_LITERAL__OPERATOR = CONSTANT__OPERATOR;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIME_LITERAL__RIGHT = CONSTANT__RIGHT;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIME_LITERAL__EXPRESSION = CONSTANT__EXPRESSION;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIME_LITERAL__VALUE = CONSTANT__VALUE;

  /**
   * The number of structural features of the '<em>Time Literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIME_LITERAL_FEATURE_COUNT = CONSTANT_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.DurationImpl <em>Duration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.DurationImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getDuration()
   * @generated
   */
  int DURATION = 44;

  /**
   * The feature id for the '<em><b>Num</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DURATION__NUM = TIME_LITERAL__NUM;

  /**
   * The feature id for the '<em><b>Array Element</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DURATION__ARRAY_ELEMENT = TIME_LITERAL__ARRAY_ELEMENT;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DURATION__LEFT = TIME_LITERAL__LEFT;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DURATION__OPERATOR = TIME_LITERAL__OPERATOR;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DURATION__RIGHT = TIME_LITERAL__RIGHT;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DURATION__EXPRESSION = TIME_LITERAL__EXPRESSION;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DURATION__VALUE = TIME_LITERAL__VALUE;

  /**
   * The number of structural features of the '<em>Duration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DURATION_FEATURE_COUNT = TIME_LITERAL_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Time_Of_DayImpl <em>Time Of Day</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Time_Of_DayImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getTime_Of_Day()
   * @generated
   */
  int TIME_OF_DAY = 45;

  /**
   * The feature id for the '<em><b>Num</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIME_OF_DAY__NUM = TIME_LITERAL__NUM;

  /**
   * The feature id for the '<em><b>Array Element</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIME_OF_DAY__ARRAY_ELEMENT = TIME_LITERAL__ARRAY_ELEMENT;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIME_OF_DAY__LEFT = TIME_LITERAL__LEFT;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIME_OF_DAY__OPERATOR = TIME_LITERAL__OPERATOR;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIME_OF_DAY__RIGHT = TIME_LITERAL__RIGHT;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIME_OF_DAY__EXPRESSION = TIME_LITERAL__EXPRESSION;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIME_OF_DAY__VALUE = TIME_LITERAL__VALUE;

  /**
   * The number of structural features of the '<em>Time Of Day</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIME_OF_DAY_FEATURE_COUNT = TIME_LITERAL_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.DateImpl <em>Date</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.DateImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getDate()
   * @generated
   */
  int DATE = 46;

  /**
   * The feature id for the '<em><b>Num</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATE__NUM = TIME_LITERAL__NUM;

  /**
   * The feature id for the '<em><b>Array Element</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATE__ARRAY_ELEMENT = TIME_LITERAL__ARRAY_ELEMENT;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATE__LEFT = TIME_LITERAL__LEFT;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATE__OPERATOR = TIME_LITERAL__OPERATOR;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATE__RIGHT = TIME_LITERAL__RIGHT;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATE__EXPRESSION = TIME_LITERAL__EXPRESSION;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATE__VALUE = TIME_LITERAL__VALUE;

  /**
   * The number of structural features of the '<em>Date</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATE_FEATURE_COUNT = TIME_LITERAL_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Date_And_TimeImpl <em>Date And Time</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Date_And_TimeImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getDate_And_Time()
   * @generated
   */
  int DATE_AND_TIME = 47;

  /**
   * The feature id for the '<em><b>Num</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATE_AND_TIME__NUM = TIME_LITERAL__NUM;

  /**
   * The feature id for the '<em><b>Array Element</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATE_AND_TIME__ARRAY_ELEMENT = TIME_LITERAL__ARRAY_ELEMENT;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATE_AND_TIME__LEFT = TIME_LITERAL__LEFT;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATE_AND_TIME__OPERATOR = TIME_LITERAL__OPERATOR;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATE_AND_TIME__RIGHT = TIME_LITERAL__RIGHT;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATE_AND_TIME__EXPRESSION = TIME_LITERAL__EXPRESSION;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATE_AND_TIME__VALUE = TIME_LITERAL__VALUE;

  /**
   * The number of structural features of the '<em>Date And Time</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATE_AND_TIME_FEATURE_COUNT = TIME_LITERAL_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Non_Generic_Type_NameImpl <em>Non Generic Type Name</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Non_Generic_Type_NameImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getNon_Generic_Type_Name()
   * @generated
   */
  int NON_GENERIC_TYPE_NAME = 48;

  /**
   * The number of structural features of the '<em>Non Generic Type Name</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NON_GENERIC_TYPE_NAME_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Elementary_Type_NameImpl <em>Elementary Type Name</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Elementary_Type_NameImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getElementary_Type_Name()
   * @generated
   */
  int ELEMENTARY_TYPE_NAME = 49;

  /**
   * The number of structural features of the '<em>Elementary Type Name</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELEMENTARY_TYPE_NAME_FEATURE_COUNT = NON_GENERIC_TYPE_NAME_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Numeric_Type_NameImpl <em>Numeric Type Name</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Numeric_Type_NameImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getNumeric_Type_Name()
   * @generated
   */
  int NUMERIC_TYPE_NAME = 50;

  /**
   * The number of structural features of the '<em>Numeric Type Name</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMERIC_TYPE_NAME_FEATURE_COUNT = ELEMENTARY_TYPE_NAME_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Integer_Type_NameImpl <em>Integer Type Name</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Integer_Type_NameImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getInteger_Type_Name()
   * @generated
   */
  int INTEGER_TYPE_NAME = 51;

  /**
   * The number of structural features of the '<em>Integer Type Name</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_TYPE_NAME_FEATURE_COUNT = NUMERIC_TYPE_NAME_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Signed_Integer_Type_NameImpl <em>Signed Integer Type Name</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Signed_Integer_Type_NameImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getSigned_Integer_Type_Name()
   * @generated
   */
  int SIGNED_INTEGER_TYPE_NAME = 52;

  /**
   * The number of structural features of the '<em>Signed Integer Type Name</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIGNED_INTEGER_TYPE_NAME_FEATURE_COUNT = INTEGER_TYPE_NAME_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Unsigned_Integer_Type_NameImpl <em>Unsigned Integer Type Name</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Unsigned_Integer_Type_NameImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getUnsigned_Integer_Type_Name()
   * @generated
   */
  int UNSIGNED_INTEGER_TYPE_NAME = 53;

  /**
   * The number of structural features of the '<em>Unsigned Integer Type Name</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNSIGNED_INTEGER_TYPE_NAME_FEATURE_COUNT = INTEGER_TYPE_NAME_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Real_Type_NameImpl <em>Real Type Name</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Real_Type_NameImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getReal_Type_Name()
   * @generated
   */
  int REAL_TYPE_NAME = 54;

  /**
   * The number of structural features of the '<em>Real Type Name</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REAL_TYPE_NAME_FEATURE_COUNT = NUMERIC_TYPE_NAME_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Bit_String_Type_NameImpl <em>Bit String Type Name</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Bit_String_Type_NameImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getBit_String_Type_Name()
   * @generated
   */
  int BIT_STRING_TYPE_NAME = 55;

  /**
   * The number of structural features of the '<em>Bit String Type Name</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BIT_STRING_TYPE_NAME_FEATURE_COUNT = ELEMENTARY_TYPE_NAME_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Time_Type_NameImpl <em>Time Type Name</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Time_Type_NameImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getTime_Type_Name()
   * @generated
   */
  int TIME_TYPE_NAME = 56;

  /**
   * The number of structural features of the '<em>Time Type Name</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIME_TYPE_NAME_FEATURE_COUNT = ELEMENTARY_TYPE_NAME_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Date_Type_NameImpl <em>Date Type Name</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Date_Type_NameImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getDate_Type_Name()
   * @generated
   */
  int DATE_TYPE_NAME = 57;

  /**
   * The number of structural features of the '<em>Date Type Name</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATE_TYPE_NAME_FEATURE_COUNT = ELEMENTARY_TYPE_NAME_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Xor_ExpressionImpl <em>Xor Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Xor_ExpressionImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getXor_Expression()
   * @generated
   */
  int XOR_EXPRESSION = 58;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int XOR_EXPRESSION__LEFT = EXPRESSION__LEFT;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int XOR_EXPRESSION__OPERATOR = EXPRESSION__OPERATOR;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int XOR_EXPRESSION__RIGHT = EXPRESSION__RIGHT;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int XOR_EXPRESSION__EXPRESSION = EXPRESSION__EXPRESSION;

  /**
   * The number of structural features of the '<em>Xor Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int XOR_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.And_ExpressionImpl <em>And Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.And_ExpressionImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getAnd_Expression()
   * @generated
   */
  int AND_EXPRESSION = 59;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_EXPRESSION__LEFT = EXPRESSION__LEFT;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_EXPRESSION__OPERATOR = EXPRESSION__OPERATOR;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_EXPRESSION__RIGHT = EXPRESSION__RIGHT;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_EXPRESSION__EXPRESSION = EXPRESSION__EXPRESSION;

  /**
   * The number of structural features of the '<em>And Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.ComparisonImpl <em>Comparison</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.ComparisonImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getComparison()
   * @generated
   */
  int COMPARISON = 60;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON__LEFT = EXPRESSION__LEFT;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON__OPERATOR = EXPRESSION__OPERATOR;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON__RIGHT = EXPRESSION__RIGHT;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON__EXPRESSION = EXPRESSION__EXPRESSION;

  /**
   * The number of structural features of the '<em>Comparison</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Equ_ExpressionImpl <em>Equ Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Equ_ExpressionImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getEqu_Expression()
   * @generated
   */
  int EQU_EXPRESSION = 61;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQU_EXPRESSION__LEFT = EXPRESSION__LEFT;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQU_EXPRESSION__OPERATOR = EXPRESSION__OPERATOR;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQU_EXPRESSION__RIGHT = EXPRESSION__RIGHT;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQU_EXPRESSION__EXPRESSION = EXPRESSION__EXPRESSION;

  /**
   * The number of structural features of the '<em>Equ Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQU_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Add_ExpressionImpl <em>Add Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Add_ExpressionImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getAdd_Expression()
   * @generated
   */
  int ADD_EXPRESSION = 62;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_EXPRESSION__LEFT = EXPRESSION__LEFT;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_EXPRESSION__OPERATOR = EXPRESSION__OPERATOR;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_EXPRESSION__RIGHT = EXPRESSION__RIGHT;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_EXPRESSION__EXPRESSION = EXPRESSION__EXPRESSION;

  /**
   * The number of structural features of the '<em>Add Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.TermImpl <em>Term</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.TermImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getTerm()
   * @generated
   */
  int TERM = 63;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM__LEFT = EXPRESSION__LEFT;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM__OPERATOR = EXPRESSION__OPERATOR;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM__RIGHT = EXPRESSION__RIGHT;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM__EXPRESSION = EXPRESSION__EXPRESSION;

  /**
   * The number of structural features of the '<em>Term</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Power_ExpressionImpl <em>Power Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Power_ExpressionImpl
   * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getPower_Expression()
   * @generated
   */
  int POWER_EXPRESSION = 64;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POWER_EXPRESSION__LEFT = EXPRESSION__LEFT;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POWER_EXPRESSION__OPERATOR = EXPRESSION__OPERATOR;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POWER_EXPRESSION__RIGHT = EXPRESSION__RIGHT;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POWER_EXPRESSION__EXPRESSION = EXPRESSION__EXPRESSION;

  /**
   * The number of structural features of the '<em>Power Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POWER_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;


  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.structuredTextAlgorithm <em>structured Text Algorithm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>structured Text Algorithm</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.structuredTextAlgorithm
   * @generated
   */
  EClass getstructuredTextAlgorithm();

  /**
   * Returns the meta object for the containment reference list '{@link org.fordiac.ide.model.structuredtext.structuredText.structuredTextAlgorithm#getAdapters <em>Adapters</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Adapters</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.structuredTextAlgorithm#getAdapters()
   * @see #getstructuredTextAlgorithm()
   * @generated
   */
  EReference getstructuredTextAlgorithm_Adapters();

  /**
   * Returns the meta object for the containment reference list '{@link org.fordiac.ide.model.structuredtext.structuredText.structuredTextAlgorithm#getVarDeclarations <em>Var Declarations</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Var Declarations</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.structuredTextAlgorithm#getVarDeclarations()
   * @see #getstructuredTextAlgorithm()
   * @generated
   */
  EReference getstructuredTextAlgorithm_VarDeclarations();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.structuredTextAlgorithm#getStatements <em>Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statements</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.structuredTextAlgorithm#getStatements()
   * @see #getstructuredTextAlgorithm()
   * @generated
   */
  EReference getstructuredTextAlgorithm_Statements();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.VarDeclaration <em>Var Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Var Declaration</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.VarDeclaration
   * @generated
   */
  EClass getVarDeclaration();

  /**
   * Returns the meta object for the attribute '{@link org.fordiac.ide.model.structuredtext.structuredText.VarDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.VarDeclaration#getName()
   * @see #getVarDeclaration()
   * @generated
   */
  EAttribute getVarDeclaration_Name();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.VarDeclaration#getSpecification <em>Specification</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Specification</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.VarDeclaration#getSpecification()
   * @see #getVarDeclaration()
   * @generated
   */
  EReference getVarDeclaration_Specification();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.AdapterDefinition <em>Adapter Definition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Adapter Definition</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.AdapterDefinition
   * @generated
   */
  EClass getAdapterDefinition();

  /**
   * Returns the meta object for the attribute '{@link org.fordiac.ide.model.structuredtext.structuredText.AdapterDefinition#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.AdapterDefinition#getName()
   * @see #getAdapterDefinition()
   * @generated
   */
  EAttribute getAdapterDefinition_Name();

  /**
   * Returns the meta object for the containment reference list '{@link org.fordiac.ide.model.structuredtext.structuredText.AdapterDefinition#getVarDeclarations <em>Var Declarations</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Var Declarations</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.AdapterDefinition#getVarDeclarations()
   * @see #getAdapterDefinition()
   * @generated
   */
  EReference getAdapterDefinition_VarDeclarations();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.AdapterDeclaration <em>Adapter Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Adapter Declaration</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.AdapterDeclaration
   * @generated
   */
  EClass getAdapterDeclaration();

  /**
   * Returns the meta object for the attribute '{@link org.fordiac.ide.model.structuredtext.structuredText.AdapterDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.AdapterDeclaration#getName()
   * @see #getAdapterDeclaration()
   * @generated
   */
  EAttribute getAdapterDeclaration_Name();

  /**
   * Returns the meta object for the reference '{@link org.fordiac.ide.model.structuredtext.structuredText.AdapterDeclaration#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Type</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.AdapterDeclaration#getType()
   * @see #getAdapterDeclaration()
   * @generated
   */
  EReference getAdapterDeclaration_Type();

  /**
   * Returns the meta object for the containment reference list '{@link org.fordiac.ide.model.structuredtext.structuredText.AdapterDeclaration#getParameters <em>Parameters</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Parameters</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.AdapterDeclaration#getParameters()
   * @see #getAdapterDeclaration()
   * @generated
   */
  EReference getAdapterDeclaration_Parameters();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Parameter <em>Parameter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Parameter</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Parameter
   * @generated
   */
  EClass getParameter();

  /**
   * Returns the meta object for the reference '{@link org.fordiac.ide.model.structuredtext.structuredText.Parameter#getParam <em>Param</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Param</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Parameter#getParam()
   * @see #getParameter()
   * @generated
   */
  EReference getParameter_Param();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.Parameter#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Parameter#getValue()
   * @see #getParameter()
   * @generated
   */
  EReference getParameter_Value();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Simple_Spec_Init <em>Simple Spec Init</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Simple Spec Init</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Simple_Spec_Init
   * @generated
   */
  EClass getSimple_Spec_Init();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.Simple_Spec_Init#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Simple_Spec_Init#getType()
   * @see #getSimple_Spec_Init()
   * @generated
   */
  EReference getSimple_Spec_Init_Type();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.Simple_Spec_Init#getConstant <em>Constant</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Constant</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Simple_Spec_Init#getConstant()
   * @see #getSimple_Spec_Init()
   * @generated
   */
  EReference getSimple_Spec_Init_Constant();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Spec_Init <em>Array Spec Init</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Array Spec Init</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Array_Spec_Init
   * @generated
   */
  EClass getArray_Spec_Init();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Specification <em>Array Specification</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Array Specification</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Array_Specification
   * @generated
   */
  EClass getArray_Specification();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Specification#getInitialization <em>Initialization</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Initialization</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Array_Specification#getInitialization()
   * @see #getArray_Specification()
   * @generated
   */
  EReference getArray_Specification_Initialization();

  /**
   * Returns the meta object for the containment reference list '{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Specification#getSubrange <em>Subrange</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Subrange</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Array_Specification#getSubrange()
   * @see #getArray_Specification()
   * @generated
   */
  EReference getArray_Specification_Subrange();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Specification#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Array_Specification#getType()
   * @see #getArray_Specification()
   * @generated
   */
  EReference getArray_Specification_Type();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Subrange <em>Subrange</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Subrange</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Subrange
   * @generated
   */
  EClass getSubrange();

  /**
   * Returns the meta object for the attribute '{@link org.fordiac.ide.model.structuredtext.structuredText.Subrange#getMin <em>Min</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Min</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Subrange#getMin()
   * @see #getSubrange()
   * @generated
   */
  EAttribute getSubrange_Min();

  /**
   * Returns the meta object for the attribute '{@link org.fordiac.ide.model.structuredtext.structuredText.Subrange#getMax <em>Max</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Max</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Subrange#getMax()
   * @see #getSubrange()
   * @generated
   */
  EAttribute getSubrange_Max();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Initialization <em>Array Initialization</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Array Initialization</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Array_Initialization
   * @generated
   */
  EClass getArray_Initialization();

  /**
   * Returns the meta object for the containment reference list '{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Initialization#getInitElement <em>Init Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Init Element</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Array_Initialization#getInitElement()
   * @see #getArray_Initialization()
   * @generated
   */
  EReference getArray_Initialization_InitElement();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Initial_Elements <em>Array Initial Elements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Array Initial Elements</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Array_Initial_Elements
   * @generated
   */
  EClass getArray_Initial_Elements();

  /**
   * Returns the meta object for the attribute '{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Initial_Elements#getNum <em>Num</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Num</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Array_Initial_Elements#getNum()
   * @see #getArray_Initial_Elements()
   * @generated
   */
  EAttribute getArray_Initial_Elements_Num();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Initial_Elements#getArrayElement <em>Array Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Array Element</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Array_Initial_Elements#getArrayElement()
   * @see #getArray_Initial_Elements()
   * @generated
   */
  EReference getArray_Initial_Elements_ArrayElement();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Structure_Spec <em>Structure Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Structure Spec</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Structure_Spec
   * @generated
   */
  EClass getStructure_Spec();

  /**
   * Returns the meta object for the attribute '{@link org.fordiac.ide.model.structuredtext.structuredText.Structure_Spec#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Structure_Spec#getName()
   * @see #getStructure_Spec()
   * @generated
   */
  EAttribute getStructure_Spec_Name();

  /**
   * Returns the meta object for the containment reference list '{@link org.fordiac.ide.model.structuredtext.structuredText.Structure_Spec#getElement <em>Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Element</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Structure_Spec#getElement()
   * @see #getStructure_Spec()
   * @generated
   */
  EReference getStructure_Spec_Element();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.S_Byte_String_Spec <em>SByte String Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>SByte String Spec</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.S_Byte_String_Spec
   * @generated
   */
  EClass getS_Byte_String_Spec();

  /**
   * Returns the meta object for the attribute '{@link org.fordiac.ide.model.structuredtext.structuredText.S_Byte_String_Spec#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.S_Byte_String_Spec#getType()
   * @see #getS_Byte_String_Spec()
   * @generated
   */
  EAttribute getS_Byte_String_Spec_Type();

  /**
   * Returns the meta object for the attribute '{@link org.fordiac.ide.model.structuredtext.structuredText.S_Byte_String_Spec#getSize <em>Size</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Size</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.S_Byte_String_Spec#getSize()
   * @see #getS_Byte_String_Spec()
   * @generated
   */
  EAttribute getS_Byte_String_Spec_Size();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.S_Byte_String_Spec#getString <em>String</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>String</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.S_Byte_String_Spec#getString()
   * @see #getS_Byte_String_Spec()
   * @generated
   */
  EReference getS_Byte_String_Spec_String();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.D_Byte_String_Spec <em>DByte String Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>DByte String Spec</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.D_Byte_String_Spec
   * @generated
   */
  EClass getD_Byte_String_Spec();

  /**
   * Returns the meta object for the attribute '{@link org.fordiac.ide.model.structuredtext.structuredText.D_Byte_String_Spec#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.D_Byte_String_Spec#getType()
   * @see #getD_Byte_String_Spec()
   * @generated
   */
  EAttribute getD_Byte_String_Spec_Type();

  /**
   * Returns the meta object for the attribute '{@link org.fordiac.ide.model.structuredtext.structuredText.D_Byte_String_Spec#getSize <em>Size</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Size</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.D_Byte_String_Spec#getSize()
   * @see #getD_Byte_String_Spec()
   * @generated
   */
  EAttribute getD_Byte_String_Spec_Size();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.D_Byte_String_Spec#getString <em>String</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>String</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.D_Byte_String_Spec#getString()
   * @see #getD_Byte_String_Spec()
   * @generated
   */
  EReference getD_Byte_String_Spec_String();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.StatementList <em>Statement List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Statement List</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.StatementList
   * @generated
   */
  EClass getStatementList();

  /**
   * Returns the meta object for the containment reference list '{@link org.fordiac.ide.model.structuredtext.structuredText.StatementList#getStatements <em>Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Statements</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.StatementList#getStatements()
   * @see #getStatementList()
   * @generated
   */
  EReference getStatementList_Statements();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Statement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Statement</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Statement
   * @generated
   */
  EClass getStatement();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Assignment_Statement <em>Assignment Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Assignment Statement</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Assignment_Statement
   * @generated
   */
  EClass getAssignment_Statement();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.Assignment_Statement#getVariable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Variable</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Assignment_Statement#getVariable()
   * @see #getAssignment_Statement()
   * @generated
   */
  EReference getAssignment_Statement_Variable();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.Assignment_Statement#getExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Assignment_Statement#getExpression()
   * @see #getAssignment_Statement()
   * @generated
   */
  EReference getAssignment_Statement_Expression();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Variable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Variable</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Variable
   * @generated
   */
  EClass getVariable();

  /**
   * Returns the meta object for the reference '{@link org.fordiac.ide.model.structuredtext.structuredText.Variable#getVar <em>Var</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Var</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Variable#getVar()
   * @see #getVariable()
   * @generated
   */
  EReference getVariable_Var();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Var_Variable <em>Var Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Var Variable</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Var_Variable
   * @generated
   */
  EClass getVar_Variable();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Adapter_Variable <em>Adapter Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Adapter Variable</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Adapter_Variable
   * @generated
   */
  EClass getAdapter_Variable();

  /**
   * Returns the meta object for the reference '{@link org.fordiac.ide.model.structuredtext.structuredText.Adapter_Variable#getAdapter <em>Adapter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Adapter</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Adapter_Variable#getAdapter()
   * @see #getAdapter_Variable()
   * @generated
   */
  EReference getAdapter_Variable_Adapter();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Variable <em>Array Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Array Variable</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Array_Variable
   * @generated
   */
  EClass getArray_Variable();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Variable#getNum <em>Num</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Num</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Array_Variable#getNum()
   * @see #getArray_Variable()
   * @generated
   */
  EReference getArray_Variable_Num();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Selection_Statement <em>Selection Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Selection Statement</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Selection_Statement
   * @generated
   */
  EClass getSelection_Statement();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.Selection_Statement#getExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Selection_Statement#getExpression()
   * @see #getSelection_Statement()
   * @generated
   */
  EReference getSelection_Statement_Expression();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.If_Statement <em>If Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>If Statement</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.If_Statement
   * @generated
   */
  EClass getIf_Statement();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.If_Statement#getStatments <em>Statments</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statments</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.If_Statement#getStatments()
   * @see #getIf_Statement()
   * @generated
   */
  EReference getIf_Statement_Statments();

  /**
   * Returns the meta object for the containment reference list '{@link org.fordiac.ide.model.structuredtext.structuredText.If_Statement#getElseif <em>Elseif</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Elseif</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.If_Statement#getElseif()
   * @see #getIf_Statement()
   * @generated
   */
  EReference getIf_Statement_Elseif();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.If_Statement#getElseStatements <em>Else Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Else Statements</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.If_Statement#getElseStatements()
   * @see #getIf_Statement()
   * @generated
   */
  EReference getIf_Statement_ElseStatements();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.ElseIf_Statement <em>Else If Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Else If Statement</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.ElseIf_Statement
   * @generated
   */
  EClass getElseIf_Statement();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.ElseIf_Statement#getExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.ElseIf_Statement#getExpression()
   * @see #getElseIf_Statement()
   * @generated
   */
  EReference getElseIf_Statement_Expression();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.ElseIf_Statement#getStatements <em>Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statements</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.ElseIf_Statement#getStatements()
   * @see #getElseIf_Statement()
   * @generated
   */
  EReference getElseIf_Statement_Statements();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Case_Statement <em>Case Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Case Statement</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Case_Statement
   * @generated
   */
  EClass getCase_Statement();

  /**
   * Returns the meta object for the containment reference list '{@link org.fordiac.ide.model.structuredtext.structuredText.Case_Statement#getCaseElements <em>Case Elements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Case Elements</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Case_Statement#getCaseElements()
   * @see #getCase_Statement()
   * @generated
   */
  EReference getCase_Statement_CaseElements();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.Case_Statement#getCaseElse <em>Case Else</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Case Else</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Case_Statement#getCaseElse()
   * @see #getCase_Statement()
   * @generated
   */
  EReference getCase_Statement_CaseElse();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Case_Element <em>Case Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Case Element</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Case_Element
   * @generated
   */
  EClass getCase_Element();

  /**
   * Returns the meta object for the attribute '{@link org.fordiac.ide.model.structuredtext.structuredText.Case_Element#getCase <em>Case</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Case</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Case_Element#getCase()
   * @see #getCase_Element()
   * @generated
   */
  EAttribute getCase_Element_Case();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.Case_Element#getStatements <em>Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statements</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Case_Element#getStatements()
   * @see #getCase_Element()
   * @generated
   */
  EReference getCase_Element_Statements();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Case_Else <em>Case Else</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Case Else</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Case_Else
   * @generated
   */
  EClass getCase_Else();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.Case_Else#getStatements <em>Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statements</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Case_Else#getStatements()
   * @see #getCase_Else()
   * @generated
   */
  EReference getCase_Else_Statements();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.IterationControl_Stmt <em>Iteration Control Stmt</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Iteration Control Stmt</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.IterationControl_Stmt
   * @generated
   */
  EClass getIterationControl_Stmt();

  /**
   * Returns the meta object for the attribute '{@link org.fordiac.ide.model.structuredtext.structuredText.IterationControl_Stmt#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Statement</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.IterationControl_Stmt#getStatement()
   * @see #getIterationControl_Stmt()
   * @generated
   */
  EAttribute getIterationControl_Stmt_Statement();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.For_Statement <em>For Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>For Statement</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.For_Statement
   * @generated
   */
  EClass getFor_Statement();

  /**
   * Returns the meta object for the reference '{@link org.fordiac.ide.model.structuredtext.structuredText.For_Statement#getControlVariable <em>Control Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Control Variable</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.For_Statement#getControlVariable()
   * @see #getFor_Statement()
   * @generated
   */
  EReference getFor_Statement_ControlVariable();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.For_Statement#getExpressionFrom <em>Expression From</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression From</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.For_Statement#getExpressionFrom()
   * @see #getFor_Statement()
   * @generated
   */
  EReference getFor_Statement_ExpressionFrom();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.For_Statement#getExpressionTo <em>Expression To</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression To</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.For_Statement#getExpressionTo()
   * @see #getFor_Statement()
   * @generated
   */
  EReference getFor_Statement_ExpressionTo();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.For_Statement#getExpressionBy <em>Expression By</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression By</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.For_Statement#getExpressionBy()
   * @see #getFor_Statement()
   * @generated
   */
  EReference getFor_Statement_ExpressionBy();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.For_Statement#getStatements <em>Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statements</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.For_Statement#getStatements()
   * @see #getFor_Statement()
   * @generated
   */
  EReference getFor_Statement_Statements();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Repeat_Statement <em>Repeat Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Repeat Statement</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Repeat_Statement
   * @generated
   */
  EClass getRepeat_Statement();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.Repeat_Statement#getStatements <em>Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statements</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Repeat_Statement#getStatements()
   * @see #getRepeat_Statement()
   * @generated
   */
  EReference getRepeat_Statement_Statements();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.Repeat_Statement#getExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Repeat_Statement#getExpression()
   * @see #getRepeat_Statement()
   * @generated
   */
  EReference getRepeat_Statement_Expression();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.While_Statement <em>While Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>While Statement</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.While_Statement
   * @generated
   */
  EClass getWhile_Statement();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.While_Statement#getExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.While_Statement#getExpression()
   * @see #getWhile_Statement()
   * @generated
   */
  EReference getWhile_Statement_Expression();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.While_Statement#getStatements <em>Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statements</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.While_Statement#getStatements()
   * @see #getWhile_Statement()
   * @generated
   */
  EReference getWhile_Statement_Statements();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Subprog_Ctrl_Stmt <em>Subprog Ctrl Stmt</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Subprog Ctrl Stmt</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Subprog_Ctrl_Stmt
   * @generated
   */
  EClass getSubprog_Ctrl_Stmt();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.Subprog_Ctrl_Stmt#getCall <em>Call</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Call</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Subprog_Ctrl_Stmt#getCall()
   * @see #getSubprog_Ctrl_Stmt()
   * @generated
   */
  EReference getSubprog_Ctrl_Stmt_Call();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Expression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Expression
   * @generated
   */
  EClass getExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.Expression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Expression#getLeft()
   * @see #getExpression()
   * @generated
   */
  EReference getExpression_Left();

  /**
   * Returns the meta object for the attribute '{@link org.fordiac.ide.model.structuredtext.structuredText.Expression#getOperator <em>Operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Operator</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Expression#getOperator()
   * @see #getExpression()
   * @generated
   */
  EAttribute getExpression_Operator();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.Expression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Expression#getRight()
   * @see #getExpression()
   * @generated
   */
  EReference getExpression_Right();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.Expression#getExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Expression#getExpression()
   * @see #getExpression()
   * @generated
   */
  EReference getExpression_Expression();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Func_Call <em>Func Call</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Func Call</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Func_Call
   * @generated
   */
  EClass getFunc_Call();

  /**
   * Returns the meta object for the attribute '{@link org.fordiac.ide.model.structuredtext.structuredText.Func_Call#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Func_Call#getName()
   * @see #getFunc_Call()
   * @generated
   */
  EAttribute getFunc_Call_Name();

  /**
   * Returns the meta object for the containment reference list '{@link org.fordiac.ide.model.structuredtext.structuredText.Func_Call#getParamslist <em>Paramslist</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Paramslist</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Func_Call#getParamslist()
   * @see #getFunc_Call()
   * @generated
   */
  EReference getFunc_Call_Paramslist();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Param_Assign <em>Param Assign</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Param Assign</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Param_Assign
   * @generated
   */
  EClass getParam_Assign();

  /**
   * Returns the meta object for the attribute '{@link org.fordiac.ide.model.structuredtext.structuredText.Param_Assign#getVarname <em>Varname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Varname</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Param_Assign#getVarname()
   * @see #getParam_Assign()
   * @generated
   */
  EAttribute getParam_Assign_Varname();

  /**
   * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.structuredtext.structuredText.Param_Assign#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expr</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Param_Assign#getExpr()
   * @see #getParam_Assign()
   * @generated
   */
  EReference getParam_Assign_Expr();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Constant <em>Constant</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Constant</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Constant
   * @generated
   */
  EClass getConstant();

  /**
   * Returns the meta object for the attribute '{@link org.fordiac.ide.model.structuredtext.structuredText.Constant#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Constant#getValue()
   * @see #getConstant()
   * @generated
   */
  EAttribute getConstant_Value();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Numeric_Literal <em>Numeric Literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Numeric Literal</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Numeric_Literal
   * @generated
   */
  EClass getNumeric_Literal();

  /**
   * Returns the meta object for the attribute '{@link org.fordiac.ide.model.structuredtext.structuredText.Numeric_Literal#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Numeric_Literal#getType()
   * @see #getNumeric_Literal()
   * @generated
   */
  EAttribute getNumeric_Literal_Type();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.IntegerLiteral <em>Integer Literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Integer Literal</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.IntegerLiteral
   * @generated
   */
  EClass getIntegerLiteral();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Real_Literal <em>Real Literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Real Literal</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Real_Literal
   * @generated
   */
  EClass getReal_Literal();

  /**
   * Returns the meta object for the attribute '{@link org.fordiac.ide.model.structuredtext.structuredText.Real_Literal#getExponent <em>Exponent</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Exponent</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Real_Literal#getExponent()
   * @see #getReal_Literal()
   * @generated
   */
  EAttribute getReal_Literal_Exponent();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Boolean_Literal <em>Boolean Literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean Literal</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Boolean_Literal
   * @generated
   */
  EClass getBoolean_Literal();

  /**
   * Returns the meta object for the attribute '{@link org.fordiac.ide.model.structuredtext.structuredText.Boolean_Literal#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Boolean_Literal#getType()
   * @see #getBoolean_Literal()
   * @generated
   */
  EAttribute getBoolean_Literal_Type();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Character_String <em>Character String</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Character String</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Character_String
   * @generated
   */
  EClass getCharacter_String();

  /**
   * Returns the meta object for the attribute '{@link org.fordiac.ide.model.structuredtext.structuredText.Character_String#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Character_String#getType()
   * @see #getCharacter_String()
   * @generated
   */
  EAttribute getCharacter_String_Type();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Single_Byte_Character_String_Literal <em>Single Byte Character String Literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Single Byte Character String Literal</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Single_Byte_Character_String_Literal
   * @generated
   */
  EClass getSingle_Byte_Character_String_Literal();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Double_Byte_Character_String_Literal <em>Double Byte Character String Literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Double Byte Character String Literal</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Double_Byte_Character_String_Literal
   * @generated
   */
  EClass getDouble_Byte_Character_String_Literal();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Time_Literal <em>Time Literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Time Literal</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Time_Literal
   * @generated
   */
  EClass getTime_Literal();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Duration <em>Duration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Duration</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Duration
   * @generated
   */
  EClass getDuration();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Time_Of_Day <em>Time Of Day</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Time Of Day</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Time_Of_Day
   * @generated
   */
  EClass getTime_Of_Day();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Date <em>Date</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Date</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Date
   * @generated
   */
  EClass getDate();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Date_And_Time <em>Date And Time</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Date And Time</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Date_And_Time
   * @generated
   */
  EClass getDate_And_Time();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Non_Generic_Type_Name <em>Non Generic Type Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Non Generic Type Name</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Non_Generic_Type_Name
   * @generated
   */
  EClass getNon_Generic_Type_Name();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Elementary_Type_Name <em>Elementary Type Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Elementary Type Name</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Elementary_Type_Name
   * @generated
   */
  EClass getElementary_Type_Name();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Numeric_Type_Name <em>Numeric Type Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Numeric Type Name</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Numeric_Type_Name
   * @generated
   */
  EClass getNumeric_Type_Name();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Integer_Type_Name <em>Integer Type Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Integer Type Name</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Integer_Type_Name
   * @generated
   */
  EClass getInteger_Type_Name();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Signed_Integer_Type_Name <em>Signed Integer Type Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Signed Integer Type Name</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Signed_Integer_Type_Name
   * @generated
   */
  EClass getSigned_Integer_Type_Name();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Unsigned_Integer_Type_Name <em>Unsigned Integer Type Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Unsigned Integer Type Name</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Unsigned_Integer_Type_Name
   * @generated
   */
  EClass getUnsigned_Integer_Type_Name();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Real_Type_Name <em>Real Type Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Real Type Name</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Real_Type_Name
   * @generated
   */
  EClass getReal_Type_Name();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Bit_String_Type_Name <em>Bit String Type Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Bit String Type Name</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Bit_String_Type_Name
   * @generated
   */
  EClass getBit_String_Type_Name();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Time_Type_Name <em>Time Type Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Time Type Name</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Time_Type_Name
   * @generated
   */
  EClass getTime_Type_Name();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Date_Type_Name <em>Date Type Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Date Type Name</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Date_Type_Name
   * @generated
   */
  EClass getDate_Type_Name();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Xor_Expression <em>Xor Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Xor Expression</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Xor_Expression
   * @generated
   */
  EClass getXor_Expression();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.And_Expression <em>And Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>And Expression</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.And_Expression
   * @generated
   */
  EClass getAnd_Expression();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Comparison <em>Comparison</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Comparison</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Comparison
   * @generated
   */
  EClass getComparison();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Equ_Expression <em>Equ Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Equ Expression</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Equ_Expression
   * @generated
   */
  EClass getEqu_Expression();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Add_Expression <em>Add Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Add Expression</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Add_Expression
   * @generated
   */
  EClass getAdd_Expression();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Term <em>Term</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Term</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Term
   * @generated
   */
  EClass getTerm();

  /**
   * Returns the meta object for class '{@link org.fordiac.ide.model.structuredtext.structuredText.Power_Expression <em>Power Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Power Expression</em>'.
   * @see org.fordiac.ide.model.structuredtext.structuredText.Power_Expression
   * @generated
   */
  EClass getPower_Expression();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  StructuredTextFactory getStructuredTextFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.structuredTextAlgorithmImpl <em>structured Text Algorithm</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.structuredTextAlgorithmImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getstructuredTextAlgorithm()
     * @generated
     */
    EClass STRUCTURED_TEXT_ALGORITHM = eINSTANCE.getstructuredTextAlgorithm();

    /**
     * The meta object literal for the '<em><b>Adapters</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference STRUCTURED_TEXT_ALGORITHM__ADAPTERS = eINSTANCE.getstructuredTextAlgorithm_Adapters();

    /**
     * The meta object literal for the '<em><b>Var Declarations</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference STRUCTURED_TEXT_ALGORITHM__VAR_DECLARATIONS = eINSTANCE.getstructuredTextAlgorithm_VarDeclarations();

    /**
     * The meta object literal for the '<em><b>Statements</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference STRUCTURED_TEXT_ALGORITHM__STATEMENTS = eINSTANCE.getstructuredTextAlgorithm_Statements();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.VarDeclarationImpl <em>Var Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.VarDeclarationImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getVarDeclaration()
     * @generated
     */
    EClass VAR_DECLARATION = eINSTANCE.getVarDeclaration();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VAR_DECLARATION__NAME = eINSTANCE.getVarDeclaration_Name();

    /**
     * The meta object literal for the '<em><b>Specification</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VAR_DECLARATION__SPECIFICATION = eINSTANCE.getVarDeclaration_Specification();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.AdapterDefinitionImpl <em>Adapter Definition</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.AdapterDefinitionImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getAdapterDefinition()
     * @generated
     */
    EClass ADAPTER_DEFINITION = eINSTANCE.getAdapterDefinition();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ADAPTER_DEFINITION__NAME = eINSTANCE.getAdapterDefinition_Name();

    /**
     * The meta object literal for the '<em><b>Var Declarations</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ADAPTER_DEFINITION__VAR_DECLARATIONS = eINSTANCE.getAdapterDefinition_VarDeclarations();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.AdapterDeclarationImpl <em>Adapter Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.AdapterDeclarationImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getAdapterDeclaration()
     * @generated
     */
    EClass ADAPTER_DECLARATION = eINSTANCE.getAdapterDeclaration();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ADAPTER_DECLARATION__NAME = eINSTANCE.getAdapterDeclaration_Name();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ADAPTER_DECLARATION__TYPE = eINSTANCE.getAdapterDeclaration_Type();

    /**
     * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ADAPTER_DECLARATION__PARAMETERS = eINSTANCE.getAdapterDeclaration_Parameters();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.ParameterImpl <em>Parameter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.ParameterImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getParameter()
     * @generated
     */
    EClass PARAMETER = eINSTANCE.getParameter();

    /**
     * The meta object literal for the '<em><b>Param</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PARAMETER__PARAM = eINSTANCE.getParameter_Param();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PARAMETER__VALUE = eINSTANCE.getParameter_Value();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Simple_Spec_InitImpl <em>Simple Spec Init</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Simple_Spec_InitImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getSimple_Spec_Init()
     * @generated
     */
    EClass SIMPLE_SPEC_INIT = eINSTANCE.getSimple_Spec_Init();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SIMPLE_SPEC_INIT__TYPE = eINSTANCE.getSimple_Spec_Init_Type();

    /**
     * The meta object literal for the '<em><b>Constant</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SIMPLE_SPEC_INIT__CONSTANT = eINSTANCE.getSimple_Spec_Init_Constant();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Array_Spec_InitImpl <em>Array Spec Init</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Array_Spec_InitImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getArray_Spec_Init()
     * @generated
     */
    EClass ARRAY_SPEC_INIT = eINSTANCE.getArray_Spec_Init();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Array_SpecificationImpl <em>Array Specification</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Array_SpecificationImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getArray_Specification()
     * @generated
     */
    EClass ARRAY_SPECIFICATION = eINSTANCE.getArray_Specification();

    /**
     * The meta object literal for the '<em><b>Initialization</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ARRAY_SPECIFICATION__INITIALIZATION = eINSTANCE.getArray_Specification_Initialization();

    /**
     * The meta object literal for the '<em><b>Subrange</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ARRAY_SPECIFICATION__SUBRANGE = eINSTANCE.getArray_Specification_Subrange();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ARRAY_SPECIFICATION__TYPE = eINSTANCE.getArray_Specification_Type();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.SubrangeImpl <em>Subrange</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.SubrangeImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getSubrange()
     * @generated
     */
    EClass SUBRANGE = eINSTANCE.getSubrange();

    /**
     * The meta object literal for the '<em><b>Min</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SUBRANGE__MIN = eINSTANCE.getSubrange_Min();

    /**
     * The meta object literal for the '<em><b>Max</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SUBRANGE__MAX = eINSTANCE.getSubrange_Max();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Array_InitializationImpl <em>Array Initialization</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Array_InitializationImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getArray_Initialization()
     * @generated
     */
    EClass ARRAY_INITIALIZATION = eINSTANCE.getArray_Initialization();

    /**
     * The meta object literal for the '<em><b>Init Element</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ARRAY_INITIALIZATION__INIT_ELEMENT = eINSTANCE.getArray_Initialization_InitElement();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Array_Initial_ElementsImpl <em>Array Initial Elements</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Array_Initial_ElementsImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getArray_Initial_Elements()
     * @generated
     */
    EClass ARRAY_INITIAL_ELEMENTS = eINSTANCE.getArray_Initial_Elements();

    /**
     * The meta object literal for the '<em><b>Num</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ARRAY_INITIAL_ELEMENTS__NUM = eINSTANCE.getArray_Initial_Elements_Num();

    /**
     * The meta object literal for the '<em><b>Array Element</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ARRAY_INITIAL_ELEMENTS__ARRAY_ELEMENT = eINSTANCE.getArray_Initial_Elements_ArrayElement();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Structure_SpecImpl <em>Structure Spec</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Structure_SpecImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getStructure_Spec()
     * @generated
     */
    EClass STRUCTURE_SPEC = eINSTANCE.getStructure_Spec();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute STRUCTURE_SPEC__NAME = eINSTANCE.getStructure_Spec_Name();

    /**
     * The meta object literal for the '<em><b>Element</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference STRUCTURE_SPEC__ELEMENT = eINSTANCE.getStructure_Spec_Element();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.S_Byte_String_SpecImpl <em>SByte String Spec</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.S_Byte_String_SpecImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getS_Byte_String_Spec()
     * @generated
     */
    EClass SBYTE_STRING_SPEC = eINSTANCE.getS_Byte_String_Spec();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SBYTE_STRING_SPEC__TYPE = eINSTANCE.getS_Byte_String_Spec_Type();

    /**
     * The meta object literal for the '<em><b>Size</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SBYTE_STRING_SPEC__SIZE = eINSTANCE.getS_Byte_String_Spec_Size();

    /**
     * The meta object literal for the '<em><b>String</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SBYTE_STRING_SPEC__STRING = eINSTANCE.getS_Byte_String_Spec_String();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.D_Byte_String_SpecImpl <em>DByte String Spec</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.D_Byte_String_SpecImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getD_Byte_String_Spec()
     * @generated
     */
    EClass DBYTE_STRING_SPEC = eINSTANCE.getD_Byte_String_Spec();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DBYTE_STRING_SPEC__TYPE = eINSTANCE.getD_Byte_String_Spec_Type();

    /**
     * The meta object literal for the '<em><b>Size</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DBYTE_STRING_SPEC__SIZE = eINSTANCE.getD_Byte_String_Spec_Size();

    /**
     * The meta object literal for the '<em><b>String</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DBYTE_STRING_SPEC__STRING = eINSTANCE.getD_Byte_String_Spec_String();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.StatementListImpl <em>Statement List</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StatementListImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getStatementList()
     * @generated
     */
    EClass STATEMENT_LIST = eINSTANCE.getStatementList();

    /**
     * The meta object literal for the '<em><b>Statements</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference STATEMENT_LIST__STATEMENTS = eINSTANCE.getStatementList_Statements();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.StatementImpl <em>Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StatementImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getStatement()
     * @generated
     */
    EClass STATEMENT = eINSTANCE.getStatement();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Assignment_StatementImpl <em>Assignment Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Assignment_StatementImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getAssignment_Statement()
     * @generated
     */
    EClass ASSIGNMENT_STATEMENT = eINSTANCE.getAssignment_Statement();

    /**
     * The meta object literal for the '<em><b>Variable</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ASSIGNMENT_STATEMENT__VARIABLE = eINSTANCE.getAssignment_Statement_Variable();

    /**
     * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ASSIGNMENT_STATEMENT__EXPRESSION = eINSTANCE.getAssignment_Statement_Expression();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.VariableImpl <em>Variable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.VariableImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getVariable()
     * @generated
     */
    EClass VARIABLE = eINSTANCE.getVariable();

    /**
     * The meta object literal for the '<em><b>Var</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VARIABLE__VAR = eINSTANCE.getVariable_Var();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Var_VariableImpl <em>Var Variable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Var_VariableImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getVar_Variable()
     * @generated
     */
    EClass VAR_VARIABLE = eINSTANCE.getVar_Variable();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Adapter_VariableImpl <em>Adapter Variable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Adapter_VariableImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getAdapter_Variable()
     * @generated
     */
    EClass ADAPTER_VARIABLE = eINSTANCE.getAdapter_Variable();

    /**
     * The meta object literal for the '<em><b>Adapter</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ADAPTER_VARIABLE__ADAPTER = eINSTANCE.getAdapter_Variable_Adapter();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Array_VariableImpl <em>Array Variable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Array_VariableImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getArray_Variable()
     * @generated
     */
    EClass ARRAY_VARIABLE = eINSTANCE.getArray_Variable();

    /**
     * The meta object literal for the '<em><b>Num</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ARRAY_VARIABLE__NUM = eINSTANCE.getArray_Variable_Num();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Selection_StatementImpl <em>Selection Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Selection_StatementImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getSelection_Statement()
     * @generated
     */
    EClass SELECTION_STATEMENT = eINSTANCE.getSelection_Statement();

    /**
     * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SELECTION_STATEMENT__EXPRESSION = eINSTANCE.getSelection_Statement_Expression();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.If_StatementImpl <em>If Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.If_StatementImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getIf_Statement()
     * @generated
     */
    EClass IF_STATEMENT = eINSTANCE.getIf_Statement();

    /**
     * The meta object literal for the '<em><b>Statments</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IF_STATEMENT__STATMENTS = eINSTANCE.getIf_Statement_Statments();

    /**
     * The meta object literal for the '<em><b>Elseif</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IF_STATEMENT__ELSEIF = eINSTANCE.getIf_Statement_Elseif();

    /**
     * The meta object literal for the '<em><b>Else Statements</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IF_STATEMENT__ELSE_STATEMENTS = eINSTANCE.getIf_Statement_ElseStatements();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.ElseIf_StatementImpl <em>Else If Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.ElseIf_StatementImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getElseIf_Statement()
     * @generated
     */
    EClass ELSE_IF_STATEMENT = eINSTANCE.getElseIf_Statement();

    /**
     * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ELSE_IF_STATEMENT__EXPRESSION = eINSTANCE.getElseIf_Statement_Expression();

    /**
     * The meta object literal for the '<em><b>Statements</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ELSE_IF_STATEMENT__STATEMENTS = eINSTANCE.getElseIf_Statement_Statements();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Case_StatementImpl <em>Case Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Case_StatementImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getCase_Statement()
     * @generated
     */
    EClass CASE_STATEMENT = eINSTANCE.getCase_Statement();

    /**
     * The meta object literal for the '<em><b>Case Elements</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CASE_STATEMENT__CASE_ELEMENTS = eINSTANCE.getCase_Statement_CaseElements();

    /**
     * The meta object literal for the '<em><b>Case Else</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CASE_STATEMENT__CASE_ELSE = eINSTANCE.getCase_Statement_CaseElse();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Case_ElementImpl <em>Case Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Case_ElementImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getCase_Element()
     * @generated
     */
    EClass CASE_ELEMENT = eINSTANCE.getCase_Element();

    /**
     * The meta object literal for the '<em><b>Case</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CASE_ELEMENT__CASE = eINSTANCE.getCase_Element_Case();

    /**
     * The meta object literal for the '<em><b>Statements</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CASE_ELEMENT__STATEMENTS = eINSTANCE.getCase_Element_Statements();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Case_ElseImpl <em>Case Else</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Case_ElseImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getCase_Else()
     * @generated
     */
    EClass CASE_ELSE = eINSTANCE.getCase_Else();

    /**
     * The meta object literal for the '<em><b>Statements</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CASE_ELSE__STATEMENTS = eINSTANCE.getCase_Else_Statements();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.IterationControl_StmtImpl <em>Iteration Control Stmt</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.IterationControl_StmtImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getIterationControl_Stmt()
     * @generated
     */
    EClass ITERATION_CONTROL_STMT = eINSTANCE.getIterationControl_Stmt();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ITERATION_CONTROL_STMT__STATEMENT = eINSTANCE.getIterationControl_Stmt_Statement();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.For_StatementImpl <em>For Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.For_StatementImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getFor_Statement()
     * @generated
     */
    EClass FOR_STATEMENT = eINSTANCE.getFor_Statement();

    /**
     * The meta object literal for the '<em><b>Control Variable</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FOR_STATEMENT__CONTROL_VARIABLE = eINSTANCE.getFor_Statement_ControlVariable();

    /**
     * The meta object literal for the '<em><b>Expression From</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FOR_STATEMENT__EXPRESSION_FROM = eINSTANCE.getFor_Statement_ExpressionFrom();

    /**
     * The meta object literal for the '<em><b>Expression To</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FOR_STATEMENT__EXPRESSION_TO = eINSTANCE.getFor_Statement_ExpressionTo();

    /**
     * The meta object literal for the '<em><b>Expression By</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FOR_STATEMENT__EXPRESSION_BY = eINSTANCE.getFor_Statement_ExpressionBy();

    /**
     * The meta object literal for the '<em><b>Statements</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FOR_STATEMENT__STATEMENTS = eINSTANCE.getFor_Statement_Statements();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Repeat_StatementImpl <em>Repeat Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Repeat_StatementImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getRepeat_Statement()
     * @generated
     */
    EClass REPEAT_STATEMENT = eINSTANCE.getRepeat_Statement();

    /**
     * The meta object literal for the '<em><b>Statements</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference REPEAT_STATEMENT__STATEMENTS = eINSTANCE.getRepeat_Statement_Statements();

    /**
     * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference REPEAT_STATEMENT__EXPRESSION = eINSTANCE.getRepeat_Statement_Expression();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.While_StatementImpl <em>While Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.While_StatementImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getWhile_Statement()
     * @generated
     */
    EClass WHILE_STATEMENT = eINSTANCE.getWhile_Statement();

    /**
     * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference WHILE_STATEMENT__EXPRESSION = eINSTANCE.getWhile_Statement_Expression();

    /**
     * The meta object literal for the '<em><b>Statements</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference WHILE_STATEMENT__STATEMENTS = eINSTANCE.getWhile_Statement_Statements();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Subprog_Ctrl_StmtImpl <em>Subprog Ctrl Stmt</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Subprog_Ctrl_StmtImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getSubprog_Ctrl_Stmt()
     * @generated
     */
    EClass SUBPROG_CTRL_STMT = eINSTANCE.getSubprog_Ctrl_Stmt();

    /**
     * The meta object literal for the '<em><b>Call</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SUBPROG_CTRL_STMT__CALL = eINSTANCE.getSubprog_Ctrl_Stmt_Call();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.ExpressionImpl <em>Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.ExpressionImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getExpression()
     * @generated
     */
    EClass EXPRESSION = eINSTANCE.getExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPRESSION__LEFT = eINSTANCE.getExpression_Left();

    /**
     * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute EXPRESSION__OPERATOR = eINSTANCE.getExpression_Operator();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPRESSION__RIGHT = eINSTANCE.getExpression_Right();

    /**
     * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPRESSION__EXPRESSION = eINSTANCE.getExpression_Expression();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Func_CallImpl <em>Func Call</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Func_CallImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getFunc_Call()
     * @generated
     */
    EClass FUNC_CALL = eINSTANCE.getFunc_Call();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FUNC_CALL__NAME = eINSTANCE.getFunc_Call_Name();

    /**
     * The meta object literal for the '<em><b>Paramslist</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FUNC_CALL__PARAMSLIST = eINSTANCE.getFunc_Call_Paramslist();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Param_AssignImpl <em>Param Assign</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Param_AssignImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getParam_Assign()
     * @generated
     */
    EClass PARAM_ASSIGN = eINSTANCE.getParam_Assign();

    /**
     * The meta object literal for the '<em><b>Varname</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PARAM_ASSIGN__VARNAME = eINSTANCE.getParam_Assign_Varname();

    /**
     * The meta object literal for the '<em><b>Expr</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PARAM_ASSIGN__EXPR = eINSTANCE.getParam_Assign_Expr();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.ConstantImpl <em>Constant</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.ConstantImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getConstant()
     * @generated
     */
    EClass CONSTANT = eINSTANCE.getConstant();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CONSTANT__VALUE = eINSTANCE.getConstant_Value();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Numeric_LiteralImpl <em>Numeric Literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Numeric_LiteralImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getNumeric_Literal()
     * @generated
     */
    EClass NUMERIC_LITERAL = eINSTANCE.getNumeric_Literal();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NUMERIC_LITERAL__TYPE = eINSTANCE.getNumeric_Literal_Type();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.IntegerLiteralImpl <em>Integer Literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.IntegerLiteralImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getIntegerLiteral()
     * @generated
     */
    EClass INTEGER_LITERAL = eINSTANCE.getIntegerLiteral();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Real_LiteralImpl <em>Real Literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Real_LiteralImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getReal_Literal()
     * @generated
     */
    EClass REAL_LITERAL = eINSTANCE.getReal_Literal();

    /**
     * The meta object literal for the '<em><b>Exponent</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute REAL_LITERAL__EXPONENT = eINSTANCE.getReal_Literal_Exponent();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Boolean_LiteralImpl <em>Boolean Literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Boolean_LiteralImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getBoolean_Literal()
     * @generated
     */
    EClass BOOLEAN_LITERAL = eINSTANCE.getBoolean_Literal();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BOOLEAN_LITERAL__TYPE = eINSTANCE.getBoolean_Literal_Type();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Character_StringImpl <em>Character String</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Character_StringImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getCharacter_String()
     * @generated
     */
    EClass CHARACTER_STRING = eINSTANCE.getCharacter_String();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CHARACTER_STRING__TYPE = eINSTANCE.getCharacter_String_Type();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Single_Byte_Character_String_LiteralImpl <em>Single Byte Character String Literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Single_Byte_Character_String_LiteralImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getSingle_Byte_Character_String_Literal()
     * @generated
     */
    EClass SINGLE_BYTE_CHARACTER_STRING_LITERAL = eINSTANCE.getSingle_Byte_Character_String_Literal();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Double_Byte_Character_String_LiteralImpl <em>Double Byte Character String Literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Double_Byte_Character_String_LiteralImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getDouble_Byte_Character_String_Literal()
     * @generated
     */
    EClass DOUBLE_BYTE_CHARACTER_STRING_LITERAL = eINSTANCE.getDouble_Byte_Character_String_Literal();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Time_LiteralImpl <em>Time Literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Time_LiteralImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getTime_Literal()
     * @generated
     */
    EClass TIME_LITERAL = eINSTANCE.getTime_Literal();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.DurationImpl <em>Duration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.DurationImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getDuration()
     * @generated
     */
    EClass DURATION = eINSTANCE.getDuration();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Time_Of_DayImpl <em>Time Of Day</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Time_Of_DayImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getTime_Of_Day()
     * @generated
     */
    EClass TIME_OF_DAY = eINSTANCE.getTime_Of_Day();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.DateImpl <em>Date</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.DateImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getDate()
     * @generated
     */
    EClass DATE = eINSTANCE.getDate();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Date_And_TimeImpl <em>Date And Time</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Date_And_TimeImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getDate_And_Time()
     * @generated
     */
    EClass DATE_AND_TIME = eINSTANCE.getDate_And_Time();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Non_Generic_Type_NameImpl <em>Non Generic Type Name</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Non_Generic_Type_NameImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getNon_Generic_Type_Name()
     * @generated
     */
    EClass NON_GENERIC_TYPE_NAME = eINSTANCE.getNon_Generic_Type_Name();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Elementary_Type_NameImpl <em>Elementary Type Name</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Elementary_Type_NameImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getElementary_Type_Name()
     * @generated
     */
    EClass ELEMENTARY_TYPE_NAME = eINSTANCE.getElementary_Type_Name();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Numeric_Type_NameImpl <em>Numeric Type Name</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Numeric_Type_NameImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getNumeric_Type_Name()
     * @generated
     */
    EClass NUMERIC_TYPE_NAME = eINSTANCE.getNumeric_Type_Name();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Integer_Type_NameImpl <em>Integer Type Name</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Integer_Type_NameImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getInteger_Type_Name()
     * @generated
     */
    EClass INTEGER_TYPE_NAME = eINSTANCE.getInteger_Type_Name();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Signed_Integer_Type_NameImpl <em>Signed Integer Type Name</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Signed_Integer_Type_NameImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getSigned_Integer_Type_Name()
     * @generated
     */
    EClass SIGNED_INTEGER_TYPE_NAME = eINSTANCE.getSigned_Integer_Type_Name();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Unsigned_Integer_Type_NameImpl <em>Unsigned Integer Type Name</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Unsigned_Integer_Type_NameImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getUnsigned_Integer_Type_Name()
     * @generated
     */
    EClass UNSIGNED_INTEGER_TYPE_NAME = eINSTANCE.getUnsigned_Integer_Type_Name();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Real_Type_NameImpl <em>Real Type Name</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Real_Type_NameImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getReal_Type_Name()
     * @generated
     */
    EClass REAL_TYPE_NAME = eINSTANCE.getReal_Type_Name();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Bit_String_Type_NameImpl <em>Bit String Type Name</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Bit_String_Type_NameImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getBit_String_Type_Name()
     * @generated
     */
    EClass BIT_STRING_TYPE_NAME = eINSTANCE.getBit_String_Type_Name();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Time_Type_NameImpl <em>Time Type Name</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Time_Type_NameImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getTime_Type_Name()
     * @generated
     */
    EClass TIME_TYPE_NAME = eINSTANCE.getTime_Type_Name();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Date_Type_NameImpl <em>Date Type Name</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Date_Type_NameImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getDate_Type_Name()
     * @generated
     */
    EClass DATE_TYPE_NAME = eINSTANCE.getDate_Type_Name();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Xor_ExpressionImpl <em>Xor Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Xor_ExpressionImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getXor_Expression()
     * @generated
     */
    EClass XOR_EXPRESSION = eINSTANCE.getXor_Expression();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.And_ExpressionImpl <em>And Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.And_ExpressionImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getAnd_Expression()
     * @generated
     */
    EClass AND_EXPRESSION = eINSTANCE.getAnd_Expression();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.ComparisonImpl <em>Comparison</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.ComparisonImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getComparison()
     * @generated
     */
    EClass COMPARISON = eINSTANCE.getComparison();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Equ_ExpressionImpl <em>Equ Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Equ_ExpressionImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getEqu_Expression()
     * @generated
     */
    EClass EQU_EXPRESSION = eINSTANCE.getEqu_Expression();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Add_ExpressionImpl <em>Add Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Add_ExpressionImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getAdd_Expression()
     * @generated
     */
    EClass ADD_EXPRESSION = eINSTANCE.getAdd_Expression();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.TermImpl <em>Term</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.TermImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getTerm()
     * @generated
     */
    EClass TERM = eINSTANCE.getTerm();

    /**
     * The meta object literal for the '{@link org.fordiac.ide.model.structuredtext.structuredText.impl.Power_ExpressionImpl <em>Power Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.Power_ExpressionImpl
     * @see org.fordiac.ide.model.structuredtext.structuredText.impl.StructuredTextPackageImpl#getPower_Expression()
     * @generated
     */
    EClass POWER_EXPRESSION = eINSTANCE.getPower_Expression();

  }

} //StructuredTextPackage
