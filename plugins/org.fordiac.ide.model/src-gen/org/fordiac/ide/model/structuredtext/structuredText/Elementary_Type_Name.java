/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Elementary Type Name</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getElementary_Type_Name()
 * @model
 * @generated
 */
public interface Elementary_Type_Name extends Non_Generic_Type_Name
{
} // Elementary_Type_Name
