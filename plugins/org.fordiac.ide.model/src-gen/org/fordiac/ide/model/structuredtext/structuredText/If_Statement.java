/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>If Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.If_Statement#getStatments <em>Statments</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.If_Statement#getElseif <em>Elseif</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.If_Statement#getElseStatements <em>Else Statements</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getIf_Statement()
 * @model
 * @generated
 */
public interface If_Statement extends Selection_Statement
{
  /**
   * Returns the value of the '<em><b>Statments</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Statments</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Statments</em>' containment reference.
   * @see #setStatments(StatementList)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getIf_Statement_Statments()
   * @model containment="true"
   * @generated
   */
  StatementList getStatments();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.If_Statement#getStatments <em>Statments</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Statments</em>' containment reference.
   * @see #getStatments()
   * @generated
   */
  void setStatments(StatementList value);

  /**
   * Returns the value of the '<em><b>Elseif</b></em>' containment reference list.
   * The list contents are of type {@link org.fordiac.ide.model.structuredtext.structuredText.ElseIf_Statement}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Elseif</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Elseif</em>' containment reference list.
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getIf_Statement_Elseif()
   * @model containment="true"
   * @generated
   */
  EList<ElseIf_Statement> getElseif();

  /**
   * Returns the value of the '<em><b>Else Statements</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Else Statements</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Else Statements</em>' containment reference.
   * @see #setElseStatements(StatementList)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getIf_Statement_ElseStatements()
   * @model containment="true"
   * @generated
   */
  StatementList getElseStatements();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.If_Statement#getElseStatements <em>Else Statements</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Else Statements</em>' containment reference.
   * @see #getElseStatements()
   * @generated
   */
  void setElseStatements(StatementList value);

} // If_Statement
