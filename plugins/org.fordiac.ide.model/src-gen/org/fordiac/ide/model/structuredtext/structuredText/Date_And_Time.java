/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Date And Time</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getDate_And_Time()
 * @model
 * @generated
 */
public interface Date_And_Time extends Time_Literal
{
} // Date_And_Time
