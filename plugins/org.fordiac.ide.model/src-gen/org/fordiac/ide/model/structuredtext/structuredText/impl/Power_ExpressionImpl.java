/**
 */
package org.fordiac.ide.model.structuredtext.structuredText.impl;

import org.eclipse.emf.ecore.EClass;

import org.fordiac.ide.model.structuredtext.structuredText.Power_Expression;
import org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Power Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class Power_ExpressionImpl extends ExpressionImpl implements Power_Expression
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected Power_ExpressionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return StructuredTextPackage.Literals.POWER_EXPRESSION;
  }

} //Power_ExpressionImpl
