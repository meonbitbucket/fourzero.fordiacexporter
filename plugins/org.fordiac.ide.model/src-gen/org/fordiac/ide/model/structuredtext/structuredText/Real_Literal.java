/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Real Literal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.Real_Literal#getExponent <em>Exponent</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getReal_Literal()
 * @model
 * @generated
 */
public interface Real_Literal extends Numeric_Literal
{
  /**
   * Returns the value of the '<em><b>Exponent</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Exponent</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Exponent</em>' attribute.
   * @see #setExponent(String)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getReal_Literal_Exponent()
   * @model
   * @generated
   */
  String getExponent();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.Real_Literal#getExponent <em>Exponent</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Exponent</em>' attribute.
   * @see #getExponent()
   * @generated
   */
  void setExponent(String value);

} // Real_Literal
