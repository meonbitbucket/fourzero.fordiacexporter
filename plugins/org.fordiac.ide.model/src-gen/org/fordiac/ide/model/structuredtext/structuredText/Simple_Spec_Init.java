/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple Spec Init</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.Simple_Spec_Init#getType <em>Type</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.Simple_Spec_Init#getConstant <em>Constant</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getSimple_Spec_Init()
 * @model
 * @generated
 */
public interface Simple_Spec_Init extends EObject
{
  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(Elementary_Type_Name)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getSimple_Spec_Init_Type()
   * @model containment="true"
   * @generated
   */
  Elementary_Type_Name getType();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.Simple_Spec_Init#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(Elementary_Type_Name value);

  /**
   * Returns the value of the '<em><b>Constant</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Constant</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Constant</em>' containment reference.
   * @see #setConstant(Constant)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getSimple_Spec_Init_Constant()
   * @model containment="true"
   * @generated
   */
  Constant getConstant();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.Simple_Spec_Init#getConstant <em>Constant</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Constant</em>' containment reference.
   * @see #getConstant()
   * @generated
   */
  void setConstant(Constant value);

} // Simple_Spec_Init
