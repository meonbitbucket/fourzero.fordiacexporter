/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DByte String Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.D_Byte_String_Spec#getType <em>Type</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.D_Byte_String_Spec#getSize <em>Size</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.D_Byte_String_Spec#getString <em>String</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getD_Byte_String_Spec()
 * @model
 * @generated
 */
public interface D_Byte_String_Spec extends EObject
{
  /**
   * Returns the value of the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' attribute.
   * @see #setType(String)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getD_Byte_String_Spec_Type()
   * @model
   * @generated
   */
  String getType();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.D_Byte_String_Spec#getType <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' attribute.
   * @see #getType()
   * @generated
   */
  void setType(String value);

  /**
   * Returns the value of the '<em><b>Size</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Size</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Size</em>' attribute.
   * @see #setSize(int)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getD_Byte_String_Spec_Size()
   * @model
   * @generated
   */
  int getSize();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.D_Byte_String_Spec#getSize <em>Size</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Size</em>' attribute.
   * @see #getSize()
   * @generated
   */
  void setSize(int value);

  /**
   * Returns the value of the '<em><b>String</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>String</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>String</em>' containment reference.
   * @see #setString(Double_Byte_Character_String_Literal)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getD_Byte_String_Spec_String()
   * @model containment="true"
   * @generated
   */
  Double_Byte_Character_String_Literal getString();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.D_Byte_String_Spec#getString <em>String</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>String</em>' containment reference.
   * @see #getString()
   * @generated
   */
  void setString(Double_Byte_Character_String_Literal value);

} // D_Byte_String_Spec
