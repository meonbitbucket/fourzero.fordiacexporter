/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Real Type Name</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getReal_Type_Name()
 * @model
 * @generated
 */
public interface Real_Type_Name extends Numeric_Type_Name
{
} // Real_Type_Name
