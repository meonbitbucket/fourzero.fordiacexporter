/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Time Type Name</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getTime_Type_Name()
 * @model
 * @generated
 */
public interface Time_Type_Name extends Elementary_Type_Name
{
} // Time_Type_Name
