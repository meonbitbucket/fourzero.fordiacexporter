/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Array Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Specification#getInitialization <em>Initialization</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Specification#getSubrange <em>Subrange</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Specification#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getArray_Specification()
 * @model
 * @generated
 */
public interface Array_Specification extends Array_Spec_Init
{
  /**
   * Returns the value of the '<em><b>Initialization</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Initialization</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Initialization</em>' containment reference.
   * @see #setInitialization(Array_Initialization)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getArray_Specification_Initialization()
   * @model containment="true"
   * @generated
   */
  Array_Initialization getInitialization();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Specification#getInitialization <em>Initialization</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Initialization</em>' containment reference.
   * @see #getInitialization()
   * @generated
   */
  void setInitialization(Array_Initialization value);

  /**
   * Returns the value of the '<em><b>Subrange</b></em>' containment reference list.
   * The list contents are of type {@link org.fordiac.ide.model.structuredtext.structuredText.Subrange}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Subrange</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Subrange</em>' containment reference list.
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getArray_Specification_Subrange()
   * @model containment="true"
   * @generated
   */
  EList<Subrange> getSubrange();

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(Non_Generic_Type_Name)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getArray_Specification_Type()
   * @model containment="true"
   * @generated
   */
  Non_Generic_Type_Name getType();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.Array_Specification#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(Non_Generic_Type_Name value);

} // Array_Specification
