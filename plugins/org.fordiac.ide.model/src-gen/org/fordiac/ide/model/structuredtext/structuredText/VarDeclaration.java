/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Var Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.VarDeclaration#getName <em>Name</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.VarDeclaration#getSpecification <em>Specification</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getVarDeclaration()
 * @model
 * @generated
 */
public interface VarDeclaration extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getVarDeclaration_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.VarDeclaration#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Specification</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Specification</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Specification</em>' containment reference.
   * @see #setSpecification(EObject)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getVarDeclaration_Specification()
   * @model containment="true"
   * @generated
   */
  EObject getSpecification();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.VarDeclaration#getSpecification <em>Specification</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Specification</em>' containment reference.
   * @see #getSpecification()
   * @generated
   */
  void setSpecification(EObject value);

} // VarDeclaration
