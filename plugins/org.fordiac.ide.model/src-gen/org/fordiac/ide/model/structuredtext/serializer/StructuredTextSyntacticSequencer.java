package org.fordiac.ide.model.structuredtext.serializer;

import com.google.inject.Inject;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.IGrammarAccess;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.AbstractElementAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.AlternativeAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.GroupAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.TokenAlias;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynNavigable;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynTransition;
import org.eclipse.xtext.serializer.sequencer.AbstractSyntacticSequencer;
import org.fordiac.ide.model.structuredtext.services.StructuredTextGrammarAccess;

@SuppressWarnings("all")
public class StructuredTextSyntacticSequencer extends AbstractSyntacticSequencer {

	protected StructuredTextGrammarAccess grammarAccess;
	protected AbstractElementAlias match_AdapterDefinition___VAR_INPUTKeyword_2_0_END_VARKeyword_2_2__q;
	protected AbstractElementAlias match_AdapterDefinition___VAR_OUTPUTKeyword_3_0_END_VARKeyword_3_2__q;
	protected AbstractElementAlias match_Bit_String_Type_Name_BOOLKeyword_1_3_or_BYTEKeyword_1_0_or_DWORDKeyword_1_2_or_LWORDKeyword_1_4_or_WORDKeyword_1_1;
	protected AbstractElementAlias match_Date_And_Time_DATE_AND_TIMEKeyword_0_0_or_DTKeyword_0_1;
	protected AbstractElementAlias match_Date_DATEKeyword_0_0_or_DKeyword_0_1;
	protected AbstractElementAlias match_Date_Type_Name_DATEKeyword_1_0_or_DATE_AND_TIMEKeyword_1_2_or_TIME_OF_DAYKeyword_1_1;
	protected AbstractElementAlias match_Duration_HyphenMinusKeyword_1_1_q_or_PlusSignKeyword_1_0_q;
	protected AbstractElementAlias match_Duration_TIMEKeyword_0_0_or_TKeyword_0_1;
	protected AbstractElementAlias match_Duration___HyphenMinusKeyword_1_1_or_PlusSignKeyword_1_0__q;
	protected AbstractElementAlias match_Param_Assign_NOTKeyword_1_0_q;
	protected AbstractElementAlias match_Primary_Expression_HyphenMinusKeyword_1_0_q;
	protected AbstractElementAlias match_Real_Literal_EKeyword_3_0_0_or_EKeyword_3_0_1;
	protected AbstractElementAlias match_Real_Literal___HyphenMinusKeyword_1_1_or_PlusSignKeyword_1_0__q;
	protected AbstractElementAlias match_Real_Type_Name_LREALKeyword_1_1_or_REALKeyword_1_0;
	protected AbstractElementAlias match_Signed_Integer_Type_Name_DINTKeyword_1_0_or_INTKeyword_1_1_or_LINTKeyword_1_3_or_SINTKeyword_1_2;
	protected AbstractElementAlias match_StatementList_SemicolonKeyword_1_1_a;
	protected AbstractElementAlias match_StatementList_SemicolonKeyword_1_1_p;
	protected AbstractElementAlias match_Time_Of_Day_TIME_OF_DAYKeyword_0_0_or_TODKeyword_0_1;
	protected AbstractElementAlias match_Time_Type_Name_LTIMEKeyword_1_1_or_TIMEKeyword_1_0;
	protected AbstractElementAlias match_Unsigned_Integer_Type_Name_UDINTKeyword_1_2_or_ULINTKeyword_1_3_or_UNITKeyword_1_0_or_USINTKeyword_1_1;
	protected AbstractElementAlias match_structuredTextAlgorithm___PLUGSKeyword_11_0_END_PLUGSKeyword_11_2__q;
	protected AbstractElementAlias match_structuredTextAlgorithm___SOCKETSKeyword_12_0_END_SOCKETSKeyword_12_2__q;
	protected AbstractElementAlias match_structuredTextAlgorithm___VARKeyword_13_0_END_VARKeyword_13_2__q;
	
	@Inject
	protected void init(IGrammarAccess access) {
		grammarAccess = (StructuredTextGrammarAccess) access;
		match_AdapterDefinition___VAR_INPUTKeyword_2_0_END_VARKeyword_2_2__q = new GroupAlias(false, true, new TokenAlias(false, false, grammarAccess.getAdapterDefinitionAccess().getVAR_INPUTKeyword_2_0()), new TokenAlias(false, false, grammarAccess.getAdapterDefinitionAccess().getEND_VARKeyword_2_2()));
		match_AdapterDefinition___VAR_OUTPUTKeyword_3_0_END_VARKeyword_3_2__q = new GroupAlias(false, true, new TokenAlias(false, false, grammarAccess.getAdapterDefinitionAccess().getVAR_OUTPUTKeyword_3_0()), new TokenAlias(false, false, grammarAccess.getAdapterDefinitionAccess().getEND_VARKeyword_3_2()));
		match_Bit_String_Type_Name_BOOLKeyword_1_3_or_BYTEKeyword_1_0_or_DWORDKeyword_1_2_or_LWORDKeyword_1_4_or_WORDKeyword_1_1 = new AlternativeAlias(false, false, new TokenAlias(false, false, grammarAccess.getBit_String_Type_NameAccess().getBOOLKeyword_1_3()), new TokenAlias(false, false, grammarAccess.getBit_String_Type_NameAccess().getBYTEKeyword_1_0()), new TokenAlias(false, false, grammarAccess.getBit_String_Type_NameAccess().getDWORDKeyword_1_2()), new TokenAlias(false, false, grammarAccess.getBit_String_Type_NameAccess().getLWORDKeyword_1_4()), new TokenAlias(false, false, grammarAccess.getBit_String_Type_NameAccess().getWORDKeyword_1_1()));
		match_Date_And_Time_DATE_AND_TIMEKeyword_0_0_or_DTKeyword_0_1 = new AlternativeAlias(false, false, new TokenAlias(false, false, grammarAccess.getDate_And_TimeAccess().getDATE_AND_TIMEKeyword_0_0()), new TokenAlias(false, false, grammarAccess.getDate_And_TimeAccess().getDTKeyword_0_1()));
		match_Date_DATEKeyword_0_0_or_DKeyword_0_1 = new AlternativeAlias(false, false, new TokenAlias(false, false, grammarAccess.getDateAccess().getDATEKeyword_0_0()), new TokenAlias(false, false, grammarAccess.getDateAccess().getDKeyword_0_1()));
		match_Date_Type_Name_DATEKeyword_1_0_or_DATE_AND_TIMEKeyword_1_2_or_TIME_OF_DAYKeyword_1_1 = new AlternativeAlias(false, false, new TokenAlias(false, false, grammarAccess.getDate_Type_NameAccess().getDATEKeyword_1_0()), new TokenAlias(false, false, grammarAccess.getDate_Type_NameAccess().getDATE_AND_TIMEKeyword_1_2()), new TokenAlias(false, false, grammarAccess.getDate_Type_NameAccess().getTIME_OF_DAYKeyword_1_1()));
		match_Duration_HyphenMinusKeyword_1_1_q_or_PlusSignKeyword_1_0_q = new AlternativeAlias(false, false, new TokenAlias(false, true, grammarAccess.getDurationAccess().getHyphenMinusKeyword_1_1()), new TokenAlias(false, true, grammarAccess.getDurationAccess().getPlusSignKeyword_1_0()));
		match_Duration_TIMEKeyword_0_0_or_TKeyword_0_1 = new AlternativeAlias(false, false, new TokenAlias(false, false, grammarAccess.getDurationAccess().getTIMEKeyword_0_0()), new TokenAlias(false, false, grammarAccess.getDurationAccess().getTKeyword_0_1()));
		match_Duration___HyphenMinusKeyword_1_1_or_PlusSignKeyword_1_0__q = new AlternativeAlias(false, true, new TokenAlias(false, false, grammarAccess.getDurationAccess().getHyphenMinusKeyword_1_1()), new TokenAlias(false, false, grammarAccess.getDurationAccess().getPlusSignKeyword_1_0()));
		match_Param_Assign_NOTKeyword_1_0_q = new TokenAlias(false, true, grammarAccess.getParam_AssignAccess().getNOTKeyword_1_0());
		match_Primary_Expression_HyphenMinusKeyword_1_0_q = new TokenAlias(false, true, grammarAccess.getPrimary_ExpressionAccess().getHyphenMinusKeyword_1_0());
		match_Real_Literal_EKeyword_3_0_0_or_EKeyword_3_0_1 = new AlternativeAlias(false, false, new TokenAlias(false, false, grammarAccess.getReal_LiteralAccess().getEKeyword_3_0_0()), new TokenAlias(false, false, grammarAccess.getReal_LiteralAccess().getEKeyword_3_0_1()));
		match_Real_Literal___HyphenMinusKeyword_1_1_or_PlusSignKeyword_1_0__q = new AlternativeAlias(false, true, new TokenAlias(false, false, grammarAccess.getReal_LiteralAccess().getHyphenMinusKeyword_1_1()), new TokenAlias(false, false, grammarAccess.getReal_LiteralAccess().getPlusSignKeyword_1_0()));
		match_Real_Type_Name_LREALKeyword_1_1_or_REALKeyword_1_0 = new AlternativeAlias(false, false, new TokenAlias(false, false, grammarAccess.getReal_Type_NameAccess().getLREALKeyword_1_1()), new TokenAlias(false, false, grammarAccess.getReal_Type_NameAccess().getREALKeyword_1_0()));
		match_Signed_Integer_Type_Name_DINTKeyword_1_0_or_INTKeyword_1_1_or_LINTKeyword_1_3_or_SINTKeyword_1_2 = new AlternativeAlias(false, false, new TokenAlias(false, false, grammarAccess.getSigned_Integer_Type_NameAccess().getDINTKeyword_1_0()), new TokenAlias(false, false, grammarAccess.getSigned_Integer_Type_NameAccess().getINTKeyword_1_1()), new TokenAlias(false, false, grammarAccess.getSigned_Integer_Type_NameAccess().getLINTKeyword_1_3()), new TokenAlias(false, false, grammarAccess.getSigned_Integer_Type_NameAccess().getSINTKeyword_1_2()));
		match_StatementList_SemicolonKeyword_1_1_a = new TokenAlias(true, true, grammarAccess.getStatementListAccess().getSemicolonKeyword_1_1());
		match_StatementList_SemicolonKeyword_1_1_p = new TokenAlias(true, false, grammarAccess.getStatementListAccess().getSemicolonKeyword_1_1());
		match_Time_Of_Day_TIME_OF_DAYKeyword_0_0_or_TODKeyword_0_1 = new AlternativeAlias(false, false, new TokenAlias(false, false, grammarAccess.getTime_Of_DayAccess().getTIME_OF_DAYKeyword_0_0()), new TokenAlias(false, false, grammarAccess.getTime_Of_DayAccess().getTODKeyword_0_1()));
		match_Time_Type_Name_LTIMEKeyword_1_1_or_TIMEKeyword_1_0 = new AlternativeAlias(false, false, new TokenAlias(false, false, grammarAccess.getTime_Type_NameAccess().getLTIMEKeyword_1_1()), new TokenAlias(false, false, grammarAccess.getTime_Type_NameAccess().getTIMEKeyword_1_0()));
		match_Unsigned_Integer_Type_Name_UDINTKeyword_1_2_or_ULINTKeyword_1_3_or_UNITKeyword_1_0_or_USINTKeyword_1_1 = new AlternativeAlias(false, false, new TokenAlias(false, false, grammarAccess.getUnsigned_Integer_Type_NameAccess().getUDINTKeyword_1_2()), new TokenAlias(false, false, grammarAccess.getUnsigned_Integer_Type_NameAccess().getULINTKeyword_1_3()), new TokenAlias(false, false, grammarAccess.getUnsigned_Integer_Type_NameAccess().getUNITKeyword_1_0()), new TokenAlias(false, false, grammarAccess.getUnsigned_Integer_Type_NameAccess().getUSINTKeyword_1_1()));
		match_structuredTextAlgorithm___PLUGSKeyword_11_0_END_PLUGSKeyword_11_2__q = new GroupAlias(false, true, new TokenAlias(false, false, grammarAccess.getStructuredTextAlgorithmAccess().getPLUGSKeyword_11_0()), new TokenAlias(false, false, grammarAccess.getStructuredTextAlgorithmAccess().getEND_PLUGSKeyword_11_2()));
		match_structuredTextAlgorithm___SOCKETSKeyword_12_0_END_SOCKETSKeyword_12_2__q = new GroupAlias(false, true, new TokenAlias(false, false, grammarAccess.getStructuredTextAlgorithmAccess().getSOCKETSKeyword_12_0()), new TokenAlias(false, false, grammarAccess.getStructuredTextAlgorithmAccess().getEND_SOCKETSKeyword_12_2()));
		match_structuredTextAlgorithm___VARKeyword_13_0_END_VARKeyword_13_2__q = new GroupAlias(false, true, new TokenAlias(false, false, grammarAccess.getStructuredTextAlgorithmAccess().getVARKeyword_13_0()), new TokenAlias(false, false, grammarAccess.getStructuredTextAlgorithmAccess().getEND_VARKeyword_13_2()));
	}
	
	@Override
	protected String getUnassignedRuleCallToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		return "";
	}
	
	
	@Override
	protected void emitUnassignedTokens(EObject semanticObject, ISynTransition transition, INode fromNode, INode toNode) {
		if (transition.getAmbiguousSyntaxes().isEmpty()) return;
		List<INode> transitionNodes = collectNodes(fromNode, toNode);
		for (AbstractElementAlias syntax : transition.getAmbiguousSyntaxes()) {
			List<INode> syntaxNodes = getNodesFor(transitionNodes, syntax);
			if(match_AdapterDefinition___VAR_INPUTKeyword_2_0_END_VARKeyword_2_2__q.equals(syntax))
				emit_AdapterDefinition___VAR_INPUTKeyword_2_0_END_VARKeyword_2_2__q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_AdapterDefinition___VAR_OUTPUTKeyword_3_0_END_VARKeyword_3_2__q.equals(syntax))
				emit_AdapterDefinition___VAR_OUTPUTKeyword_3_0_END_VARKeyword_3_2__q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_Bit_String_Type_Name_BOOLKeyword_1_3_or_BYTEKeyword_1_0_or_DWORDKeyword_1_2_or_LWORDKeyword_1_4_or_WORDKeyword_1_1.equals(syntax))
				emit_Bit_String_Type_Name_BOOLKeyword_1_3_or_BYTEKeyword_1_0_or_DWORDKeyword_1_2_or_LWORDKeyword_1_4_or_WORDKeyword_1_1(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_Date_And_Time_DATE_AND_TIMEKeyword_0_0_or_DTKeyword_0_1.equals(syntax))
				emit_Date_And_Time_DATE_AND_TIMEKeyword_0_0_or_DTKeyword_0_1(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_Date_DATEKeyword_0_0_or_DKeyword_0_1.equals(syntax))
				emit_Date_DATEKeyword_0_0_or_DKeyword_0_1(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_Date_Type_Name_DATEKeyword_1_0_or_DATE_AND_TIMEKeyword_1_2_or_TIME_OF_DAYKeyword_1_1.equals(syntax))
				emit_Date_Type_Name_DATEKeyword_1_0_or_DATE_AND_TIMEKeyword_1_2_or_TIME_OF_DAYKeyword_1_1(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_Duration_HyphenMinusKeyword_1_1_q_or_PlusSignKeyword_1_0_q.equals(syntax))
				emit_Duration_HyphenMinusKeyword_1_1_q_or_PlusSignKeyword_1_0_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_Duration_TIMEKeyword_0_0_or_TKeyword_0_1.equals(syntax))
				emit_Duration_TIMEKeyword_0_0_or_TKeyword_0_1(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_Duration___HyphenMinusKeyword_1_1_or_PlusSignKeyword_1_0__q.equals(syntax))
				emit_Duration___HyphenMinusKeyword_1_1_or_PlusSignKeyword_1_0__q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_Param_Assign_NOTKeyword_1_0_q.equals(syntax))
				emit_Param_Assign_NOTKeyword_1_0_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_Primary_Expression_HyphenMinusKeyword_1_0_q.equals(syntax))
				emit_Primary_Expression_HyphenMinusKeyword_1_0_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_Real_Literal_EKeyword_3_0_0_or_EKeyword_3_0_1.equals(syntax))
				emit_Real_Literal_EKeyword_3_0_0_or_EKeyword_3_0_1(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_Real_Literal___HyphenMinusKeyword_1_1_or_PlusSignKeyword_1_0__q.equals(syntax))
				emit_Real_Literal___HyphenMinusKeyword_1_1_or_PlusSignKeyword_1_0__q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_Real_Type_Name_LREALKeyword_1_1_or_REALKeyword_1_0.equals(syntax))
				emit_Real_Type_Name_LREALKeyword_1_1_or_REALKeyword_1_0(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_Signed_Integer_Type_Name_DINTKeyword_1_0_or_INTKeyword_1_1_or_LINTKeyword_1_3_or_SINTKeyword_1_2.equals(syntax))
				emit_Signed_Integer_Type_Name_DINTKeyword_1_0_or_INTKeyword_1_1_or_LINTKeyword_1_3_or_SINTKeyword_1_2(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_StatementList_SemicolonKeyword_1_1_a.equals(syntax))
				emit_StatementList_SemicolonKeyword_1_1_a(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_StatementList_SemicolonKeyword_1_1_p.equals(syntax))
				emit_StatementList_SemicolonKeyword_1_1_p(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_Time_Of_Day_TIME_OF_DAYKeyword_0_0_or_TODKeyword_0_1.equals(syntax))
				emit_Time_Of_Day_TIME_OF_DAYKeyword_0_0_or_TODKeyword_0_1(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_Time_Type_Name_LTIMEKeyword_1_1_or_TIMEKeyword_1_0.equals(syntax))
				emit_Time_Type_Name_LTIMEKeyword_1_1_or_TIMEKeyword_1_0(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_Unsigned_Integer_Type_Name_UDINTKeyword_1_2_or_ULINTKeyword_1_3_or_UNITKeyword_1_0_or_USINTKeyword_1_1.equals(syntax))
				emit_Unsigned_Integer_Type_Name_UDINTKeyword_1_2_or_ULINTKeyword_1_3_or_UNITKeyword_1_0_or_USINTKeyword_1_1(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_structuredTextAlgorithm___PLUGSKeyword_11_0_END_PLUGSKeyword_11_2__q.equals(syntax))
				emit_structuredTextAlgorithm___PLUGSKeyword_11_0_END_PLUGSKeyword_11_2__q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_structuredTextAlgorithm___SOCKETSKeyword_12_0_END_SOCKETSKeyword_12_2__q.equals(syntax))
				emit_structuredTextAlgorithm___SOCKETSKeyword_12_0_END_SOCKETSKeyword_12_2__q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_structuredTextAlgorithm___VARKeyword_13_0_END_VARKeyword_13_2__q.equals(syntax))
				emit_structuredTextAlgorithm___VARKeyword_13_0_END_VARKeyword_13_2__q(semanticObject, getLastNavigableState(), syntaxNodes);
			else acceptNodes(getLastNavigableState(), syntaxNodes);
		}
	}

	/**
	 * Syntax:
	 *     ('VAR_INPUT' 'END_VAR')?
	 */
	protected void emit_AdapterDefinition___VAR_INPUTKeyword_2_0_END_VARKeyword_2_2__q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     ('VAR_OUTPUT' 'END_VAR')?
	 */
	protected void emit_AdapterDefinition___VAR_OUTPUTKeyword_3_0_END_VARKeyword_3_2__q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'BYTE' | 'WORD' | 'LWORD' | 'DWORD' | 'BOOL'
	 */
	protected void emit_Bit_String_Type_Name_BOOLKeyword_1_3_or_BYTEKeyword_1_0_or_DWORDKeyword_1_2_or_LWORDKeyword_1_4_or_WORDKeyword_1_1(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'DATE_AND_TIME#' | 'DT#'
	 */
	protected void emit_Date_And_Time_DATE_AND_TIMEKeyword_0_0_or_DTKeyword_0_1(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'D#' | 'DATE#'
	 */
	protected void emit_Date_DATEKeyword_0_0_or_DKeyword_0_1(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'TIME_OF_DAY' | 'DATE_AND_TIME' | 'DATE'
	 */
	protected void emit_Date_Type_Name_DATEKeyword_1_0_or_DATE_AND_TIMEKeyword_1_2_or_TIME_OF_DAYKeyword_1_1(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     '-'? | '+'?
	 */
	protected void emit_Duration_HyphenMinusKeyword_1_1_q_or_PlusSignKeyword_1_0_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'T#' | 'TIME#'
	 */
	protected void emit_Duration_TIMEKeyword_0_0_or_TKeyword_0_1(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     ('-' | '+')?
	 */
	protected void emit_Duration___HyphenMinusKeyword_1_1_or_PlusSignKeyword_1_0__q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'NOT'?
	 */
	protected void emit_Param_Assign_NOTKeyword_1_0_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     '-'?
	 */
	protected void emit_Primary_Expression_HyphenMinusKeyword_1_0_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'e' | 'E'
	 */
	protected void emit_Real_Literal_EKeyword_3_0_0_or_EKeyword_3_0_1(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     ('+' | '-')?
	 */
	protected void emit_Real_Literal___HyphenMinusKeyword_1_1_or_PlusSignKeyword_1_0__q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'LREAL' | 'REAL'
	 */
	protected void emit_Real_Type_Name_LREALKeyword_1_1_or_REALKeyword_1_0(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'LINT' | 'SINT' | 'DINT' | 'INT'
	 */
	protected void emit_Signed_Integer_Type_Name_DINTKeyword_1_0_or_INTKeyword_1_1_or_LINTKeyword_1_3_or_SINTKeyword_1_2(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     ';'*
	 */
	protected void emit_StatementList_SemicolonKeyword_1_1_a(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     ';'+
	 */
	protected void emit_StatementList_SemicolonKeyword_1_1_p(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'TIME_OF_DAY#' | 'TOD#'
	 */
	protected void emit_Time_Of_Day_TIME_OF_DAYKeyword_0_0_or_TODKeyword_0_1(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'LTIME' | 'TIME'
	 */
	protected void emit_Time_Type_Name_LTIMEKeyword_1_1_or_TIMEKeyword_1_0(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'USINT' | 'UDINT' | 'ULINT' | 'UNIT'
	 */
	protected void emit_Unsigned_Integer_Type_Name_UDINTKeyword_1_2_or_ULINTKeyword_1_3_or_UNITKeyword_1_0_or_USINTKeyword_1_1(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     ('PLUGS' 'END_PLUGS')?
	 */
	protected void emit_structuredTextAlgorithm___PLUGSKeyword_11_0_END_PLUGSKeyword_11_2__q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     ('SOCKETS' 'END_SOCKETS')?
	 */
	protected void emit_structuredTextAlgorithm___SOCKETSKeyword_12_0_END_SOCKETSKeyword_12_2__q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     ('VAR' 'END_VAR')?
	 */
	protected void emit_structuredTextAlgorithm___VARKeyword_13_0_END_VARKeyword_13_2__q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
}
