package org.fordiac.systemmanagement.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.actions.DeleteResourceAction;
import org.eclipse.ui.handlers.HandlerUtil;
import org.fordiac.ide.model.libraryElement.AutomationSystem;

public class DeleteSystem extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if(selection instanceof TreeSelection) {
			if(((TreeSelection) selection).getFirstElement() instanceof AutomationSystem) {
				AutomationSystem system = (AutomationSystem) ((TreeSelection) selection).getFirstElement();
				IProject project = system.getProject();
				runDeleteAction(project);
			}
		}
		return null;
	}

	private void runDeleteAction(IProject project) {
		DeleteResourceAction action = new DeleteResourceAction(new IShellProvider() {			
			@Override
			public Shell getShell() {
				return Display.getDefault().getActiveShell();
			}
		});
		action.selectionChanged(new StructuredSelection(project));
		action.run();
	}
}
