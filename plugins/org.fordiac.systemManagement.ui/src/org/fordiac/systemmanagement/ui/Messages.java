/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.systemmanagement.ui;

import org.eclipse.osgi.util.NLS;

/**
 * The Class Messages.
 */
public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.fordiac.systemmanagement.ui.messages"; //$NON-NLS-1$
	
	/** The New application page_ applcation name label. */
	public static String NewElementPage_ElementNameLabel;
	
	/** The New application page_ default. */
	public static String NewApplicationPage_Default;
	
	/** The New application page_ error message_ empty app name. */
	public static String NewElementPage_ErrorMessage_EmptyElementName;
	
	/** The New application page_ error message invalid app name. */
	public static String NewApplicationPage_ErrorMessageInvalidAppName;
	
	/** The New application page_ error message no system selected. */
	public static String NewElementPage_ErrorMessageNoSystemSelected;
	
	/** The New application page_ ico n_ system. */
	public static String NewApplicationPage_ICON_SYSTEM;
	
	/** The New application page_ parent system label. */
	public static String NewElementPage_ParentSystemLabel;
	
	/** The New application wizard_ comment. */
	public static String NewApplicationWizard_Comment;
	
	/** The New application wizard_ description. */
	public static String NewApplicationWizard_Description;
	
	/** The New application wizard_ title. */
	public static String NewApplicationWizard_Title;
	
	public static String NewSystemWizard_ShowAdvanced;
	
	public static String NewSystemWizard_HideAdvanced;
	
	/** The New system wizard_ wizard desc. */
	public static String NewSystemWizard_WizardDesc;
	
	/** The New system wizard_ wizard name. */
	public static String NewSystemWizard_WizardName;
	
	/** The Palette management page_ labe l_ default palette. */
	public static String PaletteManagementPage_LABEL_DefaultTypeLibrary;
	
	/** The System tree view_ collaps e_ all. */
	public static String SystemTreeView_COLLAPSE_ALL;
	
	/** The System tree view_ expan d_ all. */
	public static String SystemTreeView_EXPAND_ALL;
	
	/** The System tree view_ ge t_ extension_ error. */
	public static String SystemTreeView_GET_Extension_Error;
	
	/** The System tree view_ ico n_ application. */
	public static String SystemTreeView_ICON_Application;
	
	/** The System tree view_ ico n_ basic fb. */
	public static String SystemTreeView_ICON_BasicFB;
	
	/** The System tree view_ ico n_ collaps e_ all. */
	public static String SystemTreeView_ICON_COLLAPSE_ALL;
	
	/** The System tree view_ ico n_ composite fb. */
	public static String SystemTreeView_ICON_CompositeFB;
	
	/** The System tree view_ ico n_ device. */
	public static String SystemTreeView_ICON_DEVICE;
	
	/** The System tree view_ ico n_ expan d_ all. */
	public static String SystemTreeView_ICON_EXPAND_ALL;
	
	/** The System tree view_ ico n_ fb. */
	public static String SystemTreeView_ICON_FB;
	
	/** The System tree view_ ico n_ fb network. */
	public static String SystemTreeView_ICON_FBNetwork;
	
	/** The System tree view_ ico n_ refresh. */
	public static String SystemTreeView_ICON_REFRESH;
	
	/** The System tree view_ ico n_ resource. */
	public static String SystemTreeView_ICON_RESOURCE;
	
	/** The System tree view_ ico n_ sifb. */
	public static String SystemTreeView_ICON_SIFB;
	
	/** The System tree view_ ico n_ subapp. */
	public static String SystemTreeView_ICON_SUBAPP;
	
	/** The System tree view_ ico n_ system. */
	public static String SystemTreeView_ICON_SYSTEM;
	
	/** The System tree view_ ico n_ systemconfiguration. */
	public static String SystemTreeView_ICON_SYSTEMCONFIGURATION;
	
	/** The System tree view_ popup menu. */
	public static String SystemTreeView_PopupMenu;
	
	/** The System tree view_ refresh. */
	public static String SystemTreeView_Refresh;
	
	/** The System tree view_ refresh label. */
	public static String SystemTreeView_RefreshLabel;
	
	/** The System tree view_ system configuration label. */
	public static String SystemTreeView_SystemConfigurationLabel;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
		// empty private constructor
	}
}
