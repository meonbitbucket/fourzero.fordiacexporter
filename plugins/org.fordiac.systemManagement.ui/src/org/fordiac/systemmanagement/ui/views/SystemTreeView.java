/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.systemmanagement.ui.views;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DecoratingLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelDecorator;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.ISaveablePart;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.DrillDownAdapter;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.views.properties.IPropertySheetPage;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertySheetPageContributor;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.fordiac.ide.model.libraryElement.Annotation;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.BasicFBType;
import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.I4DIACElement;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.libraryElement.ServiceInterfaceFBType;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.libraryElement.SystemConfiguration;
import org.fordiac.ide.model.libraryElement.SystemConfigurationNetwork;
import org.fordiac.ide.util.IOpenListener;
import org.fordiac.ide.util.OpenListenerManager;
import org.fordiac.ide.util.imageprovider.ImageProvider;
import org.fordiac.systemmanagement.Activator;
import org.fordiac.systemmanagement.DistributedSystemListener;
import org.fordiac.systemmanagement.SystemManager;
import org.fordiac.systemmanagement.ui.Messages;
import org.fordiac.systemmanagement.ui.popup.actions.NewSystemAction;

/**
 * The Class SystemTreeView.
 * 
 * @author mrooke, gebenh
 */
public class SystemTreeView extends ViewPart implements ISaveablePart, ITabbedPropertySheetPageContributor  {

	private TreeViewer viewer;
	private DrillDownAdapter drillDownAdapter;
	
	public TreeViewer getViewer() {
		return viewer;
	}

	private final EContentAdapter adapter = new EContentAdapter() {
		@Override
		public void notifyChanged(Notification notification) {
			Display.getDefault().asyncExec(new Runnable() {

				@Override
				public void run() {
					if (!viewer.getTree().isDisposed()) {
						viewer.refresh(true);
					}
				}
			});
			super.notifyChanged(notification);
		}

	};

	/**
	 * Instantiates a new system tree view.
	 */
	public SystemTreeView() {
		SystemManager.getInstance().addWorkspaceListener(
				new DistributedSystemListener() {
					@Override
					public void distributedSystemWorkspaceChanged() {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								if (!viewer.getTree().isDisposed()) {
									viewer.refresh(true);

								}

							}

						});
					}
				});
		
	}

	/**
	 * The Class ViewContentProvider.
	 */
	class ViewContentProvider implements IStructuredContentProvider,
			ITreeContentProvider {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java
		 * .lang.Object)
		 */
		public Object[] getElements(final Object parent) {
			if (parent.equals(getViewSite())) {

				List<AutomationSystem> systems = SystemManager.getInstance()
						.getSystems();
				for (Iterator<AutomationSystem> iterator = systems.iterator(); iterator
						.hasNext();) {
					AutomationSystem sys = iterator.next();
					if (!sys.eAdapters().contains(adapter)) {
						sys.eAdapters().add(adapter);

					}
				}
				return systems.toArray();
			}
			return getChildren(parent);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
		 */
		public void dispose() {
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse
		 * .jface .viewers.Viewer, java.lang.Object, java.lang.Object)
		 */
		public void inputChanged(final Viewer viewer, final Object oldInput,
				final Object newInput) {
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.
		 * Object)
		 */
		public Object[] getChildren(final Object parent) {
			if (parent instanceof AutomationSystem) {
				ArrayList<Object> temp = new ArrayList<Object>();
				AutomationSystem system = (AutomationSystem) parent;

				temp.addAll(system.getApplication());
				temp.add(system.getSystemConfiguration());

				for (Application app : system.getApplication()) {
					if (!(app.eAdapters().contains(adapter))) {
						app.eAdapters().add(adapter);
					}
				}
				
				return temp.toArray();
			}
			if (parent instanceof Application) {
				Application app = (Application) parent;
				ArrayList<INamedElement> temp = new ArrayList<INamedElement>();

				temp.addAll(app.getFBNetwork().getFBs());
				temp.addAll(app.getFBNetwork().getSubApps());
				
				for (SubApp subApp : app.getFBNetwork().getSubApps()) {
					if (!(subApp.getSubAppNetwork().eAdapters().contains(adapter))) {
						subApp.getSubAppNetwork().eAdapters().add(adapter);
					}	
				}
				
				return temp.toArray();
			}
			if (parent instanceof SubApp) {
				SubApp subApp = (SubApp) parent;
				ArrayList<INamedElement> temp = new ArrayList<INamedElement>();

				temp.addAll(subApp.getSubAppNetwork().getFBs());
				temp.addAll(subApp.getSubAppNetwork().getSubApps());
				
				for (SubApp subApp1 : subApp.getSubAppNetwork().getSubApps()) {
					if (!(subApp1.getSubAppNetwork().eAdapters().contains(adapter))) {
						subApp1.getSubAppNetwork().eAdapters().add(adapter);
					}	
				}
				return temp.toArray();

			}
			if (parent instanceof SystemConfiguration) {
				SystemConfigurationNetwork network = ((SystemConfiguration) parent)
						.getSystemConfigurationNetwork();
				if (network == null) {
					return new Object[] {};
				}
				if (!network.eAdapters().contains(adapter)) {
					network.eAdapters().add(adapter);
				}

				return network.getDevices().toArray();
			}
			if (parent instanceof Device) {
				Device device = (Device) parent;
				if (!device.eAdapters().contains(adapter)) {
					device.eAdapters().add(adapter);
				}
				return device.getResource().toArray();
			}
			return new Object[0];
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang
		 * .Object )
		 */
		public Object getParent(final Object child) {
			if (child instanceof FB) {
				return ((FB) child).eContainer();
			}
			if (child instanceof FBNetwork) {
				return ((FBNetwork) child).eContainer();
			}
			if (child instanceof Application) {
				return ((Application) child).eContainer();
			}
			if (child instanceof Device) {
				return ((Device) child).eContainer();
			}
			if (child instanceof Resource) {
				return ((Resource) child).getDevice();
			}
			return null;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.
		 * Object)
		 */
		public boolean hasChildren(final Object parent) {
			if (parent instanceof AutomationSystem) {
				if (!((AutomationSystem) parent).eAdapters().contains(adapter)) {
					((AutomationSystem) parent).eAdapters().add(adapter);
				}

				return (((AutomationSystem) parent).getApplication().size() > 0)
						|| ((AutomationSystem) parent).getSystemConfiguration() != null;
			}
			if (parent instanceof Application) {
				if (!((Application) parent).getFBNetwork().eAdapters()
						.contains(adapter)) {
					((Application) parent).getFBNetwork().eAdapters()
							.add(adapter);
				}
				return (((Application) parent).getFBNetwork()) != null
						&& (((Application) parent).getFBNetwork().getFBs()
								.size() > 0 || ((Application) parent)
								.getFBNetwork().getSubApps().size() > 0);
			}
			if (parent instanceof FBNetwork) {
				return (((FBNetwork) parent).getFBs().size() > 0);
			}
			if (parent instanceof SubApp) {

				return (((SubApp) parent).getSubAppNetwork()) != null
						&& (((SubApp) parent).getSubAppNetwork().getFBs()
								.size() > 0 || ((SubApp) parent)
								.getSubAppNetwork().getSubApps().size() > 0);
			}
			if (parent instanceof SystemConfiguration) {
				SystemConfigurationNetwork network = ((SystemConfiguration) parent)
						.getSystemConfigurationNetwork();

				if (network == null) {
					return false;
				} else {
					if (!network.eAdapters().contains(adapter)) {
						network.eAdapters().add(adapter);
					}
				}
				return network.getDevices().size() > 0;
			}
			if (parent instanceof Device) {
				Device device = (Device) parent;
				if (!device.eAdapters().contains(adapter)) {
					device.eAdapters().add(adapter);
				}
				return device.getResource().size() > 0;
			}
			return false;
		}

	}

	/**
	 * The Class ViewLabelProvider.
	 */
	class ViewLabelProvider extends LabelProvider {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
		 */
		@Override
		public String getText(final Object element) {
			if (element instanceof AutomationSystem) {
				return ((AutomationSystem) element).getName();
			}
			if (element instanceof SystemConfiguration) {
				return Messages.SystemTreeView_SystemConfigurationLabel;
			}
			if (element instanceof Application) {
				return ((Application) element).getName();
			}
			if (element instanceof FBNetwork) {
				return ((FBNetwork) element).getName();
			}
			if (element instanceof FB) {
				return ((FB) element).getName();
			}
			if (element instanceof INamedElement) {
				return ((INamedElement) element).getName();
			}
			return element.toString();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.jface.viewers.LabelProvider#getImage(java.lang.Object)
		 */
		@Override
		public Image getImage(final Object element) {
			Image image = null;
			if (element instanceof FB) {
				if (((FB) element).getFBType() instanceof BasicFBType) {
					image = ImageProvider
							.getImage(Messages.SystemTreeView_ICON_BasicFB);
				} else if (((FB) element).getFBType() instanceof CompositeFBType) {
					image = ImageProvider
							.getImage(Messages.SystemTreeView_ICON_CompositeFB);
				} else if (((FB) element).getFBType() instanceof ServiceInterfaceFBType) {
					image = ImageProvider
							.getImage(Messages.SystemTreeView_ICON_SIFB);
				}
				// TODO: those FBs are imported in a wrong way --> have to look
				// at the importer
				else if (((FB) element).getPaletteEntry() == null) {
					return ImageProvider
							.getImage(Messages.SystemTreeView_ICON_FB);
				}
			} else if (element instanceof FBNetwork) {
				image = ImageProvider
						.getImage(Messages.SystemTreeView_ICON_FBNetwork);
			} else if (element instanceof Application) {
				image = ImageProvider
						.getImage(Messages.SystemTreeView_ICON_Application);
			} else if (element instanceof AutomationSystem) {
				image = ImageProvider
						.getImage(Messages.SystemTreeView_ICON_SYSTEM);
			} else if (element instanceof SystemConfiguration) {
				image = ImageProvider
						.getImage(Messages.SystemTreeView_ICON_SYSTEMCONFIGURATION);
			} else if (element instanceof Device) {
				image = ImageProvider
						.getImage(Messages.SystemTreeView_ICON_DEVICE);
			} else if (element instanceof Resource) {
				image = ImageProvider
						.getImage(Messages.SystemTreeView_ICON_RESOURCE);
			} else if (element instanceof SubApp) {
				image = ImageProvider
						.getImage(Messages.SystemTreeView_ICON_SUBAPP);
			}
			if (element instanceof I4DIACElement && image != null) {
				for (Annotation anno : ((I4DIACElement) element)
						.getAnnotations()) {
					if (anno.getServity() == IMarker.SEVERITY_ERROR) {
						return ImageProvider.getErrorOverlayImage(image);
					}
				}
				return image;
			}
			return super.getImage(element);
		}
	}

	/**
	 * Hook double click action.
	 */
	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(final DoubleClickEvent event) {
				handleDoubleClick();
			}
		});
	}

	/**
	 * Handle double click.
	 */
	private void handleDoubleClick() {

		ISelection selection = viewer.getSelection();

		Object obj = ((IStructuredSelection) selection).getFirstElement();

		IOpenListener openListener = OpenListenerManager.getInstance()
				.getDefaultOpenListener(((I4DIACElement) obj).getClass(), obj);
		if (openListener != null) {
			openListener.run(null);
		}

	}

	/**
	 * Gets the open listener.
	 * 
	 * @param libElement
	 *            the lib element
	 * 
	 * @return the open listener
	 */
	public IOpenListener getOpenListener(
			final Class<? extends I4DIACElement> libElement) {
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IConfigurationElement[] elems = registry.getConfigurationElementsFor(
				org.fordiac.systemmanagement.Activator.PLUGIN_ID,
				"openListener"); //$NON-NLS-1$
		for (int i = 0; i < elems.length; i++) {
			IConfigurationElement element = elems[i];
			try {
				Object object = element.createExecutableExtension("class"); //$NON-NLS-1$
				if (object instanceof IOpenListener) {
					IOpenListener openListener = (IOpenListener) object;
					if (openListener.supportsObject(libElement)) {
						openListener.selectionChanged(null,
								viewer.getSelection());
						return openListener;
					}
				}
			} catch (CoreException corex) {
				Activator.getDefault().logError(Messages.SystemTreeView_GET_Extension_Error, corex);
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	public void createPartControl(final Composite parent) {
		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		drillDownAdapter = new DrillDownAdapter(viewer);
		viewer.setContentProvider(new ViewContentProvider());
		
		ILabelDecorator decorator = PlatformUI.getWorkbench().getDecoratorManager().getLabelDecorator();  
		
		LabelProvider lp = new ViewLabelProvider();
		viewer.setLabelProvider(new DecoratingLabelProvider(lp,decorator));
		viewer.setInput(getViewSite());

		hookContextMenu();
		hookDoubleClickAction();
		createToolbarbuttons();
		contributeToActionBars();
		
		getSite().setSelectionProvider(viewer);

		// Testing DnD in the tree...
		/*
		 * Transfer[] types = new Transfer[] { TextTransfer.getInstance() }; int
		 * operations = DND.DROP_COPY | DND.DROP_MOVE | DND.DROP_LINK;
		 * 
		 * final DragSource source = new DragSource(viewer.getTree(),
		 * operations); source.setTransfer(types);
		 * 
		 * final TreeItem[] dragSourceItem = new TreeItem[1];
		 * source.addDragListener(new DragSourceListener() {
		 * 
		 * public void dragFinished(DragSourceEvent event) { if (event.detail ==
		 * DND.DROP_MOVE) { event.detail = DND.DROP_COPY; } if (event.detail ==
		 * DND.DROP_COPY) { java.lang.System.out.println("Copy"); } }
		 * 
		 * public void dragSetData(DragSourceEvent event) { event.data =
		 * dragSourceItem[0].getData(); }
		 * 
		 * public void dragStart(DragSourceEvent event) { TreeItem[] selection =
		 * viewer.getTree().getSelection(); // make sure that it is a copy
		 * action if (event.detail == DND.DROP_MOVE || event.detail ==
		 * DND.DROP_NONE) event.detail = DND.DROP_COPY; if (selection.length > 0
		 * && selection[0].getItemCount() == 0) { event.doit = true;
		 * dragSourceItem[0] = selection[0]; } else { event.doit = false; } }
		 * 
		 * });
		 * 
		 * DropTarget target = new DropTarget(viewer.getTree(), operations);
		 * target.setTransfer(types); target.addDropListener(new
		 * DropTargetAdapter() {
		 * 
		 * @Override public void dragEnter(DropTargetEvent event) { if
		 * (event.detail == DND.DROP_DEFAULT) { if ((event.operations &
		 * DND.DROP_COPY) != 0) { event.detail = DND.DROP_COPY; } else {
		 * event.detail = DND.DROP_NONE; } } } // event.item is the TreeItem
		 * where the element is copied to. public void drop(DropTargetEvent
		 * event) { if (event.data == null) { event.detail = DND.DROP_NONE;
		 * return; } FB sourceFB = (FB) event.data; TreeItem item = (TreeItem)
		 * event.item; // TODO: �berpr�fung Application destAppl =
		 * (Application)item.getData(); TreeItem parent = item.getParentItem();
		 * 
		 * FB newFB = (FB)EcoreUtil.copy(sourceFB);
		 * destAppl.getFBNetwork().getFBs().add(newFB); }
		 * 
		 * });
		 */
		// end testing DnD
	}

	/**
	 * Creates the toolbarbuttons.
	 */
	private void createToolbarbuttons() {
		IToolBarManager toolBarManager = getViewSite().getActionBars()
				.getToolBarManager();
		Action collapseAllAction = new Action() {
			@Override
			public void run() {
				viewer.collapseAll();
			}
		};
		collapseAllAction.setText(Messages.SystemTreeView_COLLAPSE_ALL);
		collapseAllAction.setToolTipText(Messages.SystemTreeView_COLLAPSE_ALL);
		collapseAllAction.setImageDescriptor(ImageDescriptor
				.createFromImage(ImageProvider
						.getImage(Messages.SystemTreeView_ICON_COLLAPSE_ALL)));
		toolBarManager.add(collapseAllAction);

		Action expandAllAction = new Action() {
			@Override
			public void run() {
				viewer.expandAll();
			}
		};
		expandAllAction.setText(Messages.SystemTreeView_EXPAND_ALL);
		expandAllAction.setToolTipText(Messages.SystemTreeView_EXPAND_ALL);
		expandAllAction.setImageDescriptor(ImageDescriptor
				.createFromImage(ImageProvider
						.getImage(Messages.SystemTreeView_ICON_EXPAND_ALL)));
		toolBarManager.add(expandAllAction);

		Action refresh = new Action() {
			@Override
			public void run() {
				viewer.refresh(true);
			}
		};
		refresh.setText(Messages.SystemTreeView_Refresh);
		refresh.setToolTipText(Messages.SystemTreeView_Refresh);
		refresh.setImageDescriptor(ImageDescriptor
				.createFromImage(ImageProvider
						.getImage(Messages.SystemTreeView_ICON_REFRESH)));
		toolBarManager.add(refresh);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
	}

	/**
	 * Hook context menu.
	 */
	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager(Messages.SystemTreeView_PopupMenu);
		menuMgr.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {

			public void menuAboutToShow(final IMenuManager manager) {
				SystemTreeView.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	/**
	 * Contribute to action bars.
	 */
	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalToolBar(bars.getToolBarManager());
	}

	/**
	 * Fill context menu.
	 * 
	 * @param manager
	 *            the manager
	 */
	private void fillContextMenu(final IMenuManager manager) {
		drillDownAdapter.addNavigationActions(manager);

		// add refresh action to context menu
		Action refreshAction = new Action() {
			@Override
			public void run() {
				TreeItem[] selection = viewer.getTree().getSelection();
				for (int i = 0; i < selection.length; i++) {
					TreeItem treeItem = selection[i];
					viewer.refresh(treeItem.getData(), true);

				}
			}
		};
		refreshAction.setText(Messages.SystemTreeView_RefreshLabel);
		refreshAction.setImageDescriptor(ImageProvider
				.getImageDescriptor("refresh.gif"));
		manager.add(refreshAction);

		TreeItem[] selection = viewer.getTree().getSelection();
		if (selection.length > 0
				&& selection[0].getData() instanceof I4DIACElement) {

			MenuManager openMenu = new MenuManager("Open");
			manager.add(openMenu);

			List<IOpenListener> listener = OpenListenerManager
					.getInstance()
					.getOpenListener(
							((I4DIACElement) selection[0].getData()).getClass(),
							(selection[0].getData()));

			for (IOpenListener openListener : listener) {
				openMenu.add(openListener.getOpenListenerAction());
			}

		}

		NewSystemAction newSystemAction = new NewSystemAction();
		manager.add(newSystemAction);

	}

	/**
	 * Fill local tool bar.
	 * 
	 * @param manager
	 *            the manager
	 */
	private void fillLocalToolBar(final IToolBarManager manager) {
		drillDownAdapter.addNavigationActions(manager);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.ISaveablePart#doSave(org.eclipse.core.runtime.IProgressMonitor
	 * )
	 */
	@Override
	public void doSave(final IProgressMonitor monitor) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.ISaveablePart#doSaveAs()
	 */
	@Override
	public void doSaveAs() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.ISaveablePart#isDirty()
	 */
	@Override
	public boolean isDirty() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.ISaveablePart#isSaveAsAllowed()
	 */
	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.ISaveablePart#isSaveOnCloseNeeded()
	 */
	@Override
	public boolean isSaveOnCloseNeeded() {
		return false;
	}

	@Override
	public String getContributorId() {
		return "property.contributor.system";
	}

	@SuppressWarnings("rawtypes")
	public Object getAdapter(Class required) {
		if(required == IPropertySheetPage.class){
			return new TabbedPropertySheetPage(this);
		}
		return super.getAdapter(required);
	}
}
