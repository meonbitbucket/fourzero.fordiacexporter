/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.systemmanagement.ui.popup.actions;

import java.util.ArrayList;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.View;
import org.fordiac.ide.util.commands.DeleteFBCommand;
import org.fordiac.ide.util.commands.UnmapSubAppCommand;
import org.fordiac.systemmanagement.SystemManager;

/**
 * The Class DeleteApplication.
 */
public class DeleteApplication implements IObjectActionDelegate {

	private Application app;

	/**
	 * Constructor for Action1.
	 */
	public DeleteApplication() {
		super();
	}

	/**
	 * Sets the active part.
	 * 
	 * @param action
	 *          the action
	 * @param targetPart
	 *          the target part
	 * 
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(final IAction action,
			final IWorkbenchPart targetPart) {
	}

	/**
	 * Run.
	 * 
	 * @param action
	 *          the action
	 * 
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(final IAction action) {

		ArrayList<DeleteFBCommand> commands = new ArrayList<DeleteFBCommand>();
		ArrayList<UnmapSubAppCommand> unmapSubAppCmds = new ArrayList<UnmapSubAppCommand>();

		if (app != null) {
			Diagram diagram = (Diagram) app.getFBNetwork().eContainer();
			if (diagram != null) {
				for (View view : diagram.getChildren()) {
					if (view instanceof FBView) {
						DeleteFBCommand cmd = new DeleteFBCommand((FBView) view);
						commands.add(cmd);
					}
					if (view instanceof SubAppView) {
						UnmapSubAppCommand unmapSubApp = new UnmapSubAppCommand(
								((SubAppView) view).getMappedSubApp());
						unmapSubAppCmds.add(unmapSubApp);

					}

				}
			}

			for (DeleteFBCommand deleteFBCommand : commands) {
				try {
					deleteFBCommand.execute();
				} catch (Exception e) {
					// nothing to do -> all gets deleted
				}
			}
			for (UnmapSubAppCommand unmapCmd : unmapSubAppCmds) {
				try {
					unmapCmd.execute();
				} catch (Exception e) {
					// nothing to do -> all gets deleted
				}
			}

			AutomationSystem system = SystemManager.getInstance().getSystemForNetwork(app.getFBNetwork());
			system.removeApplication(app);
			
			System.out.println("call saved");
			SystemManager.getInstance().saveSystem(system, false);
			System.out.println("saved");
		}

	}

	/**
	 * Selection changed.
	 * 
	 * @param action
	 *          the action
	 * @param selection
	 *          the selection
	 * 
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(final IAction action, final ISelection selection) {

		if (selection instanceof IStructuredSelection) {
			Object element = ((IStructuredSelection) selection).getFirstElement();
			if (element instanceof Application) {
				app = (Application) element;
			} else {
				app = null;
			}

		}

	}

}
