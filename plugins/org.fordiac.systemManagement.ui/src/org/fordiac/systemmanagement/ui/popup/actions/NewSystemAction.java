/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.systemmanagement.ui.popup.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.fordiac.ide.util.imageprovider.ImageProvider;
import org.fordiac.systemmanagement.ui.wizard.NewSystemWizard;

public class NewSystemAction extends Action implements IObjectActionDelegate {

	public NewSystemAction() {
		setText("New System");
		setImageDescriptor(ImageProvider.getImageDescriptor("system.png"));
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		// nothing to do
	}

	@Override
	public void run(IAction action) {
		NewSystemWizard wizard = new NewSystemWizard();
		Shell shell = PlatformUI.getWorkbench().getDisplay().getActiveShell();

		// Instantiates the wizard container with the wizard and opens it
		WizardDialog dialog = new WizardDialog(shell, wizard);
		dialog.create();
		dialog.open();

	}
	
	@Override
	public void run() {
		run(null);
		
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// nothing to do
	}

}
