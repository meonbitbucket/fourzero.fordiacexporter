/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.contentprovider;

import java.util.ArrayList;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.fordiac.ide.model.libraryElement.AdapterType;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.VarDeclaration;

/**
 * The Class InputVarContentProvider.
 */
public class InputVarContentProvider implements IStructuredContentProvider {

	/** The fb type. */
	private final FBType fbType;

	/**
	 * Instantiates a new input var content provider.
	 * 
	 * @param fbType
	 *            the fb type
	 */
	public InputVarContentProvider(final FBType fbType) {
		this.fbType = fbType;
	}

	/**
	 * Instantiates a new input var content provider.
	 * 
	 * @param fbType
	 *            the fb type
	 * @param filterAdapter
	 * 				if true, the contentprovider doesn't return elements which are of type Adapter         
	 */
	public InputVarContentProvider(final FBType fbType, boolean filterAdapter) {
		this.fbType = fbType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java
	 * .lang.Object)
	 */
	@Override
	public Object[] getElements(final Object inputElement) {
		ArrayList<VarDeclaration> inputs = new ArrayList<VarDeclaration>();
		// filter adapter elements as the are not allowed to be connected by
		// with
		for (VarDeclaration var : fbType.getInterfaceList().getInputVars()) {
			if (!(var.getType() instanceof AdapterType)) {
				inputs.add(var);
			}
		}
		return inputs.toArray();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
	 */
	@Override
	public void dispose() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface
	 * .viewers.Viewer, java.lang.Object, java.lang.Object)
	 */
	@Override
	public void inputChanged(final Viewer viewer, final Object oldInput,
			final Object newInput) {
	}

}
