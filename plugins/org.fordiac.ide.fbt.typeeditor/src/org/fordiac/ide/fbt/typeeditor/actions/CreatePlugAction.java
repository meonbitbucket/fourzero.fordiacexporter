/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.actions;

import org.eclipse.gef.ui.actions.WorkbenchPartAction;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.fbt.typeeditor.AdaperCreationFactory;
import org.fordiac.ide.fbt.typeeditor.commands.CreatePlugCommand;
import org.fordiac.ide.model.Palette.AdapterTypePaletteEntry;
import org.fordiac.ide.model.libraryElement.FBType;

public class CreatePlugAction extends WorkbenchPartAction {
	private static final String ID_PREFIX = "PLUG_";

	/** The fb type. */
	private FBType fbType;

	private AdapterTypePaletteEntry entry;

	public CreatePlugAction(IWorkbenchPart part, FBType fbType,
			AdapterTypePaletteEntry entry) {
		super(part);

		setId(getID(entry));
		setText(entry.getLabel());

		this.fbType = fbType;
		this.entry = entry;
	}

	@Override
	protected boolean calculateEnabled() {
		return (null != fbType);
	}

	@Override
	public void run() {
		CreatePlugCommand cmd = new CreatePlugCommand(entry, AdaperCreationFactory.createNewAdapterDeclaration(entry), fbType, -1);
		execute(cmd);
	}

	public static String getID(AdapterTypePaletteEntry entry ) {
		return ID_PREFIX + entry.getFile().getFullPath().toString();
	}

}
