/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.commands;

import java.util.Iterator;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.libraryElement.With;
import org.fordiac.ide.util.commands.DeleteFBCommand;

/**
 * The Class DeleteInterfaceCommand.
 */
public class DeleteInterfaceCommand extends Command {

	/** The interface element. */
	private final IInterfaceElement interfaceElement;

	/** The delete withs. */
	private CompoundCommand deleteWiths;

	/** The parent. */
	private InterfaceList parent;
	private FBType fb;
	/** The old index. */
	private int oldIndex;

	private DeleteFBCommand deleteAdapterBlockCmd;

	/**
	 * Instantiates a new delete interface command.
	 * 
	 * @param interfaceElement the interface element
	 */
	public DeleteInterfaceCommand(final IInterfaceElement interfaceElement) {
		this.interfaceElement = interfaceElement;
		this.fb = (FBType) interfaceElement.eContainer().eContainer();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		deleteWiths = new CompoundCommand();

		parent = (InterfaceList) interfaceElement.eContainer();

		if (interfaceElement instanceof VarDeclaration) {
			VarDeclaration varDecl = (VarDeclaration) interfaceElement;
			for (Iterator<With> iterator = varDecl.getWiths().iterator(); iterator
					.hasNext();) {
				With with = iterator.next();
				deleteWiths.add(new DeleteWithCommand(with));
			}
		} else if (interfaceElement instanceof Event) {
			Event event = (Event) interfaceElement;
			for (Iterator<With> iterator = event.getWith().iterator(); iterator
					.hasNext();) {
				With with = iterator.next();
				deleteWiths.add(new DeleteWithCommand(with));
			}
		}
		if (deleteWiths.canExecute()) {
			deleteWiths.execute();
		}

		if (interfaceElement.isIsInput()) {
			if (interfaceElement instanceof Event) {
				oldIndex = parent.getEventInputs().indexOf(interfaceElement);
				parent.getEventInputs().remove(interfaceElement);
			}
			if (interfaceElement instanceof VarDeclaration) {
				oldIndex = parent.getInputVars().indexOf(interfaceElement);
				parent.getInputVars().remove(interfaceElement);
			}
			if (interfaceElement instanceof AdapterDeclaration) {
				parent.getSockets().remove(interfaceElement);
				if(fb instanceof CompositeFBType){
					deleteAdapterBlockCmd = new DeleteFBCommand(((AdapterDeclaration) interfaceElement).getAdapterBlock());
					deleteAdapterBlockCmd.execute();
				}
			}
		} else {
			if (interfaceElement instanceof Event) {
				oldIndex = parent.getEventOutputs().indexOf(interfaceElement);
				parent.getEventOutputs().remove(interfaceElement);
			}
			if (interfaceElement instanceof VarDeclaration) {
				oldIndex = parent.getOutputVars().indexOf(interfaceElement);
				parent.getOutputVars().remove(interfaceElement);
			}
			if (interfaceElement instanceof AdapterDeclaration) {
				parent.getPlugs().remove(interfaceElement);
				if(fb instanceof CompositeFBType){
					deleteAdapterBlockCmd = new DeleteFBCommand(((AdapterDeclaration) interfaceElement).getAdapterBlock());
					deleteAdapterBlockCmd.execute();	
				}
			}
		}
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		if (interfaceElement.isIsInput()) {
			if (interfaceElement instanceof Event) {
				parent.getEventInputs().add(oldIndex, (Event) interfaceElement);
			}
			if (interfaceElement instanceof VarDeclaration) {
				parent.getInputVars().add(oldIndex,
						(VarDeclaration) interfaceElement);
			}
		} else {
			if (interfaceElement instanceof Event) {
				parent.getEventOutputs()
						.add(oldIndex, (Event) interfaceElement);
			}
			if (interfaceElement instanceof VarDeclaration) {
				parent.getOutputVars().add(oldIndex,
						(VarDeclaration) interfaceElement);
			}
		}
		if (deleteWiths.canUndo()) {
			deleteWiths.undo();
		}
		if (deleteAdapterBlockCmd != null) {
			deleteAdapterBlockCmd.undo();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		deleteWiths.redo();
		if (interfaceElement.isIsInput()) {
			if (interfaceElement instanceof Event) {
				oldIndex = parent.getEventInputs().indexOf(interfaceElement);
				parent.getEventInputs().remove(interfaceElement);
			}
			if (interfaceElement instanceof VarDeclaration) {
				oldIndex = parent.getInputVars().indexOf(interfaceElement);
				parent.getInputVars().remove(interfaceElement);
			}
		} else {
			if (interfaceElement instanceof Event) {
				oldIndex = parent.getEventOutputs().indexOf(interfaceElement);
				parent.getEventOutputs().remove(interfaceElement);
			}
			if (interfaceElement instanceof VarDeclaration) {
				oldIndex = parent.getOutputVars().indexOf(interfaceElement);
				parent.getOutputVars().remove(interfaceElement);
			}
		}
		if (deleteAdapterBlockCmd != null) {
			deleteAdapterBlockCmd.redo();
		}
	}
}
