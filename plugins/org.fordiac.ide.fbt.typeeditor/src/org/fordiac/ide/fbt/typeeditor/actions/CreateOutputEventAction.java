/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.actions;

import org.eclipse.gef.ui.actions.WorkbenchPartAction;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.fbt.typeeditor.commands.CreateOutputEventCommand;
import org.fordiac.ide.model.data.EventType;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.typelibrary.EventTypeLibrary;

public class CreateOutputEventAction extends WorkbenchPartAction {
	/** The Constant ID. */
	public static final String ID = "CreateOutputEventAction"; //$NON-NLS-1$
	
	/** The fb type. */
	private FBType fbType;

	public CreateOutputEventAction(IWorkbenchPart part, FBType fbType) {
		super(part);
		setId(ID);
		setText("Create Output Event");

		this.fbType = fbType;
	}

	@Override
	protected boolean calculateEnabled() {
		return (null != fbType);
	}

	@Override
	public void run() {
		CreateOutputEventCommand cmd = new CreateOutputEventCommand(
				(EventType) EventTypeLibrary.getInstance()
						.getType(null), fbType, -1);
		execute(cmd);
	}

}
