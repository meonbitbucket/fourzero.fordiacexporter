/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.editparts;

import java.text.MessageFormat;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.editparts.ZoomManager;
import org.fordiac.ide.fbt.typeeditor.Activator;
import org.fordiac.ide.model.Palette.Palette;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.libraryElement.With;

/**
 * A factory for creating FBInterfaceEditPart objects.
 */
public class FBInterfaceEditPartFactory implements EditPartFactory {

    Palette systemPalette;
    protected ZoomManager zoomManager;
    
	public FBInterfaceEditPartFactory(Palette systemPalette, ZoomManager zoomManager) {
		this.systemPalette = systemPalette;
		this.zoomManager = zoomManager;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.EditPartFactory#createEditPart(org.eclipse.gef.EditPart,
	 * java.lang.Object)
	 */
	public EditPart createEditPart(final EditPart context,
			final Object modelElement) {
		// get EditPart for model element
		EditPart part = null;
		try {
			part = getPartForElement(context, modelElement);
			// store model element in EditPart
			part.setModel(modelElement);
		} catch (RuntimeException e) {
			Activator.getDefault().logError(e.getMessage(), e);
		}
		return part;
	}

	/**
	 * Maps an object to an EditPart.
	 * 
	 * @param context
	 *            the context
	 * @param modelElement
	 *            the model element
	 * 
	 * @return the part for element
	 * 
	 * @throws RuntimeException
	 *             if no match was found (programming error)
	 */
	protected EditPart getPartForElement(final EditPart context,
			final Object modelElement) {
		if (modelElement instanceof FBType && context == null) {
			return new FBTypeRootEditPart();
		}
		if (modelElement instanceof FBType
				&& context instanceof FBTypeRootEditPart) {
			return new FBTypeEditPart(zoomManager);
		}
		if (modelElement instanceof EventInputContainer
				|| modelElement instanceof EventOutputContainer
				|| modelElement instanceof VariableInputContainer
				|| modelElement instanceof VariableOutputContainer
				|| modelElement instanceof SocketContainer
			    || modelElement instanceof PlugContainer) {
			return new InterfaceContainerEditPart();
		}

		if (modelElement instanceof Event) {
			return new InterfaceEditPart();
		}
		if (modelElement instanceof VarDeclaration) {
			if (modelElement instanceof AdapterDeclaration){
				return new AdapterInterfaceEditPart(systemPalette);
			}
			else{
				return createInterfaceEditPart();
			}
		}
		if (modelElement instanceof With) {
			return new WithEditPart();
		}
		if (modelElement instanceof CommentTypeField) {
			return new CommentTypeEditPart();
		}
		if (modelElement instanceof CommentTypeField.CommentTypeSeparator) {
			return new CommentTypeSeparatorEditPart();
		}
		if (modelElement instanceof CommentField){
			return new CommentEditPart();
		}
		if (modelElement instanceof TypeField){
			return new TypeEditPart(systemPalette);
		}
		

		throw new RuntimeException(MessageFormat.format(
				"Can't create part for model ...",
				new Object[] { ((modelElement != null) ? modelElement
						.getClass().getName() : "null") })); //$NON-NLS-1$
	}
	
	protected EditPart createInterfaceEditPart() {
		return new InterfaceEditPart();
	}
}
