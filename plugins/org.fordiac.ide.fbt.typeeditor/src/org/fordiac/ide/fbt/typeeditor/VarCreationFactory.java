/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor;

import org.fordiac.ide.gef.utilities.TemplateCreationFactory;
import org.fordiac.ide.model.data.DataType;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.VarDeclaration;

public class VarCreationFactory extends TemplateCreationFactory {
	
	//** buffer for the new type that has been created last. Required that after creation selection is working correctly.
	VarDeclaration newVar = null;

	public VarCreationFactory(DataType typeTemplate) {
		super(typeTemplate);
	}
	
	@Override
	public Object getNewObject() {
		if(null == newVar){
			newVar = createNewVarDeclaration((DataType)super.getObjectType());
		}		
		return newVar;
	}
	

	@Override
	public Object getObjectType() {
		newVar = null;
		return super.getObjectType();
	}

	public static VarDeclaration createNewVarDeclaration(DataType dataType) {
		VarDeclaration varDecl = LibraryElementFactory.eINSTANCE.createVarDeclaration();
		varDecl.setType(dataType);
		varDecl.setTypeName(dataType.getName());
		return varDecl;
	}
}
