/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.properties;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.contentoutline.ContentOutline;
import org.eclipse.ui.views.properties.tabbed.AbstractPropertySection;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.fordiac.ide.fbt.typeeditor.commands.ChangeNameCommand;
import org.fordiac.ide.fbt.typeeditor.commands.ChangeTypeCommand;
import org.fordiac.ide.fbt.typeeditor.editors.FBTypeContentOutline;
import org.fordiac.ide.fbt.typeeditor.editors.FBTypeEditor;
import org.fordiac.ide.fbt.typeeditor.editparts.CommentEditPart;
import org.fordiac.ide.fbt.typeeditor.editparts.InterfaceEditPart;
import org.fordiac.ide.fbt.typeeditor.editparts.TypeEditPart;
import org.fordiac.ide.model.Palette.AdapterTypePaletteEntry;
import org.fordiac.ide.model.Palette.PaletteEntry;
import org.fordiac.ide.model.data.DataType;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.util.IdentifierVerifyListener;
import org.fordiac.ide.util.commands.ChangeCommentCommand;

public class AdapterInterfaceElementSection extends AbstractPropertySection {
	
	protected IInterfaceElement type;
	protected CommandStack commandStack;
	protected Composite leftComposite;
	protected Composite rightComposite;
	
	private Text nameText;
	private Text commentText;
	protected Combo typeCombo;
	
	private final EContentAdapter contentAdapter = new EContentAdapter() {
		@Override
		public void notifyChanged(Notification notification) {
			if(type.eAdapters().contains(contentAdapter)){
				refresh();
			}
		}
	};
	
	@Override
	public void dispose() {
		removeContentAdapter();
		super.dispose();
	}
	
	protected void removeContentAdapter(){
		if (type != null && type.eAdapters().contains(contentAdapter)) {
			type.eAdapters().remove(contentAdapter);
		}
	}
	
	protected void addContentAdapter(){
		if(null != type  && ! type.eAdapters().contains(contentAdapter)){
			type.eAdapters().add(contentAdapter);
		}
	}
	
	protected IInterfaceElement getInputType(Object input) {
		if(input instanceof InterfaceEditPart){
			return ((InterfaceEditPart) input).getCastedModel();	
		}
		if(input instanceof TypeEditPart){
			return ((TypeEditPart) input).getCastedModel();	
		}
		if(input instanceof CommentEditPart){
			return ((CommentEditPart) input).getCastedModel();	
		}
		if(input instanceof Event){
			return (Event) input;	
		}
		if(input instanceof VarDeclaration){
			return (VarDeclaration) input;	
		}
		return null;
	}
	
	protected void setType(Object input){
		type = getInputType(input);
		addContentAdapter();
	}
	
	protected CommandStack getCommandStack(IWorkbenchPart part) {
		if(part instanceof FBTypeEditor){
			return ((FBTypeEditor)part).getCommandStack();
		}
		if(part instanceof ContentOutline){
			return ((FBTypeContentOutline) ((ContentOutline)part).getCurrentPage()).getCommandStack();
		}
		return null;
	}
	
	protected void executeCommand(Command cmd){
		if (type != null && commandStack != null) {
			commandStack.execute(cmd);
		}
	}
	
	public void createControls(final Composite parent, final TabbedPropertySheetPage tabbedPropertySheetPage) {
		super.createControls(parent, tabbedPropertySheetPage);	
		SashForm view = new SashForm(parent, SWT.HORIZONTAL | SWT.SMOOTH);
		view.setLayout(new FillLayout());
		leftComposite = getWidgetFactory().createComposite(view);
		leftComposite.setLayout(new GridLayout());
		rightComposite = getWidgetFactory().createComposite(view);
		rightComposite.setLayout(new GridLayout());	
		createTypeAndCommentSection(leftComposite);	
	}

	protected Text createGroupText(Composite group, Boolean editable) {			
		Text text = getWidgetFactory().createText(group, "", SWT.BORDER);
		text.setLayoutData(new GridData(SWT.FILL, 0, true, false));
		text.setEditable(editable);	
		text.setEnabled(editable);
		return text;
	}
	
	private void createTypeAndCommentSection(Composite parent) {
		Composite composite = getWidgetFactory().createComposite(parent);
		composite.setLayout(new GridLayout(2, false));
		composite.setLayoutData(new GridData(SWT.FILL, 0, true, false));
		getWidgetFactory().createCLabel(composite, "Name:"); 
		nameText = createGroupText(composite, true);	
		nameText.addVerifyListener(new IdentifierVerifyListener());
		nameText.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				removeContentAdapter();
				executeCommand(new ChangeNameCommand(type, nameText.getText()));
				addContentAdapter();
			}
		});
		getWidgetFactory().createCLabel(composite, "Comment:"); 
		commentText = createGroupText(composite, true);
		commentText.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				removeContentAdapter();
				executeCommand(new ChangeCommentCommand(type, commentText.getText()));
				addContentAdapter();
			}
		});
		getWidgetFactory().createCLabel(composite, "Type: ");
		typeCombo = new Combo(composite, SWT.SINGLE | SWT.READ_ONLY | SWT.BORDER);
		GridData languageComboGridData = new GridData(SWT.FILL, 0, true, false);
		typeCombo.setLayoutData(languageComboGridData);	
		typeCombo.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				DataType newType = getTypeForSelection(typeCombo.getText());
				if(null != newType){
					executeCommand(new ChangeTypeCommand((VarDeclaration)type, newType));
					//refresh();
				}
			}
			
			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
			}
		});
	}
	
	@Override
	public void setInput(final IWorkbenchPart part, final ISelection selection) {
		Assert.isTrue(selection instanceof IStructuredSelection);
		Object input = ((IStructuredSelection) selection).getFirstElement();
		commandStack = getCommandStack(part);
		if(null == commandStack){ //disable all fields
			nameText.setEnabled(false);
			commentText.setEnabled(false);
			typeCombo.removeAll();
		}
		setType(input);
	}	

	@Override
	public void refresh() {
		CommandStack commandStackBuffer = commandStack;
		commandStack = null;
		if(null != type) {
			nameText.setText(type.getName() != null ? type.getName() : "");
			commentText.setText(type.getComment() != null ? type.getComment() : "");
			setTypeDropdown();
		}
		commandStack = commandStackBuffer;
	}
	
	Collection<DataType> typeList; 
	
	protected void setTypeDropdown(){
		typeCombo.removeAll();
		
		typeList = getTypes();
		
		if(null != typeList){
			ArrayList<String> typeNames = new ArrayList<String>();
			
			for (DataType type : typeList) {
				typeNames.add(type.getName());
			}
			
			Collections.sort(typeNames);

			for (int i = 0; i < typeNames.size(); i++) {
				typeCombo.add(typeNames.get(i));
				if(typeNames.get(i).equals(((VarDeclaration)type).getType().getName())){
					typeCombo.select(i);
				}	
			}	
		}
	}
	
	protected Collection<DataType> getTypes() {
		ArrayList<DataType> types = new ArrayList<DataType>();
		
		FBType fbType = (FBType)type.eContainer().eContainer();
		PaletteEntry entry = fbType.getPaletteEntry();
		
		for (AdapterTypePaletteEntry adaptertype : TypeEditPart.getAdapterTypes(entry.getGroup().getPallete())) {
			types.add(adaptertype.getAdapterType());
		}
		
		return types;
	}

	protected DataType getTypeForSelection(String text) {
		if(null != typeList){
			for (DataType dataType : typeList) {
				if(dataType.getName().equals(text)){
					return dataType;
				}
			}
		}
		return null;
	}
}
