/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.policies;

import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.PlatformUI;
import org.fordiac.ide.gef.editparts.IConnectionHandleHelper;
import org.fordiac.ide.util.imageprovider.ImageProvider;

/**
 * The Class WithConnectionHandle.
 */
public class WithConnectionHandle extends
		org.fordiac.gmfextensions.ConnectionHandle {

	/**
	 * Instantiates a new with connection handle.
	 * 
	 * @param ownerEditPart the owner edit part
	 * @param relationshipDirection the relationship direction
	 * @param tooltip the tooltip
	 */
	public WithConnectionHandle(final GraphicalEditPart ownerEditPart,
			final HandleDirection relationshipDirection, final String tooltip) {
		super(ownerEditPart, relationshipDirection, tooltip);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.gmfextensions.ConnectionHandle#getImage(int)
	 */
	@Override
	protected Image getImage(final int side) {
		if(null == scaled){
			Image image = ImageProvider.getImage("segmentLocator.gif");
			if (getOwner() instanceof IConnectionHandleHelper) {
				scaled = new Image(PlatformUI.getWorkbench().getDisplay(), image
						.getImageData().scaledTo(
								((IConnectionHandleHelper) getOwner())
										.getConnectionHandleWidth(), 5));
			} else {
				scaled = new Image(PlatformUI.getWorkbench().getDisplay(), image
						.getImageData().scaledTo(getOwnerFigure().getSize().width,
								5));
			}
		}
		return scaled;
	}
	
	static Image scaled = null;
}
