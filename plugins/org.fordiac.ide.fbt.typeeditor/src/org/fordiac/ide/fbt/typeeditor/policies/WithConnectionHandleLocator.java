/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.policies;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.handles.HandleBounds;
import org.fordiac.gmfextensions.ConnectionHandleLocator;

/**
 * The Class WithConnectionHandleLocator.
 */
public class WithConnectionHandleLocator extends ConnectionHandleLocator {

	/** The position hint. */
	private final Point positionHint;


	/**
	 * Instantiates a new with connection handle locator.
	 * 
	 * @param reference the reference
	 * @param cursorPosition the cursor position
	 * @param positionHint the position hint
	 */
	public WithConnectionHandleLocator(final IFigure reference,
			final Point cursorPosition, final Point positionHint) {
		super(reference, cursorPosition);
		this.positionHint = positionHint;

	}

	/**
	 * Gets the reference figure bounds.
	 * 
	 * @return the reference figure bounds
	 */
	private Rectangle getReferenceFigureBounds() {
		Rectangle bounds = getReference() instanceof HandleBounds ? ((HandleBounds) getReference())
				.getHandleBounds().getCopy()
				: getReference().getBounds().getCopy();
		return bounds;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.Locator#relocate(org.eclipse.draw2d.IFigure)
	 */
	@Override
	public void relocate(final IFigure target) {
		Rectangle bounds = getReferenceFigureBounds();

		Point borderPointTranslated = positionHint.getCopy();
		getReference().translateToAbsolute(bounds);
		target.translateToRelative(bounds);
		getReference().translateToAbsolute(borderPointTranslated);
		target.translateToRelative(borderPointTranslated);

		// resetBorderPointAndSide();

		// set location based on side (either NORTH or SOUTH)
		target.setLocation(borderPointTranslated);

	}

}
