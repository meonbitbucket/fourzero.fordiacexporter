/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.commands;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.common.ui.util.StatusLineUtil;
import org.eclipse.ui.PlatformUI;
import org.fordiac.ide.model.NameRepository;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;

/**
 * The Class ChangeNameCommand.
 */
public class ChangeNameCommand extends Command {

	@Override
	public boolean canExecute() {
		return performNameCheck(name);
	}

	/** The interface element. */
	private IInterfaceElement interfaceElement;

	private final FBType type;
	
	/** The name. */
	private String name;

	/** The old name. */
	private String oldName;

	/**
	 * Instantiates a new change name command.
	 * 
	 * @param interfaceElement the interface element
	 * @param name the name
	 */
	public ChangeNameCommand(final IInterfaceElement interfaceElement, final String name) {
		super();
		this.interfaceElement = interfaceElement;
		
		if(interfaceElement.eContainer() instanceof FBType){
			//this is needed for internal veriables
			this.type = (FBType)interfaceElement.eContainer();
		}
		else{
			this.type = (FBType)interfaceElement.eContainer().eContainer();	
		}
		this.name = name;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		oldName = interfaceElement.getName();
		if(performNameCheck(name)){
			redo();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		interfaceElement.setName(oldName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		interfaceElement.setName(name);
	}

	public boolean performNameCheck(String nameToCheck) {
		
		if(nameToCheck.equals(NameRepository.getUniqueInterfaceElementName(interfaceElement, type, nameToCheck))){
			StatusLineUtil.outputErrorMessage(PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getActivePage().getActivePart(),
					null);	
			return true;
		}
		else{
			StatusLineUtil.outputErrorMessage(PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getActivePage().getActivePart(),
					"Element with Name: " + nameToCheck + " already exists!");
			return false;
		}
	}
}
