/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.actions;

import java.io.File;
import java.io.FileFilter;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.ui.actions.WorkbenchPartAction;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.fbt.typeeditor.AdaperCreationFactory;
import org.fordiac.ide.fbt.typeeditor.commands.CreateOutputEventCommand;
import org.fordiac.ide.fbt.typemanagement.wizards.NewFBTypeWizard;
import org.fordiac.ide.fbt.typemanagement.wizards.NewFBTypeWizardPage;
import org.fordiac.ide.model.Palette.AdapterTypePaletteEntry;
import org.fordiac.ide.model.Palette.PaletteEntry;
import org.fordiac.ide.model.data.EventType;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.typelibrary.EventTypeLibrary;


/** Base class for creating adapter instances from a new adapter type
 * 
 */
public abstract class CreateFromNewAdapterAction extends WorkbenchPartAction {
	/** The Constant ID. */
	public static final String ID = "CreateNewPlugAction"; //$NON-NLS-1$
	
	/** The fb type. */
	private FBType fbType;
	PaletteEntry paletteEntry;

	protected FBType getFbType() {
		return fbType;
	}

	public CreateFromNewAdapterAction(IWorkbenchPart part, FBType fbType) {
		super(part);
		setText("New Adapter ...");
		this.fbType = fbType;
	}

	@Override
	protected boolean calculateEnabled() {
		return (null != fbType);
	}

	@Override
	public void run() {
		NewFBTypeWizard wizard = new NewFBTypeWizard(){
			@Override
			protected NewFBTypeWizardPage createNewFBTypeWizardPage() {
				return new NewFBTypeWizardPage(getSelection()){
					@Override
					protected FileFilter createTemplatesFileFilter() {
						return new FileFilter() {
							@Override
							public boolean accept(File pathname) {
								//only show adapter templates in template selection box 
								return pathname.getName().toUpperCase().endsWith(".ADP");
							}
						};
					}
					
				};
			}
		};
		
		
		WizardDialog dialog = new WizardDialog(getWorkbenchPart().getSite().getShell(), wizard);
		dialog.create();
		
		if(Window.OK == dialog.open()){
			//the type could be created new execute the command
			PaletteEntry entry = wizard.getPaletteEntry();
			if((null != entry) && (entry instanceof AdapterTypePaletteEntry)){
				AdapterTypePaletteEntry adpEntry = (AdapterTypePaletteEntry)entry;
				execute(getCreationCommand(adpEntry, AdaperCreationFactory.createNewAdapterDeclaration(adpEntry)));
			}
		}
		
		CreateOutputEventCommand cmd = new CreateOutputEventCommand(
				(EventType) EventTypeLibrary.getInstance()
						.getType(null), fbType, -1);
		execute(cmd);
	}

	protected IStructuredSelection getSelection() {
		if(null != paletteEntry){
			//if we have a paletteEntry we will use this to mark the place in the typelibrary 
			//to indicate the user where he might add the adapter
			return new StructuredSelection(paletteEntry.getFile().getParent());
		}
		return new StructuredSelection();
	}

	abstract protected Command getCreationCommand(AdapterTypePaletteEntry adpEntry,
			AdapterDeclaration adapterDecl);

	public void setPaletteEntry(PaletteEntry paletteEntry) {
		this.paletteEntry = paletteEntry;
	}

}
