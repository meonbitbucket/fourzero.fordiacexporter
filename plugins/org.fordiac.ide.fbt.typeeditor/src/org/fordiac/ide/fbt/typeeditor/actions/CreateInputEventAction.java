/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.actions;

import org.eclipse.gef.ui.actions.WorkbenchPartAction;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.model.data.EventType;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.typelibrary.EventTypeLibrary;
import org.fordiac.ide.fbt.typeeditor.commands.CreateInputEventCommand;

public class CreateInputEventAction extends WorkbenchPartAction {
	/** The Constant ID. */
	public static final String ID = "InsertInputEventAction"; //$NON-NLS-1$
	
	/** The fb type. */
	private FBType fbType;

	public CreateInputEventAction(IWorkbenchPart part, FBType fbType) {
		super(part);
		setId(ID);
		setText("Create Input Event");

		this.fbType = fbType;
	}

	public FBType getFbType() {
		return fbType;
	}

	public IWorkbenchPart getWorkbenchPart(){
		return super.getWorkbenchPart();
	}
	
	@Override
	protected boolean calculateEnabled() {
		return (null != fbType);
	}

	@Override
	public void run() {
		CreateInputEventCommand cmd = new CreateInputEventCommand(
				(EventType) EventTypeLibrary.getInstance()
						.getType(null), fbType, -1);
		execute(cmd);
	}

}
