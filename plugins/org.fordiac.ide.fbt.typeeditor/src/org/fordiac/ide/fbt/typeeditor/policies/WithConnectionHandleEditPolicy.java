/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.policies;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.GraphicalEditPart;
import org.fordiac.gmfextensions.ConnectionHandle;
import org.fordiac.gmfextensions.ConnectionHandleLocator;
import org.fordiac.gmfextensions.GEFConnectionHandleEditPolicy;
import org.fordiac.gmfextensions.ConnectionHandle.HandleDirection;
import org.fordiac.ide.gef.editparts.IConnectionHandleHelper;
import org.fordiac.ide.gef.editparts.IConnectionHandleHelperConnectionHandle;

/**
 * The Class WithConnectionHandleEditPolicy.
 */
public class WithConnectionHandleEditPolicy extends
		GEFConnectionHandleEditPolicy {

	/** The amount of time to wait before showing the diagram assistant. */
	private static final int APPEARANCE_DELAY = 1;

	/**
	 * The amount of time to wait before hiding the diagram assistant after it
	 * has been made visible.
	 */
	private static final int DISAPPEARANCE_DELAY = 10000;

	/**
	 * The amount of time to wait before hiding the diagram assistant after the
	 * user has moved the mouse outside of the editpart.
	 */
	private static final int DISAPPEARANCE_DELAY_UPON_EXIT = 1;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.gmfextensions.GEFDiagramAssistantEditPolicy#getAppearanceDelay()
	 */
	@Override
	protected int getAppearanceDelay() {
		return APPEARANCE_DELAY;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.gmfextensions.GEFDiagramAssistantEditPolicy#getDisappearanceDelay()
	 */
	@Override
	protected int getDisappearanceDelay() {
		return DISAPPEARANCE_DELAY;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.gmfextensions.GEFDiagramAssistantEditPolicy#getDisappearanceDelayUponExit()
	 */
	@Override
	protected int getDisappearanceDelayUponExit() {
		return DISAPPEARANCE_DELAY_UPON_EXIT;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.gmfextensions.GEFConnectionHandleEditPolicy#getHandleFigures()
	 */
	@Override
	protected List<ConnectionHandle> getHandleFigures() {
		List<ConnectionHandle> list = new ArrayList<ConnectionHandle>(1);

		String tooltip = null;
		tooltip = buildTooltip(HandleDirection.INCOMING);
		if (tooltip != null) {
			list.add(new IConnectionHandleHelperConnectionHandle(
					(GraphicalEditPart) getHost(), HandleDirection.INCOMING,
					tooltip));
		}
		tooltip = buildTooltip(HandleDirection.OUTGOING);
		if (tooltip != null) {
			list.add(new IConnectionHandleHelperConnectionHandle(
					(GraphicalEditPart) getHost(), HandleDirection.OUTGOING,
					tooltip));
		}

		return list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.gmfextensions.GEFConnectionHandleEditPolicy#getConnectionHandleLocator(org.eclipse.draw2d.geometry.Point)
	 */
	@Override
	protected ConnectionHandleLocator getConnectionHandleLocator(
			final Point referencePoint) {
		if (getHost() instanceof IConnectionHandleHelper) {

			return new WithConnectionHandleLocator(getHostFigure(),
					referencePoint, ((IConnectionHandleHelper) getHost())
							.getConnectionHandlePosition());
		}
		return new WithConnectionHandleLocator(getHostFigure(), referencePoint,
				new Point(10, 10));

	}

}
