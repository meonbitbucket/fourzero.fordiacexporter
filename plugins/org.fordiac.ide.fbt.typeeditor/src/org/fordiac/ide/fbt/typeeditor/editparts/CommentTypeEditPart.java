package org.fordiac.ide.fbt.typeeditor.editparts;

import java.util.List;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FlowLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.gef.EditPart;

public class CommentTypeEditPart extends AbstractGraphicalEditPart implements
EditPart {
	
	/**
	 * The Class CommentTypeContainerFigure for handling the layout of one comment and type
	 * label of an fb interface.
	 */
	private class CommentTypeContainerFigure extends Figure {

		/**
		 * Instantiates a new variable output container figure.
		 */
		public CommentTypeContainerFigure() {
			FlowLayout layout = new FlowLayout(true);
			layout.setMajorSpacing(0);
			layout.setMinorSpacing(0);
			layout.setStretchMinorAxis(true);
			setLayoutManager(layout);
		}
	}

	protected CommentTypeField getCastedModel() {
		return (CommentTypeField) getModel();
	}

	@Override
	protected IFigure createFigure() {
		return new CommentTypeContainerFigure();
	}

	@Override
	protected void createEditPolicies() {

	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#getModelChildren()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	protected List getModelChildren() {
		return getCastedModel().getChildren();
	}
}
