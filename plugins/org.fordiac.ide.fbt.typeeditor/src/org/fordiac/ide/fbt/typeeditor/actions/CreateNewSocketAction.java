/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.actions;

import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.fbt.typeeditor.commands.CreateSocketCommand;
import org.fordiac.ide.model.Palette.AdapterTypePaletteEntry;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.FBType;


/** Creates a plug with a new adapter
 * 
 */
public class CreateNewSocketAction extends CreateFromNewAdapterAction {

	/** The Constant ID. */
	public static final String ID = "CreateNewSocketAction"; //$NON-NLS-1$
	

	public CreateNewSocketAction(IWorkbenchPart part, FBType fbType) {
		super(part, fbType);
	}

	@Override
	public String getId() {
		return ID;
	}

	protected Command getCreationCommand(AdapterTypePaletteEntry adpEntry,
			AdapterDeclaration adapterDecl) {
		return new CreateSocketCommand(adpEntry, adapterDecl, getFbType(), -1);
	}

}
