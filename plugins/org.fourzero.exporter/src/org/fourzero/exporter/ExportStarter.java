package org.fourzero.exporter;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.fordiac.ide.typeimport.FBTImporter;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.LibraryElement;
import org.fordiac.ide.export.forte1_0_x.*;
import org.fordiac.ide.export.*;
import org.fordiac.ide.export.utils.ExportException;
import org.fordiac.ide.model.Palette.*;
import org.fordiac.systemmanagement.SystemManager;
import org.eclipse.core.resources.IProject;
import org.fordiac.ide.typelibrary.TypeLibrary;


public class ExportStarter
{
	//args will be in the following format
	//[destination] [filePath1] [filePath 2] ... [filePath n]
	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException
	{				
		if (args.length == 1)
			System.out.println("No Function Block to export for this project");

		for (int i = 1; i < args.length; i++)
		{
			String fileName = args[i];
			System.out.println(i + ">--- Exporting Function Block: " + fileName);
			File selectedFile = new File(fileName);			

			//retrieve file extension
			String extension = "";
			int j = fileName.lastIndexOf('.');
			if (j > 0) 
				extension = fileName.substring(j + 1);			

			if (extension.equals("fbt") || extension.equals("fbs"))
			{			
				//first create importer so that "type" can be imported
				FBTImporter FBTimporter = new FBTImporter();
				LibraryElement type;
				
				try
				{
					@SuppressWarnings("unused")
					Palette palette =  TypeLibrary.getInstance().getPalette();					
					type = (LibraryElement) FBTimporter.importType(selectedFile, palette);
					System.out.print(i + "> " + fileName + " -> ");
				}
				
				catch (Exception e)
				{
					System.out.println("Error importing: " + fileName + " Exception thrown: " + e );
					continue;
				}
				
				//then create the exporter
				ForteExportFilter1_0_x exportFilter = new ForteExportFilter1_0_x();
				boolean overWrite = true;
				String OutputDirectory = args[0];
				
				try
				{
					exportFilter.export(selectedFile, OutputDirectory, overWrite, type);
					String exportedFileName = selectedFile.getName().replace(".fbt", "").replace(".fbs", "");
					
					if (i == args.length - 1) //last fb
					System.out.print(OutputDirectory + "\\" + exportedFileName + ".cpp" + " " + 
									 OutputDirectory + "\\" + exportedFileName + ".h");
					else
					System.out.print(OutputDirectory + "\\" + exportedFileName + ".cpp" + " " + 
							 OutputDirectory + "\\" + exportedFileName + ".h\n");
				}
				
				catch (ExportException e)
				{
					System.out.print("Error exporting: " + fileName + " Excpetion thrown: " + e);
					continue;
				}
			}
		}
	}		
}

