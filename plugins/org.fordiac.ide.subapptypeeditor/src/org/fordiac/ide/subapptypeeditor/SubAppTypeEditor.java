/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.subapptypeeditor;

import org.fordiac.ide.fbt.typeeditor.editors.FBTypeEditor;
import org.fordiac.ide.model.Palette.PaletteEntry;
import org.fordiac.ide.model.Palette.SubApplicationTypePaletteEntry;
import org.fordiac.ide.model.libraryElement.FBType;

public class SubAppTypeEditor extends FBTypeEditor {

	@Override
	protected FBType getFBType(PaletteEntry paletteEntry) {
		if(paletteEntry instanceof SubApplicationTypePaletteEntry){
			return ((SubApplicationTypePaletteEntry)paletteEntry).getSubApplicationType();
		}
		return null;
	}

	@Override
	protected boolean checkTypeEditorType(FBType fbType, String editorType) {
		return ((editorType.equals("ForAllTypes")) || 
				(editorType.equals("subapp")));
	}
}
