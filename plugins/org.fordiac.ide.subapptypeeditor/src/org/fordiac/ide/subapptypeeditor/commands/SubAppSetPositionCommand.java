package org.fordiac.ide.subapptypeeditor.commands;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.util.Utils;

public class SubAppSetPositionCommand extends Command {

	/** The new position. */
	private final String newX;
	private final String newY;

	/** The old position */
	private String oldX;
	private String oldY;

	/** The request. */
	private final ChangeBoundsRequest request;
	
	/** The editor. */
	private IEditorPart editor;
	private SubApp subApp;

	
	public SubAppSetPositionCommand(SubApp subApp, ChangeBoundsRequest request,
			Rectangle constraint) {
		this.subApp = subApp;
		this.request = request;
		this.newX = Integer.toString(constraint.x);
		this.newY = Integer.toString(constraint.y);
	}
	

	@Override
	public boolean canUndo() {
		return editor.equals(Utils.getCurrentActiveEditor());

	}
	
	@Override
	public boolean canExecute() {
		Object type = request.getType();
		// make sure the Request is of a type we support: (Move or
		// Move_Children)
		// e.g. a FB moves within an application
		return RequestConstants.REQ_MOVE.equals(type)
				|| RequestConstants.REQ_MOVE_CHILDREN.equals(type)
				|| RequestConstants.REQ_ALIGN_CHILDREN.equals(type);
	}
	
	@Override
	public void execute() {
		editor = Utils.getCurrentActiveEditor();
		setLabel(getLabel() + "(" + editor.getTitle() + ")"); //$NON-NLS-1$ //$NON-NLS-2$
		
		oldX = subApp.getX();
		oldY = subApp.getY();
		
		redo();
	}
	
	@Override
	public void redo() {
		subApp.setX(newX);
		subApp.setY(newY);
	}

	/**
	 * Restores the old position.
	 */
	@Override
	public void undo() {
		subApp.setX(oldX);
		subApp.setY(oldY);
	}

}
