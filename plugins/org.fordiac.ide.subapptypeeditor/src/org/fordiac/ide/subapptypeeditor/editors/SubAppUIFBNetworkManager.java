/*******************************************************************************
 * Copyright (c) 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.subapptypeeditor.editors;

import org.eclipse.emf.common.util.EList;
import org.fordiac.ide.fbt.typeeditor.network.CFBUIFBNetworkManager;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.libraryElement.SubAppType;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;

public class SubAppUIFBNetworkManager extends CFBUIFBNetworkManager {
	

	public SubAppUIFBNetworkManager(SubAppType subAppType) {
		addSubApps(subAppType.getFBNetwork().getSubApps());		
		setCompositeType(subAppType);
	}
	
	private void addSubApps(EList<SubApp> eList) {
		for (SubApp subApp : eList) {
			SubAppView subAppView = createSubAppView(subApp);
			if(null != subAppView){
				addChild(subAppView);
			}
		}
	}
	
	
	@Override
	protected InterfaceElementView getInterfaceElement(
			IInterfaceElement interfaceElement) {
		
		if(interfaceElement.eContainer().eContainer() instanceof SubApp){
			SubApp subApp = (SubApp)interfaceElement.eContainer().eContainer();
			SubAppView subAppView = getSubAppView(subApp);
			
			if(null != subAppView){
				for (InterfaceElementView viewInterFaceElement : subAppView.getInterfaceElements()) {
					if(viewInterFaceElement.getIInterfaceElement().equals(interfaceElement)){
						return viewInterFaceElement;
					}
				}
			}
		}else{
			return super.getInterfaceElement(interfaceElement);
		}
		return null;
	}

}
