/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.subapptypeeditor.policies;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gef.requests.CreateRequest;
import org.fordiac.ide.application.commands.CreateSubAppInstanceCommand;
import org.fordiac.ide.application.editparts.SubAppForFBNetworkEditPart;
import org.fordiac.ide.fbt.typeeditor.network.editparts.CompositeFBNetworkLayoutEditPolicy;
import org.fordiac.ide.model.Palette.SubApplicationTypePaletteEntry;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.subapptypeeditor.commands.SubAppSetPositionCommand;

public class SubAppTypeFBNetworkLayoutEditPolicy extends
		CompositeFBNetworkLayoutEditPolicy {

	@Override
	protected Command getCreateCommand(CreateRequest request) {
		if (request == null) {
			return null;
		}
		Object childClass = request.getNewObjectType();
		Rectangle constraint = (Rectangle) getConstraintFor(request);
		if (childClass instanceof SubApplicationTypePaletteEntry) {
			SubApplicationTypePaletteEntry type = (SubApplicationTypePaletteEntry) request
					.getNewObjectType();

			if (getHost().getModel() instanceof UIFBNetwork) {
				UIFBNetwork uiFBNetwork = (UIFBNetwork) getHost().getModel();
				CreateSubAppInstanceCommand cmd = new CreateSubAppInstanceCommand(type, 
						uiFBNetwork.getFbNetwork(), new Rectangle(
								constraint.getLocation().x, constraint.getLocation().y,
								-1, -1));
				return cmd;
			}
		}
		return super.getCreateCommand(request);
	}

	@Override
	protected Command createChangeConstraintCommand(
			final ChangeBoundsRequest request, final EditPart child,
			final Object constraint) {
		// return a command that can move a "ViewEditPart"
		if (child instanceof SubAppForFBNetworkEditPart
				&& constraint instanceof Rectangle) {
			SubAppForFBNetworkEditPart temp = (SubAppForFBNetworkEditPart) child;
			return new SubAppSetPositionCommand(temp.getCastedModel().getSubApp(), request,
					(Rectangle) constraint);
		}
		return super.createChangeConstraintCommand(request, child, constraint);
	}
	
	

}
