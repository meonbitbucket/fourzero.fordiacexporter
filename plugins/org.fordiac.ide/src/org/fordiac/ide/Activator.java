/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.core.runtime.ILogListener;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Version;

/**
 * The activator class controls the plug-in life cycle.
 */
public class Activator extends AbstractUIPlugin {

	/** The Constant PLUGIN_ID. */
	public static final String PLUGIN_ID = "org.fordiac.ide"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;

	private ILogListener listener;

	/**
	 * The constructor.
	 */
	public Activator() {
		// empty constructor
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		Version v = context.getBundle().getVersion();
		String version = v.getMajor() + "." + v.getMinor() + "." + v.getMicro();
		System.setProperty("org.fordiac.ide.version", version);

		String qualifier = v.getQualifier();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
			Date d = sdf.parse(qualifier);
			SimpleDateFormat sdf_visu = new SimpleDateFormat("yyyy-MM-dd_HHmm");
			qualifier = sdf_visu.format(d);
		} catch (Exception ex) {
			// can be ignored
		}

		System.setProperty("org.fordiac.ide.buildid", qualifier);
		System.out.println(version);
		System.out.println(v.getQualifier());

		plugin = this;

		String fordiacLogListener = System.getProperty("4diac.logListener",
				"Enabled");
		if (fordiacLogListener.equalsIgnoreCase("Enabled")) {
			listener = new FordiacLogListener();
			Platform.addLogListener(listener);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void stop(final BundleContext context) throws Exception {
		Platform.removeLogListener(listener);
		listener = null;

		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance.
	 * 
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in
	 * relative path.
	 * 
	 * @param path
	 *            the path
	 * 
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(final String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
}
