/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide;

import org.eclipse.core.runtime.ILogListener;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.swt.widgets.Display;

public class FordiacLogListener implements ILogListener {

	@Override
	public void logging(IStatus status, String plugin) {

		if ((status.getException() != null) && (!status.getPlugin().startsWith("org.fordiac"))) {
			//we have an exception not reported from a afordiac plugin to log inform the user
			if((null != Display.getCurrent()) && (null != Display.getCurrent().getActiveShell())){
				ErrorDialog.openError(Display.getCurrent().getActiveShell(),
					"Unhandled Exception Occurred", 
					"An unhandled exception occurred within 4DIAC-IDE!\n" +
							"This can result in broken systems. Therefore we recommend to save your changes and restart 4DIAC-IDE.\n" +
							"We know that this is rather annoying. If you like to help that such situation will occur less in the feature please report it by" +
							"copying the information you can find under details with some what you just did before this situation under:\n" +
							"http://sourceforge.net/p/fordiac/issues/", 
							status);
			}
		}

	}

}
