/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.intro;

import org.eclipse.osgi.util.NLS;

/**
 * The Class Messages.
 */
public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.fordiac.ide.intro.messages"; //$NON-NLS-1$

	/** The ApplicationActionBarAdvisor_LABEL_Action_NEW. */
	public static String ApplicationActionBarAdvisor_LABEL_Action_NEW;

	/** The ApplicationActionBarAdvisor_MENU_Edit. */
	public static String ApplicationActionBarAdvisor_MENU_Edit;

	/** TheApplicationActionBarAdvisor_MENU_Extras. */
	public static String ApplicationActionBarAdvisor_MENU_Extras;

	/** The ApplicationActionBarAdvisor_MENU_File. */
	public static String ApplicationActionBarAdvisor_MENU_File;

	/** The ApplicationActionBarAdvisor_MENU_Help. */
	public static String ApplicationActionBarAdvisor_MENU_Help;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
