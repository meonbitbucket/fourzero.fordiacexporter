/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.intro;

import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.ICoolBarManager;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.action.ToolBarContributionItem;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.ide.IDEActionFactory;

/**
 * The Class ApplicationActionBarAdvisor.
 */
public class ApplicationActionBarAdvisor extends ActionBarAdvisor {

	/**
	 * Instantiates a new application action bar advisor.
	 * 
	 * @param configurer the configurer
	 */
	public ApplicationActionBarAdvisor(final IActionBarConfigurer configurer) {
		super(configurer);
	}

	private IAction aboutAction;

	@Override
	protected void makeActions(final IWorkbenchWindow window) {

		if (System.getProperty("eclipse.vm") == null) {
			System.setProperty("eclipse.vm", System.getProperty("java.library.path"));
		}
		register(ActionFactory.NEW_WIZARD_DROP_DOWN.create(window));
		register(ActionFactory.NEW.create(window));
		register(ActionFactory.CLOSE.create(window));

		register(ActionFactory.NEW.create(window));

		register(ActionFactory.IMPORT.create(window));
		register(ActionFactory.EXPORT.create(window));

		register(ActionFactory.CLOSE_ALL.create(window));

		register(ActionFactory.SAVE.create(window));

		// register(ActionFactory.SAVE_AS.create(window));

		register(ActionFactory.SAVE_ALL.create(window));

		register(ActionFactory.QUIT.create(window));

		register(ActionFactory.UNDO.create(window));

		register(ActionFactory.REDO.create(window));

		register(ActionFactory.CUT.create(window));

		register(ActionFactory.COPY.create(window));

		register(ActionFactory.PASTE.create(window));

		register(ActionFactory.DELETE.create(window));

		register(ActionFactory.SELECT_ALL.create(window));

		register(ActionFactory.OPEN_NEW_WINDOW.create(window));

		register(ActionFactory.PRINT.create(window));

		register(ActionFactory.PREFERENCES.create(window));

		register(aboutAction = ActionFactory.ABOUT.create(window));

		register(IDEActionFactory.OPEN_WORKSPACE.create(window));

		// register(ActionFactory.OPEN_PERSPECTIVE_DIALOG.create(window));

	}

	@Override
	protected void fillMenuBar(final IMenuManager menu) {

		IMenuManager menuX = new MenuManager(
				Messages.ApplicationActionBarAdvisor_MENU_File, "org.fordiac.ide.file");
		menuX.add(getAction(ActionFactory.NEW_WIZARD_DROP_DOWN.getId()));
		getAction(ActionFactory.NEW_WIZARD_DROP_DOWN.getId()).setText("New");

		menu.add(menuX);
		//
		// // menuX.add(new GroupMarker(IWorkbenchActionConstants.FILE_START));
		//
		IMenuManager menuXX = new MenuManager(
				Messages.ApplicationActionBarAdvisor_MENU_Extras,
				"org.fordiac.ide.extras");
		menuXX.add(getAction(ActionFactory.PREFERENCES.getId()));
		// menuXX.add(getAction(ActionFactory.OPEN_PERSPECTIVE_DIALOG.getId()));

		menuXX.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
		// menuX.add(menuXX);

		// menuX.add(new Separator());

		// menuX.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));

		menuX.add(new Separator());
		// IAction action = getAction(ActionFactory.NEW.getId());
		// // FIXME use orig new menu!
		// action.setText(Messages.ApplicationActionBarAdvisor_LABEL_Action_NEW);
		// menuX.add(action);

		menuX.add(getAction(ActionFactory.IMPORT.getId()));
		menuX.add(getAction(ActionFactory.EXPORT.getId()));
		menuX.add(getAction(ActionFactory.PRINT.getId()));

		menuX.add(getAction(IDEActionFactory.OPEN_WORKSPACE.getId()));

		menuX.add(getAction(ActionFactory.CLOSE.getId()));

		menuX.add(getAction(ActionFactory.CLOSE_ALL.getId()));

		menuX.add(new Separator());

		menuX.add(getAction(ActionFactory.SAVE.getId()));

		// menuX.add(getAction(ActionFactory.SAVE_AS.getId()));

		menuX.add(getAction(ActionFactory.SAVE_ALL.getId()));

		menuX.add(new Separator());

		menuX.add(getAction(ActionFactory.QUIT.getId()));

		menuX.add(new GroupMarker(IWorkbenchActionConstants.FILE_END));
		menu.add(menuX);

		menuX = new MenuManager(Messages.ApplicationActionBarAdvisor_MENU_Edit,
				IWorkbenchActionConstants.M_EDIT);

		menuX.add(new GroupMarker(IWorkbenchActionConstants.EDIT_START));

		menuX.add(getAction(ActionFactory.UNDO.getId()));

		menuX.add(getAction(ActionFactory.REDO.getId()));

		menuX.add(new GroupMarker(IWorkbenchActionConstants.UNDO_EXT));

		menuX.add(new Separator());

		menuX.add(getAction(ActionFactory.CUT.getId()));

		menuX.add(getAction(ActionFactory.COPY.getId()));

		menuX.add(getAction(ActionFactory.PASTE.getId()));

		menuX.add(new GroupMarker(IWorkbenchActionConstants.CUT_EXT));

		menuX.add(new Separator());

		menuX.add(getAction(ActionFactory.DELETE.getId()));

		menuX.add(getAction(ActionFactory.SELECT_ALL.getId()));

		menuX.add(new Separator());

		menuX.add(new GroupMarker(IWorkbenchActionConstants.ADD_EXT));

		menuX.add(new GroupMarker(IWorkbenchActionConstants.EDIT_END));

		// menuX.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		menu.add(menuX);

		// menu.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));

		// menuX = new MenuManager("Window",
		// IWorkbenchActionConstants.M_WINDOW);

		// menuX.add(getAction(ActionFactory.OPEN_NEW_WINDOW.getId()));

		// menuX.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
		menu.add(menuX);

		menuX = new MenuManager(Messages.ApplicationActionBarAdvisor_MENU_Help,
				IWorkbenchActionConstants.M_HELP);
		menuX.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));

		// menuX.add(new GroupMarker(IWorkbenchActionConstants.HELP_START));
		//
		menuX.add(aboutAction);
		//
		// menuX.add(new GroupMarker(IWorkbenchActionConstants.HELP_END));

		// menuX.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
		// menu.add(new MenuManager("Edit", "org.fordiac.ide.edit"));
		menu.add(menuXX);
		menu.add(menuX);

	}

	@Override
	protected void fillCoolBar(final ICoolBarManager toolBar) {
		IMenuManager popUpMenu = new MenuManager();
		// popUpMenu.add(new ActionContributionItem(lockToolBarAction));
		// popUpMenu.add(new ActionContributionItem(toggleCoolbarAction));
		toolBar.setContextMenuManager(popUpMenu);

		toolBar.add(new GroupMarker("group.file")); //$NON-NLS-1$

		{
			IToolBarManager toolBarX = new ToolBarManager();

			toolBarX.add(new Separator(IWorkbenchActionConstants.NEW_GROUP));

			toolBarX.add(new GroupMarker(IWorkbenchActionConstants.NEW_EXT));

			toolBarX.add(new GroupMarker(IWorkbenchActionConstants.SAVE_GROUP));

			toolBarX.add(getAction(ActionFactory.SAVE.getId()));

			toolBarX.add(new GroupMarker(IWorkbenchActionConstants.SAVE_EXT));

			toolBarX.add(getAction(ActionFactory.PRINT.getId()));

			toolBarX.add(new GroupMarker(IWorkbenchActionConstants.PRINT_EXT));

			toolBarX.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
			toolBar.add(new ToolBarContributionItem(toolBarX,
					IWorkbenchActionConstants.TOOLBAR_FILE));
		}

		toolBar.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));

		toolBar.add(new GroupMarker("group.nav")); //$NON-NLS-1$

		toolBar.add(new GroupMarker(IWorkbenchActionConstants.GROUP_EDITOR));

		toolBar.add(new GroupMarker(IWorkbenchActionConstants.GROUP_HELP));

		{
			IToolBarManager toolBarX = new ToolBarManager();

			toolBarX.add(new Separator(IWorkbenchActionConstants.GROUP_HELP));

			toolBarX.add(new GroupMarker(IWorkbenchActionConstants.GROUP_APP));
			toolBar.add(new ToolBarContributionItem(toolBarX,
					IWorkbenchActionConstants.TOOLBAR_HELP));
		}
	}
}
