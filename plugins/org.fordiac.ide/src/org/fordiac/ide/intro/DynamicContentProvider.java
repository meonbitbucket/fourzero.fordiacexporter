/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.intro;

import java.io.PrintWriter;
import java.util.Date;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.intro.config.IIntroContentProviderSite;
import org.eclipse.ui.intro.config.IIntroXHTMLContentProvider;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * The Class DynamicContentProvider.
 */
public class DynamicContentProvider implements IIntroXHTMLContentProvider {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.intro.config.IIntroContentProvider#init(org.eclipse.ui.intro
	 * .config.IIntroContentProviderSite)
	 */
	public void init(IIntroContentProviderSite site) {
		// empty
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.intro.config.IIntroContentProvider#createContent(java.lang
	 * .String, java.io.PrintWriter)
	 */
	public void createContent(String id, PrintWriter out) {
		// empty
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.intro.config.IIntroContentProvider#createContent(java.lang
	 * .String, org.eclipse.swt.widgets.Composite,
	 * org.eclipse.ui.forms.widgets.FormToolkit)
	 */
	public void createContent(String id, Composite parent, FormToolkit toolkit) {
		// empty
	}

	private String getCurrentTimeString() {
		StringBuffer content = new StringBuffer(
				"Dynamic content from Intro ContentProvider: ");
		content.append("Current time is: ");
		content.append(new Date(System.currentTimeMillis()));
		return content.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.intro.config.IIntroXHTMLContentProvider#createContent(java
	 * .lang.String, org.w3c.dom.Element)
	 */
	public void createContent(String id, Element parent) {
		Document dom = parent.getOwnerDocument();
		Element para = dom.createElement("p");
		para.setAttribute("id", "someDynamicContentId");
		para.appendChild(dom.createTextNode(getCurrentTimeString()));
		parent.appendChild(para);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.intro.config.IIntroContentProvider#dispose()
	 */
	public void dispose() {
		// empty
	}

}
