/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.intro;

import org.eclipse.core.runtime.Platform;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;

/**
 * This class controls all aspects of the application's execution.
 */
public class Application implements IApplication {
	private static final String PROP_EXIT_CODE = "eclipse.exitcode";

	/* (non-Javadoc)
	 * @see org.eclipse.equinox.app.IApplication#start(org.eclipse.equinox.app.IApplicationContext)
	 */
	public Object start(final IApplicationContext context) {
		System.out.println("*** InstanceLocation: " 
				+ Platform.getInstanceLocation().getURL());
		//
		// // 1. get config pref node
		// Preferences node = new ConfigurationScope()
		// .getNode(IDEWorkbenchPlugin.IDE_WORKBENCH);
		//
		// // 2. get value for showDialog
		// node.putBoolean(IDE.Preferences.SHOW_WORKSPACE_SELECTION_DIALOG, true);
		//
		// // 6. store the node
		// try {
		// node.flush();
		// } catch (BackingStoreException e) {
		// // do nothing
		// }

		Display display = PlatformUI.createDisplay();
		try {
			int returnCode = PlatformUI.createAndRunWorkbench(display,
					new ApplicationWorkbenchAdvisor());

			if (returnCode != PlatformUI.RETURN_RESTART) {
				return EXIT_OK; // if the exit code property has been set to the
				// relaunch code, then // return that code now,
				// otherwise this is a normal restart
			}
			return EXIT_RELAUNCH.equals(Integer.getInteger(PROP_EXIT_CODE)) ? EXIT_RELAUNCH
					: EXIT_RESTART;

			// return IApplication.EXIT_OK;
		} finally {
			display.dispose();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.equinox.app.IApplication#stop()
	 */
	public void stop() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		if (workbench == null) {
			return;
		}
		final Display display = workbench.getDisplay();
		display.syncExec(new Runnable() {
			public void run() {
				if (!display.isDisposed()) {
					workbench.close();
				}
			}
		});
	}
}
