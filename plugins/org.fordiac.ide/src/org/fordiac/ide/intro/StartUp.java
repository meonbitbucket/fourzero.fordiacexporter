/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.intro;

import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PerspectiveAdapter;
import org.eclipse.ui.PlatformUI;

/**
 * The Class StartUp.
 */
public class StartUp implements IStartup {

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IStartup#earlyStartup()
	 */
	public void earlyStartup() {
		/*
		 * The registration of the listener should have been done in the UI thread
		 * since PlatformUI.getWorkbench().getActiveWorkbenchWindow() returns null
		 * if it is called outside of the UI thread.
		 */
		Display.getDefault().asyncExec(new Runnable() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see java.lang.Runnable#run()
			 */
			public void run() {
				final IWorkbenchWindow workbenchWindow = PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow();
				if (workbenchWindow != null) {
					workbenchWindow.addPerspectiveListener(new PerspectiveAdapter() {
						/*
						 * (non-Javadoc)
						 * 
						 * @see
						 * org.eclipse.ui.PerspectiveAdapter#perspectiveActivated(org.eclipse
						 * .ui.IWorkbenchPage, org.eclipse.ui.IPerspectiveDescriptor)
						 */
						@Override
						public void perspectiveActivated(IWorkbenchPage page,
								IPerspectiveDescriptor perspectiveDescriptor) {
							super.perspectiveActivated(page, perspectiveDescriptor);
						}

						@Override
						public void perspectiveDeactivated(IWorkbenchPage page,
								IPerspectiveDescriptor perspective) {
							super.perspectiveDeactivated(page, perspective);

						}

					});
				}
			}
		});
	}

}
