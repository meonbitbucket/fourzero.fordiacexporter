/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util.imageprovider;

import org.eclipse.osgi.util.NLS;

/**
 * The Class Messages.
 */
public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.fordiac.ide.util.imageprovider.messages"; //$NON-NLS-1$
	
	/** The Image provider_ erro r_ not found. */
	public static String ImageProvider_ERROR_NotFound;
	
	/** The Image provider_ fil e_ basicfb. */
	public static String ImageProvider_FILE_Basicfb;
	
	/** The Image provider_ fil e_ composite fb. */
	public static String ImageProvider_FILE_CompositeFB;
	
	/** The Image provider_ fil e_ sifb. */
	public static String ImageProvider_FILE_SIFB;
	
	/** The Image provider_image directory. */
	public static String ImageProvider_imageDirectory;
	
	/** The Image provider_ labe l_ image. */
	public static String ImageProvider_LABEL_Image;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
		// private empty constructor
	}
}
