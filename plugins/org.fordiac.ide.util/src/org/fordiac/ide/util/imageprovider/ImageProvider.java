/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

package org.fordiac.ide.util.imageprovider;

import java.util.Hashtable;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DecorationOverlayIcon;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.fordiac.ide.util.Activator;

/**
 * This class provides features to manage the handling of images/icons.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class ImageProvider {
	private static final String IMAGES_DIRECTORY = Messages.ImageProvider_imageDirectory;
	private static Hashtable<String, Image> images = new Hashtable<String, Image>();
	private static Hashtable<String, ImageDescriptor> imageDescriptors = new Hashtable<String, ImageDescriptor>();
	private static Hashtable<Image, Image> errorImages = new Hashtable<Image, Image>();
	
	/** The filename for the basifb icon. */
	public static String ICON_BASICFB = Messages.ImageProvider_FILE_Basicfb;

	/** The filename for the composite icon. */
	public static String ICON_COMPOSITEFB = Messages.ImageProvider_FILE_CompositeFB;

	/** The filename for the serviceinterface icon. */
	public static String ICON_SERVICEFB = Messages.ImageProvider_FILE_SIFB;

	/**
	 * Returns an Image object (from the central image repository).
	 * 
	 * @param imageName the image name
	 * 
	 * @return the image
	 */
	public static Image getImage(final String imageName) {
		return getImage(Activator.PLUGIN_ID, imageName);
	}

	/**
	 * Returns an Image object (from the central image repository).
	 * 
	 * @param imageName the image name
	 * @param pluginid the pluginid
	 * 
	 * @return the image
	 */
	public static Image getImage(String pluginid, final String imageName) {
		if (!images.containsKey(imageName)) {
			try {
				// Image image = new Image(Display.getCurrent(), uri
				// .toFileString()
				// + imageName);
				ImageDescriptor imageDesc = Activator.imageDescriptorFromPlugin(
						pluginid, IMAGES_DIRECTORY + "/" + imageName); //$NON-NLS-1$

				Image image = null;
				if (imageDesc != null) {
					image = imageDesc.createImage();
				}
				// the image was not found in the plug-ins image directory, try
				// to find it in the images directory of the install location
				// furthermore, third party plug-ins can use the imageprovider
				// using the images directory of the install location - no
				// change of the util plug-in is required - otherwise it would
				// be necessary to add the image to the util plugin
				if (image == null) {
					URI uri = URI.createFileURI(Platform.getInstallLocation().getURL()
							.getFile()
							+ IMAGES_DIRECTORY);
					image = new Image(Display.getCurrent(), uri.toFileString()
							+ imageName);
				}

				if (image != null) {
					images.put(imageName, image);
				} 
			} catch (Exception ex) {
				org.fordiac.ide.util.Activator.getDefault().logError(
						Messages.ImageProvider_LABEL_Image + imageName
								+ Messages.ImageProvider_ERROR_NotFound, ex);
				ISharedImages si = PlatformUI.getWorkbench().getSharedImages();
				return si.getImage(ISharedImages.IMG_OBJS_ERROR_TSK);
			}
		}
		if (images.containsKey(imageName)) {
			return images.get(imageName);
		} else {
			ISharedImages si = PlatformUI.getWorkbench().getSharedImages();
			return si.getImage(ISharedImages.IMG_OBJS_ERROR_TSK);
		}
	}

	/**
	 * Returns an Image object (from the central image repository).
	 * 
	 * @param imageName the image name
	 * 
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(final String imageName) {
		if (!imageDescriptors.containsKey(imageName)) {
			Image image = getImage(imageName);
			ImageDescriptor descriptor = ImageDescriptor.createFromImage(image);
			imageDescriptors.put(imageName, descriptor);
		}
		return imageDescriptors.get(imageName);
	}

	/**
	 * Returns an Image object (from the central image repository).
	 * 
	 * @param imageName the image name
	 * @param pluginid the pluginid
	 * 
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String pluginid,
			final String imageName) {
		if (!imageDescriptors.containsKey(imageName)) {
			Image image = getImage(pluginid, imageName);
			ImageDescriptor descriptor = ImageDescriptor.createFromImage(image);
			imageDescriptors.put(imageName, descriptor);
		}
		return imageDescriptors.get(imageName);
	}
	private static int count = 0;
	
	public static Image getErrorOverlayImage(Image image) {
		if (image == null) {
			return getImage("error.gif");
		}
		if (!errorImages.containsKey(image)) {
			ImageDescriptor descriptor = ImageProvider.getImageDescriptor("error.gif");
			DecorationOverlayIcon overlay = new DecorationOverlayIcon(image, descriptor, IDecoration.TOP_LEFT);
			count++;
			System.out.println("createErrorOverlayImage " + count);
			errorImages.put(image, overlay.createImage());
		}
		return errorImages.get(image);
	}
}
