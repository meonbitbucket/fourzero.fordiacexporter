/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util.dnd;

import org.eclipse.gef.dnd.SimpleObjectTransfer;

/**
 * Transfer object used to transfer the template of a palette tool. It is used
 * for drag-and-drop from the palette as well as copy/paste from the palette.
 * 
 * @author Eric Bordeau
 * @since 2.1
 */
public final class FbTypeTransfer extends SimpleObjectTransfer {

	private static final FbTypeTransfer INSTANCE = new FbTypeTransfer();
	private static final String TYPE_NAME = "Template transfer"//$NON-NLS-1$
			+ System.currentTimeMillis() + ":" + INSTANCE.hashCode();//$NON-NLS-1$
	private static final int TYPEID = registerType(TYPE_NAME);
	private Object createdObject = null;

	private FbTypeTransfer() {
		// empty private constructor
	}

	/**
	 * Returns the singleton instance.
	 * 
	 * @return the singleton
	 */
	public static FbTypeTransfer getInstance() {
		return INSTANCE;
	}

	/**
	 * Returns the <i>template</i> object.
	 * 
	 * @return the template
	 */
	public Object getTemplate() {
		return getObject();
	}

	/**
	 * @see org.eclipse.swt.dnd.Transfer#getTypeIds()
	 */
	@Override
	protected int[] getTypeIds() {
		return new int[] { TYPEID };
	}

	/**
	 * @see org.eclipse.swt.dnd.Transfer#getTypeNames()
	 */
	@Override
	protected String[] getTypeNames() {
		return new String[] { TYPE_NAME };
	}

	/**
	 * Sets the <i>template</i> Object.
	 * 
	 * @param template the template
	 */
	public void setTemplate(final Object template) {
		setObject(template);
	}
	
	public Object getCreatedObject() {
		return createdObject;
	}

	public void setCreatedObject(Object createdObject) {
		this.createdObject = createdObject;
	}
}
