/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util.editorpart;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.EditorPart;
import org.fordiac.ide.model.libraryElement.LibraryElement;
import org.fordiac.ide.model.libraryElement.VersionInfo;
import org.fordiac.ide.util.commands.AddNewVersionInfoCommand;
import org.fordiac.ide.util.commands.ChangeApplicationDomainCommand;
import org.fordiac.ide.util.commands.ChangeAuthorCommand;
import org.fordiac.ide.util.commands.ChangeClassificationCommand;
import org.fordiac.ide.util.commands.ChangeCommentCommand;
import org.fordiac.ide.util.commands.ChangeDateCommand;
import org.fordiac.ide.util.commands.ChangeDescriptionCommand;
import org.fordiac.ide.util.commands.ChangeFunctionCommand;
import org.fordiac.ide.util.commands.ChangeIdentifcationTypeCommand;
import org.fordiac.ide.util.commands.ChangeOrganizationCommand;
import org.fordiac.ide.util.commands.ChangeRemarksCommand;
import org.fordiac.ide.util.commands.ChangeStandardCommand;
import org.fordiac.ide.util.commands.ChangeVersionCommand;
import org.fordiac.ide.util.commands.DeleteVersionInfoCommand;
import org.fordiac.ide.util.contentprovider.VersionContentProvider;
import org.fordiac.ide.util.imageprovider.ImageProvider;
import org.fordiac.ide.util.labelprovider.VersionLabelProvider;

/**
 * The Class GeneralInformation.
 */
public abstract class GeneralInformation extends EditorPart {

	/** The Constant VERSION_PROPERTY. */
	private static final String VERSION_PROPERTY = "version";

	/** The Constant ORGANIZATION_PROPERTY. */
	private static final String ORGANIZATION_PROPERTY = "organization";

	/** The Constant AUTHOR_PROPERTY. */
	private static final String AUTHOR_PROPERTY = "author";

	/** The Constant DATE_PROPERTY. */
	private static final String DATE_PROPERTY = "date";

	/** The Constant REMARKS_PROPERTY. */
	private static final String REMARKS_PROPERTY = "remarks";

	/** The type. */
	private LibraryElement type;

	public LibraryElement getType() {
		return type;
	}

	public void setType(LibraryElement type) {
		this.type = type;
		if(null != this.type){
			type.eAdapters().add(adapter);
		}
	}
	
	private final EContentAdapter adapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {
			super.notifyChanged(notification);
			
			if((!blockListeners) && (null != type)){
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						blockListeners = true;
						
						if((null != parent) && (!parent.isDisposed())){
							if(null != type.getComment()){
								commentText.setText(type.getComment());
							}
							updateIdentificationInfo();
							versionViewer.refresh();
						}
						
						blockListeners = false;
					}
				});
			}
		}
	};

	/** The comment text. */
	private Text commentText;

	/** The standard text. */
	private Text standardText;

	/** The classification text. */
	private Text classificationText;

	/** The domain text. */
	private Text domainText;

	/** The function text. */
	private Text functionText;

	/** The type text. */
	private Text typeText;

	/** The description text. */
	private Text descriptionText;

	/** The version viewer. */
	private TableViewer versionViewer;

	/** The delete row action. */
	private IAction deleteRowAction;

	/**
	 * The Class NewVersionInfoAction.
	 */
	private class NewVersionInfoAction extends Action {
		/**
		 * Instantiates a new new version info action.
		 */
		public NewVersionInfoAction() {
			super("New VersionInfo");
			setImageDescriptor(ImageProvider.getImageDescriptor("add_obj.gif"));
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.action.Action#run()
		 */
		@Override
		public void run() {
			getCommandStack().execute(new AddNewVersionInfoCommand(type));
		}
	}

	/**
	 * The Class DeleteVersionInfoAction.
	 */
	private class DeleteVersionInfoAction extends Action {

		/**
		 * Instantiates a new delete version info action.
		 */
		public DeleteVersionInfoAction() {
			super("Delete VersionInfo");
			setImageDescriptor(ImageProvider.getImageDescriptor("delete_obj.gif"));
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.action.Action#run()
		 */
		@Override
		public void run() {
			getCommandStack().execute(new DeleteVersionInfoCommand(type,
					(VersionInfo)((IStructuredSelection) versionViewer.getSelection())
							.getFirstElement()));
		}
	}

	/**
	 * Instantiates a new general information.
	 */
	public GeneralInformation() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor
	 * )
	 */
	@Override
	public void doSave(final IProgressMonitor monitor) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.EditorPart#doSaveAs()
	 */
	@Override
	public void doSaveAs() {
	}
	
	@Override
	public boolean isDirty() {
		return getCommandStack().isDirty();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.EditorPart#isSaveAsAllowed()
	 */
	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}
	
	private Composite parent;

	public Composite getParent() {
		return parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	public void createPartControl(final Composite comp) {
		//comp.setLayout(new GridLayout());
		comp.setLayout(new FillLayout());
		
		ScrolledComposite scrollContainer = new ScrolledComposite(comp, SWT.H_SCROLL| SWT.V_SCROLL);
		parent = new Composite(scrollContainer, SWT.NONE);
		
		// Set the child as the scrolled content of the ScrolledComposite
		scrollContainer.setContent(parent);
	
	    // Set the minimum size
		scrollContainer.setMinSize(900, 800);
	
	    // Expand both horizontally and vertically
		scrollContainer.setExpandHorizontal(true);
		scrollContainer.setExpandVertical(true);

		GridLayout mainLayout = new GridLayout(2, false);
		parent.setLayout(mainLayout);
		parent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		Label nameLabel = new Label(parent, SWT.NONE);
		nameLabel.setText("Name");

		Text fbTypeNameText = new Text(parent, SWT.BORDER);
		fbTypeNameText.setText(type != null ? type.getName() : "FBType");
		GridData fbTypeNameTextData = new GridData();
		fbTypeNameTextData.horizontalAlignment = GridData.FILL;
		fbTypeNameTextData.grabExcessHorizontalSpace = true;
		fbTypeNameText.setLayoutData(fbTypeNameTextData);
		fbTypeNameText.setEditable(false);

		Label commentLabel = new Label(parent, SWT.NONE);
		commentLabel.setText("Comment: ");

		commentText = new Text(parent, SWT.SINGLE | SWT.BORDER);
		GridData commentTextGridData = new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_BEGINNING);
		commentText.setLayoutData(commentTextGridData);
		commentText.setText((type != null && type.getComment() != null) ? type.getComment() : ""); 
		commentText.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				if(!blockListeners){
					if (!commentText.isDisposed()) {
						// update variable comment
						if (type != null) {
							blockListeners = true;
							getCommandStack().execute(new ChangeCommentCommand(type, commentText.getText()));
							blockListeners = false;
						}
					}
				}
			}
		});

		createIdentificationGroup(parent);

		createVersionInfoGroup(parent);
	}

	/**
	 * Creates the version info group.
	 * 
	 * @param parent
	 *          the parent
	 */
	private void createVersionInfoGroup(final Composite parent) {
		Group versionInfoGroup = new Group(parent, SWT.NONE);
		versionInfoGroup.setText("Version Info");
		GridData versionInfoGroupData = new GridData();
		versionInfoGroupData.horizontalAlignment = GridData.FILL;
		versionInfoGroupData.grabExcessHorizontalSpace = true;
		versionInfoGroupData.horizontalSpan = 2;
		versionInfoGroup.setLayoutData(versionInfoGroupData);

		GridLayout versionInfoGroupLayout = new GridLayout();
		versionInfoGroup.setLayout(versionInfoGroupLayout);

		// Composite buttons = new Composite(versionInfoGroup, SWT.NONE);
		// buttons.setLayout(new RowLayout());

		Composite groupComp = new Composite(versionInfoGroup, SWT.NONE);

		GridData groupLayoutData = new GridData();
		groupLayoutData.horizontalAlignment = GridData.FILL;
		groupLayoutData.verticalAlignment = GridData.FILL;
		groupLayoutData.grabExcessHorizontalSpace = true;
		groupLayoutData.grabExcessVerticalSpace = true;
		groupLayoutData.verticalSpan = 30;
		groupComp.setLayoutData(groupLayoutData);

		final Table table = new Table(groupComp, SWT.FULL_SELECTION | SWT.BORDER);
		table.setLinesVisible(true);
		versionViewer = new TableViewer(table);

		TableColumn column1 = new TableColumn(versionViewer.getTable(), SWT.LEFT);
		column1.setText("Version"); //$NON-NLS-1$		
		TableColumn column2 = new TableColumn(versionViewer.getTable(), SWT.LEFT);
		column2.setText("Organization"); //$NON-NLS-1$
		TableColumn column3 = new TableColumn(versionViewer.getTable(), SWT.LEFT);
		column3.setText("Author"); //$NON-NLS-1$
		TableColumn column4 = new TableColumn(versionViewer.getTable(), SWT.LEFT);
		column4.setText("Date"); //$NON-NLS-1$
		TableColumn column5 = new TableColumn(versionViewer.getTable(), SWT.LEFT);
		column5.setText("Remarks"); //$NON-NLS-1$

		TableColumnLayout layout = new TableColumnLayout();
		groupComp.setLayout( layout );

		layout.setColumnData( column1, new ColumnWeightData( 8 ) );
		layout.setColumnData( column2, new ColumnWeightData( 20 ) );
		layout.setColumnData( column3, new ColumnWeightData( 8 ) );
		layout.setColumnData( column4, new ColumnWeightData( 10 ) );
		layout.setColumnData( column5, new ColumnWeightData( 54 ) );

		// versionViewer.getTable().setLayoutData(internalVarViewerGridData);
		versionViewer.getTable().setHeaderVisible(true);

		versionViewer.setContentProvider(new VersionContentProvider(type));

		versionViewer.setLabelProvider(new VersionLabelProvider());

		versionViewer.setInput(new Object());

		versionViewer.setCellModifier(new ICellModifier() {
			public boolean canModify(final Object element, final String property) {
				return true;
			}

			public Object getValue(final Object element, final String property) {
				if (VERSION_PROPERTY.equals(property)) {
					return ((VersionInfo) element).getVersion();
				} else if (ORGANIZATION_PROPERTY.equals(property)) {
					return ((VersionInfo) element).getOrganization();
				} else if (AUTHOR_PROPERTY.equals(property)) {
					return ((VersionInfo) element).getAuthor();
				} else if (DATE_PROPERTY.equals(property)) {
					return ((VersionInfo) element).getDate();
				} else /* if (REMARKS_PROPERTY.equals(property)) */{
					return ((VersionInfo) element).getRemarks();
				}
			}

			public void modify(final Object element, final String property,
					final Object value) {
				TableItem tableItem = (TableItem) element;
				VersionInfo data = (VersionInfo) tableItem.getData();
				if(!blockListeners){
					if (VERSION_PROPERTY.equals(property)) {
						blockListeners = true;
						getCommandStack().execute(new ChangeVersionCommand(data, value.toString()));
						blockListeners = false;
					} else if (ORGANIZATION_PROPERTY.equals(property)) {
						blockListeners = true;
						getCommandStack().execute(new ChangeOrganizationCommand(data, value.toString()));
						blockListeners = false;
					} else if (AUTHOR_PROPERTY.equals(property)) {
						blockListeners = true;
						getCommandStack().execute(new ChangeAuthorCommand(data, value.toString()));
						blockListeners = false;
					} else if (DATE_PROPERTY.equals(property)) {
						blockListeners = true;
						getCommandStack().execute(new ChangeDateCommand(data, value.toString()));
						blockListeners = false;
					} else /* if (REMARKS_PROPERTY.equals(property)) */{
						blockListeners = true;
						getCommandStack().execute(new ChangeRemarksCommand(data, value.toString()));
						blockListeners = false;
					}

				}
				versionViewer.refresh(data);
				
			}
		});

		versionViewer.setCellEditors(new CellEditor[] { new TextCellEditor(table),
				new TextCellEditor(table), new TextCellEditor(table),
				new TextCellEditor(table), new TextCellEditor(table) });

		versionViewer
				.setColumnProperties(new String[] { VERSION_PROPERTY,
						ORGANIZATION_PROPERTY, AUTHOR_PROPERTY, DATE_PROPERTY,
						REMARKS_PROPERTY });

		versionViewer
				.addPostSelectionChangedListener(new ISelectionChangedListener() {

					@Override
					public void selectionChanged(final SelectionChangedEvent event) {
						boolean enabled = (!versionViewer.getSelection().isEmpty() && ((IStructuredSelection) versionViewer
								.getSelection()).getFirstElement() instanceof VersionInfo);

						deleteRowAction.setEnabled(enabled);
					}

				});

		MenuManager popupMenu = new MenuManager();
		IAction newRowAction = new NewVersionInfoAction();
		popupMenu.add(newRowAction);
		deleteRowAction = new DeleteVersionInfoAction();
		deleteRowAction.setEnabled(false);
		popupMenu.add(deleteRowAction);
		Menu menu = popupMenu.createContextMenu(table);
		table.setMenu(menu);
	}

	/**
	 * Creates the identification group.
	 * 
	 * @param parent
	 *          the parent
	 */
	private void createIdentificationGroup(final Composite parent) {
		Group identificationGroup = new Group(parent, SWT.NONE);
		identificationGroup.setText("Identification");
		GridData identificationGroupData = new GridData();
		identificationGroupData.horizontalAlignment = GridData.FILL;
		identificationGroupData.grabExcessHorizontalSpace = true;
		identificationGroupData.horizontalSpan = 2;
		//identificationGroupData.verticalSpan = 33;
		identificationGroup.setLayoutData(identificationGroupData);

		GridLayout identificationGroupLayout = new GridLayout(2, false);
		identificationGroup.setLayout(identificationGroupLayout);

		Label l = new Label(identificationGroup, SWT.None);
		l.setText("Standard: ");

		GridData standardTextData = new GridData();
		standardTextData.horizontalAlignment = GridData.FILL;
		standardTextData.grabExcessHorizontalSpace = true;
		standardText = new Text(identificationGroup, SWT.BORDER);
		standardText.setLayoutData(standardTextData);
		standardText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				if(!blockListeners){
					if (type != null) {
						blockListeners = true;
						getCommandStack().execute(new ChangeStandardCommand(type, standardText.getText()));
						blockListeners = false;
					}
				}
			}
		});

		l = new Label(identificationGroup, SWT.None);
		l.setText("Classification: ");

		GridData classificationTextData = new GridData();
		classificationTextData.horizontalAlignment = GridData.FILL;
		classificationTextData.grabExcessHorizontalSpace = true;
		classificationText = new Text(identificationGroup, SWT.BORDER);
		classificationText.setLayoutData(classificationTextData);
		classificationText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				if(!blockListeners){
					if (type != null) {
						blockListeners = true;
						getCommandStack().execute(new ChangeClassificationCommand(type, classificationText.getText()));
						blockListeners = false;
					}
				}
			}

		});

		l = new Label(identificationGroup, SWT.None);
		l.setText("Application Domain: ");

		GridData domainTextData = new GridData();
		domainTextData.horizontalAlignment = GridData.FILL;
		domainTextData.grabExcessHorizontalSpace = true;
		domainText = new Text(identificationGroup, SWT.BORDER);
		domainText.setLayoutData(domainTextData);
		domainText.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(final ModifyEvent e) {
				if(!blockListeners){
					if (type != null) {
						blockListeners = true;
						getCommandStack().execute(new ChangeApplicationDomainCommand(type, domainText.getText()));
						blockListeners = false;
					}
				}
			}

		});

		l = new Label(identificationGroup, SWT.None);
		l.setText("Function: ");

		GridData functionTextData = new GridData();
		functionTextData.horizontalAlignment = GridData.FILL;
		functionTextData.grabExcessHorizontalSpace = true;
		functionText = new Text(identificationGroup, SWT.BORDER);
		functionText.setLayoutData(functionTextData);
		functionText.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(final ModifyEvent e) {
				if(!blockListeners){
					if (type != null) {
						blockListeners = true;
						getCommandStack().execute(new ChangeFunctionCommand(type, functionText.getText()));
						blockListeners = false;
					}	
				}
			}

		});

		l = new Label(identificationGroup, SWT.None);
		l.setText("Type: ");

		GridData typeTextData = new GridData();
		typeTextData.horizontalAlignment = GridData.FILL;
		typeTextData.grabExcessHorizontalSpace = true;
		typeText = new Text(identificationGroup, SWT.BORDER);
		typeText.setLayoutData(typeTextData);
		typeText.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(final ModifyEvent e) {
				if(!blockListeners){
					if (type != null) {
						blockListeners = true;
						getCommandStack().execute(new ChangeIdentifcationTypeCommand(type, typeText.getText()));
						blockListeners = false;
					}
				}
			}

		});

		l = new Label(identificationGroup, SWT.None);
		l.setText("Description: ");

		GridData descriptionTextData = new GridData();
		descriptionTextData.horizontalAlignment = GridData.FILL;
		descriptionTextData.verticalAlignment = GridData.FILL;
		descriptionTextData.grabExcessHorizontalSpace = true;
		descriptionTextData.verticalSpan = 20;
		descriptionTextData.grabExcessVerticalSpace = true;
		descriptionText = new Text(identificationGroup, SWT.WRAP | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		descriptionText.setLayoutData(descriptionTextData);
		descriptionText.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(final ModifyEvent e) {
				if(!blockListeners){
					if (type != null) {
						blockListeners = true;
						getCommandStack().execute(new ChangeDescriptionCommand(type, descriptionText.getText()));
						blockListeners = false;
					}
				}
			}

		});

		blockListeners = true;
		updateIdentificationInfo();
		blockListeners = false;
	}

	private void updateIdentificationInfo() {
		if (type.getIdentification() != null) {
			standardText
					.setText(type.getIdentification().getStandard() != null ? type
							.getIdentification().getStandard() : "");
			classificationText
					.setText(type.getIdentification().getClassification() != null ? type
							.getIdentification().getClassification() : "");
			domainText
					.setText(type.getIdentification().getApplicationDomain() != null ? type
							.getIdentification().getApplicationDomain()
							: "");
			functionText
					.setText(type.getIdentification().getFunction() != null ? type
							.getIdentification().getFunction() : "");
			typeText.setText(type.getIdentification().getType() != null ? type
					.getIdentification().getType() : "");
			descriptionText
					.setText(type.getIdentification().getDescription() != null ? type
							.getIdentification().getDescription() : "");
		}
	}

	boolean blockListeners = false;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#dispose()
	 */
	@Override
	public void dispose() {
		if (type != null && type.eAdapters().contains(adapter)) {
			type.eAdapters().remove(adapter);
		}
		super.dispose();
	}
	
	
	private CommandStack commandStack = null;
	
	protected CommandStack getCommandStack(){
		if(null == commandStack){
			//if we don't have yet one create one
			commandStack = new CommandStack();
		}
		return commandStack;
	}
	

	protected void setCommandStack(CommandStack commandStack){
		this.commandStack = commandStack;
	}
}
