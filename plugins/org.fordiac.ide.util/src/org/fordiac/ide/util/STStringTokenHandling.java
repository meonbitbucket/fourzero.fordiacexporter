/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util;

import java.util.StringTokenizer;

public class STStringTokenHandling {

	public final static String stTokenDelimiters = new String(
			" &():.=[]+-*/><;\n\r\t\"\'!,");
	
	public static String replaceSTToken(String stString, final String oldToken, final String newToken){
		String retVal = new String();
		
		StringTokenizer t = new StringTokenizer(stString, STStringTokenHandling.stTokenDelimiters,
				true);
		while (t.hasMoreElements()) {
			String s = t.nextToken();
			if(s.equals(oldToken)){
				retVal += newToken;
			}else{
				retVal += s;
			}
		}
		return retVal;
	}

}
