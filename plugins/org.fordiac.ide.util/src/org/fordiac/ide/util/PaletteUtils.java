/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.PaletteToolbar;
import org.eclipse.gef.palette.ToolEntry;

/**
 * The Class PaletteUtils.
 */
public class PaletteUtils {

	/**
	 * Returns a GEF Palette group of default tools like "select" or "draw
	 * connection".
	 * 
	 * @param root the root
	 * 
	 * @return the tool group
	 */
	public static PaletteContainer getToolGroup(PaletteRoot root) {
		PaletteToolbar toolGroup;
		toolGroup = new PaletteToolbar("Tools");

		List<ToolEntry> tools = new ArrayList<ToolEntry>();
		AdvancedPanningSelectionToolEntry defaultEntry = new AdvancedPanningSelectionToolEntry(
				"Select and Pan", "Select an Pan");
		tools.add(defaultEntry);
		root.setDefaultEntry(defaultEntry);
		toolGroup.addAll(tools);
		return toolGroup;
	}

}
