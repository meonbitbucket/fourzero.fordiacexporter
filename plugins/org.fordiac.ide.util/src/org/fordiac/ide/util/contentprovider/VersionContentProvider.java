/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util.contentprovider;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.fordiac.ide.model.libraryElement.LibraryElement;

/**
 * The Class VersionContentProvider.
 */
public class VersionContentProvider implements IStructuredContentProvider {

	/** The fb type. */
	private LibraryElement fbType;

	/**
	 * Instantiates a new version content provider.
	 * 
	 * @param fbType the fb type
	 */
	public VersionContentProvider(final LibraryElement fbType) {
		this.fbType = fbType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang
	 * .Object)
	 */
	@Override
	public Object[] getElements(final Object inputElement) {
		if(null != fbType){
			return fbType.getVersionInfo().toArray();
		}
		return new Object[] {};
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
	 */
	@Override
	public void dispose() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface
	 * .viewers.Viewer, java.lang.Object, java.lang.Object)
	 */
	@Override
	public void inputChanged(final Viewer viewer, final Object oldInput,
			final Object newInput) {
		
	}

}
