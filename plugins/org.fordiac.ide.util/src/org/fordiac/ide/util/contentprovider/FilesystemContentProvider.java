/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util.contentprovider;

import java.io.File;
import java.io.FileFilter;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

/**
 * The Class FilesystemContentProvider.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class FilesystemContentProvider implements ITreeContentProvider {

	/** The file filter. */
	private final FileFilter fileFilter;

	/**
	 * Instantiates a new filesystem content provider.
	 * 
	 * @param fileFilter the file filter
	 */
	public FilesystemContentProvider(final FileFilter fileFilter) {
		this.fileFilter = fileFilter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.Object
	 * )
	 */
	public Object[] getChildren(final Object parentElement) {
		if (parentElement instanceof File && ((File) parentElement).isDirectory()) {
			return ((File) parentElement).listFiles(fileFilter);
		}
		return new Object[0];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.Object)
	 */
	public Object getParent(final Object element) {
		if (element instanceof File) {
			return ((File) element).getParent();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.Object
	 * )
	 */
	public boolean hasChildren(final Object element) {
		return (element instanceof File && ((File) element).isDirectory()
				&& ((File) element).listFiles() != null && ((File) element).listFiles().length > 0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang
	 * .Object)
	 */
	public Object[] getElements(final Object inputElement) {
		if (inputElement instanceof File && ((File) inputElement).isDirectory()) {
			return ((File) inputElement).listFiles(fileFilter);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
	 */
	public void dispose() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface
	 * .viewers.Viewer, java.lang.Object, java.lang.Object)
	 */
	public void inputChanged(final Viewer viewer, final Object oldInput,
			final Object newInput) {
	}

}
