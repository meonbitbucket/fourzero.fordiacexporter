/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

package org.fordiac.ide.util.labelprovider;

import java.io.File;
import java.util.Hashtable;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.fordiac.ide.util.imageprovider.ImageProvider;

/**
 * The Class FilesystemLabelProvider.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class FilesystemLabelProvider extends LabelProvider {

	/** The DIRECTORY. */
	public static String DIRECTORY = "*"; //$NON-NLS-1$

	/*
	 * contains a list of file endings and icon names; * is used for directories
	 * example: .fbt -> fb_16.gif * -> folder.gif DIRECTORY can be used for *
	 */
	/** The icons. */
	Hashtable<String, String> icons;

	/**
	 * Instantiates a new filesystem label provider.
	 * 
	 * @param iconMapping the icon mapping
	 */
	public FilesystemLabelProvider(final Hashtable<String, String> iconMapping) {
		this.icons = iconMapping;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
	 */
	@Override
	public String getText(final Object element) {
		if (element instanceof File) {
			File file = (File) element;
			return file.getName();
		}
		return super.getText(element);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.LabelProvider#getImage(java.lang.Object)
	 */
	@Override
	public Image getImage(final Object element) {
		if (element instanceof File) {
			File file = (File) element;
			if (file.isDirectory()) {
				if (icons.containsKey(DIRECTORY)) {
					return ImageProvider.getImage(icons.get(DIRECTORY));
				}
			} else {
				int lastIndex = file.getName().lastIndexOf("."); //$NON-NLS-1$
				String ending = file.getName().substring(lastIndex);
				if (icons.containsKey(ending)) {
					return ImageProvider.getImage(icons.get(ending));
				}
			}
		}
		return super.getImage(element);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.BaseLabelProvider#dispose()
	 */
	@Override
	public void dispose() {

		super.dispose();
	}

}
