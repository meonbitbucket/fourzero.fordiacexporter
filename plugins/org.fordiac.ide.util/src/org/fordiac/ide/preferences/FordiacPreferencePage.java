/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.preferences;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ColorFieldEditor;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.fordiac.ide.ui.controls.jface.AdvancedBooleanFieldEditor;
import org.fordiac.ide.util.Activator;

/**
 * The Class FordiacPreferencePage.
 */
public class FordiacPreferencePage extends FieldEditorPreferencePage implements
		IWorkbenchPreferencePage {

	private Composite colorComposite;

	AdvancedBooleanFieldEditor syncColorWithDev;

	ColorFieldEditor basicFBColor;
	ColorFieldEditor siFBColor;
	ColorFieldEditor compFBColor;

	private Button btn;

	/**
	 * Instantiates a new fordiac preference page.
	 */
	public FordiacPreferencePage() {
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription(Messages.FordiacPreferencePage_LABEL_PreferencePageDescription);
	}

	/**
	 * Creates the field editors. Field editors are abstractions of the common
	 * GUI blocks needed to manipulate various types of preferences. Each field
	 * editor knows how to save and restore itself.
	 */
	@Override
	public void createFieldEditors() {

		addField(new BooleanFieldEditor(
				PreferenceConstants.P_VISIBILITY_OF_SUB_APP_INTERFACE_ELEMENTS,
				Messages.FordiacPreferencePage_LABEL_InterfaceEelementOfSubAppsVisibility,
				getFieldEditorParent()));
		addField(new ColorFieldEditor(
				PreferenceConstants.P_EVENT_CONNECTOR_COLOR,
				Messages.FordiacPreferencePage_LABEL_DefaultEventConnectorColor,
				getFieldEditorParent()));
		addField(new ColorFieldEditor(
				PreferenceConstants.P_DATA_CONNECTOR_COLOR,
				Messages.FordiacPreferencePage_LABEL_DefaultDataConnectorColor,
				getFieldEditorParent()));

		addField(syncColorWithDev = new AdvancedBooleanFieldEditor(
				PreferenceConstants.P_SYNC_FBColor_WITH_DEVICE_Color,
				Messages.FordiacPreferencePage_LABEL_SyncFBColorWithDeviceColor,
				getFieldEditorParent()));

		createFBColorFieldEditors();

		addField(new ComboFieldEditor(
				PreferenceConstants.P_DEFAULT_COMPLIANCE_PROFILE,
				Messages.FordiacPreferencePage_LABEL_DefaultComplianceProfile,
				getSupportedProfiles(), getFieldEditorParent()));

		registerListener();
	}

	private void registerListener() {
		btn = syncColorWithDev.getCheckbox(getFieldEditorParent());
		if (btn != null) {
			btn.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					setFBColorFieldsState();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					// not required
				}
			});
		}
	}

	private void setFBColorFieldsState() {
		basicFBColor.setEnabled(!btn.getSelection(), getColorComposite());
		siFBColor.setEnabled(!btn.getSelection(), getColorComposite());
		compFBColor.setEnabled(!btn.getSelection(), getColorComposite());

	}

	private void createFBColorFieldEditors() {
		addField(basicFBColor = new ColorFieldEditor(
				PreferenceConstants.P_BASIC_FB_BG_COLOR,
				Messages.FordiacPreferencePage_LABEL_DefaultBasicFBBackgroundColor,
				getColorComposite()));
		addField(siFBColor = new ColorFieldEditor(
				PreferenceConstants.P_SI_FB_BG_COLOR,
				Messages.FordiacPreferencePage_LABEL_DefaultSIFBBackgroundColor,
				getColorComposite()));
		addField(compFBColor = new ColorFieldEditor(
				PreferenceConstants.P_COMPOSITE_FB_BG_COLOR,
				Messages.FordiacPreferencePage_LABEL_DefaultCompositeFBBackgroundColor,
				getColorComposite()));
	}

	private Composite getColorComposite() {
		if (colorComposite == null) {
			colorComposite = new Composite(getFieldEditorParent(), SWT.NONE);
			GridData gd = new GridData();
			gd.horizontalSpan = 2;
			colorComposite.setLayoutData(gd);
		}
		colorComposite.setEnabled(false);
		return colorComposite;
	}

	@Override
	protected Composite getFieldEditorParent() {
		return super.getFieldEditorParent();
	}

	private String[][] getSupportedProfiles() {
		// FIXME return installed/supported profiles
		return new String[][] { { "HOLOBLOC", "HOLOBLOC" } }; //$NON-NLS-1$ //$NON-NLS-2$

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(final IWorkbench workbench) {
		// nothing to do
	}
	
	@Override
	protected void checkState() {
		super.checkState();
		setFBColorFieldsState();
	}

}