/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.preferences;

/**
 * Constant definitions for plug-in preferences.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class PreferenceConstants {

	/** The Constant P_EVENT_CONNECTOR_COLOR. */
	public static final String P_EVENT_CONNECTOR_COLOR = "EventConnectionConnectorColor"; //$NON-NLS-1$

	/** The Constant P_DATA_CONNECTOR_COLOR. */
	public static final String P_DATA_CONNECTOR_COLOR = "DataConnectionConnectorColor";//$NON-NLS-1$

	/** The Constant P_DATA_CONNECTOR_COLOR. */
	public static final String P_SYNC_FBColor_WITH_DEVICE_Color = "P_SYNC_FBColor_WITH_DEVICE_Color";//$NON-NLS-1$

	/** The Constant P_BASIC_FB_BG_COLOR. */
	public static final String P_BASIC_FB_BG_COLOR = "BaicFBBackgroundColorPreference";//$NON-NLS-1$

	/** The Constant P_SI_FB_BG_COLOR. */
	public static final String P_SI_FB_BG_COLOR = "SIFBBackgroundColorPreference";//$NON-NLS-1$

	/** The Constant P_COMPOSITE_FB_BG_COLOR. */
	public static final String P_COMPOSITE_FB_BG_COLOR = "CompositeFBBorderColorPreference";//$NON-NLS-1$

	/** The Constant P_VISIBILITY_OF_SUB_APP_INTERFACE_ELEMENTS. */
	public static final String P_VISIBILITY_OF_SUB_APP_INTERFACE_ELEMENTS = "P_VISIBILITY_OF_SUB_APP_INTERFACE_ELEMENTS";//$NON-NLS-1$

	/** The Constant P_DEFAULT_COMPLIANCE_PROFILE. */
	public static final String P_DEFAULT_COMPLIANCE_PROFILE = "P_DEFAULT_COMPLIANCE_PROFILE";//$NON-NLS-1$
	
	/** The Constant P_HIDE_EVENT_CON. */
	public static final String P_HIDE_EVENT_CON = "hideEventConnections";//$NON-NLS-1$
	
	/** The Constant P_HIDE_DATA_CON. */
	public static final String P_HIDE_DATA_CON = "hideDataConnections";//$NON-NLS-1$

}
