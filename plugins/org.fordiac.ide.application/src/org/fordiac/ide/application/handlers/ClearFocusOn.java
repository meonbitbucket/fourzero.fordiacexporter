/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

package org.fordiac.ide.application.handlers;

import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.fordiac.ide.application.editors.FBNetworkEditor;
import org.fordiac.ide.application.editparts.ConnectionEditPart;
import org.fordiac.ide.gef.editparts.AbstractViewEditPart;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.FBView;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class ClearFocusOn extends AbstractHandler {
	/**
	 * The constructor.
	 */
	public ClearFocusOn() {
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchPart part = HandlerUtil.getActiveEditor(event);
		if (part instanceof FBNetworkEditor) {
			GraphicalViewer viewer = ((FBNetworkEditor) part).getViewer();
			Map<?,?> map = viewer.getEditPartRegistry();
			for (Object obj : map.keySet()) {
				if (obj instanceof FBView) {
					Object editPartAsObject = map.get(obj);
					if (editPartAsObject instanceof AbstractViewEditPart) {
						((AbstractViewEditPart) editPartAsObject)
								.setTransparency(255);
					}
				} else if (obj instanceof ConnectionView) {
					Object editPartAsObject = map.get(obj);
					if (editPartAsObject instanceof ConnectionEditPart) {
						((ConnectionEditPart) editPartAsObject)
								.setTransparency(255);
					}
				}
			}
		}
		return null;
	}
}
