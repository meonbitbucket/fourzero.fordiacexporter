/*******************************************************************************
 * Copyright (c) 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.utilities;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.gef.commands.CommandStack;
import org.fordiac.ide.gef.DiagramManager;
import org.fordiac.ide.model.libraryElement.AdapterConnection;
import org.fordiac.ide.model.libraryElement.Connection;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.libraryElement.SubAppInterfaceList;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.Position;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.model.ui.UISubAppNetwork;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.ide.model.ui.View;

public class ApplicationUIFBNetworkManager extends DiagramManager{
	
	//TODO consider to remove all command stack requirements for the uifbnetworkmanager
	private CommandStack commandStack;
	
	
	public void setUIFBNetwork(UIFBNetwork uiFBNetwork) {
		setDiagram(uiFBNetwork);
		adaptFBNetwork(uiFBNetwork.getFbNetwork());
	}	

	public UIFBNetwork getUiFBNetwork() {
		return (UIFBNetwork)getDiagram();
	} 
	
	@Override
	protected Diagram createDiagram() {
		//Applications do not need to create a diagram they get it set from the editor
		return null;
	}
	
	
	@Override
	protected FBNetworkAdapter createFBNetworkAdapter(SubAppNetwork fbNetwork) {
		return new FBNetworkAdapter(fbNetwork){
			@Override
			public void notifyChanged(Notification notification) {
				Object feature = notification.getFeature();
				
				if(LibraryElementPackage.eINSTANCE.getSubAppNetwork_SubApps().equals(feature)){
					handleSubAppChange(notification);
				} else if(LibraryElementPackage.eINSTANCE.getSubApp_X().equals(feature)){
					Object obj = notification.getNewValue();
					SubAppView subAppView = getSubAppView((SubApp)notification.getNotifier());
					if(null != subAppView){
						subAppView.getPosition().setX(Integer.parseInt((String)obj));
					}
				} else if(LibraryElementPackage.eINSTANCE.getSubApp_Y().equals(feature)){
					Object obj = notification.getNewValue();
					SubAppView subAppView = getSubAppView((SubApp)notification.getNotifier());
					if(null != subAppView){
						subAppView.getPosition().setY(Integer.parseInt((String)obj));
					}
				} else {			
					super.notifyChanged(notification);
				}
			}

			private void handleSubAppChange(Notification notification) {
				switch (notification.getEventType()) {
				case Notification.ADD: 
					SubApp subApp = (SubApp)notification.getNewValue();
					if(null != subApp){
						SubAppView subAppView = getSubAppView(subApp);
						if(null == subAppView){
							//only add if it is not already in the list
							addChild(createSubAppView(subApp));
						}
					}
					break;
					
				case Notification.REMOVE:
					SubApp removedSubApp = (SubApp)notification.getOldValue();
					if(null != removedSubApp){
						SubAppView subAppView = getSubAppView(removedSubApp);
						if(null != subAppView){
							removeChild(subAppView);
						}					
					}
					break;
					
				default:
					break;
				}	
			}

		};
	}

	protected void configureFromFBNetwork(FBNetwork fbNetwork) {		
		setFBNetwork(fbNetwork); 
		
		addFBs(fbNetwork);
		
		addEventConnections(fbNetwork.getEventConnections());
		addDataConnections(fbNetwork.getDataConnections());
		addAdapterConnections(fbNetwork.getAdapterConnections());
	}

	private void setFBNetwork(FBNetwork fbNetwork) {
		getUiFBNetwork().setFbNetwork(fbNetwork);		
		adaptFBNetwork(fbNetwork);
	}
	

	private void addFBs(FBNetwork fbNetwork) {
		for (FB fb : fbNetwork.getFBs()) {
			addChild(createFBView(fb, fbNetwork));
		}
		
	}
	
	private void addEventConnections(EList<EventConnection> eventConnections) {
		for (EventConnection eventConnection : eventConnections) {
			addConnection(eventConnection, eventConnection.getSource(), eventConnection.getDestination());
		}
		
	}
	
	private void addDataConnections(EList<DataConnection> dataConnections) {
		for (DataConnection connection : dataConnections) {
			addConnection(connection, connection.getSource(), connection.getDestination());
		}
		
	}
	
	private void addAdapterConnections(EList<Connection> adapterConnections) {
		for (Connection connection : adapterConnections) {
			AdapterConnection adapterConnection = (AdapterConnection)connection;
			addConnection(connection, adapterConnection.getSource(), adapterConnection.getDestination());
		}
	}
	
	private void addConnection(Connection connection, IInterfaceElement source, IInterfaceElement destination) {
		if((null != source) && (null != destination)){
			ConnectionView connectionView = createConnectionView(connection, source, destination);
			if(null != connectionView){
				getDiagram().getConnections().add(connectionView);
			}
		}
	}

	private ConnectionView createConnectionView(Connection connection,
			IInterfaceElement source, IInterfaceElement destination) {
		
		InterfaceElementView sourceInterfaceElement = getInterfaceElement(source);
		InterfaceElementView destInterfaceElement = getInterfaceElement(destination);
		
		if((null != sourceInterfaceElement) && (null != destInterfaceElement)){
			ConnectionView connectionView = UiFactory.eINSTANCE
					.createConnectionView();
			connectionView.setConnectionElement(connection);
			connectionView.setSource(sourceInterfaceElement);
			connectionView.setDestination(destInterfaceElement);
			return connectionView;
		}		
		
		return null;
	}

	protected InterfaceElementView getInterfaceElement(
			IInterfaceElement interfaceElement) {
		
		FB fb = (FB)interfaceElement.eContainer().eContainer();
		FBView fbView = getFBView(fb);
		
		if(null != fbView){
			for (InterfaceElementView viewInterFaceElement : fbView.getInterfaceElements()) {
				if(viewInterFaceElement.getIInterfaceElement().equals(interfaceElement)){
					return viewInterFaceElement;
				}
			}
		}
		
		return null;
	}
	
	protected SubAppView getSubAppView(SubApp subApp) {
		for (View view : getChildren()) {
			if(view instanceof SubAppView){
				SubAppView subAppView = (SubAppView)view;
				if(subAppView.getSubApp().equals(subApp)){
					return subAppView;
				}
			}
		}
		return null;
	}
	
	protected SubAppView createSubAppView(SubApp subApp){
		SubAppView subAppView = UiFactory.eINSTANCE.createSubAppView();
		
		UISubAppNetwork uiSubAppNetwork = UiFactory.eINSTANCE.createUISubAppNetwork();
		subAppView.setUiSubAppNetwork(uiSubAppNetwork);
		
		subAppView.setSubApp(subApp);
		
		Position pos = UiFactory.eINSTANCE.createPosition();
		pos.setX(Integer.parseInt(subApp.getX()));
		pos.setY(Integer.parseInt(subApp.getY()));
		
		subAppView.setPosition(pos);
		
		uiSubAppNetwork.setSubAppNetwork(subApp.getSubAppNetwork());
		
		String fileName = subApp.getId() + ".xml"; //$NON-NLS-1$
		uiSubAppNetwork.setFileName(fileName);
		
		if (getDiagram() instanceof UIFBNetwork) {
			uiSubAppNetwork.setRootApplication((UIFBNetwork) getDiagram());
		} else if (getDiagram() instanceof UISubAppNetwork) {
			uiSubAppNetwork.setRootApplication(((UISubAppNetwork) getDiagram()).getRootApplication());
		}		
		
		configureSubAppInterfaceElements(subAppView.getInterfaceElements(), subApp.getInterface());
		
		return subAppView;
	}

	private void configureSubAppInterfaceElements(
			EList<SubAppInterfaceElementView> interfaceElements,
			SubAppInterfaceList interfaceList) {
		
		if (interfaceList != null) {
			for (Event eventInput: interfaceList.getEventInputs()) {
				interfaceElements.add(createSubAppInterfaceElementView(eventInput));
			}
			for (VarDeclaration varInput: interfaceList.getInputVars()) {
				interfaceElements.add(createSubAppInterfaceElementView(varInput));
			}
			for (Event eventOutput : interfaceList.getEventOutputs()) {
				interfaceElements.add(createSubAppInterfaceElementView(eventOutput));
			}
			for (VarDeclaration varOutput: interfaceList.getOutputVars()) {
				interfaceElements.add(createSubAppInterfaceElementView(varOutput));
			}
		}
		
	}
	
	private SubAppInterfaceElementView createSubAppInterfaceElementView(IInterfaceElement interfaceElement){
		SubAppInterfaceElementView subAppInterfaceElementView = UiFactory.eINSTANCE.createSubAppInterfaceElementView();
		subAppInterfaceElementView.setIInterfaceElement(interfaceElement);
		subAppInterfaceElementView.setVisible(true);
		return subAppInterfaceElementView;
	}
		

	public CommandStack getCommandStack() {
		return commandStack;
	}

	public void setCommandStack(CommandStack commandStack) {
		this.commandStack = commandStack;
	}
	
}
