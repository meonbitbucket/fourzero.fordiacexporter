/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.utilities;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.Request;
import org.eclipse.gef.dnd.AbstractTransferDropTargetListener;
import org.eclipse.gef.dnd.TemplateTransfer;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gef.requests.CreationFactory;
import org.eclipse.swt.dnd.DND;
import org.fordiac.ide.application.commands.FBCreateCommand;
import org.fordiac.ide.model.Palette.FBTypePaletteEntry;
import org.fordiac.ide.model.Palette.SubApplicationTypePaletteEntry;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.util.dnd.FbTypeTransfer;
import org.fordiac.ide.util.dnd.TransferDataSelectionOfFb;

/**
 * Performs a native Drop using the {@link TemplateTransfer}. The Drop is
 * performed by using a {@link CreateRequest} to obtain a <code>Command</code>
 * from the targeted <code>EditPart</code>.
 * <P>
 * This class is <code>abstract</code>. Subclasses are responsible for
 * providing the appropriate <code>Factory</code> object based on the template
 * that is being dragged.
 * 
 * @since 2.1
 * @author Eric Bordeau
 */
public abstract class FbTypeTemplateTransferDropTargetListener extends
		AbstractTransferDropTargetListener {

	
	AutomationSystem system;
	
	/**
	 * Constructs a listener on the specified viewer.
	 * 
	 * @param viewer the EditPartViewer
	 */
	public FbTypeTemplateTransferDropTargetListener(final EditPartViewer viewer, AutomationSystem system) {
		super(viewer, FbTypeTransfer.getInstance());
		this.system = system;
	}

	/**
	 * @see org.eclipse.gef.dnd.AbstractTransferDropTargetListener#createTargetRequest()
	 */
	@Override
	protected Request createTargetRequest() {
		// Look at the data on templatetransfer.
		// Create factory
		CreateRequest request = new CreateRequest();
		request.setFactory(getFactory(FbTypeTransfer.getInstance().getTemplate()));
		return request;
	}

	/**
	 * A helper method that casts the target Request to a CreateRequest.
	 * 
	 * @return CreateRequest
	 */
	protected final CreateRequest getCreateRequest() {
		return ((CreateRequest) getTargetRequest());
	}

	/**
	 * Returns the appropriate Factory object to be used for the specified
	 * template. This Factory is used on the CreateRequest that is sent to the
	 * target EditPart.
	 * 
	 * @param template
	 *            the template Object
	 * @return a Factory
	 */
	protected abstract CreationFactory getFactory(Object template);

	/**
	 * The purpose of a template is to be copied. Therefore, the drop operation
	 * can't be anything but <code>DND.DROP_COPY</code>.
	 * 
	 * @see AbstractTransferDropTargetListener#handleDragOperationChanged()
	 */
	@Override
	protected void handleDragOperationChanged() {
		getCurrentEvent().detail = DND.DROP_COPY;
		super.handleDragOperationChanged();
	}

	/**
	 * The purpose of a template is to be copied. Therefore, the Drop operation
	 * is set to <code>DND.DROP_COPY</code> by default.
	 * 
	 * @see org.eclipse.gef.dnd.AbstractTransferDropTargetListener#handleDragOver()
	 */
	@Override
	protected void handleDragOver() {
		super.handleDragOver();
		getCurrentEvent().feedback = DND.FEEDBACK_SCROLL | DND.FEEDBACK_EXPAND;
		if (FbTypeTransfer.getInstance().getTemplate() == null) {
			getCurrentEvent().detail = DND.DROP_NONE;
			getCurrentEvent().operations = DND.DROP_NONE;
	
			
		} else {
			if(FbTypeTransfer.getInstance().getTemplate() instanceof FBTypePaletteEntry){
				FBTypePaletteEntry entry = (FBTypePaletteEntry)FbTypeTransfer.getInstance().getTemplate(); 
				AutomationSystem paletteSystem = entry.getGroup().getPallete().getAutomationSystem();
				
				//If project is null it is an entry from the tool palette
				if((((null == paletteSystem) && null == system)) || 		
						((null != paletteSystem) && (null != system) &&
								(system.equals(paletteSystem)))){
					getCurrentEvent().detail = DND.DROP_COPY;
				}
				else{
					getCurrentEvent().detail = DND.DROP_NONE;
					getCurrentEvent().operations = DND.DROP_NONE;
				}
			}			
		}
	}
	

	
	/**
	 * Overridden to select the created object.
	 * 
	 * @see org.eclipse.gef.dnd.AbstractTransferDropTargetListener#handleDrop()
	 */
	@Override
	protected void handleDrop() {
		
		if(!(getCurrentEvent().data instanceof FBTypePaletteEntry) &&
				!(getCurrentEvent().data instanceof SubApplicationTypePaletteEntry) &&
				!(getCurrentEvent().data instanceof TransferDataSelectionOfFb[])){
			//only allow FB type drops and of TransferDataSelectionOfFb --> filter e.g. Folder Drops from Type Navigator
			return;
		}
//		
		super.handleDrop();
		selectAddedObject();
		
		Object[] cmds = getViewer().getEditDomain().getCommandStack().getCommands();
		
		if(cmds.length <= 0){
			return;
		}
		Object cmd = cmds[cmds.length - 1];
		if(cmd instanceof FBCreateCommand){
			FB fb = ((FBCreateCommand)cmd).getFB();
			FbTypeTransfer.getInstance().setCreatedObject(fb) ;
			FbTypeTransfer.getInstance().setTemplate(null);
		}
	}

	private void selectAddedObject() {
		Object model = getCreateRequest().getNewObject();
		if (model == null) {
			return;
		}
		EditPartViewer viewer = getViewer();
		viewer.getControl().forceFocus();
		Object editpart = viewer.getEditPartRegistry().get(model);
		if (editpart instanceof EditPart) {
			// Force a layout first.
			getViewer().flush();
			viewer.select((EditPart) editpart);
		}
	}

	/**
	 * Assumes that the target request is a {@link CreateRequest}.
	 */
	@Override
	protected void updateTargetRequest() {
		CreateRequest request = getCreateRequest();
		request.setLocation(getDropLocation());
	}

}
