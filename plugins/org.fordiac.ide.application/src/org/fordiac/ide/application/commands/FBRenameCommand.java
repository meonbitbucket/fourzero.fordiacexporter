/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

package org.fordiac.ide.application.commands;

import org.fordiac.ide.gef.commands.ViewRenameCommand;
import org.fordiac.ide.model.NameRepository;
import org.fordiac.ide.model.libraryElement.FB;

public class FBRenameCommand extends ViewRenameCommand {
	
	FB fb;
	
	public FBRenameCommand(FB fb, String newName){
		super();
		this.fb = fb;
		setNamedElement(fb);
        setName(newName);
	}

	@Override
	public void execute() {
		// remove the old name from the repository
		NameRepository.getInstance().removeInstanceName(fb);
		super.execute();
		// add the new one
		NameRepository.getInstance().addSystemUniqueFBInstanceName(fb);
	}

	@Override
	public void undo() {
		// remove the old name from the repository
		NameRepository.getInstance().removeInstanceName(fb);
		super.undo();
		// add the new one
		NameRepository.getInstance().addSystemUniqueFBInstanceName(fb);
	}

	@Override
	public void redo() {
		// remove the old name from the repository
		NameRepository.getInstance().removeInstanceName(fb);
		super.redo();
		// add the new one
		NameRepository.getInstance().addSystemUniqueFBInstanceName(fb);
	}
}

