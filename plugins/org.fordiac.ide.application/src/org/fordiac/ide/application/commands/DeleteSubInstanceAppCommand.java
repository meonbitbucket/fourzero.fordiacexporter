/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.commands;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.util.commands.DeleteConnectionCommand;

public class DeleteSubInstanceAppCommand extends Command {
	
	/** The view. */
	private SubAppView subAppView;
	
	private SubAppNetwork parentSubAppNetwork;
	
	/** The view parent. */
	private Diagram viewParent;
	
	/** The delete cmds. */
	ArrayList<DeleteConnectionCommand> deleteCmds = new ArrayList<DeleteConnectionCommand>();
	
	
	public DeleteSubInstanceAppCommand(final SubAppView subAppView){
		this.subAppView = subAppView;
		parentSubAppNetwork = (SubAppNetwork)subAppView.getSubApp().eContainer();
		viewParent = (Diagram)subAppView.eContainer();
		
	}
	
	@Override
	public void execute() {
		deleteConnections(subAppView);
		
		parentSubAppNetwork.getSubApps().remove(subAppView.getSubApp());
	}
	
	@Override
	public void redo() {
		reDeleteConnections();
		parentSubAppNetwork.getSubApps().remove(subAppView.getSubApp());		
	}
	
	@Override
	public void undo() {
		viewParent.getChildren().add(subAppView);
		parentSubAppNetwork.getSubApps().add(subAppView.getSubApp());
		restoreConnections();
	}
	
	private void deleteConnections(final SubAppView view) {
		for (SubAppInterfaceElementView element : view.getInterfaceElements()) {
			ArrayList<ConnectionView> temp = new ArrayList<ConnectionView>(
					element.getInConnections());
			for (Iterator<ConnectionView> iterator2 = temp.iterator(); iterator2
					.hasNext();) {
				ConnectionView connectionView = iterator2.next();
				DeleteConnectionCommand dcc = new DeleteConnectionCommand(
						connectionView);
				deleteCmds.add(dcc);
				dcc.execute();
			}
			temp = new ArrayList<ConnectionView>(element.getOutConnections());
			for (Iterator<ConnectionView> iterator2 = temp.iterator(); iterator2
					.hasNext();) {
				ConnectionView connectionView = iterator2.next();
				DeleteConnectionCommand dcc = new DeleteConnectionCommand(
						connectionView);
				deleteCmds.add(dcc);
				dcc.execute();
			}
		}
	}
	
	private void reDeleteConnections() {
		for (DeleteConnectionCommand cmd : deleteCmds) {
			cmd.redo();
		}
	}
	
	protected void restoreConnections() {
		for (DeleteConnectionCommand cmd : deleteCmds) {
			cmd.undo();
		}
	}
}
