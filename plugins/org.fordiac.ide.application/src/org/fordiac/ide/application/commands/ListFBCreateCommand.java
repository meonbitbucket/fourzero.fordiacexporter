/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.commands;

import java.util.Arrays;
import java.util.List;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gmf.runtime.common.ui.util.StatusLineUtil;
import org.eclipse.gmf.runtime.diagram.ui.menus.PopupMenu;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.fordiac.ide.model.Palette.FBTypePaletteEntry;
import org.fordiac.ide.model.Palette.PaletteEntry;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.Value;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.util.dnd.TransferDataSelectionFBParameter;
import org.fordiac.ide.util.dnd.TransferDataSelectionOfFb;
import org.fordiac.ide.util.imageprovider.ImageProvider;

public class ListFBCreateCommand extends FBCreateCommand {
	private FBTypePaletteEntry[] typeList;
	private TransferDataSelectionOfFb[] selectionList;
	private AutomationSystem system;

	/**
	 * Instantiates a new fB create command.
	 * 
	 * @param type
	 *            the type
	 * @param parent
	 *            the parent
	 * @param bounds
	 *            the bounds
	 */
	public ListFBCreateCommand(final FBTypePaletteEntry[] type,
			final Diagram parent, final Rectangle bounds) {
		super(null, parent, bounds); // values will be set in execute()
		typeList = type;
		selectionList = null;
	}

	public ListFBCreateCommand(final TransferDataSelectionOfFb[] fbList,
			final Diagram parent, final Rectangle bounds,
			AutomationSystem system) {
		super(null, parent, bounds); // values will be set in execute()
		typeList = null;
		selectionList = fbList;
		this.system = system;
	}

	@Override
	public boolean canExecute() {
		if (typeList != null) {
			return typeList.length != 0 && bounds != null && (parentDiagram != null);
		} else if (selectionList != null) {
			return selectionList.length != 0 && bounds != null
					&& (parentDiagram != null);
		}
		return false;
	}

	private void executeTransferData() {

		PopupMenu popup = new PopupMenu(Arrays.asList(selectionList),
				new LabelProvider() {

					@Override
					public String getText(Object element) {
						if (element instanceof TransferDataSelectionOfFb) {
							return ((TransferDataSelectionOfFb) element)
									.getSelectionLabel();
						}
						return element.toString();
					}

					@Override
					public Image getImage(Object element) {
						if (element instanceof TransferDataSelectionOfFb) {
							return ((TransferDataSelectionOfFb) element)
									.getSelectionImage();
						}
						return null;
					}
				});

		popup.show(Display.getCurrent().getActiveShell());

		Object res = popup.getResult();
		if (res instanceof TransferDataSelectionOfFb) {
			TransferDataSelectionOfFb element = ((TransferDataSelectionOfFb) res);
			// get PaletteEntry for fbTypeName
			List<PaletteEntry> fbTypes = system.getPalette().getTypeEntries(element.getFbTypeName());
			if (fbTypes.size() > 0
					&& fbTypes.get(0) instanceof FBTypePaletteEntry) {
				element.setTypePaletteEntry(((FBTypePaletteEntry) fbTypes
						.get(0)));
				this.paletteEntry = element.getTypePaletteEntry();
				super.execute();

				for (TransferDataSelectionFBParameter fbParametert : element
						.getFbParameters()) {
					IInterfaceElement fbInterfaceElement = fB
							.getInterfaceElement(fbParametert.getName());
					if (fbInterfaceElement != null) {
						Value val = fbInterfaceElement.getValue();
						val.setValue(fbParametert.getValue());
					}
				}
			} else {
				// warning/info in statusline that fbtype can not be found
				StatusLineUtil.outputErrorMessage(PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getActivePage().getActivePart(),
						"FBType not found!");
			}
		}
	}

	private void executeFBTypePalette() {
		PopupMenu popup = new PopupMenu(Arrays.asList(typeList),
				new LabelProvider() {

					@Override
					public String getText(Object element) {
						if (element instanceof FBTypePaletteEntry) {
							return ((FBTypePaletteEntry) element).getLabel();
						}
						return element.toString();
					}

					@Override
					public Image getImage(Object element) {
						if (element instanceof FBTypePaletteEntry) {
							return ImageProvider.getImage("fb_16.gif");
						}
						return null;
					}
				});

		popup.show(Display.getCurrent().getActiveShell());

		Object res = popup.getResult();
		if (res instanceof FBTypePaletteEntry) {
			this.paletteEntry = ((FBTypePaletteEntry) res);
			super.execute();
		}
	}

	@Override
	public void execute() {
		if (selectionList != null && system != null) {
			executeTransferData();
		}
		if (typeList != null) {
			executeFBTypePalette();
		}

	}

	@Override
	public boolean canUndo() {
		if (editor != null) {
			return super.canUndo();
		}
		return false;
	}
}
