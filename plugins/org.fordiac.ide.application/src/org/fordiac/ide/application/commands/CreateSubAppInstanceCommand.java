/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.commands;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.application.ApplicationPlugin;
import org.fordiac.ide.model.NameRepository;
import org.fordiac.ide.model.Palette.SubApplicationTypePaletteEntry;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.libraryElement.Value;

public class CreateSubAppInstanceCommand extends Command {

	
	protected SubApplicationTypePaletteEntry paletteEntry;
	
	private final SubAppNetwork subAppNetwork;
	
	/** The create position. */
	protected Rectangle bounds;
	
	/** The editor. */
	protected IEditorPart editor;
	
	private SubApp subApp;
	
	
	public CreateSubAppInstanceCommand(final SubApplicationTypePaletteEntry paletteEntry, final SubAppNetwork subAppNetwork,
			final Rectangle bounds) {
		this.paletteEntry = paletteEntry;
		this.subAppNetwork = subAppNetwork;
		this.bounds = bounds;
		setLabel("Create Subapplication Instance");
	}
	
	@Override
	public boolean canExecute() {
		return paletteEntry != null && bounds != null && (subAppNetwork != null);
	}
	
	@Override
	public boolean canUndo() {
		return (null != editor) && editor.equals(ApplicationPlugin.getDefault().getCurrentActiveEditor());
	}
	
	@Override
	public void execute() {
		editor = ApplicationPlugin.getDefault().getCurrentActiveEditor();
		setLabel(getLabel()
				+ "(" + (editor != null ? editor.getTitle() : "") + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		if(null != subAppNetwork){	
			subApp = LibraryElementFactory.eINSTANCE.createSubApp();
			SubAppNetwork newSubAppNetwork = LibraryElementFactory.eINSTANCE.createSubAppNetwork();
			subApp.setSubAppNetwork(newSubAppNetwork);
			newSubAppNetwork.setParentSubApp(subApp);
			subApp.setPaletteEntry(paletteEntry);
			subApp.setId(EcoreUtil.generateUUID());
			subApp.setInterface((InterfaceList) EcoreUtil.copy(paletteEntry.getSubApplicationType().getInterfaceList()));
			subApp.setIdentifier(true);
			String name = "";
			
			if (subAppNetwork != null) {
				name = NameRepository.getInstance().getNetworkUniqueSubApplicationName(subAppNetwork, paletteEntry.getLabel());
			} 
			
			subApp.setName(name);
			newSubAppNetwork.setName(subApp.getName());
			
			subApp.setX(Integer.toString(bounds.x)); 
			subApp.setY(Integer.toString(bounds.y));
			
			createValues();
			subAppNetwork.getSubApps().add(subApp);		
		}
	}
	
	@Override
	public void redo() {
		if ((subAppNetwork != null) && ( null != subApp)) {
			subAppNetwork.getSubApps().add(subApp);			
		}
	}
	
	@Override
	public void undo() {
		if (subAppNetwork != null) {
			subAppNetwork.getSubApps().remove(subApp);
		}
	}
	
	
	protected void createValues() {
		ArrayList<IInterfaceElement> iInterfaceElements = new ArrayList<IInterfaceElement>();

		// iInterfaceElements.addAll(fB.getInterface().getEventInputs());
		iInterfaceElements.addAll(subApp.getInterface().getInputVars());

		for (Iterator<IInterfaceElement> iterator = iInterfaceElements
				.iterator(); iterator.hasNext();) {
			IInterfaceElement element = iterator.next();
			Value value = LibraryElementFactory.eINSTANCE.createValue();
			element.setValue(value);

		}
	}
	
	
	
}
