/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.editors;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.ui.IMemento;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.ui.UISubAppNetwork;
import org.fordiac.ide.util.AbstractUntypedEditorInputFactory;
import org.fordiac.systemmanagement.SystemManager;

public class SubApplicationEditorInputFactory extends AbstractUntypedEditorInputFactory {
	
	/**
     * Factory id. The workbench plug-in registers a factory by this name
     * with the "org.eclipse.ui.elementFactories" extension point.
     */
    private static final String ID_FACTORY = "org.fordiac.ide.application.SubApplicationEditorInputFactory"; //$NON-NLS-1$
    
    /**
     * Tag for the application name.
     */
    private static final String TAG_APPLICATION = "APPLICATION"; //$NON-NLS-1$
    
    /**
     * Tag for the subapplication name.
     */
    private static final String TAG_SUB_APPLICATION = "SUB_APPLICATION"; //$NON-NLS-1$

	@Override
	public IAdaptable createElement(IMemento memento) {
    	String systemName = loadAutomationSystemName(memento);
    	String applicationName = memento.getString(TAG_APPLICATION);
    	String subApplicationName = memento.getString(TAG_SUB_APPLICATION);
    	if((null != systemName) && (null != applicationName)){
    		AutomationSystem system = SystemManager.getInstance().getSystemForName(systemName);
			if(null != system){
				Application application = system.getApplicationForName(applicationName);
				SubApp subApp = application.getSubAppNamned(subApplicationName);
				if(null != subApp){
					UISubAppNetwork uiSubAppNetwork = (UISubAppNetwork)subApp.getSubAppNetwork().eContainer();
					if(null != uiSubAppNetwork){
						return new SubApplicationEditorInput(uiSubAppNetwork);
					}
				}
			}    		
    	}
    	return null;
	}
	
	/**
     * Returns the element factory id for this class.
     * 
     * @return the element factory id
     */
    public static String getFactoryId() {
        return ID_FACTORY;
    }

    /**
     * Saves the state of the given file editor input into the given memento.
     *
     * @param memento the storage area for element state
     * @param input the application editor input
     */
    public static void saveState(IMemento memento, SubApplicationEditorInput input) {
    	Application app = input.getApplication();
    	saveAutomationSystem(memento, app.getAutomationSystem());
    	memento.putString(TAG_APPLICATION, app.getName());
    	memento.putString(TAG_SUB_APPLICATION, input.getName());
    }

}
