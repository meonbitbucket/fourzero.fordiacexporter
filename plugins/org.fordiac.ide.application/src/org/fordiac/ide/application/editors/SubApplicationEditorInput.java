/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.editors;

import org.eclipse.ui.IMemento;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UISubAppNetwork;
import org.fordiac.ide.util.PersistableUntypedEditorInput;

public class SubApplicationEditorInput extends PersistableUntypedEditorInput {

	public SubApplicationEditorInput(UISubAppNetwork uiSubAppNetwork) {
		super(uiSubAppNetwork, 
				uiSubAppNetwork.getSubAppNetwork().getParentSubApp().getName(),
				uiSubAppNetwork.getSubAppNetwork().getParentSubApp().getName());
	}
	
	@Override
	public void saveState(IMemento memento) {
		SubApplicationEditorInputFactory.saveState(memento, this);
		
	}

	@Override
	public String getFactoryId() {
		return SubApplicationEditorInputFactory.getFactoryId();
	}
	
	public UISubAppNetwork getUISubAppNetwork(){
		return (UISubAppNetwork) getContent();
	}
	
	public SubApp getSubApp(){
		return ((SubAppView)getUISubAppNetwork().eContainer()).getSubApp();
	}
	
	/*
	 * return the root application the sub app is contained in
	 */
	public Application getApplication(){		
		return getUISubAppNetwork().getRootApplication().getFbNetwork().getApplication();
	}

}
