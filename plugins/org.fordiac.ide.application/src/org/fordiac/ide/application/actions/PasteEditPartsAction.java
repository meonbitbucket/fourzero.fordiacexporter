/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.actions;

import java.util.Collections;
import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.ui.actions.Clipboard;
import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.actions.ActionFactory;
import org.fordiac.ide.application.commands.PasteCommand;
import org.fordiac.ide.application.editparts.FBNetworkEditPart;
import org.fordiac.ide.application.editparts.UISubAppNetworkEditPart;
import org.fordiac.ide.gef.editparts.IDiagramEditPart;

/**
 * The Class PasteEditPartsAction.
 */
public class PasteEditPartsAction extends SelectionAction {

	private static final String PASTE_TEXT = "Paste";

	/**
	 * Instantiates a new paste edit parts action.
	 * 
	 * @param editor the editor
	 */
	public PasteEditPartsAction(IWorkbenchPart editor) {
		super(editor);
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected boolean calculateEnabled() {
		List selection = getSelectedObjects();
		Object obj = null;
		if (selection != null && selection.size() == 1) {
			obj = selection.get(0);
		}

		return (((obj instanceof FBNetworkEditPart) || (obj instanceof UISubAppNetworkEditPart)) && createPasteCommand() != null);
	}

	@SuppressWarnings("rawtypes")
	protected Command createPasteCommand() {
		CompoundCommand cmd = new CompoundCommand();
		List selection = getSelectedObjects();
		if (selection != null && selection.size() == 1) {
			Object obj = selection.get(0);
			if ((obj instanceof FBNetworkEditPart) || (obj instanceof UISubAppNetworkEditPart)) {
				List templates = getClipboardContents();
				return new PasteCommand(templates, (IDiagramEditPart) obj);

			}
		}
		return cmd;
	}

	@SuppressWarnings("rawtypes")
	protected List getClipboardContents() {
		List list = Collections.EMPTY_LIST;
		Object obj = Clipboard.getDefault().getContents();

		if (obj instanceof List) {
			list = (List) obj;
		}

		return list;
	}

	protected Point getPasteLocation(GraphicalEditPart container, int i) {
		Point result = new Point(i * 10, i * 10);
		IFigure fig = container.getContentPane();
		result.translate(fig.getClientArea(Rectangle.SINGLETON).getLocation());
		fig.translateToAbsolute(result);
		return result;
	}

	@Override
	protected void init() {
		setId(ActionFactory.PASTE.getId());
		setText(PASTE_TEXT);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		execute(createPasteCommand());
	}

}
