/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.editparts;

import java.text.MessageFormat;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.editparts.ZoomManager;
import org.fordiac.ide.application.ApplicationPlugin;
import org.fordiac.ide.application.Messages;
import org.fordiac.ide.gef.DiagramEditor;
import org.fordiac.ide.gef.IEditPartCreator;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.model.ui.UISubAppNetwork;

/**
 * A factory for creating new EditParts.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class InstanceViewerEditPartFactory implements EditPartFactory {

	private Diagram diagram; 
	private DiagramEditor editor; 
	protected ZoomManager zoomManager;
	
	public DiagramEditor getEditor() {
		return editor;
	}

	public void setEditor(DiagramEditor editor) {
		this.editor = editor;
	}

	public Diagram getDiagram() {
		return diagram;
	}

	public void setDiagram(Diagram diagram) {
		this.diagram = diagram;
	}

	public InstanceViewerEditPartFactory(ZoomManager zoomManager) {
		this.zoomManager = zoomManager;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.EditPartFactory#createEditPart(org.eclipse.gef.EditPart,
	 * java.lang.Object)
	 */
	public EditPart createEditPart(final EditPart context,
			final Object modelElement) {
		// get EditPart for model element
		EditPart part = null;
		try {
			part = getPartForElement(context, modelElement);
			// store model element in EditPart
			part.setModel(modelElement);
		} catch (RuntimeException e) {
			ApplicationPlugin.getDefault().logError(
					Messages.ElementEditPartFactory_ERROR_CantCreatePartForModelElement,
					e);

		}
		return part;
	}

	/**
	 * Maps an object to an EditPart.
	 * 
	 * @throws RuntimeException
	 *           if no match was found (programming error)
	 */
	protected EditPart getPartForElement(final EditPart context,
			final Object modelElement) {
		if (modelElement instanceof UIFBNetwork) {
			return new FBNetworkEditPart();
		}
		if (modelElement instanceof FBView) {
			return new FBEditPart(zoomManager);
		}
		if (modelElement instanceof InterfaceElementView) {
			return new InterfaceEditPart();
		}
		if (modelElement instanceof ConnectionView) {
			return new ConnectionEditPart();
		}
		if (modelElement instanceof SubAppView) {
			return new SubAppForFBNetworkEditPart();
		}
		if (modelElement instanceof SubAppInterfaceElementView) {
			return new InterfaceEditPart();
		}
		if (modelElement instanceof UISubAppNetwork) {
			return new UISubAppNetworkEditPart();
		}

		if (modelElement instanceof IEditPartCreator) {
			return ((IEditPartCreator) modelElement).createEditPart(Integer.toString(editor.hashCode()));
		}

		throw new RuntimeException(MessageFormat.format(
				Messages.ElementEditPartFactory_ERROR_CantCreatePartForModelElement,
				new Object[] { ((modelElement != null) ? modelElement.getClass()
						.getName() : "null") })); //$NON-NLS-1$
	}
}
