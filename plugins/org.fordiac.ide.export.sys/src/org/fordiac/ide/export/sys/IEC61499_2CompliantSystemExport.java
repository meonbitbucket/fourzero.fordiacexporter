/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.export.sys;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.eclipse.emf.common.util.EList;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.Connection;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.libraryElement.ResourceTypeFB;
import org.fordiac.ide.model.libraryElement.SystemConfiguration;
import org.fordiac.ide.model.libraryElement.SystemConfigurationNetwork;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.DeviceView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.IConnectionView;
import org.fordiac.ide.model.ui.LinkView;
import org.fordiac.ide.model.ui.MappedSubAppView;
import org.fordiac.ide.model.ui.Position;
import org.fordiac.ide.model.ui.ResourceView;
import org.fordiac.ide.model.ui.SegmentView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.model.ui.UISystemConfiguration;
import org.fordiac.ide.model.ui.View;
import org.fordiac.ide.typeexport.CommonElementExporter;
import org.fordiac.ide.util.Utils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * The Class IEC61499_2CompliantSystemExport.
 * 
 * @author gebenh
 */
public class IEC61499_2CompliantSystemExport implements ISystemExport {

	private ArrayList<String> warnings;
	private ArrayList<String> errors;
	private ArrayList<String> infos;

	/**
	 * Constructor. Should only be called by the extension point!
	 */
	public IEC61499_2CompliantSystemExport() {
		initializeFields();
	}

	private void initializeFields() {
		infos = new ArrayList<String>();
		errors = new ArrayList<String>();
		warnings = new ArrayList<String>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.fordiac.ide.export.sys.ISystemExport#export(org.fordiac.ide.model.
	 * libraryElement.AutomationSystem, java.lang.String, boolean)
	 */
	@Override
	public void export(AutomationSystem system, String destinationPath,
			boolean overwriteWithoutWarning) {

		if (system == null) {
			errors.add("System must not be null!");
			return;
		}
		if (destinationPath == null || destinationPath.equals("")) {
			errors.add("Destination must not be empty!");
			return;
		}

		saveAsSYSFile(system, destinationPath, overwriteWithoutWarning);

	}

	/**
	 * Gets the document.
	 * 
	 * @param fb
	 *            the fb
	 * 
	 * @return the document
	 */
	private Document getDocument(final AutomationSystem system) {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;

		try {
			db = dbf.newDocumentBuilder();
			Document dom = db.newDocument();

			addSystem(dom, system);
			return dom;
		} catch (ParserConfigurationException e) {
			Activator.getDefault().logError(e.getMessage(), e);
			return null;
		}

	}

	/*
	 * <!-- System elements --> <!ELEMENT System (Identification?, VersionInfo+,
	 * Application, Device+, Mapping, Segment, Link)> <!ATTLIST System Name
	 * CDATA #REQUIRED Comment CDATA #IMPLIED > <!ELEMENT Application
	 * (FBNetwork)> <!ATTLIST Application Name CDATA #REQUIRED Comment CDATA
	 * #IMPLIED > <!ELEMENT Mapping EMPTY> <!ATTLIST Mapping From CDATA
	 * #REQUIRED To CDATA #REQUIRED > <!ELEMENT Device
	 * (Parameter,Resource,FBNetwork?)> <!ATTLIST Device Name CDATA #REQUIRED
	 * Type CDATA #REQUIRED Comment CDATA #IMPLIED x CDATA #IMPLIED y CDATA
	 * #IMPLIED > /
	 * 
	 * / Adds the fb type.
	 * 
	 * @param dom the dom
	 * 
	 * @param fb the fb
	 */
	private void addSystem(final Document dom, final AutomationSystem system) {
		Element rootEle = dom.createElement("System");
		rootEle.setAttribute("Name", system.getName());
		if (system.getComment() != null && !system.getComment().equals("")) {
			rootEle.setAttribute("Comment", system.getComment());
		}

		dom.appendChild(rootEle);

		CommonElementExporter.addIdentification(dom, rootEle, system);
		CommonElementExporter.addVersionInfo(dom, rootEle, system);

		addApplication(dom, rootEle, system);
		addDevice(dom, rootEle, system);
		addMapping(dom, rootEle, system);
		addSegment(dom, rootEle, system);
		addLink(dom, rootEle, system);

	}

	private static void addLink(Document dom, Element rootEle,
			AutomationSystem system) {

		SystemConfiguration systemConfiguration = system
				.getSystemConfiguration();
		if (systemConfiguration == null) {
			return;
		}
		SystemConfigurationNetwork systemNetwork = systemConfiguration
				.getSystemConfigurationNetwork();
		if (systemNetwork == null) {
			return;
		}
		UISystemConfiguration uiSystemConf = (UISystemConfiguration) systemNetwork
				.eContainer();
		for (Iterator<IConnectionView> iterator = uiSystemConf.getConnections()
				.iterator(); iterator.hasNext();) {
			IConnectionView connection = (IConnectionView) iterator.next();
			if (connection instanceof LinkView) {
				LinkView linkView = (LinkView) connection;
				Element linkElement = dom.createElement("Link");
				linkElement.setAttribute("SegmentName", linkView.getSource()
						.getSegment().getName());
				linkElement.setAttribute("CommResource", linkView
						.getDestination().getDeviceElement().getName());
				linkElement.setAttribute("Comment", linkView.getLink()
						.getComment());
				rootEle.appendChild(linkElement);

			}
		}
	}

	private static void addSegment(Document dom, Element rootEle,
			AutomationSystem system) {
		SystemConfiguration systemConfiguration = system
				.getSystemConfiguration();
		if (systemConfiguration == null) {
			return;
		}
		SystemConfigurationNetwork systemNetwork = systemConfiguration
				.getSystemConfigurationNetwork();
		if (systemNetwork == null) {
			return;
		}
		UISystemConfiguration uiSystemConf = (UISystemConfiguration) systemNetwork
				.eContainer();

		for (Iterator<View> iterator = uiSystemConf.getChildren().iterator(); iterator
				.hasNext();) {
			View view = (View) iterator.next();
			if (view instanceof SegmentView) {
				SegmentView segmentView = (SegmentView) view;

				Element segmentElement = dom.createElement("Segment");
				if (segmentView.getSegment().getName() != null) {
					segmentElement.setAttribute("Name", segmentView
							.getSegment().getName());
				} else {
					segmentElement.setAttribute("Name", "");
				}
				if (segmentView.getSegment().getSegmentType() != null) {
					segmentElement.setAttribute("Type", segmentView
							.getSegment().getName());
				} else {
					segmentElement.setAttribute("Type", "");
				}
				if (segmentView.getSegment().getComment() != null) {
					segmentElement.setAttribute("Comment", segmentView
							.getSegment().getComment());
				}
				if (segmentView.getPosition() != null) {
					Position uiPosition = null;
					uiPosition = segmentView.getPosition();
					if (uiPosition != null) {

						segmentElement.setAttribute("x", CommonElementExporter
								.reConvertCoordinate(uiPosition.getX())
								.toString());
						segmentElement.setAttribute("y", CommonElementExporter
								.reConvertCoordinate(uiPosition.getY())
								.toString());
					} else {
						segmentElement.setAttribute("x", segmentView
								.getSegment().getX());
						segmentElement.setAttribute("y", segmentView
								.getSegment().getY());

					}

				}
				if (segmentView.getSize() != null) {
					segmentElement.setAttribute(
							"dx1",
							CommonElementExporter.reConvertCoordinate(
									segmentView.getSize().getWidth())
									.toString());
				}

				rootEle.appendChild(segmentElement);
			}
		}
	}

	private static void addMapping(Document dom, Element rootEle,
			AutomationSystem system) {

		for (Iterator<Application> iterator = system.getApplication()
				.iterator(); iterator.hasNext();) {
			Application app = (Application) iterator.next();
			Diagram diagram = (Diagram) app.getFBNetwork().eContainer();
			for (Iterator<View> iterator2 = diagram.getChildren().iterator(); iterator2
					.hasNext();) {
				View view = (View) iterator2.next();
				if (view instanceof FBView) {
					FBView fbView = (FBView) view;
					if (fbView.getMappedFB() != null) {
						FBView mappedFB = fbView.getMappedFB();
						Element mappingElement = dom.createElement("Mapping");
						mappingElement.setAttribute("From", app.getName() + "."
								+ fbView.getFb().getName());

						UIResourceEditor uiResourceEditor = (UIResourceEditor) mappedFB
								.eContainer();
						Resource resource = uiResourceEditor
								.getResourceElement();
						Device dev = resource.getDevice();

						mappingElement.setAttribute("To", dev.getName() + "."
								+ resource.getName() + "."
								+ mappedFB.getFb().getName());
						rootEle.appendChild(mappingElement);
					}
				}
			}
		}

	}

	private void addDevice(Document dom, Element rootEle,
			AutomationSystem system) {
		SystemConfiguration systemConfiguration = system
				.getSystemConfiguration();
		if (systemConfiguration == null) {
			return;
		}
		SystemConfigurationNetwork systemNetwork = systemConfiguration
				.getSystemConfigurationNetwork();
		if (systemNetwork == null) {
			return;
		}
		UISystemConfiguration uiSystemConf = (UISystemConfiguration) systemNetwork
				.eContainer();
		if (uiSystemConf != null) {
			for (Iterator<View> iterator = uiSystemConf.getChildren()
					.iterator(); iterator.hasNext();) {
				View view = (View) iterator.next();
				if (view instanceof DeviceView) {
					DeviceView devView = (DeviceView) view;
					addDevice(dom, rootEle, devView);
				}
			}
		}
	}

	private void addApplication(Document dom, Element rootEle,
			AutomationSystem system) {

		for (Iterator<Application> iterator = system.getApplication()
				.iterator(); iterator.hasNext();) {
			Application app = (Application) iterator.next();

			Element appElement = dom.createElement("Application");
			appElement.setAttribute("Name", app.getName());
			if (app.getComment() != null && !app.getComment().equals("")) { 
				appElement.setAttribute("Comment", app.getComment());
			}
			addFBNetwork(dom, appElement, (Diagram) app.getFBNetwork()
					.eContainer(), "SubAppNetwork");

			rootEle.appendChild(appElement);

		}

	}

	/**
	 * Adds the f bs.
	 * 
	 * @param dom
	 *            the dom
	 * @param fbNetwork
	 *            the fb network
	 * @param network
	 *            the network
	 */
	public static void addFBs(final Document dom, final Element fbNetwork,
			final Diagram network) {

		for (Iterator<View> iter = network.getChildren().iterator(); iter
				.hasNext();) {
			View fbview = iter.next();

			if (fbview instanceof SubAppView) {
				SubAppView subAppView = (SubAppView) fbview;
				addFBs(dom, fbNetwork, subAppView.getUiSubAppNetwork());
			}
			if (fbview instanceof MappedSubAppView) {
				MappedSubAppView subAppView = (MappedSubAppView) fbview;
				addFBs(dom, fbNetwork, subAppView.getApplicationSubApp()
						.getUiSubAppNetwork());
			}

			if (fbview instanceof FBView
					&& !(((FBView) fbview).getFb() instanceof ResourceTypeFB)) {
				FB fb = ((FBView) fbview).getFb();

				Element fbElement = dom.createElement("FB");
				if (fb.getName() != null) {
					fbElement.setAttribute("Name", fb.getName());
				} else {
					fbElement.setAttribute("Name", "");
				}
				if (fb.getPaletteEntry() != null) {
					fbElement.setAttribute("Type", fb.getPaletteEntry().getLabel());
				} else {
					fbElement.setAttribute("Type", "");
				}
				if (fb.getComment() != null) {
					fbElement.setAttribute("Comment", fb.getComment());
				}
				if (fbview.getPosition() != null) {
					Position uiPosition = null;
					uiPosition = ((FBView) fbview).getPosition();
					if (uiPosition != null) {

						fbElement.setAttribute("x", CommonElementExporter
								.reConvertCoordinate(uiPosition.getX())
								.toString());
						fbElement.setAttribute("y", CommonElementExporter
								.reConvertCoordinate(uiPosition.getY())
								.toString());
					} else if (fb.getPosition() != null) {
						fbElement.setAttribute("x", CommonElementExporter
								.reConvertCoordinate(fb.getPosition().getX())
								.toString());
						fbElement.setAttribute("y", CommonElementExporter
								.reConvertCoordinate(fb.getPosition().getY())
								.toString());

					}

				}

				for (Iterator<VarDeclaration> iterator = fb.getInterface()
						.getInputVars().iterator(); iterator.hasNext();) {
					VarDeclaration var = (VarDeclaration) iterator.next();
					if (var.getValue() != null
							&& var.getValue().getValue() != null
							&& !var.getValue().getValue().equals("")) {
						Element parameterElement = dom
								.createElement("Parameter");
						parameterElement.setAttribute("Name", var.getName());
						parameterElement.setAttribute("Value", var.getValue()
								.getValue());
						fbElement.appendChild(parameterElement);
					}
				}

				fbNetwork.appendChild(fbElement);
			}
		}
	}

	/**
	 * Adds the fb network.
	 * 
	 * @param dom
	 *            the dom
	 * @param elementName
	 *            TODO
	 * @param rootEle
	 *            the root ele
	 * @param type
	 *            the type
	 */
	private void addFBNetwork(final Document dom, final Element appElement,
			final Diagram fbNetworkObject, String elementName) {
		Element fbNetwork = dom.createElement(elementName);

		addFBs(dom, fbNetwork, fbNetworkObject);
		ArrayList<Connection> connections = new ArrayList<Connection>();
		for (Iterator<IConnectionView> iterator = fbNetworkObject
				.getConnections().iterator(); iterator.hasNext();) {
			ConnectionView connectionView = (ConnectionView) iterator.next();
			if (!connectionView.getConnectionElement().isResTypeConnection()
					&& !connections.contains(connectionView
							.getConnectionElement())) {
				checkAgainstForteTypeCast(connections, connectionView);

			}
		}

		addSubAppConnections(fbNetworkObject, connections);

		CommonElementExporter.addConnections(dom, fbNetwork, connections);
		if (fbNetwork.getChildNodes().getLength() > 0) {
			appElement.appendChild(fbNetwork);
		}
	}

	private void checkAgainstForteTypeCast(ArrayList<Connection> connections,
			ConnectionView connectionView) {
		if (connectionView.getConnectionElement() instanceof DataConnection) {
			DataConnection dataCon = (DataConnection) connectionView
					.getConnectionElement();
			if (Utils.defaultTypeCompatibilityCheck(dataCon.getSource(),
					dataCon.getDestination())) {
				connections.add(connectionView.getConnectionElement());

			} else {

				FB sourceFB = (FB) dataCon.getSource().eContainer()
						.eContainer();
				FB destFB = (FB) dataCon.getDestination().eContainer()
						.eContainer();

				String sourceFBName = "";
				String destFBName = "";
				if (sourceFB != null) {
					sourceFBName = sourceFB.getName();
				}

				if (destFB != null) {
					destFBName = destFB.getName();
				}
				warnings.add("Data Connection " + sourceFBName + "."
						+ dataCon.getSource().getName() + " --> " + destFBName
						+ "." + dataCon.getDestination().getName()
						+ " not exported! (Forte Type Cast)");
			}
		} else if (connectionView.getConnectionElement() instanceof EventConnection) {
			connections.add(connectionView.getConnectionElement());

		}
	}

	private void addSubAppConnections(final Diagram fbNetworkObject,
			ArrayList<Connection> connections) {
		for (Iterator<View> iterator = fbNetworkObject.getChildren().iterator(); iterator
				.hasNext();) {
			View view = (View) iterator.next();
			if (view instanceof MappedSubAppView) {
				MappedSubAppView mappedSubAppView = (MappedSubAppView) view;
				view = mappedSubAppView.getApplicationSubApp();
			}

			if (view instanceof SubAppView) {
				SubAppView subAppView = (SubAppView) view;
				subAppView.getUiSubAppNetwork().getConnections();
				for (Iterator<IConnectionView> iterator2 = subAppView
						.getUiSubAppNetwork().getConnections().iterator(); iterator2
						.hasNext();) {
					ConnectionView connectionView = (ConnectionView) iterator2
							.next();
					if (!connectionView.getConnectionElement()
							.isResTypeConnection()
							&& !connections.contains(connectionView
									.getConnectionElement())) {
						checkAgainstForteTypeCast(connections, connectionView);
					}
				}
				addSubAppConnections(subAppView.getUiSubAppNetwork(),
						connections);
			}
		}
	}

	/**
	 * Adds the device.
	 * 
	 * @param dom
	 *            the dom
	 * @param parent
	 *            the parent
	 * @param device
	 *            the device
	 */
	public void addDevice(final Document dom, final Element parent,
			DeviceView device) {

		Element deviceElement = dom.createElement("Device");
		if (device.getDeviceElement().getName() != null) {
			deviceElement.setAttribute("Name", device.getDeviceElement()
					.getName());
		} else {
			deviceElement.setAttribute("Name", "");
		}
		if (device.getDeviceElement().getDeviceType() != null) {
			deviceElement.setAttribute("Type", device.getDeviceElement()
					.getDeviceType().getName());
		} else {
			deviceElement.setAttribute("Type", "");
		}
		if (device.getDeviceElement().getComment() != null) {
			deviceElement.setAttribute("Comment", device.getDeviceElement()
					.getComment());
		}
		if (device.getPosition() != null) {
			Position uiPosition = device.getPosition();

			if (uiPosition != null) {

				deviceElement.setAttribute("x", CommonElementExporter
						.reConvertCoordinate(uiPosition.getX()).toString());
				deviceElement.setAttribute("y", CommonElementExporter
						.reConvertCoordinate(uiPosition.getY()).toString());
			} else {
				deviceElement.setAttribute("x", device.getDeviceElement()
						.getX());
				deviceElement.setAttribute("y", device.getDeviceElement()
						.getY());

			}

		}
		for (Iterator<VarDeclaration> iterator = device.getDeviceElement()
				.getVarDeclarations().iterator(); iterator.hasNext();) {
			VarDeclaration var = (VarDeclaration) iterator.next();
			if (var.getValue() != null && var.getValue().getValue() != null
					&& !var.getValue().getValue().equals("")) {
				Element parameterElement = dom.createElement("Parameter");
				parameterElement.setAttribute("Name", var.getName());
				parameterElement.setAttribute("Value", var.getValue()
						.getValue());
				deviceElement.appendChild(parameterElement);
			}
		}

		addResources(dom, deviceElement, device.getResourceContainerView()
				.getResources());

		parent.appendChild(deviceElement);

	}

	private void addResources(Document dom, Element deviceElement,
			EList<ResourceView> resources) {
		for (Iterator<ResourceView> iterator = resources.iterator(); iterator
				.hasNext();) {
			ResourceView resource = (ResourceView) iterator.next();
			addResource(dom, deviceElement, resource);
		}

	}

	/**
	 * Adds the resource.
	 * 
	 * @param dom
	 *            the dom
	 * @param parent
	 *            the parent
	 * @param resourceView
	 *            the resource view
	 */
	public void addResource(final Document dom, final Element parent,
			ResourceView resourceView) {

		Element resourceElement = dom.createElement("Resource");
		if (resourceView.getResourceElement().getName() != null) {
			resourceElement.setAttribute("Name", resourceView
					.getResourceElement().getName());
		} else {
			resourceElement.setAttribute("Name", "");
		}
		if (resourceView.getResourceElement().getResourceType() != null) {
			resourceElement.setAttribute("Type", resourceView
					.getResourceElement().getResourceType().getName());
		} else {
			resourceElement.setAttribute("Type", "");
		}
		if (resourceView.getResourceElement().getComment() != null) {
			resourceElement.setAttribute("Comment", resourceView
					.getResourceElement().getComment());
		}
		if (resourceView.getResourceElement().getX() != null) {
			resourceElement.setAttribute("x", resourceView.getResourceElement()
					.getX() != null ? resourceView.getResourceElement().getX()
					: "0");
			resourceElement.setAttribute("y", resourceView.getResourceElement()
					.getY() != null ? resourceView.getResourceElement().getY()
					: "0");
		}
		for (Iterator<VarDeclaration> iterator = resourceView.getResourceElement()
				.getVarDeclarations().iterator(); iterator.hasNext();) {
			VarDeclaration var = (VarDeclaration) iterator.next();
			if (var.getValue() != null && var.getValue().getValue() != null
					&& !var.getValue().getValue().equals("")) {
				Element parameterElement = dom.createElement("Parameter");
				parameterElement.setAttribute("Name", var.getName());
				parameterElement.setAttribute("Value", var.getValue()
						.getValue());
				resourceElement.appendChild(parameterElement);
			}
		}

		addFBNetwork(dom, resourceElement, resourceView.getUIResourceDiagram(),
				"FBNetwork");

		parent.appendChild(resourceElement);

	}

	/**
	 * Save as fbt file.
	 * 
	 * @param destDir
	 *            the dest dir
	 * @param system
	 *            the system
	 * @param forceOverwrite
	 *            the force overwrite
	 */
	public void saveAsSYSFile(final AutomationSystem system,
			final String destDir, boolean forceOverwrite) {
		try {
			File outFile = new File(destDir + File.separatorChar + system.getName() + ".sys");
			int res = SWT.YES;
			
			if (outFile.exists()) {
				MessageBox msgBox = new MessageBox(Display.getDefault().getActiveShell(), SWT.YES
						| SWT.NO | SWT.ICON_QUESTION);
				String msg = MessageFormat.format(
						"File Exists, overwrite?",
						new Object[] { outFile.getAbsolutePath() });
				msgBox.setMessage(msg);

				
				if (forceOverwrite) {
					res = SWT.YES;
				} else {
					res = msgBox.open();
				}
				
				if (res == SWT.NO) {
					warnings.add(" - Export aborted by user because file exists!");
				}
			} 
			
			if (res == SWT.YES) {
				Transformer transformer = createTransformer();

				// write the dom to the file
				Document dom = getDocument(system);
				Source source = new DOMSource(dom); // Document to be transformed
				
				File f = File.createTempFile(system.getName(), ".sys");
				Result result = new StreamResult(new OutputStreamWriter(new FileOutputStream(f), "UTF-8"));
				transformer.transform(source, result);
			
				// XXX the following code is necessary, because FBDK (Holobloc,
				// Inc.) does not support the standalone="no" tag
				BufferedReader in = new BufferedReader(new FileReader(f));
	
				PrintWriter out = new PrintWriter(outFile);
	
				String line; // a line in the file
	
				while ((line = in.readLine()) != null) {
					if (line.contains("standalone=\"no\"")) {
						line = line.replace("standalone=\"no\"", "");
					}
					out.println(line);
				}
				in.close();
				out.flush();
				out.close();
				infos.add(system.getName() + " exported.");
			}
		} catch (Exception e) {
			Activator.getDefault().logError(e.getMessage(), e);
		}
		File temp = new File(destDir + File.separatorChar + system.getName()
				+ ".sys_");
		if (temp.exists() && temp.isDirectory()) {
			temp.delete();
		}
	}

	private Transformer createTransformer()
			throws TransformerFactoryConfigurationError,
			TransformerConfigurationException {
		Transformer transformer;
		TransformerFactory tFactory = TransformerFactory.newInstance();
		tFactory.setAttribute("indent-number", new Integer(2));
		transformer = tFactory.newTransformer();
		transformer.setOutputProperty(
				javax.xml.transform.OutputKeys.DOCTYPE_SYSTEM,
				"http://www.holobloc.com/xml/LibraryElement.dtd");
		transformer.setOutputProperty(
				javax.xml.transform.OutputKeys.ENCODING, "UTF-8");
		transformer.setOutputProperty(
				javax.xml.transform.OutputKeys.VERSION, "1.0");
		transformer.setOutputProperty(
				"{http://xml.apache.org/xslt}indent-amount", "2");
		transformer.setOutputProperty(
				javax.xml.transform.OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(
				javax.xml.transform.OutputKeys.STANDALONE, "no");
		transformer.setOutputProperty(
				javax.xml.transform.OutputKeys.METHOD, "xml");
		return transformer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.export.sys.ISystemExport#getErrors()
	 */
	@Override
	public ArrayList<String> getErrors() {
		return errors;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.export.sys.ISystemExport#getExportFilterDescription()
	 */
	@Override
	public String getExportFilterDescription() {
		return "Compliant export regarding the LibraryElement and DataType DTDs given in IEC Standard 61499-2 published in January 2005.";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.export.sys.ISystemExport#getExportFilterName()
	 */
	@Override
	public String getExportFilterName() {
		return "IEC 61499 System - IEC 61499-2 compliant";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.export.sys.ISystemExport#getInfos()
	 */
	@Override
	public ArrayList<String> getInfos() {
		return infos;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.export.sys.ISystemExport#getWarnings()
	 */
	@Override
	public ArrayList<String> getWarnings() {
		return warnings;
	}

}
