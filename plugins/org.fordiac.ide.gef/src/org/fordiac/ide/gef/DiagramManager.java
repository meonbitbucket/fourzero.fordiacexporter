/*******************************************************************************
 * Copyright (c) 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef;

import org.eclipse.core.runtime.Assert;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.swt.graphics.Color;
import org.fordiac.ide.model.libraryElement.BasicFBType;
import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.libraryElement.ServiceInterfaceFBType;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.ide.model.ui.View;
import org.fordiac.ide.preferences.PreferenceConstants;
import org.fordiac.ide.preferences.PreferenceGetter;

abstract public class DiagramManager {

	public DiagramManager() {
		diagram = createDiagram();
	}

	abstract protected Diagram createDiagram();

	/* the Diagram managed by this manager */
	private Diagram diagram = null; 
	
	protected void setDiagram(Diagram diagram){
		this.diagram = diagram;
	}
	
	public Diagram getDiagram() {
		Assert.isNotNull(diagram, "diagram requested but not correctly initialized!");
		return diagram;
	}
	
	
	protected class FBNetworkAdapter extends EContentAdapter {
		
		SubAppNetwork fbNetwork;
		

		public FBNetworkAdapter(SubAppNetwork fbNetwork) {
			super();
			this.fbNetwork = fbNetwork;
		}

		@Override
		public void notifyChanged(Notification notification) {
			Object feature = notification.getFeature();
			
			if(LibraryElementPackage.eINSTANCE.getSubAppNetwork_FBs().equals(feature)){
				handleFBChange(notification);
			}
			
			super.notifyChanged(notification);
		}

		private void handleFBChange(Notification notification) {
			switch (notification.getEventType()) {
			case Notification.ADD: 
				FB fb = (FB)notification.getNewValue();
				if(null != fb){
					FBView fbView = getFBView(fb);
					if(null == fbView){
						//only add if it is not already in the list
						addChild(createFBView(fb, fbNetwork));
					}
				}
				break;
				
			case Notification.REMOVE:
				FB removedFB = (FB)notification.getOldValue();
				if(null != removedFB){
					FBView fbView = getFBView(removedFB);
					if(null != fbView){
						removeChild(fbView);
					}					
				}
				break;
				
			default:
				break;
			}
		}

		
	};
	
	FBNetworkAdapter fbNetworkAdapter = null;
	
	protected void adaptFBNetwork(SubAppNetwork fbNetwork){
		fbNetworkAdapter = createFBNetworkAdapter(fbNetwork);
		fbNetwork.eAdapters().add(fbNetworkAdapter);
	}

	/** Method that allows to specify a special FBNetwork adapter for listining to other things then FB Instance changes
	 * 
	 * @param fbNetwork
	 * @return
	 */
	protected FBNetworkAdapter createFBNetworkAdapter(SubAppNetwork fbNetwork) {
		return new FBNetworkAdapter(fbNetwork);
	}
	
	public void dispose(){
		if(null != fbNetworkAdapter){
			fbNetworkAdapter.fbNetwork.eAdapters().remove(fbNetworkAdapter);
		}
	}
	
	protected FBView getFBView(FB fb) {
		for (View view : getChildren()) {
			if(view instanceof FBView){
				FBView fbView = (FBView)view;
				if(fbView.getFb().equals(fb)){
					return fbView;
				}
			}
		}
		return null;
	}
	
	protected FBView getFBViewNamed(String name) {
		for (View view : getChildren()) {
			if(view instanceof FBView){
				FBView fbView = (FBView)view;
				if(fbView.getFb().getName().equals(name)){
					return fbView;
				}
			}
		}
		return null;
	}

	protected EList<View> getChildren() {
		return diagram.getChildren();
	}
	
	protected void addChild(View view) {
		getChildren().add(view);
	}

	protected void removeChild(View view) {
		getChildren().remove(view);
	}
	

	public static FBView createFBView(final FB fb, final SubAppNetwork parent) {
		FBView fbView = UiFactory.eINSTANCE.createFBView();
		org.fordiac.ide.model.ui.Position pos = UiFactory.eINSTANCE
				.createPosition();
		pos.setX(fb.getPosition().getX());
		pos.setY(fb.getPosition().getY());
		fbView.setPosition(pos);
		fbView.setFb(fb);

		InterfaceList interfaceList = fb.getInterface();
		if (interfaceList != null) {
			for (Event eventInput: interfaceList.getEventInputs()) {
				fbView.getInterfaceElements().add(createInterfaceElementView(eventInput, parent));
			}
			for (VarDeclaration varInput: interfaceList.getInputVars()) {
				fbView.getInterfaceElements().add(createInterfaceElementView(varInput, parent));
			}
			for (Event eventOutput : interfaceList.getEventOutputs()) {
				fbView.getInterfaceElements().add(createInterfaceElementView(eventOutput, parent));
			}
			for (VarDeclaration varOutput: interfaceList.getOutputVars()) {
				fbView.getInterfaceElements().add(createInterfaceElementView(varOutput, parent));
			}
		}

		Color color = null;
		if (fb.getFBType() instanceof BasicFBType) {
			color = PreferenceGetter
					.getColor(PreferenceConstants.P_BASIC_FB_BG_COLOR);
		} else if (fb.getFBType() instanceof ServiceInterfaceFBType) {
			color = PreferenceGetter
					.getColor(PreferenceConstants.P_SI_FB_BG_COLOR);
		} else if (fb.getFBType() instanceof CompositeFBType) {
			color = PreferenceGetter
					.getColor(PreferenceConstants.P_COMPOSITE_FB_BG_COLOR);
		} else {
			color = ColorConstants.lightGray;
		}
		org.fordiac.ide.model.ui.Color forDiacColor = createFordiacColor(color);
		fbView.setBackgroundColor(forDiacColor);
		
		return fbView;
	}
	
	private static InterfaceElementView createInterfaceElementView(IInterfaceElement element, SubAppNetwork parent) {
		InterfaceElementView view = UiFactory.eINSTANCE
				.createInterfaceElementView();
		view.setIInterfaceElement(element);
		view.setFbNetwork(parent);
		return view;
	}

	private static org.fordiac.ide.model.ui.Color createFordiacColor(final Color color) {
		org.fordiac.ide.model.ui.Color forDiacColor = UiFactory.eINSTANCE
				.createColor();
		forDiacColor.setRed(color.getRed());
		forDiacColor.setBlue(color.getBlue());
		forDiacColor.setGreen(color.getGreen());
		return forDiacColor;
	}

}
