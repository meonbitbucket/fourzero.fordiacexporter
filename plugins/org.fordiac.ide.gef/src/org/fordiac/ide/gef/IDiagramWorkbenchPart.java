/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef;

import org.eclipse.gef.EditPart;
import org.eclipse.ui.IWorkbenchPart;

/**
 * The diagram workbench part interface.
 */
public interface IDiagramWorkbenchPart extends IWorkbenchPart {
	
	/**
	 * Method getDiagramEditPart.
	 * 
	 * @return EditPart
	 */
	public EditPart getDiagramEditPart();

}
