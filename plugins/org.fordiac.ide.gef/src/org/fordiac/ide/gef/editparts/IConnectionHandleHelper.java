/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.editparts;

/**
 * The Interface IConnectionHandleHelper.
 */
public interface IConnectionHandleHelper {
	
	/**
	 * Gets the connection handle position.
	 * 
	 * @return the connection handle position
	 */
	public org.eclipse.draw2d.geometry.Point getConnectionHandlePosition();

	/**
	 * Gets the connection handle width.
	 * 
	 * @return the connection handle width
	 */
	public int getConnectionHandleWidth();
}
