/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.editparts;

import org.eclipse.draw2d.ActionEvent;
import org.eclipse.draw2d.ActionListener;
import org.eclipse.draw2d.Clickable;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.Shape;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.swt.graphics.Image;
import org.fordiac.ide.util.imageprovider.ImageProvider;

/**
 * This abstract class for the EditParts.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class CollapsibleEditPart extends AbstractGraphicalEditPart {

	@Override
	protected IFigure createFigure() {
		return new CollapsibleFigure();
	}

	@Override
	protected void createEditPolicies() {
	}

	/**
	 * The Class CollapsibleFigure.
	 */
	public class CollapsibleFigure extends Shape {
		
		/** The open. */
		Image open = ImageProvider.getImage("open.png");
		
		/** The close. */
		Image close = ImageProvider.getImage("close.png");
		private boolean isOpen = true;

		private final Figure content = new Figure();
		private final Figure main = new Figure();

		/**
		 * Instantiates a new collapsible figure.
		 */
		public CollapsibleFigure() {

			GridLayout gridLayout = new GridLayout();
			gridLayout.verticalSpacing = 0;
			gridLayout.marginHeight = 0;
			gridLayout.marginWidth = 0;
			setLayoutManager(gridLayout);

			GridLayout mainLayout = new GridLayout();
			mainLayout.verticalSpacing = 0;
			mainLayout.marginHeight = 0;
			mainLayout.marginWidth = 0;
			GridData mainLayoutData = new GridData(
					GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL
							| GridData.VERTICAL_ALIGN_FILL);
			add(main);
			main.setLayoutManager(mainLayout);
			setConstraint(main, mainLayoutData);

			final Label l = new Label(open);
			Clickable cl = new Clickable(l);
			cl.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(final ActionEvent event) {
					if (isOpen) {
						isOpen = false;
						l.setIcon(close);
						content.setVisible(false);
						invalidateTree();
						main.invalidate();
						main.repaint();
						revalidate();
						main.revalidate();
						refreshVisuals();
					} else {
						isOpen = true;
						l.setIcon(open);
						content.setVisible(true);
						invalidateTree();
						main.invalidate();
						main.repaint();
						revalidate();
						main.revalidate();
						refreshVisuals();
					}
				}
			});
			main.add(cl);

			content.setLayoutManager(new GridLayout());
			Label label = new Label("Test");
			content.add(label);
			// content.add(new Label("s"));
			content.setVisible(true);
			content.setBorder(new LineBorder(ColorConstants.blue));

			GridData contentPaneLyoutData = new GridData(
					GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL
							| GridData.VERTICAL_ALIGN_FILL);

			main.add(content);
			main.setConstraint(content, contentPaneLyoutData);

		}

		@Override
		protected void fillShape(final Graphics graphics) {
		}

		@Override
		protected void outlineShape(final Graphics graphics) {
		}

	}

	@Override
	protected void refreshVisuals() {
		GridData contentPaneLyoutData = new GridData(
				GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL);

		((GraphicalEditPart) getParent()).setLayoutConstraint(this,
				getFigure(), contentPaneLyoutData);

		super.refreshVisuals();
	}

}
