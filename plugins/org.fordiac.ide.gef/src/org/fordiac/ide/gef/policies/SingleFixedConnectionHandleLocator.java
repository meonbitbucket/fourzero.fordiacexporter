/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.policies;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.handles.HandleBounds;

/**
 * {@link org.eclipse.gmf.runtime.diagram.ui.handles.ConnectionHandleLocator}
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class SingleFixedConnectionHandleLocator extends
		org.fordiac.gmfextensions.ConnectionHandleLocator {

	/** the side on which the handle will appear, value from PositionConstants */
	private int side = PositionConstants.WEST;

	/** the point on the border where the handles should appear */
	private final Point borderPoint = new Point(0, 0);

	/**
	 * The Constructor.
	 * 
	 * @param reference the referenced figure
	 * @param cursorPosition the current
	 * @param side the side where to show (possible values are PositionConstants.WEST
	 * or EAST!) if an other value is used, WEST as default is used
	 */
	public SingleFixedConnectionHandleLocator(IFigure reference,
			Point cursorPosition, int side) {
		super(reference, cursorPosition);

		if (side == PositionConstants.WEST || side == PositionConstants.EAST) {
			this.side = side;
		}
	}

	private Rectangle getReferenceFigureBounds() {
		Rectangle bounds = getReference() instanceof HandleBounds ? ((HandleBounds) getReference())
				.getHandleBounds().getCopy()
				: getReference().getBounds().getCopy();
		return bounds;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.Locator#relocate(org.eclipse.draw2d.IFigure)
	 */
	@Override
	public void relocate(IFigure target) {
		Rectangle bounds = getReferenceFigureBounds();

		Point borderPointTranslated = borderPoint.getCopy();
		getReference().translateToAbsolute(bounds);
		target.translateToRelative(bounds);
		getReference().translateToAbsolute(borderPointTranslated);
		target.translateToRelative(borderPointTranslated);

		// resetBorderPointAndSide();

		// set location based on side (either WEST or EAST)
		if (side == PositionConstants.WEST) {
			// target.setLocation(new Point(bounds.x - 12, bounds.y
			// + (bounds.height / 2) - 7));
			target.setLocation(new Point(bounds.x, bounds.y));
		} else if (side == PositionConstants.EAST) {
			// target.setLocation(new Point(bounds.x + bounds.width - 4, bounds.y
			// + (bounds.height / 2) - 7));
			target.setLocation(new Point(bounds.x, bounds.y));
		}

	}
}
