/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.policies;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.GraphicalEditPart;
import org.fordiac.gmfextensions.ConnectionHandle;
import org.fordiac.gmfextensions.ConnectionHandleLocator;
import org.fordiac.gmfextensions.GEFConnectionHandleEditPolicy;
import org.fordiac.gmfextensions.ConnectionHandle.HandleDirection;

/**
 * The Class SingleFixedConnectionHandleEditPolicy.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class SingleFixedConnectionHandleEditPolicy extends
		GEFConnectionHandleEditPolicy {

	/**
	 * The amount of time to wait before showing the diagram assistant.
	 */
	private static final int APPEARANCE_DELAY = 1;

	/**
	 * The amount of time to wait before hiding the diagram assistant after it has
	 * been made visible.
	 */
	private static final int DISAPPEARANCE_DELAY = 10000;

	/**
	 * The amount of time to wait before hiding the diagram assistant after the
	 * user has moved the mouse outside of the editpart.
	 */
	private static final int DISAPPEARANCE_DELAY_UPON_EXIT = 1;

	private boolean isInput = false;

	/**
	 * Instantiates a new single fixed connection handle edit policy.
	 * 
	 * @param isInput the is input
	 */
	public SingleFixedConnectionHandleEditPolicy(boolean isInput) {
		this.isInput = isInput;
	}

	@Override
	protected int getAppearanceDelay() {
		return APPEARANCE_DELAY;
	}

	@Override
	protected int getDisappearanceDelay() {
		return DISAPPEARANCE_DELAY;
	}

	@Override
	protected int getDisappearanceDelayUponExit() {
		return DISAPPEARANCE_DELAY_UPON_EXIT;
	}

	@Override
	protected List<ConnectionHandle> getHandleFigures() {
		List<ConnectionHandle> list = new ArrayList<ConnectionHandle>(1);

		String tooltip = null;
		if (isInput) {
			tooltip = buildTooltip(HandleDirection.INCOMING);
			if (tooltip != null) {
				list.add(new SimpleConnectionHandle((GraphicalEditPart) getHost(),
						HandleDirection.INCOMING, tooltip));
			}
		} else {
			tooltip = buildTooltip(HandleDirection.OUTGOING);
			if (tooltip != null) {
				list.add(new SimpleConnectionHandle((GraphicalEditPart) getHost(),
						HandleDirection.OUTGOING, tooltip));
			}
		}
		return list;
	}

	@Override
	protected ConnectionHandleLocator getConnectionHandleLocator(
			Point referencePoint) {
		if (isInput) {
			return new SingleFixedConnectionHandleLocator(getHostFigure(),
					referencePoint, PositionConstants.WEST);
		} else {
			return new SingleFixedConnectionHandleLocator(getHostFigure(),
					referencePoint, PositionConstants.EAST);
		}
	}
}
