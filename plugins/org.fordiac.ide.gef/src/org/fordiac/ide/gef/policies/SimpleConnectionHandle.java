/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.policies;

import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.swt.graphics.Image;

/**
 * The Class SimpleConnectionHandle.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class SimpleConnectionHandle extends
		org.fordiac.gmfextensions.ConnectionHandle {

	/**
	 * Instantiates a new simple connection handle.
	 * 
	 * @param ownerEditPart the owner edit part
	 * @param relationshipDirection the relationship direction
	 * @param tooltip the tooltip
	 */
	public SimpleConnectionHandle(final GraphicalEditPart ownerEditPart,
			final HandleDirection relationshipDirection, final String tooltip) {
		super(ownerEditPart, relationshipDirection, tooltip);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.gmfextensions.ConnectionHandle#getImage(int)
	 */
	@Override
	protected Image getImage(final int side) {
		return null;
		// if (side == PositionConstants.WEST) {
		// return ImageProvider.getImage(Messages.SimpleConnectionHandle_ICON_WEST);
		// } else {
		// return ImageProvider.getImage(Messages.SimpleConnectionHandle_ICON_EAST);
		// }
	}

}
