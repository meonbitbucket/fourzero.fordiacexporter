/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.policies;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.ConstrainedLayoutEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.jface.preference.IPreferenceStore;
import org.fordiac.ide.gef.Activator;
import org.fordiac.ide.gef.preferences.DiagramPreferences;

/**
 * An non abstract ConstrainedLayoutEditPolicy.
 */
public class EmptyXYLayoutEditPolicy extends ConstrainedLayoutEditPolicy {
	private static final Dimension DEFAULT_SIZE = new Dimension(-1, -1);

	@Override
	protected Command createAddCommand(final EditPart child,
			final Object constraint) {
		return null;
	}

	@Override
	protected Command createChangeConstraintCommand(final EditPart child,
			final Object constraint) {
		return null;
	}

	@Override
	protected Command getCreateCommand(final CreateRequest request) {
		return null;
	}

	@Override
	protected Command getDeleteDependantCommand(final Request request) {
		return null;
	}

	@Override
	protected EditPolicy createChildEditPolicy(EditPart child) {
		IPreferenceStore pf = Activator.getDefault().getPreferenceStore();
		int cornerDim = pf.getInt(DiagramPreferences.CORNER_DIM);
		if (cornerDim > 1) {
			cornerDim = cornerDim / 2;
		}
		return new ModifiedNonResizeableEditPolicy(cornerDim, new Insets(1));
	}

	/**
	 * Returns a Rectangle at the given Point with width and height of -1.
	 * <code>XYLayout</code> uses width or height equal to '-1' to mean use the
	 * figure's preferred size.
	 * 
	 * @param p the input Point
	 * 
	 * @return a Rectangle
	 */
	@Override
	public Object getConstraintFor(final Point p) {
		return new Rectangle(p, DEFAULT_SIZE);
	}

	/**
	 * Returns a new Rectangle equivalent to the passed Rectangle.
	 * 
	 * @param r the input Rectangle
	 * 
	 * @return a copy of the input Rectangle
	 */
	@Override
	public Object getConstraintFor(final Rectangle r) {
		return new Rectangle(r);
	}

	@Override
	protected void showSizeOnDropFeedback(CreateRequest request) {
		super.showSizeOnDropFeedback(request);
	}

	@Override
	protected void addFeedback(IFigure figure) {
		super.addFeedback(figure);
	}

}
