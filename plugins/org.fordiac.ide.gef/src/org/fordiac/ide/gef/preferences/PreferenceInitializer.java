/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.gef.rulers.RulerProvider;
import org.eclipse.gmf.runtime.diagram.ui.preferences.IPreferenceConstants;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceConverter;
import org.fordiac.ide.gef.Activator;

/**
 * Class used to initialize default preference values.
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#
	 * initializeDefaultPreferences()
	 */
	@Override
	public void initializeDefaultPreferences() {
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		store.setDefault(IPreferenceConstants.PREF_SNAP_TO_GRID, true);
		store.setDefault(IPreferenceConstants.PREF_SHOW_GRID, true);
		store.setDefault(IPreferenceConstants.PREF_GRID_SPACING, 0.2);
		store.setDefault(IPreferenceConstants.PREF_RULER_UNITS,
				RulerProvider.UNIT_INCHES);
		store.setDefault(DiagramPreferences.CONNECTION_ROUTER,
				"WestEastManhattenConnectionRouter");
		PreferenceConverter.setDefault(store, DiagramPreferences.SELECTION_COLOR,
				ColorConstants.gray.getRGB());
		store.setDefault(DiagramPreferences.CORNER_DIM, 14);

	}
}
