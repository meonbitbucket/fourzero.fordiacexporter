/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

package org.fordiac.ide.gef.properties;

import org.eclipse.swt.widgets.Composite;
import org.fordiac.ide.gef.Messages;
import org.fordiac.ide.preferences.PreferenceConstants;
import org.fordiac.ide.util.Activator;

public class FBAppearancePropertySection extends AppearancePropertySection {

	@Override
	protected void initializeControls(Composite parent) {
		super.initializeControls(parent);
		boolean syncColorWithDevice = Activator
				.getDefault()
				.getPreferenceStore()
				.getBoolean(
						PreferenceConstants.P_SYNC_FBColor_WITH_DEVICE_Color);
		colorLabel.setEnabled(!syncColorWithDevice);
		chooseColorBtn.setEnabled(!syncColorWithDevice);
		if (syncColorWithDevice) {
			getWidgetFactory().createLabel(colorsGroup, Messages.FBAppearancePropertySection_LABEL_NotifySyncEnabled);
		}
	}

}
