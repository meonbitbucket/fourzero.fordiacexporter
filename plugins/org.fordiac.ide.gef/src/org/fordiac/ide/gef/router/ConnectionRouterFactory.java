/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.router;

import org.eclipse.draw2d.ConnectionRouter;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PolylineConnection;
import org.fordiac.gmfextensions.PolylineConnectionEx;
import org.fordiac.ide.gef.figures.HideableConnection;

public class ConnectionRouterFactory implements IConnectionRouterFactory {

	public ConnectionRouterFactory() {
		// empty router
	}

	@Override
	public ConnectionRouter getConnectionRouter(IFigure container) {
		return new MoveableRouter();
	}

	@Override
	public PolylineConnection createConnectionFigure() {
		PolylineConnection connection = null;
		connection = new HideableConnection();
		((PolylineConnectionEx) connection).setJumpLinks(true);
		((PolylineConnectionEx) connection).setJumpLinksStyles(
				PolylineConnectionEx.JUMPLINK_FLAG_ABOVE, false, true, false);
		((PolylineConnectionEx) connection).setRoundedBendpointsRadius(1);
		return connection;
	}
}
