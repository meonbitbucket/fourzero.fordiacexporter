/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.commands;

import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.gef.Messages;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.util.Utils;

/**
 * A command to rename any INamedElement.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class ChangeCommentCommand extends Command {

	/** The Constant RENAME. */
	private static final String RENAME = Messages.ChangeCommentCommand_LABEL_ChangeComment;

	/** The named element. */
	private INamedElement namedElement;

	/** The old name. */
	private String comment, oldComment;

	/** The editor. */
	private IEditorPart editor;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return editor.equals(Utils.getCurrentActiveEditor());
	}

	/**
	 * Instantiates a new view rename command.
	 */
	public ChangeCommentCommand() {
		super(RENAME);
	}

	/**
	 * Renames the INamedElement.
	 */
	@Override
	public void execute() {
		editor = Utils.getCurrentActiveEditor();
		oldComment = namedElement.getComment();
		// gebenh - it should be no problem if comment is ""
		//		if (!(comment.equals(""))) { //$NON-NLS-1$
		// // to old name
		namedElement.setComment(comment);
		// } else {
		// namedElement.setComment(oldComment);
		// }
	}

	/**
	 * Sets the INamedElement that needs to be renamed.
	 * 
	 * @param namedElement - the element to be renamed
	 */
	public void setNamedElement(final INamedElement namedElement) {
		this.namedElement = namedElement;
	}

	/**
	 * Sets the new name of the element.
	 * 
	 * @param comment the comment
	 */
	public void setComment(final String comment) {
		this.comment = comment;
	}

	/**
	 * Restores the old InstanceName.
	 */
	@Override
	public void undo() {
		namedElement.setComment(oldComment);
	}

	/**
	 * Redo.
	 * 
	 * @see ChangeCommentCommand#execute()
	 */
	@Override
	public void redo() {
		namedElement.setComment(comment);
	}

	/**
	 * Gets the old Comment.
	 * 
	 * @return the old Comment
	 */
	public String getOldComment() {
		return oldComment;
	}

	/**
	 * Sets the old Comment.
	 * 
	 * @param oldComment the old comment
	 */
	public void setOldComment(final String oldComment) {
		this.oldComment = oldComment;
	}

	/**
	 * Gets the named element.
	 * 
	 * @return the named element
	 */
	public INamedElement getNamedElement() {
		return namedElement;
	}

	/**
	 * Gets the Comment.
	 * 
	 * @return the Comment
	 */
	public String getComment() {
		return comment;
	}

}
