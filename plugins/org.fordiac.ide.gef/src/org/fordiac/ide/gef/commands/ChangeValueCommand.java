/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.commands;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.common.ui.util.StatusLineUtil;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.fordiac.ide.gef.Messages;
import org.fordiac.ide.model.libraryElement.Value;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.typelibrary.DataTypeLibrary;
import org.fordiac.ide.util.Utils;

/**
 * A command to rename any INamedElement.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class ChangeValueCommand extends Command {

	/** The Constant CHANGE. */
	private static final String CHANGE = Messages.ChangeValueCommand_LABEL_ChangeValue;

	/** The value. */
	private Value value;

	/** The old value. */
	private String newValue, oldValue;

	/** The editor. */
	private IEditorPart editor;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		if (value != null && value.eContainer() instanceof VarDeclaration) {
			VarDeclaration parent = (VarDeclaration) value.eContainer();
			if (parent != null
					&& parent.getType() != null
					&& parent.getType().equals(
							DataTypeLibrary.getInstance().getType("ANY"))
							&& null != newValue) {
				if ((!newValue.equals("")) && (!newValue.contains("#"))){
					StatusLineUtil.outputErrorMessage(PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow().getActivePage()
							.getActivePart(),
							"Constant Values are not allowed on ANY Input!");
					return false;
				}
			}
		}
		return super.canExecute();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return editor.equals(Utils.getCurrentActiveEditor());

	}

	/**
	 * Instantiates a new change value command.
	 */
	public ChangeValueCommand() {
		super(CHANGE);
	}

	/**
	 * Renames the UIFB.
	 */
	@Override
	public void execute() {
		editor = Utils.getCurrentActiveEditor();
		oldValue = value.getValue();
		if (newValue == null || newValue.equals("")) { //$NON-NLS-1$
			value.setValue(null);
		} else {
			value.setValue(newValue);
		}
	}

	/**
	 * Sets the Value that needs to be renamed.
	 * 
	 * @param value
	 *            the value
	 */
	public void setValue(final Value value) {
		this.value = value;
	}

	/**
	 * Sets the new Value of the element.
	 * 
	 * @param value
	 *            the value
	 */
	public void setNewValue(final String value) {
		this.newValue = value;
	}

	/**
	 * Restores the old InstanceName.
	 */
	@Override
	public void undo() {
		value.setValue(oldValue);
	}

	/**
	 * Redo.
	 * 
	 * @see ChangeValueCommand#execute()
	 */
	@Override
	public void redo() {
		value.setValue(newValue);
	}

	/**
	 * Gets the old value.
	 * 
	 * @return the old value
	 */
	public String getOldValue() {
		return oldValue;
	}

	/**
	 * Sets the old value.
	 * 
	 * @param oldValue
	 *            the new old value
	 */
	public void setOldValue(final String oldValue) {
		this.oldValue = oldValue;
	}

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Value getValue() {
		return value;
	}

}
