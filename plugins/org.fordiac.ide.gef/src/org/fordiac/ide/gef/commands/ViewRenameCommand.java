/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.commands;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.gef.Messages;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.util.Utils;

/**
 * A command to rename any INamedElement.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class ViewRenameCommand extends Command {

	/** The Constant RENAME. */
	private static final String RENAME = Messages.ViewRenameCommand_LABEL_RenameView;

	/** The named element. */
	private INamedElement namedElement;

	private EObject element;

	/** The old name. */
	private String name, oldName;

	private Object oldObject;

	/** The editor. */
	private IEditorPart editor;

	private int featureID;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return editor.equals(Utils.getCurrentActiveEditor());
	}

	/**
	 * Instantiates a new view rename command.
	 */
	public ViewRenameCommand() {
		super(RENAME);
	}

	/**
	 * Instantiates a new view rename command.
	 * 
	 * @param featureID the featureid to be set.
	 * @param object the object
	 */
	public ViewRenameCommand(EObject object, int featureID) {
		super(RENAME);
		this.element = object;
		this.featureID = featureID;
	}

	/**
	 * Renames the INamedElement.
	 */
	@Override
	public void execute() {
		editor = Utils.getCurrentActiveEditor();

		if (namedElement != null) {
			oldName = namedElement.getName();
			if (!(name.equals(""))) { //$NON-NLS-1$
				// to old name
				namedElement.setName(name);
			} else {
				namedElement.setName(oldName);
			}
		}
		if (element != null && featureID >= 0) {
			oldObject = element.eGet(element.eClass().getEStructuralFeature(
					featureID));
			if (!(name.equals(""))) { //$NON-NLS-1$
				// to old name
				element.eSet(element.eClass().getEStructuralFeature(featureID),
						name);
			} else {
				element.eSet(element.eClass().getEStructuralFeature(featureID),
						oldObject);
			}
		}
	}

	/**
	 * Sets the INamedElement that needs to be renamed.
	 * 
	 * @param namedElement - the element to be renamed
	 */
	public void setNamedElement(final INamedElement namedElement) {
		this.namedElement = namedElement;
	}

	/**
	 * Sets the new name of the element.
	 * 
	 * @param name the new name of the element
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Restores the old InstanceName.
	 */
	@Override
	public void undo() {
		if (namedElement != null) {
			namedElement.setName(oldName);
		}
		if (element != null) {
			element.eSet(element.eClass().getEStructuralFeature(featureID),
					oldObject);
		}
	}

	/**
	 * Redo.
	 * 
	 * @see ViewRenameCommand#execute()
	 */
	@Override
	public void redo() {
		if (namedElement != null) {
			namedElement.setName(name);
		}
		if (element != null) {
			element.eSet(element.eClass().getEStructuralFeature(featureID),
					name);
		}
	}

	/**
	 * Gets the old name.
	 * 
	 * @return the old name
	 */
	public String getOldName() {
		return oldName;
	}

	/**
	 * Sets the old name.
	 * 
	 * @param oldName the new old name
	 */
	public void setOldName(final String oldName) {
		this.oldName = oldName;
	}

	/**
	 * Gets the named element.
	 * 
	 * @return the named element
	 */
	public INamedElement getNamedElement() {
		return namedElement;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

}
