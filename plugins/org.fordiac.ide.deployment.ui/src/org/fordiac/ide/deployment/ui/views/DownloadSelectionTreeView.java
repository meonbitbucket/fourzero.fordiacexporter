/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.deployment.ui.views;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.dialogs.ContainerCheckedTreeViewer;
import org.eclipse.ui.part.DrillDownAdapter;
import org.eclipse.ui.part.ViewPart;
import org.fordiac.ide.deployment.DeploymentCoordinator;
import org.fordiac.ide.deployment.ui.Messages;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.util.ISelectedElementsChangedListener;
import org.fordiac.ide.util.imageprovider.ImageProvider;
import org.fordiac.systemmanagement.DistributedSystemListener;
import org.fordiac.systemmanagement.SystemManager;

/**
 * The Class DownloadSelectionTreeView.
 */
public class DownloadSelectionTreeView extends ViewPart {

	/** The viewer. */
	private ContainerCheckedTreeViewer viewer;

	public ContainerCheckedTreeViewer getViewer() {
		return viewer;
	}

	/** The drilldownadapter. */
	private DrillDownAdapter drillDownAdapter;

	private final ArrayList<Device> initialized = new ArrayList<Device>();


	private void initSelectedProperties(Device device) {
		ArrayList<VarDeclaration> selectedProperties = new ArrayList<VarDeclaration>();
		for (VarDeclaration varDecl : device.getVarDeclarations()) {
			if (!varDecl.getName().equalsIgnoreCase("mgr_id")) {
				selectedProperties.add(varDecl);
			}
		}
		DeploymentCoordinator.getInstance().setDeviceProperties(device,
				selectedProperties);
	}
	

	/**
	 * The constructor.
	 */
	public DownloadSelectionTreeView() {
		SystemManager.getInstance().addWorkspaceListener(
				new DistributedSystemListener() {
					@Override
					public void distributedSystemWorkspaceChanged() {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								if (!viewer.getTree().isDisposed()) {
									viewer.refresh(true);

								}

							}

						});
					}
				});
	}

	private void initializeDeviceProperties() {
		for (AutomationSystem system : SystemManager.getInstance().getSystems()) {
			for (Device dev : system.getSystemConfiguration()
					.getSystemConfigurationNetwork().getDevices()) {
				if (!initialized.contains(dev)) {
					initSelectedProperties(dev);
					initialized.add(dev);
				}

			}
		}
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 * 
	 * @param parent the parent
	 */
	@Override
	public void createPartControl(final Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout());

		initializeDeviceProperties();

		combo = new Combo(composite, SWT.READ_ONLY | SWT.BORDER);
		combo.add("Application");
		combo.add("Resource");
		combo.add("Device");

		combo.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (combo.getText().equals("Application")) {
					DeploymentCoordinator.getInstance().setScope(
							DeploymentCoordinator.DownloadScope.Application);
					MessageBox notSupported = new MessageBox(Display.getCurrent()
							.getActiveShell(), SWT.ICON_INFORMATION);
					notSupported.setMessage("Currently not supported!");
					notSupported.open();
				} else if (combo.getText().equals("Resource")) {
					DeploymentCoordinator.getInstance().setScope(
							DeploymentCoordinator.DownloadScope.Resource);
				} else if (combo.getText().equals("Device")) {
					DeploymentCoordinator.getInstance().setScope(
							DeploymentCoordinator.DownloadScope.Device);
					MessageBox notSupported = new MessageBox(Display.getCurrent()
							.getActiveShell(), SWT.ICON_INFORMATION);
					notSupported.setMessage("Currently not supported!");
					notSupported.open();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		combo.select(1);
		combo.setEnabled(false);
		DeploymentCoordinator.getInstance().setScope(
				DeploymentCoordinator.DownloadScope.Application);
		GridData fillHorizontal = new GridData();
		fillHorizontal.horizontalAlignment = GridData.FILL;
		fillHorizontal.grabExcessHorizontalSpace = true;

		combo.setLayoutData(fillHorizontal);


		viewer = new DownloadSelectionTree( composite, SWT.FULL_SELECTION
				| SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);

		drillDownAdapter = new DrillDownAdapter(viewer);

		GridData fillBoth = new GridData();
		fillBoth.horizontalAlignment = GridData.FILL;
		fillBoth.grabExcessHorizontalSpace = true;
		fillBoth.verticalAlignment = GridData.FILL;
		fillBoth.grabExcessVerticalSpace = true;
		viewer.getTree().setLayoutData(fillBoth);

		
		viewer.setInput(getViewSite());
		
		viewer.addCheckStateListener(new ICheckStateListener() {
			@Override
			public void checkStateChanged(final CheckStateChangedEvent event) {
				notifyListeners();
			}
		});

		getSite().setSelectionProvider(viewer);

		makeActions();
		createToolbarbuttons();
		hookContextMenu();
		hookDoubleClickAction();
		contributeToActionBars();
	}

	/**
	 * Hook context menu.
	 */
	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager(
				Messages.DownloadSelectionTreeView_LABEL_PopupMenu);
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(final IMenuManager manager) {
				DownloadSelectionTreeView.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	/**
	 * Contribute to action bars.
	 */
	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	/**
	 * Fill local pull down.
	 * 
	 * @param manager
	 *          the manager
	 */
	private void fillLocalPullDown(final IMenuManager manager) {
		// not used
	}

	/**
	 * Fill context menu.
	 * 
	 * @param manager
	 *          the manager
	 */
	private void fillContextMenu(final IMenuManager manager) {
		manager.add(new Separator());
		drillDownAdapter.addNavigationActions(manager);
		// Other plug-ins can contribute there actions here
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	/**
	 * Fill local tool bar.
	 * 
	 * @param manager
	 *          the manager
	 */
	private void fillLocalToolBar(final IToolBarManager manager) {
		manager.add(new Separator());
		drillDownAdapter.addNavigationActions(manager);
	}

	/**
	 * Creates the toolbarbuttons.
	 */
	private void createToolbarbuttons() {
		IToolBarManager toolBarManager = getViewSite().getActionBars()
				.getToolBarManager();
		Action collapseAllAction = new Action() {
			@Override
			public void run() {
				viewer.collapseAll();
			}
		};
		collapseAllAction
				.setText(org.fordiac.ide.deployment.ui.Messages.DownloadSelectionTreeView_COLLAPSE_ALL);
		collapseAllAction
				.setToolTipText(Messages.DownloadSelectionTreeView_COLLAPSE_ALL);
		collapseAllAction.setImageDescriptor(ImageDescriptor
				.createFromImage(ImageProvider
						.getImage(Messages.DownloadSelectionTreeView_ICON_COLLAPSE_ALL)));
		toolBarManager.add(collapseAllAction);

		Action expandAllAction = new Action() {
			@Override
			public void run() {
				viewer.expandAll();
			}
		};
		expandAllAction.setText(Messages.DownloadSelectionTreeView_EXPAND_ALL);
		expandAllAction
				.setToolTipText(Messages.DownloadSelectionTreeView_EXPAND_ALL);
		expandAllAction.setImageDescriptor(ImageDescriptor
				.createFromImage(ImageProvider
						.getImage(Messages.DownloadSelectionTreeView_ICON_EXPAND_ALL)));
		toolBarManager.add(expandAllAction);

		Action refresh = new Action() {
			@Override
			public void run() {
				viewer.refresh(true);
			}
		};
		refresh.setText(Messages.DownloadSelectionTreeView_Refresh);
		refresh.setToolTipText(Messages.DownloadSelectionTreeView_Refresh);
		refresh.setImageDescriptor(ImageDescriptor.createFromImage(ImageProvider
				.getImage(Messages.DownloadSelectionTreeView_ICON_REFRESH)));
		toolBarManager.add(refresh);
	}

	/**
	 * Make actions.
	 */
	private void makeActions() {
		// not used
	}

	/**
	 * Hook double click action.
	 */
	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(final DoubleClickEvent event) {
				// doubleClickAction.run();
			}
		});
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	/** The listeners. */
	private final ArrayList<ISelectedElementsChangedListener> listeners = new ArrayList<ISelectedElementsChangedListener>();

	private Combo combo;

	/**
	 * Adds the selected elements changed listener.
	 * 
	 * @param listener the listener
	 */
	public void addSelectedElementsChangedListener(
			final ISelectedElementsChangedListener listener) {
		if ((!listeners.contains(listener)) && (null != listener)) {
			listeners.add(listener);
		}
	}
	
	/**
	 * Adds the selected elements changed listener.
	 * 
	 * @param listener the listener
	 */
	public void removeSelectedElementsChangedListener(
			final ISelectedElementsChangedListener listener) {
		listeners.remove(listener);
	}

	/**
	 * Notify listeners.
	 */
	private void notifyListeners() {
		for (Iterator<ISelectedElementsChangedListener> iterator = listeners
				.iterator(); iterator.hasNext();) {
			ISelectedElementsChangedListener listener = iterator.next();
			listener.selectionChanged();
		}
	}

	/**
	 * Gets the selected elements.
	 * 
	 * @return the selected elements
	 */
	public Object[] getSelectedElements() {
		return viewer.getCheckedElements();
	}

}