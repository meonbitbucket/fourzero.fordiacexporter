/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.deployment.ui;

import org.eclipse.osgi.util.NLS;

/**
 * The Class Messages.
 */
public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.fordiac.ide.deployment.ui.messages"; //$NON-NLS-1$

	/** The Annotation marker access_ labe l_ error annotation. */
	public static String AnnotationMarkerAccess_LABEL_ErrorAnnotation;

	/** The Annotation marker access_ labe l_ warning annotation. */
	public static String AnnotationMarkerAccess_LABEL_WarningAnnotation;

	/** The Download selection tree view_ collaps e_ all. */
	public static String DownloadSelectionTreeView_COLLAPSE_ALL;

	/** The Download selection tree view_ expan d_ all. */
	public static String DownloadSelectionTreeView_EXPAND_ALL;

	/** The Download selection tree view_ ico n_ collaps e_ all. */
	public static String DownloadSelectionTreeView_ICON_COLLAPSE_ALL;

	/** The Download selection tree view_ ico n_ device. */
	public static String DownloadSelectionTreeView_ICON_Device;

	/** The Download selection tree view_ ico n_ expan d_ all. */
	public static String DownloadSelectionTreeView_ICON_EXPAND_ALL;

	/** The Download selection tree view_ ico n_ firmware resource. */
	public static String DownloadSelectionTreeView_ICON_FirmwareResource;

	/** The Download selection tree view_ ico n_ refresh. */
	public static String DownloadSelectionTreeView_ICON_REFRESH;

	/** The Download selection tree view_ ico n_ resource. */
	public static String DownloadSelectionTreeView_ICON_Resource;

	/** The Download selection tree view_ ico n_ system. */
	public static String DownloadSelectionTreeView_ICON_System;

	/** The Download selection tree view_ labe l_ popup menu. */
	public static String DownloadSelectionTreeView_LABEL_PopupMenu;

	/** The Download selection tree view_ refresh. */
	public static String DownloadSelectionTreeView_Refresh;

	/** The Error annotation_ annotation error image. */
	public static String ErrorAnnotation_AnnotationErrorImage;

	/** The Warning annotation_ annotation warning image. */
	public static String WarningAnnotation_AnnotationWarningImage;

	/** The Error annotation_ download error. */
	public static String ErrorAnnotation_DownloadError;

	/** The Log listener_ malformed error. */
	public static String LogListener_MalformedError;

	/** The Log listener_ returned error. */
	public static String LogListener_ReturnedError;

	/** The Mode_ download button label. */
	public static String Mode_DownloadButtonLabel;

	/** The Mode_ icon download button. */
	public static String Mode_ICONDownloadButton;

	/** The Output_ clear action label. */
	public static String Output_ClearActionLabel;

	/** The Output_ clear description. */
	public static String Output_ClearDescription;

	/** The Output_ clear tooltip. */
	public static String Output_ClearTooltip;

	/** The Output_ comment. */
	public static String Output_Comment;

	/** The Output_ download error. */
	public static String Output_DownloadError;

	/** The Output_ download warning. */
	public static String Output_DownloadWarning;

	/** The Output_ icon clear action. */
	public static String Output_ICONClearAction;
	
	/** Title of the create bootfile wizard */  
	public static String FordiacCreateBootfilesWizard_LABEL_Window_Title;
	
	/** Page name of the create bootfile wizard page*/  
	public static String FordiacCreateBootfilesWizard_PageName;
	
	/** Description of the create bootfile page */  
	public static String FordiacCreateBootfilesWizard_PageDESCRIPTION;
	
	/** Title of the create bootfile wizard page*/  
	public static String FordiacCreateBootfilesWizard_PageTITLE;
		
	
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
		// empty private constructor
	}
}
