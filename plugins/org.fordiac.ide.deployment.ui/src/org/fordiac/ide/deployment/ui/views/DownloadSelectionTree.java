/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.deployment.ui.views;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.DecoratingLabelProvider;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.ILabelDecorator;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ContainerCheckedTreeViewer;
import org.fordiac.ide.deployment.DeploymentCoordinator;
import org.fordiac.ide.deployment.ui.Messages;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.libraryElement.SystemConfigurationNetwork;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.util.imageprovider.ImageProvider;
import org.fordiac.systemmanagement.SystemManager;

public class DownloadSelectionTree extends ContainerCheckedTreeViewer {
	
	private static String DOWNLOAD_DEV_SELECTION = "DOWNLOAD_DEV_SELECTION";
	private static String DOWNLOAD_DEV_PROPERTIES = "DOWNLOAD_DEV_PROPERTIES";

	
		
	/**
	 * The Class ViewContentProvider.
	 */
	class ViewContentProvider implements IStructuredContentProvider,
			ITreeContentProvider {
		
		/** The adapter. */
		private final EContentAdapter adapter = new EContentAdapter() {

			@Override
			public void notifyChanged(Notification notification) {
				Display.getDefault().asyncExec(new Runnable() {

					@Override
					public void run() {
						if (!getTree().isDisposed()) {
							refresh(true);
						}
					}
				});
				super.notifyChanged(notification);
			}

		};

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface
		 * .viewers.Viewer, java.lang.Object, java.lang.Object)
		 */
		public void inputChanged(final Viewer v, final Object oldInput,
				final Object newInput) {
			// not used
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
		 */
		public void dispose() {
			// TODO check whether resorces needs to be freed
			// not used
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java
		 * .lang.Object)
		 */
		public Object[] getElements(final Object parent) {
			if (parent.equals(getInput())) {
				List<AutomationSystem> systems = SystemManager.getInstance()
						.getSystems();
				
				for (AutomationSystem sys : systems) {
					if (!sys.eAdapters().contains(adapter)) {
						sys.eAdapters().add(adapter);

					}
				}
				return systems.toArray();
			}
			return getChildren(parent);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.Object
		 * )
		 */
		public Object getParent(final Object child) {
			if (child instanceof Device) {
				return ((Device) child).eContainer();
			}
			if (child instanceof Resource) {
				return ((Resource) child).getDevice();
			}
			return null;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.
		 * Object)
		 */
		public Object[] getChildren(final Object parent) {
			if (parent instanceof AutomationSystem) {
				SystemConfigurationNetwork network = ((AutomationSystem) parent).getSystemConfiguration()
						.getSystemConfigurationNetwork();
				
				if (network == null) {
					return new Object[] {};
				}
				if (!network.eAdapters().contains(adapter)) {
					network.eAdapters().add(adapter);
				}

				return network.getDevices().toArray();
			}
			if (parent instanceof Device) {
				Device device = (Device) parent;
				if (!device.eAdapters().contains(adapter)) {
					device.eAdapters().add(adapter);
				}
				
				List<Resource> resource = new ArrayList<Resource>();
				for (Iterator<Resource> iterator = ((Device) parent).getResource()
						.iterator(); iterator.hasNext();) {
					Resource res = iterator.next();
					if (!res.isDeviceTypeResource()) {
						resource.add(res);
					}
				}
				return resource.toArray();
			}
			if (parent instanceof Resource) {
				ArrayList<INamedElement> elements = new ArrayList<INamedElement>();
				Resource res = (Resource) parent;
				if (res.getFBNetwork() != null) {
					elements.addAll(res.getFBNetwork().getMappedFBs());
					elements.addAll(res.getFBNetwork().getMappedSubApps());
				}
				return elements.toArray();

			}
			return new Object[0];
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.
		 * Object)
		 */
		public boolean hasChildren(final Object parent) {
			if (parent instanceof AutomationSystem) {
				return ((AutomationSystem) parent).getSystemConfiguration()
						.getSystemConfigurationNetwork().getDevices().size() > 0;
			}
			if (parent instanceof Device) {
				return ((Device) parent).getResource().size() > 0;
			}
			if (parent instanceof Resource) {
				Resource res = (Resource) parent;
				if (res.getFBNetwork() == null) {
					return false;
				}
				// return ((Resource)
				// parent).getFBNetwork().getMappedFBs().size() > 0
				// || ((Resource) parent).getFBNetwork().getMappedFBs().size() >
				// 0;
				return false;
			}
			return false;
		}
	}
	
	
	/**
	 * The Class ViewLabelProvider.
	 */
	class ViewLabelProvider extends LabelProvider{

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
		 */
		@Override
		public String getText(final Object obj) {
			if (obj instanceof AutomationSystem) {
				return ((AutomationSystem) obj).getName();
			}
			if (obj instanceof INamedElement) {
				return ((INamedElement) obj).getName();
			}
			return obj.toString();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.LabelProvider#getImage(java.lang.Object)
		 */
		@Override
		public Image getImage(final Object obj) {
			String imageKey = ISharedImages.IMG_OBJ_ELEMENT;
			if (obj instanceof AutomationSystem) {
				return ImageProvider
						.getImage(Messages.DownloadSelectionTreeView_ICON_System);
			}
			if (obj instanceof Device) {
				return ImageProvider
						.getImage(Messages.DownloadSelectionTreeView_ICON_Device);
			}
			if (obj instanceof Resource) {
				Resource res = (Resource) obj;
				if (res.isDeviceTypeResource()) {
					return ImageProvider
							.getImage(Messages.DownloadSelectionTreeView_ICON_FirmwareResource);
				}
				return ImageProvider
						.getImage(Messages.DownloadSelectionTreeView_ICON_Resource);
			}
			return PlatformUI.getWorkbench().getSharedImages().getImage(imageKey);
		}

	}
	
	class DownloadDecoratingLabelProvider extends DecoratingLabelProvider implements ITableLabelProvider {

		public DownloadDecoratingLabelProvider(ILabelProvider provider,
				ILabelDecorator decorator) {
			super(provider, decorator);
		}

		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			if (columnIndex == 0) {
				return getImage(element);
			}
			return null;
		}

		@Override
		public String getColumnText(Object element, int columnIndex) {
			if (columnIndex == 0) {
				return getText(element);
			} else if (columnIndex == 1) {
				if (element instanceof Device) {
					return getSelectedString(element);
				}
			}
			return "";
		}
		
	}
	
	

	public DownloadSelectionTree(Composite parent, int style) {
		super(parent, style);
		
		getTree().setHeaderVisible(true);
		TreeColumn column1 = new TreeColumn(getTree(), SWT.LEFT);
		column1.setText("Selection");
		column1.setWidth(200);
		TreeColumn column2 = new TreeColumn(getTree(), SWT.LEFT);
		column2.setText("Properties");
		column2.setWidth(200);
		
		setContentProvider(new ViewContentProvider());
		//setLabelProvider(new ViewLabelProvider());
		ILabelDecorator decorator = PlatformUI.getWorkbench().getDecoratorManager().getLabelDecorator();  		
		LabelProvider lp = new ViewLabelProvider();		
		setLabelProvider(new DownloadDecoratingLabelProvider(lp,decorator));
		
		setCellModifier(new ICellModifier() {

			public boolean canModify(final Object element, final String property) {
				if (property.equals(DOWNLOAD_DEV_PROPERTIES)
						&& element instanceof Device) {
					return true;
				}
				return false;
			}

			public Object getValue(final Object element, final String property) {
				if (DOWNLOAD_DEV_PROPERTIES.equals(property)) {
					return getSelectedString(element);
				}
				if (DOWNLOAD_DEV_SELECTION.equals(property)) {
					// nothing to do
				}
				return null;
			}

			public void modify(final Object element, final String property,
					final Object value) {
				// nothing to do
			}
		});
		
		setCellEditors(new CellEditor[] { new TextCellEditor(),
				new DialogCellEditor(getTree()) {

					@Override
					protected Object openDialogBox(Control cellEditorWindow) {
						DeviceParametersDialog dialog = new DeviceParametersDialog(
								cellEditorWindow.getShell());

						if (((TreeSelection) getSelection()).getFirstElement() instanceof Device) {
							dialog.setDevice((Device) ((TreeSelection) getSelection())
									.getFirstElement());

							int ret = dialog.open();
							if (ret == Window.OK) {
								DeploymentCoordinator.getInstance().setDeviceProperties(
										dialog.getDevice(), dialog.getSelectedProperties());
								refresh(dialog.getDevice(), true);
							} else {

							}
						}
						return null;
					}

				} });

		setColumnProperties(new String[] { DOWNLOAD_DEV_SELECTION,
				DOWNLOAD_DEV_PROPERTIES });
	}
	
	private String getSelectedString(Object element) {
		ArrayList<VarDeclaration> temp = DeploymentCoordinator.getInstance()
				.getSelectedDeviceProperties((Device) element);
		if (temp != null) {
			StringBuffer buffer = new StringBuffer();
			buffer.append("[");
			boolean first = true;
			for (VarDeclaration varDeclaration : temp) {
				if (first) {
					first = false;
				} else {
					buffer.append(", ");
				}
				buffer.append(varDeclaration.getName());
				buffer.append("=");
				buffer.append(varDeclaration.getValue() != null ? varDeclaration
						.getValue().getValue() : "");
			}
			buffer.append("]");
			return buffer.toString();
		}
		return "[]";
	}


}
