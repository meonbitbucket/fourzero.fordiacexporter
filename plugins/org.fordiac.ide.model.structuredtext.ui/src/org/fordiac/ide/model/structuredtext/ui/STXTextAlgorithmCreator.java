/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.structuredtext.ui;

import java.util.LinkedHashSet;
import java.util.Set;
import org.fordiac.ide.fbt.typeeditor.algorithm.xtext.XTextAlgorithmCreator;
import org.fordiac.ide.fbt.typeeditor.algorithm.xtext.XTextAlgorithmEditor;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.AdapterType;
import org.fordiac.ide.model.libraryElement.BasicFBType;
import org.fordiac.ide.model.libraryElement.VarDeclaration;

public class STXTextAlgorithmCreator extends XTextAlgorithmCreator {

	@SuppressWarnings("restriction")
	@Override
	protected XTextAlgorithmEditor createXTextAlgorithmEditor(
			BasicFBType fbType, org.eclipse.xtext.ui.editor.embedded.EmbeddedEditor editor) {
		return new XTextAlgorithmEditor(editor, fbType){

			@Override
			protected String regeneratePrefix() {
				String retval = new String();				
				
				Set<AdapterDeclaration> list = new LinkedHashSet<AdapterDeclaration>();
				list.addAll(getFbType().getInterfaceList().getPlugs());
				list.addAll(getFbType().getInterfaceList().getSockets());
				Set<String> nameList = new LinkedHashSet<String>();
				for(AdapterDeclaration a : list){
					nameList.add(a.getTypeName());
				}
				for (AdapterDeclaration adapter : list) {
					if(nameList.contains(adapter.getTypeName())){
						nameList.remove(adapter.getTypeName());
						AdapterType a = ((AdapterType)adapter.getType());
						retval += "ADAPTER " + a.getName() + " ";
						if(!a.getInterfaceList().getInputVars().isEmpty()){
							retval += "VAR_INPUT\n";
							for (VarDeclaration var : a.getInterfaceList().getInputVars()) {
								retval += generateSTVarDecl(var);
							}
							retval += " END_VAR\n";
						}
						if(!a.getInterfaceList().getOutputVars().isEmpty()){
							retval += "\nVAR_OUTPUT\n";
							for (VarDeclaration var : a.getInterfaceList().getOutputVars()) {
								retval += generateSTVarDecl(var);
							}		
							retval += "END_VAR\n";
						}
						retval += "END_ADAPTER\n";
					}	
				}
				
				retval += "\nVAR_INPUT\n";
				for (VarDeclaration var : getFbType().getInterfaceList().getInputVars()) {
					retval += generateSTVarDecl(var);
				}
				retval += " END_VAR\n";
				
				retval += "\nVAR_OUTPUT\n";
				for (VarDeclaration var : getFbType().getInterfaceList().getOutputVars()) {
					retval += generateSTVarDecl(var);
				}		
				retval += "END_VAR\n";
				
				retval += "\nVAR_INTERNAL\n";
				for (VarDeclaration var : getFbType().getInternalVars()) {
					retval += generateSTVarDecl(var);
				}
				retval += "END_VAR\n";
				
				retval += "\nPLUGS\n";
				for (AdapterDeclaration var : getFbType().getInterfaceList().getPlugs()) {
					//AdapterType at = (AdapterType)var.getType();
					retval += generateSTVarDecl(var);
				}
				retval += "END_PLUGS\n";

				retval += "\nSOCKETS\n";
				for (AdapterDeclaration var : getFbType().getInterfaceList().getSockets()) {
					retval += generateSTVarDecl(var);
				}
				retval += "END_SOCKETS\n";
				return retval;
			}

			private String generateSTVarDecl(VarDeclaration var) {
				String retVal = var.getName() + ": ";
				
				if(var.isArray()){
					retVal += "ARRAY[0.." + (new Integer(var.getArraySize())).toString() + "] OF ";  
				}
				
				retVal += var.getType().getName() + ";\n";
				
				return retVal;
			}
			
		};
	}

}
