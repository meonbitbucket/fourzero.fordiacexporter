/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.structuredtext.ui;

import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightedPositionAcceptor;
import org.eclipse.xtext.ui.editor.syntaxcoloring.ISemanticHighlightingCalculator;

public class StructuredTextSemanticHighlighter implements
		ISemanticHighlightingCalculator {


	@Override
	public void provideHighlightingFor(XtextResource resource,
			IHighlightedPositionAcceptor acceptor) {
		if (resource == null || resource.getParseResult() == null)
		    return;
		    
		  INode root = resource.getParseResult().getRootNode();
		  for (INode node : root.getAsTreeIterable()) {
		    if ((node.getGrammarElement() instanceof RuleCall) && 
		    		("Variable".equals(((RuleCall)node.getGrammarElement()).getRule().getName()))) {
		      acceptor.addPosition(node.getOffset(), node.getLength(),
		    		  StructuredTextHighlightningConfiguration.Variable_ID);
		    }
		  }
	}

}
