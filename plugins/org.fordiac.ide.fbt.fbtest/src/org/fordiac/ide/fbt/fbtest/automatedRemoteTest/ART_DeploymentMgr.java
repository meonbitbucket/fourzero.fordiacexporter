package org.fordiac.ide.fbt.fbtest.automatedRemoteTest;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Iterator;
import java.util.List;

import org.fordiac.ide.deployment.exceptions.CreateResourceInstanceException;
import org.fordiac.ide.deployment.exceptions.DisconnectException;
import org.fordiac.ide.deployment.iec61499.DeploymentExecutor;
import org.fordiac.ide.deployment.iec61499.EthernetDeviceManagementCommunicationHandler;
import org.fordiac.ide.deployment.iec61499.Messages;
import org.fordiac.ide.deployment.util.IDeploymentListener;
import org.fordiac.ide.fbt.fbtest.Activator;
import org.fordiac.ide.fbt.fbtest.util.FBTHelper;
import org.fordiac.ide.model.Palette.PaletteEntry;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.DeviceType;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.typelibrary.TypeLibrary;

public class ART_DeploymentMgr {

	private Resource res;
	private DeploymentExecutor executor;
	private IDeploymentListener listener;
	private Device dev;

	private FBType fbType;

	private int REQID=0;
	private String address;

	private int UID=-2;

	private boolean deploymentError;
	private int deploymentResponseCounter=0;
	public String MgmtResponse="";
	public String MgmtCommands="";

	public ART_DeploymentMgr(FBType fbType, String address, int paUID) {

		deploymentError=false;
		MgmtResponse="";
		MgmtCommands="";
		UID=paUID;
		this.fbType=fbType;
		if (null!= address) {
			this.address=address;
		} else {
			this.address = "localhost:61499";
		}

		LibraryElementFactory libFactory = org.fordiac.ide.model.libraryElement.impl.LibraryElementFactoryImpl.init();
		DeviceType devType = libFactory.createDeviceType();
		devType.setId("TestDeviceType");
		devType.setName("RMT_DEV");

		dev = libFactory.createDevice();

		dev.setPaletteEntry(null);
		dev.setProfile("HOLOBLOC");
		dev.setName("TestDevice");


		res = libFactory.createResource();
		res.setDevice(dev);
		
		res.setType("EMB_RES");

		List<PaletteEntry> entries = TypeLibrary.getInstance().getPalette().getTypeEntries(res.getType());
		if (entries.size() > 0) {
			PaletteEntry entry = entries.get(0);
			res.setPaletteEntry(entry);
		} 
		
				
		res.setName("__"+fbType.getName()+"Test"+UID);


		executor = new DeploymentExecutor();
		executor.setDeviceManagementCommunicationHandler(new EthernetDeviceManagementCommunicationHandler());
		
		listener = new IDeploymentListener() {
			
			@Override
			public void responseReceived(String response, String source) {
				if (response.toLowerCase().indexOf("reason")>-1) {
					deploymentError=true;
					MgmtCommands+=(response+"\n\n");
					MgmtResponse+=response;
				}
				deploymentResponseCounter--;
				
			}
			
			@Override
			public void postCommandSent(String info, String destination, String command) {
			}
			
			@Override
			public void postCommandSent(String message) {
			}
			
			@Override
			public void postCommandSent(String command, String destination) {
				MgmtCommands+=(destination+command+"\n");
			}
			
			@Override
			public void finished() {
			}
		};
		
		executor.getDevMgmComHandler().addDeploymentListener(listener);
	}

	
	public boolean cleanRes() {
		try {
			executor.getDevMgmComHandler().connect(address);
			deploymentResponseCounter++; //Kill Res
			deploymentResponseCounter++; //Delete Res
			executor.deleteResource(res);
			executor.getDevMgmComHandler().disconnect();
			
		} catch (Exception e) {
			Activator.getDefault().logError(e.getMessage(), e);
			return false;
		}

		return true;
		
	}
	
	public boolean deploy(String TestChannelID) {
		MgmtResponse="";
		MgmtCommands="";
		boolean Error=false;
		int numEI=FBTHelper.getEISize(fbType);
		int numEO=FBTHelper.getEOSize(fbType);

		int numDI=FBTHelper.getDISize(fbType);
		int numDO=FBTHelper.getDOSize(fbType);

		try {
			executor.getDevMgmComHandler().connect(address);
		} catch (Exception e1) {
			MgmtCommands="Error during connection to device: "+address+"\n";
			Error = true;
		} 

		if (Error)
		{
			return false;
		}

		try {
			deploymentResponseCounter++;
			executor.createResource(res);
		} catch (CreateResourceInstanceException e) {
			Activator.getDefault().logError(e.getMessage(), e);
			Error = true;
		} 

		while ((!Error)&&(!deploymentError)&& deploymentResponseCounter>0) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
			}
		}
		
		if (deploymentError || Error) {
//no clean-up required; creation of resource failed!
			
			return false;
			
		}
		
		String destination = res.getName();
		
		try {
			sendREQ(res.getName(), createFB_Request("FBuT",fbType.getName()));
			sendREQ(res.getName(), createFB_Request("Server","SERVER_"+(numDO+1)+"_"+(numDI+1)) );
			sendREQ(res.getName(), createFB_Request("MuX","E_MUX_"+numEO));
			sendREQ(res.getName(), createFB_Request("DeMuX","E_DEMUX_"+numEI));

			//EventConns
			sendREQ(res.getName(), createConnection_Request("Server", "IND", "DeMuX", "EI"));
			sendREQ(res.getName(), createConnection_Request("MuX", "EO", "Server", "RSP"));

			sendREQ(res.getName(), createConnection_Request("START", "COLD", "Server", "INIT"));

			if (null != fbType.getInterfaceList()) {
				if (null!= fbType.getInterfaceList().getEventInputs()) {
					for (Iterator<Event> iterator=fbType.getInterfaceList().getEventInputs().iterator(); iterator.hasNext();) {
						Event EI = iterator.next();
						sendREQ(res.getName(), createConnection_Request("DeMuX", "EO"+(FBTHelper.getEIID(fbType, EI.getName())+1), "FBuT", EI.getName()));
					}
				}
				if (null!= fbType.getInterfaceList().getEventOutputs()) {
					for (Iterator<Event> iterator=fbType.getInterfaceList().getEventOutputs().iterator(); iterator.hasNext();) {
						Event EO = iterator.next();
						sendREQ(res.getName(), createConnection_Request("FBuT", EO.getName(), "MuX", "EI"+(FBTHelper.getEOID(fbType, EO.getName())+1)));
					}
				}

			}

			sendREQ(destination, writeFBParameter_Request("Server.ID", TestChannelID));
			sendREQ(destination, writeFBParameter_Request("Server.QI", "1"));

			//DataConns
			sendREQ(res.getName(), createConnection_Request("Server", "RD_1", "DeMuX", "K"));
			sendREQ(res.getName(), createConnection_Request("MuX", "K", "Server", "SD_1"));

			//Server to FBuT
			if (null!= fbType.getInterfaceList().getInputVars()) {
				for (Iterator<VarDeclaration> iterator=fbType.getInterfaceList().getInputVars().iterator(); iterator.hasNext();) {
					VarDeclaration DI = iterator.next();
					sendREQ(res.getName(), createConnection_Request("Server", "RD_"+(2+FBTHelper.getDIID(fbType, DI.getName())), "FBuT", DI.getName()));
				}
			}
			
			
			//FBuT to Server
			if (null!= fbType.getInterfaceList().getOutputVars()) {
				for (Iterator<VarDeclaration> iterator=fbType.getInterfaceList().getOutputVars().iterator(); iterator.hasNext();) {
					VarDeclaration DO = iterator.next();
					sendREQ(res.getName(), createConnection_Request( "FBuT", DO.getName(), "Server", "SD_"+(2+FBTHelper.getDOID(fbType, DO.getName()))));
				}
			}
			
			deploymentResponseCounter++;
			executor.startResource(res);
			
		} catch (Exception e) {
			Activator.getDefault().logError(e.getMessage(), e);
			Error = true;
		}

		
		try {
			executor.getDevMgmComHandler().disconnect();
		} catch (DisconnectException e) {
			Activator.getDefault().logError(e.getMessage(), e);
		}

		if (deploymentError) {
			cleanRes();
			return false;
		}
		
		return (!Error);
	}


	private String createFB_Request(String fbName, String fbTypeName) {
		return MessageFormat.format(
				Messages.DeploymentExecutor_CreateFBInstance, new Object[] { this.REQID++,
						fbName, fbTypeName });
	}

	private String createConnection_Request(String SrcFBName, String SrcIfElemName, String DstFBName, String DstIfElemName) {
		return MessageFormat.format(
				Messages.DeploymentExecutor_CreateEventConnection, new Object[] {
						this.REQID++, SrcFBName + "." //$NON-NLS-1$
						+ SrcIfElemName,
						DstFBName + "." //$NON-NLS-1$
						+ DstIfElemName });
	}

	
	private String writeFBParameter_Request(final String Dst, final String value) {
		return MessageFormat.format(
				Messages.DeploymentExecutor_WriteParameter, new Object[] { this.REQID++,
						value, Dst }); //$NON-NLS-1$

	}

	private void sendREQ(final String destination, final String request) throws IOException {
		deploymentResponseCounter++;
		executor.getDevMgmComHandler().sendREQ(destination, request);
	}

}
