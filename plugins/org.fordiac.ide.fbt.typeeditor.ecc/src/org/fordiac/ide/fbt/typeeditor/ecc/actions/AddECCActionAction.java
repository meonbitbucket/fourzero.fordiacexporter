/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc.actions;

import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.fordiac.ide.fbt.typeeditor.ecc.ActionCreationFactory;
import org.fordiac.ide.fbt.typeeditor.ecc.ECCEditor;
import org.fordiac.ide.fbt.typeeditor.ecc.commands.CreateECActionCommand;
import org.fordiac.ide.fbt.typeeditor.ecc.editparts.ECStateEditPart;
import org.fordiac.ide.model.libraryElement.ECAction;

public class AddECCActionAction extends SelectionAction {

	/**
	 * Add ECC Action action id. Value: <code>"org.fordiac.ide.fbt.typeeditor.ecc.actions.CreateStateAction"</code>
	 */
	public static final String ADD_ECC_ACTION = "org.fordiac.ide.fbt.typeeditor.ecc.actions.AddECCActionAction";//$NON-NLS-1$

	ActionCreationFactory actionFactory = new ActionCreationFactory();
	
	public AddECCActionAction(IWorkbenchPart part) {
		super(part);
		setId(ADD_ECC_ACTION);
		setText("Add Action");
		setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().getImageDescriptor(ISharedImages.IMG_OBJ_ADD));
	}

	@Override
	protected boolean calculateEnabled() {
		if(1 == getSelectedObjects().size()){
			return (getSelectedObjects().get(0) instanceof ECStateEditPart);
		}
		return false;
	}

	@Override
	public void run() {		
		ECCEditor editor = (ECCEditor)getWorkbenchPart();
		ECAction action = (ECAction)actionFactory.getNewObject();
		execute(new CreateECActionCommand(action, ((ECStateEditPart)(getSelectedObjects().get(0))).getCastedModel()));
		
		editor.outlineSelectionChanged(action);
	}
	
	

}
