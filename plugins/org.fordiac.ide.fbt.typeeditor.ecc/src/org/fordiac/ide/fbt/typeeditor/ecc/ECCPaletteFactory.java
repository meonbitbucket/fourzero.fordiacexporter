/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc;

import org.eclipse.gef.palette.CombinedTemplateCreationEntry;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.ui.palette.FlyoutPaletteComposite;
import org.eclipse.gef.ui.palette.FlyoutPaletteComposite.FlyoutPreferences;
import org.eclipse.jface.resource.ImageDescriptor;
import org.fordiac.ide.util.PaletteUtils;
import org.fordiac.ide.util.imageprovider.ImageProvider;

/**
 * A factory for creating ECCPalette objects.
 */
public final class ECCPaletteFactory {

	/** Preference ID used to persist the palette location. */
	private static final String PALETTE_DOCK_LOCATION = "ECCPaletteFactory.Location"; //$NON-NLS-1$

	/** Preference ID used to persist the palette size. */
	private static final String PALETTE_SIZE = "ECCPaletteFactory.Size"; //$NON-NLS-1$

	/** Preference ID used to persist the flyout palette's state. */
	private static final String PALETTE_STATE = "ECCPaletteFactory.State"; //$NON-NLS-1$

	/**
	 * Return a FlyoutPreferences instance used to save/load the preferences of a
	 * flyout palette.
	 * 
	 * @return the flyout preferences
	 */
	public static FlyoutPreferences createPalettePreferences() {
		boolean val = Activator.getDefault().getPreferenceStore().contains(PALETTE_STATE);
		
		FlyoutPreferences preferences = new FlyoutPreferences() {

			public int getDockLocation() {
				return Activator.getDefault().getPreferenceStore().getInt(
						PALETTE_DOCK_LOCATION);
			}

			public int getPaletteState() {
				return Activator.getDefault().getPreferenceStore()
						.getInt(PALETTE_STATE);

			}

			public int getPaletteWidth() {
				return Activator.getDefault().getPreferenceStore().getInt(PALETTE_SIZE);

			}

			public void setDockLocation(final int location) {
				Activator.getDefault().getPreferenceStore().setValue(
						PALETTE_DOCK_LOCATION, location);
			}

			public void setPaletteState(final int state) {
				Activator.getDefault().getPreferenceStore().setValue(PALETTE_STATE,
						state);

			}

			public void setPaletteWidth(final int width) {
				Activator.getDefault().getPreferenceStore().setValue(PALETTE_SIZE,
						width);

			}
		};
		
		if(!val){
			preferences.setPaletteState(FlyoutPaletteComposite.STATE_PINNED_OPEN);
			preferences.setPaletteWidth(125);
		}
		
		return preferences;
	}

	/**
	 * Creates the PaletteRoot for a PaletteViewer with the contents from
	 * FBTypePalette.
	 * 
	 * @return PaletteRoot
	 */
	public static PaletteRoot createPalette() {
		final PaletteRoot palette = new PaletteRoot();
		fillPalette(palette);
		return palette;
	}

	/**
	 * Fill palette.
	 * 
	 * @param palette
	 *          the palette
	 */
	private static void fillPalette(final PaletteRoot palette) {
		palette.add(PaletteUtils.getToolGroup(palette));
		PaletteDrawer drawer = new PaletteDrawer(
				Messages.ECCPaletteFactory_LABEL_ECCGroup);

		ImageDescriptor desc = ImageProvider
				.getImageDescriptor(Messages.ECCPaletteFactory_ICON_ECState16);
		CombinedTemplateCreationEntry combined = new CombinedTemplateCreationEntry(
				Messages.ECCPaletteFactory_LABEL_State,
				Messages.ECCPaletteFactory_TOOLTIP_State, new StateCreationFactory(),
				desc, desc);

		drawer.add(combined);

		desc = ImageProvider
				.getImageDescriptor(Messages.ECCPaletteFactory_ICON_ECAction);
		combined = new CombinedTemplateCreationEntry(
				Messages.ECCPaletteFactory_LABEL_Action,
				Messages.ECCPaletteFactory_TOOLTIP_Action, new ActionCreationFactory(),
				desc, desc);

		drawer.add(combined);

		palette.add(drawer);
	}

}
