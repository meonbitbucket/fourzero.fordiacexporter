/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {

	/** The Constant P_ECC_STATE_COLOR. */                                                          
	public static final String P_ECC_STATE_COLOR = "ECCEditorStateColor";                           
	                                                                                                
	/** The Constant P_ECC_STATE_BORDER_COLOR. */                                                   
	public static final String P_ECC_STATE_BORDER_COLOR = "ECCEditorStateBorderColor";              
	                                                                                                
	/** The Constant P_ECC_TRANSITION_COLOR. */                                                     
	public static final String P_ECC_TRANSITION_COLOR = "ECCEditorTransitionColor";                 
	                                                                                                
	/** The Constant P_ECC_ALGORITHM_COLOR. */                                                      
	public static final String P_ECC_ALGORITHM_COLOR = "ECCEditorAlgorithmColor";                   
	                                                                                                
	/** The Constant P_ECC_ALGORITHM_BORDER_COLOR. */                                               
	public static final String P_ECC_ALGORITHM_BORDER_COLOR = "ECCEditorAlgorithmBorderColor";      
	                                                                                                
	/** The Constant P_ECC_EVENT_COLOR. */                                                          
	public static final String P_ECC_EVENT_COLOR = "ECCEditorEventColor";                           
	                                                                                                
	/** The Constant P_ECC_EVENT_BORDER_COLOR. */                                                   
	public static final String P_ECC_EVENT_BORDER_COLOR = "ECCEditorEventBorderColor";    
	
}
