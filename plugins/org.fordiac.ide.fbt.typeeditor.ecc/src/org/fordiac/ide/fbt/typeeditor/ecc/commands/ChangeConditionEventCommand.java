/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.ECTransition;
import org.fordiac.ide.model.libraryElement.Event;

/**
 * The Class ChangeConditionCommand.
 */
public class ChangeConditionEventCommand extends Command {

	/** The transition. */
	private final ECTransition transition;

	/** The condition. */
	private final Event conditionEvent;

	/** The old condition. */
	private Event oldConditionEvent;

	/**
	 * Instantiates a new change condition command.
	 * 
	 * @param transition the transition
	 * @param conditionEvent the condition
	 */
	public ChangeConditionEventCommand(final ECTransition transition,
			final Event conditionEvent) {
		super();
		this.transition = transition;
		this.conditionEvent = conditionEvent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		oldConditionEvent = transition.getConditionEvent();
		redo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		transition.setConditionEvent(oldConditionEvent);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		transition.setConditionEvent(conditionEvent);
	}

}
