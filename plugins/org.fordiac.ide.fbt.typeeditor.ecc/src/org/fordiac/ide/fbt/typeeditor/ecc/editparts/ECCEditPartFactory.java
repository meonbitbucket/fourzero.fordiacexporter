/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc.editparts;

import java.text.MessageFormat;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.fordiac.ide.fbt.typeeditor.ecc.Activator;
import org.fordiac.ide.fbt.typeeditor.ecc.Messages;
import org.fordiac.ide.model.libraryElement.ECC;
import org.fordiac.ide.model.libraryElement.ECState;
import org.fordiac.ide.model.libraryElement.ECTransition;

/**
 * A factory for creating ECCEditPart objects.
 */
public class ECCEditPartFactory implements EditPartFactory {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.EditPartFactory#createEditPart(org.eclipse.gef.EditPart,
	 * java.lang.Object)
	 */
	public EditPart createEditPart(final EditPart context,
			final Object modelElement) {
		// get EditPart for model element
		EditPart part = null;
		try {
			part = getPartForElement(context, modelElement);
			// store model element in EditPart
			part.setModel(modelElement);
		} catch (RuntimeException e) {
			Activator.getDefault().logError(e.getMessage(), e);
		}
		return part;
	}

	/**
	 * Maps an object to an EditPart.
	 * 
	 * @param context
	 *          the context
	 * @param modelElement
	 *          the model element
	 * 
	 * @return the part for element
	 * 
	 * @throws RuntimeException
	 *           if no match was found (programming error)
	 */
	protected EditPart getPartForElement(final EditPart context,
			final Object modelElement) {
		if (modelElement instanceof ECC) {
			return new ECCRootEditPart();
		}
		if (modelElement instanceof ECState) {
			return new ECStateEditPart();
		}
		if (modelElement instanceof ECTransition) {
			return new ECTransitionEditPart();
		}
				
		if (modelElement instanceof ECActionAlgorithm){
			return new ECActionAlgorithmEditPart();
		}
		
		if (modelElement instanceof ECActionOutputEvent){
			return new ECActionOutputEventEditPart();
		}

		throw new RuntimeException(MessageFormat.format(
				Messages.ECCEditPartFactory_ERROR_Message,
				new Object[] { ((modelElement != null) ? modelElement.getClass()
						.getName() : "null") })); //$NON-NLS-1$
	}

}
