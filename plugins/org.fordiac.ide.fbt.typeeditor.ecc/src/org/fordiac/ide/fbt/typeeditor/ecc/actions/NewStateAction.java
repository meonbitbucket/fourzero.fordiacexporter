/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc.actions;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.ui.actions.WorkbenchPartAction;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.fbt.typeeditor.ecc.ECCEditor;
import org.fordiac.ide.fbt.typeeditor.ecc.StateCreationFactory;
import org.fordiac.ide.fbt.typeeditor.ecc.commands.CreateECStateCommand;
import org.fordiac.ide.model.libraryElement.ECState;

public class NewStateAction extends WorkbenchPartAction {

	/**
	 * Create State action id. Value: <code>"org.fordiac.ide.fbt.typeeditor.ecc.actions.CreateStateAction"</code>
	 */
	public static final String CREATE_STATE = "org.fordiac.ide.fbt.typeeditor.ecc.actions.CreateStateAction";//$NON-NLS-1$
	
	StateCreationFactory stateFactory = new StateCreationFactory();
	
	public NewStateAction(IWorkbenchPart part) {
		super(part);
		setId(CREATE_STATE);
		setText("New State");
		// setToolTipText(GEFMessages.AlignLeftAction_Tooltip);
		// setImageDescriptor(InternalImages.DESC_HORZ_ALIGN_LEFT);
		// setDisabledImageDescriptor(InternalImages.DESC_HORZ_ALIGN_LEFT_DIS);
	}

	@Override
	protected boolean calculateEnabled() {
		return true; // we can always be enabled
	}

	
	@Override
	public void run() {		
		ECCEditor editor = (ECCEditor)getWorkbenchPart();
		
		org.eclipse.swt.graphics.Point pos = Display.getCurrent().getCursorLocation();
		pos = editor.getGraphicalViewer().getControl().toControl(pos);
		
		ECState model = (ECState) stateFactory.getNewObject();
		execute(new CreateECStateCommand( model, new Point(pos.x, pos.y), editor.getFbType().getECC()));
				
		editor.outlineSelectionChanged(model);
	}
}
