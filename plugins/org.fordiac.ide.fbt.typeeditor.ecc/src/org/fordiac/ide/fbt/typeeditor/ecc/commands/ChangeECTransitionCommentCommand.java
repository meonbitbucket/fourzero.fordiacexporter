/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc.commands;

import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.model.libraryElement.ECTransition;
import org.fordiac.ide.util.Utils;

/**
 * A command to rename an EC Transition comment.
 * 
 */
public class ChangeECTransitionCommentCommand extends Command {

	/** The Constant RENAME. */
	private static final String RENAME = "Change Comment";

	/** The named element. */
	private ECTransition ecTransition;

	/** The old name. */
	private String comment, oldComment;

	/** The editor. */
	private IEditorPart editor;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return editor.equals(Utils.getCurrentActiveEditor());
	}

	/**
	 * Instantiates a new view rename command.
	 */
	public ChangeECTransitionCommentCommand() {
		super(RENAME);
	}

	/**
	 * Renames the INamedElement.
	 */
	@Override
	public void execute() {
		editor = Utils.getCurrentActiveEditor();
		oldComment = ecTransition.getComment();
		ecTransition.setComment(comment);
	}

	/**
	 * Sets the INamedElement that needs to be renamed.
	 * 
	 * @param namedElement - the element to be renamed
	 */
	public void setECTransition(final ECTransition ecTransition) {
		this.ecTransition = ecTransition;
	}

	/**
	 * Sets the new name of the element.
	 * 
	 * @param comment the comment
	 */
	public void setComment(final String comment) {
		this.comment = comment;
	}

	/**
	 * Restores the old InstanceName.
	 */
	@Override
	public void undo() {
		ecTransition.setComment(oldComment);
	}

	/**
	 * Redo.
	 * 
	 * @see ChangeECTransitionCommentCommand#execute()
	 */
	@Override
	public void redo() {
		ecTransition.setComment(comment);
	}

	/**
	 * Gets the old Comment.
	 * 
	 * @return the old Comment
	 */
	public String getOldComment() {
		return oldComment;
	}

	/**
	 * Sets the old Comment.
	 * 
	 * @param oldComment the old comment
	 */
	public void setOldComment(final String oldComment) {
		this.oldComment = oldComment;
	}

	/**
	 * Gets the named element.
	 * 
	 * @return the named element
	 */
	public ECTransition getECTransition() {
		return ecTransition;
	}

	/**
	 * Gets the Comment.
	 * 
	 * @return the Comment
	 */
	public String getComment() {
		return comment;
	}

}
