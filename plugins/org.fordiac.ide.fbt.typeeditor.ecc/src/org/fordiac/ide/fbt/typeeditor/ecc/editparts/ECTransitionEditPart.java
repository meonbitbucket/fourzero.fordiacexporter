/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc.editparts;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.draw2d.AbsoluteBendpoint;
import org.eclipse.draw2d.Bendpoint;
import org.eclipse.draw2d.BendpointConnectionRouter;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.ConnectionEndpointLocator;
import org.eclipse.draw2d.ConnectionLocator;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.DragTracker;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.editparts.AbstractConnectionEditPart;
import org.eclipse.gef.editpolicies.ConnectionEditPolicy;
import org.eclipse.gef.editpolicies.ConnectionEndpointEditPolicy;
import org.eclipse.gef.editpolicies.XYLayoutEditPolicy;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gef.requests.GroupRequest;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.views.properties.ComboBoxPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertySource;
import org.eclipse.ui.views.properties.PropertyDescriptor;
import org.eclipse.ui.views.properties.TextPropertyDescriptor;
import org.fordiac.gmfextensions.PolylineConnectionEx;
import org.fordiac.ide.fbt.typeeditor.ecc.Activator;
import org.fordiac.ide.fbt.typeeditor.ecc.commands.ChangeConditionEventCommand;
import org.fordiac.ide.fbt.typeeditor.ecc.commands.ChangeConditionExpressionCommand;
import org.fordiac.ide.fbt.typeeditor.ecc.commands.ChangeECTransitionCommentCommand;
import org.fordiac.ide.fbt.typeeditor.ecc.commands.DeleteTransitionCommand;
import org.fordiac.ide.fbt.typeeditor.ecc.commands.MoveBendpointCommand;
import org.fordiac.ide.fbt.typeeditor.ecc.policies.TransitionBendPointEditPolicy;
import org.fordiac.ide.fbt.typeeditor.ecc.preferences.PreferenceConstants;
import org.fordiac.ide.fbt.typeeditor.ecc.preferences.PreferenceGetter;
import org.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.AdapterEvent;
import org.fordiac.ide.model.libraryElement.AdapterType;
import org.fordiac.ide.model.libraryElement.BasicFBType;
import org.fordiac.ide.model.libraryElement.ECTransition;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.util.STStringTokenHandling;

/**
 * The Class ConnectionEditPart.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class ECTransitionEditPart extends AbstractConnectionEditPart implements
		IPropertySource {

	/** The Constant VALUE_PARAMETER. */
	private static final String EVENT_NAME_PARAMETER = "Input Event";

	/** The Constant COMMENT_PARAMETER. */
	private static final String CONDITION_PARAMETER = "Condition";

	private static final String COMMENT_PARAMETER = "Comment";

	/** The adapter. */
	private final EContentAdapter adapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {
			super.notifyChanged(notification);
			refresh();
		}


	};

	private void updateOrderLabel() {
		ECTransition transition = getCastedModel();
		
		if(null != transition.getSource()){		
			if(1 < transition.getSource().getOutTransitions().size()){
				int i = 1;
				for (ECTransition runner : transition.getSource().getOutTransitions()) {
					if(runner.equals(transition)){
						orderLabel.setText((new Integer(i)).toString());
						
					}
					i++;
				}
			}
			else{
				//if we are the only transition we don't need to enumerate it
				orderLabel.setText("");
			}
		}
		
	}
	
	/** The adapter. */
	private final EContentAdapter interfaceAdapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {
			if (notification.getEventType() == Notification.REMOVE) {
				if ((notification.getOldValue() == getCastedModel()
						.getConditionEvent())
						|| ((getCastedModel().getConditionEvent() instanceof AdapterEvent)
								&& (notification.getOldValue() instanceof AdapterDeclaration) && (((AdapterEvent) getCastedModel()
								.getConditionEvent()).getAdapterDeclaration() == notification
								.getOldValue()))) {
					AbstractDirectEditableEditPart.executeCommand(new ChangeConditionEventCommand(
							getCastedModel(), null));
				}
			} else if (notification.getEventType() == Notification.SET) {
				if (null != getCastedModel().getConditionEvent()) {
					if (notification.getNewValue() instanceof String) {
						if (getCastedModel().getConditionEvent().getName()
								.equals((String) notification.getNewValue())) {
							super.notifyChanged(notification);
							refresh();
						} else if ((getCastedModel().getConditionEvent() instanceof AdapterEvent)
								&& (((AdapterEvent) getCastedModel()
										.getConditionEvent())
										.getAdapterDeclaration().getName()
										.equals(notification.getNewValue()))) {
							super.notifyChanged(notification);
							refresh();
						}						
					}
				}
				
				if(notification.getNotifier() instanceof VarDeclaration){
					checkConditionExpresion(notification);
				}				
			}			
		}

		private void checkConditionExpresion(Notification notification) {
			if (notification.getNewValue() instanceof String) {
				
				Object feature = notification.getFeature();
				
				if (LibraryElementPackage.eINSTANCE.getINamedElement_Name().equals(feature)){
				
					if(null != getCastedModel().getConditionExpression()){
						if(-1 != getCastedModel().getConditionExpression().indexOf((String)notification.getOldStringValue())){
							//String expresion = getCastedModel().getConditionExpression().replace((String)notification.getOldStringValue(), (String)notification.getNewStringValue());
							String expresion = STStringTokenHandling.replaceSTToken(getCastedModel().getConditionExpression(), 
									(String)notification.getOldStringValue(), (String)notification.getNewStringValue());
							
							getCastedModel().setConditionExpression(expresion);
							refresh();
						}
					}
				}
			}
		}

	};
	
	

	/** The condition. */
	private Label condition;
	
	private Label orderLabel;

	private List<Event> inputEvents;

	/**
	 * Gets the casted model.
	 * 
	 * @return the casted model
	 */
	public ECTransition getCastedModel() {
		return (ECTransition) getModel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		// // Selection handle edit policy.
		// // Makes the connection show a feedback, when selected by the user.
		installEditPolicy(EditPolicy.CONNECTION_ENDPOINTS_ROLE,
				new ConnectionEndpointEditPolicy());

		installEditPolicy(EditPolicy.CONNECTION_BENDPOINTS_ROLE,
				new TransitionBendPointEditPolicy(getCastedModel()));

		// // Allows the removal of the connection model element
		installEditPolicy(EditPolicy.CONNECTION_ROLE,
				new ConnectionEditPolicy() {

					@Override
					protected Command getDeleteCommand(
							final GroupRequest request) {
						return new DeleteTransitionCommand(getCastedModel());
					}

				});
		
		installEditPolicy(EditPolicy.LAYOUT_ROLE,
				new XYLayoutEditPolicy(){

					@Override
					public Command getCommand(Request request) {
						if(RequestConstants.REQ_MOVE.equals(request.getType())){
							return getTransitionMoveCommand((ChangeBoundsRequest)request);
						}
						return null;
					}

					
					
					@Override
					public boolean understandsRequest(Request request) {
						return RequestConstants.REQ_MOVE.equals(request.getType());
						
					}



					protected Command getTransitionMoveCommand(ChangeBoundsRequest request) {			
						
						Point p = new Point(getCastedModel().getPosition().getX(), getCastedModel().getPosition().getY());
						p.x += request.getMoveDelta().x;
						p.y += request.getMoveDelta().y; 
						
						return new MoveBendpointCommand(getCastedModel(), p);
					}

					@Override
					protected EditPolicy createChildEditPolicy(EditPart child) {
						return null;
					}

					@Override
					protected Command getCreateCommand(CreateRequest request) {
						return null;
					}

					@Override
					protected Command getMoveChildrenCommand(Request request) {
						return null;
					}
			});
	}

	

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editparts.AbstractEditPart#performRequest(org.eclipse.gef
	 * .Request)
	 */
	@Override
	public void performRequest(final Request request) {
		// REQ_DIRECT_EDIT -> first select 0.4 sec pause -> click -> edit
		// REQ_OPEN -> doubleclick
		if (request.getType() == RequestConstants.REQ_OPEN) {
			// TODO implement direct edit

		} else {
			super.performRequest(request);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#refreshVisuals()
	 */
	@Override
	protected void refreshVisuals() {
		condition.setText(getCastedModel().getConditionText());
		updateOrderLabel();
	
		AbsoluteBendpoint ab = new AbsoluteBendpoint(getCastedModel()
				.getPosition().getX(), getCastedModel().getPosition().getY());
		List<Bendpoint> bendPoints = new ArrayList<Bendpoint>();
		bendPoints.add(ab);

		getConnectionFigure().getConnectionRouter().setConstraint(
				getConnectionFigure(), bendPoints);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractConnectionEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {

		// PolylineConnection connection = (PolylineConnection) super
		// .createFigure();
		PolylineConnection connection = new PolylineConnectionEx();
		((PolylineConnectionEx) connection)
				.setSmoothness(PolylineConnectionEx.SMOOTH_MORE);
		((PolylineConnectionEx) connection).setAntialias(SWT.ON);
		((PolylineConnectionEx) connection).setJumpLinks(false);
		((PolylineConnectionEx) connection).setLineWidth(2);

		PolygonDecoration rectDec = new PolygonDecoration();
		rectDec.setTemplate(PolygonDecoration.TRIANGLE_TIP);
		rectDec.setScale(7, 4);
		rectDec.setFill(true);

		connection.setForegroundColor(PreferenceGetter
				.getColor(PreferenceConstants.P_ECC_TRANSITION_COLOR));

		connection.setTargetDecoration(rectDec);

		ConnectionLocator constraintLocator = new ConnectionLocator(connection,
				ConnectionLocator.MIDDLE);
		condition = new Label(getCastedModel().getConditionText());
		condition.setBorder(new MarginBorder(3, 6, 3, 6));
		condition.setBackgroundColor(ColorConstants.white);
		condition.setOpaque(true);
		connection.add(condition, constraintLocator);

		BendpointConnectionRouter bcr = new BendpointConnectionRouter();
		int x = 0;
		int y = 0;

		if (getCastedModel().getPosition() != null) {
			x = getCastedModel().getPosition().getX();
			y = getCastedModel().getPosition().getY();
		} else {
			x = condition.getLocation().x;
			y = condition.getLocation().y;
		}

		AbsoluteBendpoint ab = new AbsoluteBendpoint(x, y);
		List<Bendpoint> bendPoints = new ArrayList<Bendpoint>();
		bendPoints.add(ab);
		bcr.setConstraint(connection, bendPoints);
		connection.setConnectionRouter(bcr);
		
		ConnectionEndpointLocator sourceEndpointLocator = new ConnectionEndpointLocator(connection, false);
		//sourceEndpointLocator.setVDistance(5);
		orderLabel = new Label();
		connection.add(orderLabel, sourceEndpointLocator);
		
		Display display = Display.getCurrent();
		Color black = display.getSystemColor(SWT.COLOR_BLACK);
		orderLabel.setForegroundColor(black);
		updateOrderLabel();

		return connection;
	}

	/** The property change listener. */
	private final IPropertyChangeListener propertyChangeListener = new IPropertyChangeListener() {
		@Override
		public void propertyChange(PropertyChangeEvent event) {
			getConnectionFigure()
					.setForegroundColor(
							PreferenceGetter
									.getColor(PreferenceConstants.P_ECC_TRANSITION_COLOR));
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#activate()
	 */
	@Override
	public void activate() {
		if (!isActive()) {
			super.activate();
			Activator.getDefault().getPreferenceStore()
					.addPropertyChangeListener(propertyChangeListener);
			getCastedModel().eAdapters().add(adapter);
			getCastedModel().eContainer().eAdapters().add(adapter);

			// Adapt to the fbtype so that we get informed on interface changes
			((BasicFBType) getCastedModel().eContainer().eContainer())
					.getInterfaceList().eAdapters().add(interfaceAdapter);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#deactivate()
	 */
	@Override
	public void deactivate() {
		if (isActive()) {
			super.deactivate();
			Activator.getDefault().getPreferenceStore()
					.removePropertyChangeListener(propertyChangeListener);
			getCastedModel().eAdapters().remove(adapter);
			getCastedModel().eContainer().eAdapters().remove(adapter);
			
			((BasicFBType) getCastedModel().eContainer().eContainer())
					.getInterfaceList().eAdapters().remove(interfaceAdapter);
		}
	}

	@Override
	public Object getEditableValue() {
		return this;
	}

	@Override
	public IPropertyDescriptor[] getPropertyDescriptors() {
		IPropertyDescriptor[] descriptors = new IPropertyDescriptor[3];
		descriptors[0] = createConditionEventPropertyDescriptor();
		if (getCastedModel().getConditionExpression().equals("1")) { //$NON-NLS-N$
			// if the expression is one we are not allowed to add an additional
			// expression
			descriptors[1] = new PropertyDescriptor(new String(
					CONDITION_PARAMETER), CONDITION_PARAMETER);
		} else {
			descriptors[1] = new TextPropertyDescriptor(new String(
					CONDITION_PARAMETER), CONDITION_PARAMETER);
		}
		descriptors[2] = new TextPropertyDescriptor(new String(
				COMMENT_PARAMETER), COMMENT_PARAMETER);
		return descriptors;
	}

	@Override
	public Object getPropertyValue(Object id) {
		if (id instanceof String) {
			String stringId = (String) id;
			if (stringId.equals(EVENT_NAME_PARAMETER)) {
				if (null != getCastedModel().getConditionEvent()) {
					int i = 0;
					String condEventName = getCastedModel().getConditionEvent()
							.getName();
					for (Event ei : inputEvents) {
						if (ei.getName().equals(condEventName)) {
							break;
						}
						i++;
					}

					return new Integer(i);
				} else if (getCastedModel().getConditionExpression()
						.equals("1")) { //$NON-NLS-N$
					return new Integer(inputEvents.size());
				} else {
					return new Integer(inputEvents.size() + 1);
				}
			} else if (stringId.equals(CONDITION_PARAMETER)) {
				if (getCastedModel().getConditionExpression().equals("1")) { //$NON-NLS-N$
					return "";
				} else
					return getCastedModel().getConditionExpression();
			} else if (stringId.equals(COMMENT_PARAMETER)) {
				return getCastedModel().getComment();
			}
		}
		return null;
	}

	@Override
	public boolean isPropertySet(Object id) {
		return false;
	}

	@Override
	public void resetPropertyValue(Object id) {
	}

	@Override
	public void setPropertyValue(Object id, Object value) {
		if (id instanceof String) {
			String stringId = (String) id;
			if (stringId.equals(EVENT_NAME_PARAMETER)) {
				setEventPropertyValue(((Integer) value).intValue());
			} else if (stringId.equals(CONDITION_PARAMETER)) {
				setConditionExpressionPropertyValue((String) value);
			} else if (stringId.equals(COMMENT_PARAMETER)) {
				ChangeECTransitionCommentCommand changeComment = new ChangeECTransitionCommentCommand();
				changeComment.setECTransition(getCastedModel());
				changeComment.setComment((String) value);
				AbstractDirectEditableEditPart.executeCommand(changeComment);
			}
		}
	}

	private void setEventPropertyValue(int selectedNum) {
		Command cmd = null;
		if (selectedNum < inputEvents.size()) {
			if (getCastedModel().getConditionExpression().equals("1")) { //$NON-NLS-N$
				cmd = new CompoundCommand();
				((CompoundCommand) cmd).add(new ChangeConditionEventCommand(
						getCastedModel(), inputEvents.get(selectedNum)));
				((CompoundCommand) cmd)
						.add(new ChangeConditionExpressionCommand(
								getCastedModel(), "")); //$NON-NLS-N$
			} else {
				cmd = new ChangeConditionEventCommand(getCastedModel(),
						inputEvents.get(selectedNum));
			}
		} else if (selectedNum == inputEvents.size()) {
			// 1 has been chosen
			if (getCastedModel().getConditionEvent() == null) {
				cmd = new ChangeConditionExpressionCommand(getCastedModel(),
						"1"); //$NON-NLS-N$
			} else {
				cmd = new CompoundCommand();
				((CompoundCommand) cmd).add(new ChangeConditionEventCommand(
						getCastedModel(), null));
				((CompoundCommand) cmd)
						.add(new ChangeConditionExpressionCommand(
								getCastedModel(), "1")); //$NON-NLS-N$	
			}
		} else {
			// no event has been chosen
			if (getCastedModel().getConditionExpression().equals("1")) { //$NON-NLS-N$				
				cmd = new ChangeConditionExpressionCommand(getCastedModel(), ""); //$NON-NLS-N$
			} else {
				cmd = new ChangeConditionEventCommand(getCastedModel(), null);
			}
		}

		if (cmd != null) {
			AbstractDirectEditableEditPart.executeCommand(cmd);
		}
	}

	private void setConditionExpressionPropertyValue(String value) {
		Command cmd;

		// first trim leading and trailing spaces
		value = value.replaceAll("^\\s+", "");
		value = value.replaceAll("\\s+$", "");

		if (value.equals("1")) { //$NON-NLS-N$
			// if the user entered a 1 transition remove the event, maybe a warn
			// would be nice or so
			cmd = new CompoundCommand();
			((CompoundCommand) cmd).add(new ChangeConditionEventCommand(
					getCastedModel(), null));
			((CompoundCommand) cmd).add(new ChangeConditionExpressionCommand(
					getCastedModel(), "1")); //$NON-NLS-N$
		} else {
			cmd = new ChangeConditionExpressionCommand(getCastedModel(), value);

		}
		if (cmd != null) {
			AbstractDirectEditableEditPart.executeCommand(cmd);
		}
	}

	private IPropertyDescriptor createConditionEventPropertyDescriptor() {
		populateInputEvents();

		String[] values = new String[inputEvents.size() + 2];
		int i = 0;
		for (Event inEvent : inputEvents) {
			values[i] = inEvent.getName();
			i++;
		}
		values[inputEvents.size()] = "1"; //$NON-NLS-N$
		values[inputEvents.size() + 1] = " "; //$NON-NLS-N$

		return new ComboBoxPropertyDescriptor(new String(EVENT_NAME_PARAMETER),
				EVENT_NAME_PARAMETER, values);
	}

	private void populateInputEvents() {
		inputEvents = new ArrayList<Event>();

		BasicFBType type = (BasicFBType) getCastedModel().eContainer()
				.eContainer();

		inputEvents.addAll(type.getInterfaceList().getEventInputs());

		for (AdapterDeclaration socket : type.getInterfaceList().getSockets()) {
			inputEvents.addAll(ECActionHelpers.createAdapterEventList(((AdapterType) socket
					.getType()).getInterfaceList().getEventOutputs(), socket));
		}

		for (AdapterDeclaration plug : type.getInterfaceList().getPlugs()) {
			inputEvents.addAll(ECActionHelpers.createAdapterEventList(((AdapterType) plug
					.getType()).getInterfaceList().getEventInputs(), plug));
		}

		Collections.sort(inputEvents, new Comparator<Event>() {
			Collator col = Collator.getInstance();

			@Override
			public int compare(Event o1, Event o2) {
				return col.compare(o1.getName(), o2.getName());
			}
		});
	}

	
	public void highlight(boolean highlight) {
		PolylineConnection pc = null;
		if (getConnectionFigure() instanceof PolylineConnection) {
			pc = (PolylineConnection) getConnectionFigure();
		}
		if (highlight && pc != null) {
			pc.setLineWidth(3);
		} else if (!highlight && pc != null) {
			pc.setLineWidth(2);
		}
	}
	
	public DragTracker getDragTracker(Request request) {
		return new org.eclipse.gef.tools.DragEditPartsTracker(this){

			@Override
			protected boolean isMove() {
				if(getSourceEditPart() instanceof ECTransitionEditPart){
					return true;
				}
				return super.isMove();
			}
			
		};
	}

}
