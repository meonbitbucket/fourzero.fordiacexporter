/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc;

import org.eclipse.osgi.util.NLS;

/**
 * The Class Messages.
 */
public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.fordiac.ide.fbt.typeeditor.ecc.messages"; //$NON-NLS-1$
	
	/** The EC action dialog_ labe l_ algorithm. */
	public static String ECActionDialog_LABEL_Algorithm;
	
	/** The EC action dialog_ labe l_ event. */
	public static String ECActionDialog_LABEL_Event;
	
	/** The EC action dialog_ labe l_ title. */
	public static String ECActionDialog_LABEL_Title;
	
	/** The EC action dialog_ statu s_ error. */
	public static String ECActionDialog_STATUS_Error;
	
	/** The EC action dialog_ statu s_ error_ message_ alg and event empty. */
	public static String ECActionDialog_STATUS_Error_Message_AlgAndEventEmpty;
	
	/** The EC action dialog_ statu s_ ok. */
	public static String ECActionDialog_STATUS_OK;
	
	/** The ECC editor_ ico n_ ecc editor. */
	public static String ECCEditor_ICON_ECCEditor;
	
	/** The ECC editor_ labe l_ ecc editor tab name. */
	public static String ECCEditor_LABEL_ECCEditorTabName;
	
	/** The ECC edit part factory_ erro r_ message. */
	public static String ECCEditPartFactory_ERROR_Message;
	
	/** The ECC palette factory_ ico n_ ec action. */
	public static String ECCPaletteFactory_ICON_ECAction;
	
	/** The ECC palette factory_ ico n_ ec state16. */
	public static String ECCPaletteFactory_ICON_ECState16;
	
	/** The ECC palette factory_ labe l_ action. */
	public static String ECCPaletteFactory_LABEL_Action;
	
	/** The ECC palette factory_ labe l_ ecc group. */
	public static String ECCPaletteFactory_LABEL_ECCGroup;
	
	/** The ECC palette factory_ labe l_ state. */
	public static String ECCPaletteFactory_LABEL_State;
	
	/** The ECC palette factory_ labe l_ tools group. */
	public static String ECCPaletteFactory_LABEL_ToolsGroup;
	
	/** The ECC palette factory_ toolti p_ action. */
	public static String ECCPaletteFactory_TOOLTIP_Action;
	
	/** The ECC palette factory_ toolti p_ state. */
	public static String ECCPaletteFactory_TOOLTIP_State;
	
	/** The EC state dialog_ labe l_ ec initial state. */
	public static String ECStateDialog_LABEL_ECInitialState;
	
	/** The EC state dialog_ labe l_ ec state comment. */
	public static String ECStateDialog_LABEL_ECStateComment;
	
	/** The EC state dialog_ labe l_ ec state name. */
	public static String ECStateDialog_LABEL_ECStateName;
	
	/** The EC state dialog_ labe l_ statu s_ empty name error. */
	public static String ECStateDialog_LABEL_STATUS_EmptyNameError;
	
	/** The EC state dialog_ labe l_ statu s_ error. */
	public static String ECStateDialog_LABEL_STATUS_Error;
	
	/** The EC state dialog_ labe l_ statu s_ ok. */
	public static String ECStateDialog_LABEL_STATUS_OK;
	
	/** The EC state dialog_ labe l_ title. */
	public static String ECStateDialog_LABEL_Title;
	
	/** The EC state set position command_ labe l_ move. */
	public static String ECStateSetPositionCommand_LABEL_Move;
	
	/** The EC transition condition dialog_ labe l_ condition. */
	public static String ECTransitionConditionDialog_LABEL_Condition;
	
	/** The EC transition condition dialog_ labe l_ event. */
	public static String ECTransitionConditionDialog_LABEL_Event;
	
	/** The EC transition condition dialog_ labe l_ title. */
	public static String ECTransitionConditionDialog_LABEL_Title;
	
	/** The State creation factory_ labe l_ new ec state. */
	public static String StateCreationFactory_LABEL_NewECState;
	
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
