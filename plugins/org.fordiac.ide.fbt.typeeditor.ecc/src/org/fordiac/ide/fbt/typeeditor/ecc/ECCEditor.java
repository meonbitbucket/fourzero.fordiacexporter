/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.DragTracker;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.KeyHandler;
import org.eclipse.gef.KeyStroke;
import org.eclipse.gef.MouseWheelHandler;
import org.eclipse.gef.MouseWheelZoomHandler;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.rulers.RulerProvider;
import org.eclipse.gef.tools.MarqueeDragTracker;
import org.eclipse.gef.tools.MarqueeSelectionTool;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.gef.ui.actions.AlignmentAction;
import org.eclipse.gef.ui.actions.GEFActionConstants;
import org.eclipse.gef.ui.actions.ZoomInAction;
import org.eclipse.gef.ui.actions.ZoomOutAction;
import org.eclipse.gef.ui.palette.FlyoutPaletteComposite.FlyoutPreferences;
import org.eclipse.gef.ui.parts.GraphicalEditorWithFlyoutPalette;
import org.eclipse.gef.ui.parts.GraphicalViewerKeyHandler;
import org.eclipse.gef.ui.parts.ScrollingGraphicalViewer;
import org.eclipse.gef.ui.rulers.RulerComposite;
import org.eclipse.gmf.runtime.diagram.ui.preferences.IPreferenceConstants;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.commands.ActionHandler;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.handlers.IHandlerService;
import org.fordiac.gmfextensions.AnimatedZoomScalableFreeformRootEditPart;
import org.fordiac.gmfextensions.DirectEditKeyHandler;
import org.fordiac.ide.fbt.typeeditor.FBTypeEditDomain;
import org.fordiac.ide.fbt.typeeditor.ecc.actions.AddECCActionAction;
import org.fordiac.ide.fbt.typeeditor.ecc.actions.DeleteECCAction;
import org.fordiac.ide.fbt.typeeditor.ecc.actions.ECCSelectAllAction;
import org.fordiac.ide.fbt.typeeditor.ecc.actions.NewStateAction;
import org.fordiac.ide.fbt.typeeditor.ecc.actions.SetInitialStateAction;
import org.fordiac.ide.fbt.typeeditor.ecc.editparts.ECActionAlgorithm;
import org.fordiac.ide.fbt.typeeditor.ecc.editparts.ECCEditPartFactory;
import org.fordiac.ide.fbt.typeeditor.ecc.editparts.ECStateEditPart;
import org.fordiac.ide.fbt.typeeditor.editors.IFBTEditorPart;
import org.fordiac.ide.fbt.typemanagement.FBTypeEditorInput;
import org.fordiac.ide.gef.Activator;
import org.fordiac.ide.gef.ZoomUndoRedoContextMenuProvider;
import org.fordiac.ide.gef.actions.SaveImageAction;
import org.fordiac.ide.model.libraryElement.BasicFBType;
import org.fordiac.ide.model.libraryElement.ECAction;
import org.fordiac.ide.model.libraryElement.provider.ECCItemProvider;
import org.fordiac.ide.util.imageprovider.ImageProvider;

/**
 * The Class ECCEditor.
 */
public class ECCEditor extends GraphicalEditorWithFlyoutPalette implements
		IFBTEditorPart {

	/** The fb type. */
	private BasicFBType fbType;

	public BasicFBType getFbType() {
		return fbType;
	}

	/** The shared key handler. */
	private KeyHandler sharedKeyHandler;

	private final EContentAdapter adapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {
			super.notifyChanged(notification);

			if (Notification.REMOVING_ADAPTER != notification.getEventType()) {
				if (((notification.getNewValue() == null) && (notification
						.getNewValue() != notification.getOldValue()))
						|| ((notification.getNewValue() != null) && !(notification
								.getNewValue().equals(notification
								.getOldValue())))) {

				}
			}
		}
	};

	/**
	 * Instantiates a new eCC editor.
	 */
	public ECCEditor() {
	}

	private RulerComposite rulerComp;

	@Override
	protected void createGraphicalViewer(final Composite parent) {
		rulerComp = new RulerComposite(parent, SWT.NONE);
		super.createGraphicalViewer(rulerComp);
		rulerComp
				.setGraphicalViewer((ScrollingGraphicalViewer) getGraphicalViewer());

		RulerProvider rpV = new RulerProvider() {

			@Override
			public int getUnit() {
				return Activator.getDefault().getPreferenceStore()
						.getInt(IPreferenceConstants.PREF_RULER_UNITS);
			}

			@Override
			public Object getRuler() {
				return this;
			}

		};
		RulerProvider rpH = new RulerProvider() {

			@Override
			public int getUnit() {
				return Activator.getDefault().getPreferenceStore()
						.getInt(IPreferenceConstants.PREF_RULER_UNITS);
			}

			@Override
			public Object getRuler() {
				return this;
			}

		};

		getGraphicalViewer().setProperty(RulerProvider.PROPERTY_VERTICAL_RULER,
				rpV);
		getGraphicalViewer().setProperty(
				RulerProvider.PROPERTY_HORIZONTAL_RULER, rpH);
	}

	@Override
	protected Control getGraphicalControl() {
		return rulerComp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.ui.parts.GraphicalEditor#init(org.eclipse.ui.IEditorSite,
	 * org.eclipse.ui.IEditorInput)
	 */
	@Override
	public void init(final IEditorSite site, final IEditorInput input)
			throws PartInitException {
		setInput(input);
		if (input instanceof FBTypeEditorInput) {
			FBTypeEditorInput untypedInput = (FBTypeEditorInput) input;
			if (untypedInput.getContent() instanceof BasicFBType) {
				fbType = (BasicFBType) untypedInput.getContent();
				if (fbType.getECC() != null) { // TODO: BasicFB should have ECC
												// - check why SimpleFBs are
												// also BasicFBs without ECC?
					fbType.getECC().eAdapters().add(adapter);
				}
			}
		}
		setSite(site);
		setEditDomain(new FBTypeEditDomain(this, commandStack));
		setPartName(Messages.ECCEditor_LABEL_ECCEditorTabName);
		setTitleImage(ImageProvider.getImage(Messages.ECCEditor_ICON_ECCEditor));
		super.init(site, input);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.parts.GraphicalEditor#configureGraphicalViewer()
	 */
	@Override
	protected void configureGraphicalViewer() {
		super.configureGraphicalViewer();
		ScrollingGraphicalViewer viewer = (ScrollingGraphicalViewer) getGraphicalViewer();
		/**
		 * the ScaleableFreeformRootEditPart provides a ZoomManager ... (see GEF
		 * API reference)
		 */
		AnimatedZoomScalableFreeformRootEditPart root = createRootEditPart();

		List<String> zoomLevels = new ArrayList<String>(3);
		zoomLevels.add(ZoomManager.FIT_ALL);
		zoomLevels.add(ZoomManager.FIT_WIDTH);
		zoomLevels.add(ZoomManager.FIT_HEIGHT);
		root.getZoomManager().setZoomLevelContributions(zoomLevels);
		// TODO __geben -> move ZoomManager.Animate_zooom_in_out to preference
		root.getZoomManager().setZoomAnimationStyle(
				ZoomManager.ANIMATE_ZOOM_IN_OUT);

		IAction zoomIn = new ZoomInAction(root.getZoomManager());
		IAction zoomOut = new ZoomOutAction(root.getZoomManager());
		getActionRegistry().registerAction(zoomIn);
		getActionRegistry().registerAction(zoomOut);

		IHandlerService zoomInService = (IHandlerService) getSite().getService(
				IHandlerService.class);
		zoomInService.activateHandler(zoomIn.getActionDefinitionId(),
				new ActionHandler(zoomIn));

		IHandlerService zoomOutService = (IHandlerService) getSite()
				.getService(IHandlerService.class);
		zoomOutService.activateHandler(zoomOut.getActionDefinitionId(),
				new ActionHandler(zoomOut));

		viewer.setRootEditPart(root);
		viewer.setEditPartFactory(new ECCEditPartFactory());

		getActionRegistry().registerAction(new SaveImageAction(viewer));

		// configure the context menu provider
		ContextMenuProvider cmProvider = new ZoomUndoRedoContextMenuProvider(
				viewer, root.getZoomManager(), getActionRegistry()) {
			@Override
			public void buildContextMenu(IMenuManager menu) {
				super.buildContextMenu(menu);

				IAction action = registry.getAction(ActionFactory.DELETE
						.getId());
				menu.appendToGroup(GEFActionConstants.GROUP_EDIT, action);				
								
				action = registry.getAction(NewStateAction.CREATE_STATE);
				menu.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
				
				action = registry.getAction(AddECCActionAction.ADD_ECC_ACTION);
				menu.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
				
				action = registry.getAction(SetInitialStateAction.SET_INITIAL_STATE_ACTION);
				menu.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
				
			}
		};
		viewer.setContextMenu(cmProvider);

		viewer.setProperty(MouseWheelHandler.KeyGenerator.getKey(SWT.MOD1),
				MouseWheelZoomHandler.SINGLETON);

		KeyHandler viewerKeyHandler = new GraphicalViewerKeyHandler(viewer)
				.setParent(getCommonKeyHandler());

		viewer.setKeyHandler(new DirectEditKeyHandler(viewer)
				.setParent(viewerKeyHandler));
	}

	public AnimatedZoomScalableFreeformRootEditPart createRootEditPart() {
		return new AnimatedZoomScalableFreeformRootEditPart(){

			@Override
			public DragTracker getDragTracker(Request req) {
				MarqueeDragTracker dragTracker = new MarqueeDragTracker();
				dragTracker.setMarqueeBehavior(MarqueeSelectionTool.BEHAVIOR_NODES_CONTAINED_AND_RELATED_CONNECTIONS);
				return dragTracker;
			}
			
		};
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.gef.ui.parts.GraphicalEditorWithFlyoutPalette#
	 * initializeGraphicalViewer()
	 */
	@Override
	protected void initializeGraphicalViewer() {
		// super.initializeGraphicalViewer();
		GraphicalViewer viewer = getGraphicalViewer();
		if (fbType.getECC() != null) {
			viewer.setContents(fbType.getECC());
		}
	}

	/**
	 * Gets the common key handler.
	 * 
	 * @return the common key handler
	 */
	protected KeyHandler getCommonKeyHandler() {
		if (sharedKeyHandler == null) {
			sharedKeyHandler = new KeyHandler();
			sharedKeyHandler
					.put(KeyStroke.getPressed(SWT.DEL, 127, 0),
							getActionRegistry().getAction(
									ActionFactory.DELETE.getId()));
			sharedKeyHandler.put(
					KeyStroke.getPressed(SWT.F2, 0),
					getActionRegistry().getAction(
							GEFActionConstants.DIRECT_EDIT));
			sharedKeyHandler.put(/* CTRL + '=' */
			KeyStroke.getPressed('+', 0x3d, SWT.CTRL), getActionRegistry()
					.getAction(GEFActionConstants.ZOOM_IN));

		}
		return sharedKeyHandler;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.ui.parts.GraphicalEditor#selectionChanged(org.eclipse
	 * .ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	@Override
	public void selectionChanged(final IWorkbenchPart part,
			final ISelection selection) {
		super.selectionChanged(part, selection);
		updateActions(getSelectionActions());
	}

	/** The palette root. */
	PaletteRoot paletteRoot;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.ui.parts.GraphicalEditorWithFlyoutPalette#getPaletteRoot
	 * ()
	 */
	@Override
	protected PaletteRoot getPaletteRoot() {
		return paletteRoot = ECCPaletteFactory.createPalette();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.
	 * IProgressMonitor)
	 */
	@Override
	public void doSave(final IProgressMonitor monitor) {
		getCommandStack().markSaveLocation();
		firePropertyChange(IEditorPart.PROP_DIRTY);
	}

	public void createPartControl(final Composite parent) {
		SashForm s = new SashForm(parent, SWT.VERTICAL | SWT.SMOOTH);
		Composite graphicaEditor = new Composite(s, SWT.NONE);
		graphicaEditor.setLayout(new FillLayout());
		super.createPartControl(graphicaEditor);
		getSite().setSelectionProvider(getGraphicalViewer());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void createActions() {
		ActionRegistry registry = getActionRegistry();
		IAction action;
		
		action = new NewStateAction(this);
		registry.registerAction(action);
	
		action = new AddECCActionAction(this);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());
		
		action = new SetInitialStateAction(this);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());
		
		action = new AlignmentAction((IWorkbenchPart) this,
				PositionConstants.LEFT);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());

		action = new AlignmentAction((IWorkbenchPart) this,
				PositionConstants.RIGHT);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());

		action = new AlignmentAction((IWorkbenchPart) this,
				PositionConstants.TOP);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());

		action = new AlignmentAction((IWorkbenchPart) this,
				PositionConstants.BOTTOM);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());

		action = new AlignmentAction((IWorkbenchPart) this,
				PositionConstants.CENTER);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());

		action = new AlignmentAction((IWorkbenchPart) this,
				PositionConstants.MIDDLE);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());
		
		super.createActions();
		
		//we need a special delete action that will remove ecc_actions from the list if it contains also the according state		
		action = registry.getAction(ActionFactory.DELETE.getId());
		registry.removeAction(action);
		getSelectionActions().remove(action.getId());
		
		action = new DeleteECCAction((IWorkbenchPart) this);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());
		
		//remove the select all action added in the graphical editor and replace it with our
		action = registry.getAction(ActionFactory.SELECT_ALL.getId());
		registry.removeAction(action);
		getSelectionActions().remove(action.getId());
		
		action = new ECCSelectAllAction((IWorkbenchPart) this);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());
	}

	@Override
	public void dispose() {
		super.dispose();
		if (fbType != null && fbType.getECC() != null) {
			fbType.getECC().eAdapters().remove(adapter);
		}
	}

	@Override
	public boolean outlineSelectionChanged(Object selectedElement) {
		Object obj = getGraphicalViewer().getEditPartRegistry().get(selectedElement);
		if(null != obj){
			if(obj instanceof EditPart){
				getGraphicalViewer().select((EditPart)obj);
				return true;
			}
		}
		if(selectedElement instanceof ECCItemProvider){
			return true;
		}
		if(selectedElement instanceof ECAction){
			obj = getGraphicalViewer().getEditPartRegistry().get(
					((ECAction)selectedElement).eContainer());
			if(null != obj){
				for (Object element : ((ECStateEditPart)obj).getCurrentChildren()) {
					if((element instanceof ECActionAlgorithm) && (selectedElement.equals(
							((ECActionAlgorithm)element).getAction()))){
						obj = getGraphicalViewer().getEditPartRegistry().get(element);
						if(null != obj){
							getGraphicalViewer().select((EditPart)obj);
						}
					}
				}
			}
			return true;
		}
		
		return false;
	}
	
	private CommandStack commandStack;
	
	@Override
	public void setCommonCommandStack(CommandStack commandStack) {
		this.commandStack = commandStack;
	}
	
	public GraphicalViewer getGraphicalViewer() {
		return super.getGraphicalViewer();
	}
	
	@Override
	protected FlyoutPreferences getPalettePreferences() {
		return ECCPaletteFactory.createPalettePreferences();
	}
}
