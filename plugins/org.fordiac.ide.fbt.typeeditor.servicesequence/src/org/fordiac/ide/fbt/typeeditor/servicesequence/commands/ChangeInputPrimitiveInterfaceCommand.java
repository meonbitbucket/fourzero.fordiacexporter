/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.InputPrimitive;
import org.fordiac.ide.model.libraryElement.ServiceInterface;

/**
 * This command is used to change the interface of an
 * <code>org.fordiac.ide.model.libraryElement.InputPrimitive </code>.
 * 
 * @author gebenh
 */
public class ChangeInputPrimitiveInterfaceCommand extends Command {

	private final ServiceInterface serviceInterface;
	private final InputPrimitive inputPrimitive;

	private ServiceInterface oldInterface;

	/**
	 * Constructor of the Command.
	 * 
	 * @param serviceInterface the new ServiceInterface
	 * @param inputPrimitive the inputPrimitive where the ServiceInterface needs to be set
	 */
	public ChangeInputPrimitiveInterfaceCommand(
			ServiceInterface serviceInterface, InputPrimitive inputPrimitive) {
		super();
		this.inputPrimitive = inputPrimitive;
		this.serviceInterface = serviceInterface;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		oldInterface = inputPrimitive.getInterface();
		inputPrimitive.setInterface(serviceInterface);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		inputPrimitive.setInterface(oldInterface);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		inputPrimitive.setInterface(serviceInterface);
	}

}
