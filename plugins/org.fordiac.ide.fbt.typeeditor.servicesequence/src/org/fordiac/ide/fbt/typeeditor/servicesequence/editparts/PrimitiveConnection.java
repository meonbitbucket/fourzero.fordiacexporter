/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.editparts;

/**
 * The Class PrimitiveConnection.
 */
public class PrimitiveConnection {

	private boolean inputPrimitive = false;

	/**
	 * Instantiates a new primitive connection.
	 * 
	 * @param isInputPrimitive the is input primitive
	 */
	public PrimitiveConnection(final boolean isInputPrimitive) {
		this.inputPrimitive = isInputPrimitive;
	}

	/**
	 * Checks if is input primitive.
	 * 
	 * @return true, if is input primitive
	 */
	public boolean isInputPrimitive() {
		return inputPrimitive;
	}

}
