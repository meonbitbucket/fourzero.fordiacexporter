/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.commands;

import org.eclipse.gef.commands.Command;
import org.eclipse.jface.window.Window;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceSequenceView;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.InputPrimitive;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.ServiceInterface;
import org.fordiac.ide.model.libraryElement.ServiceTransaction;
import org.fordiac.ide.util.Utils;

/**
 * Command that creates a new transaction.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class CreateTransactionCommand extends Command {

	private final ServiceSequenceView view;

	private ServiceTransaction transaction;
	
	private boolean defaultLeftInterface;

	private int retVal;

	/**
	 * The Constructor.
	 * 
	 * @param view the view
	 */
	public CreateTransactionCommand(ServiceSequenceView view, boolean defaultLeftInterface) {
		this.view = view;
		this.defaultLeftInterface = defaultLeftInterface;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		return view != null && view.getServiceSequence() != null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return transaction != null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		transaction = LibraryElementFactory.eINSTANCE
				.createServiceTransaction();
		FBType fbType = (FBType) view.getServiceSequence().eContainer().eContainer();

		TransactionDialog dialog = new TransactionDialog(Utils
				.getCurrentActiveEditor().getSite().getShell(), fbType, defaultLeftInterface);
		
		retVal = dialog.open();
		if (retVal == org.eclipse.jface.window.Window.OK) {
			InputPrimitive primitive = LibraryElementFactory.eINSTANCE
					.createInputPrimitive();

			ServiceInterface serviceInterface = dialog.getInterface();
			if (serviceInterface != null) {
				primitive.setInterface(serviceInterface);
			}

			String event = dialog.getEvent();
			if (event != null) {
				primitive.setEvent(event);
			}

			String parameter = dialog.getParameter();
			if (parameter != null) {
				primitive.setParameters(parameter);
			}
			view.getServiceSequence().getServiceTransaction().add(transaction);
			transaction.setInputPrimitive(primitive);
		}

	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		if (retVal == Window.OK) {
			view.getServiceSequence().getServiceTransaction().remove(
					transaction);
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		if (retVal == Window.OK) {
			view.getServiceSequence().getServiceTransaction().add(transaction);
		}
	}

}
