/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ServiceInterfacePaletteFactory;
import org.fordiac.ide.fbt.typeeditor.servicesequence.editparts.TransactionElement;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.OutputPrimitive;

/**
 * The Class CreatePrimitiveCommand.
 */
public class CreateOutputPrimitiveCommand extends Command {

	private final String type;
	private final TransactionElement element;
	private final OutputPrimitive refElement;

	/**
	 * Instantiates a new creates the primitive command.
	 * 
	 * @param type the type
	 * @param element the element
	 */
	public CreateOutputPrimitiveCommand(String type, TransactionElement element, OutputPrimitive refElement) {
		this.type = type;
		this.element = element;
		this.refElement = refElement;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		if (type == null || element == null) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		OutputPrimitive outputPrimitive = LibraryElementFactory.eINSTANCE
				.createOutputPrimitive();
		outputPrimitive.setEvent("INITO");
		
		if (type.equals(ServiceInterfacePaletteFactory.LEFT_OUTPUT_PRIMITIVE)) {			
			outputPrimitive.setInterface(element.getLeftInterface()
					.getServiceInterface());
		} else if (type.equals(ServiceInterfacePaletteFactory.RIGHT_OUTPUT_PRIMITIVE)) {
			outputPrimitive.setInterface(element.getRightInterface()
					.getServiceInterface());

		}
		if(null == refElement){
			element.getTransaction().getOutputPrimitive().add(outputPrimitive);
		}
		else{
			int index = element.getTransaction().getOutputPrimitive().indexOf(refElement);
			element.getTransaction().getOutputPrimitive().add(index, outputPrimitive);
		}
		super.execute();
	}
}
