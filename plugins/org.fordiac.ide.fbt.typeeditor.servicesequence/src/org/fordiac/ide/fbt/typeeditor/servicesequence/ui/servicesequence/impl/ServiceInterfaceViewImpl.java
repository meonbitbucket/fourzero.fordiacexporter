/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceInterfaceView;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServicesequencePackage;

import org.fordiac.ide.model.libraryElement.ServiceInterface;

import org.fordiac.ide.model.ui.impl.ViewImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Service Interface View</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.impl.ServiceInterfaceViewImpl#getServiceInterface <em>Service Interface</em>}</li>
 * <li>{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.impl.ServiceInterfaceViewImpl#isIsLeftInterface <em>Is Left Interface</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ServiceInterfaceViewImpl extends ViewImpl implements ServiceInterfaceView {
	/**
	 * The cached value of the '{@link #getServiceInterface() <em>Service Interface</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServiceInterface()
	 * @generated
	 * @ordered
	 */
	protected ServiceInterface serviceInterface;

	/**
	 * The default value of the '{@link #isIsLeftInterface() <em>Is Left Interface</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsLeftInterface()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_LEFT_INTERFACE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsLeftInterface() <em>Is Left Interface</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsLeftInterface()
	 * @generated
	 * @ordered
	 */
	protected boolean isLeftInterface = IS_LEFT_INTERFACE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServiceInterfaceViewImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ServicesequencePackage.Literals.SERVICE_INTERFACE_VIEW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the service interface
	 * 
	 * @generated
	 */
	public ServiceInterface getServiceInterface() {
		if (serviceInterface != null && serviceInterface.eIsProxy()) {
			InternalEObject oldServiceInterface = (InternalEObject)serviceInterface;
			serviceInterface = (ServiceInterface)eResolveProxy(oldServiceInterface);
			if (serviceInterface != oldServiceInterface) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ServicesequencePackage.SERVICE_INTERFACE_VIEW__SERVICE_INTERFACE, oldServiceInterface, serviceInterface));
			}
		}
		return serviceInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the service interface
	 * 
	 * @generated
	 */
	public ServiceInterface basicGetServiceInterface() {
		return serviceInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @param newServiceInterface the new service interface
	 * 
	 * @generated
	 */
	public void setServiceInterface(ServiceInterface newServiceInterface) {
		ServiceInterface oldServiceInterface = serviceInterface;
		serviceInterface = newServiceInterface;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServicesequencePackage.SERVICE_INTERFACE_VIEW__SERVICE_INTERFACE, oldServiceInterface, serviceInterface));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return true, if checks if is is left interface
	 * 
	 * @generated
	 */
	public boolean isIsLeftInterface() {
		return isLeftInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @param newIsLeftInterface the new is left interface
	 * 
	 * @generated
	 */
	public void setIsLeftInterface(boolean newIsLeftInterface) {
		boolean oldIsLeftInterface = isLeftInterface;
		isLeftInterface = newIsLeftInterface;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServicesequencePackage.SERVICE_INTERFACE_VIEW__IS_LEFT_INTERFACE, oldIsLeftInterface, isLeftInterface));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @param featureID the feature id
	 * @param resolve the resolve
	 * @param coreType the core type
	 * 
	 * @return the object
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ServicesequencePackage.SERVICE_INTERFACE_VIEW__SERVICE_INTERFACE:
				if (resolve) return getServiceInterface();
				return basicGetServiceInterface();
			case ServicesequencePackage.SERVICE_INTERFACE_VIEW__IS_LEFT_INTERFACE:
				return isIsLeftInterface() ? Boolean.TRUE : Boolean.FALSE;
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @param featureID the feature id
	 * @param newValue the new value
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ServicesequencePackage.SERVICE_INTERFACE_VIEW__SERVICE_INTERFACE:
				setServiceInterface((ServiceInterface)newValue);
				return;
			case ServicesequencePackage.SERVICE_INTERFACE_VIEW__IS_LEFT_INTERFACE:
				setIsLeftInterface(((Boolean)newValue).booleanValue());
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @param featureID the feature id
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ServicesequencePackage.SERVICE_INTERFACE_VIEW__SERVICE_INTERFACE:
				setServiceInterface((ServiceInterface)null);
				return;
			case ServicesequencePackage.SERVICE_INTERFACE_VIEW__IS_LEFT_INTERFACE:
				setIsLeftInterface(IS_LEFT_INTERFACE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @param featureID the feature id
	 * 
	 * @return true, if e is set
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ServicesequencePackage.SERVICE_INTERFACE_VIEW__SERVICE_INTERFACE:
				return serviceInterface != null;
			case ServicesequencePackage.SERVICE_INTERFACE_VIEW__IS_LEFT_INTERFACE:
				return isLeftInterface != IS_LEFT_INTERFACE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the string
	 * 
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (isLeftInterface: ");
		result.append(isLeftInterface);
		result.append(')');
		return result.toString();
	}

} //ServiceInterfaceViewImpl
