/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.OutputPrimitive;
import org.fordiac.ide.model.libraryElement.ServiceInterface;

/**
 * This command is used to change the interface of an
 * <code>org.fordiac.ide.model.libraryElement.OutputPrimitive</code>.
 * 
 * @author gebenh
 */
public class ChangeOutputPrimitiveInterfaceCommand extends Command {

	private final ServiceInterface serviceInterface;
	private final OutputPrimitive outputPrimitive;
	private ServiceInterface oldInterface;

	/**
	 * Constructor of the Command.
	 * 
	 * @param serviceInterface the new ServiceInterface
	 * @param outputPrimitive the OutputPrimitive where the ServiceInterface needs to be set
	 */
	public ChangeOutputPrimitiveInterfaceCommand(
			ServiceInterface serviceInterface, OutputPrimitive outputPrimitive) {
		super();
		this.outputPrimitive = outputPrimitive;
		this.serviceInterface = serviceInterface;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		oldInterface = outputPrimitive.getInterface();
		outputPrimitive.setInterface(serviceInterface);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		outputPrimitive.setInterface(oldInterface);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		outputPrimitive.setInterface(serviceInterface);
	}
}
