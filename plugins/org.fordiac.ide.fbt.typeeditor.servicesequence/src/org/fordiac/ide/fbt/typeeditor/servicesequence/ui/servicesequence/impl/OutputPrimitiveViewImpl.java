/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.OutputPrimitiveView;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceInterfaceView;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServicesequencePackage;

import org.fordiac.ide.model.libraryElement.OutputPrimitive;

import org.fordiac.ide.model.ui.impl.ViewImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Output Primitive View</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.impl.OutputPrimitiveViewImpl#getOutputPrimitive <em>Output Primitive</em>}</li>
 * <li>{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.impl.OutputPrimitiveViewImpl#getInterfaceView <em>Interface View</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class OutputPrimitiveViewImpl extends ViewImpl implements OutputPrimitiveView {
	/**
	 * The cached value of the '{@link #getOutputPrimitive() <em>Output Primitive</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputPrimitive()
	 * @generated
	 * @ordered
	 */
	protected OutputPrimitive outputPrimitive;
	/**
	 * The cached value of the '{@link #getInterfaceView() <em>Interface View</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterfaceView()
	 * @generated
	 * @ordered
	 */
	protected ServiceInterfaceView interfaceView;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OutputPrimitiveViewImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ServicesequencePackage.Literals.OUTPUT_PRIMITIVE_VIEW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the output primitive
	 * 
	 * @generated
	 */
	public OutputPrimitive getOutputPrimitive() {
		if (outputPrimitive != null && outputPrimitive.eIsProxy()) {
			InternalEObject oldOutputPrimitive = (InternalEObject)outputPrimitive;
			outputPrimitive = (OutputPrimitive)eResolveProxy(oldOutputPrimitive);
			if (outputPrimitive != oldOutputPrimitive) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ServicesequencePackage.OUTPUT_PRIMITIVE_VIEW__OUTPUT_PRIMITIVE, oldOutputPrimitive, outputPrimitive));
			}
		}
		return outputPrimitive;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the output primitive
	 * 
	 * @generated
	 */
	public OutputPrimitive basicGetOutputPrimitive() {
		return outputPrimitive;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @param newOutputPrimitive the new output primitive
	 * 
	 * @generated
	 */
	public void setOutputPrimitive(OutputPrimitive newOutputPrimitive) {
		OutputPrimitive oldOutputPrimitive = outputPrimitive;
		outputPrimitive = newOutputPrimitive;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServicesequencePackage.OUTPUT_PRIMITIVE_VIEW__OUTPUT_PRIMITIVE, oldOutputPrimitive, outputPrimitive));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the interface view
	 * 
	 * @generated
	 */
	public ServiceInterfaceView getInterfaceView() {
		if (interfaceView != null && interfaceView.eIsProxy()) {
			InternalEObject oldInterfaceView = (InternalEObject)interfaceView;
			interfaceView = (ServiceInterfaceView)eResolveProxy(oldInterfaceView);
			if (interfaceView != oldInterfaceView) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ServicesequencePackage.OUTPUT_PRIMITIVE_VIEW__INTERFACE_VIEW, oldInterfaceView, interfaceView));
			}
		}
		return interfaceView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the service interface view
	 * 
	 * @generated
	 */
	public ServiceInterfaceView basicGetInterfaceView() {
		return interfaceView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @param newInterfaceView the new interface view
	 * 
	 * @generated
	 */
	public void setInterfaceView(ServiceInterfaceView newInterfaceView) {
		ServiceInterfaceView oldInterfaceView = interfaceView;
		interfaceView = newInterfaceView;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServicesequencePackage.OUTPUT_PRIMITIVE_VIEW__INTERFACE_VIEW, oldInterfaceView, interfaceView));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @param featureID the feature id
	 * @param resolve the resolve
	 * @param coreType the core type
	 * 
	 * @return the object
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ServicesequencePackage.OUTPUT_PRIMITIVE_VIEW__OUTPUT_PRIMITIVE:
				if (resolve) return getOutputPrimitive();
				return basicGetOutputPrimitive();
			case ServicesequencePackage.OUTPUT_PRIMITIVE_VIEW__INTERFACE_VIEW:
				if (resolve) return getInterfaceView();
				return basicGetInterfaceView();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @param featureID the feature id
	 * @param newValue the new value
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ServicesequencePackage.OUTPUT_PRIMITIVE_VIEW__OUTPUT_PRIMITIVE:
				setOutputPrimitive((OutputPrimitive)newValue);
				return;
			case ServicesequencePackage.OUTPUT_PRIMITIVE_VIEW__INTERFACE_VIEW:
				setInterfaceView((ServiceInterfaceView)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @param featureID the feature id
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ServicesequencePackage.OUTPUT_PRIMITIVE_VIEW__OUTPUT_PRIMITIVE:
				setOutputPrimitive((OutputPrimitive)null);
				return;
			case ServicesequencePackage.OUTPUT_PRIMITIVE_VIEW__INTERFACE_VIEW:
				setInterfaceView((ServiceInterfaceView)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @param featureID the feature id
	 * 
	 * @return true, if e is set
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ServicesequencePackage.OUTPUT_PRIMITIVE_VIEW__OUTPUT_PRIMITIVE:
				return outputPrimitive != null;
			case ServicesequencePackage.OUTPUT_PRIMITIVE_VIEW__INTERFACE_VIEW:
				return interfaceView != null;
		}
		return super.eIsSet(featureID);
	}

} //OutputPrimitiveViewImpl
