/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.editparts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.Shape;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceInterfaceView;
import org.fordiac.ide.gef.editparts.AbstractViewEditPart;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.ui.InterfaceElementView;

/**
 * The Class ServiceInterfaceEditPart.
 */
public class ServiceInterfaceEditPart extends AbstractViewEditPart {

	private final EContentAdapter adapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {
			super.notifyChanged(notification);
		}
	};

	private class ServiceInterfaceFigure extends Shape {

		private final Label name;

		// private final Figure serviceInterface;

		public ServiceInterfaceFigure() {
			GridLayout gl;
			setLayoutManager(gl = new GridLayout());
			gl.verticalSpacing = 0;
			gl.marginHeight = 5;

			GridData nameData = new GridData();
			nameData.grabExcessHorizontalSpace = true;
			nameData.horizontalAlignment = GridData.FILL;

			name = new Label(getCastedModel().getServiceInterface().getName());
			add(name);
			setConstraint(name, nameData);

			if (getCastedModel().isIsLeftInterface()) {
				name.setLabelAlignment(PositionConstants.RIGHT);
			} else {
				name.setLabelAlignment(PositionConstants.LEFT);

			}

		}

		@Override
		protected void fillShape(final Graphics graphics) {
		}

		@Override
		protected void outlineShape(final Graphics graphics) {
		}

		public Label getName() {
			return name;
		}

		// public Figure getServiceInterface() {
		// return serviceInterface;
		// }

	}

	@Override
	protected List<Object> getModelChildren() {
		ArrayList<Object> temp = new ArrayList<Object>();

		return temp;
	}

	// @Override
	// protected void addChildVisual(final EditPart childEditPart, final int
	// index) {
	// IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
	// if (childEditPart instanceof InputPrimitiveEditPart
	// || childEditPart instanceof OutputPrimitiveEditPart) {
	// ((ServiceInterfaceFigure) getFigure()).getServiceInterface().add(
	// child);
	// } else {
	// super.addChildVisual(childEditPart, index);
	// }
	// }

	@Override
	protected IFigure createFigureForModel() {
		return new ServiceInterfaceFigure();
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getContentAdapter()
	 */
	@Override
	public EContentAdapter getContentAdapter() {
		return adapter;

	}

	/**
	 * Gets the casted model.
	 * 
	 * @return the casted model
	 */
	public ServiceInterfaceView getCastedModel() {
		return (ServiceInterfaceView) getModel();
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getINamedElement()
	 */
	@Override
	public INamedElement getINamedElement() {
		return getCastedModel().getServiceInterface();
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getInterfaceElements()
	 */
	@Override
	public List<? extends InterfaceElementView> getInterfaceElements() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getNameLabel()
	 */
	@Override
	public Label getNameLabel() {
		return ((ServiceInterfaceFigure) getFigure()).getName();
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getPreferenceChangeListener()
	 */
	@Override
	public IPropertyChangeListener getPreferenceChangeListener() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.views.properties.IPropertySource#getEditableValue()
	 */
	@Override
	public Object getEditableValue() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.views.properties.IPropertySource#getPropertyValue(java.lang.Object)
	 */
	@Override
	public Object getPropertyValue(final Object id) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.views.properties.IPropertySource#isPropertySet(java.lang.Object)
	 */
	@Override
	public boolean isPropertySet(final Object id) {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.views.properties.IPropertySource#resetPropertyValue(java.lang.Object)
	 */
	@Override
	public void resetPropertyValue(final Object id) {

	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.views.properties.IPropertySource#setPropertyValue(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void setPropertyValue(final Object id, final Object value) {

	}

}
