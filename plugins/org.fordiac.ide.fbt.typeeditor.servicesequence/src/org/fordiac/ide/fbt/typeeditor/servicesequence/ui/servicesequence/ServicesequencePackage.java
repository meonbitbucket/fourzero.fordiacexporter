/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.EReference;
import org.fordiac.ide.model.ui.UiPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServicesequenceFactory
 * @model kind="package"
 * @generated
 */
public interface ServicesequencePackage extends EPackage {
	
	/** The package name. <!-- begin-user-doc --> <!-- end-user-doc --> */
	String eNAME = "servicesequence";

	/** The package namespace URI. <!-- begin-user-doc --> <!-- end-user-doc --> */
	String eNS_URI = "org.fordiac.ide.fbt.typeeditor.servicesequence.model.ui";

	/** The package namespace name. <!-- begin-user-doc --> <!-- end-user-doc --> */
	String eNS_PREFIX = "org.fordiac.ide.fbt.typeeditor.servicesequence.model.ui";

	/** The singleton instance of the package. <!-- begin-user-doc --> <!-- end-user-doc --> */
	ServicesequencePackage eINSTANCE = org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.impl.ServicesequencePackageImpl.init();

	/** The meta object id for the '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.impl.ServiceSequenceViewImpl <em>Service Sequence View</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc --> */
	int SERVICE_SEQUENCE_VIEW = 0;

	/** The feature id for the '<em><b>Position</b></em>' containment reference. <!-- begin-user-doc --> <!-- end-user-doc --> */
	int SERVICE_SEQUENCE_VIEW__POSITION = UiPackage.VIEW__POSITION;

	/** The feature id for the '<em><b>Background Color</b></em>' containment reference. <!-- begin-user-doc --> <!-- end-user-doc --> */
	int SERVICE_SEQUENCE_VIEW__BACKGROUND_COLOR = UiPackage.VIEW__BACKGROUND_COLOR;

	/** The feature id for the '<em><b>Service Sequence</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc --> */
	int SERVICE_SEQUENCE_VIEW__SERVICE_SEQUENCE = UiPackage.VIEW_FEATURE_COUNT + 0;

	/** The number of structural features of the '<em>Service Sequence View</em>' class. <!-- begin-user-doc --> <!-- end-user-doc --> */
	int SERVICE_SEQUENCE_VIEW_FEATURE_COUNT = UiPackage.VIEW_FEATURE_COUNT + 1;


	/** The meta object id for the '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.impl.ServiceInterfaceViewImpl <em>Service Interface View</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc --> */
	int SERVICE_INTERFACE_VIEW = 1;

	/** The feature id for the '<em><b>Position</b></em>' containment reference. <!-- begin-user-doc --> <!-- end-user-doc --> */
	int SERVICE_INTERFACE_VIEW__POSITION = UiPackage.VIEW__POSITION;

	/** The feature id for the '<em><b>Background Color</b></em>' containment reference. <!-- begin-user-doc --> <!-- end-user-doc --> */
	int SERVICE_INTERFACE_VIEW__BACKGROUND_COLOR = UiPackage.VIEW__BACKGROUND_COLOR;

	/** The feature id for the '<em><b>Service Interface</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc --> */
	int SERVICE_INTERFACE_VIEW__SERVICE_INTERFACE = UiPackage.VIEW_FEATURE_COUNT + 0;

	/** The feature id for the '<em><b>Is Left Interface</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc --> */
	int SERVICE_INTERFACE_VIEW__IS_LEFT_INTERFACE = UiPackage.VIEW_FEATURE_COUNT + 1;

	/** The number of structural features of the '<em>Service Interface View</em>' class. <!-- begin-user-doc --> <!-- end-user-doc --> */
	int SERVICE_INTERFACE_VIEW_FEATURE_COUNT = UiPackage.VIEW_FEATURE_COUNT + 2;


	/** The meta object id for the '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.impl.InputPrimitiveViewImpl <em>Input Primitive View</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc --> */
	int INPUT_PRIMITIVE_VIEW = 2;

	/** The feature id for the '<em><b>Position</b></em>' containment reference. <!-- begin-user-doc --> <!-- end-user-doc --> */
	int INPUT_PRIMITIVE_VIEW__POSITION = UiPackage.VIEW__POSITION;

	/** The feature id for the '<em><b>Background Color</b></em>' containment reference. <!-- begin-user-doc --> <!-- end-user-doc --> */
	int INPUT_PRIMITIVE_VIEW__BACKGROUND_COLOR = UiPackage.VIEW__BACKGROUND_COLOR;

	/** The feature id for the '<em><b>Input Primitive</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc --> */
	int INPUT_PRIMITIVE_VIEW__INPUT_PRIMITIVE = UiPackage.VIEW_FEATURE_COUNT + 0;

	/** The feature id for the '<em><b>Interface View</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc --> */
	int INPUT_PRIMITIVE_VIEW__INTERFACE_VIEW = UiPackage.VIEW_FEATURE_COUNT + 1;

	/** The number of structural features of the '<em>Input Primitive View</em>' class. <!-- begin-user-doc --> <!-- end-user-doc --> */
	int INPUT_PRIMITIVE_VIEW_FEATURE_COUNT = UiPackage.VIEW_FEATURE_COUNT + 2;

	/** The meta object id for the '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.impl.OutputPrimitiveViewImpl <em>Output Primitive View</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc --> */
	int OUTPUT_PRIMITIVE_VIEW = 3;

	/** The feature id for the '<em><b>Position</b></em>' containment reference. <!-- begin-user-doc --> <!-- end-user-doc --> */
	int OUTPUT_PRIMITIVE_VIEW__POSITION = UiPackage.VIEW__POSITION;

	/** The feature id for the '<em><b>Background Color</b></em>' containment reference. <!-- begin-user-doc --> <!-- end-user-doc --> */
	int OUTPUT_PRIMITIVE_VIEW__BACKGROUND_COLOR = UiPackage.VIEW__BACKGROUND_COLOR;

	/** The feature id for the '<em><b>Output Primitive</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc --> */
	int OUTPUT_PRIMITIVE_VIEW__OUTPUT_PRIMITIVE = UiPackage.VIEW_FEATURE_COUNT + 0;

	/** The feature id for the '<em><b>Interface View</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc --> */
	int OUTPUT_PRIMITIVE_VIEW__INTERFACE_VIEW = UiPackage.VIEW_FEATURE_COUNT + 1;

	/** The number of structural features of the '<em>Output Primitive View</em>' class. <!-- begin-user-doc --> <!-- end-user-doc --> */
	int OUTPUT_PRIMITIVE_VIEW_FEATURE_COUNT = UiPackage.VIEW_FEATURE_COUNT + 2;


	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceSequenceView <em>Service Sequence View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Service Sequence View</em>'.
	 * 
	 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceSequenceView
	 * @generated
	 */
	EClass getServiceSequenceView();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceSequenceView#getServiceSequence <em>Service Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Service Sequence</em>'.
	 * 
	 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceSequenceView#getServiceSequence()
	 * @see #getServiceSequenceView()
	 * @generated
	 */
	EReference getServiceSequenceView_ServiceSequence();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceInterfaceView <em>Service Interface View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Service Interface View</em>'.
	 * 
	 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceInterfaceView
	 * @generated
	 */
	EClass getServiceInterfaceView();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceInterfaceView#getServiceInterface <em>Service Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Service Interface</em>'.
	 * 
	 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceInterfaceView#getServiceInterface()
	 * @see #getServiceInterfaceView()
	 * @generated
	 */
	EReference getServiceInterfaceView_ServiceInterface();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceInterfaceView#isIsLeftInterface <em>Is Left Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Is Left Interface</em>'.
	 * 
	 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceInterfaceView#isIsLeftInterface()
	 * @see #getServiceInterfaceView()
	 * @generated
	 */
	EAttribute getServiceInterfaceView_IsLeftInterface();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.InputPrimitiveView <em>Input Primitive View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Input Primitive View</em>'.
	 * 
	 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.InputPrimitiveView
	 * @generated
	 */
	EClass getInputPrimitiveView();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.InputPrimitiveView#getInputPrimitive <em>Input Primitive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Input Primitive</em>'.
	 * 
	 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.InputPrimitiveView#getInputPrimitive()
	 * @see #getInputPrimitiveView()
	 * @generated
	 */
	EReference getInputPrimitiveView_InputPrimitive();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.InputPrimitiveView#getInterfaceView <em>Interface View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Interface View</em>'.
	 * 
	 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.InputPrimitiveView#getInterfaceView()
	 * @see #getInputPrimitiveView()
	 * @generated
	 */
	EReference getInputPrimitiveView_InterfaceView();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.OutputPrimitiveView <em>Output Primitive View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Output Primitive View</em>'.
	 * 
	 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.OutputPrimitiveView
	 * @generated
	 */
	EClass getOutputPrimitiveView();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.OutputPrimitiveView#getOutputPrimitive <em>Output Primitive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Output Primitive</em>'.
	 * 
	 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.OutputPrimitiveView#getOutputPrimitive()
	 * @see #getOutputPrimitiveView()
	 * @generated
	 */
	EReference getOutputPrimitiveView_OutputPrimitive();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.OutputPrimitiveView#getInterfaceView <em>Interface View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Interface View</em>'.
	 * 
	 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.OutputPrimitiveView#getInterfaceView()
	 * @see #getOutputPrimitiveView()
	 * @generated
	 */
	EReference getOutputPrimitiveView_InterfaceView();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * 
	 * @generated
	 */
	ServicesequenceFactory getServicesequenceFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->.
	 * 
	 * @generated
	 */
	interface Literals {
		
		/** The meta object literal for the '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.impl.ServiceSequenceViewImpl <em>Service Sequence View</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc --> */
		EClass SERVICE_SEQUENCE_VIEW = eINSTANCE.getServiceSequenceView();
		
		/** The meta object literal for the '<em><b>Service Sequence</b></em>' reference feature. <!-- begin-user-doc --> <!-- end-user-doc --> */
		EReference SERVICE_SEQUENCE_VIEW__SERVICE_SEQUENCE = eINSTANCE.getServiceSequenceView_ServiceSequence();
		
		/** The meta object literal for the '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.impl.ServiceInterfaceViewImpl <em>Service Interface View</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc --> */
		EClass SERVICE_INTERFACE_VIEW = eINSTANCE.getServiceInterfaceView();
		
		/** The meta object literal for the '<em><b>Service Interface</b></em>' reference feature. <!-- begin-user-doc --> <!-- end-user-doc --> */
		EReference SERVICE_INTERFACE_VIEW__SERVICE_INTERFACE = eINSTANCE.getServiceInterfaceView_ServiceInterface();
		
		/** The meta object literal for the '<em><b>Is Left Interface</b></em>' attribute feature. <!-- begin-user-doc --> <!-- end-user-doc --> */
		EAttribute SERVICE_INTERFACE_VIEW__IS_LEFT_INTERFACE = eINSTANCE.getServiceInterfaceView_IsLeftInterface();
		
		/** The meta object literal for the '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.impl.InputPrimitiveViewImpl <em>Input Primitive View</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc --> */
		EClass INPUT_PRIMITIVE_VIEW = eINSTANCE.getInputPrimitiveView();
		
		/** The meta object literal for the '<em><b>Input Primitive</b></em>' reference feature. <!-- begin-user-doc --> <!-- end-user-doc --> */
		EReference INPUT_PRIMITIVE_VIEW__INPUT_PRIMITIVE = eINSTANCE.getInputPrimitiveView_InputPrimitive();
		
		/** The meta object literal for the '<em><b>Interface View</b></em>' reference feature. <!-- begin-user-doc --> <!-- end-user-doc --> */
		EReference INPUT_PRIMITIVE_VIEW__INTERFACE_VIEW = eINSTANCE.getInputPrimitiveView_InterfaceView();
		
		/** The meta object literal for the '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.impl.OutputPrimitiveViewImpl <em>Output Primitive View</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc --> */
		EClass OUTPUT_PRIMITIVE_VIEW = eINSTANCE.getOutputPrimitiveView();
		
		/** The meta object literal for the '<em><b>Output Primitive</b></em>' reference feature. <!-- begin-user-doc --> <!-- end-user-doc --> */
		EReference OUTPUT_PRIMITIVE_VIEW__OUTPUT_PRIMITIVE = eINSTANCE.getOutputPrimitiveView_OutputPrimitive();
		
		/** The meta object literal for the '<em><b>Interface View</b></em>' reference feature. <!-- begin-user-doc --> <!-- end-user-doc --> */
		EReference OUTPUT_PRIMITIVE_VIEW__INTERFACE_VIEW = eINSTANCE.getOutputPrimitiveView_InterfaceView();

	}

} //ServicesequencePackage
