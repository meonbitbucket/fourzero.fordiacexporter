/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class ServicesequenceFactoryImpl extends EFactoryImpl implements ServicesequenceFactory {
	
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the servicesequence factory
	 * 
	 * @generated
	 */
	public static ServicesequenceFactory init() {
		try {
			ServicesequenceFactory theServicesequenceFactory = (ServicesequenceFactory)EPackage.Registry.INSTANCE.getEFactory("org.fordiac.ide.fbt.typeeditor.servicesequence.model.ui"); 
			if (theServicesequenceFactory != null) {
				return theServicesequenceFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ServicesequenceFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ServicesequenceFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @param eClass the e class
	 * 
	 * @return the e object
	 * 
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ServicesequencePackage.SERVICE_SEQUENCE_VIEW: return createServiceSequenceView();
			case ServicesequencePackage.SERVICE_INTERFACE_VIEW: return createServiceInterfaceView();
			case ServicesequencePackage.INPUT_PRIMITIVE_VIEW: return createInputPrimitiveView();
			case ServicesequencePackage.OUTPUT_PRIMITIVE_VIEW: return createOutputPrimitiveView();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the service sequence view
	 * 
	 * @generated
	 */
	public ServiceSequenceView createServiceSequenceView() {
		ServiceSequenceViewImpl serviceSequenceView = new ServiceSequenceViewImpl();
		return serviceSequenceView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the service interface view
	 * 
	 * @generated
	 */
	public ServiceInterfaceView createServiceInterfaceView() {
		ServiceInterfaceViewImpl serviceInterfaceView = new ServiceInterfaceViewImpl();
		return serviceInterfaceView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the input primitive view
	 * 
	 * @generated
	 */
	public InputPrimitiveView createInputPrimitiveView() {
		InputPrimitiveViewImpl inputPrimitiveView = new InputPrimitiveViewImpl();
		return inputPrimitiveView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the output primitive view
	 * 
	 * @generated
	 */
	public OutputPrimitiveView createOutputPrimitiveView() {
		OutputPrimitiveViewImpl outputPrimitiveView = new OutputPrimitiveViewImpl();
		return outputPrimitiveView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the servicesequence package
	 * 
	 * @generated
	 */
	public ServicesequencePackage getServicesequencePackage() {
		return (ServicesequencePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the package
	 * 
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ServicesequencePackage getPackage() {
		return ServicesequencePackage.eINSTANCE;
	}

} //ServicesequenceFactoryImpl
