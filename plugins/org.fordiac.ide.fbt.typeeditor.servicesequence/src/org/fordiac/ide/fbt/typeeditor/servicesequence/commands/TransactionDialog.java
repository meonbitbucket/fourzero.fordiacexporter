/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.commands;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.StatusDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.ServiceInterface;

/**
 * The Class TransactionDialog.
 * 
 * @author gebenh
 */
public class TransactionDialog extends StatusDialog {

	private final FBType fbType;
	private Button leftInterface;
	private Button rightInterface;
	private Text eventText;
	private Text parameterText;

	private String event;
	private String parameter;
	private ServiceInterface serviceInterface;
	
	private boolean defaultLeftInterface;

	/**
	 * Creates a new TransactionDialog.
	 * 
	 * @param parent the parent shell
	 * @param fbType the fbtype the transaction is related to
	 */
	public TransactionDialog(Shell parent, FBType fbType, boolean defaultLeftInterface) {
		super(parent);
		setShellStyle(getShellStyle() | SWT.RESIZE);
		this.fbType = fbType;
		this.defaultLeftInterface = defaultLeftInterface;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#getInitialSize()
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(400, 280);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		if (leftInterface.getSelection()) {
			serviceInterface = fbType.getService().getLeftInterface();
		} else if (rightInterface.getSelection()) {
			serviceInterface = fbType.getService().getRightInterface();
		} else {
			serviceInterface = null;
		}
		parameter = parameterText.getText();
		event = eventText.getText();
		super.okPressed();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	protected Control createDialogArea(final Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		Composite main = new Composite(composite, SWT.NONE);
		main.setLayout(new GridLayout(2, false));
		main.setLayoutData(new GridData(GridData.FILL_BOTH));

		GridData groupData = new GridData();
		groupData.horizontalSpan = 2;
		groupData.horizontalAlignment = GridData.FILL;

		Group group = new Group(main, SWT.NONE);
		group.setText("Interface");
		group.setLayout(new GridLayout());
		group.setLayoutData(groupData);

		leftInterface = new Button(group, SWT.RADIO);
		leftInterface.setText(fbType.getService().getLeftInterface().getName());

		rightInterface = new Button(group, SWT.RADIO);
		rightInterface.setText(fbType.getService().getRightInterface().getName());
		
		if(defaultLeftInterface){
			leftInterface.setSelection(true);
		}else{
			rightInterface.setSelection(true);
	    }

		Label eventLabel = new Label(main, SWT.NONE);
		eventLabel.setText("Event");

		eventText = new Text(main, SWT.BORDER);
		if (fbType.getInterfaceList() != null
				&& fbType.getInterfaceList().getEventInputs().size() > 0) {
			eventText.setText(fbType.getInterfaceList().getEventInputs().get(0)
					.getName());
		}
		GridData fillEventText = new GridData();
		fillEventText.grabExcessHorizontalSpace = true;
		fillEventText.horizontalAlignment = GridData.FILL;
		eventText.setLayoutData(fillEventText);

		Label parameterLabel = new Label(main, SWT.NONE);
		parameterLabel.setText("Parameters");

		parameterText = new Text(main, SWT.BORDER);

		GridData fillParameterText = new GridData();
		fillParameterText.grabExcessHorizontalSpace = true;
		fillParameterText.horizontalAlignment = GridData.FILL;
		parameterText.setLayoutData(fillParameterText);

		checkIsValid();
		return composite;
	}

	/**
	 * Check is valid.
	 */
	protected void checkIsValid() {
		IStatus status = new Status(Status.OK, "OK", "");
		updateStatus(status);
	}

	/**
	 * Gets the event.
	 * 
	 * @return the event specified
	 */
	public String getEvent() {
		return event;
	}

	/**
	 * Gets the parameter.
	 * 
	 * @return the parameter specified
	 */
	public String getParameter() {
		return parameter;
	}

	/**
	 * Gets the interface.
	 * 
	 * @return the interface selected
	 */
	public ServiceInterface getInterface() {
		return serviceInterface;
	}

}
