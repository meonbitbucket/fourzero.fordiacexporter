/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence;

import org.fordiac.ide.model.libraryElement.ServiceInterface;

import org.fordiac.ide.model.ui.View;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Service Interface View</b></em>'.
 * <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceInterfaceView#getServiceInterface <em>Service Interface</em>}</li>
 * <li>{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceInterfaceView#isIsLeftInterface <em>Is Left Interface</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServicesequencePackage#getServiceInterfaceView()
 * @model
 * @generated
 */
public interface ServiceInterfaceView extends View {
	
	/**
	 * Returns the value of the '<em><b>Service Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Service Interface</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Service Interface</em>' reference.
	 * 
	 * @see #setServiceInterface(ServiceInterface)
	 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServicesequencePackage#getServiceInterfaceView_ServiceInterface()
	 * @model
	 * @generated
	 */
	ServiceInterface getServiceInterface();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceInterfaceView#getServiceInterface <em>Service Interface</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value the new value of the '<em>Service Interface</em>' reference.
	 * 
	 * @see #getServiceInterface()
	 * @generated
	 */
	void setServiceInterface(ServiceInterface value);

	/**
	 * Returns the value of the '<em><b>Is Left Interface</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Left Interface</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Is Left Interface</em>' attribute.
	 * 
	 * @see #setIsLeftInterface(boolean)
	 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServicesequencePackage#getServiceInterfaceView_IsLeftInterface()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 * @generated
	 */
	boolean isIsLeftInterface();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceInterfaceView#isIsLeftInterface <em>Is Left Interface</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value the new value of the '<em>Is Left Interface</em>' attribute.
	 * 
	 * @see #isIsLeftInterface()
	 * @generated
	 */
	void setIsLeftInterface(boolean value);

} // ServiceInterfaceView
