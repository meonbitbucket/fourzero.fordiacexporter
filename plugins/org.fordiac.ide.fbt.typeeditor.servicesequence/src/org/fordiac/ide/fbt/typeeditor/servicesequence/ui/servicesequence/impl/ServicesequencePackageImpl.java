/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.InputPrimitiveView;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.OutputPrimitiveView;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceInterfaceView;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceSequenceView;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServicesequenceFactory;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServicesequencePackage;

import org.fordiac.ide.model.Palette.PalettePackage;

import org.fordiac.ide.model.data.DataPackage;

import org.fordiac.ide.model.libraryElement.LibraryElementPackage;

import org.fordiac.ide.model.ui.UiPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class ServicesequencePackageImpl extends EPackageImpl implements ServicesequencePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serviceSequenceViewEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serviceInterfaceViewEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass inputPrimitiveViewEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass outputPrimitiveViewEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServicesequencePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ServicesequencePackageImpl() {
		super(eNS_URI, ServicesequenceFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this
	 * model, and for any others upon which it depends.  Simple
	 * dependencies are satisfied by calling this method on all
	 * dependent packages before doing anything else.  This method drives
	 * initialization for interdependent packages directly, in parallel
	 * with this package, itself.
	 * <p>Of this package and its interdependencies, all packages which
	 * have not yet been registered by their URI values are first created
	 * and registered.  The packages are then initialized in two steps:
	 * meta-model objects for all of the packages are created before any
	 * are initialized, since one package's meta-model objects may refer to
	 * those of another.
	 * <p>Invocation of this method will not affect any packages that have
	 * already been initialized.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the servicesequence package
	 * 
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ServicesequencePackage init() {
		if (isInited) return (ServicesequencePackage)EPackage.Registry.INSTANCE.getEPackage(ServicesequencePackage.eNS_URI);

		// Obtain or create and register package
		ServicesequencePackageImpl theServicesequencePackage = (ServicesequencePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(eNS_URI) instanceof ServicesequencePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(eNS_URI) : new ServicesequencePackageImpl());

		isInited = true;

		// Initialize simple dependencies
		PalettePackage.eINSTANCE.eClass();
		LibraryElementPackage.eINSTANCE.eClass();
		DataPackage.eINSTANCE.eClass();
		UiPackage.eINSTANCE.eClass();
		XMLTypePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theServicesequencePackage.createPackageContents();

		// Initialize created meta-data
		theServicesequencePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theServicesequencePackage.freeze();

		return theServicesequencePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the service sequence view
	 * 
	 * @generated
	 */
	public EClass getServiceSequenceView() {
		return serviceSequenceViewEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the service sequence view_ service sequence
	 * 
	 * @generated
	 */
	public EReference getServiceSequenceView_ServiceSequence() {
		return (EReference)serviceSequenceViewEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the service interface view
	 * 
	 * @generated
	 */
	public EClass getServiceInterfaceView() {
		return serviceInterfaceViewEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the service interface view_ service interface
	 * 
	 * @generated
	 */
	public EReference getServiceInterfaceView_ServiceInterface() {
		return (EReference)serviceInterfaceViewEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the service interface view_ is left interface
	 * 
	 * @generated
	 */
	public EAttribute getServiceInterfaceView_IsLeftInterface() {
		return (EAttribute)serviceInterfaceViewEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the input primitive view
	 * 
	 * @generated
	 */
	public EClass getInputPrimitiveView() {
		return inputPrimitiveViewEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the input primitive view_ input primitive
	 * 
	 * @generated
	 */
	public EReference getInputPrimitiveView_InputPrimitive() {
		return (EReference)inputPrimitiveViewEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the input primitive view_ interface view
	 * 
	 * @generated
	 */
	public EReference getInputPrimitiveView_InterfaceView() {
		return (EReference)inputPrimitiveViewEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the output primitive view
	 * 
	 * @generated
	 */
	public EClass getOutputPrimitiveView() {
		return outputPrimitiveViewEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the output primitive view_ output primitive
	 * 
	 * @generated
	 */
	public EReference getOutputPrimitiveView_OutputPrimitive() {
		return (EReference)outputPrimitiveViewEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the output primitive view_ interface view
	 * 
	 * @generated
	 */
	public EReference getOutputPrimitiveView_InterfaceView() {
		return (EReference)outputPrimitiveViewEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the servicesequence factory
	 * 
	 * @generated
	 */
	public ServicesequenceFactory getServicesequenceFactory() {
		return (ServicesequenceFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		serviceSequenceViewEClass = createEClass(SERVICE_SEQUENCE_VIEW);
		createEReference(serviceSequenceViewEClass, SERVICE_SEQUENCE_VIEW__SERVICE_SEQUENCE);

		serviceInterfaceViewEClass = createEClass(SERVICE_INTERFACE_VIEW);
		createEReference(serviceInterfaceViewEClass, SERVICE_INTERFACE_VIEW__SERVICE_INTERFACE);
		createEAttribute(serviceInterfaceViewEClass, SERVICE_INTERFACE_VIEW__IS_LEFT_INTERFACE);

		inputPrimitiveViewEClass = createEClass(INPUT_PRIMITIVE_VIEW);
		createEReference(inputPrimitiveViewEClass, INPUT_PRIMITIVE_VIEW__INPUT_PRIMITIVE);
		createEReference(inputPrimitiveViewEClass, INPUT_PRIMITIVE_VIEW__INTERFACE_VIEW);

		outputPrimitiveViewEClass = createEClass(OUTPUT_PRIMITIVE_VIEW);
		createEReference(outputPrimitiveViewEClass, OUTPUT_PRIMITIVE_VIEW__OUTPUT_PRIMITIVE);
		createEReference(outputPrimitiveViewEClass, OUTPUT_PRIMITIVE_VIEW__INTERFACE_VIEW);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		UiPackage theUiPackage = (UiPackage)EPackage.Registry.INSTANCE.getEPackage(UiPackage.eNS_URI);
		LibraryElementPackage theLibraryElementPackage = (LibraryElementPackage)EPackage.Registry.INSTANCE.getEPackage(LibraryElementPackage.eNS_URI);
		XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		serviceSequenceViewEClass.getESuperTypes().add(theUiPackage.getView());
		serviceInterfaceViewEClass.getESuperTypes().add(theUiPackage.getView());
		inputPrimitiveViewEClass.getESuperTypes().add(theUiPackage.getView());
		outputPrimitiveViewEClass.getESuperTypes().add(theUiPackage.getView());

		// Initialize classes and features; add operations and parameters
		initEClass(serviceSequenceViewEClass, ServiceSequenceView.class, "ServiceSequenceView", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getServiceSequenceView_ServiceSequence(), theLibraryElementPackage.getServiceSequence(), null, "serviceSequence", null, 0, 1, ServiceSequenceView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(serviceInterfaceViewEClass, ServiceInterfaceView.class, "ServiceInterfaceView", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getServiceInterfaceView_ServiceInterface(), theLibraryElementPackage.getServiceInterface(), null, "serviceInterface", null, 0, 1, ServiceInterfaceView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getServiceInterfaceView_IsLeftInterface(), theXMLTypePackage.getBoolean(), "isLeftInterface", null, 0, 1, ServiceInterfaceView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(inputPrimitiveViewEClass, InputPrimitiveView.class, "InputPrimitiveView", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInputPrimitiveView_InputPrimitive(), theLibraryElementPackage.getInputPrimitive(), null, "inputPrimitive", null, 0, 1, InputPrimitiveView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInputPrimitiveView_InterfaceView(), this.getServiceInterfaceView(), null, "interfaceView", null, 0, 1, InputPrimitiveView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(outputPrimitiveViewEClass, OutputPrimitiveView.class, "OutputPrimitiveView", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOutputPrimitiveView_OutputPrimitive(), theLibraryElementPackage.getOutputPrimitive(), null, "outputPrimitive", null, 0, 1, OutputPrimitiveView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOutputPrimitiveView_InterfaceView(), this.getServiceInterfaceView(), null, "interfaceView", null, 0, 1, OutputPrimitiveView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //ServicesequencePackageImpl
