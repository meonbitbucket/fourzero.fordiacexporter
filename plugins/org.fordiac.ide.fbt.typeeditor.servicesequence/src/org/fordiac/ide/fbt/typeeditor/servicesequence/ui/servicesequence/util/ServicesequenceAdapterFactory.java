/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.*;

import org.fordiac.ide.model.ui.View;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * 
 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServicesequencePackage
 * @generated
 */
public class ServicesequenceAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ServicesequencePackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ServicesequenceAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ServicesequencePackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * 
	 * @param object the object
	 * 
	 * @return whether this factory is applicable for the type of the object.
	 * 
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch the delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServicesequenceSwitch<Adapter> modelSwitch =
		new ServicesequenceSwitch<Adapter>() {
			@Override
			public Adapter caseServiceSequenceView(ServiceSequenceView object) {
				return createServiceSequenceViewAdapter();
			}
			@Override
			public Adapter caseServiceInterfaceView(ServiceInterfaceView object) {
				return createServiceInterfaceViewAdapter();
			}
			@Override
			public Adapter caseInputPrimitiveView(InputPrimitiveView object) {
				return createInputPrimitiveViewAdapter();
			}
			@Override
			public Adapter caseOutputPrimitiveView(OutputPrimitiveView object) {
				return createOutputPrimitiveViewAdapter();
			}
			@Override
			public Adapter caseView(View object) {
				return createViewAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param target the object to adapt.
	 * 
	 * @return the adapter for the <code>target</code>.
	 * 
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceSequenceView <em>Service Sequence View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * 
	 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceSequenceView
	 * @generated
	 */
	public Adapter createServiceSequenceViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceInterfaceView <em>Service Interface View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * 
	 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceInterfaceView
	 * @generated
	 */
	public Adapter createServiceInterfaceViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.InputPrimitiveView <em>Input Primitive View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * 
	 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.InputPrimitiveView
	 * @generated
	 */
	public Adapter createInputPrimitiveViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.OutputPrimitiveView <em>Output Primitive View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * 
	 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.OutputPrimitiveView
	 * @generated
	 */
	public Adapter createOutputPrimitiveViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.View <em>View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * 
	 * @see org.fordiac.ide.model.ui.View
	 * @generated
	 */
	public Adapter createViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * 
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ServicesequenceAdapterFactory
