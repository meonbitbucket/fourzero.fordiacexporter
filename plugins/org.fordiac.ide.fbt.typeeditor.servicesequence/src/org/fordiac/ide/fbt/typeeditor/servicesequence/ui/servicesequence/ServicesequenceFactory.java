/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * 
 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServicesequencePackage
 * @generated
 */
public interface ServicesequenceFactory extends EFactory {
	
	/** The singleton instance of the factory. <!-- begin-user-doc --> <!-- end-user-doc --> */
	ServicesequenceFactory eINSTANCE = org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.impl.ServicesequenceFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Service Sequence View</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Service Sequence View</em>'.
	 * 
	 * @generated
	 */
	ServiceSequenceView createServiceSequenceView();

	/**
	 * Returns a new object of class '<em>Service Interface View</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Service Interface View</em>'.
	 * 
	 * @generated
	 */
	ServiceInterfaceView createServiceInterfaceView();

	/**
	 * Returns a new object of class '<em>Input Primitive View</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Input Primitive View</em>'.
	 * 
	 * @generated
	 */
	InputPrimitiveView createInputPrimitiveView();

	/**
	 * Returns a new object of class '<em>Output Primitive View</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Output Primitive View</em>'.
	 * 
	 * @generated
	 */
	OutputPrimitiveView createOutputPrimitiveView();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the package supported by this factory.
	 * 
	 * @generated
	 */
	ServicesequencePackage getServicesequencePackage();

} //ServicesequenceFactory
