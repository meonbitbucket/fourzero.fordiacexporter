/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.editparts;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FlowLayout;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.fordiac.ide.fbt.typeeditor.servicesequence.sequence.SequenceLayoutEditPolicy;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceInterfaceView;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceSequenceView;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServicesequenceFactory;
import org.fordiac.ide.gef.editparts.AbstractViewEditPart;
import org.fordiac.ide.gef.figures.VerticalLineFigure;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.libraryElement.ServiceTransaction;
import org.fordiac.ide.model.ui.InterfaceElementView;

/**
 * The Class ServiceSequenceEditPart.
 */
public class ServiceSequenceEditPart extends AbstractViewEditPart {

	private final Figure centerFigure = new Figure();

	private class ServiceSequenceFigure extends Shape {

		private final Label name;

		private final Figure leftContainer = new Figure();
		private final Figure rightContainer = new Figure();

		private final Figure transactionContainer;

		public ServiceSequenceFigure() {

			GridLayout mainLayout;
			setLayoutManager(mainLayout = new GridLayout(3, false));
			mainLayout.marginHeight = 0;
			mainLayout.verticalSpacing = 0;

			GridLayout gl;
			centerFigure.setLayoutManager(gl = new GridLayout(3, false));
			gl.horizontalSpacing = 0;
			gl.verticalSpacing = 0;
			gl.marginHeight = 0;
			gl.marginWidth = 0;

			VerticalLineFigure lineLeft;
			VerticalLineFigure lineRight;

			GridData spaceData = new GridData();
			spaceData.widthHint = 35;
			Figure space = new Figure();

			centerFigure.add(lineLeft = new VerticalLineFigure(21));
			centerFigure.add(space);
			centerFigure.add(lineRight = new VerticalLineFigure(21));

			centerFigure.setConstraint(space, spaceData);

			GridData fillDataLeft = new GridData();
			fillDataLeft.grabExcessVerticalSpace = true;
			fillDataLeft.verticalAlignment = GridData.FILL;

			// fillDataLeft.widthHint = 20;

			GridData fillDataRight = new GridData();
			fillDataRight.grabExcessVerticalSpace = true;
			fillDataRight.verticalAlignment = GridData.FILL;
			// fillDataRight.widthHint = 20;

			centerFigure.setConstraint(lineLeft, fillDataLeft);
			centerFigure.setConstraint(lineRight, fillDataRight);

			GridData leftContainerData = new GridData();
			leftContainerData.widthHint = 150;
			// leftContainerData.grabExcessHorizontalSpace = true;
			leftContainerData.horizontalAlignment = GridData.FILL;

			GridData rightContainerData = new GridData();
			rightContainerData.widthHint = 150;
			// rightContainerData.grabExcessHorizontalSpace = true;
			rightContainerData.horizontalAlignment = GridData.FILL;

			GridData nameLayoutData = new GridData();
			nameLayoutData.horizontalSpan = 3;
			nameLayoutData.heightHint = 30;
			nameLayoutData.grabExcessHorizontalSpace = false;
			nameLayoutData.horizontalAlignment = GridData.FILL;

			name = new Label(getCastedModel().getServiceSequence().getName());
			add(name);
			setConstraint(name, nameLayoutData);

			GridLayout leftLayout = new GridLayout();
			leftLayout.verticalSpacing = 0;
			leftLayout.marginHeight = 0;
			GridLayout rightLayout = new GridLayout();
			rightLayout.verticalSpacing = 0;
			rightLayout.marginHeight = 0;
			add(leftContainer);
			leftContainer.setLayoutManager(leftLayout);
			setConstraint(leftContainer, leftContainerData);
			add(centerFigure);
			add(rightContainer);
			rightContainer.setLayoutManager(rightLayout);
			setConstraint(rightContainer, rightContainerData);

			GridData containerData = new GridData();
			containerData.horizontalSpan = 3;

			transactionContainer = new Figure();
			FlowLayout fl = new FlowLayout(false);
			fl.setMinorSpacing(0);
			transactionContainer.setLayoutManager(fl);

			add(transactionContainer);
			setConstraint(transactionContainer, containerData);

			setSize(-1, -1);

		}

		@Override
		protected void fillShape(final Graphics graphics) {
		}

		@Override
		protected void outlineShape(final Graphics graphics) {
		}

		public Label getName() {
			return name;
		}

		public Figure getLeftContainer() {
			return leftContainer;
		}

		public Figure getRightContainer() {
			return rightContainer;
		}

		public Figure getTransactionContainer() {
			return transactionContainer;
		}

	}

	/** The adapter. */
	EContentAdapter adapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {
			super.notifyChanged(notification);
			refreshVisuals();
		}

	};

	private final EContentAdapter childrenAdapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {
			super.notifyChanged(notification);
			if (getParent() != null && getRoot() != null) {
				refreshChildren();
			}
		}

	};

	/**
	 * returns the typed version of the model.
	 * 
	 * @return the model object as ServiceSequenceView
	 */
	public ServiceSequenceView getCastedModel() {
		return (ServiceSequenceView) getModel();
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#activate()
	 */
	@Override
	public void activate() {
		super.activate();
		if (getCastedModel() != null
				&& getCastedModel().getServiceSequence() != null) {
			getCastedModel().getServiceSequence().eAdapters().add(childrenAdapter);
		}
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#deactivate()
	 */
	@Override
	public void deactivate() {
		super.deactivate();

		if (getCastedModel() != null
				&& getCastedModel().getServiceSequence() != null) {
			getCastedModel().getServiceSequence().eAdapters().remove(childrenAdapter);
		}
	}

	/** The l. */
	Label l = new Label();

	@Override
	protected IFigure createFigureForModel() {

		return new ServiceSequenceFigure();
	}

	/** The model children. */
	ArrayList<Object> modelChildren = new ArrayList<Object>();

	@SuppressWarnings("rawtypes")
	@Override
	protected List getModelChildren() {
		modelChildren = new ArrayList<Object>();
		if (getParent() instanceof SequenceRootEditPart) {
			FBType type = getFBType();

			ServiceInterfaceView leftInterfaceView = ServicesequenceFactory.eINSTANCE
					.createServiceInterfaceView();
			leftInterfaceView.setIsLeftInterface(true);
			leftInterfaceView.setServiceInterface(type.getService().getLeftInterface());

			ServiceInterfaceView rightInterfaceView = ServicesequenceFactory.eINSTANCE
					.createServiceInterfaceView();
			rightInterfaceView.setIsLeftInterface(false);
			rightInterfaceView.setServiceInterface(type.getService().getRightInterface());

			modelChildren.add(leftInterfaceView);
			modelChildren.add(rightInterfaceView);
			for (Iterator<ServiceTransaction> iterator = getCastedModel().getServiceSequence()
					.getServiceTransaction().iterator(); iterator.hasNext();) {
				ServiceTransaction trans = (ServiceTransaction) iterator.next();
				TransactionElement element = new TransactionElement(trans, type,
						leftInterfaceView, rightInterfaceView);
				modelChildren.add(element);
			}
		}

		return modelChildren;
	}

	private FBType getFBType() {
		SequenceRootEditPart ep = (SequenceRootEditPart) getParent();
		FBType type = ep.getCastedModel();
		return type;
	}

	@Override
	protected void addChildVisual(final EditPart childEditPart, final int index) {
		IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
		if (childEditPart instanceof ServiceInterfaceEditPart) {
			if (((ServiceInterfaceEditPart) childEditPart).getCastedModel()
					.isIsLeftInterface()) {
				((ServiceSequenceFigure) getFigure()).getLeftContainer().add(child);
				GridData nameData = new GridData();
				nameData.grabExcessHorizontalSpace = true;
				nameData.horizontalAlignment = GridData.FILL;
				((ServiceSequenceFigure) getFigure()).getLeftContainer().setConstraint(
						child, nameData);

			} else {
				((ServiceSequenceFigure) getFigure()).getRightContainer().add(child);
				GridData nameData = new GridData();
				nameData.grabExcessHorizontalSpace = true;
				nameData.horizontalAlignment = GridData.FILL;
				((ServiceSequenceFigure) getFigure()).getRightContainer()
						.setConstraint(child, nameData);

			}
			// }
		} else if (childEditPart instanceof TransactionEditPart) {
			((ServiceSequenceFigure) getFigure()).getTransactionContainer()
					.add(child);
		} else {
			super.addChildVisual(childEditPart, index);
		}
	}

	@Override
	protected void removeChildVisual(final EditPart childEditPart) {
		IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
		if (childEditPart instanceof ServiceInterfaceEditPart) {
			if (((ServiceInterfaceEditPart) childEditPart).getCastedModel()
					.isIsLeftInterface()) {
				((ServiceSequenceFigure) getFigure()).getLeftContainer().remove(child);
			} else {
				((ServiceSequenceFigure) getFigure()).getRightContainer().remove(child);

			}
			// }
		} else if (childEditPart instanceof TransactionEditPart) {
			((ServiceSequenceFigure) getFigure()).getTransactionContainer().remove(
					child);
		} else {
			super.removeChildVisual(childEditPart);
		}
	}

	@Override
	protected void createEditPolicies() {
		super.createEditPolicies();
		installEditPolicy(EditPolicy.LAYOUT_ROLE, new SequenceLayoutEditPolicy());

	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#getContentPane()
	 */
	@Override
	public IFigure getContentPane() {
		return ((ServiceSequenceFigure) getFigure()).getTransactionContainer();
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getContentAdapter()
	 */
	@Override
	public EContentAdapter getContentAdapter() {
		return adapter;
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getINamedElement()
	 */
	@Override
	public INamedElement getINamedElement() {
		return getCastedModel().getServiceSequence();
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getInterfaceElements()
	 */
	@Override
	public List<? extends InterfaceElementView> getInterfaceElements() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getNameLabel()
	 */
	@Override
	public Label getNameLabel() {
		return ((ServiceSequenceFigure) getFigure()).getName();
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getPreferenceChangeListener()
	 */
	@Override
	public IPropertyChangeListener getPreferenceChangeListener() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.views.properties.IPropertySource#getEditableValue()
	 */
	@Override
	public Object getEditableValue() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.views.properties.IPropertySource#getPropertyValue(java.lang.Object)
	 */
	@Override
	public Object getPropertyValue(final Object id) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.views.properties.IPropertySource#isPropertySet(java.lang.Object)
	 */
	@Override
	public boolean isPropertySet(final Object id) {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.views.properties.IPropertySource#resetPropertyValue(java.lang.Object)
	 */
	@Override
	public void resetPropertyValue(final Object id) {
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.views.properties.IPropertySource#setPropertyValue(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void setPropertyValue(final Object id, final Object value) {
	}

	@Override
	protected void refreshVisuals() {
		super.refreshVisuals();
		Rectangle bounds = new Rectangle(0, 0, -1, -1);
		// } else {
		// bounds = new Rectangle()
		// }
		((GraphicalEditPart) getParent()).setLayoutConstraint(this, getFigure(),
				bounds);

	}

}
