/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.editparts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.gef.editparts.AbstractConnectionEditPart;

/**
 * The Class ConnectionEditPart.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class ConnectingConnectionEditPart extends AbstractConnectionEditPart {

	/**
	 * Gets the casted model.
	 * 
	 * @return the casted model
	 */
	public PrimitiveConnection getCastedModel() {
		return (PrimitiveConnection) getModel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		// // Selection handle edit policy.
		// // Makes the connection show a feedback, when selected by the user.
		// installEditPolicy(EditPolicy.CONNECTION_ENDPOINTS_ROLE,
		// new FeedbackConnectionEndpointEditPolicy());
		//
		// // Allows the removal of the connection model element
		// installEditPolicy(EditPolicy.CONNECTION_ROLE,
		// new DeleteConnectionEditPolicy());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractConnectionEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {

		// PointList pl = new PointList();
		// pl.addPoint(-1, -1);
		// pl.addPoint(-1, 1);
		// pl.addPoint(1, 1);
		// pl.addPoint(1, -1);
		// pl.addPoint(-1, -1);
		//
		PolylineConnection connection = (PolylineConnection) super
				.createFigure();
		// PolygonDecoration rect = new PolygonDecoration();
		// rect.setTemplate(pl);
		// rect.setScale(4, 4);
		//
		// connection.setTargetDecoration(rect);
		// PolygonDecoration rect2 = new PolygonDecoration();
		// rect2.setTemplate(pl);
		// rect2.setScale(4, 4);

		// connection.setTargetDecoration(rect);
		//
		// connection.setSourceDecoration(rect2);

		// if (getCastedModel().getConnectionElement() instanceof
		// EventConnection) {
		// connection.setForegroundColor(PreferenceGetter
		// .getColor(PreferenceConstants.P_EVENT_CONNECTOR_COLOR));
		// }
		//
		// if (getCastedModel().getConnectionElement() instanceof
		// DataConnection) {
		// connection.setForegroundColor(PreferenceGetter
		// .getColor(PreferenceConstants.P_DATA_CONNECTOR_COLOR));
		// }

		return connection;
	}
}
