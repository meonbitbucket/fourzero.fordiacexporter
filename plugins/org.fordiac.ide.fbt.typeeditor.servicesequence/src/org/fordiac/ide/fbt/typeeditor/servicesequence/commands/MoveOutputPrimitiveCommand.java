/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.OutputPrimitive;
import org.fordiac.ide.model.libraryElement.ServiceTransaction;

public class MoveOutputPrimitiveCommand extends Command {

	private ServiceTransaction transaction;
	private OutputPrimitive element;
	private OutputPrimitive refElement;
	
	public MoveOutputPrimitiveCommand(ServiceTransaction transaction,
			OutputPrimitive element, OutputPrimitive refElement) {
		this.transaction = transaction;
		this.element = element;
		this.refElement = refElement;
	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		return ((transaction != null) && (element != null));
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		
		if(null == refElement){
			transaction.getOutputPrimitive().move(transaction.getOutputPrimitive().size() - 1, element);
		}
		else{
			int index = transaction.getOutputPrimitive().indexOf(refElement);
			transaction.getOutputPrimitive().move(index, element);
		}
		super.execute();
	}
	
}
