/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.editparts;

import java.text.MessageFormat;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.fordiac.ide.fbt.typeeditor.Activator;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.InputPrimitiveView;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.OutputPrimitiveView;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceInterfaceView;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceSequenceView;
import org.fordiac.ide.model.libraryElement.FBType;

/**
 * A factory for creating FBInterfaceEditPart objects.
 */
public class ServiceSequenceEditPartFactory implements EditPartFactory {

	/**
	 * Instantiates a new service sequence edit part factory.
	 * 
	 * @param editor the editor
	 */
	public ServiceSequenceEditPartFactory() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.EditPartFactory#createEditPart(org.eclipse.gef.EditPart,
	 * java.lang.Object)
	 */
	public EditPart createEditPart(final EditPart context,
			final Object modelElement) {
		// get EditPart for model element
		EditPart part = null;
		try {
			part = getPartForElement(context, modelElement);
			part.setModel(modelElement);
		} catch (RuntimeException e) {
			Activator.getDefault().logError(e.getMessage(), e);

		}
		// store model element in EditPart
		return part;
	}

	/**
	 * Maps an object to an EditPart.
	 * 
	 * @param context
	 *          the context
	 * @param modelElement
	 *          the model element
	 * 
	 * @return the part for element
	 * 
	 * @throws RuntimeException
	 *           if no match was found (programming error)
	 */
	protected EditPart getPartForElement(final EditPart context,
			final Object modelElement) {

		if (modelElement instanceof FBType) {
			return new SequenceRootEditPart();
		}
		if (modelElement instanceof ServiceSequenceView) {
			return new ServiceSequenceEditPart();
		}
		if (modelElement instanceof ServiceInterfaceView) {
			return new ServiceInterfaceEditPart();
		}

		if (modelElement instanceof TransactionElement) {
			return new TransactionEditPart();
		}

		if (modelElement instanceof InputPrimitiveView) {
			return new InputPrimitiveEditPart();
		}

		if (modelElement instanceof OutputPrimitiveView) {
			return new OutputPrimitiveEditPart();
		}
		if (modelElement instanceof PrimitiveConnection) {
			return new PrimitiveConnectionEditPart();
		}
		if (modelElement instanceof ConnectingConnection) {
			return new ConnectingConnectionEditPart();
		}
		throw new RuntimeException(MessageFormat.format(
				"Can't create part for model ...",
				new Object[] { ((modelElement != null) ? modelElement.getClass()
						.getName() : "null") })); //$NON-NLS-1$
	}
}
