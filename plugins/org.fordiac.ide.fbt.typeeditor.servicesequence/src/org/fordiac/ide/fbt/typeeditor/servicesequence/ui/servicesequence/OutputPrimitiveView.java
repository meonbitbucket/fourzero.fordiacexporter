/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence;

import org.fordiac.ide.model.libraryElement.OutputPrimitive;

import org.fordiac.ide.model.ui.View;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Output Primitive View</b></em>'.
 * <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.OutputPrimitiveView#getOutputPrimitive <em>Output Primitive</em>}</li>
 * <li>{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.OutputPrimitiveView#getInterfaceView <em>Interface View</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServicesequencePackage#getOutputPrimitiveView()
 * @model
 * @generated
 */
public interface OutputPrimitiveView extends View {
	
	/**
	 * Returns the value of the '<em><b>Output Primitive</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Primitive</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Output Primitive</em>' reference.
	 * 
	 * @see #setOutputPrimitive(OutputPrimitive)
	 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServicesequencePackage#getOutputPrimitiveView_OutputPrimitive()
	 * @model
	 * @generated
	 */
	OutputPrimitive getOutputPrimitive();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.OutputPrimitiveView#getOutputPrimitive <em>Output Primitive</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value the new value of the '<em>Output Primitive</em>' reference.
	 * 
	 * @see #getOutputPrimitive()
	 * @generated
	 */
	void setOutputPrimitive(OutputPrimitive value);

	/**
	 * Returns the value of the '<em><b>Interface View</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interface View</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Interface View</em>' reference.
	 * 
	 * @see #setInterfaceView(ServiceInterfaceView)
	 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServicesequencePackage#getOutputPrimitiveView_InterfaceView()
	 * @model
	 * @generated
	 */
	ServiceInterfaceView getInterfaceView();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.OutputPrimitiveView#getInterfaceView <em>Interface View</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value the new value of the '<em>Interface View</em>' reference.
	 * 
	 * @see #getInterfaceView()
	 * @generated
	 */
	void setInterfaceView(ServiceInterfaceView value);

} // OutputPrimitiveView
