/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.sequence;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.FlowLayoutEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.fordiac.ide.fbt.typeeditor.servicesequence.commands.CreateOutputPrimitiveCommand;
import org.fordiac.ide.fbt.typeeditor.servicesequence.commands.CreatePrimitiveCommand;
import org.fordiac.ide.fbt.typeeditor.servicesequence.commands.MoveInputPrimitiveToOtherTransactionCommand;
import org.fordiac.ide.fbt.typeeditor.servicesequence.commands.MoveOutputPrimitiveCommand;
import org.fordiac.ide.fbt.typeeditor.servicesequence.commands.MoveOutputPrimitiveToOtherTransactionCommand;
import org.fordiac.ide.fbt.typeeditor.servicesequence.editparts.InputPrimitiveEditPart;
import org.fordiac.ide.fbt.typeeditor.servicesequence.editparts.OutputPrimitiveEditPart;
import org.fordiac.ide.fbt.typeeditor.servicesequence.editparts.TransactionElement;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.InputPrimitiveView;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.OutputPrimitiveView;
import org.fordiac.ide.model.libraryElement.InputPrimitive;
import org.fordiac.ide.model.libraryElement.OutputPrimitive;
import org.fordiac.ide.model.libraryElement.ServiceTransaction;

/**
 * The Class EventInputContainerLayoutEditPolicy.
 */
public class TransactionLayoutEditPolicy extends FlowLayoutEditPolicy {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.OrderedLayoutEditPolicy#createAddCommand(org.eclipse.gef.EditPart,
	 *      org.eclipse.gef.EditPart)
	 */
	@Override
	protected Command createAddCommand(final EditPart child,
			final EditPart after) {
		
		TransactionElement target = (TransactionElement)getHost().getModel();
		
		if((child instanceof OutputPrimitiveEditPart) && 
				((after != null) ||				//we are out in the first place or if we are put in the first place there is no inputprimitive 
						((after == null) && (target.getTransaction().getInputPrimitive() == null)))){

			OutputPrimitive element = ((OutputPrimitiveView)child.getModel()).getOutputPrimitive();
			
			OutputPrimitive refElement = null;
						
			if(after != null){
					if(after instanceof OutputPrimitiveEditPart){
						refElement = ((OutputPrimitiveView)after.getModel()).getOutputPrimitive();
					}else if (after instanceof InputPrimitiveEditPart){
						if(0 < target.getTransaction().getOutputPrimitive().size()){
							refElement = target.getTransaction().getOutputPrimitive().get(0);
						}
					}
					else{
						refElement = null;
					}				
			}
			
			return new MoveOutputPrimitiveToOtherTransactionCommand((ServiceTransaction)element.eContainer(), target.getTransaction(), element, refElement);
		}
		
		if(child instanceof InputPrimitiveEditPart){
			InputPrimitive element = ((InputPrimitiveView)child.getModel()).getInputPrimitive();
			if(null == target.getTransaction().getInputPrimitive()){
				//we can only move into a transaction without input primitive
				if(after != null){
					if(after instanceof OutputPrimitiveEditPart){
						OutputPrimitive refElement = ((OutputPrimitiveView)after.getModel()).getOutputPrimitive();
						if(0 == target.getTransaction().getOutputPrimitive().indexOf(refElement)){
							return new MoveInputPrimitiveToOtherTransactionCommand((ServiceTransaction)element.eContainer(), target.getTransaction(), element);
						}
					}
				}
				else{
					if(0 == target.getTransaction().getOutputPrimitive().size()){
						return new MoveInputPrimitiveToOtherTransactionCommand((ServiceTransaction)element.eContainer(), target.getTransaction(), element);
					}
				}
			}
					
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.OrderedLayoutEditPolicy#createMoveChildCommand(org.eclipse.gef.EditPart,
	 *      org.eclipse.gef.EditPart)
	 */
	@Override
	protected Command createMoveChildCommand(final EditPart child,
			final EditPart after) {

		TransactionElement target = (TransactionElement)getHost().getModel();
		
		if((child instanceof OutputPrimitiveEditPart) && 
				((after != null) ||				//we are out in the first place or if we are put in the first place there is no inputprimitive 
						((after == null) && (target.getTransaction().getInputPrimitive() == null)))){

			OutputPrimitive element = ((OutputPrimitiveView)child.getModel()).getOutputPrimitive();
			
			OutputPrimitive refElement = null;
						
			if(after != null){
				if(after instanceof OutputPrimitiveEditPart){
					refElement = ((OutputPrimitiveView)after.getModel()).getOutputPrimitive();
				}else if (after instanceof InputPrimitiveEditPart){
					if(0 < target.getTransaction().getOutputPrimitive().size()){
						refElement = target.getTransaction().getOutputPrimitive().get(0);
					}
				}
				else{
					refElement = null;
				}				
			}
			
			return new MoveOutputPrimitiveCommand(target.getTransaction(), element, refElement);
		}
			
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.LayoutEditPolicy#getCreateCommand(org.eclipse.gef.requests.CreateRequest)
	 */
	@Override
	protected Command getCreateCommand(final CreateRequest request) {

		Object type = request.getNewObjectType();
		Object model = getHost().getModel();
		

		if (type.equals("LeftInputPrimitive")
				|| type.equals("RightInputPrimitive")){
			CreatePrimitiveCommand cmd = new CreatePrimitiveCommand(
					(String) type, (TransactionElement) model);
			return cmd;
		}
		else if(type.equals("LeftOutputPrimitive")
			     || type.equals("RightOutputPrimitive")) {
			OutputPrimitive refPrimitiv = null;	
			if(null != getInsertionReference(request)){
				if(getInsertionReference(request).getModel() instanceof InputPrimitiveView){
					// we can not be above the input primitive
					return null;
				}
				if(getInsertionReference(request).getModel() instanceof OutputPrimitiveView){
					refPrimitiv = ((OutputPrimitiveView)getInsertionReference(request).getModel()).getOutputPrimitive();
				}
			}
			return new CreateOutputPrimitiveCommand(
					(String) type, (TransactionElement) model, refPrimitiv);
		}
		return null;
	}
}
