/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ServiceInterfacePaletteFactory;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.OutputPrimitive;
import org.fordiac.ide.model.libraryElement.ServiceInterface;
import org.fordiac.ide.model.libraryElement.ServiceTransaction;

/**
 * The Class SimpleCreatePrimitiveCommand.
 */
public class SimpleCreatePrimitiveCommand extends Command {

	private final String type;

	private final ServiceInterface serviceInterface;
	private final ServiceTransaction serviceTransaction;

	/**
	 * Instantiates a new simple create primitive command.
	 * 
	 * @param type the type
	 * @param serviceInterface the service interface
	 * @param transaction the transaction
	 */
	public SimpleCreatePrimitiveCommand(String type,
			ServiceInterface serviceInterface, ServiceTransaction transaction) {
		this.type = type;
		this.serviceInterface = serviceInterface;
		this.serviceTransaction = transaction;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		if (type == null || serviceInterface == null) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {

		if (type.equals(ServiceInterfacePaletteFactory.LEFT_OUTPUT_PRIMITIVE)) {
			OutputPrimitive outputPrimitive = LibraryElementFactory.eINSTANCE
					.createOutputPrimitive();
			outputPrimitive.setInterface(serviceInterface);
			outputPrimitive.setEvent("INITO");
			serviceTransaction.getOutputPrimitive().add(outputPrimitive);

		} else if (type
				.equals(ServiceInterfacePaletteFactory.RIGHT_OUTPUT_PRIMITIVE)) {
			OutputPrimitive outputPrimitive = LibraryElementFactory.eINSTANCE
					.createOutputPrimitive();
			outputPrimitive.setInterface(serviceInterface);
			outputPrimitive.setEvent("INITO");
			serviceTransaction.getOutputPrimitive().add(outputPrimitive);

		}

		super.execute();
	}
}
