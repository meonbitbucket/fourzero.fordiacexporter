/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence;

import org.fordiac.ide.model.libraryElement.InputPrimitive;
import org.fordiac.ide.model.ui.View;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Input Primitive View</b></em>'.
 * <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.InputPrimitiveView#getInputPrimitive <em>Input Primitive</em>}</li>
 * <li>{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.InputPrimitiveView#getInterfaceView <em>Interface View</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServicesequencePackage#getInputPrimitiveView()
 * @model
 * @generated
 */
public interface InputPrimitiveView extends View {
	
	/**
	 * Returns the value of the '<em><b>Input Primitive</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Primitive</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Input Primitive</em>' reference.
	 * 
	 * @see #setInputPrimitive(InputPrimitive)
	 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServicesequencePackage#getInputPrimitiveView_InputPrimitive()
	 * @model
	 * @generated
	 */
	InputPrimitive getInputPrimitive();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.InputPrimitiveView#getInputPrimitive <em>Input Primitive</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value the new value of the '<em>Input Primitive</em>' reference.
	 * 
	 * @see #getInputPrimitive()
	 * @generated
	 */
	void setInputPrimitive(InputPrimitive value);

	/**
	 * Returns the value of the '<em><b>Interface View</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interface View</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Interface View</em>' reference.
	 * 
	 * @see #setInterfaceView(ServiceInterfaceView)
	 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServicesequencePackage#getInputPrimitiveView_InterfaceView()
	 * @model
	 * @generated
	 */
	ServiceInterfaceView getInterfaceView();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.InputPrimitiveView#getInterfaceView <em>Interface View</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value the new value of the '<em>Interface View</em>' reference.
	 * 
	 * @see #getInterfaceView()
	 * @generated
	 */
	void setInterfaceView(ServiceInterfaceView value);

} // InputPrimitiveView
