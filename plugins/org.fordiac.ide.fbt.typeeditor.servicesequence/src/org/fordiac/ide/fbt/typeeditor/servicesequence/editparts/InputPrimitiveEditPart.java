/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.editparts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.Shape;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.fordiac.ide.fbt.typeeditor.servicesequence.figures.AdvancedFixedAnchor;
import org.fordiac.ide.fbt.typeeditor.servicesequence.sequence.DeletePrimitiveEditPolicy;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.InputPrimitiveView;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.OutputPrimitiveView;
import org.fordiac.ide.gef.FixedAnchor;
import org.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart;
import org.fordiac.ide.gef.figures.VerticalLineFigure;
import org.fordiac.ide.gef.policies.ChangeStringEditPolicy;
import org.fordiac.ide.gef.policies.IChangeStringEditPart;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;

/**
 * The Class InputPrimitiveEditPart.
 */
public class InputPrimitiveEditPart extends AbstractDirectEditableEditPart
		implements NodeEditPart, IChangeStringEditPart {

	/** The name label. */
	Label nameLabel;
	private final Figure centerFigure = new Figure();

	private class PrimitiveFigure extends Shape {

		public PrimitiveFigure() {
			GridLayout mainLayout;
			setLayoutManager(mainLayout = new GridLayout(6, false));
			mainLayout.marginHeight = 0;

			GridLayout gl;
			centerFigure.setLayoutManager(gl = new GridLayout(3, false));
			gl.horizontalSpacing = 0;
			gl.verticalSpacing = 0;
			gl.marginHeight = 0;
			gl.marginWidth = 0;

			VerticalLineFigure lineLeft;
			VerticalLineFigure lineRight;

			GridData spaceData = new GridData();
			spaceData.widthHint = 35;
			Figure space = new Figure();

			centerFigure.add(lineLeft = new VerticalLineFigure(21));
			centerFigure.add(space);
			centerFigure.add(lineRight = new VerticalLineFigure(21));

			centerFigure.setConstraint(space, spaceData);

			GridData fillDataLeft = new GridData();
			fillDataLeft.grabExcessVerticalSpace = true;
			fillDataLeft.verticalAlignment = GridData.FILL;

			// fillDataLeft.widthHint = 20;

			GridData fillDataRight = new GridData();
			fillDataRight.grabExcessVerticalSpace = true;
			fillDataRight.verticalAlignment = GridData.FILL;
			// fillDataRight.widthHint = 20;

			centerFigure.setConstraint(lineLeft, fillDataLeft);
			centerFigure.setConstraint(lineRight, fillDataRight);

			GridData nameLabelData = new GridData();
			nameLabelData.widthHint = 100;

			GridData emptyLabelData = new GridData();
			emptyLabelData.widthHint = 100;

			GridData arrowLeftData = new GridData();
			arrowLeftData.widthHint = 40;
			Figure leftFigure = new Figure();

			GridData arrowRightData = new GridData();
			arrowRightData.widthHint = 40;
			Figure rightFigure = new Figure();

			if (getCastedModel().getInterfaceView().isIsLeftInterface()) {

				nameLabel = new Label();
				nameLabel.setText(getCastedModel().getInputPrimitive()
						.getEvent());
				nameLabel.setLabelAlignment(PositionConstants.RIGHT);
				add(nameLabel);
				setConstraint(nameLabel, nameLabelData);
				add(leftFigure);
				add(centerFigure);
				add(rightFigure);

				Label right = new Label();
				add(right);
				setConstraint(right, emptyLabelData);
			} else {
				nameLabel = new Label();
				nameLabel.setText(getCastedModel().getInputPrimitive()
						.getEvent());
				nameLabel.setLabelAlignment(PositionConstants.LEFT);

				Label left = new Label();

				add(left);
				setConstraint(left, emptyLabelData);
				add(leftFigure);
				add(centerFigure);
				add(rightFigure);
				add(nameLabel);
				setConstraint(nameLabel, nameLabelData);
			}
			setConstraint(leftFigure, arrowLeftData);
			setConstraint(rightFigure, arrowRightData);
		}

		@Override
		protected void fillShape(final Graphics graphics) {
		}

		@Override
		protected void outlineShape(final Graphics graphics) {
		}

	}

	@Override
	protected IFigure createFigure() {
		return new PrimitiveFigure();
	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.COMPONENT_ROLE,
				new DeletePrimitiveEditPolicy());
		// EditPolicy which allows the direct edit the events
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE,
				new ChangeStringEditPolicy());

	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editparts.AbstractEditPart#performRequest(org.eclipse.gef
	 * .Request)
	 */
	@Override
	public void performRequest(final Request request) {
		// REQ_DIRECT_EDIT -> first select 0.4 sec pause -> click -> edit
		// REQ_OPEN -> doubleclick

		if (request.getType() == RequestConstants.REQ_OPEN) {
			//Perform double click as direct edit
			request.setType(RequestConstants.REQ_DIRECT_EDIT); 
		}
		super.performRequest(request);
	}

	/**
	 * Gets the casted model.
	 * 
	 * @return the casted model
	 */
	public InputPrimitiveView getCastedModel() {
		return (InputPrimitiveView) getModel();
	}

	private TransactionEditPart getCastedParent() {
		return (TransactionEditPart) getParent();
	}

	/** The connection. */
	PrimitiveConnection connection = new PrimitiveConnection(true);
	
	/** The connecting connection. */
	ConnectingConnection connectingConnection = new ConnectingConnection();

	/**
	 * Gets the connecting connection.
	 * 
	 * @return the connecting connection
	 */
	public ConnectingConnection getConnectingConnection() {
		return connectingConnection;
	}

	@Override
	protected List<Object> getModelSourceConnections() {

		ArrayList<Object> temp = new ArrayList<Object>();

		OutputPrimitiveView view = getCastedParent()
				.getPossibleOutputPrimitive(getCastedModel());
		if (view != null) {
			temp.add(connectingConnection);
			// return g
		}

		temp.add(connection);
		return temp;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#getModelTargetConnections()
	 */
	@Override
	public List<Object> getModelTargetConnections() {
		ArrayList<Object> temp = new ArrayList<Object>();
		temp.add(connection);
		return temp;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.NodeEditPart#getSourceConnectionAnchor(org.eclipse.gef.ConnectionEditPart)
	 */
	@Override
	public ConnectionAnchor getSourceConnectionAnchor(
			final ConnectionEditPart connection) {
		if (connection instanceof PrimitiveConnectionEditPart) {
			if (getCastedModel().getInterfaceView().isIsLeftInterface()) {
				return new FixedAnchor(nameLabel, false);
			} else {
				return new FixedAnchor(nameLabel, true);
			}
		} else if (connection instanceof ConnectingConnectionEditPart) {
			if (getCastedModel().getInterfaceView().isIsLeftInterface()) {
				return new AdvancedFixedAnchor(centerFigure, true, 3, 0) {

				};
			} else {
				return new AdvancedFixedAnchor(centerFigure, false, -4, 0) {
				};
			}

		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.NodeEditPart#getSourceConnectionAnchor(org.eclipse.gef.Request)
	 */
	@Override
	public ConnectionAnchor getSourceConnectionAnchor(final Request request) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.NodeEditPart#getTargetConnectionAnchor(org.eclipse.gef.ConnectionEditPart)
	 */
	@Override
	public ConnectionAnchor getTargetConnectionAnchor(
			final ConnectionEditPart connection) {
		if (connection instanceof PrimitiveConnectionEditPart) {
			if (getCastedModel().getInterfaceView().isIsLeftInterface()) {
				return new FixedAnchor(centerFigure, true);
			} else {
				return new FixedAnchor(centerFigure, false);
			}
		} else if (connection instanceof ConnectingConnectionEditPart) {
			if (getCastedModel().getInterfaceView().isIsLeftInterface()) {
				return new AdvancedFixedAnchor(centerFigure, true, 3, 0);
			} else {
				return new AdvancedFixedAnchor(centerFigure, false, -4, 0);
			}

		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.NodeEditPart#getTargetConnectionAnchor(org.eclipse.gef.Request)
	 */
	@Override
	public ConnectionAnchor getTargetConnectionAnchor(final Request request) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart#getINamedElement()
	 */
	@Override
	public INamedElement getINamedElement() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart#getNameLabel()
	 */
	@Override
	public Label getNameLabel() {
		return nameLabel;
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.policies.IChangeStringEditPart#getElement()
	 */
	@Override
	public EObject getElement() {
		return getCastedModel().getInputPrimitive();
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.policies.IChangeStringEditPart#getFeatureID()
	 */
	@Override
	public int getFeatureID() {
		return LibraryElementPackage.INPUT_PRIMITIVE__EVENT;
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.policies.IChangeStringEditPart#getLabel()
	 */
	@Override
	public Label getLabel() {
		return getNameLabel();
	}

}
