/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.ServiceTransaction;

/**
 * The Class MoveTransactionCommand.
 */
public class MoveTransactionCommand extends Command {

	@SuppressWarnings("unused")
	private final ServiceTransaction serviceTransaction;
	private final int oldIndex;
	private int newIndex;

	/**
	 * Instantiates a new move transaction command.
	 * 
	 * @param serviceTransaction the service transaction
	 * @param oldIndex the old index
	 * @param newIndex the new index
	 */
	public MoveTransactionCommand(ServiceTransaction serviceTransaction,
			int oldIndex, int newIndex) {
		super();
		this.serviceTransaction = serviceTransaction;
		this.oldIndex = oldIndex;
		this.newIndex = newIndex;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		if (newIndex > oldIndex) {
			newIndex--;
		}
		// parent.getInterfaceList().getEventInputs().move(newIndex, child);
	}

}
