/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.sequence;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.FlowLayoutEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.fordiac.ide.fbt.typeeditor.servicesequence.commands.CreateTransactionCommand;
import org.fordiac.ide.fbt.typeeditor.servicesequence.commands.MoveTransactionCommand;
import org.fordiac.ide.fbt.typeeditor.servicesequence.editparts.TransactionEditPart;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceSequenceView;
import org.fordiac.ide.model.libraryElement.ServiceTransaction;

/**
 * The Class EventInputContainerLayoutEditPolicy.
 */
public class SequenceLayoutEditPolicy extends FlowLayoutEditPolicy {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.OrderedLayoutEditPolicy#createAddCommand(org
	 * .eclipse.gef.EditPart, org.eclipse.gef.EditPart)
	 */
	@Override
	protected Command createAddCommand(final EditPart child, final EditPart after) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.OrderedLayoutEditPolicy#createMoveChildCommand
	 * (org.eclipse.gef.EditPart, org.eclipse.gef.EditPart)
	 */
	@Override
	protected Command createMoveChildCommand(final EditPart child,
			final EditPart after) {
		if (child instanceof TransactionEditPart) {
			TransactionEditPart childEP = (TransactionEditPart) child;
			int oldIndex = getHost().getChildren().indexOf(child);
			int newIndex = -1;
			if (after == null) {
				newIndex = getHost().getChildren().size();
			} else {
				newIndex = getHost().getChildren().indexOf(after);
			}
			return new MoveTransactionCommand(
					(ServiceTransaction) childEP.getModel(), oldIndex, newIndex);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.LayoutEditPolicy#getCreateCommand(org.eclipse
	 * .gef.requests.CreateRequest)
	 */
	@Override
	protected Command getCreateCommand(final CreateRequest request) {

		Object type = request.getNewObjectType();
		Object model = getHost().getModel();

		if (type.equals("ServiceTransaction")) { //$NON-NLS-1$
			CreateTransactionCommand cmd = new CreateTransactionCommand(
					(ServiceSequenceView) model, true);
			return cmd;

		}
		
		if (type.equals("LeftInputPrimitive")){
			CreateTransactionCommand cmd = new CreateTransactionCommand(
					(ServiceSequenceView) model, true);
			return cmd;
		}
		if (type.equals("RightInputPrimitive")){
			CreateTransactionCommand cmd = new CreateTransactionCommand(
					(ServiceSequenceView) model, false);
			return cmd;
		}
		return null;
	}

}
