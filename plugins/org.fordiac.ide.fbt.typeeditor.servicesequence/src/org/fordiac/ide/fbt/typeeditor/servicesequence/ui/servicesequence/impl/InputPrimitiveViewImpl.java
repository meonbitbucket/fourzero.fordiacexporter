/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.InputPrimitiveView;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceInterfaceView;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServicesequencePackage;
import org.fordiac.ide.model.libraryElement.InputPrimitive;
import org.fordiac.ide.model.ui.impl.ViewImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Input Primitive View</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.impl.InputPrimitiveViewImpl#getInputPrimitive <em>Input Primitive</em>}</li>
 * <li>{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.impl.InputPrimitiveViewImpl#getInterfaceView <em>Interface View</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class InputPrimitiveViewImpl extends ViewImpl implements InputPrimitiveView {
	/**
	 * The cached value of the '{@link #getInputPrimitive() <em>Input Primitive</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputPrimitive()
	 * @generated
	 * @ordered
	 */
	protected InputPrimitive inputPrimitive;
	/**
	 * The cached value of the '{@link #getInterfaceView() <em>Interface View</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterfaceView()
	 * @generated
	 * @ordered
	 */
	protected ServiceInterfaceView interfaceView;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InputPrimitiveViewImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ServicesequencePackage.Literals.INPUT_PRIMITIVE_VIEW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the input primitive
	 * 
	 * @generated
	 */
	public InputPrimitive getInputPrimitive() {
		if (inputPrimitive != null && inputPrimitive.eIsProxy()) {
			InternalEObject oldInputPrimitive = (InternalEObject)inputPrimitive;
			inputPrimitive = (InputPrimitive)eResolveProxy(oldInputPrimitive);
			if (inputPrimitive != oldInputPrimitive) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ServicesequencePackage.INPUT_PRIMITIVE_VIEW__INPUT_PRIMITIVE, oldInputPrimitive, inputPrimitive));
			}
		}
		return inputPrimitive;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the input primitive
	 * 
	 * @generated
	 */
	public InputPrimitive basicGetInputPrimitive() {
		return inputPrimitive;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @param newInputPrimitive the new input primitive
	 * 
	 * @generated
	 */
	public void setInputPrimitive(InputPrimitive newInputPrimitive) {
		InputPrimitive oldInputPrimitive = inputPrimitive;
		inputPrimitive = newInputPrimitive;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServicesequencePackage.INPUT_PRIMITIVE_VIEW__INPUT_PRIMITIVE, oldInputPrimitive, inputPrimitive));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the interface view
	 * 
	 * @generated
	 */
	public ServiceInterfaceView getInterfaceView() {
		if (interfaceView != null && interfaceView.eIsProxy()) {
			InternalEObject oldInterfaceView = (InternalEObject)interfaceView;
			interfaceView = (ServiceInterfaceView)eResolveProxy(oldInterfaceView);
			if (interfaceView != oldInterfaceView) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ServicesequencePackage.INPUT_PRIMITIVE_VIEW__INTERFACE_VIEW, oldInterfaceView, interfaceView));
			}
		}
		return interfaceView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the service interface view
	 * 
	 * @generated
	 */
	public ServiceInterfaceView basicGetInterfaceView() {
		return interfaceView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @param newInterfaceView the new interface view
	 * 
	 * @generated
	 */
	public void setInterfaceView(ServiceInterfaceView newInterfaceView) {
		ServiceInterfaceView oldInterfaceView = interfaceView;
		interfaceView = newInterfaceView;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServicesequencePackage.INPUT_PRIMITIVE_VIEW__INTERFACE_VIEW, oldInterfaceView, interfaceView));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @param featureID the feature id
	 * @param resolve the resolve
	 * @param coreType the core type
	 * 
	 * @return the object
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ServicesequencePackage.INPUT_PRIMITIVE_VIEW__INPUT_PRIMITIVE:
				if (resolve) return getInputPrimitive();
				return basicGetInputPrimitive();
			case ServicesequencePackage.INPUT_PRIMITIVE_VIEW__INTERFACE_VIEW:
				if (resolve) return getInterfaceView();
				return basicGetInterfaceView();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @param featureID the feature id
	 * @param newValue the new value
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ServicesequencePackage.INPUT_PRIMITIVE_VIEW__INPUT_PRIMITIVE:
				setInputPrimitive((InputPrimitive)newValue);
				return;
			case ServicesequencePackage.INPUT_PRIMITIVE_VIEW__INTERFACE_VIEW:
				setInterfaceView((ServiceInterfaceView)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @param featureID the feature id
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ServicesequencePackage.INPUT_PRIMITIVE_VIEW__INPUT_PRIMITIVE:
				setInputPrimitive((InputPrimitive)null);
				return;
			case ServicesequencePackage.INPUT_PRIMITIVE_VIEW__INTERFACE_VIEW:
				setInterfaceView((ServiceInterfaceView)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @param featureID the feature id
	 * 
	 * @return true, if e is set
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ServicesequencePackage.INPUT_PRIMITIVE_VIEW__INPUT_PRIMITIVE:
				return inputPrimitive != null;
			case ServicesequencePackage.INPUT_PRIMITIVE_VIEW__INTERFACE_VIEW:
				return interfaceView != null;
		}
		return super.eIsSet(featureID);
	}

} //InputPrimitiveViewImpl
