/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceSequenceView;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServicesequencePackage;

import org.fordiac.ide.model.libraryElement.ServiceSequence;
import org.fordiac.ide.model.ui.impl.ViewImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Service Sequence View</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.impl.ServiceSequenceViewImpl#getServiceSequence <em>Service Sequence</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ServiceSequenceViewImpl extends ViewImpl implements ServiceSequenceView {
	/**
	 * The cached value of the '{@link #getServiceSequence() <em>Service Sequence</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServiceSequence()
	 * @generated
	 * @ordered
	 */
	protected ServiceSequence serviceSequence;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServiceSequenceViewImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ServicesequencePackage.Literals.SERVICE_SEQUENCE_VIEW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the service sequence
	 * 
	 * @generated
	 */
	public ServiceSequence getServiceSequence() {
		if (serviceSequence != null && serviceSequence.eIsProxy()) {
			InternalEObject oldServiceSequence = (InternalEObject)serviceSequence;
			serviceSequence = (ServiceSequence)eResolveProxy(oldServiceSequence);
			if (serviceSequence != oldServiceSequence) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ServicesequencePackage.SERVICE_SEQUENCE_VIEW__SERVICE_SEQUENCE, oldServiceSequence, serviceSequence));
			}
		}
		return serviceSequence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @return the service sequence
	 * 
	 * @generated
	 */
	public ServiceSequence basicGetServiceSequence() {
		return serviceSequence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @param newServiceSequence the new service sequence
	 * 
	 * @generated
	 */
	public void setServiceSequence(ServiceSequence newServiceSequence) {
		ServiceSequence oldServiceSequence = serviceSequence;
		serviceSequence = newServiceSequence;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServicesequencePackage.SERVICE_SEQUENCE_VIEW__SERVICE_SEQUENCE, oldServiceSequence, serviceSequence));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @param featureID the feature id
	 * @param resolve the resolve
	 * @param coreType the core type
	 * 
	 * @return the object
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ServicesequencePackage.SERVICE_SEQUENCE_VIEW__SERVICE_SEQUENCE:
				if (resolve) return getServiceSequence();
				return basicGetServiceSequence();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @param featureID the feature id
	 * @param newValue the new value
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ServicesequencePackage.SERVICE_SEQUENCE_VIEW__SERVICE_SEQUENCE:
				setServiceSequence((ServiceSequence)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @param featureID the feature id
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ServicesequencePackage.SERVICE_SEQUENCE_VIEW__SERVICE_SEQUENCE:
				setServiceSequence((ServiceSequence)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * 
	 * @param featureID the feature id
	 * 
	 * @return true, if e is set
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ServicesequencePackage.SERVICE_SEQUENCE_VIEW__SERVICE_SEQUENCE:
				return serviceSequence != null;
		}
		return super.eIsSet(featureID);
	}

} //ServiceSequenceViewImpl
