/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.editparts;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.fordiac.ide.fbt.typeeditor.servicesequence.sequence.DeleteTransactionEditPolicy;
import org.fordiac.ide.fbt.typeeditor.servicesequence.sequence.TransactionLayoutEditPolicy;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.InputPrimitiveView;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.OutputPrimitiveView;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServicesequenceFactory;
import org.fordiac.ide.gef.draw2d.AdvancedLineBorder;
import org.fordiac.ide.gef.policies.HighlightEditPolicy;
import org.fordiac.ide.model.libraryElement.OutputPrimitive;
import org.fordiac.ide.model.libraryElement.ServiceTransaction;
import org.fordiac.ide.model.ui.View;

/**
 * The Class TransactionEditPart.
 */
public class TransactionEditPart extends AbstractGraphicalEditPart {

	@Override
	protected IFigure createFigure() {
		Figure f = new Figure() {
			@Override
			public void setBackgroundColor(Color bg) {
				super.setBackgroundColor(bg);
				for (@SuppressWarnings("rawtypes")
				Iterator iterator = getChildren().iterator(); iterator
						.hasNext();) {
					Figure fig = (Figure) iterator.next();
					fig.setBackgroundColor(bg);
				}
			}
		};

		ToolbarLayout fl = new ToolbarLayout(false);
		fl.setSpacing(0);
		f.setLayoutManager(fl);
		AdvancedLineBorder alb = new AdvancedLineBorder(
				PositionConstants.NORTH, SWT.LINE_DASH);

		f.setBorder(alb);

		return f;
	}

	/**
	 * Gets the casted model.
	 * 
	 * @return the casted model
	 */
	public TransactionElement getCastedModel() {
		return (TransactionElement) getModel();
	}

	/**
	 * Gets the possible output primitive.
	 * 
	 * @param inputView the input view
	 * 
	 * @return the possible output primitive
	 */
	public OutputPrimitiveView getPossibleOutputPrimitive(
			final InputPrimitiveView inputView) {
		int index = modelChildren.indexOf(inputView);
		try {
			if (modelChildren.get(index + 1) instanceof OutputPrimitiveView) {
				return (OutputPrimitiveView) modelChildren.get(index + 1);
			}
		} catch (IndexOutOfBoundsException e) {
			return null;

		}
		return null;
	}

	/**
	 * Gets the possible input primitive.
	 * 
	 * @param outputView the output view
	 * 
	 * @return the possible input primitive
	 */
	public InputPrimitiveView getPossibleInputPrimitive(
			final OutputPrimitiveView outputView) {
		int index = modelChildren.indexOf(outputView);
		try {
			if (modelChildren.get(index - 1) instanceof InputPrimitiveView) {
				return (InputPrimitiveView) modelChildren.get(index - 1);
			}
		} catch (IndexOutOfBoundsException e) {
			return null;

		}
		return null;
	}

	/** The model children. */
	ArrayList<View> modelChildren = new ArrayList<View>();

	@Override
	protected List<View> getModelChildren() {
		ServiceTransaction transaction = getCastedModel().getTransaction();

		if (transaction.getInputPrimitive() != null) {
			InputPrimitiveView inputPrimitiveView = ServicesequenceFactory.eINSTANCE
					.createInputPrimitiveView();
			inputPrimitiveView.setInputPrimitive(transaction
					.getInputPrimitive());
			if (transaction.getInputPrimitive().getInterface().equals(
					getCastedModel().getType().getService().getLeftInterface())) {
				inputPrimitiveView.setInterfaceView(getCastedModel()
						.getLeftInterface());
			} else {
				inputPrimitiveView.setInterfaceView(getCastedModel()
						.getRightInterface());

			}
			modelChildren.add(inputPrimitiveView);
		}

		for (Iterator<OutputPrimitive> iterator2 = transaction.getOutputPrimitive().iterator(); iterator2
				.hasNext();) {
			OutputPrimitive outputPrimitive = (OutputPrimitive) iterator2
					.next();
			OutputPrimitiveView outputPrimitiveView = ServicesequenceFactory.eINSTANCE
					.createOutputPrimitiveView();
			outputPrimitiveView.setOutputPrimitive(outputPrimitive);
			if (outputPrimitive.getInterface().equals(
					getCastedModel().getType().getService().getLeftInterface())) {
				outputPrimitiveView.setInterfaceView(getCastedModel()
						.getLeftInterface());
			} else {
				outputPrimitiveView.setInterfaceView(getCastedModel()
						.getRightInterface());

			}
			modelChildren.add(outputPrimitiveView);

		}
		return modelChildren;

	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.LAYOUT_ROLE,
				new TransactionLayoutEditPolicy());
		installEditPolicy(EditPolicy.SELECTION_FEEDBACK_ROLE,
				new HighlightEditPolicy());
		installEditPolicy(EditPolicy.COMPONENT_ROLE,
				new DeleteTransactionEditPolicy());

	}

}
