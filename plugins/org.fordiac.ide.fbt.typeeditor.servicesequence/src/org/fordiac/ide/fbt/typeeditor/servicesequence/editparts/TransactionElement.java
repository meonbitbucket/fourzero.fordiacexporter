/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.editparts;

import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceInterfaceView;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.ServiceTransaction;

/**
 * The Class TransactionElement.
 */
public class TransactionElement {

	private ServiceTransaction transaction;
	private FBType type;
	private ServiceInterfaceView leftInterface;
	private ServiceInterfaceView rightInterface;
	
	
	/**
	 * Instantiates a new transaction element.
	 * 
	 * @param transaction the transaction
	 * @param type the type
	 * @param leftInterface the left interface
	 * @param rightInterface the right interface
	 */
	public TransactionElement(ServiceTransaction transaction, FBType type,
			ServiceInterfaceView leftInterface,
			ServiceInterfaceView rightInterface) {
		super();
		this.transaction = transaction;
		this.type = type;
		this.leftInterface = leftInterface;
		this.rightInterface = rightInterface;
	}
	
	/**
	 * Gets the transaction.
	 * 
	 * @return the transaction
	 */
	public ServiceTransaction getTransaction() {
		return transaction;
	}
	
	/**
	 * Gets the left interface.
	 * 
	 * @return the left interface
	 */
	public ServiceInterfaceView getLeftInterface() {
		return leftInterface;
	}
	
	/**
	 * Gets the right interface.
	 * 
	 * @return the right interface
	 */
	public ServiceInterfaceView getRightInterface() {
		return rightInterface;
	}
	
	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public FBType getType() {
		return type;
	}
	
	

	
}
