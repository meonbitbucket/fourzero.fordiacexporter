/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence;

import org.fordiac.ide.model.libraryElement.ServiceSequence;
import org.fordiac.ide.model.ui.View;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Service Sequence View</b></em>'.
 * <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceSequenceView#getServiceSequence <em>Service Sequence</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServicesequencePackage#getServiceSequenceView()
 * @model
 * @generated
 */
public interface ServiceSequenceView extends View {

	/**
	 * Returns the value of the '<em><b>Service Sequence</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Service Sequence</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Service Sequence</em>' reference.
	 * 
	 * @see #setServiceSequence(ServiceSequence)
	 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServicesequencePackage#getServiceSequenceView_ServiceSequence()
	 * @model
	 * @generated
	 */
	ServiceSequence getServiceSequence();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceSequenceView#getServiceSequence <em>Service Sequence</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value the new value of the '<em>Service Sequence</em>' reference.
	 * 
	 * @see #getServiceSequence()
	 * @generated
	 */
	void setServiceSequence(ServiceSequence value);
} // ServiceSequenceView
