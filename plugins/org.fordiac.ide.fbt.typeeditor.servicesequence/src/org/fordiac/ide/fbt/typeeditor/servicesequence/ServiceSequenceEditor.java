/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.command.BasicCommandStack;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.provider.EcoreItemProviderAdapterFactory;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory;
import org.eclipse.emf.edit.provider.resource.ResourceItemProviderAdapterFactory;
import org.eclipse.emf.edit.ui.celleditor.AdapterFactoryTreeEditor;
import org.eclipse.emf.edit.ui.dnd.EditingDomainViewerDropAdapter;
import org.eclipse.emf.edit.ui.dnd.LocalTransfer;
import org.eclipse.emf.edit.ui.dnd.ViewerDragAdapter;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.edit.ui.provider.UnwrappingSelectionProvider;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.KeyHandler;
import org.eclipse.gef.KeyStroke;
import org.eclipse.gef.MouseWheelHandler;
import org.eclipse.gef.MouseWheelZoomHandler;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.ui.actions.GEFActionConstants;
import org.eclipse.gef.ui.actions.ZoomInAction;
import org.eclipse.gef.ui.actions.ZoomOutAction;
import org.eclipse.gef.ui.parts.GraphicalEditorWithFlyoutPalette;
import org.eclipse.gef.ui.parts.GraphicalViewerKeyHandler;
import org.eclipse.gef.ui.parts.ScrollingGraphicalViewer;
import org.eclipse.gmf.runtime.notation.provider.NotationItemProviderAdapterFactory;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.commands.ActionHandler;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.part.EditorActionBarContributor;
import org.eclipse.ui.part.MultiPageEditorSite;
import org.eclipse.ui.views.properties.PropertySheetPage;
import org.fordiac.gmfextensions.AnimatedZoomScalableFreeformRootEditPart;
import org.fordiac.gmfextensions.DirectEditKeyHandler;
import org.fordiac.ide.fbt.typeeditor.FBTypeEditDomain;
import org.fordiac.ide.fbt.typeeditor.editors.FBTypeEditor;
import org.fordiac.ide.fbt.typeeditor.editors.IFBTEditorPart;
import org.fordiac.ide.fbt.typeeditor.servicesequence.commands.AddServiceSequeceCommand;
import org.fordiac.ide.fbt.typeeditor.servicesequence.commands.DeleteInputPrimitiveCommand;
import org.fordiac.ide.fbt.typeeditor.servicesequence.commands.DeleteOutputPrimitiveCommand;
import org.fordiac.ide.fbt.typeeditor.servicesequence.commands.DeleteServiceSquenceCommand;
import org.fordiac.ide.fbt.typeeditor.servicesequence.commands.DeleteTransactionCommand;
import org.fordiac.ide.fbt.typeeditor.servicesequence.editparts.InputPrimitiveEditPart;
import org.fordiac.ide.fbt.typeeditor.servicesequence.editparts.OutputPrimitiveEditPart;
import org.fordiac.ide.fbt.typeeditor.servicesequence.editparts.SequenceRootEditPart;
import org.fordiac.ide.fbt.typeeditor.servicesequence.editparts.ServiceSequenceEditPartFactory;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.InputPrimitiveView;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.OutputPrimitiveView;
import org.fordiac.ide.fbt.typemanagement.FBTypeEditorInput;
import org.fordiac.ide.gef.ZoomUndoRedoContextMenuProvider;
import org.fordiac.ide.gef.actions.SaveImageAction;
import org.fordiac.ide.model.Palette.provider.PaletteItemProviderAdapterFactory;
import org.fordiac.ide.model.data.provider.DataItemProviderAdapterFactory;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.InputPrimitive;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.libraryElement.OutputPrimitive;
import org.fordiac.ide.model.libraryElement.Service;
import org.fordiac.ide.model.libraryElement.ServiceSequence;
import org.fordiac.ide.model.libraryElement.ServiceTransaction;
import org.fordiac.ide.model.libraryElement.provider.LibraryElementItemProviderAdapterFactory;
import org.fordiac.ide.model.ui.provider.UiItemProviderAdapterFactory;
import org.fordiac.ide.util.imageprovider.ImageProvider;

/**
 * The Class BasicFBInterfaceEditor.
 */
public class ServiceSequenceEditor extends GraphicalEditorWithFlyoutPalette
		implements IFBTEditorPart, IMenuListener{

	/** The fb type. */
	private FBType fbType;

	/** The adapter. */
	private final EContentAdapter adapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {
			super.notifyChanged(notification);
			Object feature = notification.getFeature();
			if (LibraryElementPackage.eINSTANCE.getService_ServiceSequence()
					.equals(feature)) {
				if (!serviceSequencesViewer.getTree().isDisposed()) {
					serviceSequencesViewer.refresh();
				}
			}
			//TODO handle interface changes
		}

	};

	/** The shared key handler. */
	private KeyHandler sharedKeyHandler;

	private TreeViewer serviceSequencesViewer;
	
	protected AdapterFactoryEditingDomain editingDomain;
	
	protected ComposedAdapterFactory adapterFactory;
	
	protected PropertySheetPage propertySheetPage;
	
	Button deleteButton;

	private boolean blockGraphicalViewerUpdate = false;

	/**
	 * Instantiates a new basic fb interface editor.
	 */
	public ServiceSequenceEditor() {
		super();
		initializeEditingDomain();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.ui.parts.GraphicalEditor#init(org.eclipse.ui.IEditorSite,
	 * org.eclipse.ui.IEditorInput)
	 */
	@Override
	public void init(final IEditorSite site, final IEditorInput input)
			throws PartInitException {
		//setInput(input);
		setInputWithNotify(input);
		if (input instanceof FBTypeEditorInput) {
			FBTypeEditorInput untypedInput = (FBTypeEditorInput) input;
			if (untypedInput.getContent() instanceof FBType) {
				fbType = (FBType) untypedInput.getContent();
				fbType.eAdapters().add(adapter);
				// for (Iterator iterator = fbType.getInterfaceList()
				// .getEventInputs().iterator(); iterator.hasNext();) {
				// Event e = (Event) iterator.next();
				// e.eAdapters().add(adapter);
				// }
			}
		}
		setSite(site);
		setEditDomain(new FBTypeEditDomain(this, commandStack));
		setPartName("ServiceSequence");
		setTitleImage(ImageProvider.getImage("sequence.png"));
		super.init(site, input);
		
		//site.setSelectionProvider(this);
		//site.getPage().addPartListener(partListener);
	}
	
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.parts.GraphicalEditor#configureGraphicalViewer()
	 */
	@Override
	protected void configureGraphicalViewer() {
		super.configureGraphicalViewer();
		ScrollingGraphicalViewer viewer = (ScrollingGraphicalViewer) getGraphicalViewer();
		/**
		 * the ScaleableFreeformRootEditPart provides a ZoomManager ... (see GEF
		 * API reference)
		 */
		// ScalableFreeformRootEditPart root = new
		// ScalableFreeformRootEditPart();
		AnimatedZoomScalableFreeformRootEditPart root = new AnimatedZoomScalableFreeformRootEditPart();

		List<String> zoomLevels = new ArrayList<String>(3);
		zoomLevels.add(ZoomManager.FIT_ALL);
		zoomLevels.add(ZoomManager.FIT_WIDTH);
		zoomLevels.add(ZoomManager.FIT_HEIGHT);
		root.getZoomManager().setZoomLevelContributions(zoomLevels);

		// TODO __geben -> move ZoomManager.Animate_zooom_in_out to preference
		// page
		root.getZoomManager().setZoomAnimationStyle(
				ZoomManager.ANIMATE_ZOOM_IN_OUT);

		IAction zoomIn = new ZoomInAction(root.getZoomManager());
		IAction zoomOut = new ZoomOutAction(root.getZoomManager());
		getActionRegistry().registerAction(zoomIn);
		getActionRegistry().registerAction(zoomOut);

		IHandlerService zoomInService = (IHandlerService) getSite().getService(
				IHandlerService.class);
		zoomInService.activateHandler(zoomIn.getActionDefinitionId(),
				new ActionHandler(zoomIn));

		IHandlerService zoomOutService = (IHandlerService) getSite()
				.getService(IHandlerService.class);
		zoomOutService.activateHandler(zoomOut.getActionDefinitionId(),
				new ActionHandler(zoomOut));

		viewer.setRootEditPart(root);
		viewer.setEditPartFactory(new ServiceSequenceEditPartFactory());
		getActionRegistry().registerAction(new SaveImageAction(viewer));
		// configure the context menu provider
		ContextMenuProvider cmProvider = new ZoomUndoRedoContextMenuProvider(
				viewer, root.getZoomManager(), getActionRegistry()) {
			@Override
			public void buildContextMenu(IMenuManager menu) {
				super.buildContextMenu(menu);

				IAction action = registry.getAction(ActionFactory.DELETE
						.getId());
				menu.appendToGroup(GEFActionConstants.GROUP_COPY, action);
			}
		};
		viewer.setContextMenu(cmProvider);
		viewer.setProperty(MouseWheelHandler.KeyGenerator.getKey(SWT.MOD1),
				MouseWheelZoomHandler.SINGLETON);

		KeyHandler viewerKeyHandler = new GraphicalViewerKeyHandler(viewer)
				.setParent(getCommonKeyHandler());

		viewer.setKeyHandler(new DirectEditKeyHandler(viewer)
				.setParent(viewerKeyHandler));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.ui.parts.GraphicalEditorWithFlyoutPalette#createPartControl
	 * (org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createPartControl(final Composite parent) {
		SashForm top = new SashForm(parent, SWT.HORIZONTAL | SWT.SMOOTH);
		top.setLayout(new FillLayout());

		Composite serviceSequencesContainer = new Composite(top, SWT.BORDER);
		serviceSequencesContainer.setLayout(new GridLayout());

		createAddDeleteButtons(serviceSequencesContainer);

		createSequenceTreeViewer(serviceSequencesContainer);

		SashForm s = new SashForm(top, SWT.VERTICAL | SWT.SMOOTH);
		s.setLayout(new FillLayout());

		Composite graphicaEditor = new Composite(s, SWT.NONE);
		graphicaEditor.setLayout(new FillLayout());
		super.createPartControl(graphicaEditor);

		Composite properties = new Composite(s, SWT.NONE);
		properties.setLayout(new FillLayout());
		propertySheetPage = new PropertySheetPage();
		propertySheetPage.createControl(properties);
		FormData data = new FormData();
		data.left = new FormAttachment(0, 0);
		data.right = new FormAttachment(100, 0);
		data.top = new FormAttachment(0, 0);
		data.bottom = new FormAttachment(100, 0);
		propertySheetPage.getControl().setLayoutData(data);
		propertySheetPage.getControl().addControlListener(new ControlAdapter() {
			public void controlResized(ControlEvent e) {
				// page.resizeScrolledComposite();
			}
		});
		
		propertySheetPage.setPropertySourceProvider(new AdapterFactoryContentProvider(getAdapterFactory()));
		
		top.setWeights(new int[] { 25, 75 });
		s.setWeights(new int[] { 80, 20 });
	}

	private void createSequenceTreeViewer(Composite composite) {
		
		serviceSequencesViewer = new TreeViewer(composite, SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		
		GridData serviceSequenceData = new GridData();
		serviceSequenceData.horizontalAlignment = GridData.FILL;
		serviceSequenceData.verticalAlignment = GridData.FILL;
		serviceSequenceData.grabExcessHorizontalSpace = true;
		serviceSequenceData.grabExcessVerticalSpace = true;

		serviceSequencesViewer.getTree().setLayoutData(serviceSequenceData);
		
		serviceSequencesViewer.setContentProvider(new AdapterFactoryContentProvider(
				getAdapterFactory()));
		serviceSequencesViewer.setLabelProvider(new AdapterFactoryLabelProvider(
				getAdapterFactory()));

		new AdapterFactoryTreeEditor(serviceSequencesViewer.getTree(), adapterFactory);

		createContextMenuFor(serviceSequencesViewer);
		
		serviceSequencesViewer.setInput(fbType.getService());
		
		serviceSequencesViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(
					final SelectionChangedEvent event) {
				if (event.getSelection() instanceof StructuredSelection) {
					StructuredSelection selection = (StructuredSelection) event
							.getSelection();					
					propertySheetPage.selectionChanged(getEditorSite().getPart(), selection);
					
					deleteButton.setEnabled(!selection.isEmpty());
					
					if(!blockGraphicalViewerUpdate){
						ServiceSequence selectedSequence = null;
						if(selection.getFirstElement() instanceof ServiceSequence){
							selectedSequence = (ServiceSequence) selection.getFirstElement();
						}
						else if(selection.getFirstElement() instanceof ServiceTransaction){
							selectedSequence = (ServiceSequence)((ServiceTransaction) selection.getFirstElement()).eContainer();
						}
						else if(selection.getFirstElement() instanceof InputPrimitive){
							selectedSequence = (ServiceSequence)((InputPrimitive) selection.getFirstElement()).eContainer().eContainer();
						}
						else if(selection.getFirstElement() instanceof OutputPrimitive){
							selectedSequence = (ServiceSequence)((OutputPrimitive) selection.getFirstElement()).eContainer().eContainer();
						}
						
						if((null != selectedSequence) && (selectedSequence != getSelectedSequence())){
							//only update selection if we have a new sequence to select
							//this ensures that the selection in the graphical viewer is not lost during clicks 
							//in the treeviewer on the same item, furthermore we keep the viewer present when 
							//services primitives are deleted. 
							selectNewSequence(selectedSequence);
						}
					}
				}
			}
		});
	}

	private void createAddDeleteButtons(Composite serviceSequencesContainer) {
		Composite buttonContainer = new Composite(serviceSequencesContainer, SWT.NONE);

		GridData serviceSequenceData = new GridData();
		serviceSequenceData.horizontalAlignment = GridData.FILL;
		serviceSequenceData.verticalAlignment = GridData.FILL;
		serviceSequenceData.grabExcessHorizontalSpace = true;
		serviceSequenceData.grabExcessVerticalSpace = false;

		buttonContainer.setLayoutData(serviceSequenceData);
		
		GridLayoutFactory.swtDefaults().numColumns(4).margins(0, 0).applyTo(buttonContainer);

		Button addButton = new Button(buttonContainer, SWT.NONE);
		addButton.setImage(ImageProvider.getImage("add_obj.gif"));
		addButton.setText("Add  Sequence");
		addButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				AddServiceSequeceCommand cmd = new AddServiceSequeceCommand(
						fbType);
				getCommandStack().execute(cmd);
			}

			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
			}

		});

		deleteButton = new Button(buttonContainer, SWT.NONE);
		deleteButton.setImage(ImageProvider.getImage("delete_obj.gif"));
		deleteButton.setText("Delete");
		deleteButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				
				TreeSelection selection = (TreeSelection)serviceSequencesViewer.getSelection();
				if(!selection.isEmpty()){
					Command cmd = null;
					Object selected = selection.getFirstElement();
					if(selected instanceof ServiceSequence){
						selectNewSequence(null);  //clear the graphical viewer of the whole service sequence is deleted
						cmd = new DeleteServiceSquenceCommand(
								fbType, (ServiceSequence)selected);						
					}else if( selected instanceof ServiceTransaction){
						cmd = new DeleteTransactionCommand((ServiceTransaction)selected);
					}else if(selected instanceof InputPrimitive){
						cmd = new DeleteInputPrimitiveCommand((InputPrimitive)selected);
					}else if(selected instanceof OutputPrimitive){
						cmd = new DeleteOutputPrimitiveCommand((OutputPrimitive)selected);
					}
					getCommandStack().execute(cmd);
				}
			}

			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
			}

		});
		
		deleteButton.setEnabled(false);
		
		Composite toolbarContaier = new Composite(buttonContainer, SWT.FILL| SWT.RIGHT);
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		toolbarContaier.setLayoutData(gridData);		
		toolbarContaier.setLayout(new FillLayout());
		
		
		ToolBar toolBar = new ToolBar(buttonContainer, SWT.FLAT | SWT.RIGHT);
		
		ToolItem collapseAll = new ToolItem(toolBar, SWT.FLAT);
		collapseAll.setImage(ImageProvider.getImage("collapseall.gif"));
		collapseAll.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				serviceSequencesViewer.collapseAll();
			}
			
			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
			}
		});
		
		
		ToolItem expandAll = new ToolItem(toolBar, SWT.FLAT);
		expandAll.setImage(ImageProvider.getImage("expandall.gif"));
		expandAll.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				serviceSequencesViewer.expandAll();
			}

			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.gef.ui.parts.GraphicalEditorWithFlyoutPalette#
	 * initializeGraphicalViewer()
	 */
	@Override
	protected void initializeGraphicalViewer() {
		// super.initializeGraphicalViewer();
		GraphicalViewer viewer = getGraphicalViewer();
		viewer.setContents(fbType);
		// listen for dropped parts
		// viewer.addDropTargetListener(createTransferDropTargetListener());
		// enable drag from palette
		// getGraphicalViewer().addDropTargetListener(
		// new TemplateTransferDropTargetListener(getGraphicalViewer()));
		// // // adds a new mapping of this editor and the model to a central
		// place
		// Activator.getDefault().addEditorForModel(model, this);
		// this.tabbedPropertySheetPage = new TabbedPropertySheetPage(this);
	}

	/**
	 * Gets the common key handler.
	 * 
	 * @return the common key handler
	 */
	protected KeyHandler getCommonKeyHandler() {
		if (sharedKeyHandler == null) {
			sharedKeyHandler = new KeyHandler();
			sharedKeyHandler
					.put(KeyStroke.getPressed(SWT.DEL, 127, 0),
							getActionRegistry().getAction(
									ActionFactory.DELETE.getId()));
			sharedKeyHandler.put(
					KeyStroke.getPressed(SWT.F2, 0),
					getActionRegistry().getAction(
							GEFActionConstants.DIRECT_EDIT));
			sharedKeyHandler.put(/* CTRL + '=' */
			KeyStroke.getPressed('+', 0x3d, SWT.CTRL), getActionRegistry()
					.getAction(GEFActionConstants.ZOOM_IN));

		}
		return sharedKeyHandler;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.ui.parts.GraphicalEditor#selectionChanged(org.eclipse
	 * .ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	@Override
	public void selectionChanged(final IWorkbenchPart part,
			final ISelection selection) {
		super.selectionChanged(part, selection);
		// If not in FBTypeEditor ignore selection changed
		if (part.getSite().getPage().getActiveEditor() instanceof FBTypeEditor) {
			updateActions(getSelectionActions());
			if (!selection.isEmpty() && selection instanceof IStructuredSelection) {
				IStructuredSelection sel = (IStructuredSelection) selection;
				Object ob = null;
				
				if (sel.getFirstElement() instanceof ConnectionEditPart) {
					ob = ((ConnectionEditPart)sel.getFirstElement()).getSource().getModel();
					if(ob instanceof InputPrimitiveView){
						ob = ((InputPrimitiveView)ob).getInputPrimitive();
					}
					else if(ob instanceof OutputPrimitiveView){
						ob = ((OutputPrimitiveView)ob).getOutputPrimitive();
					}
					else{
						ob = null;
					}
				}
				else if(sel.getFirstElement() instanceof OutputPrimitiveEditPart){
					ob = ((OutputPrimitiveView)((EditPart)sel.getFirstElement()).getModel()).getOutputPrimitive(); 
				}
				else if(sel.getFirstElement() instanceof InputPrimitiveEditPart){
					ob = ((InputPrimitiveView)((EditPart)sel.getFirstElement()).getModel()).getInputPrimitive();
				}			
				
				if(null != ob){
					blockGraphicalViewerUpdate  = true;
					serviceSequencesViewer.setSelection(new StructuredSelection(ob));
					blockGraphicalViewerUpdate = false;
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.ui.parts.GraphicalEditorWithFlyoutPalette#getPaletteRoot
	 * ()
	 */
	@Override
	protected PaletteRoot getPaletteRoot() {
		return ServiceInterfacePaletteFactory.createPalette();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.
	 * IProgressMonitor)
	 */
	@Override
	public void doSave(final IProgressMonitor monitor) {
		//currently nothing needs to be done here
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.parts.GraphicalEditor#dispose()
	 */
	@Override
	public void dispose() {
		if (fbType != null && fbType.eAdapters().contains(adapter)) {
			fbType.eAdapters().remove(adapter);
		}
		super.dispose();
	}

	@Override
	public boolean outlineSelectionChanged(Object selectedElement) {
		if (selectedElement instanceof Service) {
			return true;
		}

		if ((selectedElement instanceof ServiceSequence) ||
				(selectedElement instanceof ServiceTransaction) ||
				(selectedElement instanceof InputPrimitive)||
				(selectedElement instanceof OutputPrimitive)){ 
			serviceSequencesViewer.setSelection(new StructuredSelection(
					selectedElement), true);
			return true;
		}

		return false;
	}

		
	private ComposedAdapterFactory getAdapterFactory() {
		
		if(null == adapterFactory){
			adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

			adapterFactory.addAdapterFactory(new ResourceItemProviderAdapterFactory());
			adapterFactory.addAdapterFactory(new PaletteItemProviderAdapterFactory());
			adapterFactory.addAdapterFactory(new LibraryElementItemProviderAdapterFactory());
			adapterFactory.addAdapterFactory(new UiItemProviderAdapterFactory());
			adapterFactory.addAdapterFactory(new DataItemProviderAdapterFactory());
			adapterFactory.addAdapterFactory(new EcoreItemProviderAdapterFactory());
			adapterFactory.addAdapterFactory(new NotationItemProviderAdapterFactory());
			adapterFactory.addAdapterFactory(new ReflectiveItemProviderAdapterFactory());
		}
			
		return adapterFactory;	
	}

	protected void initializeEditingDomain() {
		// Create the command stack that will notify this editor as commands are executed.
		//
		BasicCommandStack commandStack = new BasicCommandStack();

		// Add a listener to set the most recent command's affected objects to be the selection of the viewer with focus.
		//
//		commandStack.addCommandStackListener
//			(new CommandStackListener() {
//				 public void commandStackChanged(final EventObject event) {
//					 getContainer().getDisplay().asyncExec
//						 (new Runnable() {
//							  public void run() {
//								  firePropertyChange(IEditorPart.PROP_DIRTY);
//
//								  // Try to select the affected objects.
//								  //
//								  Command mostRecentCommand = ((CommandStack)event.getSource()).getMostRecentCommand();
//								  if (mostRecentCommand != null) {
//									  setSelectionToViewer(mostRecentCommand.getAffectedObjects());
//								  }
//								  if (propertySheetPage != null && !propertySheetPage.getControl().isDisposed()) {
//									  propertySheetPage.refresh();
//								  }
//							  }
//						  });
//				 }
//			 });

		// Create the editing domain with a special command stack.
		//
		editingDomain = new AdapterFactoryEditingDomain(getAdapterFactory(), commandStack, new HashMap<Resource, Boolean>());
	}

	protected void createContextMenuFor(StructuredViewer viewer) {
		MenuManager contextMenu = new MenuManager("#PopUp");
		contextMenu.add(new Separator("additions"));
		contextMenu.setRemoveAllWhenShown(true);
		contextMenu.addMenuListener(this);
		Menu menu= contextMenu.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(contextMenu, new UnwrappingSelectionProvider(viewer));

		int dndOperations = DND.DROP_COPY | DND.DROP_MOVE | DND.DROP_LINK;
		Transfer[] transfers = new Transfer[] { LocalTransfer.getInstance() };
		viewer.addDragSupport(dndOperations, transfers, new ViewerDragAdapter(viewer));
		viewer.addDropSupport(dndOperations, transfers, new EditingDomainViewerDropAdapter(editingDomain, viewer));
	}

	@Override
	public void menuAboutToShow(IMenuManager manager) {
		EditorActionBarContributor actionbarcontributer = (EditorActionBarContributor)((MultiPageEditorSite)getEditorSite()).getMultiPageEditor().getEditorSite().getActionBarContributor();
		((IMenuListener)actionbarcontributer).menuAboutToShow(manager);
	}
	
	private CommandStack commandStack;
	
	@Override
	public void setCommonCommandStack(CommandStack commandStack) {
		this.commandStack = commandStack;
	}


	private void selectNewSequence(ServiceSequence selectedSequence) {
		((SequenceRootEditPart)getGraphicalViewer().getRootEditPart().getContents()).setSelectedSequence(selectedSequence);
	}
	
	protected ServiceSequence getSelectedSequence() {
		return ((SequenceRootEditPart)getGraphicalViewer().getRootEditPart().getContents()).getSelectedSequence();
	}

}
