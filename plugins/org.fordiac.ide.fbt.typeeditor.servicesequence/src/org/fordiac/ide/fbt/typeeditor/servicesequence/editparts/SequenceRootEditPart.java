/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.editparts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.FreeformLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.gef.editpolicies.RootComponentEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.fordiac.ide.fbt.typeeditor.servicesequence.commands.CreateTransactionCommand;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServiceSequenceView;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServicesequenceFactory;
import org.fordiac.ide.gef.policies.EmptyXYLayoutEditPolicy;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.ServiceSequence;
import org.fordiac.ide.model.ui.View;

/**
 * Edit Part for the visualization of FBs.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class SequenceRootEditPart extends AbstractGraphicalEditPart {

	/** The adapter. */
	private EContentAdapter adapter;

	private ServiceSequence selectedSequence;

	private ServiceSequenceView view;

	/**
	 * Instantiates a new sequence root edit part.
	 * 
	 * @param editor the editor
	 */
	public SequenceRootEditPart() {
	}

	/**
	 * Creates the <code>Figure</code> to be used as this part's <i>visuals</i>.
	 * 
	 * @return a figure
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		Figure f = new FreeformLayer();
		f.setBorder(new MarginBorder(10));
		f.setLayoutManager(new FreeformLayout());
		f.setOpaque(false);

		return f;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#activate()
	 */
	@Override
	public void activate() {
		if (!isActive()) {
			super.activate();
			((Notifier) getModel()).eAdapters().add(getContentAdapter());

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#deactivate()
	 */
	@Override
	public void deactivate() {
		if (isActive()) {
			super.deactivate();
			((Notifier) getModel()).eAdapters().remove(getContentAdapter());

		}
	}

	/**
	 * Gets the content adapter.
	 * 
	 * @return the content adapter
	 */
	public EContentAdapter getContentAdapter() {
		if (adapter == null) {
			adapter = new EContentAdapter() {
				@Override
				public void notifyChanged(final Notification notification) {
					int type = notification.getEventType();
					switch (type) {
					case Notification.ADD:
					case Notification.ADD_MANY:
					case Notification.REMOVE:
					case Notification.REMOVE_MANY:
						refreshChildren();
						break;
					case Notification.SET:
						break;
					}
				}
			};
		}
		return adapter;
	}

	/**
	 * Creates the EditPolicies used for this EditPart.
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.COMPONENT_ROLE,
				new RootComponentEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE,
				new EmptyXYLayoutEditPolicy() {
					@Override
					protected Command getCreateCommand(CreateRequest request) {
						Object type = request.getNewObjectType();

						if (type.equals("ServiceTransaction")) { //$NON-NLS-1$
							CreateTransactionCommand cmd = new CreateTransactionCommand(
									view, true);
							return cmd;
						}
						if (type.equals("LeftInputPrimitive")){
							CreateTransactionCommand cmd = new CreateTransactionCommand(
									(ServiceSequenceView) view, true);
							return cmd;
						}
						if (type.equals("RightInputPrimitive")){
							CreateTransactionCommand cmd = new CreateTransactionCommand(
									(ServiceSequenceView) view, false);
							return cmd;
						}
						return null;
					}
				});

	}

	/**
	 * returns the model object as <code>FBNetwork</code>.
	 * 
	 * @return FBNetwork to be visualized
	 */
	public FBType getCastedModel() {
		return (FBType) getModel();
	}

	/**
	 * Returns the children of the FBNetwork.
	 * 
	 * @return the list of children s
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#getModelChildren()
	 */
	@Override
	protected List<?> getModelChildren() {
		ArrayList<View> temp = new ArrayList<View>();
		if (selectedSequence != null) {
			view = ServicesequenceFactory.eINSTANCE.createServiceSequenceView();
			view.setServiceSequence(selectedSequence);
			temp.add(view);

		}
		return temp;
	}

	public void setSelectedSequence(ServiceSequence newSelectedSequence) {
		this.selectedSequence = newSelectedSequence;
		refresh();
	}

	public ServiceSequence getSelectedSequence() {
		return selectedSequence;
	}

}
