/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.util;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.*;

import org.fordiac.ide.model.ui.View;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * 
 * @see org.fordiac.ide.fbt.typeeditor.servicesequence.ui.servicesequence.ServicesequencePackage
 * @generated
 */
public class ServicesequenceSwitch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ServicesequencePackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ServicesequenceSwitch() {
		if (modelPackage == null) {
			modelPackage = ServicesequencePackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param theEObject the the e object
	 * 
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * 
	 * @generated
	 */
	public T doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ServicesequencePackage.SERVICE_SEQUENCE_VIEW: {
				ServiceSequenceView serviceSequenceView = (ServiceSequenceView)theEObject;
				T result = caseServiceSequenceView(serviceSequenceView);
				if (result == null) result = caseView(serviceSequenceView);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServicesequencePackage.SERVICE_INTERFACE_VIEW: {
				ServiceInterfaceView serviceInterfaceView = (ServiceInterfaceView)theEObject;
				T result = caseServiceInterfaceView(serviceInterfaceView);
				if (result == null) result = caseView(serviceInterfaceView);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServicesequencePackage.INPUT_PRIMITIVE_VIEW: {
				InputPrimitiveView inputPrimitiveView = (InputPrimitiveView)theEObject;
				T result = caseInputPrimitiveView(inputPrimitiveView);
				if (result == null) result = caseView(inputPrimitiveView);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServicesequencePackage.OUTPUT_PRIMITIVE_VIEW: {
				OutputPrimitiveView outputPrimitiveView = (OutputPrimitiveView)theEObject;
				T result = caseOutputPrimitiveView(outputPrimitiveView);
				if (result == null) result = caseView(outputPrimitiveView);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Service Sequence View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object the target of the switch.
	 * 
	 * @return the result of interpreting the object as an instance of '<em>Service Sequence View</em>'.
	 * 
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServiceSequenceView(ServiceSequenceView object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Service Interface View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object the target of the switch.
	 * 
	 * @return the result of interpreting the object as an instance of '<em>Service Interface View</em>'.
	 * 
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServiceInterfaceView(ServiceInterfaceView object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Input Primitive View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object the target of the switch.
	 * 
	 * @return the result of interpreting the object as an instance of '<em>Input Primitive View</em>'.
	 * 
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInputPrimitiveView(InputPrimitiveView object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Output Primitive View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object the target of the switch.
	 * 
	 * @return the result of interpreting the object as an instance of '<em>Output Primitive View</em>'.
	 * 
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOutputPrimitiveView(OutputPrimitiveView object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object the target of the switch.
	 * 
	 * @return the result of interpreting the object as an instance of '<em>View</em>'.
	 * 
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseView(View object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @param object the target of the switch.
	 * 
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * 
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object) {
		return null;
	}

} //ServicesequenceSwitch
