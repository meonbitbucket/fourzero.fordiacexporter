/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.fbt.typeeditor.servicesequence.ServiceInterfacePaletteFactory;
import org.fordiac.ide.fbt.typeeditor.servicesequence.editparts.TransactionElement;
import org.fordiac.ide.model.libraryElement.InputPrimitive;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.OutputPrimitive;

/**
 * The Class CreatePrimitiveCommand.
 */
public class CreatePrimitiveCommand extends Command {

	private final String type;
	private final TransactionElement element;

	/**
	 * Instantiates a new creates the primitive command.
	 * 
	 * @param type the type
	 * @param element the element
	 */
	public CreatePrimitiveCommand(String type, TransactionElement element) {
		this.type = type;
		this.element = element;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		if (type == null || element == null) {
			return false;
		}
		if (type.equals(ServiceInterfacePaletteFactory.LEFT_INPUT_PRIMITIVE)
				|| type.equals(ServiceInterfacePaletteFactory.RIGHT_INPUT_PRIMITIVE)
				&& element.getTransaction().getInputPrimitive() != null) {
			return false;
		}

		return true;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {

		if (type.equals(ServiceInterfacePaletteFactory.LEFT_INPUT_PRIMITIVE)) {
			InputPrimitive inputPrimitive = LibraryElementFactory.eINSTANCE
					.createInputPrimitive();
			inputPrimitive.setInterface(element.getLeftInterface()
					.getServiceInterface());
			inputPrimitive.setEvent("INIT");
			element.getTransaction().setInputPrimitive(inputPrimitive);
		} else if (type
				.equals(ServiceInterfacePaletteFactory.LEFT_OUTPUT_PRIMITIVE)) {
			OutputPrimitive outputPrimitive = LibraryElementFactory.eINSTANCE
					.createOutputPrimitive();
			outputPrimitive.setInterface(element.getLeftInterface()
					.getServiceInterface());
			outputPrimitive.setEvent("INITO");
			element.getTransaction().getOutputPrimitive().add(outputPrimitive);

		} else if (type
				.equals(ServiceInterfacePaletteFactory.RIGHT_INPUT_PRIMITIVE)) {
			InputPrimitive inputPrimitive = LibraryElementFactory.eINSTANCE
					.createInputPrimitive();
			inputPrimitive.setInterface(element.getRightInterface()
					.getServiceInterface());
			inputPrimitive.setEvent("INIT");
			element.getTransaction().setInputPrimitive(inputPrimitive);

		} else if (type
				.equals(ServiceInterfacePaletteFactory.RIGHT_OUTPUT_PRIMITIVE)) {
			OutputPrimitive outputPrimitive = LibraryElementFactory.eINSTANCE
					.createOutputPrimitive();
			outputPrimitive.setInterface(element.getRightInterface()
					.getServiceInterface());
			outputPrimitive.setEvent("INITO");
			element.getTransaction().getOutputPrimitive().add(outputPrimitive);

		}

		super.execute();
	}
}
