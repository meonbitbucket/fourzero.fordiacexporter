/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.editparts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.gef.editparts.AbstractConnectionEditPart;

/**
 * The Class ConnectionEditPart.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class PrimitiveConnectionEditPart extends AbstractConnectionEditPart {

	/**
	 * Gets the casted model.
	 * 
	 * @return the casted model
	 */
	public PrimitiveConnection getCastedModel() {
		return (PrimitiveConnection) getModel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		// // Selection handle edit policy.
		// // Makes the connection show a feedback, when selected by the user.
		// installEditPolicy(EditPolicy.CONNECTION_ENDPOINTS_ROLE,
		// new FeedbackConnectionEndpointEditPolicy());
		//
		// // Allows the removal of the connection model element
		// installEditPolicy(EditPolicy.CONNECTION_ROLE,
		// new DeleteConnectionEditPolicy());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractConnectionEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {

		PolylineConnection connection = (PolylineConnection) super
				.createFigure();
		PolygonDecoration arrowRect = new PolygonDecoration();

		PointList pl = new PointList();
		pl.addPoint(-4, -4);
		pl.addPoint(-4, 4);
		pl.addPoint(4, 4);
		pl.addPoint(4, -4);
		pl.addPoint(-4, -4);
		pl.addPoint(-4, 0);
		pl.addPoint(-11, -4);
		pl.addPoint(-11, 4);
		pl.addPoint(-4, 0);
		arrowRect.setTemplate(pl);
		arrowRect.setScale(1, 1);

		PolygonDecoration rect = new PolygonDecoration();

		pl = new PointList();
		pl.addPoint(-4, -4);
		pl.addPoint(-4, 4);
		pl.addPoint(4, 4);
		pl.addPoint(4, -4);
		pl.addPoint(-4, -4);
		rect.setTemplate(pl);
		rect.setScale(1, 1);

		PolygonDecoration arrow = new PolygonDecoration();

		pl = new PointList();
		pl.addPoint(0, 0);
		pl.addPoint(-7, -4);
		pl.addPoint(-7, 4);
		pl.addPoint(0, 0);
		arrow.setTemplate(pl);
		arrow.setScale(1, 1);

		if (getCastedModel().isInputPrimitive()) {
			connection.setTargetDecoration(arrowRect);
		} else {
			connection.setSourceDecoration(rect);
			connection.setTargetDecoration(arrow);

		}
		return connection;
	}

}
