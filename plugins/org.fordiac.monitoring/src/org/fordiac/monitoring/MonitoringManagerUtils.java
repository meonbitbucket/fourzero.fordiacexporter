package org.fordiac.monitoring;

import java.util.ArrayList;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EObject;
import org.fordiac.ide.application.editparts.FBEditPart;
import org.fordiac.ide.fbt.typeeditor.network.viewer.CompositeNetworkViewerEditPart;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.libraryElement.ResourceFBNetwork;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.systemmanagement.Activator;

public class MonitoringManagerUtils {

	public static ArrayList<IMonitoringManager> getAllMonitoringManager() {
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IConfigurationElement[] elems = registry
				.getConfigurationElementsFor(
						org.fordiac.monitoring.Activator.PLUGIN_ID,
						"monitoringmanager"); //$NON-NLS-1$
		ArrayList<IMonitoringManager> managers = new ArrayList<IMonitoringManager>();
		for (int i = 0; i < elems.length; i++) {
			IConfigurationElement element = elems[i];
			try {
				Object object = element.createExecutableExtension("Interface"); //$NON-NLS-1$
				if (object instanceof IMonitoringManagerProvider) {
					IMonitoringManagerProvider monitoringManagerProvider = (IMonitoringManagerProvider) object;
					if (monitoringManagerProvider.getMonitoringManager() != null
							&& !managers.contains(monitoringManagerProvider
									.getMonitoringManager())) {
						managers.add(monitoringManagerProvider
								.getMonitoringManager());
					}
				}
			} catch (CoreException corex) {
				Activator.getDefault().logError("Error loading Monitoring Manager Provider!", corex);
			}
		}
		return managers;
	}

	public static IMonitoringManager getMonitoringManager(
			AutomationSystem system) {
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IConfigurationElement[] elems = registry
				.getConfigurationElementsFor(
						org.fordiac.monitoring.Activator.PLUGIN_ID,
						"monitoringmanager"); //$NON-NLS-1$
		IMonitoringManager manager = null;
		for (int i = 0; i < elems.length; i++) {
			IConfigurationElement element = elems[i];
			try {
				Object object = element.createExecutableExtension("Interface"); //$NON-NLS-1$
				if (object instanceof IMonitoringManagerProvider) {
					IMonitoringManagerProvider monitoringManagerProvider = (IMonitoringManagerProvider) object;
					IProject project = system.getProject();
					if (monitoringManagerProvider
							.projectContainsSupportedNature(project)) {
						manager = monitoringManagerProvider
								.getMonitoringManager();
						break;
					}
				}
			} catch (CoreException corex) {
				Activator.getDefault().logError("Error loading Monitoring Manager Provider!", corex);
			}
		}
		if (manager == null) { // return default 61499 Monitoring Manager for
								// backward compatibility
			manager = MonitoringManager.getInstance();
		}
		return manager;
	}

	public static PortElement createPortElement(org.fordiac.ide.gef.editparts.InterfaceEditPart editPart) {
		if (editPart.getParent() instanceof FBEditPart
				&& editPart.getParent().getParent() instanceof CompositeNetworkViewerEditPart) {
			return createCompositeInternalPortString(editPart);
		}

		Object obj = editPart.getCastedModel().getIInterfaceElement().eContainer().eContainer();
		if(obj instanceof FB){
			FB fb = (FB)obj; 
			return createPortElement(fb, editPart);					
		}
		
		return null;

	}

	private static PortElement createPortElement(FB fb,
			org.fordiac.ide.gef.editparts.InterfaceEditPart ep) {
		ResourceFBNetwork resFBNetwork = findResourceFBNetwork(fb);
		if (resFBNetwork == null) {
			return null; // can not be monitored because the fb is not mapped
		}

		Resource res = (Resource) resFBNetwork.eContainer();
		if (res == null) {
			return null;
		}
		Device dev = (Device) res.eContainer();
		if (dev == null) {
			return null;
		}
		
		AutomationSystem system = dev.getAutomationSystem();

		PortElement p = new PortElement();
		p.system = system;
		p.setDevice(dev);
		p.setResource(res);
		p.setFb(fb);
		p.setInterfaceElement(ep.getCastedModel().getIInterfaceElement());
		return p;
	}

	private static PortElement createCompositeInternalPortString(
			org.fordiac.ide.gef.editparts.InterfaceEditPart editPart) {
		
		CompositeNetworkViewerEditPart cnep = (CompositeNetworkViewerEditPart) editPart
				.getParent().getParent();

		ArrayList<CompositeNetworkViewerEditPart> parents = new ArrayList<CompositeNetworkViewerEditPart>();

		CompositeNetworkViewerEditPart root = cnep.getparentInstanceViewerEditPart(); 

		while (root.getparentInstanceViewerEditPart() != null) {
			parents.add(0, root.getparentInstanceViewerEditPart());
			root = root.getparentInstanceViewerEditPart();
		}

		FB fb = root.getFbInstance();
		PortElement pe = createPortElement(fb, editPart);
		if (pe != null) {
			pe.setFb(cnep.getFbInstance());

			for (CompositeNetworkViewerEditPart compositeNetworkEditPart : parents) {
				pe.getHierarchy().add(
						compositeNetworkEditPart.getFbInstance().getName());
			}
			return pe;
		}
		return null;
	}

	private static ResourceFBNetwork findResourceFBNetwork(FB fb) {
		EObject container = fb.eContainer();
		
		if(container instanceof ResourceFBNetwork){
			//we have a resource FB
			return (ResourceFBNetwork)container;
		}
		
		while (!(container instanceof FBNetwork)) {
			if (container instanceof SubAppNetwork) {
				ResourceFBNetwork resourceNetwork = ((SubAppNetwork) container)
						.getParentSubApp().getResource();
				if (resourceNetwork != null) {
					return resourceNetwork;
				}
				container = ((SubAppNetwork) container).getParentSubApp()
						.eContainer();
			}
		}
		return fb.getResource();
	}
}
