package org.fordiac.monitoring.decorators;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DecorationOverlayIcon;
import org.eclipse.jface.viewers.ILabelDecorator;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.swt.graphics.Image;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.monitoring.Activator;
import org.fordiac.monitoring.MonitoringManagerUtils;

public class SystemMonitoringDecorator implements ILabelDecorator {

	@Override
	public void addListener(ILabelProviderListener listener) {
	}

	@Override
	public void dispose() {
		if(null != overlayImage){
			overlayImage.dispose();
		}
	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {

	}

	@Override
	public Image decorateImage(Image image, Object element) {
		Image retval;

		if ((element instanceof AutomationSystem)
				&& (MonitoringManagerUtils
						.getMonitoringManager((AutomationSystem) element)
						.containsSystem(((AutomationSystem) element).getName()))) {
			retval = getOverlayImage(image);
		} else {
			retval = image;
		}

		return retval;
	}
	
	private Image overlayImage = null;

	private Image getOverlayImage(Image image) {
		if(null == overlayImage){
			ImageDescriptor imageDescriptor = Activator.getImageDescriptor("images/monitoring_decorator.gif");
		
			DecorationOverlayIcon DOC = new DecorationOverlayIcon(image, imageDescriptor, org.eclipse.jface.viewers.IDecoration.TOP_LEFT);
			overlayImage = DOC.createImage();
		}
		
		return overlayImage;
	}

	@Override
	public String decorateText(String text, Object element) {
		return null;
	}
}
