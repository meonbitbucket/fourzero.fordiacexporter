/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.monitoring;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.model.gen.monitoring.MonitoringElement;
import org.fordiac.model.gen.monitoring.MonitoringFactory;
import org.fordiac.monitoring.communication.MonitorInformation;
import org.fordiac.monitoring.communication.TCPCommunicationObject;
import org.fordiac.systemmanagement.Activator;
import org.fordiac.systemmanagement.SystemManager;


/**
 * Singleton instance that Coordinates and manages all the Ports to be
 * monitored.
 * 
 * @author gebenh
 */
public class MonitoringManager extends AbstractMonitoringManager {


	/** The instance. */
	private static MonitoringManager instance;



	/** The enabled systems. */
	private final HashSet<String> enabledSystems = new HashSet<String>();

	/** The element mapping. */
	private final Hashtable<String, MonitoringElement> elementMapping = new Hashtable<String, MonitoringElement>();

	/**
	 * Gets the single instance of MonitoringManager.
	 * 
	 * @return single instance of MonitoringManager
	 */
	public static MonitoringManager getInstance() {
		if (instance == null) {
			instance = new MonitoringManager();
		}
		return instance;
	}

	/**
	 * Instantiates a new monitoring manager.
	 */
	private MonitoringManager() {
	}


	/**
	 * Notify enable system.
	 * 
	 * @param system
	 *            the system
	 */
	private void notifyEnableSystem(AutomationSystem system) {
		for (Iterator<IMonitoringListener> iterator = monitoringListeners.iterator(); iterator
				.hasNext();) {
			IMonitoringListener monitoringListener = (IMonitoringListener) iterator
					.next();
			monitoringListener.notifyEnableMonitoring(system);
		}
	}

	/**
	 * Notify disable system.
	 * 
	 * @param system
	 *            the system
	 */
	private void notifyDisableSystem(AutomationSystem system) {
		for (Iterator<IMonitoringListener> iterator = monitoringListeners.iterator(); iterator
				.hasNext();) {
			IMonitoringListener monitoringListener = (IMonitoringListener) iterator
					.next();
			monitoringListener.notifyDisableMonitoring(system);
		}
	}

	/**
	 * Notify add port.
	 * 
	 * @param port
	 *            the port
	 */
	private void notifyAddPort(String port) {
		for (Iterator<IMonitoringListener> iterator = monitoringListeners.iterator(); iterator
				.hasNext();) {
			IMonitoringListener monitoringListener = (IMonitoringListener) iterator
					.next();
			monitoringListener.notifyAddPort(port);
		}
	}

	/**
	 * Notify remove port.
	 * 
	 * @param port
	 *            the port
	 */
	private void notifyRemovePort(String port) {
		for (Iterator<IMonitoringListener> iterator = monitoringListeners.iterator(); iterator
				.hasNext();) {
			IMonitoringListener monitoringListener = (IMonitoringListener) iterator
					.next();
			monitoringListener.notifyRemovePort(port);
		}
	}

	/**
	 * Notify trigger event.
	 * 
	 * @param port
	 *            the port
	 */
	private void notifyTriggerEvent(String port) {
		for (Iterator<IMonitoringListener> iterator = monitoringListeners.iterator(); iterator
				.hasNext();) {
			IMonitoringListener monitoringListener = (IMonitoringListener) iterator
					.next();
			monitoringListener.notifyTriggerEvent(port);
		}
	}

	/**
	 * Adds the element mapping.
	 * 
	 * @param port
	 *            the port
	 * @param element
	 *            the element
	 */
	public void addElementMapping(String port, MonitoringElement element) {
		elementMapping.put(port, element);
	}

	/**
	 * Gets the monitoring element.
	 * 
	 * @param port
	 *            the port
	 * 
	 * @return the monitoring element
	 */
	public MonitoringElement getMonitoringElement(String port) {
		if(port != null){
			return elementMapping.get(port);
		}
		else{
			return null;
		}
	}

	/**
	 * Adds the port to the monitoring elements.
	 * 
	 * @param port
	 *            the port
	 */
	public void addPort(PortElement port) {
		String portName = port.getPortString();
		elementsToMonitor.add(portName);
		MonitoringElement element = createMonitoringElement(port);
		notifyWatchesAdapterPortAdded(portName);

		if (element.getInterfaceElement() instanceof VarDeclaration) {
			addWatch(port.getSystem(), port.getDevice(), element);
		} else if (element.getInterfaceElement() instanceof Event) {
			addWatch(port.getSystem(), port.getDevice(), element);
		}
		notifyAddPort(portName);
	}

	/**
	 * Removes the port.
	 * 
	 * @param port
	 *            the port
	 */
	public void removePort(PortElement port) {
		String portName = port.getPortString();
		MonitoringElement element = createMonitoringElement(port);
		elementsToMonitor.remove(portName);
		watches.getWatches().remove(element);
		elementMapping.remove(portName);
		notifyWatchesAdapterPortRemoved(portName);		
		removeWatch(port.getSystem(), port.getDevice(), element);
		notifyRemovePort(portName);
	}



	/**
	 * Contains port.
	 * 
	 * @param port
	 *            the port
	 * 
	 * @return true, if successful
	 */
	public boolean containsPort(String port) {
		return elementsToMonitor.contains(port);
	}

	/** The open communication. */
	private final Hashtable<String, TCPCommunicationObject> openCommunication = new Hashtable<String, TCPCommunicationObject>();

	/**
	 * Enable system.
	 * 
	 * @param system
	 *            the system
	 */
	public void enableSystem(String system) {
		// get system from the SystemManager
		AutomationSystem automationSystem = SystemManager.getInstance()
				.getSystemForName(system);
		if (automationSystem == null) {
			Activator.getDefault().logError(system + " could not be found.");
			return;
		} else {
			enabledSystems.add(system);
			// search for devices with monitoring resource and create a
			// TCPCommunicationObject
			// TODO make TCPCommunicationObject more general and select the
			// required communication method from a device property - maybe add
			// an extension point for this
			for (Iterator<?> iterator = automationSystem.getSystemConfiguration()
					.getSystemConfigurationNetwork().getDevices().iterator(); iterator
					.hasNext();) {
				Device dev = (Device) iterator.next();
				MonitorInformation monitorInfo = getMonitoringInfo(
						automationSystem, dev);
				if (monitorInfo != null) {
					if (!openCommunication.containsKey(monitorInfo.toString())) {
						TCPCommunicationObject commObject = new TCPCommunicationObject(
								monitorInfo);
						commObject.enable();
						openCommunication.put(monitorInfo.toString(),
								commObject);
					} else {
						TCPCommunicationObject object = openCommunication
								.get(monitorInfo.toString());
						object.enable();
					}
				} else {
					Activator.getDefault().logInfo(dev.getName() + " has no monitoring resource.");
				}
			}
			notifyEnableSystem(automationSystem);
		}
	}

	/**
	 * Gets the monitoring info.
	 * 
	 * @param automationSystem
	 *            the automation system
	 * @param dev
	 *            the dev
	 * 
	 * @return the monitoring info
	 */
	private MonitorInformation getMonitoringInfo(
			AutomationSystem automationSystem, Device dev) {
		for (Iterator<VarDeclaration> iterator = dev.getVarDeclarations()
				.iterator(); iterator.hasNext();) {
			VarDeclaration varDecl = iterator.next();
			if (varDecl.getName().equalsIgnoreCase("MGR_ID")) {
				if (varDecl.getValue() != null) {
					String val = varDecl.getValue().getValue();
					if (val.startsWith("%")) {
						String replacedValue = SystemManager.getInstance()
								.getReplacedString(automationSystem, dev, val);
						if (replacedValue != null) {
							return new MonitorInformation(replacedValue);
						}
					}
					// System.out.println("return val: " + val);
					return new MonitorInformation(val);
				}
			}
		}

		return null;
	}
	
	private MonitorInformation getMonitoringInfo(PortElement port){
		String portString = port.getPortString();
		AutomationSystem automationSystem = port.getSystem(); 

		if (automationSystem == null) {
			Activator.getDefault().logError("System could not be found for triggering event (." + portString + ")");
			MessageDialog.openError(Display.getDefault().getActiveShell(),
					"Error",
					"System could not be found for triggering event (." + portString + ")");
			return null;
		}
		Device device = port.getDevice();
		if (device == null) {
			Activator.getDefault().logError("Device could not be found for triggering event (." + portString + ")");
			MessageDialog.openError(Display.getDefault().getActiveShell(),
					"Error",
					"Device could not be found for triggering event (." + portString + ")");
			return null;
		}
		return getMonitoringInfo(automationSystem, device);
	}

	/**
	 * Disable system.
	 * 
	 * @param system
	 *            the system
	 */
	public void disableSystem(String system) {
		AutomationSystem automationSystem = SystemManager.getInstance()
				.getSystemForName(system);
		if (automationSystem == null) {
			Activator.getDefault().logError(system + " could not be found for deactivating monitoring.");
			return;
		} else {
			notifyDisableSystem(automationSystem);
			enabledSystems.remove(system);
			for (Iterator<?> iterator = automationSystem.getSystemConfiguration()
					.getSystemConfigurationNetwork().getDevices().iterator(); iterator
					.hasNext();) {
				Device dev = (Device) iterator.next();
				MonitorInformation monitorInfo = getMonitoringInfo(
						automationSystem, dev);
				if (monitorInfo != null) {
					if (openCommunication.containsKey(monitorInfo.toString())) {
						TCPCommunicationObject commObject = openCommunication
								.get(monitorInfo.toString());
						commObject.disable();
					}
				} else {
					Activator.getDefault().logInfo(dev.getName() + " has no monitoring resource.");
				}
			}
		}
	}

	/**
	 * Contains system.
	 * 
	 * @param system
	 *            the system
	 * 
	 * @return true, if successful
	 */
	public boolean containsSystem(String system) {
		return enabledSystems.contains(system);
	}

	public enum BreakPoint {
		add, remove, clear
	}

	/**
	 * Trigger event.
	 * 
	 * @param port
	 *            the port
	 */
	public void toggleBreakpoint(PortElement port, BreakPoint set) {
		MonitoringElement element = createMonitoringElement(port);
		
		MonitorInformation monitorInfo = getMonitoringInfo(port);
		if(null != monitorInfo){
			TCPCommunicationObject commObject = openCommunication.get(monitorInfo
					.toString());
			commObject.toggleBreakpoint(element, set);
			if (set.equals(BreakPoint.add)) {
				element.setBreakpoint(true);
				breakpoints.getBreakpoints().add(element);
			} else if (set.equals(BreakPoint.remove)) {
				element.setBreakpoint(false);
				breakpoints.getBreakpoints().remove(element);
			} else if (set.equals(BreakPoint.clear)) {
			}
		}
	}

	/**
	 * Trigger event.
	 * 
	 * @param port
	 *            the port
	 */
	public void triggerEvent(PortElement port) {
		MonitoringElement element = createMonitoringElement(port);
		
		MonitorInformation monitorInfo = getMonitoringInfo(port);
		TCPCommunicationObject commObject = openCommunication.get(monitorInfo.toString());
		if(commObject != null){
			commObject.triggerEvent(element);
			notifyTriggerEvent(port.getPortString());
		}
	}

	/**
	 * Adds the watch in the runtime system.
	 * 
	 * @param system
	 *            the system
	 * @param device
	 *            the device
	 * @param element
	 *            the element
	 */
	public void addWatch(AutomationSystem system, Device device,
			MonitoringElement element) {
		MonitorInformation monitorInfo = getMonitoringInfo(system, device);
		TCPCommunicationObject commObject = openCommunication.get(monitorInfo
				.toString());
		if(null != commObject){
			commObject.addWatch(element);
		}
	}

	/**
	 * Start event cnt in the runtime system.
	 * 
	 * @param system
	 *            the system
	 * @param device
	 *            the device
	 * @param element
	 *            the element
	 */
	public void startEventCnt(AutomationSystem system, Device device,
			MonitoringElement element) {
		MonitorInformation monitorInfo = getMonitoringInfo(system, device);
		TCPCommunicationObject commObject = openCommunication.get(monitorInfo
				.toString());
		commObject.startEventCnt(element);

	}

	/**
	 * Gets the comm object.
	 * 
	 * @param system
	 *            the system
	 * @param device
	 *            the device
	 * 
	 * @return the comm object
	 */
	public TCPCommunicationObject getCommObject(AutomationSystem system,
			Device device) {

		MonitorInformation monitorInfo = getMonitoringInfo(system, device);
		if (monitorInfo != null) {
			return openCommunication.get(monitorInfo.toString());
		}
		return null;
	}

	/**
	 * Send req.
	 * 
	 * @param commObject
	 *            the comm object
	 * @param system
	 *            the system
	 * @param device
	 *            the device
	 */
	public void sendReq(TCPCommunicationObject commObject,
			AutomationSystem system, Device device) {
		commObject.sendReq(system, device);
	}

	public void queryBreakpoints(TCPCommunicationObject commObject,
			AutomationSystem system, Device device) {
		commObject.queryBreakpoints(system, device);
	}

	/**
	 * Removes the watch.
	 * 
	 * @param system
	 *            the system
	 * @param device
	 *            the device
	 * @param element
	 *            the element
	 */
	public void removeWatch(AutomationSystem system, Device device,
			MonitoringElement element) {
		MonitorInformation monitorInfo = getMonitoringInfo(system, device);
		if (monitorInfo != null) {
			TCPCommunicationObject commObject = openCommunication.get(monitorInfo.toString());
			if(null != commObject){
				commObject.removeWatch(element);
			}
		}
	}

	/**
	 * Creates the monitoring element.
	 * 
	 * @param port
	 *            the port
	 * 
	 * @return the monitoring element
	 */
	private MonitoringElement createMonitoringElement(PortElement port) {
		MonitoringElement element = null;
		if (port != null) {
			String portName =  port.getPortString();
			
			if (!elementMapping.containsKey(portName)) {
				
				element = MonitoringFactory.eINSTANCE
						.createMonitoringElement();
				watches.getWatches().add(element);
				// element.addForceValueListener(this);
				
				element.setMonitoringElement(portName);
				elementMapping.put(portName, element);
				addElementMapping(portName, element);
				
				element.setFb(port.getFb());
				element.setInterfaceElement(port.getInterfaceElement());
			}
			else{
				element = elementMapping.get(portName);				
			}
		}
		return element;
	}	
	

	public void forceValue(MonitoringElement element, String value) {
		AutomationSystem automationSystem = SystemManager.getInstance()
				.getSystemForName(element.getSystemString());

		if (automationSystem == null) {
			Activator.getDefault().logError("System could not be found to force value (" + element.getMonitoringElement() + ").");
			MessageDialog.openError(Display.getDefault().getActiveShell(),
					"Error", "System could not be found for triggering event ("
							+ element.getMonitoringElement() + ").");
			return;
		}
		Device device = automationSystem.getDeviceForName(element
				.getDeviceString());
		if (device == null) {
			Activator.getDefault().logError("Device could not be found for triggering event (" + element.getMonitoringElement() + ").");
			MessageDialog.openError(Display.getDefault().getActiveShell(),
					"Error", "Device could not be found for triggering event ("
							+ element.getMonitoringElement() + ").");
			return;
		}
		MonitorInformation monitorInfo = getMonitoringInfo(automationSystem,
				device);
		element.forceValue(value);
		TCPCommunicationObject commObject = openCommunication.get(monitorInfo
				.toString());

		if(commObject != null){
			if (element.isForce()) {
				commObject.forceValue(element, value);
			} else {
				commObject.clearForce(element);
				
			}
		}
	}
}
