package org.fordiac.monitoring;

import java.util.HashSet;

import org.eclipse.emf.ecore.util.EContentAdapter;
import org.fordiac.model.gen.monitoring.MonitoringElement;

public interface IMonitoringManager {

	public void registerMonitoringListener(IMonitoringListener listener);
	
	public void addBreakpointsAdapter(EContentAdapter adapter);

	public void removeBreakpointsAdapter(EContentAdapter adapter);

	public void addWatchesAdapter(IMonitoringListener adapter);

	public void removeWatchesAdapter(IMonitoringListener adapter);
	
	public void enableSystem(String system);
	
	public void disableSystem(String system);
	
	public boolean containsSystem(String system);

	public MonitoringElement getMonitoringElement(String port);
	
	public boolean containsPort(String port);
	
	public void removePort(PortElement port);
	
	public void addPort(PortElement port);
	
	public HashSet<String> getElementsToMonitor() ;
}
