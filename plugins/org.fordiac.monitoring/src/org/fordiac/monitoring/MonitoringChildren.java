/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.monitoring;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.fordiac.ide.gef.IEditPartCreator;
import org.fordiac.ide.gef.editparts.IChildrenProvider;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.model.gen.monitoring.MonitoringElement;
import org.fordiac.monitoring.communication.TCPCommunicationObject;
import org.fordiac.monitoring.preferences.PreferenceConstants;
import org.fordiac.systemmanagement.Activator;


public class MonitoringChildren implements IMonitoringListener,
		IChildrenProvider, IForceValueListener {

	public MonitoringChildren() {
		MonitoringManager.getInstance().registerMonitoringListener(this);
	}

	@Override
	public ArrayList<IEditPartCreator> getChildren(Diagram diagram) {
		ArrayList<IEditPartCreator> arrayList = new ArrayList<IEditPartCreator>();
		monitoringElements.clear();
		for (Iterator<String> iterator = MonitoringManager.getInstance()
				.getElementsToMonitor().iterator(); iterator.hasNext();) {
			String port = iterator.next();
			MonitoringElement element = MonitoringManager.getInstance()
					.getMonitoringElement(port);
			
			
			if(null != element){
				if(diagram.getFunctionBlockNetwork().getFBs().contains(element.getFb())){
					arrayList.add(element);
					monitoringElements.put(port, element);	
				}
				else if(null != element.getFb().getResource() && (!element.getFb().isResourceFB())){
					//check if we are in the resource diagram editor for a mapped FB	
					 if(((Resource)element.getFb().getResource().eContainer()).getFBNetwork().equals(diagram.getFunctionBlockNetwork())){
						arrayList.add(element);
						monitoringElements.put(port, element);
					 }
				}
			}
			
		}
		return arrayList;
	}

	private final Hashtable<String, MonitoringElement> monitoringElements = new Hashtable<String, MonitoringElement>();

	private final Hashtable<String, Boolean> runningMonitoring = new Hashtable<String, Boolean>();

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public void notifyEnableMonitoring(AutomationSystem automationSystem) {
		for (Iterator<Device> iterator = automationSystem.getSystemConfiguration()
				.getSystemConfigurationNetwork().getDevices().iterator(); iterator
				.hasNext();) {
			Device dev = iterator.next();

			// addWatches
			AddWatchesRunnable addWatches = new AddWatchesRunnable(automationSystem,
					dev);
			Shell shell = Display.getDefault().getActiveShell();
			try {
				new ProgressMonitorDialog(Display.getDefault().getActiveShell()).run(
						true, true, addWatches);
			} catch (InvocationTargetException ex) {
				MessageDialog.openError(shell, "Error", ex.getMessage());
			} catch (InterruptedException ex) {
				MessageDialog.openInformation(shell, "Download Aborted",
						"Download Aborted");
			}

			Thread t = new Thread(
					new TriggerRequestRunnable(automationSystem, dev));
			runningMonitoring.put(automationSystem.getName(), true);
			t.start();

		}
	}

	class TriggerRequestRunnable implements Runnable {

		private final AutomationSystem system;
		private final Device device;

		public TriggerRequestRunnable(AutomationSystem system, Device device) {
			this.system = system;
			this.device = device;
		}

		int i = 0;

		@Override
		public void run() {
			Boolean running = runningMonitoring.get(system.getName());
			TCPCommunicationObject commObj = MonitoringManager.getInstance()
					.getCommObject(system, device);
			if (commObj != null) {
				int pollingIntervall = Activator.getDefault().getPreferenceStore()
						.getInt(PreferenceConstants.P_POLLING_INTERVAL);
				while (running != null && running) {
					try {
						Thread.sleep(pollingIntervall);
					} catch (InterruptedException e) {
						Activator.getDefault().logError(e.getMessage(), e);
					}
					if (monitoringElements.size() > 0) {
						MonitoringManager.getInstance().sendReq(commObj, system, device);
						MonitoringManager.getInstance().queryBreakpoints(commObj, system,
								device);
					}
					running = runningMonitoring.get(system.getName());
				}
			}
		}
	}

	class AddWatchesRunnable implements IRunnableWithProgress {
		private final AutomationSystem system;
		private final Device device;

		public AddWatchesRunnable(AutomationSystem system, Device device) {
			this.device = device;
			this.system = system;
		}

		@Override
		public void run(IProgressMonitor monitor) throws InvocationTargetException,
				InterruptedException {
			int count = 0;
			for (Iterator<String> iterator = monitoringElements.keySet().iterator(); iterator
					.hasNext();) {
				String key = iterator.next();
				if (key.startsWith(system.getName() + "." + device.getName())) {
					count++;
				}
			}
			monitor.beginTask("Add Watch", count);
			addWatches(system, device, monitor);
			monitor.done();
		}

		private void addWatches(AutomationSystem system, Device device,
				IProgressMonitor monitor) {
			for (Iterator<String> iterator = monitoringElements.keySet().iterator(); iterator
					.hasNext()
					&& !monitor.isCanceled();) {
				String key = (String) iterator.next();
				if (key.startsWith(system.getName() + "." + device.getName())) {
					MonitoringElement element = monitoringElements.get(key);
					monitor.setTaskName("Add watch for: " + element.getPortString());
					if (element.getInterfaceElement() instanceof VarDeclaration) {
						MonitoringManager.getInstance().addWatch(system, device, element);
					}
					if (element.getInterfaceElement() instanceof Event) {
						MonitoringManager.getInstance().addWatch(system, device, element);
						// MonitoringManager.getInstance().startEventCnt(system, device,
						// element);
					}
					monitor.worked(1);
				}
			}
		}
	}

	@Override
	public void notifyAddPort(String port) {
		// nothing to do;
	}

	@Override
	public void notifyRemovePort(String port) {
	}

	@Override
	public void notifyTriggerEvent(String port) {
	}

	@Override
	public void clearForce(MonitoringElement element) {
	}

	@Override
	public void forceValue(MonitoringElement element, String value) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.monitoring.IMonitoringListener#notifyDisableMonitoring(java
	 * .lang.String)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void notifyDisableMonitoring(AutomationSystem automationSystem) {
		// set the system to stop sending requests
		runningMonitoring.put(automationSystem.getName(), false);
		// TODO remove Watches from system?
		for (Iterator iterator = automationSystem.getSystemConfiguration()
				.getSystemConfigurationNetwork().getDevices().iterator(); iterator
				.hasNext();) {
			Device dev = (Device) iterator.next();

			RemoveWatchesRunnable removeWatches = new RemoveWatchesRunnable(
					automationSystem, dev);
			Shell shell = Display.getDefault().getActiveShell();
			try {
				new ProgressMonitorDialog(Display.getDefault().getActiveShell()).run(
						true, true, removeWatches);
			} catch (InvocationTargetException ex) {
				MessageDialog.openError(shell, "Error", ex.getMessage());
			} catch (InterruptedException ex) {
				MessageDialog.openInformation(shell, "Download Aborted",
						"Download Aborted");
			}
		}
		// TODO ask user whether to clear forces?
	}

	class RemoveWatchesRunnable implements IRunnableWithProgress {
		private final AutomationSystem system;
		private final Device device;

		public RemoveWatchesRunnable(AutomationSystem system, Device device) {
			this.device = device;
			this.system = system;
		}

		@Override
		public void run(IProgressMonitor monitor) throws InvocationTargetException,
				InterruptedException {
			int count = 0;
			for (Iterator<String> iterator = monitoringElements.keySet().iterator(); iterator
					.hasNext();) {
				String key = iterator.next();
				if (key.startsWith(system.getName() + "." + device.getName())) {
					count++;
				}
			}
			monitor.beginTask("Remove Watch", count);
			removeWatches(system, device, monitor);
			monitor.done();
		}

		private void removeWatches(AutomationSystem system, Device device,
				IProgressMonitor monitor) {
			Set<String> elements = monitoringElements.keySet();
			for (Iterator<String> iterator = elements.iterator(); iterator.hasNext();) {
				String key = iterator.next();
				if (key.startsWith(system.getName() + "." + device.getName())) {
					MonitoringElement element = monitoringElements.get(key);
					monitor.setTaskName("Remove watch for: " + element.getPortString());
					if (element.getInterfaceElement() instanceof VarDeclaration) {
						// notifyRemovePort(element.getPortString());
						MonitoringManager.getInstance()
								.removeWatch(system, device, element);
					}
					if (element.getInterfaceElement() instanceof Event) {
						// startEventCnt(element, outputStream, inputStream);
						MonitoringManager.getInstance()
								.removeWatch(system, device, element);
					}
					monitor.worked(1);
				}
			}
		}
	}

}
