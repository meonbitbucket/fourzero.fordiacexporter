/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.monitoring.popup.actions;

import java.util.Iterator;

import org.eclipse.gef.EditPart;
import org.eclipse.jface.action.IAction;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.application.editors.FBNetworkEditor;
import org.fordiac.ide.gef.DiagramEditor;
import org.fordiac.ide.resourceediting.editors.ResourceDiagramEditor;

public abstract class AbstractMonitoringAction implements IObjectActionDelegate {

	private DiagramEditor editor;

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		if ((targetPart instanceof FBNetworkEditor)
				|| (targetPart instanceof ResourceDiagramEditor)) {
			DiagramEditor editor = (DiagramEditor) targetPart;
			this.editor = editor;
		}

	}

	@SuppressWarnings("rawtypes")
	protected void refreshEditor() {
		if (null != editor) {
			editor.getViewer().getRootEditPart().refresh();
			for (Iterator iterator = editor.getViewer().getRootEditPart()
					.getChildren().iterator(); iterator.hasNext();) {
				EditPart part = (EditPart) iterator.next();
				part.refresh();
			}
		}
	}
}
