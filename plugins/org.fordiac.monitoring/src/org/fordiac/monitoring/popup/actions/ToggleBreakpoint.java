/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.monitoring.popup.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.gef.editparts.InterfaceEditPart;
import org.fordiac.model.gen.monitoring.MonitoringElement;
import org.fordiac.monitoring.MonitoringManager;
import org.fordiac.monitoring.MonitoringManager.BreakPoint;
import org.fordiac.monitoring.MonitoringManagerUtils;
import org.fordiac.monitoring.PortElement;


public class ToggleBreakpoint implements IObjectActionDelegate {

	public ToggleBreakpoint() {
		// empty constructor
	}

	private StructuredSelection selection;

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		if ((selection).getFirstElement() instanceof InterfaceEditPart) {
			final InterfaceEditPart editPart = (InterfaceEditPart) (selection)
					.getFirstElement();

			PortElement port = MonitoringManagerUtils.createPortElement(editPart);

			MonitoringManager.getInstance().toggleBreakpoint(port,
					action.isChecked() ? BreakPoint.add : BreakPoint.remove);

		}
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		if (selection instanceof StructuredSelection) {
			this.selection = (StructuredSelection) selection;
			if (((StructuredSelection) selection).getFirstElement() instanceof InterfaceEditPart) {
				InterfaceEditPart editPart = (InterfaceEditPart) ((StructuredSelection) selection)
						.getFirstElement();
				PortElement port = MonitoringManagerUtils.createPortElement(editPart);
				
				if (port != null) {

					action.setEnabled(editPart.isEvent());
					MonitoringElement element = MonitoringManager.getInstance()
							.getMonitoringElement(port.getPortString());
					if (element == null) {
						action.setEnabled(false);
					}
					action.setChecked(element != null && element.isBreakpoint());
				}
			}
		}
	}

	
}
