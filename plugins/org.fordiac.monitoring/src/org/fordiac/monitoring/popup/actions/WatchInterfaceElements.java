/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.monitoring.popup.actions;

import java.util.Iterator;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.fordiac.ide.application.editparts.FBEditPart;
import org.fordiac.ide.gef.editparts.InterfaceEditPart;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.monitoring.MonitoringManager;
import org.fordiac.monitoring.MonitoringManagerUtils;
import org.fordiac.monitoring.PortElement;


public class WatchInterfaceElements extends AbstractMonitoringAction {
	private StructuredSelection selection;
	

	public WatchInterfaceElements() {

	}

	@SuppressWarnings("rawtypes")
	@Override
	public void run(IAction action) {
		for (Iterator iter = (this.selection).iterator(); iter.hasNext();) {
			Object selectedObject = iter.next();

			if (selectedObject instanceof FBEditPart) {
				FBEditPart editPart = (FBEditPart) selectedObject;
				for (Iterator<InterfaceElementView> iterator = editPart.getCastedModel()
						.getInterfaceElements().iterator(); iterator.hasNext();) {
					InterfaceElementView element = (InterfaceElementView) iterator.next();
					InterfaceEditPart iEditPart = new org.fordiac.ide.application.editparts.InterfaceEditPart();
					iEditPart.setModel(element);
					PortElement port = MonitoringManagerUtils.createPortElement(iEditPart);
					if (port != null) {
						MonitoringManager.getInstance().addPort(port);
					}
				}
			}
		}
		
		refreshEditor();		
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		if (selection instanceof StructuredSelection) {
			this.selection = (StructuredSelection) selection;
			for (Iterator iter = (this.selection).iterator(); iter.hasNext();) {
				Object selectedObject = iter.next();

				if (selectedObject instanceof FBEditPart) {
					action.setEnabled(true);
				}
			}
		}
	}	
}
