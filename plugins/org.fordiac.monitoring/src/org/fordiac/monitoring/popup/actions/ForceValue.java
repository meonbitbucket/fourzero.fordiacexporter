/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.monitoring.popup.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IActionDelegate;
import org.fordiac.ide.gef.editparts.InterfaceEditPart;
import org.fordiac.model.gen.monitoring.MonitoringElement;
import org.fordiac.monitoring.MonitoringManager;
import org.fordiac.monitoring.MonitoringManagerUtils;
import org.fordiac.monitoring.PortElement;


public class ForceValue extends AbstractMonitoringAction {

	private StructuredSelection selection;

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		if ((selection).getFirstElement() instanceof InterfaceEditPart) {
			InterfaceEditPart editPart = (InterfaceEditPart) (selection)
					.getFirstElement();
//			String port = createPortString(editPart);
			PortElement port = MonitoringManagerUtils.createPortElement(editPart);

			if (!editPart.isEvent()) {
				MonitoringElement element = MonitoringManager.getInstance()
						.getMonitoringElement(port.getPortString());
				if (element != null) {

					InputDialog input = new InputDialog(Display.getDefault()
							.getActiveShell(), "Force Value", "Value",
							element.isForce() ? element.getForceValue() : "",
							null);
					int ret = input.open();
					if (ret == org.eclipse.jface.window.Window.OK) {
						MonitoringManager.getInstance().addPort(port); // first
																		// add
						// port to
						// the monitoring
						refreshEditor();
						
						if (element != null) {
							MonitoringManager.getInstance().forceValue(element,
									input.getValue());
						}
					}
				}
			}
		}
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		if (selection instanceof StructuredSelection) {
			this.selection = (StructuredSelection) selection;
			if (((StructuredSelection) selection).getFirstElement() instanceof InterfaceEditPart) {
				InterfaceEditPart editPart = (InterfaceEditPart) ((StructuredSelection) selection)
						.getFirstElement();
				PortElement port = MonitoringManagerUtils.createPortElement(editPart);
				action.setEnabled(port != null && editPart.isVariable());
				action.setChecked(port != null
						&& editPart.isVariable()
						&& MonitoringManager.getInstance()
						.getMonitoringElement(port.getPortString()) != null && MonitoringManager.getInstance()
								.getMonitoringElement(port.getPortString()).isForce());
			}
		}
	}
}
