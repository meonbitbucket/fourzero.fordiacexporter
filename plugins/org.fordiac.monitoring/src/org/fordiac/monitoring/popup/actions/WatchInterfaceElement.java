/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.monitoring.popup.actions;

import java.util.Iterator;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IActionDelegate;
import org.fordiac.monitoring.IMonitoringManager;
import org.fordiac.monitoring.MonitoringManagerUtils;
import org.fordiac.monitoring.PortElement;
import org.fordiac.ide.gef.editparts.InterfaceEditPart;


/**
 * The Class WatchInterfaceElement.
 */
public class WatchInterfaceElement extends AbstractMonitoringAction {

	/** The selection. */
	private StructuredSelection selection;


	/**
	 * Run.
	 * 
	 * @param action
	 *          the action
	 * 
	 * @see IActionDelegate#run(IAction)
	 */
	@SuppressWarnings("rawtypes")
	public void run(IAction action) {
		for (Iterator iterator = this.selection.iterator(); iterator.hasNext();) {
			Object obj = iterator.next();
			if (obj instanceof InterfaceEditPart) {
				InterfaceEditPart editPart = (InterfaceEditPart) obj;
				
				PortElement port = MonitoringManagerUtils.createPortElement(editPart);
				IMonitoringManager manager = MonitoringManagerUtils
						.getMonitoringManager(port.system);
				
				if (!manager.containsPort(port.getPortString())) {
					manager.addPort(port);
				}
			}
		}
		
		refreshEditor();
	}

	/**
	 * Selection changed.
	 * 
	 * @param action
	 *          the action
	 * @param selection
	 *          the selection
	 * 
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		boolean needToAdd = false;
		if (selection instanceof StructuredSelection) {
			this.selection = (StructuredSelection) selection;
			for (@SuppressWarnings("rawtypes")
			Iterator iterator = this.selection.iterator(); iterator.hasNext();) {
				Object obj = iterator.next();
				if (obj instanceof InterfaceEditPart) {
					InterfaceEditPart editPart = (InterfaceEditPart) obj;
					PortElement port = MonitoringManagerUtils.createPortElement(editPart);
					if(port != null){
						IMonitoringManager manager = MonitoringManagerUtils
								.getMonitoringManager(port.system);
						if (port.getPortString() != null
								&& !manager.containsPort(port.getPortString())) {
							needToAdd = true;
							break; // can return from loop because one is enough to enable the
							// action
						}
					}
				}
			}
		}
		action.setEnabled(needToAdd);
	}
}
