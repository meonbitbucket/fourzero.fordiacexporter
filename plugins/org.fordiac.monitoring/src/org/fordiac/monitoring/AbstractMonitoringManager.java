package org.fordiac.monitoring;

import java.util.ArrayList;
import java.util.HashSet;

import org.eclipse.emf.ecore.util.EContentAdapter;
import org.fordiac.model.gen.monitoring.Breakpoints;
import org.fordiac.model.gen.monitoring.MonitoringFactory;
import org.fordiac.model.gen.monitoring.Watches;

public abstract class AbstractMonitoringManager implements IMonitoringManager {

	protected final Breakpoints breakpoints = MonitoringFactory.eINSTANCE
			.createBreakpoints();
	protected final Watches watches = MonitoringFactory.eINSTANCE.createWatches();

	/** The elements to monitor. */
	protected final HashSet<String> elementsToMonitor = new HashSet<String>();
	/** The monitoring listeners. */
	protected final ArrayList<IMonitoringListener> monitoringListeners = new ArrayList<IMonitoringListener>();

	/**
	 * Register IMonitoringListener.
	 * 
	 * @param listener
	 *            the listener
	 */
	public void registerMonitoringListener(IMonitoringListener listener) {
		if (!monitoringListeners.contains(listener)) {
			monitoringListeners.add(listener);
		}
	}


	public void addBreakpointsAdapter(EContentAdapter adapter) {
		if (!breakpoints.eAdapters().contains(adapter)) {
			breakpoints.eAdapters().add(adapter);
		}
	}

	public void removeBreakpointsAdapter(EContentAdapter adapter) {
		breakpoints.eAdapters().remove(adapter);
	}

	ArrayList<IMonitoringListener> watchesAdapter = new ArrayList<IMonitoringListener>();
	
	public void addWatchesAdapter(IMonitoringListener adapter) {
		if (!watchesAdapter.contains(adapter)) {
			watchesAdapter.add(adapter);
		}
	}

	public void removeWatchesAdapter(IMonitoringListener adapter) {
		watchesAdapter.remove(adapter);
	}
	
	public void notifyWatchesAdapterPortAdded(String port) {
		for (IMonitoringListener adapter : watchesAdapter) {
			adapter.notifyAddPort(port);
		}
	}
	public void notifyWatchesAdapterPortRemoved(String port) {
		for (IMonitoringListener adapter : watchesAdapter) {
			adapter.notifyRemovePort(port);
		}
	}
	
	/**
	 * Gets the elements to monitor.
	 * 
	 * @return the elements to monitor
	 */
	public HashSet<String> getElementsToMonitor() {
		return elementsToMonitor;
	}
	
	
	public abstract void disableSystem(String system);
	
	public abstract void enableSystem(String system);
}
