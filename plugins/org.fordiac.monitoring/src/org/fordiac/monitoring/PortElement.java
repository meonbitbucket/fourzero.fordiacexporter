package org.fordiac.monitoring;

import java.util.ArrayList;

import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.Resource;

public class PortElement {

	public IInterfaceElement getInterfaceElement() {
		return interfaceElement;
	}

	public void setInterfaceElement(IInterfaceElement interfaceElement) {
		this.interfaceElement = interfaceElement;
	}

	public FB getFb() {
		return fb;
	}

	public void setFb(FB fb) {
		this.fb = fb;
	}

	// public String portString;
	public AutomationSystem system;
	private Device device;
	private Resource resource;
	private IInterfaceElement interfaceElement;
	private FB fb;
	private ArrayList<String> hierarchy = new ArrayList<String>();

	public String getPortString() {
		return system.getName() + "." + device.getName() + "."
				+ resource.getName() + "." + fb.getName() + "."
				+ interfaceElement.getName();
	}

	public AutomationSystem getSystem() {
		return system;
	}

	public void setSystem(AutomationSystem system) {
		this.system = system;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public ArrayList<String> getHierarchy() {
		return hierarchy;
	}

}