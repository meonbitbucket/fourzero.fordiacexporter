package org.fordiac.monitoring;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;

public class MonitoringManagerProvider implements IMonitoringManagerProvider {

	public MonitoringManagerProvider() {
	}

	@Override
	public IMonitoringManager getMonitoringManager() {
		return MonitoringManager.getInstance();
	}

	@Override
	public boolean projectContainsSupportedNature(IProject project) {
		if (project != null) {
			try {
				return project.hasNature("org.fordiac.systemmanagement.nature.IEC61499");
			} catch (CoreException e) {
				return false;
			}
		}
		return false;
	}

}
