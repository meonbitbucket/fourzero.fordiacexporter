/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.monitoring.views;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.dialogs.FilteredTree;
import org.eclipse.ui.dialogs.PatternFilter;
import org.eclipse.ui.part.ViewPart;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.model.gen.monitoring.MonitoringElement;
import org.fordiac.monitoring.Activator;
import org.fordiac.monitoring.IMonitoringListener;
import org.fordiac.monitoring.IMonitoringManagerProvider;
import org.fordiac.monitoring.provider.WatchesContentProvider;
import org.fordiac.monitoring.provider.WatchesLabelProvider;


public class WatchesView extends ViewPart {

	private FilteredTree filteredTree;

	
	IMonitoringListener listener = new IMonitoringListener() {
		
		@Override
		public void notifyTriggerEvent(String port) {			
		}
		
		@Override
		public void notifyRemovePort(String port) {
			filteredTree.getViewer().refresh();
		}
		
		@Override
		public void notifyEnableMonitoring(AutomationSystem system) {			
		}
		
		@Override
		public void notifyDisableMonitoring(AutomationSystem system) {			
		}
		
		@Override
		public void notifyAddPort(String port) {
			if(!filteredTree.isDisposed()){
				filteredTree.getViewer().refresh();
			}
		}
	};

	public WatchesView() {
	}

	@Override
	public void createPartControl(Composite parent) {
		Composite root = new Composite(parent, SWT.NONE);
		root.setLayout(new GridLayout());
		PatternFilter patternFilter = new PatternFilter();

		filteredTree = new FilteredTree(root, SWT.H_SCROLL | SWT.V_SCROLL,
				patternFilter, true);

		GridData treeGridData = new GridData();
		treeGridData.grabExcessHorizontalSpace = true;
		treeGridData.grabExcessVerticalSpace = true;
		treeGridData.horizontalAlignment = SWT.FILL;
		treeGridData.verticalAlignment = SWT.FILL;

		filteredTree.setLayoutData(treeGridData);

		TreeViewerColumn column1 = new TreeViewerColumn(filteredTree.getViewer(),
				SWT.None);
		column1.getColumn().setText("Watched Element");
		column1.getColumn().setWidth(340);
		TreeViewerColumn column2 = new TreeViewerColumn(filteredTree.getViewer(),
				SWT.None);
		column2.getColumn().setText("Value");
		column2.getColumn().setWidth(100);
		column2.setEditingSupport(new EditingSupport(column2.getViewer()) {

			@Override
			protected void setValue(Object element, Object value) {
				if (element instanceof MonitoringElement) {
					((MonitoringElement) element)
							.setBreakpointCondition(value.toString());
				}
			}

			@Override
			protected Object getValue(Object element) {
				if (element instanceof MonitoringElement) {
					return ((MonitoringElement) element).getCurrentValue();
				}
				return "";
			}

			@Override
			protected CellEditor getCellEditor(Object element) {
				return new TextCellEditor(filteredTree.getViewer().getTree());
			}

			@Override
			protected boolean canEdit(Object element) {
				return true;
			}
		});

		filteredTree.getViewer().getTree().setHeaderVisible(true);
		filteredTree.getViewer().getTree().setLinesVisible(true);

		filteredTree.getViewer().setContentProvider(new WatchesContentProvider());
		filteredTree.getViewer().setLabelProvider(new WatchesLabelProvider());
		filteredTree.getViewer().setInput(new Object());

		//MonitoringManager.getInstance().addWatchesAdapter(adapter);
		
		addWatchesAdapters();
	}

	private void addWatchesAdapters() {
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IConfigurationElement[] elems = registry.getConfigurationElementsFor(
				org.fordiac.monitoring.Activator.PLUGIN_ID, "monitoringmanager"); //$NON-NLS-1$
		for (int i = 0; i < elems.length; i++) {
			IConfigurationElement element = elems[i];
			try {
				Object object = element.createExecutableExtension("Interface"); //$NON-NLS-1$
				if (object instanceof IMonitoringManagerProvider) {
					IMonitoringManagerProvider monitoringManagerProvider = (IMonitoringManagerProvider) object;
					monitoringManagerProvider.getMonitoringManager().addWatchesAdapter(listener);
				}
			} catch (CoreException corex) {
				Activator.getDefault().logError("Error loading Monitoring Manager Provider!", corex);
			}
		}		
	}

	@Override
	public void setFocus() {

	}

}
