/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.monitoring;

import org.fordiac.ide.model.libraryElement.AutomationSystem;

public interface IMonitoringListener {

	public void notifyEnableMonitoring(AutomationSystem system);

	public void notifyDisableMonitoring(AutomationSystem system);

	public void notifyAddPort(String port);

	public void notifyTriggerEvent(String port);

	public void notifyRemovePort(String port);
}
