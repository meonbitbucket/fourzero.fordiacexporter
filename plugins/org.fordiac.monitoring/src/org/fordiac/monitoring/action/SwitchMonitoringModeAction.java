/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.monitoring.action;

import java.util.Iterator;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowPulldownDelegate2;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.fordiac.ide.deployment.ui.views.DownloadSelectionTreeView;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.monitoring.Activator;
import org.fordiac.monitoring.IMonitoringManager;
import org.fordiac.monitoring.MonitoringManagerUtils;
import org.fordiac.systemmanagement.SystemManager;
import org.fordiac.systemmanagement.ui.views.SystemTreeView;


public class SwitchMonitoringModeAction implements
		IWorkbenchWindowPulldownDelegate2 {

	boolean selectAll = true;

	@Override
	public Menu getMenu(Menu parent) {
		return null;
	}

	@Override
	public Menu getMenu(Control parent) {

		Menu menu = new Menu(parent);

		for (Iterator<AutomationSystem> iterator = SystemManager.getInstance().getSystems()
				.iterator(); iterator.hasNext();) {
			AutomationSystem system = (AutomationSystem) iterator.next();
			MenuItem item = new MenuItem(menu, SWT.CHECK);
			item.setText(system.getName());
			item.setData(system);
			final IMonitoringManager manager = MonitoringManagerUtils.getMonitoringManager(system);
			item.setSelection(manager.containsSystem(
					system.getName()));
			item.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {

					MenuItem menuItem = (MenuItem) e.getSource();
					AutomationSystem system = (AutomationSystem) menuItem.getData();

					if (menuItem.getSelection()) {
						manager.enableSystem(system.getName());

						IWorkbenchWindow window = PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow();
						try {
							window.getWorkbench().showPerspective(
									"org.fordiac.monitoring.MonitoringPerspective", window);
						} catch (WorkbenchException e1) {
							Activator.getDefault().logError(e1.getMessage(), e1);
						}

						// menuItem.setSelection(false);
					} else {
						manager.disableSystem(system.getName());
						// menuItem.setSelection(true);
					}
					refreshSystemTree();
				}

			});
		}

		return menu;
	}

	

	
	
	
	
	@Override
	public void dispose() {

	}

	@Override
	public void init(IWorkbenchWindow window) {
	}

	@Override
	public void run(IAction action) {
		if (selectAll) {
			for (Iterator<AutomationSystem> iterator = SystemManager.getInstance().getSystems()
					.iterator(); iterator.hasNext();) {
				AutomationSystem system = iterator.next();
				MonitoringManagerUtils.getMonitoringManager(system).enableSystem(system.getName());
			}
			selectAll = false;
		} else {
			for (Iterator<AutomationSystem> iterator = SystemManager.getInstance().getSystems()
					.iterator(); iterator.hasNext();) {
				AutomationSystem system = iterator.next();
				MonitoringManagerUtils.getMonitoringManager(system).disableSystem(system.getName());
			}
			selectAll = true;
		}
		refreshSystemTree();
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
	}
	
	//TODO duplicated code from MontorSystemHandler
	private void refreshSystemTree() {
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		IViewPart view = page.findView("org.fordiac.systemmanagement.ui.views.SystemTreeView");
		
		if ((null != view) && (view instanceof SystemTreeView)){
			SystemTreeView treeView = (SystemTreeView)view;
			treeView.getViewer().refresh();
		}
		
		view = page.findView("org.fordiac.ide.deployment.ui.views.DownloadSelectionTreeView");
		
		if ((null != view) && (view instanceof DownloadSelectionTreeView)){
			DownloadSelectionTreeView treeView = (DownloadSelectionTreeView)view;
			treeView.getViewer().refresh();
		}
	}

}
