package org.fordiac.monitoring;

import org.eclipse.core.resources.IProject;

public interface IMonitoringManagerProvider {
	
	public IMonitoringManager getMonitoringManager();
	
	public boolean projectContainsSupportedNature(IProject project);

}
