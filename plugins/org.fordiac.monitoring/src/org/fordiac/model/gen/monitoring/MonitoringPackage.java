/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.model.gen.monitoring;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.fordiac.ide.model.ui.UiPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.fordiac.model.gen.monitoring.MonitoringFactory
 * @model kind="package"
 * @generated
 */
public interface MonitoringPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "monitoring";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "org.fordiac";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "monitoring";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MonitoringPackage eINSTANCE = org.fordiac.model.gen.monitoring.impl.MonitoringPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.fordiac.model.gen.monitoring.impl.MonitoringElementImpl <em>Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.model.gen.monitoring.impl.MonitoringElementImpl
	 * @see org.fordiac.model.gen.monitoring.impl.MonitoringPackageImpl#getMonitoringElement()
	 * @generated
	 */
	int MONITORING_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_ELEMENT__POSITION = UiPackage.VIEW__POSITION;

	/**
	 * The feature id for the '<em><b>Background Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_ELEMENT__BACKGROUND_COLOR = UiPackage.VIEW__BACKGROUND_COLOR;

	/**
	 * The feature id for the '<em><b>Monitoring Element</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_ELEMENT__MONITORING_ELEMENT = UiPackage.VIEW_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Interface Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_ELEMENT__INTERFACE_ELEMENT = UiPackage.VIEW_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Fb</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_ELEMENT__FB = UiPackage.VIEW_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Force</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_ELEMENT__FORCE = UiPackage.VIEW_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Force Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_ELEMENT__FORCE_VALUE = UiPackage.VIEW_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Offline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_ELEMENT__OFFLINE = UiPackage.VIEW_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Breakpoint</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_ELEMENT__BREAKPOINT = UiPackage.VIEW_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Breakpoint Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_ELEMENT__BREAKPOINT_ACTIVE = UiPackage.VIEW_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Breakpoint Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_ELEMENT__BREAKPOINT_CONDITION = UiPackage.VIEW_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Current Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_ELEMENT__CURRENT_VALUE = UiPackage.VIEW_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Sec</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_ELEMENT__SEC = UiPackage.VIEW_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Usec</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_ELEMENT__USEC = UiPackage.VIEW_FEATURE_COUNT + 11;

	/**
	 * The number of structural features of the '<em>Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_ELEMENT_FEATURE_COUNT = UiPackage.VIEW_FEATURE_COUNT + 12;


	/**
	 * The meta object id for the '{@link org.fordiac.model.gen.monitoring.impl.BreakpointsImpl <em>Breakpoints</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.model.gen.monitoring.impl.BreakpointsImpl
	 * @see org.fordiac.model.gen.monitoring.impl.MonitoringPackageImpl#getBreakpoints()
	 * @generated
	 */
	int BREAKPOINTS = 1;

	/**
	 * The feature id for the '<em><b>Breakpoints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAKPOINTS__BREAKPOINTS = 0;

	/**
	 * The number of structural features of the '<em>Breakpoints</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAKPOINTS_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.fordiac.model.gen.monitoring.impl.WatchesImpl <em>Watches</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.model.gen.monitoring.impl.WatchesImpl
	 * @see org.fordiac.model.gen.monitoring.impl.MonitoringPackageImpl#getWatches()
	 * @generated
	 */
	int WATCHES = 2;

	/**
	 * The feature id for the '<em><b>Watches</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WATCHES__WATCHES = 0;

	/**
	 * The number of structural features of the '<em>Watches</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WATCHES_FEATURE_COUNT = 1;


	/**
	 * The meta object id for the '{@link org.fordiac.ide.gef.IEditPartCreator <em>IEdit Part Creator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.gef.IEditPartCreator
	 * @see org.fordiac.model.gen.monitoring.impl.MonitoringPackageImpl#getIEditPartCreator()
	 * @generated
	 */
	int IEDIT_PART_CREATOR = 3;

	/**
	 * The number of structural features of the '<em>IEdit Part Creator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEDIT_PART_CREATOR_FEATURE_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link org.fordiac.model.gen.monitoring.MonitoringElement <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element</em>'.
	 * @see org.fordiac.model.gen.monitoring.MonitoringElement
	 * @generated
	 */
	EClass getMonitoringElement();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.model.gen.monitoring.MonitoringElement#getMonitoringElement <em>Monitoring Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Monitoring Element</em>'.
	 * @see org.fordiac.model.gen.monitoring.MonitoringElement#getMonitoringElement()
	 * @see #getMonitoringElement()
	 * @generated
	 */
	EAttribute getMonitoringElement_MonitoringElement();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.model.gen.monitoring.MonitoringElement#getInterfaceElement <em>Interface Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Interface Element</em>'.
	 * @see org.fordiac.model.gen.monitoring.MonitoringElement#getInterfaceElement()
	 * @see #getMonitoringElement()
	 * @generated
	 */
	EReference getMonitoringElement_InterfaceElement();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.model.gen.monitoring.MonitoringElement#getFb <em>Fb</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Fb</em>'.
	 * @see org.fordiac.model.gen.monitoring.MonitoringElement#getFb()
	 * @see #getMonitoringElement()
	 * @generated
	 */
	EReference getMonitoringElement_Fb();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.model.gen.monitoring.MonitoringElement#isForce <em>Force</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Force</em>'.
	 * @see org.fordiac.model.gen.monitoring.MonitoringElement#isForce()
	 * @see #getMonitoringElement()
	 * @generated
	 */
	EAttribute getMonitoringElement_Force();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.model.gen.monitoring.MonitoringElement#getForceValue <em>Force Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Force Value</em>'.
	 * @see org.fordiac.model.gen.monitoring.MonitoringElement#getForceValue()
	 * @see #getMonitoringElement()
	 * @generated
	 */
	EAttribute getMonitoringElement_ForceValue();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.model.gen.monitoring.MonitoringElement#isOffline <em>Offline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Offline</em>'.
	 * @see org.fordiac.model.gen.monitoring.MonitoringElement#isOffline()
	 * @see #getMonitoringElement()
	 * @generated
	 */
	EAttribute getMonitoringElement_Offline();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.model.gen.monitoring.MonitoringElement#isBreakpoint <em>Breakpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Breakpoint</em>'.
	 * @see org.fordiac.model.gen.monitoring.MonitoringElement#isBreakpoint()
	 * @see #getMonitoringElement()
	 * @generated
	 */
	EAttribute getMonitoringElement_Breakpoint();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.model.gen.monitoring.MonitoringElement#isBreakpointActive <em>Breakpoint Active</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Breakpoint Active</em>'.
	 * @see org.fordiac.model.gen.monitoring.MonitoringElement#isBreakpointActive()
	 * @see #getMonitoringElement()
	 * @generated
	 */
	EAttribute getMonitoringElement_BreakpointActive();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.model.gen.monitoring.MonitoringElement#getBreakpointCondition <em>Breakpoint Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Breakpoint Condition</em>'.
	 * @see org.fordiac.model.gen.monitoring.MonitoringElement#getBreakpointCondition()
	 * @see #getMonitoringElement()
	 * @generated
	 */
	EAttribute getMonitoringElement_BreakpointCondition();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.model.gen.monitoring.MonitoringElement#getCurrentValue <em>Current Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Current Value</em>'.
	 * @see org.fordiac.model.gen.monitoring.MonitoringElement#getCurrentValue()
	 * @see #getMonitoringElement()
	 * @generated
	 */
	EAttribute getMonitoringElement_CurrentValue();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.model.gen.monitoring.MonitoringElement#getSec <em>Sec</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sec</em>'.
	 * @see org.fordiac.model.gen.monitoring.MonitoringElement#getSec()
	 * @see #getMonitoringElement()
	 * @generated
	 */
	EAttribute getMonitoringElement_Sec();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.model.gen.monitoring.MonitoringElement#getUsec <em>Usec</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Usec</em>'.
	 * @see org.fordiac.model.gen.monitoring.MonitoringElement#getUsec()
	 * @see #getMonitoringElement()
	 * @generated
	 */
	EAttribute getMonitoringElement_Usec();

	/**
	 * Returns the meta object for class '{@link org.fordiac.model.gen.monitoring.Breakpoints <em>Breakpoints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Breakpoints</em>'.
	 * @see org.fordiac.model.gen.monitoring.Breakpoints
	 * @generated
	 */
	EClass getBreakpoints();

	/**
	 * Returns the meta object for the containment reference list '{@link org.fordiac.model.gen.monitoring.Breakpoints#getBreakpoints <em>Breakpoints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Breakpoints</em>'.
	 * @see org.fordiac.model.gen.monitoring.Breakpoints#getBreakpoints()
	 * @see #getBreakpoints()
	 * @generated
	 */
	EReference getBreakpoints_Breakpoints();

	/**
	 * Returns the meta object for class '{@link org.fordiac.model.gen.monitoring.Watches <em>Watches</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Watches</em>'.
	 * @see org.fordiac.model.gen.monitoring.Watches
	 * @generated
	 */
	EClass getWatches();

	/**
	 * Returns the meta object for the containment reference list '{@link org.fordiac.model.gen.monitoring.Watches#getWatches <em>Watches</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Watches</em>'.
	 * @see org.fordiac.model.gen.monitoring.Watches#getWatches()
	 * @see #getWatches()
	 * @generated
	 */
	EReference getWatches_Watches();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.gef.IEditPartCreator <em>IEdit Part Creator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IEdit Part Creator</em>'.
	 * @see org.fordiac.ide.gef.IEditPartCreator
	 * @model instanceClass="org.fordiac.ide.gef.IEditPartCreator"
	 * @generated
	 */
	EClass getIEditPartCreator();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MonitoringFactory getMonitoringFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.fordiac.model.gen.monitoring.impl.MonitoringElementImpl <em>Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.model.gen.monitoring.impl.MonitoringElementImpl
		 * @see org.fordiac.model.gen.monitoring.impl.MonitoringPackageImpl#getMonitoringElement()
		 * @generated
		 */
		EClass MONITORING_ELEMENT = eINSTANCE.getMonitoringElement();

		/**
		 * The meta object literal for the '<em><b>Monitoring Element</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_ELEMENT__MONITORING_ELEMENT = eINSTANCE.getMonitoringElement_MonitoringElement();

		/**
		 * The meta object literal for the '<em><b>Interface Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MONITORING_ELEMENT__INTERFACE_ELEMENT = eINSTANCE.getMonitoringElement_InterfaceElement();

		/**
		 * The meta object literal for the '<em><b>Fb</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MONITORING_ELEMENT__FB = eINSTANCE.getMonitoringElement_Fb();

		/**
		 * The meta object literal for the '<em><b>Force</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_ELEMENT__FORCE = eINSTANCE.getMonitoringElement_Force();

		/**
		 * The meta object literal for the '<em><b>Force Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_ELEMENT__FORCE_VALUE = eINSTANCE.getMonitoringElement_ForceValue();

		/**
		 * The meta object literal for the '<em><b>Offline</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_ELEMENT__OFFLINE = eINSTANCE.getMonitoringElement_Offline();

		/**
		 * The meta object literal for the '<em><b>Breakpoint</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_ELEMENT__BREAKPOINT = eINSTANCE.getMonitoringElement_Breakpoint();

		/**
		 * The meta object literal for the '<em><b>Breakpoint Active</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_ELEMENT__BREAKPOINT_ACTIVE = eINSTANCE.getMonitoringElement_BreakpointActive();

		/**
		 * The meta object literal for the '<em><b>Breakpoint Condition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_ELEMENT__BREAKPOINT_CONDITION = eINSTANCE.getMonitoringElement_BreakpointCondition();

		/**
		 * The meta object literal for the '<em><b>Current Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_ELEMENT__CURRENT_VALUE = eINSTANCE.getMonitoringElement_CurrentValue();

		/**
		 * The meta object literal for the '<em><b>Sec</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_ELEMENT__SEC = eINSTANCE.getMonitoringElement_Sec();

		/**
		 * The meta object literal for the '<em><b>Usec</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_ELEMENT__USEC = eINSTANCE.getMonitoringElement_Usec();

		/**
		 * The meta object literal for the '{@link org.fordiac.model.gen.monitoring.impl.BreakpointsImpl <em>Breakpoints</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.model.gen.monitoring.impl.BreakpointsImpl
		 * @see org.fordiac.model.gen.monitoring.impl.MonitoringPackageImpl#getBreakpoints()
		 * @generated
		 */
		EClass BREAKPOINTS = eINSTANCE.getBreakpoints();

		/**
		 * The meta object literal for the '<em><b>Breakpoints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BREAKPOINTS__BREAKPOINTS = eINSTANCE.getBreakpoints_Breakpoints();

		/**
		 * The meta object literal for the '{@link org.fordiac.model.gen.monitoring.impl.WatchesImpl <em>Watches</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.model.gen.monitoring.impl.WatchesImpl
		 * @see org.fordiac.model.gen.monitoring.impl.MonitoringPackageImpl#getWatches()
		 * @generated
		 */
		EClass WATCHES = eINSTANCE.getWatches();

		/**
		 * The meta object literal for the '<em><b>Watches</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WATCHES__WATCHES = eINSTANCE.getWatches_Watches();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.gef.IEditPartCreator <em>IEdit Part Creator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.gef.IEditPartCreator
		 * @see org.fordiac.model.gen.monitoring.impl.MonitoringPackageImpl#getIEditPartCreator()
		 * @generated
		 */
		EClass IEDIT_PART_CREATOR = eINSTANCE.getIEditPartCreator();

	}

} //MonitoringPackage
