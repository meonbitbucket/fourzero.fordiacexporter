/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.model.gen.monitoring;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Watches</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.model.gen.monitoring.Watches#getWatches <em>Watches</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.model.gen.monitoring.MonitoringPackage#getWatches()
 * @model
 * @generated
 */
public interface Watches extends EObject {
	/**
	 * Returns the value of the '<em><b>Watches</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.model.gen.monitoring.MonitoringElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Watches</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Watches</em>' containment reference list.
	 * @see org.fordiac.model.gen.monitoring.MonitoringPackage#getWatches_Watches()
	 * @model containment="true"
	 * @generated
	 */
	EList<MonitoringElement> getWatches();

} // Watches
