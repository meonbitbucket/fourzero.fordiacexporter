/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.network;

import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.gef.dnd.TemplateTransferDropTargetListener;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.gef.ui.actions.AlignmentAction;
import org.eclipse.gef.ui.parts.ScrollingGraphicalViewer;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.util.TransferDropTargetListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.actions.ActionFactory;
import org.fordiac.gmfextensions.AnimatedZoomScalableFreeformRootEditPart;
import org.fordiac.ide.application.actions.CopyEditPartsAction;
import org.fordiac.ide.application.actions.UpdateFBTypeAction;
import org.fordiac.ide.application.utilities.ApplicationEditorTemplateTransferDropTargetListener;
import org.fordiac.ide.fbt.typeeditor.FBTypeEditDomain;
import org.fordiac.ide.fbt.typeeditor.contentprovider.InterfaceContextMenuProvider;
import org.fordiac.ide.fbt.typeeditor.editors.IFBTEditorPart;
import org.fordiac.ide.fbt.typeeditor.network.actions.CFBPasteEditPartsAction;
import org.fordiac.ide.fbt.typeeditor.network.editparts.CompositeNetworkEditPartFactory;
import org.fordiac.ide.fbt.typemanagement.FBTypeEditorInput;
import org.fordiac.ide.gef.DiagramEditor;
import org.fordiac.ide.model.Palette.Palette;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.util.imageprovider.ImageProvider;

/**
 * The Class ECCEditor.
 */
public class CompositeNetworkEditor extends DiagramEditor implements
		IFBTEditorPart {

	/** The adapter. */
	EContentAdapter adapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {
			super.notifyChanged(notification);
			// only refresh propertypage (page) if the event is not an
			// REMOVING_ADAPTER event - otherwise, the remove adapter in the
			// dispose method (called when closing the editor) will fail
			if (notification.getEventType() != Notification.REMOVING_ADAPTER
					&& (((notification.getNewValue() == null) && (notification
							.getNewValue() != notification.getOldValue())) || ((notification
							.getNewValue() != null) && !(notification
							.getNewValue().equals(notification.getOldValue()))))) {
				super.notifyChanged(notification);
			}

		}

	};

	/** The fb type. */
	protected CompositeFBType fbType;
	
	private CFBUIFBNetworkManager uiFBNetworkManager;
	
	
	
	@Override
	public Diagram getDiagramModel() {
		return uiFBNetworkManager.getUiFBNetwork();		
	}

	/**
	 * Instantiates a new Composite network editor.
	 */
	public CompositeNetworkEditor() {
	}

	protected void initializeGraphicalViewer() {
		// super.initializeGraphicalViewer();
		GraphicalViewer viewer = getGraphicalViewer();
		if (fbType.getFBNetwork() != null) {

			viewer.setContents(getDiagramModel());

			// listen for dropped parts
			TransferDropTargetListener listener = createTransferDropTargetListener();
			if (listener != null) {
				viewer.addDropTargetListener(createTransferDropTargetListener());
			}
			// enable drag from palette
			getGraphicalViewer()
					.addDropTargetListener(
							(TransferDropTargetListener) new TemplateTransferDropTargetListener(
									getGraphicalViewer()));
			// // adds a new mapping of this editor and the model to a central
			// place
		}
	}

	protected CompositeNetworkEditPartFactory getEditPartFactory() {
		return new CompositeNetworkEditPartFactory(fbType, getZoomManger() );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.ui.parts.GraphicalEditor#selectionChanged(org.eclipse
	 * .ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	@Override
	public void selectionChanged(final IWorkbenchPart part,
			final ISelection selection) {
		super.selectionChanged(part, selection);
		updateActions(getSelectionActions());
	}

	/** The palette root. */
	protected PaletteRoot paletteRoot;

	protected Palette palette;


	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.
	 * IProgressMonitor)
	 */
	@Override
	public void doSave(final IProgressMonitor monitor) {
		//currently nothing needs to be done here
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#dispose()
	 */
	@Override
	public void dispose() {
		if (fbType != null) {
			Diagram diagram = getDiagramModel();
			
			if (diagram != null) {
				diagram.eAdapters().remove(adapter);
			}
			if(null != uiFBNetworkManager){
				uiFBNetworkManager.dispose();
			}
		}
		super.dispose();
	}

	public void createPartControl(final Composite parent) {
		SashForm s = new SashForm(parent, SWT.VERTICAL | SWT.SMOOTH);
		Composite graphicaEditor = new Composite(s, SWT.NONE);
		graphicaEditor.setLayout(new FillLayout());
		super.createPartControl(graphicaEditor);
		getSite().setSelectionProvider(getGraphicalViewer());
	}
	
	@Override
	protected void configureGraphicalViewer() {
		super.configureGraphicalViewer();
		final CopyEditPartsAction copy = (CopyEditPartsAction) getActionRegistry()
				.getAction(ActionFactory.COPY.getId());

		getGraphicalViewer().addSelectionChangedListener(copy);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void createActions() {
		ActionRegistry registry = getActionRegistry();
		IAction action;

		action = new CopyEditPartsAction(this);
		registry.registerAction(action);		
		getEditorSite().getActionBars().setGlobalActionHandler(
				ActionFactory.COPY.getId(), action);
		
		action = new UpdateFBTypeAction((IWorkbenchPart) this);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());

		action = new CFBPasteEditPartsAction(this);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());
		getEditorSite().getActionBars().setGlobalActionHandler(
				ActionFactory.PASTE.getId(), action);

		action = new AlignmentAction((IWorkbenchPart) this,
				PositionConstants.LEFT);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());

		action = new AlignmentAction((IWorkbenchPart) this,
				PositionConstants.RIGHT);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());

		action = new AlignmentAction((IWorkbenchPart) this,
				PositionConstants.TOP);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());

		action = new AlignmentAction((IWorkbenchPart) this,
				PositionConstants.BOTTOM);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());

		action = new AlignmentAction((IWorkbenchPart) this,
				PositionConstants.CENTER);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());

		action = new AlignmentAction((IWorkbenchPart) this,
				PositionConstants.MIDDLE);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());
		
		InterfaceContextMenuProvider.createInterfaceEditingActions(this, registry, fbType);
		
		super.createActions();
	}
	
	@Override
	public boolean outlineSelectionChanged(Object selectedElement) {

		EditPart editPart = getEditPartForSelection(selectedElement);
		if (null != editPart) {
			getGraphicalViewer().select(editPart);
			return true;
		}
		if (selectedElement instanceof FBNetwork) {
			return true;
		}

		return false;
	}

	EditPart getEditPartForSelection(Object selectedElement) {
		@SuppressWarnings("rawtypes")
		Map map = getGraphicalViewer().getEditPartRegistry();

		for (Object key : map.keySet()) {
			if (key instanceof FBView) {
				if (((FBView) key).getFb() == selectedElement) {
					selectedElement = key;
					break;
				}
			}
			if (key instanceof ConnectionView) {
				if (((ConnectionView) key).getConnectionElement() == selectedElement) {
					selectedElement = key;
					break;
				}
			}
		}

		Object obj = getGraphicalViewer().getEditPartRegistry().get(
				selectedElement);
		if (obj instanceof EditPart) {
			return (EditPart) obj;
		}
		return null;
	}

	@Override
	protected ContextMenuProvider getContextMenuProvider(
			final ScrollingGraphicalViewer viewer,
			final AnimatedZoomScalableFreeformRootEditPart root) {
		ContextMenuProvider cmp = new CFBNetworkcontextMenuProvider(this,
				getActionRegistry(), root.getZoomManager(), palette);
		return cmp;
	}

	@Override
	protected TransferDropTargetListener createTransferDropTargetListener() {
		return new ApplicationEditorTemplateTransferDropTargetListener(
				getGraphicalViewer(),  palette.getAutomationSystem());
	}

	@Override
	public AutomationSystem getSystem() {
		return null;
	}

	@Override
	public String getFileName() {
		return null;
		
	}

	@Override
	protected void setModel(IEditorInput input) {
		if (input instanceof FBTypeEditorInput) {
			FBTypeEditorInput untypedInput = (FBTypeEditorInput) input;
			if (untypedInput.getContent() instanceof CompositeFBType) {
				fbType = (CompositeFBType) untypedInput.getContent();
				setDiagramModelManger(createDiagramModelManger());				
				configurePalette(untypedInput);
			}
		}

		setPartName(Messages.CompositeNetworkEditor_LABEL_CompositeEditor);
		setTitleImage(ImageProvider
				.getImage(Messages.CompositeNetworkEditor_ICON_CompositeNetworkEditor));

		setEditDomain(new FBTypeEditDomain(this, commandStack));
	}

	protected CFBUIFBNetworkManager createDiagramModelManger() {
		return new CFBUIFBNetworkManager(fbType);
	}
	
	protected void setDiagramModelManger(CFBUIFBNetworkManager model){
		uiFBNetworkManager = model;
		uiFBNetworkManager.setCommandStack(commandStack);
		
		Diagram diagram = getDiagramModel();
		if (diagram != null) {
			diagram.eAdapters().add(adapter);
		}
	}

	protected void configurePalette(FBTypeEditorInput fbTypeEditorInput) {
		Palette fbPallette = fbTypeEditorInput.getPaletteEntry().getGroup().getPallete();
		if (null != fbPallette) {
			palette = (Palette) fbPallette;
		}
	}
	
	protected CommandStack commandStack;
	
	@Override
	public void setCommonCommandStack(CommandStack commandStack) {
		this.commandStack = commandStack;
	}
}
