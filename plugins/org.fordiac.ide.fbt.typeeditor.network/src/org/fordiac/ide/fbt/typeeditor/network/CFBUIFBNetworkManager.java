/*******************************************************************************
 * Copyright (c) 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.network;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.commands.CompoundCommand;
import org.fordiac.ide.application.commands.UpdateFBTypeCommand;
import org.fordiac.ide.application.utilities.ApplicationUIFBNetworkManager;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.AdapterFB;
import org.fordiac.ide.model.libraryElement.AdapterType;
import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.ui.CompositeInternalInterfaceElementView;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.ide.model.ui.View;
import org.fordiac.ide.util.commands.DeleteConnectionCommand;

public class CFBUIFBNetworkManager extends ApplicationUIFBNetworkManager {
	

	private CompositeFBType fbType;
	
	EContentAdapter adapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {
			Object feature = notification.getFeature();

			if ((LibraryElementPackage.eINSTANCE.getSubAppInterfaceList_EventInputs().equals(feature)) || 
					(LibraryElementPackage.eINSTANCE.getSubAppInterfaceList_EventOutputs().equals(feature)) ||
					(LibraryElementPackage.eINSTANCE.getSubAppInterfaceList_InputVars().equals(feature)) ||
					(LibraryElementPackage.eINSTANCE.getSubAppInterfaceList_OutputVars().equals(feature))) {
				handleInterfaceElementChange(notification);
			}else if ((LibraryElementPackage.eINSTANCE.getVarDeclaration_Type().equals(feature)) && 
					  (notification.getNotifier() instanceof AdapterDeclaration)) {
				handleInterfaceAdapterChange(notification);
			}
			//TODO consider handling add and remove of adapters also here
			super.notifyChanged(notification);
		}

		private void handleInterfaceElementChange(Notification notification) {
			Object feature = notification.getFeature();
			
			switch (notification.getEventType()) {
			case Notification.ADD: {
				IInterfaceElement element = (IInterfaceElement) notification.getNewValue();		
				View newElement = createInternalInterfaceEvent(element, getUiFBNetwork().getFbNetwork());
				CompositeInternalInterfaceElementView refElement = getRefElement(feature, notification.getPosition(), element.isIsInput());
				if(null != refElement){
					int index = getChildren().indexOf(refElement);
					getChildren().add(index, newElement);
				}else{
					addChild(newElement);
				}
			}
				break;
			
			case Notification.REMOVE: {
				IInterfaceElement element = (IInterfaceElement) notification.getOldValue();
				CompoundCommand delteConnections = new CompoundCommand();
				
				CompositeInternalInterfaceElementView compView = getCompInternalInterfaceElementView(element);
				if(null != compView){
					for (ConnectionView connectionView : compView.getInConnections()) {
						delteConnections.add(new DeleteConnectionCommand(connectionView));
					}
					for (ConnectionView connectionView : compView.getOutConnections()) {
						delteConnections.add(new DeleteConnectionCommand(connectionView));
					}
					//TODO consider to not delete connections here
					getCommandStack().execute(delteConnections);
					removeChild(compView);
				}
			}
				break;
				
			case Notification.MOVE: {
				IInterfaceElement element = (IInterfaceElement) notification.getNewValue();
				CompositeInternalInterfaceElementView compView = getCompInternalInterfaceElementView(element);
				if(null != compView){
					CompositeInternalInterfaceElementView refElement = getRefElement(feature, notification.getPosition(), element.isIsInput());
					if(null != refElement){
						getChildren().move(getChildren().indexOf(refElement), compView);
					}						
				}
			}
				break;
			default:
				break;
			}
		}
		
		private void handleInterfaceAdapterChange(Notification notification) {
			switch (notification.getEventType()) {
			case Notification.SET: {
					final AdapterDeclaration adapter = (AdapterDeclaration)notification.getNotifier();
					FBView view = getFBViewNamed(adapter.getName());
					
					final AdapterType type = (AdapterType)adapter.getType(); 
					
					if(null != view){
						UpdateFBTypeCommand cmd = new UpdateFBTypeCommand(view){
							@Override
							protected FB createCopiedFBEntry(FB srcFB) {
								AdapterFB aFB = LibraryElementFactory.eINSTANCE.createAdapterFB();
								aFB.setPlug(adapter.isIsInput());
								aFB.setPaletteEntry(type.getPaletteEntry());
								return aFB;
							}
						};						
						cmd.execute();
					}					
				}
				break;
			default:
				break;
			}
		}

		private CompositeInternalInterfaceElementView getRefElement(Object feature, int position, boolean input) {
			Class<? extends IInterfaceElement> clazz = null;
			
			if((LibraryElementPackage.eINSTANCE.getSubAppInterfaceList_EventInputs().equals(feature)) || 
					(LibraryElementPackage.eINSTANCE.getSubAppInterfaceList_EventOutputs().equals(feature))){
				clazz = Event.class;
			}
			if((LibraryElementPackage.eINSTANCE.getSubAppInterfaceList_InputVars().equals(feature)) || 
					(LibraryElementPackage.eINSTANCE.getSubAppInterfaceList_OutputVars().equals(feature))){
				clazz = VarDeclaration.class;
			}
			
			int i = 0;
			for (View view : getChildren()) {
				if (view instanceof CompositeInternalInterfaceElementView) {
					CompositeInternalInterfaceElementView compView = (CompositeInternalInterfaceElementView) view;
					
					if ((clazz.isAssignableFrom(compView.getIInterfaceElement().getClass())) && 
							(compView.getIInterfaceElement().isIsInput() == input))   {
						if(i == position){
							return compView;
						}
						i++;
					}
				}
			}			
			
			return null;
		}
	};

	public CFBUIFBNetworkManager(CompositeFBType fbType) {
		setCompositeType(fbType);
	}
	
	@Override
	protected Diagram createDiagram() {
		return UiFactory.eINSTANCE.createUIFBNetwork();
	}
	
	protected CFBUIFBNetworkManager(){
		
	}
	
	protected void setCompositeType(CompositeFBType fbType){
		this.fbType = fbType;
		addInternalInterfaceElements(fbType);				
		configureFromFBNetwork(fbType.getFBNetwork());
		configureAdapters();
		
		fbType.getInterfaceList().eAdapters().add(adapter);
	}

	protected CompositeInternalInterfaceElementView getCompInternalInterfaceElementView(
			IInterfaceElement element) {
		for (View view : getChildren()) {
			if (view instanceof CompositeInternalInterfaceElementView) {
				CompositeInternalInterfaceElementView compView = (CompositeInternalInterfaceElementView) view;
				if (compView.getIInterfaceElement().equals(element)) {
					return compView;
				}
			}
		}
		return null;
	}

	@Override
	public void dispose(){
		fbType.getInterfaceList().eAdapters().remove(adapter);
		
		super.dispose();
	}
	
	private void addInternalInterfaceElements(CompositeFBType fbType) {
		addInternalInterfaceEvents(fbType.getInterfaceList().getEventInputs(), fbType.getFBNetwork());
		addInternalInterfaceEvents(fbType.getInterfaceList().getEventOutputs(), fbType.getFBNetwork());
		
		addInternalInterfaceData(fbType.getInterfaceList().getInputVars(), fbType.getFBNetwork());
		addInternalInterfaceData(fbType.getInterfaceList().getOutputVars(), fbType.getFBNetwork());
		
	}

	private void addInternalInterfaceEvents(EList<Event> eventOutputs,
			FBNetwork fbNetwork) {
		for (Event event : eventOutputs) {
			addChild(createInternalInterfaceEvent(event, fbNetwork));
		}		
	}

	private void addInternalInterfaceData(EList<VarDeclaration> inputVars,
			FBNetwork fbNetwork) {
		for (VarDeclaration data : inputVars) {
			addChild(createInternalInterfaceEvent(data, fbNetwork));
		}
	}

	private View createInternalInterfaceEvent(IInterfaceElement element, FBNetwork fbNetwork) {
		CompositeInternalInterfaceElementView view = UiFactory.eINSTANCE
				.createCompositeInternalInterfaceElementView();
		view.setIInterfaceElement(element);
		view.setFbNetwork(fbNetwork);
		return view;
	}

	@Override
	protected InterfaceElementView getInterfaceElement(
			IInterfaceElement interfaceElement) {
		if(interfaceElement.eContainer().eContainer() instanceof CompositeFBType){
			for (View view : getChildren()) {
				if((view instanceof CompositeInternalInterfaceElementView) && 
						((CompositeInternalInterfaceElementView)view).getIInterfaceElement().equals(interfaceElement)){
					return (InterfaceElementView) view; 
				}
			}			
			return null;
		}
		return super.getInterfaceElement(interfaceElement);
	}
	
	
	private void configureAdapters() {
		for (View view : getChildren()) {
			if((view instanceof FBView) && (((FBView)view).getFb() instanceof AdapterFB)){
				setAdapterFBView((FBView)view);	
			}
		}		
	}

	private void setAdapterFBView(FBView view) {
		AdapterDeclaration adapter = fbType.getInterfaceList().getAdapter(view.getFb().getName());
		if(null != adapter){
			adapter.setAdapterBlock(view);
		}
	}

	

}
