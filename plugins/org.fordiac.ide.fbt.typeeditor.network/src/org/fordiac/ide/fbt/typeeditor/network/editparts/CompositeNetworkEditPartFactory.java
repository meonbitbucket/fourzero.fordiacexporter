/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.network.editparts;

import java.text.MessageFormat;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.editparts.ZoomManager;
import org.fordiac.ide.application.editparts.ConnectionEditPart;
import org.fordiac.ide.application.editparts.FBEditPart;
import org.fordiac.ide.application.editparts.InterfaceEditPart;
import org.fordiac.ide.fbt.typeeditor.network.Activator;
import org.fordiac.ide.fbt.typeeditor.network.Messages;
import org.fordiac.ide.gef.editparts.ValueEditPart;
import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.libraryElement.Value;
import org.fordiac.ide.model.ui.CompositeInternalInterfaceElementView;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.UIFBNetwork;

/**
 * A factory for creating ECCEditPart objects.
 */
public class CompositeNetworkEditPartFactory implements EditPartFactory {

	protected CompositeFBType compositeType;
	protected ZoomManager zoomManager;
	
	public CompositeNetworkEditPartFactory(CompositeFBType compositeType, ZoomManager zoomManager) {
		this.compositeType = compositeType;
		this.zoomManager = zoomManager;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.EditPartFactory#createEditPart(org.eclipse.gef.EditPart,
	 * java.lang.Object)
	 */
	public EditPart createEditPart(final EditPart context,
			final Object modelElement) {
		// get EditPart for model element
		EditPart part = null;
		try {
			part = getPartForElement(context, modelElement);
			part.setModel(modelElement);
		} catch (RuntimeException e) {
			Activator.getDefault().logError(e.getMessage(), e);		
		}
		// store model element in EditPart
		return part;
	}

	/**
	 * Maps an object to an EditPart.
	 * 
	 * @param context
	 *          the context
	 * @param modelElement
	 *          the model element
	 * 
	 * @return the part for element
	 * 
	 * @throws RuntimeException
	 *           if no match was found (programming error)
	 */
	protected EditPart getPartForElement(final EditPart context,
			final Object modelElement) {
		if (modelElement instanceof UIFBNetwork) {
			CompositeNetworkEditPart compositeNetEP = new CompositeNetworkEditPart();
			compositeNetEP.setFbType(compositeType);
			return compositeNetEP;

		}
		if (modelElement instanceof FBView) {
			return new FBEditPart(zoomManager);
		}
		if (modelElement instanceof CompositeInternalInterfaceElementView) {
			return new CompositeInternalInterfaceEditPart();
		}
		if (modelElement instanceof InterfaceElementView) {
			return new InterfaceEditPart();
		}
		if (modelElement instanceof ConnectionView) {
			return new ConnectionEditPart();
		}
		if (modelElement instanceof Value) {
			return new ValueEditPart();
		}

		throw new RuntimeException(MessageFormat.format(
				Messages.CompositeNetworkEditPartFactory_ERROR_Message,
				new Object[] { ((modelElement != null) ? modelElement.getClass()
						.getName() : "null") })); //$NON-NLS-1$
	}

}
