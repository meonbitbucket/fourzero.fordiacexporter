/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.network.actions;

import java.util.List;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.application.actions.PasteEditPartsAction;
import org.fordiac.ide.application.commands.PasteCommand;
import org.fordiac.ide.fbt.typeeditor.network.editparts.CompositeNetworkEditPart;
import org.fordiac.ide.gef.editparts.IDiagramEditPart;

public class CFBPasteEditPartsAction extends PasteEditPartsAction {
	
	public CFBPasteEditPartsAction(IWorkbenchPart editor) {
		super(editor);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected boolean calculateEnabled() {
		List selection = getSelectedObjects();
		Object obj = null;
		if (selection != null && selection.size() == 1) {
			obj = selection.get(0);
		}

		return ((obj instanceof CompositeNetworkEditPart) && createPasteCommand() != null);
	}

	@SuppressWarnings("rawtypes")
	protected Command createPasteCommand() {
		CompoundCommand cmd = new CompoundCommand();
		List selection = getSelectedObjects();
		if (selection != null && selection.size() == 1) {
			Object obj = selection.get(0);
			if (obj instanceof CompositeNetworkEditPart) {
				List templates = getClipboardContents();
				return new PasteCommand(templates, (IDiagramEditPart) obj, ((CompositeNetworkEditPart)obj).getFbType());

			}
		}
		return cmd;
	}

}
