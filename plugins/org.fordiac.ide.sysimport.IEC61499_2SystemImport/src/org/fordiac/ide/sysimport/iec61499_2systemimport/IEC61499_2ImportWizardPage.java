/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.sysimport.iec61499_2systemimport;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.fordiac.ide.application.commands.FBCreateCommand;
import org.fordiac.ide.application.commands.MapToCommand;
import org.fordiac.ide.gef.DiagramManager;
import org.fordiac.ide.model.Palette.DeviceTypePaletteEntry;
import org.fordiac.ide.model.Palette.FBTypePaletteEntry;
import org.fordiac.ide.model.Palette.Palette;
import org.fordiac.ide.model.Palette.PaletteEntry;
import org.fordiac.ide.model.Palette.PaletteFactory;
import org.fordiac.ide.model.Palette.ResourceTypeEntry;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.Connection;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.SystemConfiguration;
import org.fordiac.ide.model.libraryElement.SystemConfigurationNetwork;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.DeviceView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.model.ui.UISystemConfiguration;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.ide.model.ui.View;
import org.fordiac.ide.systemconfiguration.commands.DeviceCreateCommand;
import org.fordiac.ide.systemconfiguration.commands.ResourceCreateCommand;
import org.fordiac.ide.typeimport.ImportUtils;
import org.fordiac.ide.typelibrary.TypeLibrary;
import org.fordiac.ide.ui.controls.DirectoryChooserControl;
import org.fordiac.ide.ui.controls.FileChooserControl;
import org.fordiac.ide.ui.controls.IDirectoryChanged;
import org.fordiac.ide.ui.controls.IFileChanged;
import org.fordiac.ide.ui.wizardpages.ResultWizardPage;
import org.fordiac.ide.util.imageprovider.ImageProvider;
import org.fordiac.systemmanagement.SystemManager;
import org.fordiac.systemmanagement.util.SystemPaletteManagement;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * The Class IEC61499_2ImportWizardPage.
 * 
 * @author gebenh
 */
public class IEC61499_2ImportWizardPage extends WizardPage {

	private DirectoryChooserControl dcc;

	private FileChooserControl fcc;

	private final ResultWizardPage resultPage;
	private ArrayList<String> errors;
	private ArrayList<String> warnings;
	private ArrayList<String> infos;
	// private URI uri;
	/** Hashtable<originalName, renamedName> */
	private final Hashtable<String, String> fbreNameMapping = new Hashtable<String, String>();
	private final Hashtable<String, String> devicereNameMapping = new Hashtable<String, String>();
	private final Hashtable<String, FB> fbMapping = new Hashtable<String, FB>();
	private final Hashtable<String, FBView> fbViewMapping = new Hashtable<String, FBView>();
	private final Hashtable<String, FBView> mappedfbViewMapping = new Hashtable<String, FBView>();
	private final Hashtable<String, FBView> fromFBViewMapping = new Hashtable<String, FBView>();
	private final Hashtable<String, UIResourceEditor> toUIResourceEditorMapping = new Hashtable<String, UIResourceEditor>();

	private boolean includeDefaultLibrary = false;

	class ResourceFBToMappedFB {

		String[] sourcePath;
		String[] destPath;

		String dx1;
		String dx2;
		String dy;

		Diagram diagram;
		Connection connection;

	}

	private ArrayList<ResourceFBToMappedFB> resourceFBToMappedFBConnections;

	public boolean isIncludeDefaultLibrary() {
		return includeDefaultLibrary;
	}

	private UISystemConfiguration uiSysConf;

	protected IEC61499_2ImportWizardPage(String pageName,
			ResultWizardPage resultPage) {
		super(pageName);
		this.resultPage = resultPage;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NULL);
		composite.setFont(parent.getFont());

		initializeDialogUnits(parent);

		composite.setLayout(new GridLayout());
		composite.setLayoutData(new GridData(GridData.FILL_BOTH));

		GridData stretch = new GridData();
		stretch.grabExcessHorizontalSpace = true;
		stretch.horizontalAlignment = SWT.FILL;

		GridData stretchFcc = new GridData();
		stretchFcc.grabExcessHorizontalSpace = true;
		stretchFcc.horizontalAlignment = SWT.FILL;
		Label systemFile = new Label(composite, SWT.NONE);
		systemFile.setText("Select a *.sys file");

		fcc = new FileChooserControl(composite, SWT.NONE, "System",
				new String[] { "System" }, new String[] { "*.sys" });
		fcc.setLayoutData(stretchFcc);
		fcc.addFileChangedListener(new IFileChanged() {

			@Override
			public void fileChanged(String newFile) {
				setPageComplete(validatePage());
			}
		});

		Label typeDirectory = new Label(composite, SWT.NONE);
		typeDirectory
				.setText("Specify the directory with the required FBTypes (e.g. the src directory of FBDK)");
		dcc = new DirectoryChooserControl(composite, SWT.NONE,
				"Types Directory");
		dcc.addDirectoryChangedListener(new IDirectoryChanged() {

			@Override
			public void directoryChanged(String newDirectory) {

				setPageComplete(validatePage());
			}
		});

		dcc.setLayoutData(stretch);

		Group options = new Group(composite, SWT.NONE);
		options.setText("Options");
		options.setLayout(new GridLayout());
		options.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		final Button includeDefaultLibraryBtn = new Button(options, SWT.CHECK);
		includeDefaultLibraryBtn
				.setText("Include Default Library during import");
		includeDefaultLibraryBtn.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				includeDefaultLibrary = includeDefaultLibraryBtn.getSelection();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});

		// TODO remove this label if fixed!
		CLabel l = new CLabel(composite, SWT.NONE | SWT.LEFT);
		l.setText("Currently the import of Parameters of Resources is not supported. All other parts should be correctly imported.");
		l.setImage(ImageProvider.getImage("warning1.gif"));
		l.pack();
		setPageComplete(validatePage());
		// Show description on opening
		setErrorMessage(null);
		setMessage(null);
		setControl(composite);
	}

	private boolean validatePage() {
		if (fcc.getFile() == null || fcc.getFile().equals("")) {
			setErrorMessage("No File choosen!");
			return false;
		}
		if (dcc.getDirectory() == null || dcc.getDirectory().equals("")) {
			setErrorMessage("No type directory choosen!");
			return false;
		}
		if (!new File(fcc.getFile()).exists()) {
			setErrorMessage("The selected File does not exist!");
			return false;

		}
		setErrorMessage(null);
		setMessage(null);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.WizardPage#getNextPage()
	 */
	@Override
	public IWizardPage getNextPage() {
		if (resultPage != null) {
			return resultPage;
		}
		return super.getNextPage();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.DialogPage#setVisible(boolean)
	 */
	@Override
	public void setVisible(boolean visible) {
		errors = new ArrayList<String>();
		warnings = new ArrayList<String>();
		infos = new ArrayList<String>();
		resourceFBToMappedFBConnections = new ArrayList<IEC61499_2ImportWizardPage.ResourceFBToMappedFB>();

		if (!visible && getNextPage().equals(resultPage)) {
			File systemFile = new File(fcc.getFile());
			if (systemFile.exists()) {
				// do import
				DocumentBuilderFactory dbf = DocumentBuilderFactory
						.newInstance();
				dbf.setNamespaceAware(false);
				DocumentBuilder db;

				// TODO: set local dtd for validating!
				dbf.setAttribute(
						"http://apache.org/xml/features/nonvalidating/load-external-dtd",
						Boolean.FALSE);
				try {
					db = dbf.newDocumentBuilder();
					Document document;
					document = db.parse(systemFile);

					NodeList systemList = document
							.getElementsByTagName("System");
					if (systemList.getLength() > 0) {
						Node node = systemList.item(0);
						if (node instanceof Element) {
							Element systemElement = (Element) node;
							// get the system name and create a Project with a valid system name
							String systemName = systemElement
									.getAttribute("Name");
							systemName = SystemManager
									.getValidSystemName(systemName);
							final AutomationSystem automationSystem = createProject(systemName);

							// import the fbtypes specified
							final File directory = new File(
									dcc.getDirectory());
							if (!directory.exists()) {
								errors.add("Selected type directory does not exist!");
								// TODO cleanup - delete project and all
								// allocated resources!
							} else {
								try {

									importIEC61499Types(automationSystem,
											directory);
								} catch (Exception ex) {
									warnings.add("Problem importing all Types from specified Directory!");
								}
								
								
								NodeList applicationList = document
										.getElementsByTagName("Application");
								for (int i = 0; i < applicationList
										.getLength(); i++) {
									Node appNode = applicationList.item(i);
									if (appNode instanceof Element) {
										Element applicationElement = (Element) appNode;
										createApplication(automationSystem,
												applicationElement);

									}
								}
								NodeList deviceList = document
										.getElementsByTagName("Device");
								for (int i = 0; i < deviceList.getLength(); i++) {
									Node devNode = deviceList.item(i);
									if (devNode instanceof Element) {
										Element devElement = (Element) devNode;
										createDevice(automationSystem,
												devElement);

									}
								}
								NodeList mappingList = document
										.getElementsByTagName("Mapping");
								for (int i = 0; i < mappingList.getLength(); i++) {
									Node mappingNode = mappingList.item(i);
									if (mappingNode instanceof Element) {
										Element mappingElement = (Element) mappingNode;
										mappFB(automationSystem,
												mappingElement);

									}
								}

								createResourceFBToMappedFBConnections();

							}
							SystemManager.getInstance().saveDiagram(
									uiSysConf, automationSystem,
									"SysConf.xml"); //$NON-NLS-1$
							SystemManager.getInstance().saveSystem(
									automationSystem, true);
							// SystemManager.getInstance().forceReload();

						}
					}
				} catch (Exception e) {
					Activator.getDefault().logError(e.getMessage(), e);
				}

			} else {
				ArrayList<String> error = new ArrayList<String>();
				error.add(MessageFormat.format(
						"Selected system file ({0}) does not exist!",
						fcc.getFile()));
				resultPage.setErrors(error);
				ArrayList<String> temp = new ArrayList<String>();
				resultPage.setWarnings(temp);
				resultPage.setInfos(temp);
				resultPage.printMessages();
			}

			resultPage.setErrors(errors);
			resultPage.setWarnings(warnings);
			resultPage.setInfos(infos);
			resultPage.printMessages();

		}
		super.setVisible(visible);

	}

	private void createResourceFBToMappedFBConnections() {
		for (ResourceFBToMappedFB rfbTmFB : resourceFBToMappedFBConnections) {

			String sourceFB = rfbTmFB.sourcePath[rfbTmFB.sourcePath.length - 2];
			String sourceInterface = rfbTmFB.sourcePath[rfbTmFB.sourcePath.length - 1];
			String destFB = rfbTmFB.destPath[rfbTmFB.destPath.length - 2];
			String destInterface = rfbTmFB.destPath[rfbTmFB.destPath.length - 1];

			FB fbSource = fbMapping.get(sourceFB);
			FB fbDest = fbMapping.get(destFB);
			// only create connection if source
			// and dest fb exist
			if (fbSource == null || fbDest == null) {
				fbDoesNotExistWarning(sourceFB, sourceInterface, destFB,
						destInterface, fbDest == null ? destFB : sourceFB);
			} else if (rfbTmFB.connection instanceof EventConnection) {


				rfbTmFB.connection.setResTypeConnection(false);
				rfbTmFB.connection.setSource((Event) getInterfaceElement(fbSource,
						sourceInterface));
				rfbTmFB.connection.setDestination((Event) getInterfaceElement(fbDest,
						destInterface));
				// import the coordinates

				setConnectionCoordinates(rfbTmFB.dx1, rfbTmFB.dx2, rfbTmFB.dy,
						rfbTmFB.connection);

				ConnectionView connView = UiFactory.eINSTANCE
						.createConnectionView();
				connView.setConnectionElement(rfbTmFB.connection);
				if (rfbTmFB.sourcePath.length == 2) {
					connView.setSource(getInterfaceElementView(
							fbViewMapping.get(sourceFB), sourceInterface));
				} else if (rfbTmFB.sourcePath.length == 3) {
					connView.setSource(getInterfaceElementView(
							mappedfbViewMapping.get(sourceFB), sourceInterface));
				} else {
					warnings.add("Defect Connection?");
					break;
				}
				if (rfbTmFB.destPath.length == 2) {
					connView.setDestination(getInterfaceElementView(
							fbViewMapping.get(destFB), destInterface));
				} else if (rfbTmFB.destPath.length == 3) {
					connView.setDestination(getInterfaceElementView(
							mappedfbViewMapping.get(destFB), destInterface));
				} else {
					warnings.add("Defect Connection?");
					break;
				}

				rfbTmFB.diagram.getFunctionBlockNetwork().getEventConnections()
						.add((EventConnection)rfbTmFB.connection);
				rfbTmFB.diagram.getConnections().add(connView);

			} else if (rfbTmFB.connection instanceof DataConnection) {


				rfbTmFB.connection.setResTypeConnection(false);
				rfbTmFB.connection.setSource((VarDeclaration) getInterfaceElement(fbSource,
						sourceInterface));
				rfbTmFB.connection.setDestination((VarDeclaration) getInterfaceElement(fbDest,
						destInterface));
				// import the coordinates

				setConnectionCoordinates(rfbTmFB.dx1, rfbTmFB.dx2, rfbTmFB.dy,
						rfbTmFB.connection);

				ConnectionView connView = UiFactory.eINSTANCE
						.createConnectionView();
				connView.setConnectionElement(rfbTmFB.connection);
				if (rfbTmFB.sourcePath.length == 2) {
					connView.setSource(getInterfaceElementView(
							fbViewMapping.get(sourceFB), sourceInterface));
				} else if (rfbTmFB.sourcePath.length == 3) {
					connView.setSource(getInterfaceElementView(
							mappedfbViewMapping.get(sourceFB), sourceInterface));
				} else {
					warnings.add("Defect Connection?");
					break;
				}
				if (rfbTmFB.destPath.length == 2) {
					connView.setDestination(getInterfaceElementView(
							fbViewMapping.get(destFB), destInterface));
				} else if (rfbTmFB.destPath.length == 3) {
					connView.setDestination(getInterfaceElementView(
							mappedfbViewMapping.get(destFB), destInterface));
				} else {
					warnings.add("Defect Connection?");
					break;
				}

				rfbTmFB.diagram.getFunctionBlockNetwork().getDataConnections()
						.add((DataConnection)rfbTmFB.connection);
				rfbTmFB.diagram.getConnections().add(connView);

			}
		}

	}

	private void mappFB(AutomationSystem automationSystem,
			Element mappingElement) {
		String from = mappingElement.getAttribute("From");
		String to = mappingElement.getAttribute("To");
		String[] toArray = to.split("\\.");
		if (toArray.length == 3) {
			FBView fbView = fromFBViewMapping.get(from);
			UIResourceEditor uiResEditor = toUIResourceEditorMapping
					.get(devicereNameMapping.get(toArray[0]) + "." + toArray[1]);

			if (fbView != null && uiResEditor != null) {
				MapToCommand cmd = new MapToCommand(fbView, uiResEditor);
				cmd.execute();
				FBView mappedFBView = cmd.getMappedFBView();
				mappedfbViewMapping.put(fbView.getFb().getName(), mappedFBView);
			} else {
				warnings.add("Mapping from " + from + " to " + to
						+ " was not executed!");
			}
		}

	}

	private void createDevice(AutomationSystem automationSystem,
			Element devElement) {
		String devName = devElement.getAttribute("Name");
		String devType = devElement.getAttribute("Type");

		List<PaletteEntry> entries = automationSystem.getPalette().getTypeEntries(devType);

		Rectangle rect = new Rectangle(0, 0, -1, -1);

		String xString = devElement.getAttribute("x");
		String yString = devElement.getAttribute("y");

		try {
			int x = ImportUtils.convertCoordinate(Float.parseFloat(xString));
			int y = ImportUtils.convertCoordinate(Float.parseFloat(yString));
			rect.setLocation(x, y);
		} catch (NumberFormatException nfe) {
			warnings.add(" - Problems reading coordinates occured");
		}
		if (entries.size() > 0
				&& entries.get(0) instanceof DeviceTypePaletteEntry) {
			DeviceCreateCommand cmd = new DeviceCreateCommand(
					(DeviceTypePaletteEntry) entries.get(0),
					(UISystemConfiguration) automationSystem
							.getSystemConfiguration()
							.getSystemConfigurationNetwork().eContainer(),
					rect, automationSystem);
			cmd.execute();
			cmd.getDevice().setName(devName);
			devicereNameMapping.put(devName, cmd.getDevice().getName());
			NodeList deviceChildNodes = devElement.getChildNodes();
			for (int l = 0; l < deviceChildNodes.getLength(); l++) {
				Node paramNode = deviceChildNodes.item(l);
				if (paramNode instanceof Element) {
					Element paramElem = (Element) paramNode;
					if (paramElem.getNodeName().equalsIgnoreCase("Parameter")) {
						String paramName = paramElem.getAttribute("Name");
						String paramValue = paramElem.getAttribute("Value");
						IInterfaceElement interfaceEelemnt = getInterfaceElement(
								cmd.getDevice(), paramName);
						if (interfaceEelemnt != null) {
							interfaceEelemnt.getValue().setValue(paramValue);
						} else {
							warnings.add(MessageFormat
									.format("Inputvariable {0} for FB {1} not found - Parameter not set!",
											paramName, devName));
						}
					} else if (paramElem.getNodeName().equalsIgnoreCase(
							"Resource")) {
						parseResource(automationSystem, cmd.getDevice(),
								cmd.getDeviceView(), paramElem);
					}
				}
			}

		}
	}

	private void parseResource(AutomationSystem automationSystem,
			Device device, DeviceView view, Element resElem) {
		String resName = resElem.getAttribute("Name");
		String resType = resElem.getAttribute("Type");
		List<PaletteEntry> entries = automationSystem.getPalette().getTypeEntries(resType);

		if (entries.size() > 0 && entries.get(0) instanceof ResourceTypeEntry) {
			ResourceCreateCommand cmd = new ResourceCreateCommand(
					(ResourceTypeEntry) entries.get(0),
					view.getResourceContainerView(), automationSystem);
			cmd.execute();
			cmd.getResource().setName(resName);
			// System.out.println(cmd.getResource());
			if (cmd.getResView() != null
					&& cmd.getResView().getUIResourceDiagram() != null
					&& cmd.getResView().getUIResourceDiagram().getChildren() != null) {

				toUIResourceEditorMapping.put(device.getName() + "." + resName,
						cmd.getResView().getUIResourceDiagram());
				// add resourcetype to list of available fbs for connection
				// creation of resource fbs
				for (View viewElem : cmd.getResView().getUIResourceDiagram()
						.getChildren()) {
					if (viewElem instanceof FBView) {
						if (((FBView) viewElem).getFb() != null
								&& ((FBView) viewElem).getFb()
										.isResourceTypeFB()) {

							fbreNameMapping.put(((FBView) viewElem).getFb()
									.getName(), ((FBView) viewElem).getFb()
									.getName());
							fbViewMapping.put(((FBView) viewElem).getFb()
									.getName(), (FBView) viewElem);
							fbMapping.put(
									((FBView) viewElem).getFb().getName(),
									((FBView) viewElem).getFb());
						}
					}
				}
			}
			NodeList childNodes = resElem.getChildNodes();
			parseFBNetwork(automationSystem, cmd.getUIResourceEditor(),
					childNodes, true, "");
		}
	}

	private void createApplication(final AutomationSystem automationSystem,
			Element applicationElement) {
		String appName = applicationElement.getAttribute("Name");

		Application application = LibraryElementFactory.eINSTANCE
				.createApplication();
		int i = 1;
		while (!SystemManager.isValidAppName(appName, automationSystem)) {
			appName = appName + "_" + i;
			i++;
		}
		application.setName(appName);

		String comment = applicationElement.getAttribute("Comment");
		application.setComment(comment);
		FBNetwork network = LibraryElementFactory.eINSTANCE.createFBNetwork();
		// SystemManager.getInstance().saveSystem(automationSystem, false);

		UIFBNetwork uifbNetwork = UiFactory.eINSTANCE.createUIFBNetwork();
		uifbNetwork.setFbNetwork(network);

		NodeList childNodes = applicationElement.getChildNodes();
		parseFBNetwork(automationSystem, uifbNetwork, childNodes, false,
				appName);
		// network.setApplication(application); // FIX @ 2009-06-03 by gebenh
		application.setFBNetwork(network);
		automationSystem.addApplication(application);
		SystemManager.getInstance().saveDiagram(uifbNetwork, automationSystem,
				application.getName() + ".xml"); //$NON-NLS-1$
		// SystemManager.getInstance().saveSystem(automationSystem, false);
	}

	private void parseFBNetwork(final AutomationSystem automationSystem,
			Diagram uifbNetwork, NodeList childNodes, boolean resourceFB,
			String appName) {
		for (int j = 0; j < childNodes.getLength(); j++) {
			Node node = childNodes.item(j);
			if (node instanceof Element) {
				Element el = (Element) node;
				if (el.getNodeName().equalsIgnoreCase("SubAppNetwork")
						|| el.getNodeName().equalsIgnoreCase("FBNetwork")) {
					NodeList fbNodes = el.getChildNodes();
					for (int k = 0; k < fbNodes.getLength(); k++) {
						Node fbNode = fbNodes.item(k);
						if (fbNode instanceof Element) {
							Element fbElement = (Element) fbNode;
							if (fbElement.getNodeName().equalsIgnoreCase("FB")) {
								String fbType = fbElement.getAttribute("Type");
								if (fbType != null && !fbType.equals("")) {
									List<PaletteEntry> entries = automationSystem.getPalette().getTypeEntries(fbType);
									if (entries.size() > 0
											&& entries.get(0) instanceof FBTypePaletteEntry) {
										Rectangle rect = new Rectangle(0, 0,
												-1, -1);

										String xString = fbElement
												.getAttribute("x");
										String yString = fbElement
												.getAttribute("y");

										try {
											int x = ImportUtils
													.convertCoordinate(Float
															.parseFloat(xString));
											int y = ImportUtils
													.convertCoordinate(Float
															.parseFloat(yString));
											rect.setLocation(x, y);
										} catch (NumberFormatException nfe) {
											warnings.add(" - Problems reading coordinates occured");
										}
										FBCreateCommand cmd = new FBCreateCommand(
												(FBTypePaletteEntry) entries
														.get(0),
												uifbNetwork, rect);
										cmd.setCreateResourceFB(resourceFB);
										cmd.execute();

										if (cmd.getFB() != null) {

											String fbName = fbElement
													.getAttribute("Name");
											String fbComment = fbElement
													.getAttribute("Comment");
											if (fbName != null
													&& !fbName.equals("")) {
												cmd.getFB().setName(fbName);
												fbreNameMapping.put(fbName, cmd
														.getFB().getName());
												
												FBView fbView = DiagramManager.createFBView(cmd.getFB(), 
														uifbNetwork.getNetwork());
												
												fbViewMapping.put(fbName,
														fbView);
												fromFBViewMapping.put(appName
														+ "." + fbName,
														fbView);
												fbMapping.put(fbName,
														cmd.getFB());
											}
											if (fbComment != null) {
												cmd.getFB().setComment(
														fbComment);
											}
											NodeList parameterList = fbElement
													.getChildNodes();
											for (int l = 0; l < parameterList
													.getLength(); l++) {
												Node paramNode = parameterList
														.item(l);
												if (paramNode instanceof Element) {
													Element paramElem = (Element) paramNode;
													if (paramElem
															.getNodeName()
															.equalsIgnoreCase(
																	"Parameter")) {
														String paramName = paramElem
																.getAttribute("Name");
														String paramValue = paramElem
																.getAttribute("Value");
														IInterfaceElement interfaceEelemnt = getInterfaceElement(
																cmd.getFB(),
																paramName);
														if (interfaceEelemnt != null) {
															interfaceEelemnt
																	.getValue()
																	.setValue(
																			paramValue);
														} else {
															warnings.add(MessageFormat
																	.format("Inputvariable {0} for FB {1} not found - Parameter not set!",
																			paramName,
																			fbName));
														}
													}
												}
											}
										}

									} else {
										errors.add(MessageFormat
												.format(" - FBType ({0}) not found! - Instance not added to the network.",
														fbType));
									}
								}
							} else if (fbElement.getNodeName()
									.equalsIgnoreCase("EventConnections")) {
								NodeList connectionList = fbElement
										.getChildNodes();
								for (int l = 0; l < connectionList.getLength(); l++) {
									Node connectionNode = connectionList
											.item(l);
									if (connectionNode instanceof Element) {
										Element connectionElement = (Element) connectionNode;
										String source = connectionElement
												.getAttribute("Source");
										String destination = connectionElement
												.getAttribute("Destination");
										String dx1 = connectionElement
												.getAttribute("dx1");
										String dx2 = connectionElement
												.getAttribute("dx2");
										String dy = connectionElement
												.getAttribute("dy");

										String[] sourcePath = source
												.split("\\.");
										String[] destPath = destination
												.split("\\.");
										if (sourcePath.length == 2
												&& destPath.length == 2) {
											String sourceFB = sourcePath[sourcePath.length - 2];
											String sourceInterface = sourcePath[sourcePath.length - 1];
											String destFB = destPath[destPath.length - 2];
											String destInterface = destPath[destPath.length - 1];

											FB fbSource = fbMapping
													.get(sourceFB);
											FB fbDest = fbMapping.get(destFB);
											// only create connection if source
											// and dest fb exist
											if (fbSource == null
													|| fbDest == null) {
												fbDoesNotExistWarning(sourceFB,
														sourceInterface,
														destFB, destInterface,
														fbDest == null ? destFB
																: sourceFB);
											} else {

												EventConnection eventCon = LibraryElementFactory.eINSTANCE
														.createEventConnection();
												eventCon.setResTypeConnection(false);
												eventCon.setSource((Event) getInterfaceElement(
														fbSource,
														sourceInterface));
												eventCon.setDestination((Event) getInterfaceElement(
														fbDest, destInterface));
												// import the coordinates

												setConnectionCoordinates(dx1,
														dx2, dy, eventCon);

												ConnectionView connView = UiFactory.eINSTANCE
														.createConnectionView();
												connView.setConnectionElement(eventCon);
												connView.setSource(getInterfaceElementView(
														fbViewMapping
																.get(sourceFB),
														sourceInterface));
												connView.setDestination(getInterfaceElementView(
														fbViewMapping
																.get(destFB),
														destInterface));

												uifbNetwork
														.getFunctionBlockNetwork()
														.getEventConnections()
														.add(eventCon);
												uifbNetwork.getConnections()
														.add(connView);

											}
										} else if ((sourcePath.length > 2 || destPath.length > 2)) {
											ResourceFBToMappedFB openConnection = new ResourceFBToMappedFB();
											openConnection.sourcePath = sourcePath;
											openConnection.destPath = destPath;
											openConnection.dx1 = dx1;
											openConnection.dx2 = dx2;
											openConnection.dy = dy;
											openConnection.diagram = uifbNetwork;
											openConnection.connection = LibraryElementFactory.eINSTANCE
													.createEventConnection();
											resourceFBToMappedFBConnections
													.add(openConnection);
										} else {
											warnings.add("Defect Connection ?");
										}
									}
								}

							} else if (fbElement.getNodeName()
									.equalsIgnoreCase("DataConnections")) {
								NodeList connectionList = fbElement
										.getChildNodes();
								for (int l = 0; l < connectionList.getLength(); l++) {
									Node connectionNode = connectionList
											.item(l);
									if (connectionNode instanceof Element) {
										Element connectionElement = (Element) connectionNode;
										String source = connectionElement
												.getAttribute("Source");
										String destination = connectionElement
												.getAttribute("Destination");
										String dx1 = connectionElement
												.getAttribute("dx1");
										String dx2 = connectionElement
												.getAttribute("dx2");
										String dy = connectionElement
												.getAttribute("dy");

										String[] sourcePath = source
												.split("\\.");
										String[] destPath = destination
												.split("\\.");
										if (sourcePath.length == 2
												&& destPath.length == 2) {
											String sourceFB = sourcePath[0];
											String sourceInterface = sourcePath[1];
											String destFB = destPath[0];
											String destInterface = destPath[1];
											FB fbSource = fbMapping
													.get(sourceFB);
											FB fbDest = fbMapping.get(destFB);
											if (fbSource == null
													|| fbDest == null) {
												fbDoesNotExistWarning(sourceFB,
														sourceInterface,
														destFB, destInterface,
														fbDest == null ? destFB
																: sourceFB);
											} else {

												DataConnection dataConn = LibraryElementFactory.eINSTANCE
														.createDataConnection();
												dataConn.setResTypeConnection(false);
												dataConn.setSource((VarDeclaration) getInterfaceElement(
														fbSource,
														sourceInterface));
												dataConn.setDestination((VarDeclaration) getInterfaceElement(
														fbDest, destInterface));
												// import the coordinates
												setConnectionCoordinates(dx1,
														dx2, dy, dataConn);

												ConnectionView connView = UiFactory.eINSTANCE
														.createConnectionView();
												connView.setConnectionElement(dataConn);
												connView.setSource(getInterfaceElementView(
														fbViewMapping
																.get(sourceFB),
														sourceInterface));
												connView.setDestination(getInterfaceElementView(
														fbViewMapping
																.get(destFB),
														destInterface));

												uifbNetwork
														.getFunctionBlockNetwork()
														.getDataConnections()
														.add(dataConn);
												uifbNetwork.getConnections()
														.add(connView);

											}
										} else if ((sourcePath.length > 2 || destPath.length > 2)) {
											ResourceFBToMappedFB openConnection = new ResourceFBToMappedFB();
											openConnection.sourcePath = sourcePath;
											openConnection.destPath = destPath;
											openConnection.dx1 = dx1;
											openConnection.dx2 = dx2;
											openConnection.dy = dy;
											openConnection.diagram = uifbNetwork;
											openConnection.connection = LibraryElementFactory.eINSTANCE
													.createDataConnection();
											resourceFBToMappedFBConnections
													.add(openConnection);
										} else {
											warnings.add("Defect Connection ?");
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private void setConnectionCoordinates(String dx1, String dx2, String dy,
			Connection connection) {
		connection.setDx1(ImportUtils.parseConnectionValue(dx1));
		connection.setDx2(ImportUtils.parseConnectionValue(dx2));
		connection.setDy(ImportUtils.parseConnectionValue(dy));
	}

	private void fbDoesNotExistWarning(String sourceFB, String sourceInterface,
			String destFB, String destInterface, String fbName) {
		warnings.add("Can not create connection " + sourceFB + "."
				+ sourceInterface + " -> " + destFB + "." + destInterface
				+ " because FB " + fbName + "does not exist.");
	}

	private InterfaceElementView getInterfaceElementView(FBView fbView,
			String name) {
		for (Iterator<InterfaceElementView> iterator = fbView
				.getInterfaceElements().iterator(); iterator.hasNext();) {
			InterfaceElementView interfaceElementView = (InterfaceElementView) iterator
					.next();
			if (interfaceElementView.getLabel().equals(name)) {
				return interfaceElementView;
			}
		}
		return null;
	}

	private IInterfaceElement getInterfaceElement(Device dev, String paramName) {
		for (Iterator<VarDeclaration> iterator = dev.getVarDeclarations()
				.iterator(); iterator.hasNext();) {
			IInterfaceElement interfaceElement = iterator.next();
			if (interfaceElement.getName().equals(paramName)) {
				return interfaceElement;
			}
		}
		return null;
	}

	private IInterfaceElement getInterfaceElement(FB fb, String paramName) {

		for (Iterator<VarDeclaration> iterator = fb.getInterface()
				.getInputVars().iterator(); iterator.hasNext();) {
			IInterfaceElement interfaceElement = iterator.next();
			if (interfaceElement.getName().equals(paramName)) {
				return interfaceElement;
			}
		}
		for (Iterator<VarDeclaration> iterator = fb.getInterface()
				.getOutputVars().iterator(); iterator.hasNext();) {
			IInterfaceElement interfaceElement = iterator.next();
			if (interfaceElement.getName().equals(paramName)) {
				return interfaceElement;
			}
		}
		for (Iterator<Event> iterator = fb.getInterface().getEventInputs()
				.iterator(); iterator.hasNext();) {
			IInterfaceElement interfaceElement = iterator.next();
			if (interfaceElement.getName().equals(paramName)) {
				return interfaceElement;
			}
		}
		for (Iterator<Event> iterator = fb.getInterface().getEventOutputs()
				.iterator(); iterator.hasNext();) {
			IInterfaceElement interfaceElement = iterator.next();
			if (interfaceElement.getName().equals(paramName)) {
				return interfaceElement;
			}
		}
		return null;
	}
	
	private final Hashtable<String, Boolean> selectedTypeFileNames = new Hashtable<String, Boolean>();

	private void importIEC61499Types(final AutomationSystem automationSystem, final File directory) {
		final ArrayList<File> files = new ArrayList<File>();
		getFiles(directory, files);
		
		storeSelectedTypes(files);
		
		for (File file : files) {
			Boolean imported = selectedTypeFileNames.get(file.getAbsolutePath());
			if ((null == imported) || (!imported)) {
				if (null != automationSystem) {
					importTypeFile(file, automationSystem.getPalette());
				}
			}
		}
	}
	
	private void storeSelectedTypes(final ArrayList<File> files) {
		for (File file : files) {
			if (!file.isDirectory()) {
				selectedTypeFileNames.put(file.getAbsolutePath(), false);
			}
		}
	}
	
	private void importTypeFile(final File srcFile, final Palette palette) {
		// get selected root directory
		String rootDir = dcc.getDirectory();
		String relativeDestionationPath = srcFile.getAbsolutePath().substring(rootDir.length());
		if (TypeLibrary.importTypeFile(srcFile, palette,relativeDestionationPath, true,
				selectedTypeFileNames.keySet(), getShell())) {
			selectedTypeFileNames.put(srcFile.getAbsolutePath(), true);
		}
	}

	private void getFiles(File directory, ArrayList<File> files) {
		File[] subDirs = directory.listFiles(new FileFilter() {

			@Override
			public boolean accept(File pathname) {
				return pathname.isDirectory();
			}
		});
		for (int i = 0; i < subDirs.length; i++) {
			File file = subDirs[i];
			getFiles(file, files);
		}
		File[] fbtFiles = directory.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File arg0, String arg1) {
				return TypeLibrary.isIEC61499TypeFile(arg1);
			}
		});
		for (int i = 0; i < fbtFiles.length; i++) {
			File file = fbtFiles[i];
			files.add(file);
		}
	}

	private AutomationSystem createProject(String systemName) {
		NullProgressMonitor monitor = new NullProgressMonitor();
		try {

			IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
			IProject project = root.getProject(systemName);
			IProjectDescription description = ResourcesPlugin.getWorkspace()
					.newProjectDescription(project.getName());

			description
					.setNatureIds(new String[] { "org.fordiac.systemManagement.DistributedNature" }); //$NON-NLS-1$

			project.create(description, monitor);
			project.open(monitor);

			// create the system
			org.fordiac.ide.model.libraryElement.AutomationSystem system = LibraryElementFactory.eINSTANCE
					.createAutomationSystem();

			system.setName(systemName);

			if (isIncludeDefaultLibrary()) {
				SystemPaletteManagement.copyToolTypeLibToProject(project);
			} else {
				Palette palette = PaletteFactory.eINSTANCE.createPalette();
				system.setPalette(palette);
			}

			// create PhysicalConfiguration
			SystemConfiguration sysConf = LibraryElementFactory.eINSTANCE
					.createSystemConfiguration();
			system.setSystemConfiguration(sysConf);

			SystemConfigurationNetwork sysConfNetwork = LibraryElementFactory.eINSTANCE
					.createSystemConfigurationNetwork();
			sysConf.setSystemConfigurationNetwork(sysConfNetwork);

			uiSysConf = UiFactory.eINSTANCE.createUISystemConfiguration();
			uiSysConf.setSystemConfigNetwork(sysConfNetwork);

			SystemManager.getInstance().addSystem(system, project);
			SystemManager.getInstance().saveDiagram(uiSysConf, system,
					"SysConf.xml"); //$NON-NLS-1$
			SystemManager.getInstance().saveSystem(system, false);

			AutomationSystem sys2 = SystemManager.getInstance()
					.getSystemForName(systemName);
			if (sys2 != null && !sys2.equals(system)) {
				system = sys2;
			}

			return system;
		} catch (CoreException x) {
			Activator.getDefault().logError(x.getMessage(), x);
		} finally {
			monitor.done();
		}
		return null;
	}
}
