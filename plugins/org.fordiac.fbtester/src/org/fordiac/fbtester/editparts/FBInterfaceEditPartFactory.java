/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.fbtester.editparts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.editparts.ZoomManager;
import org.fordiac.fbtester.Activator;
import org.fordiac.fbtester.model.TestElement;
import org.fordiac.ide.fbt.typeeditor.editparts.WithEditPart;
import org.fordiac.ide.fbt.typemanagement.FBTypeEditorInput;
import org.fordiac.ide.gef.editparts.ValueEditPart;
import org.fordiac.ide.model.Palette.Palette;
import org.fordiac.ide.model.libraryElement.Value;
import org.fordiac.ide.model.libraryElement.With;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;


/**
 * A factory for creating FBInterfaceEditPart objects.
 */
public class FBInterfaceEditPartFactory extends org.fordiac.ide.fbt.typeeditor.editparts.FBInterfaceEditPartFactory implements EditPartFactory {
	
	

	public FBInterfaceEditPartFactory(Palette systemPalette,
			ZoomManager zoomManager) {
		super(systemPalette, zoomManager);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.EditPartFactory#createEditPart(org.eclipse.gef.EditPart,
	 * java.lang.Object)
	 */
	public EditPart createEditPart(final EditPart context,
			final Object modelElement) {
		// get EditPart for model element
		EditPart part = null;
		try {
			part = getPartForElement(context, modelElement);
			// store model element in EditPart
			part.setModel(modelElement);
		} catch (RuntimeException e) {
			Activator.getDefault().logError(e.getMessage(), e);
		}
		return part;
	}

	/**
	 * Maps an object to an EditPart.
	 * 
	 * @param context
	 *          the context
	 * @param modelElement
	 *          the model element
	 * 
	 * @return the part for element
	 * 
	 * @throws RuntimeException
	 *           if no match was found (programming error)
	 */
	protected EditPart getPartForElement(final EditPart context,
			final Object modelElement) {
		if (modelElement instanceof FBTypeEditorInput && context == null) {
			return new FBTypeRootEditPart();
		}
		if (modelElement instanceof FBView) {
			return new org.fordiac.fbtester.editparts.FBEditPart(zoomManager);
		}

		if (modelElement instanceof InterfaceElementView) {
			org.fordiac.fbtester.editparts.InterfaceEditPart part = new org.fordiac.fbtester.editparts.InterfaceEditPart();
			part.setTopMargin(4);
			part.setBottomMargin(4);
			return part;
		}

		if (modelElement instanceof Value) {
			ValueEditPart part = new ValueEditPart();
			part.setContext(context);
			return part;
		}
		if (modelElement instanceof TestElement) {
			return ((TestElement) modelElement).createEditPart();
		}
		if (modelElement instanceof With) {
			return new WithEditPart();
		}
		return super.getPartForElement(context, modelElement);
	}
}
