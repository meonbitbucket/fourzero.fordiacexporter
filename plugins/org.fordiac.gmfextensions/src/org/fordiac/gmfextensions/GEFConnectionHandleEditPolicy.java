/******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    IBM Corporation - initial API and implementation 
 ****************************************************************************/
/**
 * modified by gerhard ebenhofer to work with "normal" gef editors
 * -> made abstract
 */
package org.fordiac.gmfextensions;

import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.FigureListener;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.LayerConstants;
import org.eclipse.gef.Tool;
import org.eclipse.gef.tools.SelectionTool;
import org.fordiac.gmfextensions.ConnectionHandle.HandleDirection;
import org.fordiac.ide.IHighlightEditPart;

/**
 * This editpolicy is responsible for adding the connection handles to a shape.
 * 
 * @author cmahoney
 */
public abstract class GEFConnectionHandleEditPolicy extends
		GEFDiagramAssistantEditPolicy {

	/**
	 * Listens to the owner figure being moved so the handles can be removed when
	 * this occurs.
	 */
	private class OwnerMovedListener implements FigureListener {

		/**
		 * @see org.eclipse.draw2d.FigureListener#figureMoved(org.eclipse.draw2d.IFigure)
		 */
		public void figureMoved(final IFigure source) {
			hideDiagramAssistant();
		}
	}

	/** listener for owner shape movement */
	private final OwnerMovedListener ownerMovedListener = new OwnerMovedListener();

	/** list of connection handles currently being displayed */
	@SuppressWarnings("rawtypes")
	private List handles = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gmf.runtime.diagram.ui.editpolicies.DiagramAssistantEditPolicy
	 * #isDiagramAssistant(java.lang.Object)
	 */
	@Override
	protected boolean isDiagramAssistant(final Object object) {
		return object instanceof ConnectionHandle;
	}

	/**
	 * Gets the two connection handle figures to be added to this shape if they
	 * support user gestures.
	 * 
	 * @return a list of <code>ConnectionHandle</code> objects
	 */
	@SuppressWarnings("rawtypes")
	protected abstract List getHandleFigures();

	/**
	 * Builds the applicable tooltip string based on whether the Modeling
	 * Assistant Service supports handle gestures on this element. If no gestures
	 * are supported, the tooltip returned will be null.
	 * 
	 * @param direction
	 *          the handle direction.
	 * @return tooltip the tooltip string; if null, the handle should be not be
	 *         displayed
	 */
	protected String buildTooltip(final HandleDirection direction) {
		return "Click and drag to create a connection."; //$NON-NLS-1$
	}

	@Override
	public void activate() {
		super.activate();

		((GraphicalEditPart) getHost()).getFigure().addFigureListener(
				ownerMovedListener);
	}

	@Override
	public void deactivate() {
		((GraphicalEditPart) getHost()).getFigure().removeFigureListener(
				ownerMovedListener);

		super.deactivate();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected void showDiagramAssistant(Point referencePoint) {
		if (referencePoint == null) {
			referencePoint = getHostFigure().getBounds().getRight();
		}
		handles = getHandleFigures();
		if (handles == null) {
			return;
		}

		ConnectionHandleLocator locator = getConnectionHandleLocator(referencePoint);
		IFigure layer = getLayer(LayerConstants.HANDLE_LAYER);
		for (Iterator iter = handles.iterator(); iter.hasNext();) {
			ConnectionHandle handle = (ConnectionHandle) iter.next();

			handle.setLocator(locator);
			locator.addHandle(handle);
			handle.addMouseMotionListener(this);

			layer.add(handle);

			// Register this figure with it's host editpart so mouse events
			// will be propagated to it's host.
			getHost().getViewer().getVisualPartMap().put(handle, getHost());
		}

		if (!shouldAvoidHidingDiagramAssistant()) {
			// dismiss the handles after a delay
			hideDiagramAssistantAfterDelay(getDisappearanceDelay());
		}
	}

	/**
	 * Removes the connection handles.
	 */
	@SuppressWarnings("rawtypes")
	@Override
	protected void hideDiagramAssistant() {
		if (getHost() instanceof IHighlightEditPart) {
			((IHighlightEditPart) getHost()).setHighlight(false);
		}
		if (handles == null) {
			return;
		}
		IFigure layer = getLayer(LayerConstants.HANDLE_LAYER);
		for (Iterator iter = handles.iterator(); iter.hasNext();) {
			IFigure handle = (IFigure) iter.next();
			handle.removeMouseMotionListener(this);
			layer.remove(handle);
			getHost().getViewer().getVisualPartMap().remove(handle);
		}
		handles = null;
	}

	private boolean isSelectionToolActive() {
		// getViewer calls getParent so check for null
		if (getHost() != null && getHost().getViewer() != null && getHost().getViewer().getEditDomain() != null && getHost().getParent() != null) {
			Tool theTool = getHost().getViewer().getEditDomain().getActiveTool();
			if ((theTool != null) && theTool instanceof SelectionTool) {
				return true;
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gmf.runtime.diagram.ui.editpolicies.DiagramAssistantEditPolicy
	 * #shouldShowDiagramAssistant()
	 */
	@Override
	protected boolean shouldShowDiagramAssistant() {
		if (handles != null || !isSelectionToolActive()) {
			return false;
		}
		return true;
	}

	/**
	 * get the connection handle locator using the host and the passed reference
	 * point
	 * 
	 * @param referencePoint
	 * @return <code>ConnectionHandleLocator</code>
	 */
	protected ConnectionHandleLocator getConnectionHandleLocator(
			final Point referencePoint) {
		return new ConnectionHandleLocator(getHostFigure(), referencePoint);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gmf.runtime.diagram.ui.editpolicies.DiagramAssistantEditPolicy
	 * #isDiagramAssistantShowing()
	 */
	@Override
	protected boolean isDiagramAssistantShowing() {
		return handles != null;
	}
}