/*******************************************************************************
 * Copyright (c) 2007 - 2008 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     February - April 2008: Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at 
 *     				- initial implementation 
 *******************************************************************************/
package org.fordiac.gmfextensions;

import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.FreeformLayeredPane;
import org.eclipse.draw2d.FreeformLayout;
import org.eclipse.draw2d.LayeredPane;
import org.eclipse.draw2d.ScalableFigure;
import org.eclipse.draw2d.Viewport;
import org.eclipse.gef.editparts.GridLayer;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;
import org.eclipse.gef.editparts.ZoomManager;

/**
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 * 
 */
public class AnimatedZoomScalableFreeformRootEditPart extends
		ScalableFreeformRootEditPart {

	public static final String TOPLAYER = "TOPLAYER";
	private final ZoomManager zoomManager;
	
	private ConnectionLayerEx connectionLayer; 
	
	public AnimatedZoomScalableFreeformRootEditPart() {
		zoomManager = new AnimatableZoomManager((ScalableFigure) getScaledLayers(),
				((Viewport) getFigure()));
		zoomManager.addZoomListener(new AnimatedZoomListener() {
			
			@Override
			public void zoomChanged(double zoom) {
			}
			
			@Override
			public void animatedZoomStarted() {
					ConnectionLayerEx.setJumpLinks(false);
			}
			
			@Override
			public void animatedZoomEnded() {
				ConnectionLayerEx.setJumpLinks(true);
				if (connectionLayer != null) {
					connectionLayer.cleanJumpLinks();
					connectionLayer.dirtyJumpLinks(zoomManager.getViewport().getBounds());
				}
			}
		});
	}

	
	/**
	 * Returns the zoomManager.
	 * 
	 * @return ZoomManager
	 */
	@Override
	public ZoomManager getZoomManager() {
		return zoomManager;
	}

	@Override
	protected LayeredPane createPrintableLayers() {
		FreeformLayeredPane layeredPane = new FreeformLayeredPane();
		layeredPane.add(new FreeformLayer(), PRIMARY_LAYER);
		layeredPane.add(connectionLayer = new ConnectionLayerEx(), CONNECTION_LAYER);
		FreeformLayer topLayer = new FreeformLayer();
		topLayer.setLayoutManager(new FreeformLayout());

		layeredPane.add(topLayer, TOPLAYER);
		return layeredPane;
	}

	@Override
	protected GridLayer createGridLayer() {
		return new GridLayerEx();
	}
}
