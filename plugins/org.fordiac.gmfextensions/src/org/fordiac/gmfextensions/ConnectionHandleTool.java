/******************************************************************************
 * Copyright (c) 2003, 2006 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    IBM Corporation - initial API and implementation 
 ****************************************************************************/
/**
 * copied from org.eclipse.gmf.runtime.diagram.ui.internal.tools.ConnectionHandleTool 
 * and modified for the usage in 4DIAC
 */
package org.fordiac.gmfextensions;

import java.util.List;

import org.eclipse.gef.DragTracker;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateUnspecifiedTypeConnectionRequest;
import org.eclipse.gmf.runtime.diagram.ui.tools.ConnectionCreationTool;
import org.eclipse.gmf.runtime.emf.ui.services.modelingassistant.ModelingAssistantService;
import org.eclipse.swt.widgets.Display;

/**
 * This tool is responsible for reacting to mouse events on the connection
 * handles. It will get a command to create a connection when the user clicks
 * and drags the handle. It will get a command to expand elements, when the user
 * clicks the handle. It also adds support to create relationships from target
 * to source.
 * 
 * @author cmahoney
 * @canBeSeenBy org.eclipse.gmf.runtime.diagram.ui.*
 */
public class ConnectionHandleTool extends ConnectionCreationTool implements
		DragTracker {

	/** Time in ms to display error icon when there are no related elements. */
	private static final int NO_RELATED_ELEMENTS_DISPLAY_TIME = 2000;

	/** the connection handle containing required information */
	private final ConnectionHandle connectionHandle;

	/**
	 * Constructor for ConnectionHandleTool.
	 * 
	 * @param connectionHandle
	 *          the connection handle
	 */
	public ConnectionHandleTool(
			org.fordiac.gmfextensions.ConnectionHandle connectionHandle) {
		this.connectionHandle = connectionHandle;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.tools.TargetingTool#createTargetRequest()
	 */
	@Override
	protected Request createTargetRequest() {
		if (getConnectionHandle().isIncoming()) {
			CreateUnspecifiedTypeConnectionRequest request = new CreateUnspecifiedTypeConnectionRequest(
					ModelingAssistantService.getInstance().getRelTypesOnTarget(
							getConnectionHandle().getOwner()), true, getPreferencesHint());
			request.setDirectionReversed(true);
			return request;
		} else {
			return new CreateUnspecifiedTypeConnectionRequest(
					ModelingAssistantService.getInstance().getRelTypesOnSource(
							getConnectionHandle().getOwner()), true, getPreferencesHint());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.tools.AbstractTool#getCommand()
	 */
	@Override
	protected Command getCommand() {
		if (getConnectionHandle().isIncoming()) {
			CreateUnspecifiedTypeConnectionRequest unspecifiedTypeRequest = (CreateUnspecifiedTypeConnectionRequest) getTargetRequest();
			unspecifiedTypeRequest.setDirectionReversed(true);
		}

		return super.getCommand();
	}

	/**
	 * When a double-click occurs, this is called first on the first mouse button
	 * up. In the case where a double-click is going to occur, we do not want the
	 * default behavior here (which is to create a self-connection). Therefore, we
	 * will only permit self-connections if the user has moved the mouse.
	 * 
	 * @see org.eclipse.gef.tools.AbstractTool#handleButtonUp(int)
	 */
	@Override
	protected boolean handleButtonUp(int button) {
		if (getDragMoveDelta().equals(0, 0)) {
			if (button == 3 && getCurrentViewer() != null
					&& getTargetEditPart() != null) {
				getCurrentViewer().select(getTargetEditPart());
			}
			if (button == 1 && getCurrentViewer() != null
					&& getTargetEditPart() != null) {
				getCurrentViewer().select(getTargetEditPart());
			}
			return true; // ignore this button up
		}
		return super.handleButtonUp(button);
	}

	/**
	 * On a double-click, the related elements are expanded.
	 * 
	 * @see org.eclipse.gef.tools.AbstractTool#handleDoubleClick(int)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	protected boolean handleDoubleClick(int button) {
		// When a connection is to be created, a dialog box may appear which
		// will cause
		// this tool to be deactivated. This behavior is overridden by setting
		// the
		// avoid deactivation flag.
		eraseSourceFeedback();
		setAvoidDeactivation(true);

		List relatedShapes = executeShowRelatedElementsCommand();
		if (relatedShapes != null && relatedShapes.size() < 2) {
			signalNoRelatedElements();
		}
		setAvoidDeactivation(false);
		deactivate();

		return true;
	}

	/**
	 * Gets the command to show related elements and arrange the new views.
	 * Prompts the user for the relationship types to choose if there are multiple
	 * types. Executes the command with a progress monitor.
	 * 
	 * @return the list of related shapes
	 */
	@SuppressWarnings("rawtypes")
	protected List executeShowRelatedElementsCommand() {
		// TODO implement this to work with 4DIAC
		return null;
	}

	/**
	 * Temporary shows a red X over the connection handle to indicate that there
	 * are no related elements to be expanded.
	 */
	protected void signalNoRelatedElements() {
		getConnectionHandle().addErrorIcon();
		Display.getCurrent().timerExec(NO_RELATED_ELEMENTS_DISPLAY_TIME,
				new Runnable() {

					public void run() {
						getConnectionHandle().removeErrorIcon();
					}
				});
	}

	/**
	 * Returns the connection handle.
	 * 
	 * @return the connection handle
	 */
	protected ConnectionHandle getConnectionHandle() {
		return connectionHandle;
	}

}
