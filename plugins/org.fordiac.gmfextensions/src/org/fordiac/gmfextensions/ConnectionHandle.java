/******************************************************************************
 * Copyright (c) 2003, 2006 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    IBM Corporation - initial API and implementation 
 ****************************************************************************/
/**
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 * -> changed package declaration, for usage with GEF editor
 * -> made abstract that the getImage Method has to be implemented 
 * @date April 2008
 */
package org.fordiac.gmfextensions;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.ImageFigure;
import org.eclipse.draw2d.Locator;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.TreeSearch;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.DragTracker;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.handles.AbstractHandle;
import org.eclipse.gmf.runtime.diagram.ui.l10n.SharedImages;
import org.eclipse.swt.graphics.Image;
import org.fordiac.ide.IHighlightEditPart;

/**
 * This is the handle figure used to represent a connection handle.
 * 
 * @author cmahoney
 */
public abstract class ConnectionHandle extends AbstractHandle {

	/**
	 * An enumeration of connection directions. OUTGOING = source to target
	 * INCOMING = target to source
	 */
	public static final class HandleDirection {
		private HandleDirection() {
			// empty
		}

		/** source to target */
		public static final HandleDirection OUTGOING = new HandleDirection();

		/** target to source */
		public static final HandleDirection INCOMING = new HandleDirection();
	}

	/** the error icon that can be superimposed on the connection handle image */
	private static final ImageFigure ERROR_IMAGE = new ImageFigure(SharedImages
			.get(SharedImages.IMG_ERROR));

	static {
		ERROR_IMAGE.setSize(
				SharedImages.get(SharedImages.IMG_ERROR).getBounds().width,
				SharedImages.get(SharedImages.IMG_ERROR).getBounds().height);
	}

	/** direction that the relationship is to be created */
	private HandleDirection handleDirection;

	/**
	 * Creates a new <code>ConnectionHandle</code>.
	 * 
	 * @param ownerEditPart
	 *          the editpart for which the handle belongs
	 * @param relationshipDirection
	 *          direction that the relationship is to be created
	 * @param tooltip
	 *          the tooltip
	 */
	public ConnectionHandle(final GraphicalEditPart ownerEditPart,
			final HandleDirection relationshipDirection, final String tooltip) {

		setOwner(ownerEditPart);
		setRelationshipDirection(relationshipDirection);

		// TODO check whether tooltip is necessary for 4DIAC
		if (ownerEditPart instanceof IHighlightEditPart
				&& ((IHighlightEditPart) ownerEditPart).getToolTip() != null) {
			setToolTip(((IHighlightEditPart) ownerEditPart).getToolTip());
		}

		// A stack layout is used so that the error icon can be overlayed on
		// top.
		setLayoutManager(new StackLayout());
	}

	/**
	 * @see org.eclipse.gef.handles.AbstractHandle#createDragTracker()
	 */
	@Override
	protected DragTracker createDragTracker() {
		return new org.fordiac.gmfextensions.ConnectionHandleTool(this);
	}

	/**
	 * @see org.eclipse.draw2d.IFigure#findFigureAt(int, int,
	 *      org.eclipse.draw2d.TreeSearch)
	 */
	@Override
	public IFigure findFigureAt(final int x, final int y, final TreeSearch search) {
		// return the ConnectionHandle and not the children figures
		if (containsPoint(x, y)) {
			return this;
		}
		return super.findFigureAt(x, y, search);
	}

	/**
	 * Make public.
	 * 
	 * @see org.eclipse.gef.handles.AbstractHandle#setLocator(org.eclipse.draw2d.Locator)
	 */
	@Override
	public void setLocator(final Locator locator) {
		super.setLocator(locator);
	}

	/**
	 * Make public.
	 * 
	 * @see org.eclipse.gef.handles.AbstractHandle#getOwner()
	 */
	@Override
	public GraphicalEditPart getOwner() {
		return super.getOwner();
	}

	/**
	 * Sets the direction that the relationship is to be created.
	 * 
	 * @param direction
	 *          the <code>HandleDirection</code> that the relationship is to be
	 *          created
	 */
	protected void setRelationshipDirection(final HandleDirection direction) {
		handleDirection = direction;
	}

	/**
	 * Is this for incoming relationships?
	 * 
	 * @return true if this is for incoming relationships, false otherwise
	 */
	public boolean isIncoming() {
		return handleDirection == HandleDirection.INCOMING;
	}

	/**
	 * Superimposes an error icon on this connection handle.
	 */
	public void addErrorIcon() {
		add(ERROR_IMAGE);
	}

	/**
	 * Removes the error icon if it is being displayed.
	 */
	public void removeErrorIcon() {
		if (getChildren().contains(ERROR_IMAGE)) {
			remove(ERROR_IMAGE);
		}
	}

	/**
	 * Updates the images used for the handles, based on the side they will appear
	 * on. Sets the location of the handles using the locator.
	 * 
	 * @see org.eclipse.draw2d.IFigure#validate()
	 */
	@Override
	public void validate() {
		if (isValid()) {
			return;
		}
		removeAll();
		int side = ((org.fordiac.gmfextensions.ConnectionHandleLocator) getLocator())
				.getBorderSide();

		final GraphicalEditPart owner = getOwner();
		Image image = getImage(side);
		final IFigure f = owner.getFigure();
		Rectangle bounds = f.getBounds();
		if (owner instanceof IHighlightEditPart) {
			IHighlightEditPart ep = (IHighlightEditPart) owner;
			((IHighlightEditPart) owner).setHighlight(true);
			if (ep.getConnectionHighlightBounds() != null) {
				bounds = ep.getConnectionHighlightBounds();
			}

		}

		ImageFigure imageFigure = null;
		if (image != null) {
			imageFigure = new ImageFigure(image);
			imageFigure.setSize(image.getBounds().width, image.getBounds().height);
		} else {
			imageFigure = new ImageFigure();
			imageFigure.setSize(bounds.width, bounds.height);
		}

		add(imageFigure);
		setSize(imageFigure.getSize().getUnioned(ERROR_IMAGE.getSize()));

		super.validate();
	}

	/**
	 * Gets the image to be used for the connection handle given the side of the
	 * shape where the connection handle will appear.
	 * 
	 * @param side
	 *          the side of the shape where the connection handle will appear, a
	 *          value in PositionConstants
	 * @return the image to be used for the connection handle
	 */
	protected abstract Image getImage(final int side);
}
