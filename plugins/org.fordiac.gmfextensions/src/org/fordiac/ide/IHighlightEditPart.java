/*******************************************************************************
 * Copyright (c) 2007 - 2009 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     July 2009: Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at 
 *     				- initial implementation
 *******************************************************************************/
package org.fordiac.ide;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Rectangle;

/**
 * The Interface IHighlightEditPart.
 */
public interface IHighlightEditPart {

	/**
	 * Sets the highlight.
	 * 
	 * @param highlighted
	 *          the new highlight
	 */
	public void setHighlight(boolean highlighted);

	/**
	 * Gets the bounds.
	 * 
	 * @return the bounds
	 */
	public Rectangle getConnectionHighlightBounds();

	/**
	 * Gets the tool tip.
	 * 
	 * @return the tool tip
	 */
	public IFigure getToolTip();
}
