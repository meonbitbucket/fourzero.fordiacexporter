/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.deployment.iec61499;

public class FDBK2_DeploymentExecutor extends DeploymentExecutor {

	/** The Constant PROFILE_NAME. */
	private static final String PROFILE_NAME = "FBDK2"; //$NON-NLS-1$
	
	private static final String WRITE_PARAMETER_MESSAGE = "<Request ID=\"{0}\" Action=\"WRITE\"><Parameter Value=\"{1}\" Reference=\"{2}\" /></Request>"; //$NON-NLS-1$
	
	public FDBK2_DeploymentExecutor() {
		super();
	}
	
	@Override
	public String getProfileName(){
		return PROFILE_NAME;
	}

	@Override
	protected String getWriteParameterMessage() {
		return WRITE_PARAMETER_MESSAGE;
	}

	
	
}
