/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.deployment.iec61499.preferences;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.fordiac.ide.deployment.iec61499.Activator;

/**
 * The Class HoloblocDeploymentPreferences.
 */
public class HoloblocDeploymentPreferences extends FieldEditorPreferencePage
		implements IWorkbenchPreferencePage {

	/** The Constant CONNECTION_TIMEOUT. */
	public static final String CONNECTION_TIMEOUT = "Connection Timeout";

	/**
	 * Instantiates a new holobloc deployment preferences.
	 */
	public HoloblocDeploymentPreferences() {
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription("A demonstration of a preference page implementation");
	}

	/**
	 * Creates the field editors. Field editors are abstractions of the common GUI
	 * blocks needed to manipulate various types of preferences. Each field editor
	 * knows how to save and restore itself.
	 */
	@Override
	public void createFieldEditors() {

		IntegerFieldEditor integerFieldEditor = new IntegerFieldEditor(
				CONNECTION_TIMEOUT, "Connection Timout in ms", getFieldEditorParent(),
				3000);
		integerFieldEditor.setValidRange(1, 60000);

		addField(integerFieldEditor);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
	}

}