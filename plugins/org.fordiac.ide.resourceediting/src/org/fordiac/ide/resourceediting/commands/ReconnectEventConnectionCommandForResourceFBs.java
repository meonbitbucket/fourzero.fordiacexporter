/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.resourceediting.commands;

import org.eclipse.gef.requests.ReconnectRequest;
import org.fordiac.ide.application.commands.ReconnectEventConnectionCommand;
import org.fordiac.ide.gef.editparts.InterfaceEditPart;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.util.commands.DeleteConnectionCommand;

/**
 * The Class ReconnectEventConnectionCommandForResourceFBs.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class ReconnectEventConnectionCommandForResourceFBs extends
		ReconnectEventConnectionCommand {

	/** The parent. */
	private Diagram parent;

	/**
	 * Instantiates a new reconnect event connection command for resource f bs.
	 * 
	 * @param request the request
	 */
	public ReconnectEventConnectionCommandForResourceFBs(
			final ReconnectRequest request) {
		super(request);
	}

	/**
	 * Do reconnect source.
	 */
	@Override
	protected void doReconnectSource() {
		cmd = new DeleteConnectionCommand((ConnectionView) request
				.getConnectionEditPart().getModel());
		eccc = new EventConnectionCreateCommandForResourceFBs();
		eccc.setSource((InterfaceEditPart) request.getTarget());
		eccc.setTarget((InterfaceEditPart) request.getConnectionEditPart()
				.getTarget());
		eccc.setParent((Diagram) ((ConnectionView) request
				.getConnectionEditPart().getModel()).getConnectionElement()
				.eContainer().eContainer());
		cmd.execute();
		eccc.execute();
	}

	/**
	 * Do reconnect target.
	 */
	@Override
	protected void doReconnectTarget() {
		cmd = new DeleteConnectionCommand((ConnectionView) request
				.getConnectionEditPart().getModel());
		eccc = new EventConnectionCreateCommandForResourceFBs();
		eccc.setSource((InterfaceEditPart) request.getConnectionEditPart()
				.getSource());
		eccc.setTarget((InterfaceEditPart) request.getTarget());
		eccc.setParent(parent);
		cmd.execute();
		eccc.execute();
	}

	/**
	 * Gets the parent.
	 * 
	 * @return the parent
	 */
	public Diagram getParent() {
		return parent;
	}

	/**
	 * Sets the parent.
	 * 
	 * @param parent the new parent
	 */
	public void setParent(final Diagram parent) {
		this.parent = parent;
	}

}
