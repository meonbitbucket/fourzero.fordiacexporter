/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.resourceediting;

import org.eclipse.osgi.util.NLS;

/**
 * The Class Messages.
 */
public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.fordiac.ide.resourceediting.messages"; //$NON-NLS-1$
	
	/** The Applications container edit part_ ico n_ close. */
	public static String ApplicationsContainerEditPart_ICON_CLOSE;
	
	/** The Applications container edit part_ ico n_ open. */
	public static String ApplicationsContainerEditPart_ICON_OPEN;
	
	/** The Button edit part_ ico n_ application. */
	public static String ButtonEditPart_ICON_Application;
	
	/** The Button edit part_ labe l_ not defined. */
	public static String ButtonEditPart_LABEL_NotDefined;
	
	/** The Open resource editor action_ erro r_creating editor. */
	public static String OpenResourceEditorAction_ERROR_creatingEditor;
	
	/** The Resource diagram edit part factory_ erro r_ creating edit part. */
	public static String ResourceDiagramEditPartFactory_ERROR_CreatingEditPart;
	
	/** The Resource diagram edit part factory_ null. */
	public static String ResourceDiagramEditPartFactory_NULL;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
		// private empty constructor
	}
}
