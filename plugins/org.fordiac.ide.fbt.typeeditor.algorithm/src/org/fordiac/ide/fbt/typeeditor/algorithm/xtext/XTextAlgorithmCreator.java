/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.algorithm.xtext;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.xtext.Constants;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceFactory;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.ui.editor.embedded.EmbeddedEditor;
import org.eclipse.xtext.ui.editor.embedded.EmbeddedEditorFactory;
import org.eclipse.xtext.ui.editor.embedded.IEditedResourceProvider;
import org.fordiac.ide.fbt.typeeditor.algorithm.IAlgorithmEditor;
import org.fordiac.ide.fbt.typeeditor.algorithm.IAlgorithmEditorCreator;
import org.fordiac.ide.model.libraryElement.BasicFBType;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;

@SuppressWarnings("restriction")
public class XTextAlgorithmCreator implements IAlgorithmEditorCreator {
	
	@Inject
	protected EmbeddedEditorFactory editorFactory;

	@Inject
	private Provider<XtextResourceSet> resourceSetProvider;

	@Inject
	private XtextResourceFactory resourceFactory;

	@Inject
	@Named(Constants.FILE_EXTENSIONS)
	public String fileExtension;
		
	public XTextAlgorithmCreator() {
	}

	@Override
	public IAlgorithmEditor createAlgorithmEditor(Composite parent, BasicFBType fbType) {
		IEditedResourceProvider resourceProvider = new IEditedResourceProvider() {

			@Override
			public XtextResource createResource() {
				XtextResourceSet xtextResourceSet = resourceSetProvider.get();
				Resource resource = resourceFactory
						.createResource(computeUnusedUri(xtextResourceSet));
				xtextResourceSet.getResources().add(resource);
				return (XtextResource) resource;
			}

			protected URI computeUnusedUri(ResourceSet resourceSet) {
				String name = "__synthetic";
				for (int i = 0; i < Integer.MAX_VALUE; i++) {
					URI syntheticUri = URI.createURI(name + i + "." + fileExtension);
					if (resourceSet.getResource(syntheticUri, false) == null)
						return syntheticUri;
				}
				throw new IllegalStateException();
			}
		};

		EmbeddedEditor editor = editorFactory.newEditor(resourceProvider)
				.showErrorAndWarningAnnotations().withParent(parent);
		return createXTextAlgorithmEditor(fbType, editor);
	}

	/** Factory method creating the Specific XTextAlgorithmEditor.  
	 * 
	 * Should be override if you need a special XTextAlogirthm which performs additional setups for your DSL.
	 * e.g., ST uses this to provide in the prefix the references to inputs, outputs, and internal variables
	 */
	protected XTextAlgorithmEditor createXTextAlgorithmEditor(BasicFBType fbType,
			EmbeddedEditor editor) {
		return new XTextAlgorithmEditor(editor, fbType);
	}

}
