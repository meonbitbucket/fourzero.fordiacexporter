/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.algorithm;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocumentListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.EditorPart;
import org.fordiac.ide.fbt.typeeditor.algorithm.commands.AlgorithmTextChangedCommand;
import org.fordiac.ide.fbt.typeeditor.algorithm.commands.ChangeAlgorithmNameCommand;
import org.fordiac.ide.fbt.typeeditor.algorithm.commands.ChangeAlgorithmTypeCommand;
import org.fordiac.ide.fbt.typeeditor.algorithm.commands.CreateAlgorithmCommand;
import org.fordiac.ide.fbt.typeeditor.algorithm.commands.DeleteAlgorithmCommand;
import org.fordiac.ide.fbt.typeeditor.editors.IFBTEditorPart;
import org.fordiac.ide.fbt.typemanagement.FBTypeEditorInput;
import org.fordiac.ide.model.libraryElement.Algorithm;
import org.fordiac.ide.model.libraryElement.BasicFBType;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.libraryElement.STAlgorithm;
import org.fordiac.ide.model.libraryElement.TextAlgorithm;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.libraryElement.provider.AlgorithmsItemProvider;
import org.fordiac.ide.util.IdentifierVerifyListener;
import org.fordiac.ide.util.STStringTokenHandling;
import org.fordiac.ide.util.commands.ChangeCommentCommand;
import org.fordiac.ide.util.imageprovider.ImageProvider;

/**
 * The Class AlgorithmEditor.
 */
public class AlgorithmEditor extends EditorPart implements IFBTEditorPart {

	/** The type. */
	private BasicFBType type;

	/** The name text. */
	private Text nameText;

	/** The adapter. */
	private final EContentAdapter adapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {
			super.notifyChanged(notification);
			if(!blockListeners) {
				switch(notification.getFeatureID(FBType.class)){
					case LibraryElementPackage.ALGORITHM:
					case LibraryElementPackage.ALGORITHM__NAME:
					case LibraryElementPackage.ALGORITHM__IDENTIFIER:
					case LibraryElementPackage.TEXT_ALGORITHM__TEXT:
					case LibraryElementPackage.BASIC_FB_TYPE__ALGORITHM:
						handleAlgorithmChanges();
						break;
					default:
						checkVariableNameChanges(notification);
						break;
				}
			}
		}

		protected void handleAlgorithmChanges() {
			Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
					blockListeners = true;
					
					if((null != algorithmsViewer) && (!algorithmsViewer.getControl().isDisposed())){
						algorithmsViewer.refresh();
						updateSelectedAlgorithm(selectedAlgorithm);
					}
					blockListeners = false;
				}
			});			
		}

		private void checkVariableNameChanges(Notification notification) {
			if ((notification.getEventType() == Notification.SET) && 
					(notification.getNotifier() instanceof VarDeclaration) &&
					(notification.getNewValue() instanceof String)&&
					(LibraryElementPackage.eINSTANCE.getINamedElement_Name().equals(notification.getFeature()))) {
				
				for (Algorithm algorithm : type.getAlgorithm()) {
					if(algorithm instanceof TextAlgorithm){
						TextAlgorithm textAlgorithm = (TextAlgorithm)algorithm;
						String oldAlgText = textAlgorithm.getText();
						String newAlgText = STStringTokenHandling.replaceSTToken(oldAlgText, 
								(String)notification.getOldStringValue(), (String)notification.getNewStringValue());
						textAlgorithm.setText(newAlgText);
						
						if(algorithm.equals(selectedAlgorithm)){
							updateSelectedAlgorithm(algorithm);
						}
					}
				}
			}			
		}

	};
	

	/**
	 * Instantiates a new algorithm editor.
	 */
	public AlgorithmEditor() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor
	 * )
	 */
	@Override
	public void doSave(final IProgressMonitor monitor) {
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.EditorPart#doSaveAs()
	 */
	@Override
	public void doSaveAs() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.EditorPart#init(org.eclipse.ui.IEditorSite,
	 * org.eclipse.ui.IEditorInput)
	 */
	@Override
	public void init(final IEditorSite site, final IEditorInput input)
			throws PartInitException {
		setInput(input);
		if (input instanceof FBTypeEditorInput) {
			FBTypeEditorInput untypedInput = (FBTypeEditorInput) input;
			if (untypedInput.getContent() instanceof BasicFBType) {
				type = (BasicFBType) untypedInput.getContent();
				type.eAdapters().add(adapter);
			}
		}
		setSite(site);
		setPartName("Algorithms");
		setTitleImage(ImageProvider.getImage("algorithm.gif"));

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.EditorPart#isDirty()
	 */
	@Override
	public boolean isDirty() {
		return getCommandStack().isDirty();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.EditorPart#isSaveAsAllowed()
	 */
	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	public void createPartControl(final Composite parent) {

		SashForm form = new SashForm(parent, SWT.HORIZONTAL);
		form.setLayout(new FillLayout());
		Composite algorithmsContainer = new Composite(form, SWT.BORDER);
		algorithmsContainer.setLayout(new GridLayout());

		Composite buttonContainer = new Composite(algorithmsContainer, SWT.NONE);
		buttonContainer.setLayout(new FillLayout());

		Button addButton = new Button(buttonContainer, SWT.NONE);
		addButton.setImage(ImageProvider.getImage("add_obj.gif"));
		addButton.setText("Add  ");
		addButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				CreateAlgorithmCommand cmd = new CreateAlgorithmCommand(type);
				getCommandStack().execute(cmd);
			}

			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
				// nothing to do
			}
		});

		Button deleteButton = new Button(buttonContainer, SWT.NONE);
		deleteButton.setImage(ImageProvider.getImage("delete_obj.gif"));
		deleteButton.setText("Delete");
		deleteButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				MessageBox deleteBox = new MessageBox(PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getShell(), SWT.YES | SWT.NO
						| SWT.ICON_QUESTION);
				deleteBox.setText("Delete Algorithm");
				deleteBox.setMessage(MessageFormat.format("Delete \"{0}\" ?",
						new Object[] { selectedAlgorithm.getName() }));
				int ret = deleteBox.open();
				if (ret == SWT.YES) {
					getCommandStack().execute(new DeleteAlgorithmCommand(type, selectedAlgorithm));
				}
			}

			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
				// nothing to do
			}
		});

		GridData algorithmsViewerGridData = new GridData();
		algorithmsViewerGridData.horizontalAlignment = GridData.FILL;
		algorithmsViewerGridData.verticalAlignment = GridData.FILL;
		algorithmsViewerGridData.grabExcessHorizontalSpace = true;
		algorithmsViewerGridData.grabExcessVerticalSpace = true;

		algorithmsViewer = new TableViewer(algorithmsContainer, SWT.SINGLE
				| SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		TableColumn column = new TableColumn(algorithmsViewer.getTable(), SWT.LEFT);
		column.setText("Algorithm"); //$NON-NLS-1$
		column.setWidth(80);

		column = new TableColumn(algorithmsViewer.getTable(), SWT.LEFT);
		column.setText("Language"); //$NON-NLS-1$
		column.setWidth(80);

		column = new TableColumn(algorithmsViewer.getTable(), SWT.LEFT);
		column.setText("Comment"); //$NON-NLS-1$
		column.setWidth(150);

		algorithmsViewer.getTable().setLayoutData(algorithmsViewerGridData);
		algorithmsViewer.getTable().setHeaderVisible(true);

		algorithmsViewer.setContentProvider(new AlgorithmsContentProvider(type));

		algorithmsViewer.setLabelProvider(new AlgorithmsLabelProvider());

		algorithmsViewer.setInput(new Object());
		algorithmsViewer
				.addSelectionChangedListener(new ISelectionChangedListener() {
					public void selectionChanged(final SelectionChangedEvent event) {
						if (event.getSelection() instanceof StructuredSelection) {
							StructuredSelection selection = (StructuredSelection) event
									.getSelection();
							updateSelectedAlgorithm((Algorithm) selection.getFirstElement());
						}
					}
				});

		Composite editingArea = new Composite(form, SWT.BORDER);
		editingArea.setLayout(new GridLayout(2, false));
		Label nameLabel = new Label(editingArea, SWT.NONE);
		nameLabel.setText("Name: ");

		nameText = new Text(editingArea, SWT.SINGLE | SWT.BORDER);
		nameText.addVerifyListener(new IdentifierVerifyListener());
		nameText.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				if (selectedAlgorithm != null) {
					if(!blockListeners){
						blockListeners = true;
						getCommandStack().execute(new ChangeAlgorithmNameCommand(type, selectedAlgorithm, nameText.getText()));
						algorithmsViewer.refresh();
						blockListeners = false;
					}
				}
			}
		});
		GridData textGridData = new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_BEGINNING);
		nameText.setLayoutData(textGridData);
		Label commentLabel = new Label(editingArea, SWT.NONE);
		commentLabel.setText("Comment: ");

		commentText = new Text(editingArea, SWT.SINGLE | SWT.BORDER);
		commentText.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				if (selectedAlgorithm != null) {
					if (!blockListeners) {
						blockListeners = true;
						getCommandStack().execute(new ChangeCommentCommand(selectedAlgorithm, commentText.getText()));
						algorithmsViewer.refresh();
						blockListeners = false;
					}
				}
			}
		});
		GridData commentTextGridData = new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_BEGINNING);
		commentText.setLayoutData(commentTextGridData);

		Label languageLabel = new Label(editingArea, SWT.NONE);
		languageLabel.setText("Language: ");

		languageCombo = new Combo(editingArea, SWT.SINGLE | SWT.READ_ONLY
				| SWT.BORDER);
		GridData languageComboGridData = new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_BEGINNING);
		languageCombo.setLayoutData(languageComboGridData);
		languageCombo.setItems(getLanguages().toArray(new String[0]));
		languageCombo.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				
				String language = getLanguages().get(languageCombo.getSelectionIndex());
				if(null != selectedAlgorithm){
					ChangeAlgorithmTypeCommand cmd = new ChangeAlgorithmTypeCommand(type, selectedAlgorithm, language);
					getCommandStack().execute(cmd);
					if(null != cmd.getNewAlgorithm()){
						updateSelectedAlgorithm(cmd.getNewAlgorithm());
					}
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// nothing to do
			}
		});

		GridData codeEditorsGridData = new GridData();
		codeEditorsGridData.horizontalAlignment = GridData.FILL;
		codeEditorsGridData.verticalAlignment = GridData.FILL;
		codeEditorsGridData.grabExcessHorizontalSpace = true;
		codeEditorsGridData.grabExcessVerticalSpace = true;
		codeEditorsGridData.horizontalSpan = 2;
		codeEditors = new Composite(editingArea, SWT.NONE);
		codeEditors.setLayout(stack = new StackLayout());
		codeEditors.setLayoutData(codeEditorsGridData);

		createEditors(codeEditors);

		form.setWeights(new int[] { 25, 75 });

	}

	/**
	 * Gets the languages.
	 * 
	 * @return the languages
	 */
	public List<String> getLanguages() {
		ArrayList<String> languages = new ArrayList<String>();
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint point = registry
				.getExtensionPoint("org.fordiac.ide.fbt.typeeditor.algorithm.algorithmEditor");
		IExtension[] extensions = point.getExtensions();
		for (int i = 0; i < extensions.length; i++) {
			IExtension extension = extensions[i];
			IConfigurationElement[] elements = extension.getConfigurationElements();
			for (int j = 0; j < elements.length; j++) {
				IConfigurationElement element = elements[j];
				try {
					Object obj = element.createExecutableExtension("class");
					if (obj instanceof IAlgorithmEditorCreator) {
						String lang = element.getAttribute("language");
						languages.add(lang);
					}
				} catch (Exception e) {
					// nothing to do
				}
			}
		}
		return languages;
	}

	/** The editors. */
	private final Hashtable<String, IAlgorithmEditor> editors = new Hashtable<String, IAlgorithmEditor>();

	/**
	 * Creates the editors.
	 * 
	 * @param parent
	 *          the parent
	 */
	public void createEditors(final Composite parent) {
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint point = registry
				.getExtensionPoint("org.fordiac.ide.fbt.typeeditor.algorithm.algorithmEditor");
		IExtension[] extensions = point.getExtensions();
		for (int i = 0; i < extensions.length; i++) {
			IExtension extension = extensions[i];
			IConfigurationElement[] elements = extension.getConfigurationElements();
			for (int j = 0; j < elements.length; j++) {
				IConfigurationElement element = elements[j];
				try {

					Object obj = element.createExecutableExtension("class");
					if (obj instanceof IAlgorithmEditorCreator) {
						IAlgorithmEditor editor = ((IAlgorithmEditorCreator) obj)
								.createAlgorithmEditor(parent, type);
						String lang = element.getAttribute("language");
						editors.put(lang, editor);
					}
				} catch (Exception e) {
					Activator.getDefault().logError(e.getMessage(), e);
				}
			}
		}

	}

	/** The selected algorithm. */
	private Algorithm selectedAlgorithm;

	/** The algorithms viewer. */
	private TableViewer algorithmsViewer;

	/** The comment text. */
	private Text commentText;

	/** The language combo. */
	private Combo languageCombo;

	/** The listener. */
	private final IDocumentListener listener = new IDocumentListener() {

		@Override
		public void documentChanged(final DocumentEvent event) {
			if ((selectedAlgorithm != null) && (null != currentAlgEditor)) {
				if(currentAlgEditor.isDocumentValid()){
					if (!blockListeners) {
						blockListeners = true;
						getCommandStack().execute(new AlgorithmTextChangedCommand((TextAlgorithm)selectedAlgorithm, currentAlgEditor.getAlgorithmText()));
						blockListeners = false;
					}
				}
			}
		}

		@Override
		public void documentAboutToBeChanged(final DocumentEvent event) {
		}

	};

	/** The stack. */
	private StackLayout stack;

	/** The code editors. */
	private Composite codeEditors;
	
	private IAlgorithmEditor currentAlgEditor = null; 
	
	boolean blockListeners = false;

	/**
	 * Update selected algorithm.
	 * 
	 * @param algorithm
	 *          the algorithm
	 */
	protected void updateSelectedAlgorithm(final Algorithm algorithm) {
		blockListeners = true;
		
		if(null != currentAlgEditor){
			currentAlgEditor.removeDocumentListener(listener);
		}
		
		if (algorithm != null) {
			nameText.setEnabled(true);
			commentText.setEnabled(true);
			languageCombo.setEnabled(true);
			selectedAlgorithm = algorithm;
			nameText.setText(algorithm.getName() != null ? algorithm.getName() : "");
			commentText.setText(algorithm.getComment() != null ? algorithm
					.getComment() : "");
			String algType = getAlgorithmTypeString(algorithm);
			currentAlgEditor = editors.get(algType);
			stack.topControl = currentAlgEditor.getControl();
			currentAlgEditor.setAlgorithmText(((TextAlgorithm) algorithm).getText());
			currentAlgEditor.addDocumentListener(listener);
			int index = getLanguages().indexOf(algType);
			languageCombo.select(index);
		} else {
			selectedAlgorithm = algorithm;
			nameText.setEnabled(false);
			commentText.setEnabled(false);
			languageCombo.setEnabled(false);			
			stack.topControl = null;
			nameText.setText("");
			commentText.setText("");
			languageCombo.select(-1);
		}

		codeEditors.layout();
		blockListeners = false;
	}

	private String getAlgorithmTypeString(Algorithm algorithm) {
		if (algorithm instanceof STAlgorithm) {
			return "ST";
		} else if (algorithm instanceof TextAlgorithm) {
			return "AnyText";
		}
		return "AnyText";  //per default return any text and show it as generic text
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.
	 * IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	@Override
	public void selectionChanged(final IWorkbenchPart part,
			final ISelection selection) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#dispose()
	 */
	@Override
	public void dispose() {
		if (type != null && type.eAdapters().contains(adapter)) {
			type.eAdapters().remove(adapter);
		}
		super.dispose();
	}

	@Override
	public boolean outlineSelectionChanged(Object selectedElement) {
		boolean bRetVal = false;
		if(selectedElement instanceof Algorithm){
			updateSelectedAlgorithm((Algorithm) selectedElement);
			bRetVal = true;
		}
		if(selectedElement instanceof AlgorithmsItemProvider){
			updateSelectedAlgorithm(null);
			bRetVal = true;
		}
		
		return bRetVal;
	}

	private org.eclipse.gef.commands.CommandStack commandStack;
	
	private org.eclipse.gef.commands.CommandStack getCommandStack() {
		return commandStack;
	}

	@Override
	public void setCommonCommandStack(
			org.eclipse.gef.commands.CommandStack commandStack) {
		this.commandStack = commandStack;
	}

}
