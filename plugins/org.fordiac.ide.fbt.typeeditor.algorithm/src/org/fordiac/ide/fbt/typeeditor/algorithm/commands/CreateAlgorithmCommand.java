/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.algorithm.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.NameRepository;
import org.fordiac.ide.model.libraryElement.BasicFBType;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.STAlgorithm;

public class CreateAlgorithmCommand extends Command {

	/** The fb type. */
	private final BasicFBType fbType;

	private STAlgorithm algorithm;
	
	public CreateAlgorithmCommand(final BasicFBType fbType){
		this.fbType = fbType;
	}
	
	@Override
	public void execute() {		
		algorithm = LibraryElementFactory.eINSTANCE.createSTAlgorithm();
		algorithm.setName(NameRepository.getUniqueAlgorithmName(algorithm, fbType, "ALG"));
		algorithm.setComment("new algorithm");
		algorithm.setText("");  //especially the xtext editor requires at least an empty algorithm text
		
		redo();
	}

	@Override
	public void undo() {
		fbType.getAlgorithm().remove(algorithm);
	}

	@Override
	public void redo() {
		fbType.getAlgorithm().add(algorithm);
	}
}
