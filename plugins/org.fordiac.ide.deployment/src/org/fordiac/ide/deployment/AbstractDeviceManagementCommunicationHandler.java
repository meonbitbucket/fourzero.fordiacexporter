/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.deployment;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;

import org.fordiac.ide.deployment.exceptions.DisconnectException;
import org.fordiac.ide.deployment.exceptions.InvalidMgmtID;
import org.fordiac.ide.deployment.util.IDeploymentListener;

/** Base class for coordinating the management communication to a device
 * 
 */
public abstract class AbstractDeviceManagementCommunicationHandler {
	
	/**
	 * Connect.
	 * 
	 * @param address
	 *          the address
	 * 
	 * @throws InvalidMgmtID
	 *           the invalid mgmt id
	 * @throws UnknownHostException
	 *           the unknown host exception
	 * @throws IOException
	 *           Signals that an I/O exception has occurred.
	 */
	abstract public void connect(String address) throws InvalidMgmtID,
			UnknownHostException, IOException;
	
	/**
	 * Disconnect.
	 * 
	 * @throws DisconnectException
	 *           the disconnect exception
	 */
	abstract public void disconnect() throws DisconnectException;

	
	/**
	 * Send req.
	 * 
	 * @param destination
	 *          the destination
	 * @param request
	 *          the request
	 * @param info
	 *          the info
	 * 
	 * @throws IOException
	 *           Signals that an I/O exception has occurred.
	 */
	abstract public void sendREQ(final String destination, final String request) throws IOException;
	
	
	/** The listeners. */
	private final ArrayList<IDeploymentListener> listeners = new ArrayList<IDeploymentListener>();

	/**
	 * Response received.
	 * 
	 * @param response
	 *          the response
	 * @param source
	 *          the source
	 */
	protected void responseReceived(final String response, final String source) {
		for (Iterator<IDeploymentListener> iterator = listeners.iterator(); iterator
				.hasNext();) {
			IDeploymentListener listener = iterator.next();
			listener.responseReceived(response, source);
		}
	}

	protected void postCommandSent(String info, String destination, String command) {
		
		for (Iterator<IDeploymentListener> iterator = listeners.iterator(); iterator
				.hasNext();) {
			IDeploymentListener listener = iterator.next();
			listener.postCommandSent(command, info);
			listener.postCommandSent(info, destination, command);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.deployment.IDeploymentExecutor#addDeploymentListener(
	 * org.fordiac.ide.deployment.util.IDeploymentListener)
	 */
	public void addDeploymentListener(final IDeploymentListener listener) {
		if (!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.deployment.IDeploymentExecutor#removeDeploymentListener
	 * (org.fordiac.ide.deployment.util.IDeploymentListener)
	 */
	public void removeDeploymentListener(final IDeploymentListener listener) {
		if (listeners.contains(listener)) {
			listeners.remove(listener);
		}
	}
}
