/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.ui.controls;

import org.eclipse.core.runtime.Status;
import org.eclipse.ui.plugin.AbstractUIPlugin;

public abstract class Abstract4DIACUIPlugin extends AbstractUIPlugin {

	/**
	 * Gets the id.
	 * 
	 * @return the symbolic name of this plugin
	 */
	public String getID() {
		return getBundle().getSymbolicName();
	}
	
	public void logError(String msg, Exception e) {
		getLog().log(new Status(Status.ERROR, getID(), msg, e));
	}
	
	public void logError(String msg) {
		getLog().log(new Status(Status.ERROR, getID(), msg));
	}
	
	public void logWarning(String msg, Exception e) {
		getLog().log(new Status(Status.WARNING, getID(), msg, e));
	}
	
	public void logWarning(String msg) {
		getLog().log(new Status(Status.WARNING, getID(), msg));
	}
	
	public void logInfo(String msg) {
		getLog().log(new Status(Status.INFO, getID(), msg));
	}
}
