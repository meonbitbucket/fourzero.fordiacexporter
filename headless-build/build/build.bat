REM @echo off

cls
setlocal
set BUILDER=%WORKSPACE%\headless-build\buildConfiguration
set ECLIPSE=C:\EclipseTestEnvironment\R_3_7_1
set EQUINOX=org.eclipse.equinox.launcher_1.2.0.v20110502
set PDE=org.eclipse.pde.build_3.7.0.v20111116-2009
set TARGET_W=%WORKSPACE%
set SRC_DIR=%WORKSPACE%

java -jar %ECLIPSE%/plugins/%EQUINOX%.jar -application org.eclipse.ant.core.antRunner -DbuildDirectory=%TARGET_W% -Dbase=%ECLIPSE% -Dproduct=%TARGET_W%\plugins\org.fordiac.ide\4DIAC-IDE.product -buildfile %ECLIPSE%/plugins/%PDE%/scripts/productBuild/productBuild.xml -Dbuilder=%BUILDER% 

