set version=1.3.0
set WS=%WORKSPACE:/=\%


IF EXIST %WS%\export rmdir /S /Q %WS%\export
REM finished if 
mkdir %WS%\export

%WS%\7z\7z x %WS%\I.4DIAC-IDE\4DIAC-IDE-win32.win32.x86.zip -o%WS%\export
rename %WS%\export\4DIAC-IDE 4DIAC-IDE_%version% 

%WS%\7z\7z x %WS%\I.4DIAC-IDE\4DIAC-IDE-linux.gtk.x86.zip -o%WS%\export
rename %WS%\export\4DIAC-IDE 4DIAC-IDE_%version%_Linux 

REM copy typelibrary
IF NOT EXIST %WS%\export\4DIAC-IDE_%version%\typelibrary mkdir %WS%\export\4DIAC-IDE_%version%\typelibrary
IF NOT EXIST %WS%\export\4DIAC-IDE_%version%_Linux\typelibrary mkdir %WS%\export\4DIAC-IDE_%version%_Linux\typelibrary
xcopy /E %WS%\typelibrary %WS%\export\4DIAC-IDE_%version%\typelibrary
xcopy /E %WS%\typelibrary %WS%\export\4DIAC-IDE_%version%_Linux\typelibrary

REM copy template
IF NOT EXIST %WS%\export\4DIAC-IDE_%version%\template mkdir %WS%\export\4DIAC-IDE_%version%\template
IF NOT EXIST %WS%\export\4DIAC-IDE_%version%_Linux\template mkdir %WS%\export\4DIAC-IDE_%version%_Linux\template
xcopy /E %WS%\template %WS%\export\4DIAC-IDE_%version%\template 
xcopy /E %WS%\template %WS%\export\4DIAC-IDE_%version%_Linux\template 

REM copy workspace
IF NOT EXIST %WS%\export\4DIAC-IDE_%version%\workspace mkdir %WS%\export\4DIAC-IDE_%version%\workspace
IF NOT EXIST %WS%\export\4DIAC-IDE_%version%_Linux\workspace mkdir %WS%\export\4DIAC-IDE_%version%_Linux\workspace
xcopy /E %WS%\workspace %WS%\export\4DIAC-IDE_%version%\workspace
xcopy /E %WS%\workspace %WS%\export\4DIAC-IDE_%version%_Linux\workspace

REM copy authors
copy %WS%\AUTHORS %WS%\export\4DIAC-IDE_%version%\AUTHORS 
copy %WS%\AUTHORS %WS%\export\4DIAC-IDE_%version%_Linux\AUTHORS 

REM copy changelog
copy %WS%\changelog.txt %WS%\export\4DIAC-IDE_%version%\changelog.txt
copy %WS%\changelog.txt %WS%\export\4DIAC-IDE_%version%_Linux\changelog.txt

REM copy copying
copy %WS%\COPYING %WS%\export\4DIAC-IDE_%version%\COPYING
copy %WS%\COPYING %WS%\export\4DIAC-IDE_%version%_Linux\COPYING

cd %WS%\export
%WS%\7z\7z a -tzip %WS%\export\4DIAC-IDE_%version%.zip %WS%\export\4DIAC-IDE_%version%
%WS%\7z\7z a -ttar %WS%\export\4DIAC-IDE_%version%_Linux.tar %WS%\export\4DIAC-IDE_%version%_Linux
%WS%\7z\7z a -tgzip %WS%\export\4DIAC-IDE_%version%_Linux.tar.gz %WS%\export\4DIAC-IDE_%version%_Linux.tar
cd %WS%